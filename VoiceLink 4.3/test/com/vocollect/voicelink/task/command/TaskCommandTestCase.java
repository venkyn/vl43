/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.test.DepInjectionSpringContextNGTests;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.task.service.TaskCommandService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dbunit.operation.DatabaseOperation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.testng.annotations.BeforeClass;


/**
 * The base class for all TaskCommand unit tests.
 *
 * @author ddoubleday
 */
public class TaskCommandTestCase extends DepInjectionSpringContextNGTests {
    
    protected static final String DATA_DIR = "data/dbunit/voicelink/";
    
    // Cache of responses to commands called as part of the test setup.
    private Map<String, Response> responses = new HashMap<String, Response>();
    
    // These are all auto-wired Spring beans
    private CommandDispatcher commandDispatcher;
    private SessionFactory sessionFactory;
    private TaskCommandService taskCommandService;
    private TaskVersionInfo taskVersionInfo;
    
    // Number of milliseconds in a second
    private static final Long MILLISECONDS_IN_SECOND = 1000L;

    // Constant to use to increment command date
    private static final Long TEN_MILLISECONDS = 10L;
    private Date cmdDate = null;
        
    /**
     *  This method should be used by task test cases to 
     *  set the command time for the task command.
     *  
     *  If the date member of this test object is null a new date
     *  will be created.  On subsequent calls, the date will be 
     *  incremented by 10 milliseconds.
     *  
     *  This is identical to how the task handles milliseconds.     
     *  Since the task can not send timestamps that contain milliseconds,
     *  the task fakes the millisecond portion of the timestamp.
     *    
     *  Time stamps sent from the task that contain milliseconds look like
     *  
     *  12:00:00.010
     *  12:00:00.020
     *  12:00:00.030
     *  
     *  This method does not generate timestamps in this manner.  
     *  It simply increments the date object's value by 10.
     *  @return Date the cmdDate
     */
    protected Date getCmdDateMS() {

        if (this.cmdDate == null) {
            cmdDate = new Date();
        } else {
            cmdDate.setTime(cmdDate.getTime() + TEN_MILLISECONDS);
        }
        return this.cmdDate;
    }
    
    /**
     *  This method should be used by task test cases to 
     *  set the command time for the task command.
     *  
     *  If the date member of this test object is null a new date
     *  will be created.  On subsequent calls, the date will be 
     *  incremented by 1 second.
     *    
     *  This will ensure that we we do not fail the check for duplicate
     *  task commands in the business logic.
     *  @return Date
     */
    protected Date getCmdDateSec() {
        if (this.cmdDate == null) {
            cmdDate = new Date();
        } else {
            cmdDate.setTime(cmdDate.getTime() + MILLISECONDS_IN_SECOND);
        }
        return this.cmdDate;
    }
  
    
    /**
     * @param theDate - the date to set
     */
    protected void setCmdDate(Date theDate) {
        this.cmdDate = theDate;
    }
    
    /**
     * Constructor.
     */
    public TaskCommandTestCase() {
        setAutowireMode(AUTOWIRE_BY_NAME);
    }

    /**
     * @return "CT-30-99-000"
     */
    public String badCombinedTaskVersion() {
        // It should be a long time before
        // 99 is an incompatible interface number
        String returnVal = "CT-30-99-000"; 
        return returnVal;
    }

    
    /**
     * @return "LL-30-99-000"
     */
    public String badLineloadingTaskVersion() {
        // It should be a long time before
        // 99 is an incompatible interface number
        String returnVal = "LL-30-99-000"; 
        return returnVal;
    }

    /**
     * @return the cached application context.
     */
    private ApplicationContext getCtx() {
        return getContext(getConfigLocations());
    }
    
    /**
     * @param beanName the name of the bean to retrieve
     * @return the bean with the specified name in the Spring context.
     */
    protected Object getBean(String beanName) {
        return getCtx().getBean(beanName);
    }
    
    /**
     * Execute a generic command object.
     * 
     * @param cmdString - Command to execute
     * @param args - arguments for commands
     * @return - response from command
     * @throws Exception - exception
     */
    public Response executeLutCmd(String cmdString, String[] args)
        throws Exception {

        BaseTaskCommand cmd = (BaseTaskCommand) getBean(cmdString);

        getCommandDispatcher().mapArgumentsToCommand(args, cmd);
        Response response = getTaskCommandService().executeCommand(cmd);
        ResponseRecord record = response.getRecords().iterator().next();
        assertEquals(record.getErrorCode(), "0", record.getErrorMessage());
        this.responses.put(cmdString, response);
        // These make sure that everything is committed prior to later work.
        // This more accurately simulates the real-world app environment.
        getSessionFactory().getCurrentSession().flush();
        getSessionFactory().getCurrentSession().clear();
        return response;

    }

    
    /**
     * Execute a generic command object.
     * 
     * @param cmdString - Command to execute
     * @param args - arguments for commands
     * @return - response from command
     * @throws Exception - exception
     */
    public Response executeOdrCmd(String cmdString, String[] args)
        throws Exception {

        BaseTaskCommand cmd = (BaseTaskCommand) getBean(cmdString);

        getCommandDispatcher().mapArgumentsToCommand(args, cmd);
        Response response = getTaskCommandService().executeCommand(cmd);
        // These make sure that everything is committed prior to later work.
        // This more accurately simulates the real-world app environment.
        getSessionFactory().getCurrentSession().flush();
        getSessionFactory().getCurrentSession().clear();
        return response;

    }
    
    
    
    
    /**
     * Getter for the cmdDispatcher property.
     * @return CommandDispatcher value of the property
     */
    public CommandDispatcher getCommandDispatcher() {
        return this.commandDispatcher;
    }
    
    /**
     * getter for a specific response.
     * 
     * @param command - command to get response for
     * @return - Returns a specific response for a command that was previous executed
     */
    public Response getResponseForCommand(String command) {
        return this.responses.get(command);
    }
    
    /**
     * Getter for the sessionFactory property.
     * @return SessionFactory value of the property
     */
    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    /**
     * Getter for the cmdService property.
     * @return TaskCommandService value of the property
     */
    public TaskCommandService getTaskCommandService() {
        return this.taskCommandService;
    }


    /**
     * Getter for the taskVersionInfo property.
     * @return TaskVersionInfo value of the property
     */
    public TaskVersionInfo getTaskVersionInfo() {
        return this.taskVersionInfo;
    } 

    /**
     * @return ct.ver + "-000"
     */
    public String goodCombinedTaskVersion() {
        // A good version string has 4 parts
        // TaskVersion.properties only has 3 parts
        // because taskbuilder tags on the last part
        return this.taskVersionInfo.getCombinedTaskVersion() + "-000";
    } 
    
    /**
     * @return ll.ver + "-000"
     */
    public String goodLineLoadingTaskVersion() {
        // A good version string has 4 parts
        // TaskVersion.properties only has 3 parts
        // because taskbuilder tags on the last part
        return this.taskVersionInfo.getLineloadingTaskVersion() + "-000";
    } 


    /**
     * Return a Date from a String rep that is assumed to be in the standard
     * task command date format.
     * @param dateString a date representation to turn into a date
     * @return the corresponding Date object
     * @throws ParseException on failure to parse.
     */
    public Date makeDateFromString(String dateString) throws ParseException {
        return makeDateFromString(dateString, 
            BaseTaskCommand.TASK_COMMAND_DATE_FORMAT);
    } 
    
    /**
     * @param dateString a date representation to turn into a Date.
     * @param dateFormat the format of the DateString.
     * @return the corresponding Date object
     * @throws ParseException on failure to parse.
     */
    public Date makeDateFromString(String dateString, String dateFormat) 
        throws ParseException {
        
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        return formatter.parse(dateString);
      
    } 


    /**
     * @param date Date to convert to string
     * @return - return formatted string
     */
    public String makeStringFromDate(Date date) {
        return makeStringFromDate(
            date, BaseTaskCommand.TASK_COMMAND_DATE_FORMAT);
    } 
    
    /**
     * @param date Date to convert to string
     * @param dateFormat Format to convert to
     * @return - formated date string
     */
    public String makeStringFromDate(Date date, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        return formatter.format(date);
    } 
    
    /**
     * @param date Date to convert to string
     * @return - return formatted string
     */
    public String makeStringFromDateMS(Date date) {
        return makeStringFromDate(
            date, BaseTaskCommand.TASK_COMMAND_MILLI_DATE_FORMAT);
    } 
    
    /**
     * @param date Date to convert to string
     * @param dateFormat Format to convert to
     * @return - formated date string
     */
    public String makeStringFromDateMS(Date date, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        return formatter.format(date);
    } 


    /**
     * Setter for the commandDispatcher property.
     * @param commandDispatcher the new commandDispatcher value
     */
    public void setCommandDispatcher(CommandDispatcher commandDispatcher) {
        this.commandDispatcher = commandDispatcher;
    }
    
    /**
     * Setter for the sessionFactory property.
     * @param sessionFactory the new sessionFactory value
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
    /**
     * Setter for the taskCommandService property.
     * @param taskCommandService the new taskCommandService value
     */
    public void setTaskCommandService(TaskCommandService taskCommandService) {
        this.taskCommandService = taskCommandService;
    }

    
    
    /**
     * Setter for the taskVersionInfo property.
     * @param taskVersionInfo the new taskVersionInfo value
     */
    public void setTaskVersionInfo(TaskVersionInfo taskVersionInfo) {
        this.taskVersionInfo = taskVersionInfo;
    }


    
    /**
     * Class-wide setup.
     * @throws Exception any
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * runs dbunit insert command on file.
     * 
     * @param file - file to run
     * @throws Exception - any exception
     */
    protected void classSetupCleanInsert(String file) throws Exception {
        classSetupDeleteAll(file, DatabaseOperation.CLEAN_INSERT);
    }

    /**
     * Clears system and inserts core data only (BreakTypes, Items,
     *  and Locations).
     * @throws Exception - Any Exception
     */
    protected void classSetupCore() throws Exception {
        classSetupCleanInsert("VoiceLinkDataUnitTest_Core.xml");
    }
    
    /**
     * runs dbunit delete all command on file.
     * 
     * @param file - file to run
     * @throws Exception - any exception
     */
    protected void classSetupDeleteAll(String file) throws Exception {
        classSetupDeleteAll(file, DatabaseOperation.DELETE_ALL);
    }
    
    /**
     * runs dbunit on file with operation specified.
     * 
     * @param file - file to run
     * @param operation - operation to use on file
     * @throws Exception - any exception
     */
    protected void classSetupDeleteAll(String file, DatabaseOperation operation) 
    throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.handleFlatXmlResource(DATA_DIR + file, operation);
    }
    
    /**
     * runs dbunit insert command on file.
     * 
     * @param file - file to run
     * @throws Exception - any exception
     */
    protected void classSetupInsert(String file) throws Exception {
        classSetupDeleteAll(file, DatabaseOperation.INSERT);
    }

    /**
     * Clears system and inserts Line Loading Carton data only. 
     * 
     * @throws Exception - Any Exception
     */
    protected void classSetupLineLoadingData() throws Exception {
        classSetupCleanInsert("VoiceLinkDataUnitTest_LineLoadData.xml");
    }
    
    /**
     * Clears All Line Loading Data and inserts Line Loading 
     * regions and work authroization.
     *  
     * @throws Exception - Any Exception
     */
    protected void classSetupLineLoadingRegions() throws Exception {
        classSetupDeleteAll("VoiceLinkDataUnitTest_LineLoadReset.xml");
        classSetupInsert("VoiceLinkDataUnitTest_LineLoadRegions.xml");
    }
    
    
    /**
     * Clears system and inserts Putaway License data only.
     * 
     * @throws Exception - Any Exception
     */
    protected void classSetupPutAwayData() throws Exception {
        classSetupCleanInsert("VoiceLinkDataUnitTest_PutawayData.xml");
    }
    
    /**
     * Clears All Put away Data and inserts put away regions and work authorization data.
     *  
     * @throws Exception - Any Exception
     */
    protected void classSetupPutAwayRegions() throws Exception {
        classSetupDeleteAll("VoiceLinkDataUnitTest_PutawayReset.xml");
        classSetupInsert("VoiceLinkDataUnitTest_PutawayRegions.xml");
    }
    
    
    /**
     * Clears system and inserts replenishment data only.
     * 
     * @throws Exception - Any Exception
     */
    protected void classSetupReplenData() throws Exception {
        classSetupCleanInsert("VoiceLinkDataUnitTest_ReplenData.xml");
    }

    /**
     * Clears All Replenishment Data and inserts replenishment regions and work authorization data.
     *  
     * @throws Exception - Any Exception
     */
    protected void classSetupReplenRegions() throws Exception {
        classSetupDeleteAll("VoiceLinkDataUnitTest_ReplenReset.xml");
        classSetupInsert("VoiceLinkDataUnitTest_ReplenRegions.xml");
    }
    
    /**
     * Clears All Put to store data and inserts put to store regions and work authorization data.
     *  
     * @throws Exception - Any Exception
     */
    protected void classSetupPTSRegions() throws Exception {
        classSetupDeleteAll("VoiceLinkDataUnitTest_PTSReset.xml");
        classSetupInsert("VoiceLinkDataUnitTest_PTSRegions.xml");
    }
    
    
    
    /**
     * Clears system and inserts Assignment/Pick data only. 
     * 
     * @throws Exception - Any Exception
     */
    protected void classSetupSelectionData() throws Exception {
        classSetupCleanInsert("VoiceLinkDataUnitTest_SelectionData.xml");
    }

    /**
     * Clears All Selection Data and inserts selection 
     * regions and work authorization.
     *  
     * @throws Exception - Any Exception
     */
    protected void classSetupSelectionRegions() throws Exception {
        classSetupDeleteAll("VoiceLinkDataUnitTest_SelectionReset.xml");
        classSetupInsert("VoiceLinkDataUnitTest_SelectionRegions.xml");
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.test.DepInjectionSpringContextNGTests#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        String[] daoConfigs = VoiceLinkDAOTestCase.getDAOTestConfigLocations();
        String[] configs = new String[daoConfigs.length + 3];
        System.arraycopy(daoConfigs, 0, configs, 0, daoConfigs.length);
        configs[configs.length -  3] = 
            "classpath*:/applicationContext-epp-events.xml";
        configs[configs.length -  2] = 
            "classpath*:/applicationContext-voicelink-events.xml";
        configs[configs.length -  1] = 
        "classpath*:/applicationContext-voicelink-task*.xml";
       
        
        
            
        
        return configs;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.test.DepInjectionSpringContextNGTests#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        // The following code creates a session that is used throughout
        // the command test, avoiding lazy-loading errors.
        Session session = SessionFactoryUtils.getSession(
            this.sessionFactory, true);
        TransactionSynchronizationManager.bindResource(
            sessionFactory, new SessionHolder(session));
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.test.DepInjectionSpringContextNGTests#onTearDown()
     */
    @Override
    protected void onTearDown() throws Exception {
        // The following code tears down the session after each test method
        Session session = ((SessionHolder) 
            TransactionSynchronizationManager.getResource(sessionFactory)).getSession();
        TransactionSynchronizationManager.unbindResource(sessionFactory);
        SessionFactoryUtils.releaseSession(session, sessionFactory);
        super.onTearDown();
    }
    
    /**
     * Verify that the correct number of fields were returned from the task command.
     * Also, verify that the field names match those defined in the XML bean definition.
     * 
     * This method should be called after a successful call to a task command 
     * in at least one test for each task command.
     * 
     * @param response - the response object returned by the call to the task command.
     *  
     */
    
    protected void validateCommandFields(Response response) {

        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        String [] fieldNames = response.getFields();

        // verify that the number of fields returned by the command bean was the same as 
        // the number of fields defined in the XML bean definition.
        assertEquals(record.size(), fieldNames.length, " mis-match between XML bean definition and fields returned.");
        
        // verify that all fields defined in the XML are keys in the hash returned by the command.
        for (int i = 0; i < fieldNames.length; i++) {
            // NOTE: there may be formatting characters in the field names that the proxy uses
            //       to format the data returned to the task. For example, the preAisle field name 
            //       can look like "preAisleDirection:%-50s"
            //       We need to strip out any formatting characters before we test if the key 
            //       is contained in the hash table.
            String fieldName = fieldNames[i];
            if (fieldName.contains(":")) {
                fieldName = fieldName.substring(0, fieldName.indexOf(":"));
            }
            assertTrue(record.containsKey(fieldName), fieldName + " was not found.");
        }
    }
}
