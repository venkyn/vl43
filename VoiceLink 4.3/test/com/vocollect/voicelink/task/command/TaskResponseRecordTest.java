/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;


/**
 * 
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, TASK })
public class TaskResponseRecordTest {

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.TaskResponseRecord#TaskResponseRecord()}.
     */
    public void testTaskResponseRecord() {
        TaskResponseRecord trr = new TaskResponseRecord();
        assertEquals("empty constructor", trr.getErrorCode(), 
            Integer.toString(TaskResponseRecord.SUCCESS_ERROR_CODE));
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.TaskResponseRecord#TaskResponseRecord(int)}.
     */
    @Test()
    public void testTaskResponseRecordInt() {
        TaskResponseRecord trr = new TaskResponseRecord(30);
        assertEquals("size constructor", trr.getErrorCode(), 
            Integer.toString(TaskResponseRecord.SUCCESS_ERROR_CODE));
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.TaskResponseRecord#nullSafeGet(java.lang.String)}.
     */
    @Test()
    public void testNullSafeGet() {
        TaskResponseRecord trr = new TaskResponseRecord();
        trr.put("foo", null);
        assertEquals("nullValue", "", trr.nullSafeGet("foo"));
        assertEquals("missingEntry", "", trr.nullSafeGet("bar"));
    }

}
