/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.model.Device;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.service.impl.OperatorManagerImpl;
import com.vocollect.voicelink.core.service.impl.TerminalManagerImpl;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Tests for BaseTaskCommand class.
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, TASK })
public class BaseTaskCommandTest extends TaskCommandTestCase {

    private BaseTaskCommand cmd;

    private SiteContext siteContext;

    /**
     * Getter for the siteContext property.
     * @return SiteContext value of the property
     */
    public SiteContext getSiteContext() {
        return siteContext;
    }


    /**
     * Setter for the siteContext property.
     * @param siteContext the new siteContext value
     */
    @Test(enabled = false)
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }


    /**
     * Each test setup.
     * @throws DataAccessException
     */
    @BeforeMethod
    protected void methodSetUp() throws DataAccessException {
        // just an example one.
        this.cmd = (BaseTaskCommand) getBean("cmdPrTaskLUTCoreConfiguration");

        setSiteContext((SiteContext) getBean("siteContext"));
        getSiteContext().setCurrentSite(-1L);
        getSiteContext().setHasAllSiteAccess(true);

        SiteContextHolder.setSiteContext(getSiteContext());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.BaseTaskCommand#getCommandTime()}.
     * @throws Exception any
     */
    @Test()
    public void testGetSetCommandTime() throws Exception {
        Date now = new Date();
        this.cmd.setCommandTime(now);
        assertEquals(now, this.cmd.getCommandTime());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.BaseTaskCommand#getOperatorId()}.
     */
    @Test()
    public void testGetSetOperatorId() {
        this.cmd.setOperatorId("foo");
        assertEquals("foo", this.cmd.getOperatorId());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.BaseTaskCommand#getSerialNumber()}.
     */
    @Test()
    public void testGetSetSerialNumber() {
        this.cmd.setSerialNumber("foo");
        assertEquals("foo", this.cmd.getSerialNumber());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.BaseTaskCommand#getDispatchArgs()}.
     */
    @Test()
    public void testGetSetDispatchArgs() {
        this.cmd.setDispatchArgs(new String[] {"foo"});
        // Default args are added
        assertEquals(this.cmd.getDispatchArgs().length, 4, "argListLength");
        assertEquals("foo", this.cmd.getDispatchArgs()[3], "wrongArgument");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.BaseTaskCommand#getDispatchName()}.
     */
    @Test()
    public void testGetSetDispatchName() {
        this.cmd.setDispatchName("foo");
        assertEquals("foo", this.cmd.getDispatchName());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.BaseTaskCommand#isReadOnly()}.
     */
    @Test()
    public void testIsSetReadOnly() {
        this.cmd.setReadOnly(false);
        assertFalse(this.cmd.isReadOnly(), "readOnlyFalse");
        this.cmd.setReadOnly(true);
        assertTrue(this.cmd.isReadOnly(), "readOnlyTrue");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.BaseTaskCommand#getOperatorManager()}.
     */
    @Test()
    public void testGetSetOperatorManager() {
        this.cmd.setOperatorManager(new OperatorManagerImpl(null));
        assertNotNull(this.cmd.getOperatorManager());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.BaseTaskCommand#getTerminalManager()}.
     */
    @Test()
    public void testGetSetTerminalManager() {
        this.cmd.setTerminalManager(new TerminalManagerImpl(null));
        assertNotNull(this.cmd.getTerminalManager());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.BaseTaskCommand#getOperator()}.
     * @throws Exception any
     */
    @Test()
    public void testGetOperator() throws Exception {
        this.cmd.setOperatorId("testGetOperatorFromCmd");
        Operator oper = new Operator();
        oper.setOperatorIdentifier("testGetOperatorFromCmd");
        oper.setPassword("pass");
        this.cmd.getOperatorManager().save(oper);
        assertNotNull(this.cmd.getOperator());
        this.cmd.getOperatorManager().delete(oper);
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.BaseTaskCommand#getTerminal()}.
     * @throws Exception any
     */
    @Test()
    public void testGetTerminal() throws Exception {
        this.cmd.setSerialNumber("testGetTerminalFromCmd");
        Device term = new Device();
        term.setSerialNumber("testGetTerminalFromCmd");
        this.cmd.getTerminalManager().save(term);
        assertNotNull(this.cmd.getTerminal());
        this.cmd.getTerminalManager().delete(term);
    }

    /**
     * Test method for {@link com.vocollect.voicelink.task.command.BaseTaskCommand#testDuplicateLUTCommand()}.
     * @throws Exception any
     */
    @Test()
    public void testDuplicateLUTCommand() throws Exception {
           
        // NOTE: the test to check for duplicate ODR's is contained in the PickedODRCmdTest
        //       for ease of test setup.
        this.cmd = (BaseTaskCommand) getBean("cmdPrTaskLUTCoreConfiguration");
        final String sn = "serialConfig";
        final String operId = "operConfig";
        String version = "CT-31-03-072";
        Locale locale = Locale.US;

        // Now put args in the command and execute it.
        Date cmdDate = new Date();
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDateMS(cmdDate),
                sn, operId, locale.toString(), "Default", version},
            this.cmd);

        // Call the core configuration command twice using the same timestamp.
        Response response = getTaskCommandService().executeCommand(this.cmd);

        // create a second command object
        BaseTaskCommand cmd1;
        cmd1 = (BaseTaskCommand) this.getBean("cmdPrTaskLUTCoreConfiguration");
        // Now put args in the command and execute it.
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDateMS(cmdDate),
                              sn, operId, locale.toString(), "Default", version}, cmd1);
        response = getTaskCommandService().executeCommand(cmd1);

        // Verify that we detected the duplicate command call.
        List<ResponseRecord> records = response.getRecords();
        records = response.getRecords();
        assertEquals(1, records.size(), "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "1038",  "Wrong error code");
        assertEquals("Duplicate command detected", record.getErrorMessage(), "Wrong error message");
    }
}
