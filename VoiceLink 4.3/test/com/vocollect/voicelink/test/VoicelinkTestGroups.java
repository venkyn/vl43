/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.test;

import com.vocollect.epp.test.TestGroups;

/**
 * Voicelink specific test groups.
 *
 * @author estoll
 */
public class VoicelinkTestGroups extends TestGroups {

    // FEATURE CATEGORIES
    public static final String CORE = "core";
    public static final String SELECTION = "selection";
    public static final String PUTAWAY = "putaway";
    public static final String LINELOADING = "lineloading";
    public static final String REPLENISHMENT = "replenishment";
    public static final String PUTTOSTORE = "puttostore";
    public static final String CYCLECOUNTING = "cyclecounting";
    public static final String LOADING = "loading";

}
