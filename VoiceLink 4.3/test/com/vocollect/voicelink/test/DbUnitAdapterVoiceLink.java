/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.test;

import com.vocollect.epp.test.DbUnitAdapter;

import org.dbunit.database.DatabaseConnection;


/**
 * DbUnitAdapter extension with VoiceLink-specific methods.
 *
 * @author ddoubleday
 */
public class DbUnitAdapterVoiceLink extends DbUnitAdapter {

    private static final String MULTI_SITE_ROOT = "data/dbunit/voicelink/VoiceLinkDataMultiSite";

    /**
     * Load all the multi-site data for VoiceLink.
     * @throws Exception on any failure.
     */
    public void loadFullMultiSiteData() throws Exception {

        DatabaseConnection connection = (DatabaseConnection) getConnection();

        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Admin.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Core.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Selection.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Putaway.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Replenishment.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "LineLoading.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "PutToStore.xml");
    }

    /**
     * Load all the multi-site data for Selection.
     * @throws Exception on any failure.
     */
    public void loadSelectionMultiSiteData() throws Exception {

        DatabaseConnection connection = (DatabaseConnection) getConnection();

        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Admin.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Core.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Selection.xml");
    }

    /**
     * Load all the multi-site data for Core.
     * @throws Exception on any failure.
     */
    public void loadCoreMultiSiteData() throws Exception {

        DatabaseConnection connection = (DatabaseConnection) getConnection();

        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Admin.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Core.xml");
    }

    /**
     * Load all the multi-site data for Replenishment.
     * @throws Exception on any failure.
     */
    public void loadReplenishmentMultiSiteData() throws Exception {

        DatabaseConnection connection = (DatabaseConnection) getConnection();

        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Admin.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Core.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Replenishment.xml");
    }

    /**
     * Load all the multi-site data for Put To Store.
     * @throws Exception on any failure.
     */
    public void loadPutToStoreMultiSiteData() throws Exception {

        DatabaseConnection connection = (DatabaseConnection) getConnection();

        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Admin.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "Core.xml");
        loadClientInstallationData(connection, MULTI_SITE_ROOT + "PutToStore.xml");
    }


}
