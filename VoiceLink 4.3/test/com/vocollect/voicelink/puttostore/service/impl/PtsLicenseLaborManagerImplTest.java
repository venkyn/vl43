/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLabor;
import com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManager;
import com.vocollect.voicelink.puttostore.service.PtsLicenseManager;

import java.util.Date;
import java.util.List;

import org.testng.annotations.Test;


/**
 *
 *
 * @author treed
 */
public class PtsLicenseLaborManagerImplTest extends BaseServiceManagerTestCase {

    private PtsLicenseLaborManager ptsLicenseLaborManager;

    private PtsLicenseManager ptsLicenseManager;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_PtsRegions.xml");
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_PtsLicenseLabor.xml");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.service.impl.PtsLicenseLaborManagerImplRoot#PtsLicenseLaborManagerImplRoot(com.vocollect.voicelink.puttostore.dao.PtsLicenseLaborDAO)}.
     */
    @Test()
    public void testPtsLicenseLaborManagerImplRoot() {

    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.service.impl.PtsLicenseLaborManagerImplRoot#closeLaborRecord(java.util.Date, com.vocollect.voicelink.puttostore.model.PtsLicense)}.
     */
    @Test()
    public void testCloseLaborRecord() throws DataAccessException, BusinessRuleException {
        //licenseId corresponds to a license with one open labor record.
        long licenseId = -792;
        //Confirm we have open labor.
        PtsLicenseLabor labor = ptsLicenseLaborManager.findOpenRecordByLicenseId(licenseId);
        assertNotNull(labor, "No open labor records returned when they were expected.");

        //Close the labor record.
        PtsLicense license = ptsLicenseManager.get(licenseId);
        ptsLicenseLaborManager.closeLaborRecord(new Date(), license);

        //Confirm we have no open labor.
        labor = ptsLicenseLaborManager.findOpenRecordByLicenseId(licenseId);
        assertNull(labor, "Open labor records returned when they weren't expected.");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.service.impl.PtsLicenseLaborManagerImplRoot#findOpenRecordByLicenseId(long)}.
     */
    @Test()
    public void testFindOpenRecordByLicenseId() throws DataAccessException {
        // licenseId1 corresponds to a license with one open ptsLicenseLabor record.
        long licenseId1 = -790;
        // licenseId2 corresponds to a license with one closed ptsLicenseLabor record.
        long licenseId2 = -791;
        PtsLicenseLabor labor = ptsLicenseLaborManager.findOpenRecordByLicenseId(licenseId1);
        assertNotNull(labor, "No open labor records returned when they were expected.");
        labor = ptsLicenseLaborManager.findOpenRecordByLicenseId(licenseId2);
        assertNull(labor, "Open labor records returned when they weren't expected.");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.service.impl.PtsLicenseLaborManagerImplRoot#listAllRecordsByOperatorLaborId(long)}.
     */
    @Test()
    public void testListAllRecordsByOperatorLaborId() throws DataAccessException {
        // operator labor associated with one closed and one open record.
        long operatorLaborId = 3002;
        List<PtsLicenseLabor> laborList = ptsLicenseLaborManager.listAllRecordsByOperatorLaborId(operatorLaborId);
        assertEquals(laborList.size(), 2, "Did not return all open and closed labor");

        // operator labor associated with no license labor.
        long operatorLaborIdWithoutLicenseLabor = 3003;
        laborList = ptsLicenseLaborManager.listAllRecordsByOperatorLaborId(operatorLaborIdWithoutLicenseLabor);
        assertEquals(laborList.size(), 0, "Returned labor when no labor was expected.");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.service.impl.PtsLicenseLaborManagerImplRoot#listClosedRecordsByBreakLaborId(long)}.
     */
    @Test()
    public void testListClosedRecordsByBreakLaborId() throws DataAccessException {
        //Two closed and one open labor record contain this breakLaborId.
        long breakLaborId = -101;

        List<PtsLicenseLabor> laborList = ptsLicenseLaborManager.listClosedRecordsByBreakLaborId(breakLaborId);
        for (PtsLicenseLabor pll : laborList) {
            assertEquals(pll.getBreakLaborId().longValue(), breakLaborId, "Break Labor ID returned but not requested.");
            assertNotNull(pll.getEndTime(), "Open record returned when only closed were requested.");
        }
    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.service.impl.PtsLicenseLaborManagerImplRoot#listOpenRecordsByOperatorId(long)}.
     */
    @Test()
    public void testListOpenRecordsByOperatorId() throws DataAccessException {
        //operator labor associated with one closed and one open record.
        long operatorLaborId = 302;
        List<PtsLicenseLabor> laborList = ptsLicenseLaborManager.listOpenRecordsByOperatorId(operatorLaborId);
        for (PtsLicenseLabor pll : laborList) {
            assertNull(pll.getEndTime(), "Labor record returned was closed when it should have been open.");
        }
    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.service.impl.PtsLicenseLaborManagerImplRoot#openLaborRecord(java.util.Date, com.vocollect.voicelink.core.model.Operator, com.vocollect.voicelink.puttostore.model.PtsLicense, com.vocollect.voicelink.core.model.OperatorLabor)}.
     */
    @Test()
    public void testOpenLaborRecord() throws DataAccessException, BusinessRuleException {
        //licenseId corresponds to a license with one open labor record.
        long licenseId = -793;
        long originalLaborId = 103;
        //Confirm we have open labor.
        PtsLicenseLabor labor = ptsLicenseLaborManager.findOpenRecordByLicenseId(licenseId);
        assertNotNull(labor, "No open labor records returned when they were expected.");
        assertEquals(new Long(originalLaborId), labor.getId(), "Open labor ID does match original ID.");

        //Open a new labor record (close existing record on the process).
        ptsLicenseLaborManager.openLaborRecord(
            new Date(),
            labor.getOperator(),
            labor.getLicense(),
            labor.getOperatorLabor());

        //Confirm we have new open labor.
        labor = ptsLicenseLaborManager.findOpenRecordByLicenseId(licenseId);
        assertNotNull(labor, "No open labor records returned when they were expected.");
        assertNotSame(originalLaborId, labor.getId().longValue(), "New open labor ID match original ID.");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.service.impl.PtsLicenseLaborManagerImplRoot#sumQuantityPut(long)}.
     */
    @Test()
    public void testSumQuantityPut() throws DataAccessException {
        //licenseId corresponds to a license with two closed license labor records
        long operatorLaborId = 3006;

        // Get all the records associated with this operatorLabor record.
        List<PtsLicenseLabor> laborList = ptsLicenseLaborManager.listAllRecordsByOperatorLaborId(operatorLaborId);

        // Add up the quantityPut for each labor record.
        int manualSum = 0;
        for (PtsLicenseLabor pll : laborList) {
            manualSum += pll.getQuantityPut();
        }

        // Use the method to calculate the total quantity put.
        Long querySum = ptsLicenseLaborManager.sumQuantityPut(operatorLaborId);

        assertEquals(manualSum, querySum.intValue(), "Sum of Quantity Put not correct.");
    }

    /**
     * Getter for the ptsLicenseLaborManager property.
     * @return PtsLicenseLaborManager value of the property
     */
    @Test(enabled = false)
    public PtsLicenseLaborManager getPtsLicenseLaborManager() {
        return this.ptsLicenseLaborManager;
    }


    /**
     * Setter for the ptsLicenseLaborManager property.
     * @param ptsLicenseLaborManager the new ptsLicenseLaborManager value
     */
    @Test(enabled = false)
    public void setPtsLicenseLaborManager(PtsLicenseLaborManager ptsLicenseLaborManager) {
        this.ptsLicenseLaborManager = ptsLicenseLaborManager;
    }


    /**
     * Getter for the ptsLicenseManager property.
     * @return PtsLicenseManager value of the property
     */
    @Test(enabled = false)
    public PtsLicenseManager getPtsLicenseManager() {
        return this.ptsLicenseManager;
    }


    /**
     * Setter for the ptsLicenseManager property.
     * @param ptsLicenseManager the new ptsLicenseManager value
     */
    @Test(enabled = false)
    public void setPtsLicenseManager(PtsLicenseManager ptsLicenseManager) {
        this.ptsLicenseManager = ptsLicenseManager;
    }

}
