/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;
import com.vocollect.voicelink.puttostore.model.PtsRegion;
import com.vocollect.voicelink.puttostore.service.PtsLicenseManager;
import com.vocollect.voicelink.puttostore.service.PtsRegionManager;
import com.vocollect.voicelink.selection.model.AssignmentGroup;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.util.Date;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * 
 * 
 * @author estoll
 */
@Test(groups = { FAST, PUTTOSTORE })

public class PtsLicenseManagerImplTest extends BaseServiceManagerTestCase {

    private PtsLicenseManager manager = null;

    private PtsRegionManager ptsRegionManager;
    
    /**
     * @return - ptsRegion manager
     */
    @Test(enabled = false)
    public PtsRegionManager getPtsRegionManager() {
        return ptsRegionManager;
    }

    /**
     * @param ptsRegionManager - set ptsRegionManager
     */
    @Test(enabled = false)
    public void setPtsRegionManager(PtsRegionManager ptsRegionManager) {
        this.ptsRegionManager = ptsRegionManager;
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_PtsRegions.xml", DatabaseOperation.CLEAN_INSERT);
    }

    /**
     * @param impl - implementation of assignment manager.
     */
    @Test(enabled = false)
    public void setPtsLicenseManager(PtsLicenseManager impl) {
        this.manager = impl;
    }

    /**
     * Main thing being tested here is that
     * when saving a license, the PartialNumber field
     * is set based upon the region's definition
     * of how many digits an operator must speak
     * when entering licenses.
     * 
     * @throws Exception - any exception
     */
    @Test(groups = { PERFORMANCE, SLOW })
    public void testSavePtsLicense() throws Exception {

        Long newId;
        PtsLicense l = new PtsLicense();
        // Region -44 has 0 for spokenLicenseLength
        // therefore the entire license is the partialNumber
        PtsRegion r = getPtsRegionManager().get(-44L); 
        l.setCreatedDate(new Date());
        l.setNumber("12345678");
        l.setGroupInfo(new AssignmentGroup());
        l.changeStatus(PtsLicenseStatus.Available);
        l.setRegion(r);
        manager.save(l);
        newId = l.getId();

        PtsLicense l2 = manager.get(newId);
        assertEquals(l.getNumber(), l2.getPartialNumber(), 
            "Region -44 PTS License Partial Numbers do not match");
        
        // Now test a partial number
        l = new PtsLicense();
        // Region -43 has 4 for spokenLicenseLength
        // therefore, the last 4 digits of the license
        // will be the partialNumber
        r = getPtsRegionManager().get(-43L); 
        l.setCreatedDate(new Date());
        l.setNumber("12345678");
        l.setGroupInfo(new AssignmentGroup());
        l.changeStatus(PtsLicenseStatus.Available);
        l.setRegion(r);
        manager.save(l);
        newId = l.getId();
        
        l2 = manager.get(newId);
        assertEquals(l2.getPartialNumber(), "5678", 
            "Region -43 PTS License Partial Numbers do not match");
        
    }

}
