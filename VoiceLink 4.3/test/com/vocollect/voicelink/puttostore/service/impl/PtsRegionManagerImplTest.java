/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsRegion;
import com.vocollect.voicelink.puttostore.service.PtsRegionManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;


/**
 *
 * This is a test class for the put to store region service.
 *
 * @author svoruganti
 */
@Test(groups = { FAST, PUTTOSTORE })
public class PtsRegionManagerImplTest  extends BaseServiceManagerTestCase {

    private PtsRegionManager ptsRegionManager;

    /**
     * @return - ptsRegion manager
     */
    @Test(enabled = false)
    public PtsRegionManager getPtsRegionManager() {
        return ptsRegionManager;
    }

    /**
     * @param ptsRegionManager - set ptsRegionManager
     */
    @Test(enabled = false)
    public void setPtsRegionManager(PtsRegionManager ptsRegionManager) {
        this.ptsRegionManager = ptsRegionManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataMultiSiteAdmin.xml",
            DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataMultiSiteCore.xml",
            DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataMultiSitePutToStore.xml",
            DatabaseOperation.REFRESH);
     }

    /**
     * Set the site context.
     * @param siteId - the site ID
     * @throws Exception
     *
     */
    protected void setSiteContext(Long siteId) throws Exception {
        this.setUpSiteContext(true);
        SiteContextHolder.getSiteContext().setCurrentSite(siteId);
     }


    /**
     * Test to verify that there are no operators signed in.
     *
     * @throws Exception - any exception
     */
    public void testVerifyNoOpertorsSignedIn() throws Exception {
        setSiteContext(-3L);

        // no operator(s) signed into the region
        try {
            PtsRegion ptsRegion = ptsRegionManager.get(new Long(-251L));
            ptsRegionManager.executeValidateEditRegion(ptsRegion);
        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
            assertEquals(
                e.getErrorCode().toString(), "VoiceLink-10005",
                "Expected error code VoiceLink-10005");

        }

    }


    /**
     * Test updating the partial Number.
     *
     * @throws Exception - any exception
     */
    public void testValidateLicenseStatus() throws Exception {

        setSiteContext(-2L);

        //There are licenses in the region with a status of In-Progress=true ,
        // Suspended=false, and Passed=true

        try {

            PtsRegion ptsRegion  = ptsRegionManager.get(new Long(-151L));
            ptsRegionManager.executeValidateEditRegion(ptsRegion);

        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
            assertEquals(
                e.getErrorCode().toString(),
                "VoiceLink-10006",
                "Expected error code VoiceLink-10006");
        }

    }


    /**
     * Test method for validateDigitsSpoken.
     *
     *
     * @throws Exception -Any Exception
     */
    public void testValidateDigitsSpoken() throws Exception {
        setSiteContext(-1L);

        try {
            PtsRegion ptsRegion  =  ptsRegionManager.get(new Long(-53L));
            ptsRegionManager.executeValidateEditRegion(ptsRegion);

        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
            assertEquals(
                e.getErrorCode().toString(),
                "VoiceLink-10007",
                "Expected error code VoiceLink-10007");
        }
    }

    /**
     * Test updating the partial work identifiers to full work id.
     *
     * @throws Exception - any exception
     */
    public void testUpdatePartialLicenseNumberAll() throws Exception {
        setSiteContext(-3L);

        PtsRegion ptsRegion  = ptsRegionManager.get(new Long(-251L));

        //Check size all
        ptsRegion.setSpokenLicenseLength(0);
        ptsRegionManager.save(ptsRegion);

        ptsRegion = ptsRegionManager.get(new Long(-251L));

        for (PtsLicense l : ptsRegion.getLicenses()) {

            assertEquals(l.getPartialNumber(),
                l.getNumber(),
                "Expect same values");
        }
    }

    /**
     * Test updating the partial work identifiers to full work id.
     *
     * @throws Exception - any exception
     */
    public void testUpdateCustomPartialLicenseNumber() throws Exception {

        setSiteContext(-3L);

        PtsRegion ptsRegion  =  ptsRegionManager.get(new Long(-251L));

        //Check size 2
        ptsRegion.setSpokenLicenseLength(2);
        ptsRegionManager.save(ptsRegion);

        ptsRegion = ptsRegionManager.get(new Long(-251L));

        for (PtsLicense l : ptsRegion.getLicenses()) {
            assertEquals(
                l.getPartialNumber().length(),
                2,
                "Expect length of 2");
        }
    }


}
