/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.puttostore.model.PtsSummary;
import com.vocollect.voicelink.puttostore.service.PtsRegionManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * This class tests the summaries in region manager.
 *
 * @author svoruganti
 */
@Test(groups = { FAST, PUTTOSTORE })
public class PtsSummariesRegionManagerImplTest extends
    BaseServiceManagerTestCase {

    private PtsRegionManager ptsRegionManager;

    private static final long MILLIS_PER_DAY = 24 * 60 * 60 * 1000;

 // Constant for date format
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * @return - ptsRegion manager
     */
    @Test(enabled = false)
    public PtsRegionManager getPtsRegionManager() {
        return ptsRegionManager;
    }

    /**
     * @param ptsRegionManager - set ptsRegionManager
     */
    @Test(enabled = false)
    public void setPtsRegionManager(PtsRegionManager ptsRegionManager) {
        this.ptsRegionManager = ptsRegionManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();

        adapter.handleFlatXmlResource(
                DATA_DIR + "VoiceLinkDataUnitTest_Core.xml",
                DatabaseOperation.CLEAN_INSERT);
        adapter.handleFlatXmlResource(
                DATA_DIR + "VoiceLinkDataUnitTest_PTSWorkGroups.xml",
                DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(
                DATA_DIR + "VoiceLinkDataUnitTest_PTSOperators.xml",
                DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(
                DATA_DIR + "VoiceLinkDataUnitTest_PTSRegions.xml",
                DatabaseOperation.INSERT);
        // NOTE: The license and put data for this test is in the PTSSummaries.xml file
        //       so we do not break the summary information expected for this test.
        //       If this data were in the PTSData.xml file, every time we add license/put
        //       data, this test would break.
        adapter.handleFlatXmlResource(
                DATA_DIR + "VoiceLinkDataUnitTest_PTSSummaries.xml",
                DatabaseOperation.INSERT);
    }

    /**
     * This method tests number of records returned by each summary.
     * @throws Exception
     */
    public void testSummaryQueries() throws Exception {
        List<DataObject> objects = null;
        ResultDataInfo rdi = new ResultDataInfo();
        rdi.setQueryArgs(new Object[] { new Date() });
        objects = ptsRegionManager.listLicenseSummary(rdi);
        assertEquals(objects.size(), 5, "License Summary");

        objects = ptsRegionManager.listCurrentWorkSummary(rdi);
        assertEquals(objects.size(), 5, "Current Work Summary");

        objects = ptsRegionManager.listRouteSummary(rdi);
        assertEquals(objects.size(), 7, "Route Summary");
    }

    /**
     * Tests the route summary has right number of customers.
     *
     * @throws Exception
     */
    public void testRouteSummary() throws Exception {
        List<DataObject> objects = null;
        ResultDataInfo rdi = new ResultDataInfo();

        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
        rdi.setQueryArgs(new Object[] { formatter.parse("2007-01-29 15:46:54")});

        objects = ptsRegionManager.listRouteSummary(rdi);
        for (int i = 0; i < objects.size(); i++) {
            PtsSummary ptsSum = (PtsSummary) objects.get(i);
            if (ptsSum.getRoute().equals("15")) {
                assertEquals(
                    ptsSum.getNumberOfCustomers(), 1,
                    "Route Summary number of customers are wrong");
                assertEquals(
                    ptsSum.getTotalItems(), 12,
                    "Route Summary total items wrong");
                assertEquals(
                    ptsSum.getItemsRemaining(), 6,
                    "Route Summary remaining items wrong");
                assertEquals(
                    ptsSum.getItemsComplete(), 6,
                    "Route Summary complete items wrong");
                assertEquals(
                    ptsSum.getPercentComplete(), 50.0,
                    "Route Summary percent complete items wrong");
            }
        }

    }

    /**
     * Tests the route summary has right number of customers.
     *
     * @throws Exception
     */
    public void testRouteSummaryTotal() throws Exception {
        List<DataObject> objects = null;
        ResultDataInfo rdi = new ResultDataInfo();
        rdi.setQueryArgs(new Object[] { new Date(System.currentTimeMillis()
            - MILLIS_PER_DAY) });

        objects = ptsRegionManager.listRouteSummary(rdi);
        for (int i = 0; i < objects.size(); i++) {
            PtsSummary ptsSum = (PtsSummary) objects.get(i);
            if (ptsSum.getRoute().equals("14")) {
                assertEquals(
                    ptsSum.getTotalItems(),  ptsSum.getItemsRemaining() + ptsSum.getItemsComplete(),
                    "Total items are wrong in route summary ");

            }
        }

    }

    /**
     * Tests the route summary has correct percent complete.
     *
     * @throws Exception
     */
    public void testRouteSummaryPercentComplete() throws Exception {
        List<DataObject> objects = null;
        ResultDataInfo rdi = new ResultDataInfo();
        rdi.setQueryArgs(new Object[] { new Date(System.currentTimeMillis()
            - MILLIS_PER_DAY) });

        objects = ptsRegionManager.listRouteSummary(rdi);
        for (int i = 0; i < objects.size(); i++) {
            PtsSummary ptsSum = (PtsSummary) objects.get(i);
            if (ptsSum.getRoute().equals("16")) {
                assertEquals(
                    ptsSum.getPercentComplete(), 0.0,
                    "percent complete is wrong");

            }
        }

    }

    /**
     * Tests the route summary time window.
     *
     * @throws Exception
     */
    public void testRouteSummaryTimeWindow() throws Exception {
        List<DataObject> objects = null;
        ResultDataInfo rdi = new ResultDataInfo();
        SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
         //last 7 days
        rdi.setQueryArgs(new Object[] { formatter.parse("2007-01-25 15:46:54")});
        objects = ptsRegionManager.listRouteSummary(rdi);
          for (int i = 0; i < objects.size(); i++) {
            PtsSummary ptsSum = (PtsSummary) objects.get(i);
            if (ptsSum.getRoute().equals("14")) {
                assertEquals(ptsSum.getTotalItems(), 9, "total items wrong");
             }
            if (ptsSum.getRoute().equals("15")) {
                assertEquals(ptsSum.getTotalItems(), 12, "total items wrong");
            }
            if (ptsSum.getRoute().equals("16")) {
                assertEquals(ptsSum.getTotalItems(), 8, "total items wrong");
             }
            if (ptsSum.getRoute().equals("17")) {
                assertEquals(ptsSum.getTotalItems(), 5, "total items wrong");
            }
            if (ptsSum.getRoute().equals("18")) {
                assertEquals(ptsSum.getTotalItems(), 6, "total items wrong");
            }
            if (ptsSum.getRoute().equals("19")) {
                assertEquals(ptsSum.getTotalItems(), 7, "total items wrong");
            }
            if (ptsSum.getRoute().equals("20")) {
                assertEquals(ptsSum.getTotalItems(), 9, "total items wrong");
             }
        }
        //last 3 days
        rdi.setQueryArgs(new Object[] { formatter.parse("2007-01-28 15:46:53")});
        objects = ptsRegionManager.listRouteSummary(rdi);
        for (int i = 0; i < objects.size(); i++) {
            PtsSummary ptsSum = (PtsSummary) objects.get(i);
            if (ptsSum.getRoute().equals("14")) {
                assertEquals(ptsSum.getTotalItems(), 9, "total items wrong");
             }
            if (ptsSum.getRoute().equals("15")) {
                assertEquals(ptsSum.getTotalItems(), 12, "total items wrong");
            }
            if (ptsSum.getRoute().equals("16")) {
                assertEquals(ptsSum.getTotalItems(), 8, "total items wrong");
             }
            if (ptsSum.getRoute().equals("17")) {
                assertEquals(ptsSum.getTotalItems(), 5, "total items wrong");
            }
            if (ptsSum.getRoute().equals("18")) {
                assertEquals(ptsSum.getTotalItems(), 6, "total items wrong");
            }
            if (ptsSum.getRoute().equals("19")) {
                assertEquals(ptsSum.getTotalItems(), 7, "total items wrong");
            }
            if (ptsSum.getRoute().equals("20")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
             }
        }
        //last 2 days
         rdi.setQueryArgs(new Object[] { formatter.parse("2007-01-29 02:46:53")});
        objects = ptsRegionManager.listRouteSummary(rdi);
        for (int i = 0; i < objects.size(); i++) {
            PtsSummary ptsSum = (PtsSummary) objects.get(i);
            if (ptsSum.getRoute().equals("14")) {
                assertEquals(ptsSum.getTotalItems(), 9, "total items wrong");
             }
            if (ptsSum.getRoute().equals("15")) {
                assertEquals(ptsSum.getTotalItems(), 12, "total items wrong");
            }
            if (ptsSum.getRoute().equals("16")) {
                assertEquals(ptsSum.getTotalItems(), 8, "total items wrong");
             }
            if (ptsSum.getRoute().equals("17")) {
                assertEquals(ptsSum.getTotalItems(), 5, "total items wrong");
            }
            if (ptsSum.getRoute().equals("18")) {
                assertEquals(ptsSum.getTotalItems(), 6, "total items wrong");
            }
            if (ptsSum.getRoute().equals("19")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
            }
            if (ptsSum.getRoute().equals("20")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
             }
        }
         //last 24 hours
        rdi.setQueryArgs(new Object[] { formatter.parse("2007-01-30 02:46:53")});
        objects = ptsRegionManager.listRouteSummary(rdi);
        for (int i = 0; i < objects.size(); i++) {
            PtsSummary ptsSum = (PtsSummary) objects.get(i);
            if (ptsSum.getRoute().equals("14")) {
                assertEquals(ptsSum.getTotalItems(), 9, "total items wrong");
             }
            if (ptsSum.getRoute().equals("15")) {
                assertEquals(ptsSum.getTotalItems(), 12, "total items wrong");
            }
            if (ptsSum.getRoute().equals("16")) {
                assertEquals(ptsSum.getTotalItems(), 8, "total items wrong");
             }
            if (ptsSum.getRoute().equals("17")) {
                assertEquals(ptsSum.getTotalItems(), 5, "total items wrong");
            }
            if (ptsSum.getRoute().equals("18")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
            }
            if (ptsSum.getRoute().equals("19")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
            }
            if (ptsSum.getRoute().equals("20")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
             }
        }
         //last 12 hours
        rdi.setQueryArgs(new Object[] { formatter.parse("2007-01-30 06:46:53")});
        objects = ptsRegionManager.listRouteSummary(rdi);
        for (int i = 0; i < objects.size(); i++) {
            PtsSummary ptsSum = (PtsSummary) objects.get(i);
            if (ptsSum.getRoute().equals("14")) {
                assertEquals(ptsSum.getTotalItems(), 9, "total items wrong");
             }
            if (ptsSum.getRoute().equals("15")) {
                assertEquals(ptsSum.getTotalItems(), 12, "total items wrong");
            }
            if (ptsSum.getRoute().equals("16")) {
                assertEquals(ptsSum.getTotalItems(), 8, "total items wrong");
             }
            if (ptsSum.getRoute().equals("17")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
            }
            if (ptsSum.getRoute().equals("18")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
            }
            if (ptsSum.getRoute().equals("19")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
            }
            if (ptsSum.getRoute().equals("20")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
             }
        }
        //last 8 hours
        rdi.setQueryArgs(new Object[] { formatter.parse("2007-01-30 15:46:53")});
        objects = ptsRegionManager.listRouteSummary(rdi);
        for (int i = 0; i < objects.size(); i++) {
            PtsSummary ptsSum = (PtsSummary) objects.get(i);
            if (ptsSum.getRoute().equals("14")) {
                assertEquals(ptsSum.getTotalItems(), 9, "total items wrong");
             }
            if (ptsSum.getRoute().equals("15")) {
                assertEquals(ptsSum.getTotalItems(), 12, "total items wrong");
            }
            if (ptsSum.getRoute().equals("16")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
             }
            if (ptsSum.getRoute().equals("17")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
            }
            if (ptsSum.getRoute().equals("18")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
            }
            if (ptsSum.getRoute().equals("19")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
            }
            if (ptsSum.getRoute().equals("20")) {
                assertEquals(ptsSum.getTotalItems(), 0, "total items wrong");
             }
        }
    }
}
