/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Operator;

import java.util.Date;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;


/**
 * Test for PtsPutDetail model object.
 *
 * @author svoruganti
 */
public class PtsPutDetailRootTest {


    /**
     * 
     */
    @BeforeClass()
    protected void setUp() {
    }


    /**
     * 
     */
    @Test()
    public void testPtsPutDetail() {
        PtsPutDetail thePtsPutDetail = null;
        assertNull(thePtsPutDetail, "A PtsPutDetail was created and set to null, but isn't null.");
        thePtsPutDetail = new PtsPutDetail();
        org.testng.Assert
            .assertNotNull(
                thePtsPutDetail,
                "The PtsPutDetail constructor ran but the PtsPutDetail object is still null.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetStatus() {
         PtsPutDetail thePtsPutDetail = new PtsPutDetail();
         thePtsPutDetail.setType(PtsPutDetailType.TaskPut);
        assertEquals(PtsPutDetailType.TaskPut, thePtsPutDetail.getType(), "thePtsPutDetail.getType()"
            + " did not return TASKPUT from newly instantiated object.");
        //Set Status value.
        thePtsPutDetail.setType(PtsPutDetailType.AutoShort);
        assertEquals(PtsPutDetailType.AutoShort, thePtsPutDetail.getType(),
            "thePtsPutDetail.getType() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPtsPut() {
         PtsPutDetail thePtsPutDetail = new PtsPutDetail();

        assertNull(thePtsPutDetail.getPut(), "thePtsPutDetail.getPut()"
            + " did not return null from newly instantiated object.");

        PtsPut ptsPut = new PtsPut();
        thePtsPutDetail.setPut(ptsPut);
        ptsPut.setCreatedDate(new Date());
        ptsPut.setItem(new Item());
        ptsPut.getItem().setNumber("abc123def");

        assertEquals(thePtsPutDetail.getPut(), ptsPut,
            "thePtsPutDetail.getType() did not return the proper value.");
        assertEquals(thePtsPutDetail.getPut().getItem().getNumber(), ptsPut.getItem().getNumber(),
        "thePtsPutDetail.getType() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetOperator() {
        PtsPutDetail thePtsPutDetail = new PtsPutDetail();
        assertNull(thePtsPutDetail.getOperator(), "PtsPutDetail.getOperator()"
            + "did not return Null from newly instantiated object.");
        Operator operator = new Operator();
        operator.setName("testOperator");
        operator.setOperatorIdentifier("123abc");
        thePtsPutDetail.setOperator(operator);
        assertEquals(operator, thePtsPutDetail.getOperator(),
            "PtsPutDetail.getOperator() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPutTime() {
        PtsPutDetail thePtsPutDetail = new PtsPutDetail();
           assertNull(thePtsPutDetail.getPutTime(), "PtsLicense.getStartTime()"
            + " did not return Null from newly instantiated object.");
        Date date = new Date();
        thePtsPutDetail.setPutTime(date);
        assertEquals(date, thePtsPutDetail.getPutTime(),
            "thePtsPutDetail.getPutTime() did not return the proper value.");
    }


    /**
     * 
     */
    @Test()
    public void testGetSetTotalItemQuantity() {
        PtsPutDetail thePtsPutDetail = new PtsPutDetail();
        assertEquals(0, thePtsPutDetail.getQuantityPut().intValue(), "thePtsPutDetail.getQuantityPut()"
            + " did not return default container number of empty string from newly instantiated"
            + " object.");
        //Set Container number value.
        thePtsPutDetail.setQuantityPut(123456789);
        assertEquals(123456789, thePtsPutDetail.getQuantityPut().intValue(),
            "thePtsPutDetail.getQuantityPut() did not return the proper value.");
    }


    /**
     * 
     */
    @Test()
    public void testGetSetContainerNumber() {
        PtsPutDetail thePtsPutDetail = new PtsPutDetail();
        assertNull(thePtsPutDetail.getContainerNumber(), "PtsPutDetail.getContainer()"
            + "did not return Null from newly instantiated object.");
        PtsContainer container = new PtsContainer();
        container.setContainerNumber("123");

        thePtsPutDetail.setContainerNumber(container.getContainerNumber());
        assertEquals(container.getContainerNumber(),
            thePtsPutDetail.getContainerNumber(),
            "PtsPutDetail.getContainer() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetExportStatus() {
        PtsPutDetail thePtsPutDetail = new PtsPutDetail();
         assertEquals(ExportStatus.NotExported, thePtsPutDetail.getExportStatus(), "thePtsPutDetail.getExportStatus()"
           + " did not return NotExported from newly instantiated object.");

         thePtsPutDetail.setExportStatus(ExportStatus.Exported);
       assertEquals(ExportStatus.Exported, thePtsPutDetail.getExportStatus(),
           "thePtsPutDetail.getExportStatus() did not return the proper value.");
   }
}
