/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.selection.model.AssignmentGroup;

import java.util.ArrayList;
import java.util.Date;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

/**
 *
 *
 *
 * @author mlashinsky
 */

public class ArchivePtsLicenseRootTest {

    /**
     * 
     */
    @BeforeClass()
    protected void setUp() {
    }

    /**
     * 
     */
    @Test()
    public void testArchivePtsLicense() {
        ArchivePtsLicense theArchivePtsLicense = null;
        assertNull(theArchivePtsLicense,
            "An ArchivePtsLicense was created and set to null, but isn't null.");
        theArchivePtsLicense = new ArchivePtsLicense();
        org.testng.Assert
            .assertNotNull(
                theArchivePtsLicense,
                "The ArchivePtsLicense constructor ran but the ArchivePtsLicense object is still null.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetStatus() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        assertEquals(PtsLicenseStatus.Available, theArchivePtsLicense
            .getStatus(), "ArchivePtsLicense.getStatus()"
            + " did not return NotPut from newly instantiated object.");
        // Set Status value.
        theArchivePtsLicense.setStatus(PtsLicenseStatus.Complete);
        assertEquals(PtsLicenseStatus.Complete, theArchivePtsLicense
            .getStatus(), "Put.getStatus() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetExpectedResiduals() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        assertNull(
            theArchivePtsLicense.getExpectedResiduals(),
            "ArchivePtsLicense.getExpectedResiduals()"
                + " did not return default container number of empty string from newly instantiated"
                + " object.");
        // Set Container number value.
        theArchivePtsLicense.setExpectedResiduals(123456789);
        assertEquals(123456789, theArchivePtsLicense.getExpectedResiduals()
            .intValue(),
            "ArchivePtsLicense.getQuantityToPut() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetTotalItemQuantity() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        assertEquals(
            0,
            theArchivePtsLicense.getTotalItemQuantity(),
            "ArchivePtsLicense.getExpectedResiduals()"
                + " did not return default container number of empty string from newly instantiated"
                + " object.");
        // Set Container number value.
        theArchivePtsLicense.setTotalItemQuantity(123456789);
        assertEquals(123456789, theArchivePtsLicense.getTotalItemQuantity(),
            "ArchivePtsLicense.getQuantityToPut() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPutDetailCount() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        assertEquals(
            0,
            theArchivePtsLicense.getPutDetailCount(),
            "ArchivePtsLicense.getExpectedResiduals()"
                + " did not return default container number of empty string from newly instantiated"
                + " object.");
        // Set Container number value.
        theArchivePtsLicense.setPutDetailCount(123456789);
        assertEquals(123456789, theArchivePtsLicense.getPutDetailCount(),
            "ArchivePtsLicense.getQuantityToPut() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetStartTime() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        assertNull(theArchivePtsLicense.getStartTime(),
            "ArchivePtsLicense.getStartTime()"
                + " did not return Null from newly instantiated object.");
        Date date = new Date();
        theArchivePtsLicense.setStartTime(date);
        assertEquals(date, theArchivePtsLicense.getStartTime(),
            "ArchivePtsLicense.getStartTime() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetEndTime() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        assertNull(theArchivePtsLicense.getEndTime(),
            "ArchivePtsLicense.getEndTime()"
                + " did not return Null from newly instantiated object.");
        Date date = new Date();
        theArchivePtsLicense.setEndTime(date);
        assertEquals(date, theArchivePtsLicense.getEndTime(),
            "ArchivePtsLicense.getEndTime() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetNumber() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        assertNull(theArchivePtsLicense.getNumber(),
            "ArchivePtsLicense.getNumber()"
                + " did not return Null from newly instantiated object.");
        theArchivePtsLicense.setNumber("abc123");
        assertEquals("abc123", theArchivePtsLicense.getNumber(),
            "ArchivePtsLicense.getNumber() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPartialNumber() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        assertNull(theArchivePtsLicense.getPartialNumber(),
            "ArchivePtsLicense.getPartialNumber()"
                + " did not return Null from newly instantiated object.");
        theArchivePtsLicense.setPartialNumber("abc123");
        assertEquals("abc123", theArchivePtsLicense.getPartialNumber(),
            "ArchivePtsLicense.getPartialNumber() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetReservedBy() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        assertNull(theArchivePtsLicense.getReservedBy(),
            "PtsLicense.getReservedBy()"
                + " did not return Null from newly instantiated object.");
        theArchivePtsLicense.setReservedBy("abc123");
        assertEquals("abc123", theArchivePtsLicense.getReservedBy(),
            "ArchivePtsLicense.getReservedBy() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPuts() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();

        // Create a list of put details
        ArrayList<ArchivePtsPut> putList = new ArrayList<ArchivePtsPut>();
        ArchivePtsPut archivePut = new ArchivePtsPut();
        archivePut.setCreatedDate(new Date());
        archivePut.setItem(new Item());
        archivePut.getItem().setNumber("abc123def");
        putList.add(archivePut);
        assertNotNull(theArchivePtsLicense.getArchivePuts(),
            "ArchivePtsLicense.getPuts()"
                + " returned null instead of an empty array");
        assertTrue(theArchivePtsLicense.getArchivePuts().isEmpty(),
            "ArchivePtsLicense.getPuts()"
                + " did not return null from newly instantiated object.");
        // Set List value.
        theArchivePtsLicense.setArchivePuts(putList);
        assertEquals(putList, theArchivePtsLicense.getArchivePuts(),
            "ArchivePtsLicense.getPuts() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetOperator() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        assertNull(theArchivePtsLicense.getOperatorIdentifier(),
            "ArchivePtsLicense.getOperatorIdentifier()"
                + "did not return Null from newly instantiated object.");
        Operator operator = new Operator();
        operator.setName("testOperator");
        operator.setOperatorIdentifier("123abc");
        theArchivePtsLicense.setOperatorIdentifier(operator.getOperatorIdentifier());
        assertEquals(operator.getOperatorIdentifier(), theArchivePtsLicense.getOperatorIdentifier(),
            "ArchivePtsLicense.getOperatorIdentifier() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetRegion() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        assertNull(theArchivePtsLicense.getRegion(),
            "ArchivePtsLicense.getRegion()"
                + "did not return Null from newly instantiated object.");
        PtsRegion region = new PtsRegion();
        region.setNumber(4321);
        region.setName("testReg");
        theArchivePtsLicense.setRegion(region);
        assertEquals(region, theArchivePtsLicense.getRegion(),
            "ArchivePtsLicense.getRegion() did not return the proper value.");
    }

    /**
     * 
     */
    @Test
    public void testGetSetGroupInfo() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        assertNull(
            theArchivePtsLicense.getGroupInfo(),
            "ArchivePtsLicense.getGroupInfo()"
                + "returned a Null groupInfo from newly instantiated object and "
                + "should not have.");
        AssignmentGroup groupInfo = new AssignmentGroup();
        groupInfo.setGroupNumber(123456L);
        groupInfo.setGroupCount(5);
        theArchivePtsLicense.setGroupInfo(groupInfo);
        assertEquals(groupInfo, theArchivePtsLicense.getGroupInfo(),
            "ArchivePtsLicense.getGroupInfo() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPtsLicenseLabor() {
        ArchivePtsLicense theArchivePtsLicense = new ArchivePtsLicense();
        // Create a list of archivePtsLicenseLabor
        ArrayList<ArchivePtsLicenseLabor> laborList = new ArrayList<ArchivePtsLicenseLabor>();
        ArchivePtsLicenseLabor labor = new ArchivePtsLicenseLabor();
        labor.setCreatedDate(new Date());
        labor.setActualRate(0.1);
        laborList.add(labor);

        assertTrue(theArchivePtsLicense.getArchiveLabor().isEmpty(),
            "ArchivePtsLicense.getLabor()"
                + " did not return null from newly instantiated object.");
        // Set List value.
        theArchivePtsLicense.setArchiveLabor(laborList);
        assertEquals(laborList, theArchivePtsLicense.getArchiveLabor(),
            "ArchivePtsLicense.getLabor() did not return the proper value.");
    }

}
