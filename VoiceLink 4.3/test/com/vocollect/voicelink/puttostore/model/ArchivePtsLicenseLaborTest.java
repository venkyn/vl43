/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.Operator;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;

/**
 *
 *
 * @author mlashinsky
 */
public class ArchivePtsLicenseLaborTest {

    /**
     * Constructor.
     */
    public ArchivePtsLicenseLaborTest() {

    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.model.ArchivePtsLicenseLaborRoot#hashCode()}.
     */
    @Test()
    public void testHashCode() {
        ArchivePtsLicenseLabor archivePtsLicenseLabor1 = new ArchivePtsLicenseLabor();
        Operator oper1 = new Operator();
        archivePtsLicenseLabor1.setOperatorIdentifier(oper1.getOperatorIdentifier());
        assertNotNull("ptsLicenseLabor.hashCode() returned null", archivePtsLicenseLabor1.hashCode());

    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.model.ArchivePtsLicenseLaborRoot#equals(java.lang.Object)}.
     */
    @Test
    public void testEqualsObject() {
        ArchivePtsLicenseLabor archivePtsLicenseLabor1 = new ArchivePtsLicenseLabor();
        ArchivePtsLicenseLabor archivePtsLicenseLabor2 = new ArchivePtsLicenseLabor();

        assertEquals(archivePtsLicenseLabor1, archivePtsLicenseLabor2);
        archivePtsLicenseLabor2.setOperatorIdentifier(" ");
        Operator op = new Operator();
        archivePtsLicenseLabor1.setOperatorIdentifier(op.getOperatorIdentifier());

        assertFalse(archivePtsLicenseLabor1.equals(archivePtsLicenseLabor2));
    }

}
