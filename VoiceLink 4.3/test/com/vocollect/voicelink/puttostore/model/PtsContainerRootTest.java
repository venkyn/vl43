/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */package com.vocollect.voicelink.puttostore.model;


import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationDescription;

import java.util.ArrayList;
import java.util.Date;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 *
 *
 *
 * @author jauberg
 */

public class PtsContainerRootTest {

    /**
     * 
     */
    @BeforeClass()
    protected void setUp() {
    }


    /**
     * 
     */
    @Test()
    public void testPtsContainer() {
        PtsContainer theContainer = null;
        assertNull(theContainer, "A PtsContainer was created and set to null, but isn't null.");
        theContainer = new PtsContainer();
        org.testng.Assert
            .assertNotNull(
                theContainer,
                "The PtsContainer constructor ran but the PtsContainer object is still null.");
    }


    /**
     * 
     */
    @Test()
    public void testGetSetDescriptiveText() {
        PtsContainer theContainer = new PtsContainer();
        assertEquals("", theContainer.getDescriptiveText(), "PtsContainer.getDescriptiveText()"
            + " did not return default descriptive text of empty string from newly instantiated"
            + " PtsContainer.");
        //Set Container number value.
        theContainer.setContainerNumber("ABC123");
        assertEquals("ABC123", theContainer.getDescriptiveText(),
            "PtsContainer.getDescriptiveText() did not return the proper value.");
    }


    /**
     * 
     */
    @Test()
    public void testGetSetContainerNumber() {
        PtsContainer theContainer = new PtsContainer();
        assertEquals("", theContainer.getContainerNumber(), "PtsContainer.getContainerNumber()"
            + " did not return default container number of empty string from newly instantiated"
            + " object.");
        //Set Container number value.
        theContainer.setContainerNumber("12345678901234567890ABC");
        assertEquals("12345678901234567890ABC", theContainer.getContainerNumber(),
            "PtsContainer.getContainerNumber() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetCustomer() {
        PtsContainer theContainer = new PtsContainer();
        //Create a location loc
        Location loc = new Location();
        PtsCustomerLocation custLocation = new PtsCustomerLocation();
        LocationDescription locdesc = new LocationDescription();
        locdesc.setAisle("A1");
        locdesc.setSlot("12");
        locdesc.setPostAisle("post");
        locdesc.setPreAisle("pre");
        loc.setDescription(locdesc);
        loc.setScannedVerification("*pre-A1-12-post*");
        //Set customerLocation values.
        Customer cust = new Customer();
        cust = custLocation.getCustomerInfo();
        cust.setCustomerAddress("123 Main Street");
        cust.setCustomerNumber("1234");
        cust.setCustomerName("Bob");
        custLocation.setLocation(loc);
        custLocation.setDeliveryLocation("Door 12");
        custLocation.setRoute("123");
        assertNull(theContainer.getCustomer(), "PtsContainer.getCustomer()"
            + " did not return null from newly instantiated object.");
        //Set CustomerLocationr value.
        theContainer.setCustomer(custLocation);
        assertEquals(custLocation, theContainer.getCustomer(),
            "PtsContainer.getCustomer() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetExportStatus() {
        PtsContainer theContainer = new PtsContainer();
        assertEquals(ExportStatus.NotExported, theContainer.getExportStatus(), "PtsContainer.getExportStatus()"
            + " did not return NotExported from newly instantiated object.");
        //Set ExportStatus value.
        theContainer.setExportStatus(ExportStatus.InProgress);
        assertEquals(ExportStatus.InProgress, theContainer.getExportStatus(),
            "PtsContainer.getExportStatus() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetStatus() {
        PtsContainer theContainer = new PtsContainer();
        assertEquals(PtsContainerStatus.Open, theContainer.getStatus(), "PtsContainer.getStatus()"
            + " did not return Open from newly instantiated object.");
        //Set Status value.
        theContainer.setStatus(PtsContainerStatus.Closed);
        assertEquals(PtsContainerStatus.Closed, theContainer.getStatus(),
            "PtsContainer.getStatus() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetContainerDetails() {
        PtsContainer theContainer = new PtsContainer();

        //Create a list of put details
        ArrayList <PtsContainerDetail> detailList = new ArrayList<PtsContainerDetail>();
        PtsContainerDetail detail = new PtsContainerDetail();
        detail.setPutTime(new Date());
        detail.setType(PtsPutDetailType.TaskPut);
        detailList.add(detail);

        assertEquals(0, theContainer.getContainerDetails().size(),
            "PtsContainer.getContainerDetails() did not return empty set from newly instantiated object.");
        //Set List value.
        theContainer.setContainerDetails(detailList);
        assertEquals(detailList, theContainer.getContainerDetails(),
            "PtsContainer.getContainerDetails() did not return the proper value.");


    }

}
