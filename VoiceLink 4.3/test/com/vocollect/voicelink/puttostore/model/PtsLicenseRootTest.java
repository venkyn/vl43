/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.selection.model.AssignmentGroup;

import java.util.ArrayList;
import java.util.Date;
import java.util.TreeMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 *
 *
 *
 * @author mnichols
 */

public class PtsLicenseRootTest {

    /**
     * 
     */
    @BeforeClass()
    protected void setUp() {
    }


    /**
     * 
     */
    @Test()
    public void testPtsLicense() {
        PtsLicense thePtsLicense = null;
        assertNull(thePtsLicense, "A PtsLicense was created and set to null, but isn't null.");
        thePtsLicense = new PtsLicense();
        org.testng.Assert
            .assertNotNull(
                thePtsLicense,
                "The PtsLicense constructor ran but the PtsLicense object is still null.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetStatus() {
        PtsLicense thePtsLicense = new PtsLicense();
        assertEquals(PtsLicenseStatus.Available, thePtsLicense.getStatus(), "PtsLicense.getStatus()"
            + " did not return NotPut from newly instantiated object.");
        //Set Status value.
        thePtsLicense.setStatus(PtsLicenseStatus.Complete);
        assertEquals(PtsLicenseStatus.Complete, thePtsLicense.getStatus(),
            "Put.getStatus() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetExpectedResiduals() {
        PtsLicense thePtsLicense = new PtsLicense();
        assertNull(thePtsLicense.getExpectedResiduals(), "PtsLicense.getExpectedResiduals()"
            + " did not return default container number of empty string from newly instantiated"
            + " object.");
        //Set Container number value.
        thePtsLicense.setExpectedResiduals(123456789);
        assertEquals(123456789, thePtsLicense.getExpectedResiduals().intValue(),
            "PtsLicense.getQuantityToPut() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetTotalItemQuantity() {
        PtsLicense thePtsLicense = new PtsLicense();
        assertEquals(0, thePtsLicense.getTotalItemQuantity(), "PtsLicense.getExpectedResiduals()"
            + " did not return default container number of empty string from newly instantiated"
            + " object.");
        //Set Container number value.
        thePtsLicense.setTotalItemQuantity(123456789);
        assertEquals(123456789, thePtsLicense.getTotalItemQuantity(),
            "PtsLicense.getQuantityToPut() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPutDetailCount() {
        PtsLicense thePtsLicense = new PtsLicense();
        assertEquals(0, thePtsLicense.getPutDetailCount(), "PtsLicense.getExpectedResiduals()"
            + " did not return default container number of empty string from newly instantiated"
            + " object.");
        //Set Container number value.
        thePtsLicense.setPutDetailCount(123456789);
        assertEquals(123456789, thePtsLicense.getPutDetailCount(),
            "PtsLicense.getQuantityToPut() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetStartTime() {
        PtsLicense thePtsLicense = new PtsLicense();
        assertNull(thePtsLicense.getStartTime(), "PtsLicense.getStartTime()"
            + " did not return Null from newly instantiated object.");
        Date date = new Date();
        thePtsLicense.setStartTime(date);
        assertEquals(date, thePtsLicense.getStartTime(),
            "PtsLicense.getStartTime() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetEndTime() {
        PtsLicense thePtsLicense = new PtsLicense();
        assertNull(thePtsLicense.getEndTime(), "PtsLicense.getEndTime()"
            + " did not return Null from newly instantiated object.");
        Date date = new Date();
        thePtsLicense.setEndTime(date);
        assertEquals(date, thePtsLicense.getEndTime(),
            "PtsLicense.getEndTime() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetNumber() {
        PtsLicense thePtsLicense = new PtsLicense();
        assertNull(thePtsLicense.getNumber(), "PtsLicense.getNumber()"
            + " did not return Null from newly instantiated object.");
        thePtsLicense.setNumber("abc123");
        assertEquals("abc123", thePtsLicense.getNumber(),
            "PtsLicense.getNumber() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPartialNumber() {
        PtsLicense thePtsLicense = new PtsLicense();
        assertNull(thePtsLicense.getPartialNumber(), "PtsLicense.getPartialNumber()"
            + " did not return Null from newly instantiated object.");
        thePtsLicense.setPartialNumber("abc123");
        assertEquals("abc123", thePtsLicense.getPartialNumber(),
            "PtsLicense.getPartialNumber() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetReservedBy() {
        PtsLicense thePtsLicense = new PtsLicense();
        assertNull(thePtsLicense.getReservedBy(), "PtsLicense.getReservedBy()"
            + " did not return Null from newly instantiated object.");
        thePtsLicense.setReservedBy("abc123");
        assertEquals("abc123", thePtsLicense.getReservedBy(),
            "PtsLicense.getReservedBy() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPuts() {
        PtsLicense thePtsLicense = new PtsLicense();

        //Create a list of put details
        ArrayList <PtsPut> putList = new ArrayList<PtsPut>();
        PtsPut put = new PtsPut();
        put.setCreatedDate(new Date());
        put.setItem(new Item());
        put.getItem().setNumber("abc123def");
        putList.add(put);
        assertNotNull(thePtsLicense.getPuts(), "PtsLicense.getPuts()"
            + " returned null instead of an empty array");
        assertTrue(thePtsLicense.getPuts().isEmpty(), "PtsLicense.getPuts()"
            + " did not return null from newly instantiated object.");
        //Set List value.
        thePtsLicense.setPuts(putList);
        assertEquals(putList, thePtsLicense.getPuts(),
            "PtsLicense.getPuts() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetOperator() {
        PtsLicense thePtsLicense = new PtsLicense();
        assertNull(thePtsLicense.getOperator(), "PtsLicense.getOperator()"
            + "did not return Null from newly instantiated object.");
        Operator operator = new Operator();
        operator.setName("testOperator");
        operator.setOperatorIdentifier("123abc");
        thePtsLicense.setOperator(operator);
        assertEquals(operator, thePtsLicense.getOperator(),
            "PtsLicense.getOperator() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetRegion() {
        PtsLicense thePtsLicense = new PtsLicense();
        assertNull(thePtsLicense.getRegion(), "PtsLicense.getRegion()"
            + "did not return Null from newly instantiated object.");
        PtsRegion region = new PtsRegion();
        region.setNumber(4321);
        region.setName("testReg");
        thePtsLicense.setRegion(region);
        assertEquals(region, thePtsLicense.getRegion(),
            "PtsLicense.getRegion() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetGroupInfo() {
        PtsLicense thePtsLicense = new PtsLicense();
        assertNotNull(thePtsLicense.getGroupInfo(), "PtsLicense.getGroupInfo()"
            + "returned a Null groupInfo from newly instantiated object and "
            + "should not have.");
        AssignmentGroup groupInfo = new AssignmentGroup();
        groupInfo.setGroupNumber(123456L);
        groupInfo.setGroupCount(5);
        thePtsLicense.setGroupInfo(groupInfo);
        assertEquals(groupInfo, thePtsLicense.getGroupInfo(),
            "PtsLicense.getGroupInfo() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPtsLicenseLabor() {
        PtsLicense thePtsLicense = new PtsLicense();
        //Create a list of ptsLicenseLabor
        ArrayList <PtsLicenseLabor> laborList = new ArrayList<PtsLicenseLabor>();
        PtsLicenseLabor labor = new PtsLicenseLabor();
        labor.setCreatedDate(new Date());
        labor.setActualRate(0.1);
        laborList.add(labor);

        assertTrue(thePtsLicense.getLabor().isEmpty(), "PtsLicense.getLabor()"
            + " did not return null from newly instantiated object.");
        //Set List value.
        thePtsLicense.setLabor(laborList);
        assertEquals(laborList, thePtsLicense.getLabor(),
            "PtsLicense.getLabor() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testIsEditable() {
        PtsLicense thePtsLicense = new PtsLicense();
        thePtsLicense.setStatus(PtsLicenseStatus.Available);
        assertFalse(thePtsLicense.isEditable());
        thePtsLicense.setStatus(PtsLicenseStatus.Complete);
        assertFalse(thePtsLicense.isEditable());
        thePtsLicense.setStatus(PtsLicenseStatus.Short);
        assertFalse(thePtsLicense.isEditable());
        thePtsLicense.setStatus(PtsLicenseStatus.InProgress);
        assertTrue(thePtsLicense.isEditable());
        thePtsLicense.setStatus(PtsLicenseStatus.Passed);
        assertTrue(thePtsLicense.isEditable());
        thePtsLicense.setStatus(PtsLicenseStatus.Suspended);
        assertTrue(thePtsLicense.isEditable());
    }

    /**
     * 
     */
    @Test()
    public void testGetGUITransitionStatuses() {
        PtsLicense thePtsLicense = new PtsLicense();
        thePtsLicense.setStatus(PtsLicenseStatus.Available);
        TreeMap<Integer, String> statuses = new TreeMap<Integer, String>(thePtsLicense.getGUITransitionStatuses());
        assertTrue(statuses.size() == 1);

        thePtsLicense.setStatus(PtsLicenseStatus.Complete);
        statuses = new TreeMap<Integer, String>(thePtsLicense.getGUITransitionStatuses());
        assertTrue(statuses.size() == 1);

        thePtsLicense.setStatus(PtsLicenseStatus.Short);
        statuses = new TreeMap<Integer, String>(thePtsLicense.getGUITransitionStatuses());
        assertTrue(statuses.size() == 1);

        thePtsLicense.setStatus(PtsLicenseStatus.InProgress);
        statuses = new TreeMap<Integer, String>(thePtsLicense.getGUITransitionStatuses());
        assertTrue(statuses.size() == 2);
        assertTrue(statuses.containsValue(ResourceUtil.getLocalizedEnumName(PtsLicenseStatus.InProgress)));
        assertTrue(statuses.containsValue(ResourceUtil.getLocalizedEnumName(PtsLicenseStatus.Suspended)));

        thePtsLicense.setStatus(PtsLicenseStatus.Passed);
        statuses = new TreeMap<Integer, String>(thePtsLicense.getGUITransitionStatuses());
        assertTrue(statuses.size() == 2);
        assertTrue(statuses.containsValue(ResourceUtil.getLocalizedEnumName(PtsLicenseStatus.Passed)));
        assertTrue(statuses.containsValue(ResourceUtil.getLocalizedEnumName(PtsLicenseStatus.Complete)));

        thePtsLicense.setStatus(PtsLicenseStatus.Suspended);
        statuses = new TreeMap<Integer, String>(thePtsLicense.getGUITransitionStatuses());
        assertTrue(statuses.size() == 2);
        assertTrue(statuses.containsValue(ResourceUtil.getLocalizedEnumName(PtsLicenseStatus.Complete)));
        assertTrue(statuses.containsValue(ResourceUtil.getLocalizedEnumName(PtsLicenseStatus.Suspended)));
    }
}
