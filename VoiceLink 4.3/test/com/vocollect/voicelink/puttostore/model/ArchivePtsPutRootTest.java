/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationDescription;

import java.util.ArrayList;
import java.util.Date;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 *
 *
 *
 * @author mlashinsky
 */

public class ArchivePtsPutRootTest {

    /**
     * 
     */
    @BeforeClass()
    protected void setUp() {
    }

    /**
     * 
     */
    @Test()
    public void testArchivePut() {
        ArchivePtsPut theArchivePut = null;
        assertNull(theArchivePut,
            "An Archive Put was created and set to null, but isn't null.");
        theArchivePut = new ArchivePtsPut();
        org.testng.Assert
            .assertNotNull(theArchivePut,
                "The Archive Put constructor ran but the Put object is still null.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetDescriptiveText() {
        ArchivePtsPut theArchivePut = new ArchivePtsPut();
        theArchivePut.setItem(new Item());
        assertNull(
            theArchivePut.getDescriptiveText(),
            "ArchivePtsPut.getDescriptiveText()"
                + " did not return default descriptive text of empty string from newly instantiated"
                + " Put.");
        // Set Container number value.
        theArchivePut.getItem().setNumber("ABC123");
        assertEquals("ABC123", theArchivePut.getDescriptiveText(),
            "ArchivePtsPut.getDescriptiveText() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetQuantityToPut() {
        ArchivePtsPut theArchivePut = new ArchivePtsPut();
        assertEquals(new Integer(0), theArchivePut.getQuantityToPut(),
            " default value for quantity to put should be zero.");
        // Set Container number value.
        theArchivePut.setQuantityToPut(123456789);
        assertEquals(123456789, theArchivePut.getQuantityToPut().intValue(),
            "ArchivePtsPut.getQuantityToPut() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetCustomer() {
        ArchivePtsPut theArchivePut = new ArchivePtsPut();
        // Create a location loc
        Location loc = new Location();
        PtsCustomerLocation custLocation = new PtsCustomerLocation();
        LocationDescription locdesc = new LocationDescription();
        locdesc.setAisle("A1");
        locdesc.setSlot("12");
        locdesc.setPostAisle("post");
        locdesc.setPreAisle("pre");
        loc.setDescription(locdesc);
        loc.setScannedVerification("*pre-A1-12-post*");
        // Set customerLocation values.
        Customer cust = new Customer();
        cust = custLocation.getCustomerInfo();
        cust.setCustomerAddress("123 Main Street");
        cust.setCustomerNumber("1234");
        cust.setCustomerName("Bob");
        custLocation.setLocation(loc);
        custLocation.setDeliveryLocation("Door 12");
        custLocation.setRoute("123");
        assertNull(theArchivePut.getCustomerNumber(), "ArchivePtsPut.getCustomerNumber()"
            + " did not return null from newly instantiated object.");
        // Set CustomerLocationr value.
        theArchivePut.setCustomerNumber(cust.getCustomerNumber());
        assertEquals(cust.getCustomerNumber(), theArchivePut.getCustomerNumber(),
            "ArchivePtsPut.getCustomer() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetQuantityPut() {
        ArchivePtsPut theArchivePut = new ArchivePtsPut();
        assertEquals(new Integer(0), theArchivePut.getQuantityPut(),
            "ArchivePtsPut.getQuantityPut()"
                + " did not return Null from newly instantiated object.");
        theArchivePut.setQuantityPut(987654321);
        assertEquals(987654321, theArchivePut.getQuantityPut().intValue(),
            "ArchivePtsPut.getQuantityPut() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPutTime() {
        ArchivePtsPut theArchivePut = new ArchivePtsPut();
        assertNull(theArchivePut.getPutTime(), "ArchivePtsPut.getPutTime()"
            + " did not return Null from newly instantiated object.");
        Date date = new Date();
        theArchivePut.setPutTime(date);
        assertEquals(date, theArchivePut.getPutTime(),
            "ArchivePtsPut.getPutTime() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetItem() {
        ArchivePtsPut theArchivePut = new ArchivePtsPut();
        assertNull(theArchivePut.getItem(), "ArchivePtsPut.getItem()"
            + " did not return Null from newly instantiated object.");
        Item item = new Item();
        item.setNumber("12345abcdef");
        item.setDescription("put to store test item");
        theArchivePut.setItem(item);
        assertEquals(item, theArchivePut.getItem(),
            "ArchivePtsPut.getItem() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetLicense() {
        ArchivePtsPut theArchivePut = new ArchivePtsPut();
        assertNull(theArchivePut.getLicense(), "ArchivePtsPut.getLicense()"
            + "did not return Null from newly instantiated object.");
        ArchivePtsLicense archiveLicense = new ArchivePtsLicense();
        archiveLicense.setNumber("abcd1234");
        archiveLicense.setCreatedDate(new Date());
        theArchivePut.setArchiveLicense(archiveLicense);
        assertEquals(archiveLicense, theArchivePut.getLicense(),
            "ArchivePtsPut.getLicense() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetStatus() {
        ArchivePtsPut theArchivePut = new ArchivePtsPut();
        assertEquals(PtsPutStatus.NotPut, theArchivePut.getStatus(),
            "ArchivePtsPut.getStatus()"
                + " did not return NotPut from newly instantiated object.");
        // Set Status value.
        theArchivePut.setStatus(PtsPutStatus.Put);
        assertEquals(PtsPutStatus.Put, theArchivePut.getStatus(),
            "ArchivePtsPut.getStatus() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPutDetails() {
        ArchivePtsPut theArchivePut = new ArchivePtsPut();

        // Create a list of put details
        ArrayList<ArchivePtsPutDetail> archivePutDetailList = new ArrayList<ArchivePtsPutDetail>();
        ArchivePtsPutDetail archivePutDetail = new ArchivePtsPutDetail();
        archivePutDetail.setPutTime(new Date());
        archivePutDetail.setType(PtsPutDetailType.TaskPut);
        archivePutDetailList.add(archivePutDetail);

        assertNotNull(theArchivePut.getArchivePutDetails(),
            "ArchivePtsPut.getPutDetails()"
                + " did not return null from newly instantiated object.");
        // Set List value.
        theArchivePut.setArchivePutDetails(archivePutDetailList);
        assertEquals(archivePutDetailList,
            theArchivePut.getArchivePutDetails(),
            "ArchivePtsPut.getPutDetails() did not return the proper value.");
    }

}
