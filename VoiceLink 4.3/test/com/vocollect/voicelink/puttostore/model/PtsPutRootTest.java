/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */package com.vocollect.voicelink.puttostore.model;


import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationDescription;

import java.util.ArrayList;
import java.util.Date;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 *
 *
 *
 * @author mnichols
 */

public class PtsPutRootTest {

    /**
     * 
     */
    @BeforeClass()
    protected void setUp() {
    }


    /**
     * 
     */
    @Test()
    public void testPut() {
        PtsPut thePut = null;
        assertNull(thePut, "A Put was created and set to null, but isn't null.");
        thePut = new PtsPut();
        org.testng.Assert
            .assertNotNull(
                thePut,
                "The Put constructor ran but the Put object is still null.");
    }


    /**
     * 
     */
    @Test()
    public void testGetSetDescriptiveText() {
        PtsPut thePut = new PtsPut();
        thePut.setItem(new Item());
        assertNull(thePut.getDescriptiveText(), "PtsPut.getDescriptiveText()"
            + " did not return default descriptive text of empty string from newly instantiated"
            + " Put.");
        //Set Container number value.
        thePut.getItem().setNumber("ABC123");
        assertEquals("ABC123", thePut.getDescriptiveText(),
            "PtsPut.getDescriptiveText() did not return the proper value.");
    }


    /**
     * 
     */
    @Test()
    public void testGetSetQuantityToPut() {
        PtsPut thePut = new PtsPut();
        assertEquals(new Integer(0), thePut.getQuantityToPut(),
                " default value for quantity to put should be zero.");
        //Set Container number value.
        thePut.setQuantityToPut(123456789);
        assertEquals(123456789, thePut.getQuantityToPut().intValue(),
            "PtsPut.getQuantityToPut() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetCustomer() {
        PtsPut thePut = new PtsPut();
        //Create a location loc
        Location loc = new Location();
        PtsCustomerLocation custLocation = new PtsCustomerLocation();
        LocationDescription locdesc = new LocationDescription();
        locdesc.setAisle("A1");
        locdesc.setSlot("12");
        locdesc.setPostAisle("post");
        locdesc.setPreAisle("pre");
        loc.setDescription(locdesc);
        loc.setScannedVerification("*pre-A1-12-post*");
        //Set customerLocation values.
        Customer cust = new Customer();
        cust = custLocation.getCustomerInfo();
        cust.setCustomerAddress("123 Main Street");
        cust.setCustomerNumber("1234");
        cust.setCustomerName("Bob");
        custLocation.setLocation(loc);
        custLocation.setDeliveryLocation("Door 12");
        custLocation.setRoute("123");
        assertNull(thePut.getCustomer(), "PtsPut.getCustomer()"
            + " did not return null from newly instantiated object.");
        //Set CustomerLocationr value.
        thePut.setCustomer(custLocation);
        assertEquals(custLocation, thePut.getCustomer(),
            "PtsPut.getCustomer() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetQuantityPut() {
        PtsPut thePut = new PtsPut();
        assertEquals(new Integer(0), thePut.getQuantityPut(),
                "PtsPut.getQuantityPut()"
            + " did not return Null from newly instantiated object.");
        thePut.setQuantityPut(987654321);
        assertEquals(987654321, thePut.getQuantityPut().intValue(),
            "PtsPut.getQuantityPut() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPutTime() {
        PtsPut thePut = new PtsPut();
        assertNull(thePut.getPutTime(), "PtsPut.getPutTime()"
            + " did not return Null from newly instantiated object.");
        Date date = new Date();
        thePut.setPutTime(date);
        assertEquals(date, thePut.getPutTime(),
            "PtsPut.getPutTime() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetItem() {
        PtsPut thePut = new PtsPut();
        assertNull(thePut.getItem(), "PtsPut.getItem()"
            + " did not return Null from newly instantiated object.");
        Item item = new Item();
        item.setNumber("12345abcdef");
        item.setDescription("put to store test item");
        thePut.setItem(item);
        assertEquals(item, thePut.getItem(),
            "PtsPut.getItem() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetLicense() {
        PtsPut thePut = new PtsPut();
        assertNull(thePut.getLicense(), "PtsPut.getLicense()"
            + "did not return Null from newly instantiated object.");
        PtsLicense license = new PtsLicense();
        license.setNumber("abcd1234");
        license.setCreatedDate(new Date());
        thePut.setLicense(license);
        assertEquals(license, thePut.getLicense(),
            "PtsPut.getLicense() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetStatus() {
        PtsPut thePut = new PtsPut();
        assertEquals(PtsPutStatus.NotPut, thePut.getStatus(), "PtsPut.getStatus()"
            + " did not return NotPut from newly instantiated object.");
        //Set Status value.
        thePut.setStatus(PtsPutStatus.Put);
        assertEquals(PtsPutStatus.Put, thePut.getStatus(),
            "PtsPut.getStatus() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPutDetails() {
        PtsPut thePut = new PtsPut();

        //Create a list of put details
        ArrayList <PtsPutDetail> detailList = new ArrayList<PtsPutDetail>();
        PtsPutDetail detail = new PtsPutDetail();
        detail.setPutTime(new Date());
        detail.setType(PtsPutDetailType.TaskPut);
        detailList.add(detail);

        assertNull(thePut.getPutDetails(), "PtsPut.getPutDetails()"
            + " did not return null from newly instantiated object.");
        //Set List value.
        thePut.setPutDetails(detailList);
        assertEquals(detailList, thePut.getPutDetails(),
            "PtsPut.getPutDetails() did not return the proper value.");
    }

}
