/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Operator;

import java.util.Date;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNull;

/**
 * Test for PtsPutDetail model object.
 *
 * @author mlashinsky
 */
public class ArchivePtsPutDetailRootTest {

    /**
     * 
     */
    @BeforeClass()
    protected void setUp() {
    }

    /**
     * 
     */
    @Test()
    public void testArchivePtsPutDetail() {
        ArchivePtsPutDetail theArchivePtsPutDetail = null;
        assertNull(theArchivePtsPutDetail,
            "A ArchivePtsPutDetail was created and set to null, but isn't null.");
        theArchivePtsPutDetail = new ArchivePtsPutDetail();
        org.testng.Assert
            .assertNotNull(
                theArchivePtsPutDetail,
                "The ArchivePtsPutDetail constructor ran but the ArchivePtsPutDetail object is still null.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetStatus() {
        ArchivePtsPutDetail theArchivePtsPutDetail = new ArchivePtsPutDetail();
        theArchivePtsPutDetail.setType(PtsPutDetailType.TaskPut);
        assertEquals(PtsPutDetailType.TaskPut,
            theArchivePtsPutDetail.getType(),
            "theArchivePtsPutDetail.getType()"
                + " did not return TASKPUT from newly instantiated object.");
        // Set Status value.
        theArchivePtsPutDetail.setType(PtsPutDetailType.AutoShort);
        assertEquals(PtsPutDetailType.AutoShort, theArchivePtsPutDetail
            .getType(),
            "theArchivePtsPutDetail.getType() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPtsPut() {
        ArchivePtsPutDetail theArchivePtsPutDetail = new ArchivePtsPutDetail();

        assertNull(theArchivePtsPutDetail.getArchivePut(),
            "ArchivePtsPutDetail.getPut()"
                + " did not return null from newly instantiated object.");

        ArchivePtsPut archivePtsPut = new ArchivePtsPut();
        theArchivePtsPutDetail.setArchivePut(archivePtsPut);
        archivePtsPut.setCreatedDate(new Date());
        archivePtsPut.setItem(new Item());
        archivePtsPut.getItem().setNumber("abc123def");

        assertEquals(theArchivePtsPutDetail.getArchivePut(), archivePtsPut,
            "theArchivePtsPutDetail.getType() did not return the proper value.");
        assertEquals(theArchivePtsPutDetail.getArchivePut().getItem()
            .getNumber(), archivePtsPut.getItem().getNumber(),
            "theArchivePtsPutDetail.getType() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetOperator() {
        ArchivePtsPutDetail theArchivePtsPutDetail = new ArchivePtsPutDetail();
        assertNull(theArchivePtsPutDetail.getOperatorIdentifier(),
            "ArchivePtsPutDetail.getOperatorIdentifier()"
                + "did not return Null from newly instantiated object.");
        Operator operator = new Operator();
        operator.setName("testOperator");
        operator.setOperatorIdentifier("123abc");
        theArchivePtsPutDetail.setOperatorIdentifier(operator.getOperatorIdentifier());
        assertEquals(operator.getOperatorIdentifier(), theArchivePtsPutDetail.getOperatorIdentifier(),
            "ArchivePtsPutDetail.getOperatorIdentifier() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetPutTime() {
        ArchivePtsPutDetail theArchivePtsPutDetail = new ArchivePtsPutDetail();
        assertNull(theArchivePtsPutDetail.getPutTime(),
            "ArchivePtsPutDetail.getStartTime()"
                + " did not return Null from newly instantiated object.");
        Date date = new Date();
        theArchivePtsPutDetail.setPutTime(date);
        assertEquals(date, theArchivePtsPutDetail.getPutTime(),
            "ArchivePtsPutDetail.getPutTime() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetTotalItemQuantity() {
        ArchivePtsPutDetail theArchivePtsPutDetail = new ArchivePtsPutDetail();
        assertEquals(
            0,
            theArchivePtsPutDetail.getQuantityPut().intValue(),
            "ArchivePtsPutDetail.getQuantityPut()"
                + " did not return default container number of empty string from newly instantiated"
                + " object.");
        // Set Container number value.
        theArchivePtsPutDetail.setQuantityPut(123456789);
        assertEquals(123456789, theArchivePtsPutDetail.getQuantityPut()
            .intValue(),
            "theArchivePtsPutDetail.getQuantityPut() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetContainerNumber() {
        ArchivePtsPutDetail theArchivePtsPutDetail = new ArchivePtsPutDetail();
        assertNull(theArchivePtsPutDetail.getContainerNumber(),
            "ArchivePtsPutDetail.getContainer()"
                + "did not return Null from newly instantiated object.");
        ArchivePtsContainer archiveContainer = new ArchivePtsContainer();
        archiveContainer.setContainerNumber("123");

        theArchivePtsPutDetail.setContainerNumber(archiveContainer
            .getContainerNumber());
        assertEquals(archiveContainer.getContainerNumber(),
            theArchivePtsPutDetail.getContainerNumber(),
            "ArchivePtsPutDetail.getContainer() did not return the proper value.");
    }

    /**
     * 
     */
    @Test()
    public void testGetSetExportStatus() {
        ArchivePtsPutDetail theArchivePtsPutDetail = new ArchivePtsPutDetail();
        assertEquals(ExportStatus.NotExported, theArchivePtsPutDetail
            .getExportStatus(), "ArchivePtsPutDetail.getExportStatus()"
            + " did not return NotExported from newly instantiated object.");

        theArchivePtsPutDetail.setExportStatus(ExportStatus.Exported);
        assertEquals(ExportStatus.Exported, theArchivePtsPutDetail
            .getExportStatus(),
            "ArchivePtsPutDetail.getExportStatus() did not return the proper value.");
    }
}
