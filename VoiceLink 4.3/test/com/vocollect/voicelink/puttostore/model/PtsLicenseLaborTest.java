/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.Operator;

import java.util.Date;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;

/**
 *
 *
 * @author treed
 */
public class PtsLicenseLaborTest {

    /**
     * Constructor.
     */
    public PtsLicenseLaborTest() {

    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.model.PtsLicenseLaborRoot#hashCode()}.
     */
    @Test()
    public void testHashCode() {
        PtsLicenseLabor ptsLicenseLabor1 = new PtsLicenseLabor();
        Operator oper1 = new Operator();
        ptsLicenseLabor1.setOperator(oper1);
        assertNotNull("ptsLicenseLabor.hashCode() returned null", ptsLicenseLabor1.hashCode());

    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.model.PtsLicenseLaborRoot#equals(java.lang.Object)}.
     */
    @Test()
    public void testEqualsObject() {
        PtsLicenseLabor ptsLicenseLabor1 = new PtsLicenseLabor();
        PtsLicenseLabor ptsLicenseLabor2 = new PtsLicenseLabor();
        assertEquals(ptsLicenseLabor1, ptsLicenseLabor2);
        ptsLicenseLabor1.setStartTime(new Date(0));
        ptsLicenseLabor2.setStartTime(new Date(1));
        assertFalse(ptsLicenseLabor1.equals(ptsLicenseLabor2));
    }

}
