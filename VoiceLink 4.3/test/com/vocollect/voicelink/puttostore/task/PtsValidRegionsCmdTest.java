/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * Test for ValidPTSRegionsCmd.
 *
 * @author pfunyak
 */

@Test(groups = { FAST, TASK, PUTTOSTORE })
public class PtsValidRegionsCmdTest extends TaskCommandTestCase {

    private PtsValidRegionsCmd cmd;
    private String ptsOperID = "PtsValidRegionsCmdOper";
    private String ptsSerial = "PtsValidRegionsCmdSerial";

    private String selectionOperId = "selectionOper";
    private String selectionSerial = "selectionSerial";

    /**
     * @return a bean for testing.
     */
    private PtsValidRegionsCmd getCmdBean() {
        return (PtsValidRegionsCmd)
            getBean("cmdPrTaskLUTPtsValidRegions");
    }

    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        // load the workgroups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the workgroups for fork apps so we have a realistic system
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");
        // load the operators needed for testing
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        // load the put away and replenishment regions so we can make sure that
        // when other region types are loaded we only get put to store regions
        // when requesting regions.
        classSetupPutAwayRegions();
        classSetupReplenRegions();
    }

    /**
     * @throws Exception
     */
    public void testAllValidRegionTests() throws Exception {

        // Sign on an operator who is authorized for put to store
        // and attempt to get a valid region list when there are
        // no PTS regions defined in the system.
        initialize(this.ptsSerial, this.ptsOperID);
        // Test for no put to store regions in system
        Response response = executeCommand(this.getCmdDateSec(), ptsSerial, ptsOperID);
        // Verify that we get an error (no regions returned)
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);

        assertEquals(record.get(ResponseRecord.ERROR_CODE_FIELD),
                Long.toString(TaskErrorCode.NO_REGIONS_DEFINED_FOR_REQUESTED_FUNCTION.getErrorCode()),
                "ErrorCode");
        assertFalse(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("{"),
                   "error message should not contain curly braces");
        assertEquals(record.get(ResponseRecord.ERROR_MESSAGE_FIELD),
                    "No regions exist for function Put To Store.  Please notify your supervisor.",
                    "incorrect error message");
        // load regions for remaining tests
        classSetupPTSRegions();

        // Sign on an operator who is NOT authorized for put to store.
        initialize(selectionSerial, selectionOperId);
        // We should receive an error.
        response = executeCommand(this.getCmdDateSec(), selectionSerial, selectionOperId);

        // Verify error message
        records = response.getRecords();
        record = records.get(0);
        assertEquals(record.get(ResponseRecord.ERROR_CODE_FIELD),
            Long.toString(TaskErrorCode.NO_REGIONS_DEFINED_FOR_REQUESTED_FUNCTION.getErrorCode()),
            "ErrorCode");
        assertFalse(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("{"),
                    "error message should not contain curly braces");
        assertEquals(record.get(ResponseRecord.ERROR_MESSAGE_FIELD),
                "No regions exist for function Put To Store.  Please notify your supervisor.",
                   "incorrect error message");


        // Sign on an operator who is authorized for put to store.
        initialize(this.ptsSerial, this.ptsOperID);
        //Test for all authorized regions
        response = executeCommand(this.getCmdDateSec(), ptsSerial, ptsOperID);
        // validate command response fields.
        this.validateCommandFields(response);
        // Verify that we get 5 put to store regions returned
        records = response.getRecords();
        assertEquals(records.size(), 5, "recordCount");
        assertEquals(response.getRecords().get(0).get("errorCode"), "0", "ErrorCode");
        assertEquals(response.getRecords().get(0).get("errorMessage"), "", "ErrorMessage");
        // verify that the operator's current function is put to store
        Operator oper = this.cmd.getOperator();
        assertEquals(oper.getCurrentWorkType().getFunctionType(), TaskFunctionType.PutToStore,
                "wrong function type");
        // verify that the operators's assigned region property is null
        assertNull(oper.getAssignedRegion());
        // verify that the operator's current region is null
        assertNull(oper.getCurrentRegion());
        // verify that the operator's list of regions is empty
        assertTrue(oper.getRegions().isEmpty());
    }

    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     * @param serial the serial number
     * @param operator the operator
     * @throws Exception - Exception
     */
    private void initialize(String serial, String operator) throws Exception {
        final Locale locale = Locale.US;

        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
    }


    /**
     * Execute a command.
     *
     * @param cmdDate - date for command
     * @param sn - the serial number
     * @param operId - the operator id
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String sn, String operId) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
