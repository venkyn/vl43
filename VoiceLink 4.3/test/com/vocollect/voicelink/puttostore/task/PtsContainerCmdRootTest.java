/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */


package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsContainerStatus;
import com.vocollect.voicelink.puttostore.model.PtsCustomerLocation;
import com.vocollect.voicelink.puttostore.model.PtsRegion;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for PtsContainerCmdRootTest.
 *
 * @author jtauberg
 */
@Test(groups = { FAST, TASK, PUTTOSTORE })
public class PtsContainerCmdRootTest extends TaskCommandTestCase {

    private PtsContainerCmd cmd;

    private String testOperId = "PtsContainerCmdOper";
    private String testSerial = "PtsContainerCmdSerial";

    
    /**
     * @return a bean for testing.
     */
    private PtsContainerCmd getCmdBean() {
        return (PtsContainerCmd) 
            getBean("cmdPrTaskLUTPtsContainer");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
    
    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        classSetupCore();
        // load the work groups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the operators needed for testing 
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        // load the put to store regions
        classSetupPTSRegions();
        classSetupInsert("VoiceLinkDataUnitTest_PtsData.xml");
    }
   
    
    /**
     *  Tests setter and getter for requestType member.
     */
    @Test()
    public void testSetGetRequestType() {
        PtsContainerCmd cmd1 = new PtsContainerCmd();
        assertEquals(cmd1.getRequestType(), 0, "PtsContainerCmd.getRequestType()"
            + " did not return default request type of zero from newly instantiated"
            + " PtsContainerCmd.");
        //Set Request Type value.
        cmd1.setRequestType(1);
        assertEquals(cmd1.getRequestType(), 1, 
            "PtsContainerCmd.getRequestType() did not return the proper value.");
    }


    /**
     *  Tests setter and getter for locationIdentifier member.
     */
    @Test()
    public void testSetGetLocationIdentifier() {
        PtsContainerCmd cmd1 = new PtsContainerCmd();
        assertNull(cmd1.getCustomerLocationId(), "PtsContainerCmd.getCustomerLocationId()"
            + " did not return default location identifier type of null from newly instantiated"
            + " PtsContainerCmd.");
        //Set Location Identifier value.
        long testValue = -12345;
        cmd1.setCustomerLocationId(testValue);
        assertEquals(cmd1.getCustomerLocationId().longValue(), testValue, 
            "PtsContainerCmd.getLocationIdentifier() did not return the proper value.");
    }

    
    /**
     *  Tests setter and getter for containerNumber member.
     */
    @Test()
    public void testSetGetContainerNumber() {
        PtsContainerCmd cmd1 = new PtsContainerCmd();
        assertEquals(cmd1.getContainerNumber(), "", "PtsContainerCmd.getContainerNumber()"
            + " did not return default container # type of empty string from newly instantiated"
            + " PtsContainerCmd.");
        //Set Container Number value.
        cmd1.setContainerNumber("ABC12345");
        assertEquals(cmd1.getContainerNumber(), "ABC12345", 
            "PtsContainerCmd.getContainerNumber() did not return the proper value.");
    }

   /**
    *  This test ensures that the db does not have a uniqueness constraint
    *  on the container number field.  This is because when creating containers
    *  they are created with a -1 initial value, and then the number is updated.
    */
  @Test(enabled = false)
   public void testEnsureNoDBUniquenessConstraint() throws Exception {
      PtsContainer c1 = new PtsContainer();
      PtsContainer c2 = new PtsContainer();

      //set values and save first container.
      c1.setContainerNumber("-1");
      c1.setCreatedDate(this.getCmdDateSec());
      c1.setVersion(1);
      
      //Hey, if this just failed, you probably set a uniqueness constraint on the
      // container number field of the Pts_Container table...  This would break
      // core functionality.
      this.cmd.getPtsContainerManager().save(c1);
      //set values and save second container.
      c2.setContainerNumber("-1");
      c2.setCreatedDate(this.getCmdDateSec());
      c2.setVersion(1);
      //Hey, if this just failed, you probably set a uniqueness constraint on the
      // container number field of the Pts_Container table...  This would break
      // core functionality.
      this.cmd.getPtsContainerManager().save(c2);
  }

   
    
    
    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteOprDirMultipleContainerTests() throws Exception {
        String regionStr = "44";
        String licenseStr = "00000430";
        
        // Get to the proper state in the task using license 430.
        initialize(this.testSerial, this.testOperId);
        getRegion(this.testSerial, this.testOperId, regionStr);
        getFlowThroughLocation(this.testSerial, this.testOperId, regionStr);
        verifyLicense(this.testSerial, this.testOperId, licenseStr, "0");
        getAssignment(this.testSerial, this.testOperId);
        
        //*******************************************************************
        // CLOSE CONTAINER TESTS - Container number not specified.
        //*******************************************************************
        this.closeContainerNotSpecifiedAndNotFoundAtLocation();
        this.closeContainerNotSpecifiedAndAlreadyClosed();
        this.closeContainerNotSpecifiedAndMultipleOpened();
            // turn off allow multiple open containers  
            this.setAllowMultipleOpenSetting(regionStr, false);
        this.closeContainerNotSpecifiedAndMultipleOpenedNotAllowed();
            // turn on allow multiple open containers
            this.setAllowMultipleOpenSetting(regionStr, true);
        this.closeContainerNotSpecifiedSuccessful();    
        //*******************************************************************
        // CLOSE CONTAINER - Container number is specified.
        //*******************************************************************
        this.closeContainerSpecifiedAndNotFoundAtLocation();
        this.closeContainerSpecifiedAndAlreadyClosed();
        this.closeContainerSpecifiedSuccessful();

        //*******************************************************************
        // OPEN CONTAINER - Container number not specified.
        //*******************************************************************
        // The following tests must be run in the given order since they 
        // are dependent on the results from the previous test.
        this.openContainerNotSpecifiedNoneAtLocation();
            // turn off allow multiple open containers  
            this.setAllowMultipleOpenSetting(regionStr, false);
        this.openContainerNotSpecifiedMultipleNotAllowed();
            // turn on allow multiple open containers
            this.setAllowMultipleOpenSetting(regionStr, true);     
        this.openContainerNotSpecifiedMultipleOpenAllowed();
        // turn off allow multiple open containers  
        this.setAllowMultipleOpenSetting(regionStr, false);
        this.openContainerNotSpecifiedMultipleOpenNotAllowedFatal();
        // turn on allow multiple open containers
        this.setAllowMultipleOpenSetting(regionStr, true);  

        //*******************************************************************
        // OPEN CONTAINER - Container number is specified.
        //*******************************************************************
        this.openContainerSpecifiedAlreadyExists();
        this.openContainerSpecifiedDuplicateSpokenNumber();
        this.openContainerSpecifiedSuccess();
        
    }

    
     
    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @param serial - terminal serial number
     * @param operator - operator for command
     * 
     * @throws Exception
     */
    private void initialize(String serial, String operator) throws Exception {
        final Locale locale = Locale.US;
         
        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
    }
        
    /**
     * Gets the region to sign into.
     * 
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param region - the region to sign into.
     * 
     * @throws Exception
     */
    private void getRegion(String serial, String operator, String region) throws Exception {
         
        executeLutCmd("cmdPrTaskLUTPtsValidRegions",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTPtsGetRegionConfiguration",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, region});        
    }    
    
    
    /**
     * Gets the flow through location.
     * 
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param region - the region to get flow through location.
     * 
     * @throws Exception
     */
    private void getFlowThroughLocation(String serial, String operator, String region) throws Exception {
         
        executeLutCmd("cmdPrTaskLUTPtsGetFTLocation",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, region});
    } 
    
    /**
     * reserve a license.
     * 
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param licenseNumber - the license number to reserve.
     * @param partialFlag - 1 means this is a partial license number
     * 
     * @throws Exception
     */
    private void verifyLicense(String serial, String operator, 
                               String licenseNumber, String partialFlag) 
    throws Exception {
         
        executeLutCmd("cmdPrTaskLUTPtsVerifyLicense",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, 
                                licenseNumber, partialFlag});
    }
    
    /**
     * get the assignment.
     * 
     * @param serial - terminal serial number
     * @param operator - operator for command

     * 
     * @throws Exception
     */
    private void getAssignment(String serial, String operator) throws Exception {
         
        executeLutCmd("cmdPrTaskLUTPtsGetAssignment",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
    }

    /**
     * Execute a command.
     * 
     * @param sn - the serial number
     * @param cmdDate - date for command
     * @param operId - operator ID
     * @param requestType - the request type
     * @param locId - the location ID
     * @param containerNum - the container number
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String sn, String operId,
               String requestType, String locId,
               String containerNum) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId, requestType,
                locId, containerNum}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
    
    
    
    /**
     * @param regionStr - the region string
     * @param value - the value
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    private void setAllowMultipleOpenSetting(String regionStr, boolean value) 
                                             throws DataAccessException, BusinessRuleException {
        PtsRegion region = this.cmd.getPtsRegionManager().findRegionByNumber(Integer.parseInt(regionStr));
        region.setAllowMultipleOpenContainers(value);
        this.cmd.getPtsRegionManager().save(region);
        
    }
    
    
    
    /**
     * @throws Exception
     */
    private void closeContainerNotSpecifiedAndNotFoundAtLocation() throws Exception {
        
        Response response;
        List<ResponseRecord> records;
        /*
         * Try to ("1" close) a container at a location, (custloc -1)
         * that does not have any containers at the location and the
         * container number was not specified.
         * This should throw an exception.    
         */
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "1", "-1", ""); 
        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");
        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "4", "Response returned incorrect ErrorCode");
        assertEquals("No open container at location.", records.get(0).get("errorMessage"), 
            "Response returned incorrect ErrorMessage");
    }
 
    
    
    /**
     * @throws Exception
     */
    private void closeContainerNotSpecifiedAndAlreadyClosed() throws Exception {
        Response response;
        List<ResponseRecord> records;
        /*
         * Try to ("1" close) a container at a location, (custloc -43003)
         * that has closed containers at the location and the container number 
         * was not specified.
         * This should throw an exception.    
         */
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "1", "-43003", ""); 
        
        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "4", "Response returned incorrect ErrorCode");
        assertEquals("No open container at location.", records.get(0).get("errorMessage"),
            "Response returned incorrect ErrorMessage");
    }

    
    /**
     * @throws Exception
     */
    private void closeContainerNotSpecifiedAndMultipleOpened() throws Exception {
        Response response;
        List<ResponseRecord> records;
        /*
         * Try to ("1" close) a container at a location, (custloc -43002)
         * that has multiple open containers at the location and the container number 
         * was not specified.
         * This should throw an exception.    
         */
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "1", "-43002", ""); 
        
        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "3", "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"),  
                "Multiple open containers. Please specify a container.",
                "Response returned incorrect ErrorMessage");
    }    
 
    
    /**
     * @throws Exception
     */
    private void closeContainerNotSpecifiedAndMultipleOpenedNotAllowed() throws Exception {
        Response response;
        List<ResponseRecord> records;
        /*
         * Try to ("1" close) a container at a location, (custloc -43002)
         * that has multiple open containers at the location and multiple open containers
         * are not allowed and container number was not specified.
         * This should throw an exception.    
         */
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "1", "-43002", ""); 
        
        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(1, records.size(), "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "98", "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"),
            "Multiple open containers not allowed.  Please see your supervisor.",
            "Response returned incorrect ErrorMessage");
    }
  
    
    /**
     * @throws Exception
     */
    private void closeContainerNotSpecifiedSuccessful() throws Exception {
        Response response;
        List<ResponseRecord> records;
        ResponseRecord record;
        /*
         * Try to ("1" close) a container at a location, (custloc -44106) that has one open 
         * container at the location and the container number was not specified.
         * This should succeed and container number "44100006" should be closed. 
         */
        String containerNumber = "4410006";
        PtsContainer container = this.cmd.getPtsContainerManager().findPtsContainerByNumber(containerNumber);
        assertEquals(container.getStatus(), PtsContainerStatus.Open, "Invalid status");
        
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "1", "-44106", ""); 
        
        // Verify the response returned 1 record
        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");
        //Test error code & message
        assertEquals(record.get("errorCode"), "0", "Response returned incorrect ErrorCode");
        assertEquals(record.get("errorMessage"), "", "Response returned incorrect ErrorMessage");
        assertEquals(record.get("customerLocationId"), -44106L, "wrong customerLocationId");
        assertEquals(record.get("containerNumber"), "4410006", "wrong container number");
        assertEquals(record.get("spokenContainerNumber"), "006", "wrong spokenContainerNumber ");
        // verify that the container was closed;
        container = this.cmd.getPtsContainerManager().findPtsContainerByNumber(containerNumber);
        assertEquals(container.getStatus(), PtsContainerStatus.Closed, "status should be closed");
    }    

    
    /**
     * @throws Exception
     */
    private void closeContainerSpecifiedAndNotFoundAtLocation() throws Exception {
        Response response;
        List<ResponseRecord> records;
        /*
         * Try to ("1" close) container ABC123 which doesn't exist for custloc -43001
         * This should throw an exception.    
         */
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "1", "-43001", "ABC123"); 
        
        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "5", "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"), 
                "Container ABC123 was not found or is already closed.", 
                "Response returned incorrect ErrorMessage");
    }
 
    
    /**
     * @throws Exception
     */
    private void closeContainerSpecifiedAndAlreadyClosed() throws Exception {
        Response response;
        List<ResponseRecord> records;
        /*
         * Try to ("1" close) container 1000235 which is already closed at custloc -43001
         * This should throw an exception.
         */
         response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "1", "-43001", "1000235"); 
         
         // Verify the response returned 1 record
         records = response.getRecords();
         assertEquals(1, records.size(), "Response returned incorrect recordCount.");

         //Test error code & message
         assertEquals(records.get(0).get("errorCode"), "5", "Response returned incorrect ErrorCode");
         assertEquals(records.get(0).get("errorMessage"), 
                 "Container 1000235 was not found or is already closed.",
                 "Response returned incorrect ErrorMessage");
    }
    
    /**
     * @throws Exception
     */
    private void closeContainerSpecifiedSuccessful() throws Exception {
        Response response;
        List<ResponseRecord> records;
        ResponseRecord record;
        /*
         * Try to ("1" close) a container at a location, (custloc --44105) that has one 
         * open container at the location and the container number is specified.
         * This should succeed.    
         */
        String containerNumber = "4410005";
        PtsContainer container = this.cmd.getPtsContainerManager().findPtsContainerByNumber(containerNumber);
        assertEquals(container.getStatus(), PtsContainerStatus.Open, "Invalid status");
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "1", "-44105", containerNumber); 
        
        // Verify the response returned 1 record
        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");
        //Test error code & message
        assertEquals(record.get("errorCode"), "0", "Response returned incorrect ErrorCode");
        assertEquals(record.get("errorMessage"), "", "Response returned incorrect ErrorMessage");
        assertEquals(record.get("customerLocationId"), -44105L, "wrong customerLocationId");
        assertEquals(record.get("containerNumber"), "4410005", "wrong container number");
        assertEquals(record.get("spokenContainerNumber"), "005", "wrong spokenContainerNumber ");
        // verify that the container was closed;
        container = this.cmd.getPtsContainerManager().findPtsContainerByNumber(containerNumber);
        assertEquals(container.getStatus(), PtsContainerStatus.Closed, "status should be closed");
    } 
    
    /**
     * @throws Exception
     */
    private void openContainerNotSpecifiedNoneAtLocation() throws Exception {
        /*
         * Try to ("2" open new) a container at custLoc -43004 which does not have
         * any containers open.
         * This should succeed.
         * Verify that we get one container back.
         */
        Response response;
        List<ResponseRecord> records;
        ResponseRecord record;

        PtsCustomerLocation custLoc = this.cmd.getPtsCustomerLocationManager().get(-43004L);
        List <PtsContainer> containerList = 
            this.cmd.getPtsContainerManager().listOpenPtsContainersByCustomerLocation(custLoc);
        assertEquals(containerList.size(), 0, "container list should be empty");
        
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "2", "-43004", "");
        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");
        record = records.get(0);
        
        //Test error code & message
        assertEquals(record.get("errorCode"), "0", "Response returned incorrect ErrorCode");
        assertEquals(record.get("errorMessage"), "", "Response returned incorrect ErrorMessage");
        // verify that the customer location id returned is the one expected.
        assertEquals(record.get("customerLocationId"), -43004L, "Wrong Customer Location Id");
        assertNotNull(record.get("containerNumber"), "containerNumber should not be null");
        assertNotNull(record.get("spokenContainerNumber"), "spokenContainerNumber should not be null");
        // verify that we have only 1 container at the location.
        containerList = 
            this.cmd.getPtsContainerManager().listOpenPtsContainersByCustomerLocation(custLoc);
        assertEquals(containerList.size(), 1, "should have found one container.");
    }
  
    
    /**
     * @throws Exception
     */
    private void openContainerNotSpecifiedMultipleNotAllowed() throws Exception {
        /*
         * Try to ("2" open new) a second container at custLoc -43004 which has one
         * open container
         * This should succeed.
         * 
         * The previously open container should be automatically closed because
         * multiple open containers are not allowed.
         *  
         */
        Response response;
        List<ResponseRecord> records;
        ResponseRecord record;

        PtsCustomerLocation custLoc = this.cmd.getPtsCustomerLocationManager().get(-43004L);
        List <PtsContainer> containerList = 
            this.cmd.getPtsContainerManager().listOpenPtsContainersByCustomerLocation(custLoc);
        assertEquals(containerList.size(), 1, "container list should have 1 container");
        assertEquals(containerList.get(0).getStatus(), PtsContainerStatus.Open, "status should be opened.");
        Long firstContainerId = containerList.get(0).getId();
        
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "2", "-43004", "");
        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");
        record = records.get(0);
        
        //Test error code & message
        assertEquals(record.get("errorCode"), "0", "Response returned incorrect ErrorCode");
        assertEquals(record.get("errorMessage"), "", "Response returned incorrect ErrorMessage");
        // verify that the customer location id returned is the one expected.
        assertEquals(record.get("customerLocationId"), -43004L, "Wrong Customer Location ID");
        assertNotNull(record.get("containerNumber"), "containerNumber should not be null");
        assertNotNull(record.get("spokenContainerNumber"), "spokenContainerNumber should not be null");
        
        // verify that we have only 2 containers at the location.
        containerList = 
            this.cmd.getPtsContainerManager().listOpenPtsContainersByCustomerLocation(custLoc);
        assertEquals(containerList.size(), 1, "should have found 1 containers.");
        assertEquals(containerList.get(0).getStatus(), PtsContainerStatus.Open, "status should be opened.");
        PtsContainer container = this.cmd.getPtsContainerManager().get(firstContainerId);
        // verify that the previously opened container is closed.
        assertEquals(container.getStatus(), PtsContainerStatus.Closed, "status should be closed.");
    }
    
    
    /**
     * @throws Exception
     */
    private void openContainerNotSpecifiedMultipleOpenAllowed() throws Exception {
        /*
         * Try to ("2" open new) a second container at custLoc -43004 which has one
         * open container and one closed container.
         * This should succeed.
         * 
         * There should be one open container at the location.
         *  
         */
        Response response;
        List<ResponseRecord> records;
        ResponseRecord record;

        PtsCustomerLocation custLoc = this.cmd.getPtsCustomerLocationManager().get(-43004L);
        List <PtsContainer> containerList = 
            this.cmd.getPtsContainerManager().listOpenPtsContainersByCustomerLocation(custLoc);
        assertEquals(containerList.size(), 1, "container list should have 1 container");
        assertEquals(containerList.get(0).getStatus(), PtsContainerStatus.Open, "status should be opened.");
        
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "2", "-43004", "");
        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");
        record = records.get(0);
        
        //Test error code & message
        assertEquals(record.get("errorCode"), "0", "Response returned incorrect ErrorCode");
        assertEquals(record.get("errorMessage"), "", "Response returned incorrect ErrorMessage");
        
        // verify that we have only 2 containers at the location.
        containerList = 
            this.cmd.getPtsContainerManager().listOpenPtsContainersByCustomerLocation(custLoc);
        assertEquals(containerList.size(), 2, "should have found 2 open containers.");
        assertEquals(containerList.get(0).getStatus(), PtsContainerStatus.Open, "status should be opened.");
        assertEquals(containerList.get(1).getStatus(), PtsContainerStatus.Open, "status should be opened.");
    }
    
    /**
     * @throws Exception
     */
    private void openContainerNotSpecifiedMultipleOpenNotAllowedFatal() throws Exception {
        /*
         * Try to ("2" open new) a third container at custLoc -43004 which has two
         * open container when multiple opne containers are not allowed.
         * In reality this should never happen in the system but we do throw an 
         * exception if it does.
         * 
         * This should throw an exception.
         *  
         */
        Response response;
        List<ResponseRecord> records;
        ResponseRecord record;

        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "2", "-43004", "");
        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");
        record = records.get(0);
        
        //Test error code & message
        assertEquals(record.get("errorCode"), "98", "Response returned incorrect ErrorCode");
        assertEquals(record.get("errorMessage"), "Multiple open containers not allowed.  Please see your supervisor.",
                "Response returned incorrect ErrorMessage");
    }
    
    /**
     * @throws Exception
     */
    private void openContainerSpecifiedAlreadyExists() throws Exception {
        /* Try to ("2" open new) container 1000229 at custLoc -43001 which is
         * already closed at custloc -43003
         * Scenario:  Operator tries to open a container whose container number
         *            already exists.
         *      This should throw an exception.
         */
        Response response;
        List<ResponseRecord> records;
        ResponseRecord record;
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "2", "-43001", "1000229"); 
         
        // Verify the response returned 1 record
        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(record.get("errorCode"), "1", "Response returned incorrect ErrorCode");
        assertEquals(record.get("errorMessage"), 
                "Cannot create container. 1000229 already exists.", 
                "Response returned incorrect ErrorMessage");
    }
        
    /**
     * @throws Exception
     */
    private void openContainerSpecifiedDuplicateSpokenNumber() throws Exception {
        /*
         * Try to ("2" open new) container # ABC233 at custLoc -43001.
         * Note that there is already an open container 1000233 at this location
         * and region 44 makes an operator validate last 3 digits of container #.
         * Scenario:  Operator tries to open a container whose spoken container #
         *            matches a container that's already open at this location.
         *      This should throw an exception.
         */
        Response response;
        List<ResponseRecord> records;
        ResponseRecord record;
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "2",  "-43001", "ABC233");
         
        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");
        record = records.get(0);
        //Test error code & message
        assertEquals(record.get("errorCode"), "2", "Response returned incorrect ErrorCode");
        assertEquals(record.get("errorMessage"), 
            "Location already has an open container whose spoken number is 233.",
            "Response returned incorrect ErrorMessage");
    }
    
    /**
     * @throws Exception
     */
    private void openContainerSpecifiedSuccess() throws Exception {
        /*
         * Try to ("2" open new) container # 1000228 at custLoc -4.
         *      This should succeed.
         */
        Response response;
        List<ResponseRecord> records;
        ResponseRecord record;
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, "2",  "-4", "1000228");
         
        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        record = records.get(0);
        //Test error code & message
        assertEquals(record.get("errorCode"), "0",  "Response returned incorrect ErrorCode");
        assertEquals(record.get("errorMessage"), "", "Response returned incorrect ErrorMessage");
        assertEquals(record.get("customerLocationId"), -4L, "incorrect customerLocationId");
        assertEquals(record.get("containerNumber"), "1000228", "incorrect containerNumber");
        assertEquals(record.get("spokenContainerNumber"), "228",
                                "incorrect spokenContainerNumber");
    }
}
