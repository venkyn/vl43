/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * Test for PtsPrintExceptionLabelCmd.
 *
 * @author pfunyak
 */

@Test(groups = { FAST, TASK, PUTTOSTORE })
public class PtsPrintExceptionLabelCmdTest extends TaskCommandTestCase {

    private PtsPrintExceptionLabelCmd cmd;
    private String ptsOperId = "PtsPrintExceptionLabelOper";
    private String ptsSerial = "PtsPrintExceptionLabelSerial";

    /**
     * @return a bean for testing.
     */
    private PtsPrintExceptionLabelCmd getCmdBean() {
        return (PtsPrintExceptionLabelCmd)
            getBean("cmdPrTaskLUTPtsPrintExceptionLabel");
    }

    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
    }

    /**
     * @throws Exception
     */
    public void testPrintExceptionLabel() throws Exception {

        String printerNumber = "1";
        String licenseNumber = "12345";
        String itemNumber = "98765";

        initialize(this.ptsSerial, this.ptsOperId);
        // Test for no put to store regions in system
        Response response = executeCommand(this.getCmdDateSec(), ptsSerial, ptsOperId,
                                           printerNumber, licenseNumber, itemNumber);
        // Verify that we get an error (no regions returned)
        List<ResponseRecord> records = response.getRecords();
        this.validateCommandFields(response);
        ResponseRecord record = records.get(0);

        assertEquals(records.size(), 1, "should only have a single record.");
        assertEquals(record.getErrorCode(), "0", "error code should be zero");
    }

    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     *
     * @param serial - the serial number
     * @param operator - the operator
     * @throws Exception - Exception
     */
    private void initialize(String serial, String operator) throws Exception {
        final Locale locale = Locale.US;

        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
    }


    /**
     * Execute a command.
     *
     * @param sn - the serial number
     * @param cmdDate - date for command
     * @param operId - the operator ID
     * @param printerNumber - the printer number
     * @param licenseNumber - the license number
     * @param itemNumber - the item number
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String sn, String operId, String printerNumber,
                                   String licenseNumber, String itemNumber) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId, printerNumber,
                                             licenseNumber, itemNumber}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
