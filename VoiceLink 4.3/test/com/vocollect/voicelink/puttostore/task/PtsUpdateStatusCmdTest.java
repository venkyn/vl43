/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.puttostore.model.PtsPut;
import com.vocollect.voicelink.puttostore.model.PtsPutStatus;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for UpdateStatusCmd.
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, SELECTION })
public class PtsUpdateStatusCmdTest extends TaskCommandTestCase {

    private PtsUpdateStatusCmd cmd;
    private String operatorId = "PtsUpdateStatusCmdOper";
    private String serialId = "PtsUpdateStatusCmdSerial";
    
   
    /**
     * @return a bean for testing.
     */
    private PtsUpdateStatusCmd getCmdBean() {
        return (PtsUpdateStatusCmd) getBean("cmdPrTaskLUTPtsUpdateStatus");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
    
    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        classSetupCore();
        // load the work groups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the operators needed for testing 
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        // load the put to store regions
        classSetupPTSRegions();
        classSetupInsert("VoiceLinkDataUnitTest_PtsData.xml");
    }
    

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PtsUpdateStatusCmd#getGroupNumber()}.
     */
    @Test()
    public void testGetSetGroupNumber() {
        this.cmd.setGroupNumber(1L);
        assertEquals(this.cmd.getGroupNumber().toString(), "1", "GroupNumber Getter/Setter");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#getLocationID()}.
     */
    @Test()
    public void testGetSetCustomerLocationID() {
        this.cmd.setCustomerLocationID("12345");
        assertEquals(this.cmd.getCustomerLocationID(), "12345", "LocationID Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#getSlotAisle()}.
     */
    @Test()
    public void testGetSetSlotAisle() {
        this.cmd.setSlotAisle("1");
        assertEquals(this.cmd.getSlotAisle(), "1", "SlotAisle Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#getSlotAisle()}.
     */
    @Test()
    public void testGetSetSetStatusTo() {
        this.cmd.setSetStatusTo("S");
        assertEquals(this.cmd.getSetStatusTo(), "S", "SetStatusTo Getter/Setter");
    }
  
    /**
     * Test Skipping a slot.
     * @throws Exception
     */
    
    @Test()
    public void testExecuteSkipSlot() throws Exception {
        
        // request a license that all puts are in a final status (short, put or canceled)
        // command should throw an exception
        // Get to the proper state in the task.
        initialize(this.serialId, this.operatorId);
        
        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000250", "0");
        getAssignment(this.serialId, this.operatorId);
      
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        getPut(this.serialId, this.operatorId, groupNumber.toString());
        
        Response response = executeCommand(this.getCmdDateSec(), 
            groupNumber.toString(), "-9", "0", "S");
        
        this.validateCommandFields(response);
         //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        
        //Verify put status
        assertEquals(countPutsWithStatus(PtsPutStatus.Skipped, groupNumber),  1, "putsUpdate");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteValidations() throws Exception {

        initialize(this.serialId, this.operatorId);
        
        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000252", "0");
        getAssignment(this.serialId, this.operatorId);
      
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        getPut(this.serialId, this.operatorId, groupNumber.toString());

        //Test Update Status Invalid Status
        //Skip Aisle - assumes this work (in previous test)
        Response response = executeCommand(this.getCmdDateSec(), 
                groupNumber.toString(), "-9", "1", "X");
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.INVALID_STATUS_FOR_PUT.getErrorCode(), "InvalidStatus");
        
        //Test Update Status Invalid indicator
        //Skip Aisle - assumes this work (in previous test)
         response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), "-9", "3", "S");
         //Verify Successful result
        records = response.getRecords();
        assertEquals(1, records.size(), "recordSize");
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.INVALID_INDICATOR_PUT.getErrorCode(), "InvalidStatus");
 
        //Test Update Status cannot skip entire assignment
        //Skip Aisle - assumes this work (in previous test)
         response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), "-9", "2", "S");
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                TaskErrorCode.CANNOT_SKIP_ENTIRE_ASSIGNMENT_PTS.getErrorCode(), "InvalidStatus");
        
        //Test Update Status Invalid Status
        //Skip Aisle - assumes this work (in previous test)
         response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), "-9", "0", "N");
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 0, "InvalidStatus");
      
        //Test Update Status Invalid indicator
        //Skip Aisle - assumes this work (in previous test)
         response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), "-9", "1", "N");
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 0, "InvalidStatus");
    }
    
    
    /**
     *  * Test Skipping a aisle.
     * 
     * @throws Exception
     */
    @Test()
    public void testExecuteSkipAisle() throws Exception {
        
        // request a license that all puts are in a final status (short, put or canceled)
        // command should throw an exception
        // Get to the proper state in the task.
        initialize(this.serialId, this.operatorId);
        
        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000251", "0");
        getAssignment(this.serialId, this.operatorId);
      
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        getPut(this.serialId, this.operatorId, groupNumber.toString());
        
        Response response = executeCommand(this.getCmdDateSec(), 
            groupNumber.toString(), "-25101", "1", "S");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        
        //Verify puts status
        assertEquals(countPutsWithStatus(PtsPutStatus.Skipped, groupNumber), 2, "putsUpdate");
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteValidationGroupLocation() throws Exception {
      
        Response response = executeCommand(this.getCmdDateSec(), 
            "0001", "-25101", "1", "S");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        
        //Verify puts status
        assertEquals(countPutsWithStatus(PtsPutStatus.Skipped, new Long(0001)), 2, "putsUpdate");
    }
    

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.task.PtsUpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteNotPut() throws Exception {

        // request a license that all puts are in a final status (short, put or canceled)
        // command should throw an exception
        // Get to the proper state in the task.
        initialize(this.serialId, this.operatorId);
        
        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000253", "0");
        getAssignment(this.serialId, this.operatorId);
      
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        getPut(this.serialId, this.operatorId, groupNumber.toString());

        //Test Update Status Not put
        //Skip Aisle - assumes this work (in previous test)
        Response response = executeCommand(this.getCmdDateSec(), 
            groupNumber.toString(), "-25301", "1", "S");
        
        //Verify put status
        assertEquals(countPutsWithStatus(null, groupNumber), 
            countPutsWithStatus(PtsPutStatus.Skipped, groupNumber), "putsUpdate");
        
        //Un skip items setting back to not put
        response = executeCommand(this.getCmdDateSec(), 
            groupNumber.toString(), "", "2", "N");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        
        //Verify put status
        assertEquals(countPutsWithStatus(null, groupNumber), 
            countPutsWithStatus(PtsPutStatus.NotPut, groupNumber), "putsUpdate");
    }

      
    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @param serial - terminal serial number
     * @param operator - operator for command
     * 
     * @throws Exception
     */
    private void initialize(String serial, String operator) throws Exception {
        final Locale locale = Locale.US;
         
        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
    }
        
    /**
     * Gets the region to sign into.
     * 
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param region - the region to sign into.
     * 
     * @throws Exception
     */
    private void getRegion(String serial, String operator, String region) throws Exception {
         
        executeLutCmd("cmdPrTaskLUTPtsValidRegions",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTPtsGetRegionConfiguration",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, region});        
    }    
    
    
    /**
     * Gets the flow through location.
     * 
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param region - the region to get flow through location.
     * 
     * @throws Exception
     */
    private void getFlowThroughLocation(String serial, String operator, String region) throws Exception {
         
        executeLutCmd("cmdPrTaskLUTPtsGetFTLocation",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, region});
    } 
    
    /**
     * reserve a license.
     * 
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param licenseNumber - the license number to reserve.
     * @param partialFlag - 1 means this is a partial license number
     * 
     * @throws Exception
     */
    private void verifyLicense(String serial, String operator, 
                               String licenseNumber, String partialFlag) 
    throws Exception {
         
        executeLutCmd("cmdPrTaskLUTPtsVerifyLicense",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, 
                                licenseNumber, partialFlag});
    }

    
    /**
     * get the assignment.
     * 
     * @param serial - terminal serial number
     * @param operator - operator for command

     * 
     * @throws Exception
     */
    private void getAssignment(String serial, String operator) throws Exception {
         
        executeLutCmd("cmdPrTaskLUTPtsGetAssignment",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
    }
    
    
    /**
     * get the put.
     * 
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param groupId - the group ID
     * 
     * @throws Exception
     */
    private void getPut(String serial, String operator, String groupId) throws Exception {
         
        executeLutCmd("cmdPrTaskLUTPtsGetPuts",
                new String[] {makeStringFromDate(this.getCmdDateSec()), this.serialId, this.operatorId, groupId});
    }
 
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param groupNumber - group number
     * @param locationId - Location skipping
     * @param skipIndicator - skip slot, aisle, or all
     * @param status - status to set to
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String groupNumber,
                                    String locationId,
                                    String skipIndicator,
                                    String status) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), serialId, operatorId, 
                groupNumber, locationId, skipIndicator, status}, 
            this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
    
    /**
     * count the number of puts in a group with a specified status.
     * 
     * @param status - status to count
     * @param groupNumber - the group number
     * @return - count of puts with specified status
     * @throws DataAccessException - database exception
     */
    private int countPutsWithStatus(PtsPutStatus status, Long groupNumber) 
    throws DataAccessException {
        int count = 0;
        
        for (PtsPut p : this.cmd.getPtsPutManager().listPutsForGroup(groupNumber)) {
            if (status == null) {
                count++;
            } else if (p.getStatus().equals(status)) {
                count++;
            }
        }
        return count;
    }
}
