/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author pfunyak
 *
 */

@Test(groups = { FAST, TASK, PUTTOSTORE })
public class PtsGetPutsCmdTest  extends TaskCommandTestCase  {

    private PtsGetPutsCmdRoot cmd;
    private String testOperId = "PtsGetPutsCmdOper";
    private String testSerial = "PtsGetPutsCmdSerial";

    /**
     * @return a bean for testing.
     */
    private PtsGetPutsCmdRoot getCmdBean() {
        return (PtsGetPutsCmdRoot)
            getBean("cmdPrTaskLUTPtsGetPuts");
    }

    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        classSetupCore();
        // load the work groups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the operators needed for testing
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        // load the put to store regions
        classSetupPTSRegions();
        classSetupInsert("VoiceLinkDataUnitTest_PtsData.xml");
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteNoPutsException() throws Exception {

        // request a license that all puts are in a final status (shorted or put)
        // command should throw an exception
        // Get to the proper state in the task.
        initialize(this.testSerial, this.testOperId);

        getRegion(this.testSerial, this.testOperId, "44");
        getFlowThroughLocation(this.testSerial, this.testOperId, "44");
        verifyLicense(this.testSerial, this.testOperId, "00000200", "0");
        getAssignment(this.testSerial, this.testOperId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupNumber.toString());
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get(ResponseRecord.ERROR_CODE_FIELD), "99", "ErrorCode");
        assertEquals(record.get(ResponseRecord.ERROR_MESSAGE_FIELD),
                "No puts available for current assignment.", "Wrong Error Message");
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteGoodStatus() throws Exception {

        // request a suspended license and only return puts that have a
        // status of not put, skipped or partial.
        initialize(this.testSerial, this.testOperId);
        getRegion(this.testSerial, this.testOperId, "43");
        getFlowThroughLocation(this.testSerial, this.testOperId, "43");
        verifyLicense(this.testSerial, this.testOperId, "0201", "1");
        getAssignment(this.testSerial, this.testOperId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupNumber.toString());
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        // we are expecting 3 put records
        assertEquals(records.size(), 3, "recordCount");

        ResponseRecord record;
        for (int index = 0; index < records.size(); index++) {
            record = records.get(index);
            String status = record.get("status").toString();
            if ((status != "N") && (status != "S")) {
                fail("Put Record " + index + " has an invalid status.");
            }
        }
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteGoodPutList() throws Exception {

        // request a license that we expect to return a list of puts
        // Get to the proper state in the task.
        initialize(this.testSerial, this.testOperId);
        getRegion(this.testSerial, this.testOperId, "43");
        getFlowThroughLocation(this.testSerial, this.testOperId, "43");
        verifyLicense(this.testSerial, this.testOperId, "0202", "1");
        getAssignment(this.testSerial, this.testOperId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupNumber.toString());
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        // we are expecting 3 put records
        assertEquals(records.size(), 3, "recordCount");

        // call method in the base class to verify the response fields.
        this.validateCommandFields(response);

        // verify that we did not get a error
        assertEquals("0", record.get("errorCode"), "ErrorCode");
        assertEquals("", record.get("errorMessage"), "ErrorMessage");

        // validate that fields contain the correct values
        assertEquals(record.get("status"), "N", "status should be N ");
        assertEquals(record.get("putID"), -20203L, "wrong putId ");
        assertEquals(record.get("locationID"), -20201L, "wrong location ID ");
        assertEquals(record.get("licenseNumber"), "00000202", "wrong license number ");
        assertEquals(record.get("preAisleDirection"), "building 1", " wrong preaisledirection ");
        assertEquals(record.get("aisle"), "03", "wrong aisle ");
        assertEquals(record.get("postAisleDirection"), "bay 3", "wrong postAisleDirection ");
        assertEquals(record.get("slot"), "097", "wrong slot ");
        assertEquals(record.get("checkDigits"), "10", "wrong checkDigits ");
        assertEquals(record.get("spokenLocVerification"), "03097", "wrong spokenLocVerification ");
        assertEquals(record.get("scannedLocVerification"), "0103097", "wrong scannedLocVerification ");
        assertEquals(record.get("quantityToPut"), 43,  "wrong quantity to put ");
        assertEquals(record.get("quantityPut"), 0,  "wrong quantity put ");
        assertEquals(record.get("itemNumber"), "140145", "wrong item number ");
        assertEquals(record.get("phoneticDescription"), "WRIG BIG RED T-PAK", "wrong phonetic description ");
        assertEquals(record.get("allowOverPack"), false, "wrong allowOverPack  ");
        assertEquals(record.get("residualQuantity"), 0, "wrong residualQuantity ");

        // verify that second record is correct
        record = records.get(1);
        assertEquals("0", record.get("errorCode"), "ErrorCode");
        assertEquals("", record.get("errorMessage"), "ErrorMessage");
        // validate that fields contain the correct values
        assertEquals(record.get("status"), "N", "wrong status ");
        assertEquals(record.get("putID"), -20202L, "wrong putId ");
        assertEquals(record.get("locationID"), -20203L, "wrong location ID ");
        assertEquals(record.get("licenseNumber"), "00000202", "wrong license number ");
        assertEquals(record.get("preAisleDirection"), "building 1", "wrong preAisleDirection ");
        assertEquals(record.get("aisle"), "01", "wrong aisle ");
        assertEquals(record.get("postAisleDirection"), "bay 1", "wrong postAisleDirection ");
        assertEquals(record.get("slot"), "037", "wrong slot ");
        assertEquals(record.get("checkDigits"), "12", "wrong checkDigits ");
        assertEquals(record.get("spokenLocVerification"), "01037", "wrong spokenLocVerification ");
        assertEquals(record.get("scannedLocVerification"), "0101037", "wrong scannedLocVerification ");
        assertEquals(record.get("quantityToPut"), 81,  "wrong quantity to put ");
        assertEquals(record.get("quantityPut"), 6,  "wrong quantity put ");
        assertEquals(record.get("itemNumber"), "140145", "wrong item number ");
        assertEquals(record.get("phoneticDescription"), "WRIG BIG RED T-PAK", "wrong Phonetic description ");
        assertEquals(record.get("allowOverPack"), true, "wrong allowOverPack ");
        assertEquals(record.get("residualQuantity"), 10, "wrong residualQuantity ");
     }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteSortBySequenceNumber() throws Exception {

        // Verify the that the put list is sorted by sequence number.
        // We should sort by sequence number when licenses are NOT grouped.
        initialize(this.testSerial, this.testOperId);
        getRegion(this.testSerial, this.testOperId, "43");
        getFlowThroughLocation(this.testSerial, this.testOperId, "43");
        verifyLicense(this.testSerial, this.testOperId, "0203", "1");
        getAssignment(this.testSerial, this.testOperId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupNumber.toString());
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        // we are expecting 5 put records
        assertEquals(records.size(), 5, "recordCount");
        // verify that we are sorted by sequence number
        // sequence number in the data is the same as the put ID
        for (int i = 0; i < records.size() - 1; i++) {
            Long sequence1 = (Long) records.get(i).get("putID");
            Long sequence2 = (Long) records.get(i + 1).get("putID");
            if (sequence1 > sequence2) {
                fail("Sort by sequence number - Incorrect sort order");
            }
        }
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteSortByPreAisle() throws Exception {

        // Verify the that the put list is sorted by pre-aisle.
        // ALL sequence number are the same. We should sort by PreAisle value.
        initialize(this.testSerial, this.testOperId);
        getRegion(this.testSerial, this.testOperId, "43");
        getFlowThroughLocation(this.testSerial, this.testOperId, "43");
        verifyLicense(this.testSerial, this.testOperId, "0204", "1");
        getAssignment(this.testSerial, this.testOperId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupNumber.toString());
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        // we are expecting 5 put records
        assertEquals(records.size(), 5, "recordCount");

        // verify that we are NOT sorted by sequence number.
        // To verify, put id's should be out of order.
        assertEquals(records.get(0).get("putID"), -20405L, "Should not be sorted by sequence.");
        assertEquals(records.get(1).get("putID"), -20401L, "Should not be sorted by sequence.");
        assertEquals(records.get(2).get("putID"), -20403L, "Should not be sorted by sequence.");
        assertEquals(records.get(3).get("putID"), -20404L, "Should not be sorted by sequence.");
        assertEquals(records.get(4).get("putID"), -20402L, "Should not be sorted by sequence.");

        // verify that we are sorted by pre-aisle
        assertEquals(records.get(0).get("preAisleDirection"), "building 1", "bad sort order preaisle");
        assertEquals(records.get(1).get("preAisleDirection"), "building 1", "bad sort order preaisle");
        assertEquals(records.get(2).get("preAisleDirection"), "building 2", "bad sort order preaisle");
        assertEquals(records.get(3).get("preAisleDirection"), "building 2", "bad sort order preaisle");
        assertEquals(records.get(4).get("preAisleDirection"), "building 3", "bad sort order preaisle");
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteSortByAisle() throws Exception {

        // Verify the that the put list is sorted by aisle.
        // ALL sequence numbers and pre-aisle directions are the same.
        // We should be sorted by aisle.
        initialize(this.testSerial, this.testOperId);
        getRegion(this.testSerial, this.testOperId, "43");
        getFlowThroughLocation(this.testSerial, this.testOperId, "43");
        verifyLicense(this.testSerial, this.testOperId, "0205", "1");
        getAssignment(this.testSerial, this.testOperId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupNumber.toString());
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        // we are expecting 5 put records
        assertEquals(records.size(), 5, "recordCount");
        // verify that we are NOT sorted by sequence number.
        // To verify, put id's should be out of order.
        assertEquals(records.get(0).get("putID"), -20502L, "Should not be sorted by sequence.");
        assertEquals(records.get(1).get("putID"), -20504L, "Should not be sorted by sequence.");
        assertEquals(records.get(2).get("putID"), -20505L, "Should not be sorted by sequence.");
        assertEquals(records.get(3).get("putID"), -20503L, "Should not be sorted by sequence.");
        assertEquals(records.get(4).get("putID"), -20501L, "Should not be sorted by sequence.");
        // verify that pre-aisle are the same for all puts.
        for (int i = 0; i < records.size(); i++) {
            assertEquals(records.get(i).get("preAisleDirection"),
                                            "building 3", "bad sort order preaisle should be building 3");
        }
        assertEquals(records.get(0).get("aisle"), "3 4 A", "bad sort order aisle");
        assertEquals(records.get(1).get("aisle"), "3 4 B", "bad sort order aisle");
        assertEquals(records.get(2).get("aisle"), "3 8 1", "bad sort order aisle");
        assertEquals(records.get(3).get("aisle"), "3 9 0", "bad sort order aisle");
        assertEquals(records.get(4).get("aisle"), "5 7 D", "bad sort order aisle");
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteSortByPostAisle() throws Exception {

        // Verify the that the put list is sorted by PostAisle.
        // ALL sequence numbers and pre-aisle directions and aisles are the same.
        // We should be sorted by post-aisle.
        initialize(this.testSerial, this.testOperId);
        getRegion(this.testSerial, this.testOperId, "43");
        getFlowThroughLocation(this.testSerial, this.testOperId, "43");
        verifyLicense(this.testSerial, this.testOperId, "0206", "1");
        getAssignment(this.testSerial, this.testOperId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupNumber.toString());
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        // we are expecting 5 put records
        assertEquals(records.size(), 5, "recordCount");
        // verify that we are NOT sorted by sequence number.
        // To verify, put id's should be out of order.
        assertEquals(records.get(0).get("putID"), -20604L, "Should not be sorted by sequence.");
        assertEquals(records.get(1).get("putID"), -20601L, "Should not be sorted by sequence.");
        assertEquals(records.get(2).get("putID"), -20602L, "Should not be sorted by sequence.");
        assertEquals(records.get(3).get("putID"), -20603L, "Should not be sorted by sequence.");
        assertEquals(records.get(4).get("putID"), -20605L, "Should not be sorted by sequence.");
        // verify that pre-aisle and aisle are the same for all puts.
        for (int i = 0; i < records.size(); i++) {
            assertEquals(records.get(i).get("preAisleDirection"),
                                            "building 4", "bad sort order preaisle should be building 3");
            assertEquals(records.get(i).get("aisle"), "A 3", "bad sort order aisle should be A 3");

        }
        assertEquals(records.get(0).get("postAisleDirection"), "bay 11", "bad sort order postAisle");
        assertEquals(records.get(1).get("postAisleDirection"), "bay 12", "bad sort order postAisle");
        assertEquals(records.get(2).get("postAisleDirection"), "bay 13", "bad sort order postAisle");
        assertEquals(records.get(3).get("postAisleDirection"), "bay 14", "bad sort order postAisle");
        assertEquals(records.get(4).get("postAisleDirection"), "bay 15", "bad sort order postAisle");
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteSortBySlot() throws Exception {

        // Verify the that the put list is sorted by slot.
        // ALL sequence numbers, pre-aisle, aisle, post-aisles are the same.
        // We should be sorted by slot.
        initialize(this.testSerial, this.testOperId);
        getRegion(this.testSerial, this.testOperId, "43");
        getFlowThroughLocation(this.testSerial, this.testOperId, "43");
        verifyLicense(this.testSerial, this.testOperId, "0207", "1");
        getAssignment(this.testSerial, this.testOperId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupNumber.toString());
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        // we are expecting 5 put records
        assertEquals(records.size(), 5, "recordCount");
        // verify that we are NOT sorted by sequence number.
        // To verify, put id's should be out of order.
        assertEquals(records.get(0).get("putID"), -20702L, "Should not be sorted by sequence.");
        assertEquals(records.get(1).get("putID"), -20705L, "Should not be sorted by sequence.");
        assertEquals(records.get(2).get("putID"), -20703L, "Should not be sorted by sequence.");
        assertEquals(records.get(3).get("putID"), -20701L, "Should not be sorted by sequence.");
        assertEquals(records.get(4).get("putID"), -20704L, "Should not be sorted by sequence.");
        // verify that sequence and pre-aisle, aisle, post aisle and slot
        // are the same for all puts.
        for (int i = 0; i < records.size(); i++) {
            assertEquals(records.get(i).get("preAisleDirection"),
                                            "building 5", "bad sort order preaisle should be building 3");
            assertEquals(records.get(i).get("aisle"), "A 3", "bad sort order aisle should be A 3");
            assertEquals(records.get(i).get("postAisleDirection"), "bay 11",
                                            "bad sort order post aisle direction should be bay 11");
        }
        assertEquals(records.get(0).get("slot"), "25B", "bad sort order slot");
        assertEquals(records.get(1).get("slot"), "287", "bad sort order slot");
        assertEquals(records.get(2).get("slot"), "341", "bad sort order slot");
        assertEquals(records.get(3).get("slot"), "447", "bad sort order slot");
        assertEquals(records.get(4).get("slot"), "55G", "bad sort order slot");
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteSortGroup() throws Exception {

        // Verify the we can sort a group of licenses.
        // For this test to be successful, the sequence number should be ignored
        // Therefore, the list should NOT be sorted by sequence number

        initialize(this.testSerial, this.testOperId);
        getRegion(this.testSerial, this.testOperId, "43");
        getFlowThroughLocation(this.testSerial, this.testOperId, "43");
        verifyLicense(this.testSerial, this.testOperId, "0208", "1");
        verifyLicense(this.testSerial, this.testOperId, "0209", "1");
        getAssignment(this.testSerial, this.testOperId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupNumber.toString());
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        // we are expecting 10 put records
        assertEquals(records.size(), 10, "recordCount");
        // since the first 3 sequence numbers are out of order, we are not using the
        // sequence number as one of the sort criteria which is the correct behavior.
        assertEquals(records.get(0).get("putID"), -20802L, "Should not be sorted by sequence.");
        assertEquals(records.get(1).get("putID"), -20801L, "Should not be sorted by sequence.");
        assertEquals(records.get(2).get("putID"), -20803L, "Should not be sorted by sequence.");

        for (int i = 0; i < 5; i++) {
            assertEquals(records.get(i).get("preAisleDirection"), "building 1",
                           "bad sort order group sort should be building 1");
        }
        for (int i = 5; i < 10; i++) {
            assertEquals(records.get(i).get("preAisleDirection"), "building 2",
                           "bad sort order group sort should be building 2");
        }
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteSortByGroupPosition() throws Exception {

        // Verify the we can sort a group of licenses.
        // For this test to be successful, the sequence number should be ignored
        // and we should be sorted by group position.

        initialize(this.testSerial, this.testOperId);
        getRegion(this.testSerial, this.testOperId, "43");
        getFlowThroughLocation(this.testSerial, this.testOperId, "43");
        verifyLicense(this.testSerial, this.testOperId, "0211", "1");
        verifyLicense(this.testSerial, this.testOperId, "0210", "1");
        getAssignment(this.testSerial, this.testOperId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupNumber.toString());
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        // we are expecting 6 put records
        assertEquals(records.size(), 6, "recordCount");

        // This test is verifying that we can sort puts by license position
        // (order of induction) for a group of licenses.
        // License 211 contains 3 puts for item -190 for customer a, b and c.
        // license 210 contains 3 puts for item -200 for for customer a, b and c.
        // The only thing different about the customer locations is the slot.
        // Customer A = slot 127, Customer B = Slot 207, and Customer C = slot 189
        // For the test to be successful the put list should be sorted by slot and position.
        // Since license 211 was inducted first, when the list is sorted by slot, the
        // sequence numbers for license 211 should show up first.  The sequence numbers
        // should not be in order since we ingnore the sequence number when sorting a group.

        ResponseRecord record = records.get(0);
        assertEquals(record.get("putID"), -21101L, "Wrong sequence number. ");
        assertEquals(record.get("slot"), "127", "Incorrect slot. ");
        assertEquals(record.get("itemNumber"), "140384", "item should be 140384");

        record = records.get(1);
        assertEquals(record.get("putID"), -21001L, "Wrong sequence number. ");
        assertEquals(record.get("slot"), "127", "Incorrect slot. ");
        assertEquals(record.get("itemNumber"), "140145", "item should be 140145");

        record = records.get(2);
        assertEquals(record.get("putID"), -21103L, "Wrong sequence number. ");
        assertEquals(record.get("slot"), "189", "Incorrect slot. ");
        assertEquals(record.get("itemNumber"), "140384", "item should be 140384");

        record = records.get(3);
        assertEquals(record.get("putID"), -21003L, "Wrong sequence number. ");
        assertEquals(record.get("slot"), "189", "Incorrect slot. ");
        assertEquals(record.get("itemNumber"), "140145", "item should be 140145");

        record = records.get(4);
        assertEquals(record.get("putID"), -21102L, "Wrong sequence number. ");
        assertEquals(record.get("slot"), "207", "Incorrect slot. ");
        assertEquals(record.get("itemNumber"), "140384", "item should be 140384");

        record = records.get(5);
        assertEquals(record.get("putID"), -21002L, "Wrong sequence number. ");
        assertEquals(record.get("slot"), "207", "Incorrect slot. ");
        assertEquals(record.get("itemNumber"), "140145", "item should be 140145");
    }



    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     *
     * @throws Exception
     */
    private void initialize(String serial, String operator) throws Exception {
        final Locale locale = Locale.US;

        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
    }

    /**
     * Gets the region to sign into.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param region - the region to sign into.
     *
     * @throws Exception
     */
    private void getRegion(String serial, String operator, String region) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsValidRegions",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTPtsGetRegionConfiguration",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, region});
    }


    /**
     * Gets the flow through location.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param region - the region to get flow through location.
     *
     * @throws Exception
     */
    private void getFlowThroughLocation(String serial, String operator, String region) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsGetFTLocation",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, region});
    }

    /**
     * reserve a license.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param licenseNumber - the license number to reserve.
     * @param partialFlag - 1 means this is a partial license number
     *
     * @throws Exception
     */
    private void verifyLicense(String serial, String operator,
                               String licenseNumber, String partialFlag)
    throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsVerifyLicense",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
                                licenseNumber, partialFlag});
    }


    /**
     * get the assignment.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command

     *
     * @throws Exception
     */
    private void getAssignment(String serial, String operator) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsGetAssignment",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
    }



    /**
     * Execute a command.
     *
     * @param cmdDate - date for command
     * @param serial - terminal serial number
     * @param operator - the operator
     * @param groupNumber - the group number
     *
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String serial, String operator,
                                    String groupNumber) throws Exception {
        this.cmd = getCmdBean();
        // call the command
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), serial, operator, groupNumber}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}

