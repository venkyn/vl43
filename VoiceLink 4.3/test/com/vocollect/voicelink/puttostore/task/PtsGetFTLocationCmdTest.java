/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.puttostore.task;



import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Tests for Flow Though location cmd.
 *
 * @author pfunyak
 *
 */

@Test(groups = { FAST, TASK, PUTTOSTORE })
public class PtsGetFTLocationCmdTest  extends TaskCommandTestCase  {

    private PtsGetFTLocationCmd cmd;
    private String testOperId = "PtsGetFTLocationOper";
    private String testSerial = "PtsGetFTLocationSerial";

    /**
     * @return a bean for testing.
     */
    private PtsGetFTLocationCmd getCmdBean() {
        return (PtsGetFTLocationCmd)
            getBean("cmdPrTaskLUTPtsGetFTLocation");
    }


    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        // load the workgroups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the operators needed for testing
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        // load the put to store regions
        classSetupPTSRegions();
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteGoodFTLocation() throws Exception {

        String regionNumber = "41";
        // Get to the proper state in the task.
        initialize(this.testSerial, this.testOperId, regionNumber);

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, regionNumber);
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        // validate response fields
        this.validateCommandFields(response);

        assertEquals(record.get("errorCode"), "0", "ErrorCode");
        assertEquals(record.get("errorMessage"), "", "ErrorMessage");
        assertEquals(record.get("ftLocation"), "the flow thru location");
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteNullFTLocation() throws Exception {

        String regionNumber = "42";
        // Get to the proper state in the task.
        initialize(this.testSerial, this.testOperId, regionNumber);

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, regionNumber);
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(record.get("errorCode"), "0", "ErrorCode");
        assertEquals(record.get("errorMessage"), "", "ErrorMessage");
        assertEquals(record.get("ftLocation"), null);
    }


    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param regionNumber - the region number
     *
     * @throws Exception
     */
    private void initialize(String serial, String operator, String regionNumber) throws Exception {
        final Locale locale = Locale.US;

        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
        executeLutCmd("cmdPrTaskLUTPtsValidRegions",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTPtsGetRegionConfiguration",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, regionNumber});

    }


    /**
     * Execute a command.
     *
     * @param cmdDate - date for command
     * @param serial - terminal serial number
     * @param operator - Operator id
     * @param regionNumber - the region number
     *
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String serial,
                                    String operator, String regionNumber) throws Exception {
        this.cmd = getCmdBean();
        // call the command
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), serial, operator, regionNumber}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
