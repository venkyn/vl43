/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * This class tests the PtsGetExpectedResidualCmd.
 *
 * @author mnichols
 */
@Test(groups = { FAST, TASK, PUTTOSTORE })
public class PtsGetExpectedResidualCmdTest extends TaskCommandTestCase {

    private PtsGetExpectedResidualCmd cmd;
    private String operatorId = "PTSGetExpectedResidualOper";
    private String serialId = "PTSGetExpectedResidualSerial";

    /**
     * @return a bean for testing.
     */
    private PtsGetExpectedResidualCmd getCmdBean() {
        return (PtsGetExpectedResidualCmd)
            getBean("cmdPrTaskLUTPtsGetExpectedResidual");
    }

   /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }


    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        classSetupCore();
        // load the work groups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the operators needed for testing
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        // load the put to store regions
        classSetupPTSRegions();
        classSetupInsert("VoiceLinkDataUnitTest_PtsData.xml");
    }

  /**
   * This test verifies that 2 assignments grouped together
   * returns 4 records because each assignment has 2 items.
   * @throws Exception
   */
    @Test()
    public void testGroupAssignment() throws Exception {

        initialize(this.serialId, this.operatorId);
        getRegion(this.serialId, this.operatorId, "42");
        getFlowThroughLocation(this.serialId, this.operatorId, "42");
        verifyLicense(this.serialId, this.operatorId, "01001", "1");
        verifyLicense(this.serialId, this.operatorId, "01003", "1");
        getAssignment(this.serialId, this.operatorId);

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        //Get records back
        assertEquals(response.getRecords().size(), 3, "recordSize");
        this.validateCommandFields(cmd.getResponse());
    }

    /**
     * This test verifies that we get a single response record for a license
     * that has multiple puts for the same item.
     * @throws Exception
     */
    @Test()
    public void testSingleAssignment() throws Exception {

        initialize(this.serialId, this.operatorId);

        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000501", "0");
        getAssignment(this.serialId, this.operatorId);

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        //Get records back
        assertEquals(response.getRecords().size(), 1, "recordSize");
        this.validateCommandFields(cmd.getResponse());
    }


    /**
     * This test verifies that an invalid group number throws a task command exception.
     * @throws Exception
     */
    @Test()
    public void testInvalidGroupNumber() throws Exception {

        initialize(this.serialId, this.operatorId);

        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000502", "0");
        getAssignment(this.serialId, this.operatorId);
        getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");

        Response response = executeCommand(this.getCmdDateSec(), "9999");

        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");

        assertEquals(record.getErrorCode(),
                Long.toString(TaskErrorCode.PTS_NO_LICENSES_FOR_GROUP_NUMBER.getErrorCode()),
                "Test for invalid group number - FAILED");
   }


    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     *
     * @throws Exception
     */
    private void initialize(String serial, String operator) throws Exception {
        final Locale locale = Locale.US;

        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
    }

    /**
     * Gets the region to sign into.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param region - the region to sign into.
     *
     * @throws Exception
     */
    private void getRegion(String serial, String operator, String region) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsValidRegions",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTPtsGetRegionConfiguration",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, region});
    }


    /**
     * Gets the flow through location.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param region - the region to get flow through location.
     *
     * @throws Exception
     */
    private void getFlowThroughLocation(String serial, String operator, String region) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsGetFTLocation",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, region});
    }

    /**
     * reserve a license.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param licenseNumber - the license number to reserve.
     * @param partialFlag - 1 means this is a partial license number
     *
     * @throws Exception
     */
    private void verifyLicense(String serial, String operator,
                               String licenseNumber, String partialFlag) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsVerifyLicense",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
                                licenseNumber, partialFlag});
    }


    /**
     * get the assignment.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command

     *
     * @throws Exception
     */
    private void getAssignment(String serial, String operator) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsGetAssignment",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
    }

    /**
     * Execute a command.
     *
     * @param cmdDate - date for command
     * @param groupNumber - group number to pass
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String groupNumber) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDateMS(cmdDate), serialId, operatorId,
                groupNumber},
            this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }

}
