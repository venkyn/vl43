/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for PrintCmd.
 *
 * @author treed
 */
@Test(groups = { FAST, TASK })
public class PtsPrintContainerLabelCmdTest extends TaskCommandTestCase {

    private PtsPrintContainerLabelCmd cmd;
    private String operId = "PtsPrintContainerLabelOper";
    private String sn = "PtsPrintContainerLabelSerial";

    /**
     * @return a bean for testing.
     */
    @Test(enabled = false)
    private PtsPrintContainerLabelCmd getCmdBean() {
        return (PtsPrintContainerLabelCmd) getBean("cmdPrTaskLUTPtsPrintContainerLabel");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }


    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        classSetupCore();
        // Setup Printers
        classSetupInsert("VoiceLinkDataUnitTest_Printers.xml");
        // load the work groups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the operators needed for testing
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        classSetupPTSRegions();
        classSetupInsert("VoiceLinkDataUnitTest_PtsData.xml");
    }



    /**
     * Test that an invalid printer number is caught and returned to the
     * terminal operator.
     * @throws Exception
     */
    @Test()
    public void testPrinterExceptions() throws Exception {

        initialize();

        // Printer number 2 is a valid printer.
        // all other data passed to the command is valid.
        // this call will throw an exception because the
        // print server is not configured correctly
        Response response = executeCommand(this.getCmdDateSec(), "1", "2", -44106L, "4410006");

        // Verify print server error
        List<ResponseRecord>  records = response.getRecords();
        ResponseRecord record = records.get(0);

        validateCommandFields(response);

        assertEquals(record.getErrorCode(), "1048",
            "Expected Printer Job error (Physical Printer does not exist), but got: " + record.getErrorMessage());

        // Printer number 9 should be invalid because it does not exist
        response = executeCommand(this.getCmdDateSec(), "1", "9", 1L, "001");
        //Verify invalid printer result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(record.getErrorCode(),
                Long.toString(TaskErrorCode.PRINTER_NOT_FOUND.getErrorCode()),
                "Expected InvalidPrinterNumber errorCode, but got: " + record.getErrorMessage());
        assertEquals(record.getErrorMessage(), "Printer 9 undefined", "incorrect error message");


        // Printer number 5 is disabled
        response = executeCommand(this.getCmdDateSec(), "1", "5", 1L, "001");
        records = response.getRecords();
        record = records.get(0);
        assertEquals(record.getErrorCode(),
                Long.toString(TaskErrorCode.PRINTER_IS_DISABLED.getErrorCode()),
                "got incorrect error code");
        assertEquals(record.getErrorMessage(), "Printer 5 is disabled", "incorrect error message");

        // Printer number 2 is a valid printer number
        // customer location 10 does not exist
        Long locationId = -99999L;
        String containerNumber = "1000230";

        response = executeCommand(this.getCmdDateSec(), "1", "2", locationId, containerNumber);
        // Verify not open container at the location
        records = response.getRecords();
        record = records.get(0);

        assertEquals(record.getErrorCode(), "98", "got incorrect error code");
        assertEquals(record.getErrorMessage(),
                     "Customer location identifier -99999 not found.  Please notify your supervisor.",
                     "Wrong error message");


        // try printer 2 again with a valid location ID
        // container number exists at location but is closed
        locationId = -43003L;
        containerNumber = "1000230";

        response = executeCommand(this.getCmdDateSec(), "1", "2", locationId, containerNumber);
        // Verify not open container at the location
        records = response.getRecords();
        record = records.get(0);

        //Confirm we get a 98 error code.
        assertEquals(record.getErrorCode(), "98",
            "Expected no open containers error, but got: " + record.getErrorMessage());

        //Confirm we get the expected message.
        String expectedErrorMessageContents = "No open containers defined at location "
            + locationId + ".";
        assertTrue(record.getErrorMessage().equals(expectedErrorMessageContents),
            "Expected " + expectedErrorMessageContents + " but got: " + record.getErrorMessage());
    }


    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     *
     * @throws Exception - Exception
     */

    private void initialize() throws Exception {
        final Locale locale = Locale.US;


        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration",
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId,
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes",
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn",
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }

    /**
     * Execute a command.
     *
     * @param cmdDate - Date command is issued.
     * @param groupId - group id license belongs to
     * @param printerNumber - printer to use
     * @param locationId - location of container
     * @param containerNumber - container number
     * @return - return a response from command
     * @throws Exception - all exceptions
     */

    private Response executeCommand(Date cmdDate,
                                    String groupId,
                                    String printerNumber,
                                    Long locationId,
                                    String containerNumber) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId,
                groupId, printerNumber, locationId.toString(), containerNumber}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
