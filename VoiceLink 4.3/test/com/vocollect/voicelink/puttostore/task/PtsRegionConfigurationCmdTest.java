/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;


import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.OperatorFunctionLabor;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;


import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * Test for PtsRegionConfigurationCmd.
 *
 * @author jtauberg
 */

@Test(groups = { FAST, TASK, PUTTOSTORE })
public class PtsRegionConfigurationCmdTest extends TaskCommandTestCase {

    private PtsRegionConfigurationCmd cmd;
    private static final String PTS_OPERATOR_ID = "PtsRegionConfigurationOper";
    private static final String SELECTION_OPERATOR_ID = "selectionOper";
    private static final String PTS_SERIAL = "PtsRegionConfigurationSerial";
    private static final Locale LOCALE = Locale.US;

    
    //Create constants for each of the response keys:
    private static final String NUMBER                          = "number";
    private static final String NAME                            = "name";
    private static final String SKIP_AISLE_ALLOWED              = "skipAisleAllowed";
    private static final String SKIP_SLOT_ALLOWED               = "skipSlotAllowed";
    private static final String REPICK_SKIPS                    = "repickSkips";
    private static final String SIGN_OFF_ALLOWED                = "signOffAllowed";
    private static final String ALLOW_PASS_ASSIGNMENT           = "allowPassAssignment";
    private static final String ALLOW_MULTIPLE_OPEN_CONTAINERS  = "allowMultipleOpenContainers";
    private static final String SYSTEM_GENERATES_CONTAINER_ID   = "systemGeneratesContainerID";
    private static final String SPOKEN_LICENSE_LENGTH           = "spokenLicenseLength";
    private static final String CONFIRM_SPOKEN_LICENSE          = "confirmSpokenLicense";
    private static final String VALIDATE_CONTAINERS             = "validateContainers";
    private static final String VALIDATE_CONTAINER_LENGTH       = "validateContainerLength";
    private static final String CONFIRM_SPOKEN_LOCATION         = "confirmSpokenLocation";
    private static final String SPOKEN_LOCATION_LENGTH          = "spokenLocationLength";
    private static final String LOCATION_CHECK_DIGIT_LENGTH     = "locationCheckDigitLength";
    private static final String MAX_LICENSES_IN_GROUP           = "maxLicensesInGroup";
    private static final String CURRENT_PRE_AISLE               = "currentPreAisle";
    private static final String CURRENT_AISLE                   = "currentAisle";
    private static final String CURRENT_POST_AISLE              = "currentPostAisle";
    private static final String CURRENT_SLOT                    = "currentSlot";
    private static final String PRINT_EXCEPTION_LABEL           = "printExceptionLabel";
    
    /**
     * @return a bean for testing.
     */
    private PtsRegionConfigurationCmd getCmdBean() {
        return (PtsRegionConfigurationCmd) 
            getBean("cmdPrTaskLUTPtsGetRegionConfiguration");
    }
    
    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        // load the workgroups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the workgroups for fork apps so we have a realistic system
        classSetupInsert("VoiceLinkDataUnitTest_ForkAppWorkGroups.xml");        
        // load the operators needed for testing 
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        // load the Put to store regions 
        classSetupPTSRegions();
        // load the put away and replenishment regions so we can make sure that 
        // when other region types are loaded we only get put to store regions 
        // when requesting regions.
        classSetupPutAwayRegions();
        classSetupReplenRegions();
    }


    /**
     * This test will try to sign into region 43.
     * Region 43 is sort of the opposite of Region 45 (tested below).
     * It is a put to store region where every boolean is true (except 1),
     * and all numeric values are one digit and all are different from region 45.
     * 
     * @throws Exception
     */
    @Test(enabled = false)
    public void testExecuteRegionConfiguration43() throws Exception {
        //===============================================================
        // Sign on an operator who is authorized for put to store.
        initialize(PTS_SERIAL, PTS_OPERATOR_ID);
        //Try to sign into region 43
        Response response = executeCommand(this.getCmdDateSec(), PTS_SERIAL, PTS_OPERATOR_ID, "43");

        // Verify that we get region 43's settings returned
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        // validate command fields
        this.validateCommandFields(response);
        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "0", 
            "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"), "", 
            "Response returned incorrect ErrorMessage");

        //Test fields returned from record.
        assertEquals(records.get(0).get(NUMBER), 43, 
            "Response returned incorrect " + NUMBER + ".");
        assertEquals(records.get(0).get(NAME),  "Put to store region 43", 
            "Response returned incorrect " + NAME + ".");
        assertEquals(records.get(0).get(SKIP_AISLE_ALLOWED), true, 
            "Response returned incorrect " + SKIP_AISLE_ALLOWED + ".");
        assertEquals(records.get(0).get(SKIP_SLOT_ALLOWED), true, 
            "Response returned incorrect " + SKIP_SLOT_ALLOWED + ".");
        assertEquals(records.get(0).get(REPICK_SKIPS), true, 
            "Response returned incorrect " + REPICK_SKIPS + ".");
        assertEquals(records.get(0).get(SIGN_OFF_ALLOWED), true, 
            "Response returned incorrect " + SIGN_OFF_ALLOWED + ".");
        assertEquals(records.get(0).get(ALLOW_PASS_ASSIGNMENT), true, 
            "Response returned incorrect " + ALLOW_PASS_ASSIGNMENT + ".");
        assertEquals(records.get(0).get(ALLOW_MULTIPLE_OPEN_CONTAINERS), false, 
            "Response returned incorrect " + ALLOW_MULTIPLE_OPEN_CONTAINERS + ".");
        assertEquals(records.get(0).get(SYSTEM_GENERATES_CONTAINER_ID), true, 
            "Response returned incorrect " + SYSTEM_GENERATES_CONTAINER_ID + ".");
        assertEquals(records.get(0).get(SPOKEN_LICENSE_LENGTH), 4, 
            "Response returned incorrect " + SPOKEN_LICENSE_LENGTH + ".");
        assertEquals(records.get(0).get(CONFIRM_SPOKEN_LICENSE), true, 
                "Response returned incorrect " + CONFIRM_SPOKEN_LICENSE + ".");
        assertEquals(records.get(0).get(VALIDATE_CONTAINERS), true, 
            "Response returned incorrect " + VALIDATE_CONTAINERS + ".");
        assertEquals(records.get(0).get(VALIDATE_CONTAINER_LENGTH), 3, 
            "Response returned incorrect " + VALIDATE_CONTAINER_LENGTH + ".");
        assertEquals(records.get(0).get(CONFIRM_SPOKEN_LOCATION), true, 
            "Response returned incorrect " + CONFIRM_SPOKEN_LOCATION + ".");
        assertEquals(records.get(0).get(SPOKEN_LOCATION_LENGTH), 3, 
            "Response returned incorrect " + SPOKEN_LOCATION_LENGTH + ".");
        assertEquals(records.get(0).get(LOCATION_CHECK_DIGIT_LENGTH), 2,
            "Response returned incorrect " + LOCATION_CHECK_DIGIT_LENGTH + ".");
        assertEquals(records.get(0).get(MAX_LICENSES_IN_GROUP), 3, 
            "Response returned incorrect " + MAX_LICENSES_IN_GROUP + ".");
        assertEquals(records.get(0).get(CURRENT_PRE_AISLE), "X", 
                "Response returned incorrect " + CURRENT_PRE_AISLE + ".");
        assertEquals(records.get(0).get(CURRENT_AISLE), "X", 
                "Response returned incorrect " + CURRENT_AISLE + ".");
        assertEquals(records.get(0).get(CURRENT_POST_AISLE), "X", 
                "Response returned incorrect " + CURRENT_POST_AISLE + ".");
        assertEquals(records.get(0).get(CURRENT_SLOT), "X", 
                "Response returned incorrect " + CURRENT_SLOT + ".");    
        assertEquals(records.get(0).get(PRINT_EXCEPTION_LABEL), false, 
                "Response returned incorrect " + PRINT_EXCEPTION_LABEL + "."); 
        

      //Test that a put to store labor record was opened for the correct region
      OperatorFunctionLabor operLabor = 
          (OperatorFunctionLabor) this.cmd.getLaborManager().getOperatorLaborManager()
          .findOpenRecordByOperatorId(this.cmd.getOperator().getId());

      if (operLabor == null) {
          fail("PutToStore Labor Record was not created");
      } else {
          assertEquals(operLabor.getActionType(), OperatorLaborActionType.PutToStore, 
                       "Invalid labor record type ");
          int regionNumber = operLabor.getRegion().getNumber();
          assertEquals(regionNumber, 43, "Invalid region id in labor record");
          assertNull(operLabor.getEndTime());
      }
        
    }
    
  
    /**
     * This test will try to sign into region 45.
     * Region 45 is sort of the opposite of Region 43.
     * It is a put to store region where every boolean is false (except 1),
     * and all numeric values are at their maximum length (99 or 9) and all
     * different from region 43.
     * 
     * @throws Exception
     */
   @Test(enabled = false)
    public void testExecuteRegionConfiguration45() throws Exception {

        // Sign on an operator who is authorized for put to store.
        initialize(PTS_SERIAL, PTS_OPERATOR_ID);
        //Try to sign into region 45
        Response response = executeCommand(this.getCmdDateSec(), PTS_SERIAL, PTS_OPERATOR_ID, "45");

        // Verify that we get region 45's settings returned
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "0", 
            "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"), "", 
            "Response returned incorrect ErrorMessage");

        //Test fields returned from record.
        assertEquals(records.get(0).get(NUMBER), 45, 
            "Response returned incorrect " + NUMBER + ".");
        assertEquals(records.get(0).get(NAME), "Put to store region 45 with nots", 
            "Response returned incorrect " + NAME + ".");
        assertEquals(records.get(0).get(SKIP_AISLE_ALLOWED), false, 
            "Response returned incorrect " + SKIP_AISLE_ALLOWED + ".");
        assertEquals(records.get(0).get(SKIP_SLOT_ALLOWED), false, 
            "Response returned incorrect " + SKIP_SLOT_ALLOWED + ".");
        assertEquals(records.get(0).get(REPICK_SKIPS), false, 
            "Response returned incorrect " + REPICK_SKIPS + ".");
        assertEquals(records.get(0).get(SIGN_OFF_ALLOWED), false, 
            "Response returned incorrect " + SIGN_OFF_ALLOWED + ".");
        assertEquals(records.get(0).get(ALLOW_PASS_ASSIGNMENT), false, 
            "Response returned incorrect " + ALLOW_PASS_ASSIGNMENT + ".");
        assertEquals(records.get(0).get(ALLOW_MULTIPLE_OPEN_CONTAINERS), true, 
            "Response returned incorrect " + ALLOW_MULTIPLE_OPEN_CONTAINERS + ".");
        assertEquals(records.get(0).get(SYSTEM_GENERATES_CONTAINER_ID), false, 
            "Response returned incorrect " + SYSTEM_GENERATES_CONTAINER_ID + ".");
        assertEquals(records.get(0).get(SPOKEN_LICENSE_LENGTH), 9, 
            "Response returned incorrect " + SPOKEN_LICENSE_LENGTH + ".");
        assertEquals(records.get(0).get(CONFIRM_SPOKEN_LICENSE), true, 
                "Response returned incorrect " + CONFIRM_SPOKEN_LICENSE + ".");        
        assertEquals(records.get(0).get(VALIDATE_CONTAINERS), false, 
            "Response returned incorrect " + VALIDATE_CONTAINERS + ".");
        assertEquals(records.get(0).get(VALIDATE_CONTAINER_LENGTH), 99, 
            "Response returned incorrect " + VALIDATE_CONTAINER_LENGTH + ".");
        assertEquals(records.get(0).get(CONFIRM_SPOKEN_LOCATION), false, 
            "Response returned incorrect " + CONFIRM_SPOKEN_LOCATION + ".");
        assertEquals(records.get(0).get(SPOKEN_LOCATION_LENGTH), 99, 
            "Response returned incorrect " + SPOKEN_LOCATION_LENGTH + ".");
        assertEquals(records.get(0).get(LOCATION_CHECK_DIGIT_LENGTH), 99,
            "Response returned incorrect " + LOCATION_CHECK_DIGIT_LENGTH + ".");
        assertEquals(records.get(0).get(MAX_LICENSES_IN_GROUP), 99, 
            "Response returned incorrect " + MAX_LICENSES_IN_GROUP + ".");
        assertEquals(records.get(0).get(CURRENT_PRE_AISLE), "X", 
                "Response returned incorrect " + CURRENT_PRE_AISLE + ".");
        assertEquals(records.get(0).get(CURRENT_AISLE), "X", 
                "Response returned incorrect " + CURRENT_AISLE + ".");
        assertEquals(records.get(0).get(CURRENT_POST_AISLE), "X",  
                "Response returned incorrect " + CURRENT_POST_AISLE + ".");
        assertEquals(records.get(0).get(CURRENT_SLOT), "X", 
                "Response returned incorrect " + CURRENT_SLOT + ".");        
        assertEquals(records.get(0).get(CURRENT_SLOT), "X", 
                "Response returned incorrect " + CURRENT_SLOT + "."); 
        assertEquals(records.get(0).get(PRINT_EXCEPTION_LABEL), false, 
                "Response returned incorrect " + PRINT_EXCEPTION_LABEL + ".");   
        

        //Test that a put to store labor record was opened for the correct region
        OperatorFunctionLabor operLabor = 
            (OperatorFunctionLabor) this.cmd.getLaborManager().getOperatorLaborManager()
            .findOpenRecordByOperatorId(this.cmd.getOperator().getId());

        if (operLabor == null) {
            fail("PutToStore Labor Record was not created");
        } else {
            assertEquals(OperatorLaborActionType.PutToStore, operLabor.getActionType(),
                         "Invalid labor record type ");
            assertEquals(operLabor.getRegion().getNumber().intValue(), 45, "Invalid region id in labor record");
            assertNull(operLabor.getEndTime());
            assertEquals(operLabor.getDuration().longValue(), 0, "duration should be zero.");
            assertEquals(operLabor.getActualRate().doubleValue(), 0.0, "rate should be zero");
            assertEquals(operLabor.getExportStatus(), ExportStatus.NotExported);
            assertEquals(operLabor.getCount().intValue(), 0, "count should be zero");
            assertNull(operLabor.getBreakType());
            assertEquals(operLabor.getPercentOfGoal().doubleValue(), 0.0, "percent of goal should be zero");
            assertEquals(operLabor.getFilterType(), OperatorLaborFilterType.PutToStore, "filter type should be 5");
        }
    }

    
    
    /**
     * This test will try to sign into region 34 which is a valid region for
     * replenishment but not for PTS so we should get an invalid region error.
     * 
     * @throws Exception
     */
    @Test(enabled = false)
    public void testInvalidRegionNumber() throws Exception {

        // Sign on an operator who is authorized for put to store.
        initialize(PTS_SERIAL, PTS_OPERATOR_ID);
        //Try to sign into region 34
        Response response = executeCommand(this.getCmdDateSec(), PTS_SERIAL, PTS_OPERATOR_ID, "34");

        // Verify that we get 1 record returned
        List<ResponseRecord> records = response.getRecords();
        assertEquals(1, records.size(), "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"),
                String.valueOf(TaskErrorCode.INVALID_REGION_NUMBER.getErrorCode()), 
                "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"), "Requested region 34 is invalid.", 
            "Response returned incorrect ErrorMessage");
        
        //Test that a put to store labor record was opened for the correct region
        OperatorLabor operLabor = this.cmd.getLaborManager().getOperatorLaborManager()
            .findOpenRecordByOperatorId(this.cmd.getOperator().getId());
        
        // open labor record should be a sign on labor record
        if (operLabor == null) {
            fail("No Labor Record was created");
        } else {
            assertEquals(operLabor.getActionType(), OperatorLaborActionType.SignOn, 
                         "Invalid labor record type ");
        }
    }

    
    /**
     * This test will try to sign into region 7777777 which is an invalid region
     * number.
     * 
     * @throws Exception
     */
    @Test(enabled = false)
    public void testBadRegionNumber() throws Exception {

        // Sign on an operator who is authorized for put to store.
        initialize(PTS_SERIAL, PTS_OPERATOR_ID);
        //Try to sign into region 7777777
        Response response = executeCommand(this.getCmdDateSec(), PTS_SERIAL, PTS_OPERATOR_ID, "7777777");

        // Verify that we get 1 record returned
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"),
                String.valueOf(TaskErrorCode.INVALID_REGION_NUMBER.getErrorCode()), 
                "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"),
                "Requested region 7777777 is invalid.", 
                "Response returned incorrect ErrorMessage");
        
        //Test that a put to store labor record was opened for the correct region
        OperatorLabor operLabor = this.cmd.getLaborManager().getOperatorLaborManager()
            .findOpenRecordByOperatorId(this.cmd.getOperator().getId());
        
        // open labor record should be a sign on labor record
        if (operLabor == null) {
            fail("No Labor Record was created");
        } else {
            assertEquals(operLabor.getActionType(), OperatorLaborActionType.SignOn, 
                         "Invalid labor record type ");
        }
   }

    
    /**
     * This test will try to sign into region 43 with the SelectionOperator.
     * This will cause a not authorized error.
     * 
     * @throws Exception
     */
    @Test()
    public void testNotAuthorizedRegionNumber() throws Exception {
        //===============================================================
        // Sign on an operator who is NOT authorized for put to store.
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), PTS_SERIAL, SELECTION_OPERATOR_ID, 
            LOCALE.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), PTS_SERIAL, SELECTION_OPERATOR_ID});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), PTS_SERIAL, SELECTION_OPERATOR_ID, "1234"});

        //Note we are skipping cmdPrTaskLUTPtsValidRegions cause that would not allow this.
        //Try to sign into region 43 a Put To Store Region
        Response response = executeCommand(this.getCmdDateSec(), PTS_SERIAL, SELECTION_OPERATOR_ID, "43");

        // Verify that we get 1 record returned
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), 
                String.valueOf(TaskErrorCode.NOT_AUTHORIZED_FOR_REQUESTED_REGION.getErrorCode()), 
                "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"),
                "Work type not authorized in region 43 for " + SELECTION_OPERATOR_ID + ".", 
                "Response returned incorrect ErrorMessage");
        
        //Test that a put to store labor record was opened for the correct region
        OperatorLabor operLabor = this.cmd.getLaborManager().getOperatorLaborManager()
            .findOpenRecordByOperatorId(this.cmd.getOperator().getId());
        
        // open labor record should be a sign on labor record
        if (operLabor == null) {
            fail("No Labor Record was created");
        } else {
            assertEquals(operLabor.getActionType(), OperatorLaborActionType.SignOn, 
                         "Invalid labor record type ");
        }
    }

    
    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @param serial - the serial number
     * @param operator - the operator
     * @throws Exception - Exception
     */
    private void initialize(String serial, String operator) throws Exception {
        
        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, 
            LOCALE.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
        executeLutCmd("cmdPrTaskLUTPtsValidRegions",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
    }
    
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param sn - the serial number
     * @param operId - the operator ID
     * @param region - the region
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String sn, String operId, String region) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId, region}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
