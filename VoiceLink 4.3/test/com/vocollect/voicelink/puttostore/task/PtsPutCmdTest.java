/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */



package com.vocollect.voicelink.puttostore.task;

import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.model.PtsPut;
import com.vocollect.voicelink.puttostore.model.PtsPutDetail;
import com.vocollect.voicelink.puttostore.model.PtsPutDetailType;
import com.vocollect.voicelink.puttostore.model.PtsPutStatus;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Tests for pts put command.
 *
 * @author pfunyak
 *
 */


@Test(groups = { FAST, TASK, PUTTOSTORE })
public class PtsPutCmdTest extends TaskCommandTestCase  {

    private PtsPutCmd cmd;
    private String testOperId = "PtsPutCmdOper";
    private String testSerial = "PtsPutCmdSerial";

    /**
     * @return a bean for testing.
     */
    private PtsPutCmd getCmdBean() {
        return (PtsPutCmd)
            getBean("cmdPrTaskLUTPtsPut");
    }


    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        classSetupCore();
        // load the work groups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the operators needed for testing
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        // load the put to store regions
        classSetupPTSRegions();
        classSetupInsert("VoiceLinkDataUnitTest_PtsData.xml");
    }


    /**
     *
     *
     * @throws Exception
     */
    @Test()
    public void testPutCmdExceptions() throws Exception {
        Response response;
        List<ResponseRecord> records;

        String regionStr = "44";
        String licenseStr = "00000440";

        // Get to the proper state in the task using license 440.
        initialize(this.testSerial, this.testOperId);
        getRegion(this.testSerial, this.testOperId, regionStr);
        getFlowThroughLocation(this.testSerial, this.testOperId, regionStr);
        verifyLicense(this.testSerial, this.testOperId, licenseStr, "0");
        getAssignment(this.testSerial, this.testOperId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupId = (Long) groupResponse.getRecords().get(0).get("groupID");

        /**
         * Try a put using no container number to custloc -44004 which has no open containers.
         *     This should throw an exception
         **/
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId,
            groupId.toString(), "-44004", "-190", "-44004", "43",  "", "440", "0");

        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "1",  "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"),
                "No open container. Open a new container.",
                "Response returned incorrect ErrorMessage");

        /**
         * Try a put using no container number to custloc -44002 which has 2 open containers.
         *     This should throw an exception
         **/
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId,
            groupId.toString(), "-44002", "-190", "-44004", "43",  "", "440", "0");

        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "2", "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"),
                "Multiple open containers. Specify container.",
                "Response returned incorrect ErrorMessage");


        /**
         * Try a put using container number "ABC123" to custloc -44004 which has no open containers.
         *     This should throw an exception
         **/
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId,
            groupId.toString(), "-44004", "-190", "-44004", "43",  "ABC123", "440", "0");

        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "3", "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"),
                "Container ABC123 not found or is already closed.",
                "Response returned incorrect ErrorMessage");

        /**
         * Try a put using PARTIAL container number "123" to custloc -44005 which
         * has 2 open containers already with this spoken number.
         *     This should throw an exception
         **/
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId,
            groupId.toString(), "-44005", "-190", "-44004", "43",  "123", "440", "0");

        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "98", "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"),
                "Multiple containers open at location with spoken container "
                + "number 123. Please see your supervisor.",
                "Response returned incorrect ErrorMessage");

        /**
         * Try a put using container number 4400005 to custloc -44002 which has 2
         * open containers... HOWEVER, we will use a bogus PutId "-44009".
         *     This should throw error 1228 PTS_PUT_UPDATING_UNKNOWN_RECORD
         **/
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId,
            groupId.toString(), "-44002", "-190", "-44009", "43",  "4400005",
            "440", "0");

        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "98",  "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"),
                "Put information not found for -44009. Please see your supervisor.",
                "Response returned incorrect ErrorMessage");

        /**
         * Try a put using container number 4400005 to custloc -44002 which has 2
         * open containers... HOWEVER, we will use a PutId that is already
         * put "-44001".
         *     This should throw an exception
         **/
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId,
            groupId.toString(), "-44002", "-190", "-44001", "43",  "4400005",
            "440", "0");

        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "98", "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"),
                "Put was previously completed for put i d -44001. Please see your supervisor.",
                "Response returned incorrect ErrorMessage");

        /**
         * Try a put using container number 4400005 to custloc -44002 which has 2
         * open containers... HOWEVER, we will use a PutId that is shorted
         * "-44002".
         *     This should throw an exception
         **/
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId,
            groupId.toString(), "-44002", "-190", "-44001", "43",  "4400005",
            "440", "0");

        // Verify the response returned 1 record
        records = response.getRecords();
        assertEquals(records.size(), 1, "Response returned incorrect recordCount.");

        //Test error code & message
        assertEquals(records.get(0).get("errorCode"), "98", "Response returned incorrect ErrorCode");
        assertEquals(records.get(0).get("errorMessage"),
                "Put was previously completed for put i d -44001. Please see your supervisor.",
                "Response returned incorrect ErrorMessage");
     }


    /**
     *  This test will test various put scenarios to verify that the put
     *  status is being updated correctly.
     *
     *  The test will group two licenses
     *  License 441 contains 4 puts and 2 items (-190 and -212).
     *  There are 2 puts for item -190 and 2 puts for item -212.
     *  License 442 contains 6 puts and 2 items (-190 and -200).
     *  There are 2 puts for item -200 and 4 puts for item -190.
     *
     *  The test will do the following.
     *      1. Put the entire amount for the first record (license 442).
     *      2. Put the entire amount for the second record (item -190, license 442).
     *         This is needed to validate shorting an item.
     *      3. Partial the 3 record twice (license 441).
     *      4. Skip the fourth put (item -190, license 442). The skipped
     *         record is in the same license that contains the record
     *         from step 2.
     *      5. Short the fifth record (item -190, license 442)
     *         After shorting this record, all 4 puts for item -190 in license license 442,
     *         should be in a final status.
     *         1 put should have a status of put and the remaining puts for item -190
     *         should be shorted.
     *      6. The sixth put for item -217, license 441 is over packed.
     *
     * @throws Exception
     */
    @Test()
    public void testPutCmdGoodPuts() throws Exception {

        String regionStr = "44";
        initialize(this.testSerial, this.testOperId);
        getRegion(this.testSerial, this.testOperId, regionStr);
        getFlowThroughLocation(this.testSerial, this.testOperId, regionStr);
        verifyLicense(this.testSerial, this.testOperId, "00000441", "0");
        verifyLicense(this.testSerial, this.testOperId, "00000442", "0");
        getAssignment(this.testSerial, this.testOperId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupId = (Long) groupResponse.getRecords().get(0).get("groupID");
        getPuts(this.testSerial, this.testOperId, groupId.toString());
        Response getPutsResponse = getResponseForCommand("cmdPrTaskLUTPtsGetPuts");
        List<ResponseRecord> records = getPutsResponse.getRecords();
        assertEquals(records.size(), 10, "Did not get all records back");

        // Get the first put record and put the full quantity (item 200)
        this.putFirstRecord(groupId, records.get(0));
        // get the second put for Item 190 and put the full amount
        // Item 190 needs a completed put to verify shorting an item.
        this.putSecondRecord(groupId, records.get(1));
        // get the third put for Item 212 and partial the put.
        // the quantity to put is 7.  we will put 5 then 1 and finally 1 more
        this.partialThirdRecord(groupId, records.get(2));
        // get the fourth put record and skip it.
        this.skipFourthRecord(groupId, records.get(3));
        // Get the fifth put record and short it.
        // When this put gets shorted, the put that we skipped should
        // also be shorted and the put with id -44206 should be auto shorted.
        ResponseRecord putRecord = records.get(4);
        String custLoc = putRecord.get("locationID").toString();
        String itemNumber = putRecord.get("itemNumber").toString();
        Long putId = (Long) putRecord.get("putID");
        Integer quantityPut = (Integer) putRecord.get("quantityToPut");
        Integer shortQuantity = quantityPut - 10;
        String containerNumber = "4410005";
        String licenseNumber = putRecord.get("licenseNumber").toString();
        String partalFlag = "0";

        Response putResponse = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupId.toString(),
                custLoc, itemNumber, putId.toString(), shortQuantity.toString(), containerNumber,
                licenseNumber, partalFlag);
        ResponseRecord putResponseRecord  = putResponse.getRecords().get(0);
        assertEquals(putResponseRecord.get("errorCode"), "0", "Error Code was not zero");
        // verify that put was shorted
        PtsPut shortedPut = this.cmd.getPtsPutManager().get(putId);
        assertEquals(shortedPut.getStatus(), PtsPutStatus.Shorted);
        assertEquals(shortedPut.getQuantityPut(), shortQuantity, "quantity put for short is incorrect");
        assertEquals(shortedPut.getPutDetails().size(), 1, "no detail was created for auto shorted put");
        assertEquals(shortedPut.getPutDetailCount(), 1, "wrong put detail count for auto shorted put");
        // verify that the previously skipped put was shorted.
        PtsPut skippedPut = this.cmd.getPtsPutManager().get((Long) records.get(3).get("putID"));
        assertEquals(skippedPut.getQuantityPut(), new Integer(0), "skipped put was not shorted");
        assertEquals(skippedPut.getStatus(), PtsPutStatus.Shorted, "wrong put status for skipped put");
        assertEquals(skippedPut.getPutDetails().size(), 1, "no detail was created for skipped put");
        assertEquals(skippedPut.getPutDetailCount(), 1, "wrong put detail count for skipped put");
        // verify that remaining put for item 190 was shorted.
        PtsPut autoShortedPut = this.cmd.getPtsPutManager().get(-44206L);
        assertEquals(autoShortedPut.getQuantityPut(), new Integer(0), "auto shorted put was not shorted.");
        assertEquals(autoShortedPut.getStatus(), PtsPutStatus.Shorted, "wrong put status for auto shorted put");
        assertEquals(autoShortedPut.getPutDetails().size(), 1, "no detail was created for auto shorted put");
        assertEquals(autoShortedPut.getPutDetailCount(), 1, "wrong put detail count for auto shorted put");
        // verify that nothing of importance changed on the previously completed put.
        PtsPut completedPut = this.cmd.getPtsPutManager().get((Long) records.get(1).get("putID"));
        assertEquals(completedPut.getQuantityPut(), completedPut.getQuantityToPut(),
                    "wrong quantity put for completed put.");
        assertEquals(completedPut.getStatus(), PtsPutStatus.Put,
                    "wrong put status for completed put after shorted put");
        assertEquals(completedPut.getPutDetails().size(), 1, "extra details were created for completed put");
        assertEquals(completedPut.getPutDetailCount(), 1, "wrong put detail count for completed put");

        // get the sixth put record and over pack it.
        // we should create 2 detail records.
        putRecord = records.get(5);
        custLoc = putRecord.get("locationID").toString();
        itemNumber = putRecord.get("itemNumber").toString();
        putId = (Long) putRecord.get("putID");
        quantityPut = (Integer) putRecord.get("quantityToPut");
        Integer overPackQuantity = quantityPut + 5;
        Integer newResidualQuantity = (Integer) putRecord.get("residualQuantity") - 5;
        containerNumber = "4410002";
        licenseNumber = putRecord.get("licenseNumber").toString();
        partalFlag = "0";

        putResponse = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupId.toString(),
                custLoc, itemNumber, putId.toString(), overPackQuantity.toString(), containerNumber,
                licenseNumber, partalFlag);
        putResponseRecord  = putResponse.getRecords().get(0);
        assertEquals(putResponseRecord.get("errorCode"), "0", "Error Code was not zero");
        PtsPut put = this.cmd.getPtsPutManager().get(putId);
        assertEquals(put.getStatus(), PtsPutStatus.Put);
        assertEquals(put.getQuantityPut(), overPackQuantity, "quantity put for overpack is incorrect");
        // verify that we created two detail records.
        assertEquals(put.getPutDetailCount(), 2, "incorrect put detail count");
        // verify that residual quantity was updated.
        assertEquals(put.getResidualQuantity(), newResidualQuantity, "residual quantity is incorrect");
        // Get the other put for item -190 in license 441 and verify that
        // the residual quantity has be updated.
        put = this.cmd.getPtsPutManager().get(-44102L);
        assertEquals(put.getResidualQuantity(), newResidualQuantity, "residual quantity is incorrect");
    }



  /**
   * This method validates that we can successfully complete a put.
   *
   * @param groupId - the group ID
   * @param putRecord - the put record
   * @throws Exception
   */
    private void putFirstRecord(Long groupId, ResponseRecord putRecord) throws Exception {

        String custLoc = putRecord.get("locationID").toString();
        String itemNumber = putRecord.get("itemNumber").toString();
        Long putId = (Long) putRecord.get("putID");
        Integer quantityPut = (Integer) putRecord.get("quantityToPut");
        String containerNumber = "4410004";
        String licenseNumber = putRecord.get("licenseNumber").toString();
        String partalFlag = "0";

        Response putResponse = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupId.toString(),
                custLoc, itemNumber, putId.toString(), quantityPut.toString(), containerNumber,
                licenseNumber, partalFlag);

        // validate Response fields
        this.validateCommandFields(putResponse);

        ResponseRecord putResponseRecord  = putResponse.getRecords().get(0);
        assertEquals(putResponseRecord.get("errorCode"), "0", "Error Code was not zero");
        PtsPut put = this.cmd.getPtsPutManager().get(putId);

        // Verify that the put was updated correctly.
        assertEquals(put.getQuantityPut(), quantityPut, "wrong quantity put");
        assertNotNull(put.getPutTime(), "put time should not be null");
        assertEquals(put.getPutDetailCount(), 1, "wrong detail count");
        assertEquals(put.getStatus(), PtsPutStatus.Put, "wrong put status");
        assertEquals(put.getOperator().getOperatorIdentifier(), testOperId, "wrong operator on put");
        // validate that detail was added to container
        PtsContainer container = this.cmd.getPtsContainerManager().findPtsContainerByNumber("4410004");
        assertEquals(container.getContainerDetails().size(), 1, "wrong detail count for container.");
        // validate that the detail record was created correctly
        List <PtsPutDetail> detailList = put.getPutDetails();
        PtsPutDetail detail = detailList.get(0);
        assertEquals(detail.getContainerNumber(), container.getContainerNumber(), "wrong container on put detail");
        assertEquals(detail.getPut().getId(), put.getId(), "wrong put id on put detail");
        assertEquals(detail.getType(), PtsPutDetailType.TaskPut, "wrong detail type on put detail");
        assertEquals(detail.getOperator().getOperatorIdentifier(), testOperId, "wrong operator on put detail");
        assertNotNull(detail.getPutTime(), "put time should not be null on put detail");
        assertEquals(detail.getQuantityPut(), quantityPut, "wrong quantity put on put detail");
        assertEquals(detail.getExportStatus(), ExportStatus.NotExported, "wrong export status");
        assertNotNull(detail.getCreatedDate(), "created date should not be null on put detail");
    }

    /**
     * This method puts a record and does minimum validation.
     *
     * @param groupId - the ID of the group
     * @param putRecord - the put record
     * @throws Exception
     */
      private void putSecondRecord(Long groupId, ResponseRecord putRecord) throws Exception {

          String custLoc = putRecord.get("locationID").toString();
          String itemNumber = putRecord.get("itemNumber").toString();
          Long putId = (Long) putRecord.get("putID");
          Integer quantityPut = (Integer) putRecord.get("quantityToPut");
          String containerNumber = "4410004";
          String licenseNumber = putRecord.get("licenseNumber").toString();
          String partalFlag = "0";

          Response putResponse =  executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupId.toString(),
                  custLoc, itemNumber, putId.toString(), quantityPut.toString(), containerNumber,
                  licenseNumber, partalFlag);
          ResponseRecord putResponseRecord  = putResponse.getRecords().get(0);
          assertEquals(putResponseRecord.get("errorCode"), "0", "Error Code was not zero for second put");
          // validate that detail records were added to the put and container
          PtsPut put = this.cmd.getPtsPutManager().get(putId);
          assertEquals(put.getPutDetailCount(), 1, "wrong detail count");
          PtsContainer container = this.cmd.getPtsContainerManager().findPtsContainerByNumber("4410004");
          assertEquals(container.getContainerDetails().size(), 2, "wrong detail count for container.");
      }

      /**
       *  Partial the third put record for Item 212.
       *  The quantity to put is 7.
       *  We will partial the put by putting 5 then 1 and finally 1 more
       *
       * @param groupId - the ID of the group
       * @param putRecord - the put record
       * @throws Exception
       */
        private void partialThirdRecord(Long groupId, ResponseRecord putRecord) throws Exception {

            String custLoc = putRecord.get("locationID").toString();
            String itemNumber = putRecord.get("itemNumber").toString();
            Long putId = (Long) putRecord.get("putID");
            Integer partialQuantityPut = 5;
            String containerNumber = "4410003";
            String licenseNumber = putRecord.get("licenseNumber").toString();
            String partalFlag = "1";

            Response putResponse = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupId.toString(),
                    custLoc, itemNumber, putId.toString(), partialQuantityPut.toString(), containerNumber,
                    licenseNumber, partalFlag);
            ResponseRecord putResponseRecord  = putResponse.getRecords().get(0);
            assertEquals(putResponseRecord.get("errorCode"), "0", "Error Code was not zero");
            // validate that detail records were added to the put and container
            PtsContainer container = this.cmd.getPtsContainerManager().findPtsContainerByNumber("4410003");
            assertEquals(container.getContainerDetails().size(), 1, "wrong detail count for container.");
            PtsPut put = this.cmd.getPtsPutManager().get(putId);
            assertEquals(put.getStatus(), PtsPutStatus.Partial, "wrong put status");
            assertEquals(put.getQuantityPut(), partialQuantityPut, "wrong partial quantity put");
            assertNotNull(put.getOperator(), "operator should not be null");
            assertNotNull(put.getPutTime(), "put time should not be null");
            assertEquals(put.getPutDetailCount(), 1, "wrong detail count");

            partialQuantityPut = 1;
            // Partial it again
            putResponse = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupId.toString(),
                    custLoc, itemNumber, putId.toString(), partialQuantityPut.toString(), containerNumber,
                    licenseNumber, partalFlag);
            putResponseRecord  = putResponse.getRecords().get(0);
            assertEquals(putResponseRecord.get("errorCode"), "0", "Error Code was not zero");
            // validate that detail records were added to the put and container
            container = this.cmd.getPtsContainerManager().findPtsContainerByNumber("4410003");
            assertEquals(container.getContainerDetails().size(), 2, "wrong detail count for container.");
            put = this.cmd.getPtsPutManager().get(putId);
            assertEquals(put.getStatus(), PtsPutStatus.Partial, "wrong put status");
            assertEquals(put.getQuantityPut(), new Integer(6), "wrong quantity put");
            assertNotNull(put.getOperator(), "operator should not be null");
            assertNotNull(put.getPutTime(), "put time should not be null");
            assertEquals(put.getPutDetailCount(), 2, "wrong detail count");

            // Finish the partial
            partalFlag = "0";
            putResponse = executeCommand(this.getCmdDateSec(), testSerial, testOperId, groupId.toString(),
                    custLoc, itemNumber, putId.toString(), partialQuantityPut.toString(), containerNumber,
                    licenseNumber, partalFlag);
            putResponseRecord  = putResponse.getRecords().get(0);
            assertEquals(putResponseRecord.get("errorCode"), "0", "Error Code was not zero");
            // validate that detail records were added to the put and container
            container = this.cmd.getPtsContainerManager().findPtsContainerByNumber("4410003");
            assertEquals(container.getContainerDetails().size(), 3, "wrong detail count for container.");
            put = this.cmd.getPtsPutManager().get(putId);
            assertEquals(put.getStatus(), PtsPutStatus.Put, "wrong put status");
            assertEquals(put.getQuantityPut(), put.getQuantityToPut(), "wrong quantity put");
            assertNotNull(put.getOperator(), "operator should not be null");
            assertNotNull(put.getPutTime(), "put time should not be null");
            assertEquals(put.getPutDetailCount(), 3, "wrong detail count");
        }

        /**
         * This method skips a put and verifies that the put was skipped.
         *
         * @param groupId - the ID of the group
         * @param putRecord - the put record
         * @throws Exception
         */
          private void skipFourthRecord(Long groupId, ResponseRecord putRecord) throws Exception {

              String custLoc = putRecord.get("locationID").toString();
              Long putId = (Long) putRecord.get("putID");
              this.skipPut(testSerial, testOperId, groupId.toString(), custLoc, "0", "S");
              PtsPut put = this.cmd.getPtsPutManager().get(putId);
              assertEquals(put.getStatus(), PtsPutStatus.Skipped);

         }

    /**
     *
     * @param serial - the serial number
     * @param operator - the operator
     * @param groupNumber - the group number
     * @param locationId - the location ID
     * @param slotAisle - the slot aisle combination
     * @param status - the current status
     * @throws Exception
     */

    private void skipPut(String serial, String operator, String groupNumber,
                          String locationId, String slotAisle, String status) throws Exception {

        executeOdrCmd("cmdPrTaskODRPtsUpdateStatus",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
                                                 groupNumber, locationId, slotAisle, status});
    }


    /**
     * Gets the region to sign into.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param region - the region to sign into.
     *
     * @throws Exception
     */
    private void getRegion(String serial, String operator, String region) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsValidRegions",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTPtsGetRegionConfiguration",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, region});
    }


    /**
     * Gets the flow through location.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param region - the region to get flow through location.
     *
     * @throws Exception
     */
    private void getFlowThroughLocation(String serial, String operator, String region) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsGetFTLocation",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, region});
    }

    /**
     * reserve a license.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param licenseNumber - the license number to reserve.
     * @param partialFlag - 1 means this is a partial license number
     *
     * @throws Exception
     */
    private void verifyLicense(String serial, String operator,
                               String licenseNumber, String partialFlag)
    throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsVerifyLicense",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
                                licenseNumber, partialFlag});
    }


    /**
     * get the assignment.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     *
     * @throws Exception
     */
    private void getAssignment(String serial, String operator) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsGetAssignment",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
    }

    /**
     * get the puts.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param groupNumber - the group number
     *
     * @throws Exception
     */
    private void getPuts(String serial, String operator, String groupNumber) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsGetPuts",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, groupNumber});
    }


    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     *
     * @throws Exception
     */
    private void initialize(String serial, String operator) throws Exception {
        final Locale locale = Locale.US;

        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
    }


    /**
     * Execute a command.
     *
     * @param cmdDate - date for command
     * @param serialNum - the serial number
     * @param opId - the operator ID
     * @param groupId - the group ID
     * @param custLocId - the customer location ID
     * @param item - the item
     * @param putId - the put ID
     * @param qtyPut - the quantity that was put
     * @param container - the container
     * @param licenseNum - the license number
     * @param partial - the partial string
     *
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String serialNum, String opId,
                                    String groupId, String custLocId, String item,
                                    String putId, String qtyPut, String container,
                                    String licenseNum, String partial) throws Exception {
        this.cmd = getCmdBean();
        // call the command
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), serialNum, opId, groupId,
                custLocId, item, putId, qtyPut, container, licenseNum, partial}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}

