/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * This class tests Pts Stop Assignment Command.
 *
 * @author svoruganti
 */
@Test(groups = { FAST, TASK, PUTTOSTORE })
public class PtsStopAssignmentCmdTest extends TaskCommandTestCase {

    private PtsStopAssignmentCmd cmd;
    private String operatorId = "PtsStopAssignmentOper";
    private String serialId = "PtsStopAssignmentSerial";

    /**
     * @return a bean for testing.
     */
    private PtsStopAssignmentCmd getCmdBean() {
        return (PtsStopAssignmentCmd)
            getBean("cmdPrTaskLUTPtsStopAssignment");
    }

   /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }


    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        classSetupCore();
        // load the work groups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the operators needed for testing
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        // load the put to store regions
        classSetupPTSRegions();
        classSetupInsert("VoiceLinkDataUnitTest_PtsData.xml");
    }

  /**
   * This test verifies stop assignment for license with puts having status of PUT.
   * @throws Exception
   */
    @Test()
    public void testStopAssignmentAllPut() throws Exception {

        initialize(this.serialId, this.operatorId);

        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000300", "0");
        getAssignment(this.serialId, this.operatorId);

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        Map<String, Object> record = response.getRecords().get(0);

        //Verify Successful result
        assertEquals(record.get("errorCode"), "0", "ErrorCode");
        boolean areLicensesComplete = areLicensesComplete(this.cmd.getLicenses());
        assertEquals(areLicensesComplete, true, "license complete");
        boolean hasEndTime = this.hasEndTime(this.cmd.getLicenses());
        assertEquals(hasEndTime, true, "has end time");
        boolean hasGroup = this.areLicensesUngrouped(this.cmd.getLicenses());
        assertEquals(hasGroup, true, "has group");
    }


    /**
     * This test verifies stop assignment for license with puts having status
     *  of PUT and SHORT.
     * @throws Exception
     */
    @Test()
    public void testStopAssignmentPutShort() throws Exception {

        initialize(this.serialId, this.operatorId);

        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000301", "0");
        getAssignment(this.serialId, this.operatorId);

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");

        //Verify Successful result
        for (PtsLicense l : this.cmd.getLicenses()) {
             assertEquals(l.getStatus(), PtsLicenseStatus.Short, "Shorted");
        }
        assertEquals(areLicensesComplete(this.cmd.getLicenses()), false, "license not complete");
        assertEquals(this.hasEndTime(this.cmd.getLicenses()), true, "has end time");
        assertEquals(this.areLicensesUngrouped(this.cmd.getLicenses()), true, "has group");

    }

    /**
     * This test verifies stop assignment for license with puts having status of
     * PUT, NOT PUT, SHORTED, SKIPPED,PARTIAL.
     * @throws Exception
     */
    @Test()
    public void testStopAssignmentAllStatus() throws Exception {

        initialize(this.serialId, this.operatorId);

        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000302", "0");
        getAssignment(this.serialId, this.operatorId);

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        Map<String, Object> record = response.getRecords().get(0);

        //Verify Successful result
        assertEquals(record.get("errorCode"), "98", "ErrorCode");
        for (PtsLicense l : this.cmd.getLicenses()) {
            assertEquals(l.getStatus(), PtsLicenseStatus.InProgress, "In progress");
       }
        boolean areLicensesComplete = areLicensesComplete(this.cmd.getLicenses());
        assertEquals(areLicensesComplete, false, "license not complete");
        boolean hasEndTime = this.hasEndTime(this.cmd.getLicenses());
        assertEquals(hasEndTime, false, "has end time");
        boolean hasGroup = this.areLicensesUngrouped(this.cmd.getLicenses());
        assertEquals(hasGroup, false, "has group");
    }

    /**
     * This test verifies stop assignment for license with puts having status of
     * PUT, NOT PUT.
     * @throws Exception
     */
    @Test()
    public void testStopAssignmentPutNotPut() throws Exception {

        initialize(this.serialId, this.operatorId);

        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000303", "0");
        getAssignment(this.serialId, this.operatorId);

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        Map<String, Object> record = response.getRecords().get(0);

        //Verify Successful result
        assertEquals(record.get("errorCode"), "98", "ErrorCode");

        for (PtsLicense l : this.cmd.getLicenses()) {
            assertEquals(l.getStatus(), PtsLicenseStatus.InProgress, "In progress");
        }
        boolean areLicensesComplete = areLicensesComplete(this.cmd.getLicenses());
        assertEquals(areLicensesComplete, false, "license not complete");
        boolean hasEndTime = this.hasEndTime(this.cmd.getLicenses());
        assertEquals(hasEndTime, false, "has end time");
        boolean hasGroup = this.areLicensesUngrouped(this.cmd.getLicenses());
        assertEquals(hasGroup, false, "has group");
    }

    /**
     * This test verifies stop assignment for license with puts having status of
     * PUT, SKIPPED.
     * @throws Exception
     */
    @Test()
    public void testStopAssignmentPutSkipped() throws Exception {

        initialize(this.serialId, this.operatorId);

        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000304", "0");
        getAssignment(this.serialId, this.operatorId);

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        Map<String, Object> record = response.getRecords().get(0);

        //Verify Successful result
        assertEquals(record.get("errorCode"), "98", "ErrorCode");

        for (PtsLicense l : this.cmd.getLicenses()) {
            assertEquals(l.getStatus(), PtsLicenseStatus.InProgress, "In progress");
        }
        boolean areLicensesComplete = areLicensesComplete(this.cmd.getLicenses());
        assertEquals(areLicensesComplete, false, "license not complete");
        boolean hasEndTime = this.hasEndTime(this.cmd.getLicenses());
        assertEquals(hasEndTime, false, "has end time");
        boolean hasGroup = this.areLicensesUngrouped(this.cmd.getLicenses());
        assertEquals(hasGroup, false, "has group");
    }

    /**
     * This test verifies stop assignment for license with puts having status of
     * PUT, PARTIAL.
     * @throws Exception
     */
    @Test()
    public void testStopAssignmentPutPartial() throws Exception {

        initialize(this.serialId, this.operatorId);

        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000305", "0");
        getAssignment(this.serialId, this.operatorId);

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        Map<String, Object> record = response.getRecords().get(0);

        //Verify Successful result
        assertEquals(record.get("errorCode"), "98", "ErrorCode");

        for (PtsLicense l : this.cmd.getLicenses()) {
            assertEquals(l.getStatus(), PtsLicenseStatus.InProgress, "In progress");
        }
        boolean areLicensesComplete = areLicensesComplete(this.cmd.getLicenses());
        assertEquals(areLicensesComplete, false, "license not complete");
        boolean hasEndTime = this.hasEndTime(this.cmd.getLicenses());
        assertEquals(hasEndTime, false, "has end time");
        boolean hasGroup = this.areLicensesUngrouped(this.cmd.getLicenses());
        assertEquals(hasGroup, false, "has group");
    }

    /**
     * This test verifies stop assignment for license with puts having status of
     * PUT, PARTIAL.
     * @throws Exception
     */
    @Test()
    public void testStopAssignmentGroupAssignments() throws Exception {

        initialize(this.serialId, this.operatorId);

        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000306", "0");
        verifyLicense(this.serialId, this.operatorId, "00000307", "0");
        verifyLicense(this.serialId, this.operatorId, "00000308", "0");
        verifyLicense(this.serialId, this.operatorId, "00000309", "0");
        getAssignment(this.serialId, this.operatorId);

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        Map<String, Object> record = response.getRecords().get(0);

        //Verify Successful result
        assertEquals(record.get("errorCode"), "98", "ErrorCode");

        PtsLicense pl = this.cmd.getPtsLicenseManager().get(new Long(-306));
        assertEquals(pl.getStatus(), PtsLicenseStatus.InProgress, "In progress");

        PtsLicense pl1 = this.cmd.getPtsLicenseManager().get(new Long(-307));
        assertEquals(pl1.getStatus(), PtsLicenseStatus.InProgress, "In progress");

        PtsLicense pl2 = this.cmd.getPtsLicenseManager().get(new Long(-308));
        assertEquals(pl2.getStatus(), PtsLicenseStatus.InProgress, "In progress");

        PtsLicense pl3 = this.cmd.getPtsLicenseManager().get(new Long(-309));
        assertEquals(pl3.getStatus(), PtsLicenseStatus.InProgress, "In progress");

        assertEquals(areLicensesComplete(this.cmd.getLicenses()), false, "license not complete");
        assertEquals(this.hasEndTime(this.cmd.getLicenses()), false, "has end time");
        assertEquals(this.areLicensesUngrouped(this.cmd.getLicenses()), false, "has group");
    }

    /**
     * This test verifies pass assignment succeeds.
     * @throws Exception
     */
    @Test()
    public void testPassAssignmentGroupAssignmentsComplete() throws Exception {

        initialize(this.serialId, this.operatorId);

        getRegion(this.serialId, this.operatorId, "44");
        getFlowThroughLocation(this.serialId, this.operatorId, "44");
        verifyLicense(this.serialId, this.operatorId, "00000314", "0");
        verifyLicense(this.serialId, this.operatorId, "00000315", "0");
        verifyLicense(this.serialId, this.operatorId, "00000316", "0");
        verifyLicense(this.serialId, this.operatorId, "00000317", "0");
        getAssignment(this.serialId, this.operatorId);


        Response groupResponse = getResponseForCommand("cmdPrTaskLUTPtsGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
         //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        Map<String, Object> record = response.getRecords().get(0);

        //Verify Failed result
        assertEquals(record.get("errorCode"), "0", "ErrorCode");

        PtsLicense pl = this.cmd.getPtsLicenseManager().get(new Long(-314));
        assertEquals(pl.getStatus(), PtsLicenseStatus.Short, "Shorted");

        PtsLicense pl1 = this.cmd.getPtsLicenseManager().get(new Long(-315));
        assertEquals(pl1.getStatus(), PtsLicenseStatus.Short, "Shorted");

        PtsLicense pl2 = this.cmd.getPtsLicenseManager().get(new Long(-316));
        assertEquals(pl2.getStatus(), PtsLicenseStatus.Complete, "Complete");

        PtsLicense pl3 = this.cmd.getPtsLicenseManager().get(new Long(-317));
        assertEquals(pl3.getStatus(), PtsLicenseStatus.Complete, "Complete");

        assertEquals(areLicensesComplete(this.cmd.getLicenses()), false, "license not complete");
        assertEquals(this.hasEndTime(this.cmd.getLicenses()), true, "has end time");
        assertEquals(this.areLicensesUngrouped(this.cmd.getLicenses()), true, "has group");
     }

    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     *
     * @throws Exception
     */
    private void initialize(String serial, String operator) throws Exception {
        final Locale locale = Locale.US;

        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
    }

    /**
     * Gets the region to sign into.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param region - the region to sign into.
     *
     * @throws Exception
     */
    private void getRegion(String serial, String operator, String region) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsValidRegions",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTPtsGetRegionConfiguration",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, region});
    }


    /**
     * Gets the flow through location.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param region - the region to get flow through location.
     *
     * @throws Exception
     */
    private void getFlowThroughLocation(String serial, String operator, String region) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsGetFTLocation",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, region});
    }

    /**
     * reserve a license.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param licenseNumber - the license number to reserve.
     * @param partialFlag - 1 means this is a partial license number
     *
     * @throws Exception
     */
    private void verifyLicense(String serial, String operator,
                               String licenseNumber, String partialFlag) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsVerifyLicense",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
                                licenseNumber, partialFlag});
    }


    /**
     * get the assignment.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command

     *
     * @throws Exception
     */
    private void getAssignment(String serial, String operator) throws Exception {

        executeLutCmd("cmdPrTaskLUTPtsGetAssignment",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
    }

    /**
     * Execute a command.
     *
     * @param cmdDate - date for command
     * @param groupNumber - group number to stop
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String groupNumber) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDateMS(cmdDate), serialId, operatorId,
                groupNumber}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }

    /**
     * This method checks if endtime is set for a license or not.
     *
     * @param licenses the list of licenses
     * @return boolean - true or false
     */
    private boolean hasEndTime(List<PtsLicense> licenses) {
        for (PtsLicense l : licenses) {
            if (l.getEndTime() ==  null) {
               return false;
           }
        }
           return true;
       }

    /**
     * This method checks if endtime is set for a license or not.
     *
     * @param licenses - the list of licenses
     * @return boolean - true or false
     */
    private boolean areLicensesUngrouped(List<PtsLicense> licenses) {
        for (PtsLicense l : licenses) {
         if (l.getGroupInfo().getGroupNumber() !=  null) {
            return false;
        }
     }
        return true;
    }


    /**
     * This method checks if endtime is set for a license or not.
     *
     * @param licenses - the list of licenses
     * @return boolean - true or false
     */
    private boolean areLicensesComplete(List<PtsLicense> licenses) {
        for (PtsLicense l : licenses) {
         if (l.getStatus() !=  PtsLicenseStatus.Complete) {
            return false;
        }
     }
        return true;
    }
}
