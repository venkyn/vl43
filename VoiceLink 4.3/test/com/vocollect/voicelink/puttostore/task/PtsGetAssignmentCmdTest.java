/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.puttostore.task;



import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.OperatorFunctionLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLabor;
import com.vocollect.voicelink.puttostore.model.PtsLicenseStatus;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Tests for PTSGetAssignmentCmd.
 *
 * @author estoll
 *
 */

@Test(groups = { FAST, TASK, PUTTOSTORE })
public class PtsGetAssignmentCmdTest  extends TaskCommandTestCase  {

    private PtsGetAssignmentCmd cmd;
    private PtsVerifyLicenseCmd verifyLicenseCmd;
    private String testOperId = "PTSGetAssignmentCmdOper";
    private String testSerial = "PTSGetAssignmentCmdSerial";

    /**
     * @return a bean for testing.
     */
    private PtsGetAssignmentCmd getCmdBean() {
        return (PtsGetAssignmentCmd)
            getBean("cmdPrTaskLUTPtsGetAssignment");
    }


    /**
     * @return PtsVerifyLicenseCmd
     */
    private PtsVerifyLicenseCmd getVLCmdBean() {
        return (PtsVerifyLicenseCmd)
            getBean("cmdPrTaskLUTPtsVerifyLicense");
    }


    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        classSetupCore();
        // load the workgroups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the operators needed for testing
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        // load the put to store regions
        classSetupPTSRegions();
        classSetupInsert("VoiceLinkDataUnitTest_PtsData.xml");
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testGetGroup() throws Exception {
        /* This test uses a region defined as full license
         * numbers - we will request a license
         * that should be found
         *
         * Region 44 is defined to have a max of 4 licenses in a group
         *
         * This test reserves 4 licenses and then gets them.  The licenses
         * have status' of Available, Passed and Suspended.
         *
         * Verify the fields of the GetAssignment are correct and that the
         * licenses are grouped properly.
         */

        // Get to the proper state in the task.
        initialize(this.testSerial, this.testOperId, "44");

        String licenseNumber = "00051010";
        reserveLicense(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "0");

        // Request second good license
        licenseNumber = "00051009";
        reserveLicense(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "0");

        // Request third good license -- this one is suspended
        licenseNumber = "67891000";
        reserveLicense(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "0");

        // Request fourth good license -- this one is passed
        licenseNumber = "78901000";
        reserveLicense(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "0");

        // Get the assignment
        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId);
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(record.getErrorCode(), "0", "Get Group assignment failed");

        // verify residual location values (fields from region record)
        assertEquals(record.get("unExpReturnLocation"),
            "unexpected return location", "unExpReturnLocation FAILED");
        assertEquals(record.get("unExpReturnLocationCD"),
            "22", "unExpReturnLocationCD FAILED");
        assertEquals(record.get("returnLocation"),
            "residual return location", "returnLocation FAILED");
        assertEquals(record.get("returnLocationCD"),
            "11", "returnLocationCD FAILED");

        // verify performance values
        assertEquals(record.get("performanceLast"), "-1", "performanceLast FAILED");
        assertEquals(record.get("performanceDaily"), "-1", "performanceDaily FAILED");

        // verify the appropriate fields of each license have been updated...
        Long group = (Long) record.get("groupID");
        List<PtsLicense> p = this.cmd.getPtsLicenseManager().listLicensesInGroup(group);

        // Verify we got 4 in the group
        assertEquals(p.size(), 4, "Number of Licenses in group does not equal 4");

        for (PtsLicense ptsLicense : p) {
            assertEquals(ptsLicense.getGroupInfo().getGroupNumber(), group, "Group number is wrong");
            assertNull(ptsLicense.getReservedBy(), "ReservedBy is not null");
            assertNotNull(ptsLicense.getStartTime(), "Starttime is not null");
            assertEquals(ptsLicense.getStatus(), PtsLicenseStatus.InProgress, "Status is not inprogress");
            assertEquals(ptsLicense.getOperator().getOperatorIdentifier(), this.testOperId);
        }
        //Test that a put to store labor record was opened for the correct region
        OperatorFunctionLabor operLabor =
            (OperatorFunctionLabor) this.cmd.getLaborManager().getOperatorLaborManager()
            .findOpenRecordByOperatorId(this.cmd.getOperator().getId());
        if (operLabor == null) {
            fail("PutToStore Labor Record was not created");
        } else {
            assertEquals(operLabor.getActionType(), OperatorLaborActionType.PutToStore,
                         "Invalid labor record type ");
        }
        // verify that 4 license labor records were opened.
        List <PtsLicenseLabor> lLabor = this.cmd.getLaborManager().getPtsLicenseLaborManager()
                                            .listOpenRecordsByOperatorId(this.cmd.getOperator().getId());
        // verify interesting properties.
        assertEquals(lLabor.size(), 4, "should have 4 open license labor records");
        assertNull(lLabor.get(0).getEndTime(), "endtime should be null for record 0");
        assertNull(lLabor.get(1).getEndTime(), "endtime should be null for record 1");
        assertNull(lLabor.get(2).getEndTime(), "endtime should be null for record 2");
        assertNull(lLabor.get(3).getEndTime(), "endtime should be null for record 3");
        assertEquals(lLabor.get(3).getGroupCount().intValue(), 4, "group count should be 4");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testResidualValues() throws Exception {
        /*
         * This test signs into region 42 and attempts to get
         * a grouped assignment so we can verify the number
         * of items, total slots, and expected residual values
         *
         * This grouped assignment is defined as described in
         * TestLink 13548
         */
        initialize(this.testSerial, this.testOperId, "42");
        reserveLicense(this.getCmdDateSec(), testSerial, testOperId, "01001", "1");
        reserveLicense(this.getCmdDateSec(), testSerial, testOperId, "01003", "1");
        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId);
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        // validate command fields
        this.validateCommandFields(response);
        assertEquals(record.getErrorCode(), "0", "Get Group assignment failed");

        // Test total items
        assertEquals(record.get("totalItems").toString(), "3",
            "Total Items FAILED");
        assertEquals(record.get("totalSlots").toString(), "3",
            "Total Slots FAILED");
        assertEquals(record.get("totalExpectedResidual").toString(), "5",
            "Total expected residual FAILED");

        //Test that a put to store labor record was opened for the correct region
        OperatorFunctionLabor operLabor =
            (OperatorFunctionLabor) this.cmd.getLaborManager().getOperatorLaborManager()
            .findOpenRecordByOperatorId(this.cmd.getOperator().getId());

        if (operLabor == null) {
            fail("PutToStore Labor Record was not created");
        } else {
            assertEquals(OperatorLaborActionType.PutToStore, operLabor.getActionType(),
                         "Invalid labor record type ");
        }
        // verify that two license labor records were opened.
        List <PtsLicenseLabor> lLabor = this.cmd.getLaborManager().getPtsLicenseLaborManager()
                                            .listOpenRecordsByOperatorId(this.cmd.getOperator().getId());
        // verify interesting properties.
        assertEquals(lLabor.size(), 2, "should have two open license labor records");
        assertNull(lLabor.get(0).getEndTime(), "endtime should be null for record 0");
        assertNull(lLabor.get(1).getEndTime(), "endtime should be null for record 1");
        assertEquals(lLabor.get(0).getGroupCount().intValue(), 2, "group count should be 2");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testNoPuts() throws Exception {
        /*
         * This test signs into region 42 and attempts to get
         * a single license with zero put records
         */
        // Get to the proper state in the task.
        initialize(this.testSerial, this.testOperId, "42");

        String licenseNumber = "51001";
        reserveLicense(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "1");

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId);
        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(record.getErrorCode(), "0", "Get Group assignment failed");

        // Test for zero values
        assertEquals(record.get("totalItems").toString(), "0",
            "Zero Total Items FAILED");
        assertEquals(record.get("totalSlots").toString(), "0",
            "Zero Total Slots FAILED");
        assertEquals(record.get("totalExpectedResidual").toString(), "0",
            "Zero Total expected residual FAILED");

        //Test that a put to store labor record was opened for the correct region
        OperatorFunctionLabor operLabor =
            (OperatorFunctionLabor) this.cmd.getLaborManager().getOperatorLaborManager()
            .findOpenRecordByOperatorId(this.cmd.getOperator().getId());

        if (operLabor == null) {
            fail("PutToStore Labor Record was not created");
        } else {
            assertEquals(operLabor.getActionType(), OperatorLaborActionType.PutToStore,
                         "Invalid labor record type ");
            int regionNumber = operLabor.getRegion().getNumber();
            assertEquals(regionNumber, 42, "Invalid region id in labor record");
            assertNull(operLabor.getEndTime());
        }
        // verify that only one license labor record was opened.
        List <PtsLicenseLabor> lLabor = this.cmd.getLaborManager().getPtsLicenseLaborManager()
                                            .listOpenRecordsByOperatorId(this.cmd.getOperator().getId());

        assertEquals(lLabor.size(), 1, "should only have one open license labor record");
        // verify that the data in the license labor record is correct.
        PtsLicenseLabor lRec = lLabor.get(0);
        assertNotNull(lRec.getStartTime(), "startTime should not be null");
        assertNull(lRec.getEndTime(), "endTime should be null");
        assertEquals(lRec.getActualRate(), 0.0, "rate should be zero");
        assertNull(lRec.getBreakLaborId(), "break id should be null");
        assertEquals(lRec.getDuration().longValue(), 0, "Duration should be zero");
        assertEquals(lRec.getExportStatus(), ExportStatus.NotExported, "export status should be not exported");
        assertEquals(lRec.getGroupCount().intValue(), 1, "groupCount should be 1");
        assertEquals(lRec.getGroupProrateCount().intValue(), 0, "groupProrateCount should be zero");
        assertEquals(lRec.getLicenseProrateCount().intValue(), 0, "licenseProrateCount should be zero");
        assertEquals(lRec.getPercentOfGoal().doubleValue(), 0.0, "percent of goal should be zero");
        assertEquals(lRec.getQuantityPut().intValue(), 0, "quantity put should be zero");
        assertEquals(lRec.getOperatorLabor().getId(), operLabor.getId(), "foreign key is wrong.");

        List <PtsLicense> lList = this.cmd.getPtsLicenseManager().listLicensesInGroup(lRec.getGroupNumber());
        if (lList.isEmpty()) {
            fail("Did not find license record");
        }
        assertEquals(lRec.getLicense().getId(), lList.get(0).getId(), "License foreign key is wrong");

    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param regionNumber - the region number
     *
     * @throws Exception
     */
    private void initialize(String serial, String operator, String regionNumber) throws Exception {
        final Locale locale = Locale.US;

        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
        executeLutCmd("cmdPrTaskLUTPtsValidRegions",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTPtsGetRegionConfiguration",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, regionNumber});
        executeLutCmd("cmdPrTaskLUTPtsGetFTLocation",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, regionNumber});

    }


    /**
     * Execute a command.
     *
     * @param cmdDate - date for command
     * @param serial - terminal serial number
     * @param operator - Operator id
     *
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String serial,
                                    String operator) throws Exception {
        this.cmd = getCmdBean();
        // call the command
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), serial, operator}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }

    /**
     * Reserve a license.
     *
     * @param cmdDate - date for command
     * @param serial - terminal serial number
     * @param operator - Operator id
     * @param license - either partial or full license number
     * @param partialFlag - 1 means this is a partial license number
     *
     * @throws Exception - all exceptions
     */
    private void reserveLicense(Date cmdDate, String serial,
                                    String operator, String license,
                                    String partialFlag) throws Exception {
        this.verifyLicenseCmd = getVLCmdBean();
        // call the command
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), serial, operator,
                        license, partialFlag}, this.verifyLicenseCmd);
        getTaskCommandService().executeCommand(this.verifyLicenseCmd);
    }

}
