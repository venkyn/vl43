/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.puttostore.task;



import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.PUTTOSTORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Tests for Flow Though location cmd.
 *
 * @author pfunyak
 *
 */

@Test(groups = { FAST, TASK, PUTTOSTORE })
public class PtsVerifyLicenseCmdTest  extends TaskCommandTestCase  {

    private PtsVerifyLicenseCmd cmd;
    private String testOperId = "PtsVerifyLicenseCmdOper";
    private String testSerial = "PtsVerifyLicenseCmdSerial";

    /**
     * @return a bean for testing.
     */
    private PtsVerifyLicenseCmd getCmdBean() {
        return (PtsVerifyLicenseCmd)
            getBean("cmdPrTaskLUTPtsVerifyLicense");
    }


    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        classSetupCore();
        // load the workgroups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the operators needed for testing
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        // load the put to store regions
        classSetupPTSRegions();
        classSetupInsert("VoiceLinkDataUnitTest_PtsData.xml");
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testDuplicateItemNumbers() throws Exception {
        // Get to the proper state in the task.
        initialize(this.testSerial, this.testOperId, "43");

        String licenseNumber = "0212";

        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "1");

        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");

        /*
         * Attempt to get a license with a common item number as the already
         * reserved license number
         */
        licenseNumber = "0213";

        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "1");
        // Verify the response
        records = response.getRecords();
        record = records.get(0);
        assertEquals(record.getErrorCode(),
                Long.toString(TaskErrorCode.PTS_VERIFY_LICENSE_DUPLICATE_ITEM.getErrorCode()),
                "Test for duplicate item numbers error - FAILED");
        assertEquals(record.getErrorMessage(),
                "Reserved license 00000212 and license 0213 contain the same item number 140384.",
                "incorrect error message");
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecutePartialMatchScenarios() throws Exception {

        // Get to the proper state in the task.
        initialize(this.testSerial, this.testOperId, "43");

        String licenseNumber = "1000";

        /*  There are 5 records in this region with this partial number --
         *  for various reasons none will be selectable, therefore we will
         *  get license not found message.
         */
        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "1");

        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(record.getErrorCode(),
                Long.toString(TaskErrorCode.PTS_LICENSE_REQUEST_FAILED.getErrorCode()),
                "1204 - license 1000 should return a 1204 -- FAILED");


        /*  This test uses a region defined as supporting
         * partial license numbers - we will request a license
         * that only have one match for the partial license provided
         */
        licenseNumber = "2002";
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "1");
        // Verify the response fields
        this.validateCommandFields(response);
        // Verify the response
        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(record.getErrorCode(), "0", "ErrorCode");
        assertEquals(record.get("licenseNumber"), "00052002");


        /*
         * Attempt to get the same license - this should return a 1206 -- license not found
         */
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "1");

        // Verify the response
        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(record.getErrorCode(),
                Long.toString(TaskErrorCode.PTS_LICENSE_REQUEST_FAILED.getErrorCode()),
                "2002 already reserved - failed");

        /*
         * Attempt a license that has more than one match
         */
        licenseNumber = "3010";

        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "1");
        // Verify the response
        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 2, "recordCount");
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testExecuteFullMatch() throws Exception {
        /*  This test uses a region defined as full license
         * numbers - we will request a license
         * that should be found
         *
         * Region 44 is defined to have a max of 4 licenses in a group
         */

        // Get to the proper state in the task.
        initialize(this.testSerial, this.testOperId, "44");

        String licenseNumber = "00051010";
        Response response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "0");

        // Verify the response
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(record.getErrorCode(), "0", "ErrorCode");
        assertEquals(record.get("licenseNumber"), "00051010");

        // Request a license that doesn't exist
        licenseNumber = "00051011";
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "0");

        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(record.getErrorCode(),
                Long.toString(TaskErrorCode.LICENSE_NOT_FOUND.getErrorCode()),
                "1061 - license not found number 00051011 -- FAILED");


        // Request second good license
        licenseNumber = "00051009";
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "0");
        // Verify the response
        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(record.getErrorCode(), "0", "ErrorCode failed on second good request");
        assertEquals(record.get("licenseNumber"), "00051009");

        // Request third good license -- this one is suspended
        licenseNumber = "67891000";
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "0");
        // Verify the response
        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(record.getErrorCode(), "0", "ErrorCode failed on third good request - suspended status");
        assertEquals(record.get("licenseNumber"), "67891000");

        // Request fourth good license -- this one is passed
        licenseNumber = "78901000";
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "0");
        // Verify the response
        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(record.getErrorCode(), "0", "ErrorCode failed on fourth good request - passed status");
        assertEquals(record.get("licenseNumber"), "78901000");

        // Request fifth -- this should fail with a forced sign off error code
        licenseNumber = "00051006";
        response = executeCommand(this.getCmdDateSec(), testSerial, testOperId, licenseNumber, "0");
        // Verify the response
        records = response.getRecords();
        record = records.get(0);
        assertEquals(records.size(), 1, "recordCount");
        assertEquals(record.getErrorCode(), "98", "98 error code not returned");

    }

    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     *
     * @param serial - terminal serial number
     * @param operator - operator for command
     * @param regionNumber - number of the region
     *
     * @throws Exception
     */
    private void initialize(String serial, String operator, String regionNumber) throws Exception {
        final Locale locale = Locale.US;

        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
        executeLutCmd("cmdPrTaskLUTPtsValidRegions",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTPtsGetRegionConfiguration",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, regionNumber});
        executeLutCmd("cmdPrTaskLUTPtsGetFTLocation",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, regionNumber});

    }


    /**
     * Execute a command.
     *
     * @param cmdDate - date for command
     * @param serial - terminal serial number
     * @param operator - the operator
     * @param license - either partial or full license number
     * @param partialFlag - 1 means this is tha partial license number
     *
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String serial,
                                    String operator, String license,
                                    String partialFlag) throws Exception {
        this.cmd = getCmdBean();
        // call the command
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), serial, operator,
                                            license, partialFlag}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
