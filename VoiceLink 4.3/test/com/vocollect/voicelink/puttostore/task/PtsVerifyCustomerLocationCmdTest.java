/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for PtsVerifyCustomerLocationCmd.
 *
 * @author pfunyak
 */
@Test(groups = { FAST, TASK, CORE })

public class PtsVerifyCustomerLocationCmdTest extends TaskCommandTestCase {

    private PtsVerifyCustomerLocationCmd cmd;
    private String operId = "PtsVerifyCustomerLocationOper";
    private String sn = "PtsVerifyCustomerLocationSerial";

    /**
     * @return a bean for testing.
     */
    private PtsVerifyCustomerLocationCmd getCmdBean() {
        return (PtsVerifyCustomerLocationCmd) getBean("cmdPrTaskLUTPtsVerifyCustomerLocation");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }

    /**
     * @throws Exception
     */
    @BeforeClass
    protected void testLoadData() throws Exception {
        // setup the database for the test
        classSetUp();
        classSetupCore();
        // load the workgroups for Put to store
        classSetupInsert("VoiceLinkDataUnitTest_PTSWorkGroups.xml");
        // load the operators needed for testing
        classSetupInsert("VoiceLinkDataUnitTest_PTSOperators.xml");
        // load the put to store regions
        classSetupPTSRegions();
        classSetupInsert("VoiceLinkDataUnitTest_PtsData.xml");
    }


    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.task.PtsVerifyCustomerLocationCmd#getScanned()}.
     */
    public void testGetScanned() {
        this.cmd.setScanned(true);
        assertEquals(this.cmd.isScanned(), true, "Scanned Getter/Setter");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.task.PtsVerifyCustomerLocationCmd#getCheckDigits()}.
     */
    public void testGetCheckDigits() {
        this.cmd.setCheckDigits("1");
        assertEquals(this.cmd.getCheckDigits(), "1", "CheckDigits Getter/Setter");
    }


    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.task.PtsVerifyCustomerLocationCmd#getLocationToVerify()}.
     */
    public void testGetLocationToVerify() {
        this.cmd.setLocationToVerify("1");
        assertEquals(this.cmd.getLocationToVerify(), "1", "StartLocation Getter/Setter");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.task.PtsVerifyCustomerLocationCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteValidLocation() throws Exception {

        initialize(sn, operId);
        //Test Verify scanned Location
        Response response = executeCommand(this.getCmdDateSec(), "1", "081529714", "93");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("customerLocationId").toString(), "-44106", "valid scanned location test");

        // Test verify spoken location
        response = executeCommand(this.getCmdDateSec(), "0", "15297", "23");
        records = response.getRecords();
        record = records.get(0);
        assertEquals(record.get("customerLocationId").toString(), "-44106", "valid spoken location test");

    }

    /**
     * Test method for {@link com.vocollect.voicelink.puttostore.task.PtsVerifyCustomerLocationCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteInvalidLocation() throws Exception {

        initialize(sn, operId);
         //Test Verify Location
        Response response = executeCommand(this.getCmdDateSec(), "1", "9999", "1");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"),
                Long.toString(TaskErrorCode.LOCATION_NOT_FOUND.getErrorCode()),
                "invalid scanned location test");
        //Test invalid spoken location Location
        response = executeCommand(this.getCmdDateSec(), "0", "0814189", "1");

        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(record.get("errorCode"),
                Long.toString(TaskErrorCode.LOCATION_NOT_FOUND.getErrorCode()),
                "invalid spoken location test");

        //Test Verify spoken Location -- the location 15297 exist, but the
        //correct checkdigit for that location is 23.
        response = executeCommand(this.getCmdDateSec(), "0", "15297", "99");

        //Verify Successful result
        records = response.getRecords();
        record = records.get(0);
        assertEquals(record.getErrorCode(),
                Long.toString(TaskErrorCode.LOCATION_NOT_FOUND.getErrorCode()),
                "correct location, wrong cd test");
    }


    /**
     * Intializes database by calling task commands that need to be
     * called prior to task command in unit test.
     * @param serial the serial number
     * @param operator the operator 
     * @throws Exception - Exception
     */
    private void initialize(String serial, String operator) throws Exception {
        final Locale locale = Locale.US;

        // region number doesn't matter for this test.
        // it could be any region number
        String regionNumber = "44";
        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator,
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTCoreSignOn",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, "1234"});
        executeLutCmd("cmdPrTaskLUTPtsValidRegions",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator});
        executeLutCmd("cmdPrTaskLUTPtsGetRegionConfiguration",
                new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, regionNumber});
        executeLutCmd("cmdPrTaskLUTPtsGetFTLocation",
            new String[] {makeStringFromDate(this.getCmdDateSec()), serial, operator, regionNumber});

    }

    /**
     * Execute a command.
     *
     * @param cmdDate - date for command
     * @param scanned - flag to indicate if location was scanned
     * @param locationToVerify - location specified by user
     * @param checkDigits - check digits specified by user
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String scanned,
                                    String locationToVerify,
                                    String checkDigits) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId,
                scanned, locationToVerify, checkDigits}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
