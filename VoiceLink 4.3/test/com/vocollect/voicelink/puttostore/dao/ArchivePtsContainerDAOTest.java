/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.dao;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.puttostore.model.PtsContainerStatus;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.Date;

import org.dbunit.operation.DatabaseOperation;
import org.hibernate.SessionFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 *
 * @author mlashinsky
 */
@Test(groups = { FAST, DAO })
public class ArchivePtsContainerDAOTest extends VoiceLinkDAOTestCase {

    protected static final String DATA_DIR = "data/dbunit/voicelink/";

    private ArchivePtsContainerDAO archivePtsContainerDAO;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return getVLArchiveDAOTestConfigLocations();
    }

    /**
     * @param sf - the session factory
     */
    @Test(enabled = false)
    public void setArchiveSessionFactory(SessionFactory sf) {
        masterSetSessionFactory(sf);
    }

    /**
     * Does nothing so that the sessionFactory Bean doesn't override the
     * archiveSessionFactoryBean.
     * @param sf - the session factory
     */
    @Override
    @Test(enabled = false)
    public void setSessionFactory(SessionFactory sf) {
        //Do nothing
    }

    /**
     * classSetUp is used to re-populate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Core.xml", DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_PtsWorkGroups.xml",
            DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_PtsOperators.xml",
            DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_PtsRegions.xml", DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_PtsData.xml", DatabaseOperation.INSERT);

    }

    /**
     *
     * @throws Exception
     */
    @Test()
    public void testListOlderThan() throws Exception {
        // Number of Available PTS Licenses in DB
        Integer resultSetSize = 0;
        //No open containers should exist
        PtsContainerStatus status = PtsContainerStatus.Open;
        Integer ptsArchiveContainerList = this.archivePtsContainerDAO.updateOlderThan(
            new Date(), status);
        assertEquals(ptsArchiveContainerList, resultSetSize,
            "Wrong number of Pts Licenses returned. Could be a problem with data.");

    }

    /**
     * Getter for the archivePtsContainerDAO property.
     * @return ArchivePtsContianerDAO value of the property
     */
    @Test(enabled = false)
    public ArchivePtsContainerDAO getArchivePtsContainerDAO() {
        return archivePtsContainerDAO;
    }

    /**
     * Setter for the archivePtsContainerDAO property.
     * @param archivePtsContainerDAO the new archivePtsLicenseDAO value
     */
    @Test(enabled = false)
    public void setArchivePtsContainerDAO(ArchivePtsContainerDAO archivePtsContainerDAO) {
        this.archivePtsContainerDAO = archivePtsContainerDAO;
    }

}
