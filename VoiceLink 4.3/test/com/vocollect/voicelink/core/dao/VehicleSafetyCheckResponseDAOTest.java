/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckRepairActionType;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponse;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponseType;

import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * @author mraj
 *
 */
@Test(groups = { CORE, VSC })
public class VehicleSafetyCheckResponseDAOTest extends VoiceLinkDAOTestCase {
    
    private VehicleSafetyCheckResponseDAO vehicleSafetyCheckResponseDAO = null;
    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Core_VSC_data.xml");
    }
    
    /**
     * Method to perform data clean up after all unit test cases are executed.
     * @throws Exception
     */
    @AfterClass()
    protected void cleanUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }
    /**
     * @param vehicleSafetyCheckResponseDAO the vehicleSafetyCheckResponseDAO to set
     */
    @Test(enabled = false)
    public void setVehicleSafetyCheckResponseDAO(VehicleSafetyCheckResponseDAO vehicleSafetyCheckResponseDAO) {
        this.vehicleSafetyCheckResponseDAO = vehicleSafetyCheckResponseDAO;
    }
    
    /**
     * Test method for Unique DB constraint working
     * @exception Exception any error
     */
    @Test()
    public void testVSCResponseUniqueConstraint() throws Exception {

        VehicleSafetyCheckResponse checkResponse = vehicleSafetyCheckResponseDAO.get(-1L);
        VehicleSafetyCheckResponse checkResponse2 = new VehicleSafetyCheckResponse();
        checkResponse2.setCreatedDate(checkResponse.getCreatedDate());
        checkResponse2.setOperator(checkResponse.getOperator());
        checkResponse2.setVehicle(checkResponse.getVehicle());
        checkResponse2.setSafetyCheck(checkResponse.getSafetyCheck());
        checkResponse2.setCheckResponse(VehicleSafetyCheckResponseType.INITIAL.toValue());
        checkResponse2.setRepairAction(VehicleSafetyCheckRepairActionType.NoAction);
        checkResponse2.setInternalGroupId(checkResponse.getInternalGroupId());
        
        try {
            
            vehicleSafetyCheckResponseDAO.save(checkResponse2);
        } catch (DataAccessException dae) {
            assertNotNull(dae);
            return;
        }
        
        //Attempt to save the save vehicle must fail due to Unique constraint in hibernate mapping
        fail();
    }
    
    /**
     * Test method for Unique records saves successfully
     * @exception Exception any error
     */
    @Test()
    public void testVSCResponseUniqueRecordSaves() throws Exception {

        VehicleSafetyCheckResponse checkResponse = vehicleSafetyCheckResponseDAO.get(-1L);
        VehicleSafetyCheckResponse checkResponse2 = new VehicleSafetyCheckResponse();
        checkResponse2.setCreatedDate(new Date()); //Its different
        checkResponse2.setOperator(checkResponse.getOperator());
        checkResponse2.setVehicle(checkResponse.getVehicle());
        checkResponse2.setSafetyCheck(checkResponse.getSafetyCheck());
        checkResponse2.setInternalGroupId(-1L);
        checkResponse2.setCheckResponse(VehicleSafetyCheckResponseType.INITIAL.toValue());
        checkResponse2.setRepairAction(VehicleSafetyCheckRepairActionType.NoAction);
        
        long size = vehicleSafetyCheckResponseDAO.getCount();
        try {
            
            vehicleSafetyCheckResponseDAO.save(checkResponse2);
        } catch (DataAccessException dae) {
            fail();
        }
        
        assertEquals(size + 1, vehicleSafetyCheckResponseDAO.getCount());
    }
}
