/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Location;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * Tests for LocationDAO methods.
 *
 * @author ddoubleday
 */
public class LocationDAOTest extends VoiceLinkDAOTestCase {

    private LocationDAO dao = null;
    
    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();       
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_Core.xml");
    }

    /** 
     * @param terminalDAO the user data access object
     */
    @Test(enabled = false)
    public void setLocationDAO(LocationDAO terminalDAO) {
        this.dao = terminalDAO;
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.dao.LocationDAO#findByIdentifier(java.lang.String)}.
     * @exception Exception any error
     */
    @Test()
    public void testFindByScanned() throws Exception {

        Location sample = dao.getAll().get(0);
        Location t = 
            dao.findLocationByScanned(sample.getScannedVerification());
        // Should be found.
        assertEquals(sample.getAisle(), t.getAisle(), "Aisle mismatch");
        assertEquals(sample.getSlot(), t.getSlot(), "Slot mismatch");
        assertEquals(sample.getCheckDigits(), t.getCheckDigits(), "CheckDigits mismatch");
        assertEquals(sample.getScannedVerification(), t.getScannedVerification(), "ScannedVerification mismatch");
        this.dao.delete(t);
    }

}
