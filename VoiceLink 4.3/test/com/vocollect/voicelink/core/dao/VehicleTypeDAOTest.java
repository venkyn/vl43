/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.VehicleType;

import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit Tests for VehicleTypeDAO methods.
 * 
 * @author kudupi
 */
@Test(groups = { CORE, VSC })
public class VehicleTypeDAOTest extends VoiceLinkDAOTestCase {

    private VehicleTypeDAO dao = null;

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Core_VSC_data.xml");
    }

    /**
     * Method to perform data clean up after all unit test cases are executed.
     * @throws Exception
     */
    @AfterClass()
    protected void cleanUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * @param vehicleTypeDAO the user data access object
     */
    @Test(enabled = false)
    public void setVehicleTypeDAO(VehicleTypeDAO vehicleTypeDAO) {
        this.dao = vehicleTypeDAO;
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.dao.VehicleTypeDAO#listAllVehicleTypesByNumber()}
     * .
     * @exception Exception any error
     */
    @Test()
    public void testVehicleTypeData() throws Exception {

        VehicleType vehicleType = dao.listAllVehicleTypesByNumber().get(0);

        // Should be found. And values being validated
        assertEquals(vehicleType.getNumber(), new Integer("1"),
            "Vehicle Type number mismatch");
        assertEquals(vehicleType.getDescription(),
            "Counterbalance Forklift Truck", "Description mismatch");
        assertEquals(vehicleType.getId(), new Long("-1"), "ID mismatch");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.dao.VehicleTypeDAO#listAllVehicleTypesByNumber()}
     * .
     * @exception Exception any error
     */
    @Test()
    public void testEqualsAndHashCode() throws Exception {
        List<VehicleType> listVehicleType = dao.listAllVehicleTypesByNumber();
        VehicleType vehicleType = listVehicleType.get(0);
        VehicleType vehicleType1 = listVehicleType.get(1);

        // Should be found. And values being validated
        assertTrue(!vehicleType.equals(vehicleType1));
        assertNotSame(vehicleType.hashCode(), vehicleType1.hashCode());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.dao.VehicleTypeDAO#listAllVehicleTypesByNumber()}
     * .
     * @exception Exception any error
     */
    @Test()
    public void testVehicleTypesOrder() throws Exception {
        List<VehicleType> listVehicleType = dao.listAllVehicleTypesByNumber();
        int i = 1;
        for (VehicleType vehicleType : listVehicleType) {
            assertEquals(vehicleType.getNumber(), new Integer(i));
            i++;
        }
    }

}
