/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.UnitOfMeasure;

import java.util.List;

import org.testng.annotations.Test;

/**
 * @author pkolonay
 *
 */
public class UnitOfMeasureDAOTest extends VoiceLinkDAOTestCase {

    private UnitOfMeasureDAO dao = null;
    
    
    /**
     * @throws Exception
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();    
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /** 
     * @param uomDAO the user data access object
     */
    @Test(enabled = false)
    public void setUnitOfMeasureDAO(UnitOfMeasureDAO uomDAO) {
        this.dao = uomDAO;
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.dao.LocationDAO#findByIdentifier(java.lang.String)}.
     * @exception Exception any error
     */
    @Test()
    public void testFindAll() throws Exception {

        UnitOfMeasure uom = new UnitOfMeasure();
        
        String uomName = "bundles";
        uom.setName(uomName);
        
        List<UnitOfMeasure> sample = dao.getAll();
        
        assertEquals(sample.size(), 0, "UOM not empty");
        
        dao.save(uom);
        sample = dao.getAll();
        assertEquals(sample.size(), 1, "UOM data");
    }
    
}
