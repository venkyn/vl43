/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Shift;

import static com.vocollect.epp.test.TestGroups.SHIFT;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit Tests for ShiftDAO methods.
 * 
 * @author smittal
 */
@Test(groups = { CORE, SHIFT })
public class ShiftDAOTest extends VoiceLinkDAOTestCase {

    private ShiftDAO dao = null;

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Core_Shift_data.xml");
    }

    /**
     * Method to perform data clean up after all unit test cases are executed.
     * @throws Exception
     */
    @AfterClass()
    protected void cleanUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * @param shiftDAO the user data access object
     */
    @Test(enabled = false)
    public void setShiftDAO(ShiftDAO shiftDAO) {
        this.dao = shiftDAO;
    }

    /**
     * . Test the get all method
     */
    @Test(enabled = false)
    public void testGetAll() {
        try {
            List<Shift> shifts = this.dao.getAll();
            assert (shifts.size() > 0);
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }

    @Test(enabled = false)
    public void testUniquenessByName() {
        try {
            List<Shift> shifts = this.dao.getAll();

            Long l1 = this.dao.uniquenessByName(shifts.get(0).getName());

            Long l2 = this.dao.uniquenessByName(shifts.get(1).getName());

            assert (!l1.equals(l2));
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }
}
