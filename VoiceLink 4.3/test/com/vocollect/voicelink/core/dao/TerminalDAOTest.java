/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Device;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * Tests for TerminalDAO methods.
 *
 * @author ddoubleday
 */
public class TerminalDAOTest extends VoiceLinkDAOTestCase {

    private static final String TEST_SN = "555";
    
    private TerminalDAO dao = null;
    
    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_ResetAll.xml",
            DatabaseOperation.DELETE_ALL);
        adapter.resetInstallationData();                
    }

    /** 
     * @param terminalDAO the user data access object
     */
    @Test(enabled = false)
    public void setTerminalDAO(TerminalDAO terminalDAO) {
        this.dao = terminalDAO;
    }
    /**
     * Test method for {@link com.vocollect.voicelink.core.dao.TerminalDAO#findBySerialNumber(java.lang.String)}.
     * @exception Exception any error
     */
    @Test()
    public void testFindBySerialNumber() throws Exception {
        Device t = this.dao.findBySerialNumber(TEST_SN);
        // Should not be found.
        assert t == null;
        t = new Device();
        t.setSerialNumber(TEST_SN);
        this.dao.save(t);
        // Now try again
        t = this.dao.findBySerialNumber(TEST_SN);
        assert t != null;
        this.dao.delete(t);
    }

}
