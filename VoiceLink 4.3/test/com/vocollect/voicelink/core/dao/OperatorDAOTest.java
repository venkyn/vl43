/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Operator;

import java.util.List;

import org.testng.annotations.Test;


/**
 * Tests for OperatorDAO methods.
 *
 * @author ddoubleday
 */
public class OperatorDAOTest extends VoiceLinkDAOTestCase {

    private static final String TEST_ID = "aworker";
    
    private OperatorDAO dao = null;
    
   /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        System.out.println("Method setup ODAO");
        super.onSetUp(); 
    }

    /** 
     * @param terminalDAO the user data access object
     */
    @Test(enabled = false)
    public void setOperatorDAO(OperatorDAO terminalDAO) {
        this.dao = terminalDAO;
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.dao.OperatorDAO#findByIdentifier(java.lang.String)}.
     * @exception Exception any error
     */
    @Test()
    public void testFindByIdentifier() throws Exception {
        Operator t = this.dao.findByIdentifier(TEST_ID);
        // Should not be found.
        assert t == null : "Unexpected operator, ID = " 
            + t.getOperatorIdentifier();
        t = new Operator();
        t.setOperatorIdentifier(TEST_ID);
        t.setPassword("pass");
        t.setName("new Employee");
        this.dao.save(t);
        // Now try again
        t = this.dao.findByIdentifier(TEST_ID);
        assert t != null;
        this.dao.delete(t);
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testGetAll() throws Exception {
        Operator t = new Operator();
        t.setOperatorIdentifier(TEST_ID);
        t.setPassword("pass");
        t.setName("new Employee");
        this.dao.save(t);
        List<Operator> opers = this.dao.getAll();
        assertTrue(!opers.isEmpty(), "getAll");
    }


}
