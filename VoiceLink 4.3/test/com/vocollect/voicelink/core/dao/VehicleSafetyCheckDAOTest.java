/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.dao;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.VehicleSafetyCheck;

import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit Tests for VehicleSafetyCheckDAO methods.
 * 
 * @author khazra
 */
@Test(groups = { CORE, VSC })
public class VehicleSafetyCheckDAOTest extends VoiceLinkDAOTestCase {

    private VehicleSafetyCheckDAO dao = null;

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Core_VSC_data.xml");
    }

    /**
     * Method to perform data clean up after all unit test cases are executed.
     * @throws Exception
     */
    @AfterClass()
    protected void cleanUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * @param vehicleSafetyCheckDAO the user data access object
     */
    @Test(enabled = false)
    public void setVehicleSafetyCheckDAO(VehicleSafetyCheckDAO vehicleSafetyCheckDAO) {
        this.dao = vehicleSafetyCheckDAO;
    }

 
    /**
     * @throws Exception
     */
    @Test()
    public void testSafetyCheckListReturnedAndSequenced() throws Exception {
        List<VehicleSafetyCheck> safetyChecks = dao.listAllChecksByVehicleType(1);
        assertEquals(safetyChecks.size(), 5);
        
        assertEquals(safetyChecks.get(0).getSpokenDescription(), "Breaks");
        assertEquals(safetyChecks.get(4).getSpokenDescription(), "Lights");
    }

    
    /**
     * @throws Exception
     */
    @Test()
    public void testSafetyCheckListIsEmpty() throws Exception {
        List<VehicleSafetyCheck> safetyChecks = dao.listAllChecksByVehicleType(10);
        assertEquals(safetyChecks.size() , 0);        
    }
    
    /**
     * @throws Exception
     */
    @Test()
    public void testSafetyCheckListWithNullType() throws Exception {
        List<VehicleSafetyCheck> safetyChecks = dao.listAllChecksByVehicleType(null);
        assertEquals(safetyChecks.size() , 0);        
    }
}
