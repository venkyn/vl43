/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for ValidFunctionsCmd.
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, CORE })

public class ValidFunctionsCmdTest extends TaskCommandTestCase {

    private ValidFunctionsCmd cmd;
    private String operId = "ValidFunctionsOper";
    private String sn = "ValidFunctionsSerial";
    
    /**
     * @return a bean for testing.
     */
    private ValidFunctionsCmd getCmdBean() {
        return (ValidFunctionsCmd) getBean("cmdPrTaskLUTCoreValidFunctions");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ValidFunctionsCmd#testGetSetTaskId()}.
     */
    public void testGetSetTaskId() {
        this.cmd.setTaskId(5);
        assertEquals(this.cmd.getTaskId(), (Integer) 5);
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ValidFunctionsCmd#testExecute()}.
     * @throws Exception - Exception
     */
    public void testExecuteValidFunctions() throws Exception {

        initialize();
        //Test Verify Location
        Response response = executeCommand(this.getCmdDateSec(), "0");
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 9, "recordSize");

    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ValidFunctionsCmd#testExecute()}.
     * @throws Exception - Exception
     */
    public void testExecuteNoValidFunctions() throws Exception {

        initialize();
        //Test Verify Location
        Response response = executeCommand(this.getCmdDateSec(), "8");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "98", "errorCode");

    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param taskId - the task id
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String taskId) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId, 
                taskId}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }

}
