/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.core.model.OperatorStatus;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.service.AssignmentManager;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for SignOnCmd.
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, TASK, CORE })

public class SignOnCmdTest extends TaskCommandTestCase {

    static final Comparator<OperatorLabor> OPERATOR_LABOR_ID = 
        new Comparator<OperatorLabor>() {
        public int compare(OperatorLabor o1, OperatorLabor o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };
        
    private SignOnCmd cmd;
    private String operId = "SignOnOper";
    private String sn = "SignOnSerial";

    /**
     * @return a bean for testing.
     */
    private SignOnCmd getCmdBean() {
        return (SignOnCmd) getBean("cmdPrTaskLUTCoreSignOn");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SignCmd#getLocale()}.
     */
    @Test()
    public void testGetSetLocale() {
        this.cmd.setPassword("1234");
        assert new String("1234").equals(this.cmd.getPassword());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SignOnCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();
        //Test Create new operator  
        //Assumes Operator does not exist yet
        // Now put args in the command and execute it.
        Date cmdDate = this.getCmdDateSec();
        String password = "1234";
        Response response = executeCommand(cmdDate, sn, password);
        
        //Operator should be created at this point and have password saved
        Operator oper = this.cmd.getOperatorManager().findByIdentifier(operId);
        assertNotNull(oper, "Null operator");
        assertEquals(oper.getPassword(), password, "Wrong Password");
        assertNotNull(oper.getSignOnTime(), "Null Sign On Time");
        assertEquals(makeStringFromDate(cmdDate), makeStringFromDate(oper.getSignOnTime()),
                    "Wrong sign on time");
        assertNull(oper.getSignOffTime(), "Sign Off Time Not Null");
        assertEquals(oper.getStatus(), OperatorStatus.SignedOn, "OperatorStatus");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.get("interleave"), 0, "InterleaveResponse");
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        // verify that signon labor record was created correctly
        OperatorLabor operLabor =
            this.cmd.getLaborManager().getOperatorLaborManager()
                .findOpenRecordByOperatorId(oper.getId());
        assertNotNull(operLabor, "Signon Labor Record was not created");
        assertNull(operLabor.getEndTime(), "Operator Labor end time is not null");
        assertEquals(operLabor.getActionType(), OperatorLaborActionType.SignOn,
                     "Invalid Action Type ");
        assertEquals(operLabor.getFilterType(), OperatorLaborFilterType.Other,
                     "Invalid Filter Type ");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SignOnCmd#testExecuteAlreadySignedOn()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteAlreadySignedOn() throws Exception {

        initialize();
        //Test Create new operator  
        //Assumes Operator does not exist yet
        // Now put args in the command and execute it.
        Response response = executeCommand(this.getCmdDateSec(), sn, "1234");
        
        //Sign in second time
        response = executeCommand(this.getCmdDateSec(), sn, "1234");
        
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");

        // Verify that we got 2 labor records. The first is a closed sign on record. 
        // The second is an open signon. 
        List<OperatorLabor> olList =
            this.cmd.getLaborManager().getOperatorLaborManager()
                .listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        assertEquals(olList.size(), 3, "Record Count ");
        
        Collections.sort(olList, OPERATOR_LABOR_ID);
        OperatorLabor olRecord = olList.get(0);
        // make sure it's a sign on record
        assertEquals(olRecord.getActionType(), OperatorLaborActionType.SignOn,
                     "Invalid Action Type ");
        //Test for order records returned
        assertNotNull(olRecord.getEndTime(), "Operator Labor sign on record is not closed");
        // Make sure the second record is a closed sign off record
        olRecord = olList.get(1);
        assertEquals(olRecord.getActionType(), OperatorLaborActionType.SignOff,
                     "Invalid Action Type ");
        assertNotNull(olRecord.getEndTime(), "Operator Labor sign on record is not open");

        // Make sure the second record is a closed sign off record
        olRecord = olList.get(2);
        assertEquals(olRecord.getActionType(), OperatorLaborActionType.SignOn,
                     "Invalid Action Type ");
        assertNull(olRecord.getEndTime(), "Operator Labor sign on record is not open");
    }
    

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SignOnCmd#testExecuteErrors()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteSignOffAssignments() throws Exception {

        initializeAssignment();
        // Create operator
        executeCommand(this.getCmdDateSec(), sn, "1234");
         //Verify Assignment is Suspended
        AssignmentManager am = (AssignmentManager) getBean("assignmentManager");
         Assignment a = am.get(-96L);
        assertEquals(AssignmentStatus.Suspended,
            a.getStatus(), "AssignmentSuspended");
   }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SignOnCmd#testExecuteErrors()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteErrors() throws Exception {
        final Locale locale = Locale.US;

        initialize();
        // Create operator
        Response response = executeCommand(this.getCmdDateSec(), sn, "1234");
        //Test validate password
        //Assumes Operator does not exist yet
        // Sign in with different password
        response = executeCommand(this.getCmdDateSec(), sn, "1111");
        
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 
                     TaskErrorCode.INVALID_PASSWORD.getErrorCode(),
                     "InvalidPassword");
        //Test validate terminal
        //Assumes Operator does not exist yet
        //create terminal
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", new String[] {
            makeStringFromDate(this.getCmdDateSec()), "1111", operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion() });
        // Sign in with different terminal
        response = executeCommand(this.getCmdDateSec(), "1111", "1234");
        
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(TaskErrorCode.NOT_ALLOWED_MULTIPLE_TERMINALS.getErrorCode(),
                      Long.parseLong(record.getErrorCode()), "InvalidPassword");
   }

    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, 
            operId, locale.toString(), "Default", this.goodCombinedTaskVersion()});
        
    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initializeAssignment() throws Exception {
        final Locale locale = Locale.US;

        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, locale.toString(), 
            "Default", this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "2", "3" });
        executeLutCmd("cmdPrTaskLUTGetAssignment", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1" });
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param testSn - Device Serial Number
     * @param password - Operator Password
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String testSn,
                                    String password) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), testSn, operId, 
                password}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
