/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for ValidVehicleTypesCmd.
 *
 * @author Ed Stoll
 * @author mraj
 */
@Test(groups = { FAST, TASK, CORE, VSC })

public class ValidVehicleTypesCmdTest extends TaskCommandTestCase {

    private ValidVehicleTypesCmd cmd;
    
    /**
     * @return a bean for testing.
     */
    private ValidVehicleTypesCmd getCmdBean() {
        return (ValidVehicleTypesCmd) 
            getBean("cmdPrTaskLUTCoreValidVehicleTypes");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ValidVehicleTypesCmd#testTaskId()}.
     */
    @Test()
    public void testGetSetTaskId() {
        this.cmd.setTaskId("5");
        assert "5".equals(this.cmd.getTaskId());
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.validVehicleTypes#testExecute()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testExecuteWithVehicleTypes() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_Core_VSC_data.xml");
        
        final String operId = "ValidVehicleTypesOper";
        String sn = "ValidVehicleTypesSerial";

        initialize(sn, operId);
        //Test Get No break types 
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "0"}, 
            this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 8, "recordSize");
        int type = 0;
        ResponseRecord record = records.get(type);
        assertEquals(record.get("vehicleType"), type + 1);
        assertEquals(record.get("vehicleDescription"), "Counterbalance Forklift Truck", "");
        assertEquals(record.get("captureVehicleId"), "0", "0 CaptureVehicleID");
        
        type = 5;
        record = records.get(type);
        assertEquals(record.get("vehicleType"), type + 1);
        assertEquals(record.get("vehicleDescription"), "Sideloaders", "");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.validVehicleTypes#testExecute()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testExecuteWithNoVehicleTypes() throws Exception {
        //DELIBERATELY commented to indicate there are no Vehicle types in the system
        
//        classSetupInsert("VoiceLinkDataUnitTest_VSC_data.xml"); 
        
        super.classSetUp();
        final String operId = "ValidVehicleTypesOper";
        String sn = "ValidVehicleTypesSerial";

        initialize(sn, operId);
        //Test Get No vehicle types
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "0"}, 
            this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.get("vehicleType"), "", "BlankVehicleType");
        assertEquals(record.get("vehicleDescription"), "", "BlankVehicleDescription");
        assertEquals(record.get("captureVehicleId"), "0", "0 CaptureVehicleID");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.validVehicleTypes#testExecute()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testExecuteCaptureVehicleIDSupplied() throws Exception {
        //DELIBERATELY commented to indicate there are no Vehicle types in the system
        
//        classSetupInsert("VoiceLinkDataUnitTest_VSC_data.xml"); 
        
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_Core_VSC_data.xml");
        
        final String operId = "ValidVehicleTypesOper";
        String sn = "ValidVehicleTypesSerial";

        initialize(sn, operId);
        //Test Get No vehicle types
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "0"}, 
            this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 8, "recordSize");
        
        ResponseRecord record0 = records.get(0);
        assertEquals(record0.get("captureVehicleId"), "0", "0 CaptureVehicleID");
        ResponseRecord record4 = records.get(4);
        assertEquals(record4.get("captureVehicleId"), "1", "1 CaptureVehicleID");
    }
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @param sn - Serial Number
     * @param operId - Operator
     * @throws Exception - Exception
     */
    private void initialize(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        
    }
}
