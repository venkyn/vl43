/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.voicelink.core.model.BreakType;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for BreakTypeCmd.
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, TASK, CORE })

public class BreakTypeCmdTest extends TaskCommandTestCase {

    private BreakTypeCmd cmd;

    /**
     * @return a bean for testing.
     */
    private BreakTypeCmd getCmdBean() {
        return (BreakTypeCmd) getBean("cmdPrTaskLUTCoreBreakTypes");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.BreakTypeCmd#testExecute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {
        
        final String operId = "BreakTypeCmdOper";
        String sn = "BreakTypeCmdSerial";

        Response response;
        initialize(sn, operId);
        //Test Get No break types
      
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(this.getCmdDateSec()), 
                sn, operId}, this.cmd);
        response = getTaskCommandService().executeCommand(this.cmd);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("breakNumber"), "", "EmptyBreakNumber");
        assertEquals(record.get("breakDescription"), "", "EmptyBreakName");

        //Test 1 Break Type
        addBreakType(2, "Break 1");

        this.cmd = getCmdBean();
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(this.getCmdDateSec()), 
                sn, operId}, this.cmd);
        response = getTaskCommandService().executeCommand(this.cmd);
        
        //Verify Successful result
        records = response.getRecords();
        assertEquals(1, records.size(), "singleBreakRecordSize");
        record = records.get(0);
        assertEquals(record.get("breakDescription"), "Break 1", "singleBreakNumber");
        assertEquals(record.get("breakNumber"), 2, "singleBreakName");

        //Test multiple Break Types ordered properly
        addBreakType(1, "Break 2");

        this.cmd = getCmdBean();
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(this.getCmdDateSec()), 
                sn, operId}, this.cmd);
        response = getTaskCommandService().executeCommand(this.cmd);
        
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 2, "multipleBreakRecordSize");

        record = records.get(0);
        assertEquals(record.get("breakDescription"), "Break 2", "multipleBreakNameRecord1");
        assertEquals(record.get("breakNumber"), 1, "multipleBreakNumberRecord1");

        record = records.get(1);
        assertEquals(record.get("breakDescription"), "Break 1", "multipleBreakNameRecord2");
        assertEquals(record.get("breakNumber"), 2, "multipleBreakNumberRecord2");

        //=============================================================
        //Test multiple Break Types and 2 digit break type ordered properly
        addBreakType(10, "Break 3");

        this.cmd = getCmdBean();
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(this.getCmdDateSec()), 
                sn, operId}, this.cmd);
        response = getTaskCommandService().executeCommand(this.cmd);
        
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 3, "multipleBreakRecordSize");

        record = records.get(0);
        assertEquals(record.get("breakDescription"), "Break 2", 
                     "multipleBreakNameRecord1");
        assertEquals(record.get("breakNumber"), 1, "multipleBreakNumberRecord1");

        record = records.get(1);
        assertEquals(record.get("breakDescription"), "Break 1", "multipleBreakNameRecord2");
        assertEquals(record.get("breakNumber"), 2, "multipleBreakNumberRecord2");

        record = records.get(2);
        assertEquals(record.get("breakDescription"), "Break 3", 
                     "multipleBreakNameRecord3");
        assertEquals(record.get("breakNumber"), 10, "multipleBreakNumberRecord3");

        //Clean Up
        deleteAllBreaks();
    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @param sn - Serial Number
     * @param operId - Operator
     * @throws Exception - Exception
     */
    private void initialize(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;
        
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
    }
    
    /**
     * Adds a break type to the system.
     * 
     * @param description - Break Description To Add
     * @param number - number for break type
     * 
     * @throws BusinessRuleException - Business rule exception
     * @throws DataAccessException - Database exception
     */
    private void addBreakType(int number, String description) 
    throws BusinessRuleException, DataAccessException {
        BreakType bt = new BreakType();
        bt.setNumber(number);
        bt.setName(description);
        this.cmd.getBreakTypeManager().save(bt);
    }
    
    /**
     * Deletes all breaks types from database.
     * 
     * @throws DataAccessException - Database access exception
     * @throws BusinessRuleException - Business exception
     */
    private void deleteAllBreaks() 
    throws DataAccessException, BusinessRuleException {
//        List<BreakType> breakTypes = this.cmd.getBreakTypeManager().getAll();
        
//        for (BreakType b : breakTypes) {
//           this.cmd.getBreakTypeManager().delete(b);
//        }
        
    }
}
