/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for GetReasonCodesCmd.
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, CORE })

public class GetReasonCodesCmdTest extends TaskCommandTestCase {

    private GetReasonCodesCmd cmd;
    private String operId = "GetReasonCodesOper";
    private String sn = "GetReasonCodesSerial";

    /**
     * @return a bean for testing.
     */
    private GetReasonCodesCmd getCmdBean() {
        return (GetReasonCodesCmd) getBean("cmdPrTaskLUTCoreGetReasonCodes");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetReasonCodesCmd#getFunctionNumber()}.
     */
    public void testGetFunctionNumber() {
        this.cmd.setFunctionNumber(1);
        assertEquals(this.cmd.getFunctionNumber().toString(), "1", "FunctionNumber Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetReasonCodesCmd#getRequests()}.
     */
    public void testGetRequests() {
        this.cmd.setRequests("1");
        assertEquals(this.cmd.getRequests(), "1", "Requests Getter/Setter");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetReasonCodesCmd#testExecute()}.
     * @throws Exception any
     */
    public void testExecute() throws Exception {  

        initialize();

        //Test get region codes
        Response response = executeCommand(this.getCmdDateSec(), "1", "0");

        //Verify Successful result
        assertEquals(response.getRecords().size(), 3, "recordSize ");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("reasonNumber"), 1, "reasonNumber");
        assertEquals(record.get("reasonDescription"), "Reason 1", "reasonDescription");
        record = records.get(1);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("reasonNumber"), 2, "reasonNumber");
        assertEquals(record.get("reasonDescription"), "Reason 2", "reasonDescription");
        record = records.get(2);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("reasonNumber"), 3, "reasonNumber");
        assertEquals(record.get("reasonDescription"), "Reason 3", "reasonDescription");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetReasonCodesCmd#testExecute()}.
     * @throws Exception any
     */
    public void testExecuteNoCodes() throws Exception {  

        initialize();
        //Test get region codes
        Response response = executeCommand(this.getCmdDateSec(), "2", "0");

        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize ");
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("reasonNumber"), null, "reasonNumber");
        assertEquals(record.get("reasonDescription"), null, "reasonDescription");
        
    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();
        

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param functionNumber - the function codes being requested
     * @param requests - not currently used in 2.2
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String functionNumber, 
                                    String requests) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
                new String[] {makeStringFromDate(cmdDate), sn, operId, 
                functionNumber, requests}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
