/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for VerifyLocationCmd.
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, CORE })

public class VerifyLocationCmdTest extends TaskCommandTestCase {

    private VerifyLocationCmd cmd;
    private String operId = "VerifyLocationOper";
    private String sn = "VerifyLocationSerial";

    /**
     * @return a bean for testing.
     */
    private VerifyLocationCmd getCmdBean() {
        return (VerifyLocationCmd) getBean("cmdPrTaskLUTCoreVerifyLocation");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.VerifyLocationCmd#getScanned()}.
     */
    public void testGetScanned() {
        this.cmd.setScanned(1);
        assertEquals(this.cmd.getScanned(), 1, "Scanned Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.VerifyLocationCmd#getCheckDigits()}.
     */
    public void testGetCheckDigits() {
        this.cmd.setCheckDigits("1");
        assertEquals(this.cmd.getCheckDigits(), "1", "CheckDigits Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.VerifyLocationCmd#getStartLocation()}.
     */
    public void testGetStartLocation() {
        this.cmd.setStartLocation(1);
        assertEquals(this.cmd.getStartLocation(), 1, "StartLocation Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.VerifyLocationCmd#getStartLocation()}.
     */
    public void testGetLocationToVerify() {
        this.cmd.setLocationToVerify("1");
        assertEquals(this.cmd.getLocationToVerify(), "1", "StartLocation Getter/Setter");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.VerifyLocationCmd#testExecute()}.
     * @throws Exception any
     */
    public void testExecuteValidLocationScanned() throws Exception {

        initialize();
        //Test Verify Location
        Response response = executeCommand(this.getCmdDateSec(), "1", "0814207", "93", "0");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("locationNumber").toString(),
                     "-1779", "locationNumber");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.VerifyLocationCmd#testExecute()}.
     * @throws Exception any
     */
    public void testExecuteValidLocationSpoken() throws Exception {

        initialize();
        //Test Verify Location
        Response response = executeCommand(this.getCmdDateSec(), "0", "14189", "53", "1");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("locationNumber").toString(), "-1772", "locationNumber");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.VerifyLocationCmd#testExecute()}.
     * @throws Exception any
     */
    public void testExecuteInvalidLocationScanned() throws Exception {

        initialize();
        //Test Verify Location
        Response response = executeCommand(this.getCmdDateSec(), "1", "9999", "1", "0");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode").toString(),
                     Long.toString(TaskErrorCode.LOCATION_NOT_FOUND.getErrorCode()),
                     "errorCode");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.VerifyLocationCmd#testExecute()}.
     * @throws Exception any
     */
    public void testExecuteInvalidLocationSpoken() throws Exception {

        initialize();
        //Test Verify Location
        Response response = executeCommand(this.getCmdDateSec(), "0", "0814189", "1", "0");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode").toString(),
                Long.toString(TaskErrorCode.LOCATION_NOT_FOUND.getErrorCode()),
                "errorCode");
    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "3" });
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "2", "3" });
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param scanned - flag to indicate if location was scanned
     * @param locationToVerify - location specified by user
     * @param checkDigits - check digits specified by user
     * @param startLocation - floag to indicate if verifying start location
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String scanned, 
                                    String locationToVerify, 
                                    String checkDigits,
                                    String startLocation) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId, 
                scanned, locationToVerify, checkDigits, startLocation}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
