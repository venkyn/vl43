/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.voicelink.core.model.OperatorBreakLabor;
import com.vocollect.voicelink.core.model.OperatorFunctionLabor;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.selection.model.AssignmentLabor;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for SendBreakInfoCmd.
 *
 * @author pfunyak
 */
@Test(groups = { FAST, TASK, CORE })

public class SendBreakInfoCmdTest extends TaskCommandTestCase {
  
   
    private SendBreakInfoCmd cmd;
    private String operId = "SendBreakInfoOper";
    private String sn = "SendBreakInfoSerial";

    /**
     * @return a bean for testing.
     */
    private SendBreakInfoCmd getCmdBean() {
        return (SendBreakInfoCmd) getBean("cmdPrTaskODRCoreSendBreakInfo");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendBreakInfoCmd#getBreakTypeSent()}.
     */
    @Test()
    public void testGetBreakTypeSent() {
        this.cmd.setBreakTypeSent(1);
        assertEquals(this.cmd.getBreakTypeSent(), 1, "Scanned Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendBreakInfoCmd#getStartEndFlag()}.
     */
    @Test()
    public void testGetStartEndFlag() {
        this.cmd.setStartEndFlag(1);
        assertEquals(this.cmd.getStartEndFlag(), 1, "CheckDigits Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendBreakInfoCmd#getDescription()}.
     */
    @Test()
    public void testgetDescription() {
        this.cmd.setDescription("1");
        assertEquals(this.cmd.getDescription(), "1", "StartLocation Getter/Setter");
    }

    /**
     * Test method for 
     * @throws Exception any
     */
    @Test()
    public void testExecuteCreateBreakRecord() throws Exception {

        // verify that we can sign on to the system and immediately take a break
        // also verify that all fields in the break record are populated correctly.

        initialize();
        signOnOperator();
        //take a break and verify response
        Response response = executeCommand(this.getCmdDateMS(), "1", "0", "1");
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        
        // verify that an open break labor record was created correctly
        OperatorBreakLabor breakLabor = (OperatorBreakLabor)
            this.cmd.getLaborManager().getOperatorLaborManager()
                    .findOpenRecordByOperatorId(this.cmd.getOperator().getId());
        assertNotNull(breakLabor, "Break labor record was not created");
        assertNotNull(breakLabor.getOperator(), "operator is null ");
        assertEquals(breakLabor.getActionType(), OperatorLaborActionType.Break, "actionType is invalid ");
        assertNotNull(breakLabor.getStartTime(), "startTime is null "); 
        assertNull(breakLabor.getEndTime(), "endTime is not null");
        assertNotNull(breakLabor.getDuration(), "duration is null ");
        assertEquals(breakLabor.getDuration(), new Long(0), "duration is not zero ");
        assertNotNull(breakLabor.getCreatedDate(), "createdDate is null");
        assertNotNull(breakLabor.getBreakType(), "breakId is null ");
        assertNotNull(breakLabor.getPreviousOperatorLabor(), "previousOperatorLabor is null ");
        assertEquals(breakLabor.getFilterType(), OperatorLaborFilterType.Other, "Invalid Filter Type ");
    }
    
    
       
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendBreakInfoCmd#testExecute()}.
     * @throws Exception any
     */
    
    @Test()
    public void testExecuteCreateSelectionTakeBreak() throws Exception {
        
        // Sign on to the system and choose the selection function then
        // take a break before getting an assignment.
        // this should result in 3 labor records being created.
        // The first is a closed sign on record. 
        // The second is a closed selection.
        // The third is an open break record.
        
        initialize();
        signOnOperator();
        getPickingRegion();
        //take a break and verify response
        Response response = executeCommand(this.getCmdDateMS(), "1", "0", "1");
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
 
        // Verify that we got 3 labor records.
         List<OperatorLabor> olList =
            this.cmd.getLaborManager().getOperatorLaborManager()
                .listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        assertEquals(olList.size(), 3, "Bad Record Count ");
        OperatorLabor olRec = olList.get(0);
        assertEquals(OperatorLaborActionType.SignOn, olRec.getActionType(), "Bad Signon Record ");
        assertNotNull(olRec.getEndTime(), "SignOn record not closed ");
        olRec = olList.get(1);
        assertEquals(olRec.getActionType(), OperatorLaborActionType.Selection, "Bad Selection Record ");
        assertNotNull(olRec.getEndTime(), "Selection record not closed ");
        assertEquals(olRec.getFilterType(), OperatorLaborFilterType.Selection, "Invalid Filter Type ");
        olRec = olList.get(2);
        assertEquals(olRec.getActionType(), OperatorLaborActionType.Break, "Bad Break Record ");
        assertNull(olRec.getEndTime(), "Break record not opened ");
    }


    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendBreakInfoCmd#testExecute()}.
     * @throws Exception any
     */
    
    @Test()
    public void testExecuteStartSelectionTakeBreak() throws Exception {
        
        // Sign on to the system and choose the selection function,
        // get an assignment, start to pick, then take a break.
        // this should result in 3 operator labor records being created.
        // and one assignment labor record being created and closed.
        // The first operator labor record is a closed sign on record. 
        // The second operator labor record is a closed selection.
        // The third operator labor record is an open break record.

        initialize();
        signOnOperator();
        getPickingRegion();
        getAssignment("1"); // get 1 assignment
        // get the group number that was returned.
        Response assignmentResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        ResponseRecord r =  assignmentResponse.getRecords().get(0);

        getPicks(r.get("groupID").toString());
        pickSomePicks(r.get("groupID").toString(), 3);
        
        //take a break and verify response
        Response response = executeCommand(this.getCmdDateMS(), "1", "0", "1");
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        
        // Get the managers
        LaborManager laborManager = this.cmd.getLaborManager();
        OperatorLaborManager olManager = laborManager.getOperatorLaborManager();
        AssignmentLaborManager alManager = laborManager.getAssignmentLaborManager();
        long opertorId = this.cmd.getOperator().getId();
        
        // Verify that we got three labor records created.
        List<OperatorLabor> olList = olManager.listAllRecordsByOperatorId(opertorId);
        assertEquals(olList.size(), 3, "Bad Operator Labor Record Count ");
        OperatorLabor olRec = olList.get(0);
        assertEquals(olRec.getActionType(), OperatorLaborActionType.SignOn, "Bad Labor Record ");
        assertNotNull(olRec.getEndTime(), "Sign on record not closed ");
        // verify selection labor record
        OperatorFunctionLabor flRec = (OperatorFunctionLabor) olList.get(1);
        assertEquals(flRec.getActionType(), OperatorLaborActionType.Selection, "Bad Labor Record ");
        assertNotNull(flRec.getEndTime(), "Selection record not closed ");
        // verify totals on selection labor record
        assertEquals(flRec.getCount(), new Integer(7), "Quantity picked ");
        if (flRec.getDuration() == 0) {
            fail("Operator Labor duration was zero");
        }
        if (flRec.getActualRate() == 0.0) {
            fail("Operator Labor actual rate was zero");
        }
        if (flRec.getPercentOfGoal() == 0.0) {
            fail("Operator Labor actual rate was zero");
        }
        // verify that we got an open operator break labor record
        OperatorBreakLabor blRec = (OperatorBreakLabor) olList.get(2);
        assertEquals(blRec.getActionType(), OperatorLaborActionType.Break, "Bad Labor Record ");
        assertNull(blRec.getEndTime(), "Break record not opened ");
         
        // Verify that we closed the assignment labor record
        List<AssignmentLabor> alList = alManager.listClosedRecordsByBreakLaborId(blRec.getId());
        assertEquals(alList.size(), 1, "Bad Assignment Labor Record Count ");
        AssignmentLabor alRec = alList.get(0); 
        assertNotNull(alRec.getEndTime(), "Assignment labor record not closed ");
        // Verify that totals were calculated correctly for the labor record.
        if (alRec.getDuration() == 0) {
            fail("Assignment labor duration was zero");
        }
        Integer result = new Integer(7);
        assertEquals(alRec.getQuantityPicked(), result, "Quantity picked ");
        assertEquals(alRec.getAssignmentProrateCount(), result, "AssignmentProrateCount");
        assertEquals(alRec.getGroupProrateCount(), result, "GroupProrateCount ");
        if (alRec.getActualRate() == 0.0) {
            fail("Operator Labor actual rate was zero");
        }
        if (alRec.getPercentOfGoal() == 0.0) {
            fail("Operator Labor actual rate was zero");
        }
        
        // BEGIN VLINK-3585
        // Send the break message again and verify response
        response = executeCommand(this.getCmdDateMS(), "1", "0", "1");
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        // Verify that we did not create a new break labor record by comparing
        // the previously created break labor record to the one retreived by
        // the following call.
        OperatorLabor secondBreakRecord;
        secondBreakRecord = olManager.findOpenRecordByOperatorId(opertorId);
        assertEquals(blRec, secondBreakRecord, "ERROR Break records are not equal");
        // END VLINK-3585
        
        //Return from the break.  Verify that we opened a selection labor record 
        //and an assignment labor record.
        response = executeCommand(this.getCmdDateMS(), "1", "1", "1");
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        // There should now be 4 labor records in operator labor.
        // Verify that the last one is an open selection Selection labor record.  
        olList = olManager.listAllRecordsByOperatorId(opertorId);
        assertEquals(olList.size(), 4, "Bad operator labor count after returning from break ");
        olRec = olList.get(3);
        assertEquals(olRec.getActionType(), OperatorLaborActionType.Selection, "Bad Labor Record ");
        assertNull(olRec.getEndTime(), "Selection record not opened ");
        // Verify that we opened a new assignment labor record.
        alList = alManager.listOpenRecordsByOperatorId(opertorId);
        assertEquals(alList.size(), 1, "Bad Assignment Labor Record Count ");
        alRec = alList.get(0); 
        assertNull(alRec.getEndTime(), "Assignment labor record not opened ");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.VerifyLocationCmd#testExecute()}.
     * @throws Exception any
     */
    

    @Test()
    public void testExecuteGroupSelectionTakeBreak() throws Exception {
  
        
        // Sign on to the system and choose the selection function,
        // Request two assignments start to pick, then take a break.
        // this should result in 3 operator labor records being created.
        // and 2 assignment labor records being created and closed.
        // The first operator labor record is a closed sign on record. 
        // The second operator labor record is a closed selection.
        // The third operator labor record is an open break record.
        
        initializeManual();  // calls request work to reserve 2 assignments.
        getAssignment("2"); // get two assignments.
        // get the group number that was returned.
        Response assignmentResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        ResponseRecord r =  assignmentResponse.getRecords().get(0);
        getPicks(r.get("groupID").toString());
        pickSomePicks(r.get("groupID").toString(), 4);
        
        //take a break and verify response
        Response response = executeCommand(this.getCmdDateMS(), "1", "0", "1");
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        
        // Get the managers
        LaborManager laborManager = this.cmd.getLaborManager();
        OperatorLaborManager olManager = laborManager.getOperatorLaborManager();
        AssignmentLaborManager alManager = laborManager.getAssignmentLaborManager();
        long opertorId = this.cmd.getOperator().getId();
        
        // Verify that we got three labor records created.
        List<OperatorLabor> olList = olManager.listAllRecordsByOperatorId(opertorId);
        assertEquals(olList.size(), 3, "Bad Operator Labor Record Count ");
        OperatorLabor olRec = olList.get(0);
        assertEquals(olRec.getActionType(), OperatorLaborActionType.SignOn, "Bad Labor Record ");
        assertNotNull(olRec.getEndTime(), "Sign on record not closed ");
        OperatorFunctionLabor flRec = (OperatorFunctionLabor) olList.get(1);
        assertEquals(flRec.getActionType(), OperatorLaborActionType.Selection, "Bad Labor Record ");
        assertNotNull(flRec.getEndTime(), "Selection record not closed ");
        OperatorBreakLabor blRec = (OperatorBreakLabor) olList.get(2);
        assertEquals(blRec.getActionType(), OperatorLaborActionType.Break, "Bad Labor Record ");
        assertNull(blRec.getEndTime(), "Break record not opened ");

        // Verify that we closed two assignment labor records
        List<AssignmentLabor> alList = alManager.listClosedRecordsByBreakLaborId(blRec.getId());
        assertEquals(alList.size(), 2, "Bad Assignment Labor Record Count ");
        AssignmentLabor alRec = alList.get(0); 
        assertNotNull(alRec.getEndTime(), "Assignment labor record not closed ");
        // Verify that totals were calculated correctly for the labor record.
        if (alRec.getDuration() == 0) {
            fail("duration was zero");
        }
        assertEquals(alRec.getQuantityPicked(), new Integer(1), "Quantity picked ");
        alRec = alList.get(1); 
        assertNotNull(alRec.getEndTime(), "Assignment labor record not closed ");
        // Verify that totals were calculated correctly for the labor record.
        if (alRec.getDuration() == 0) {
            fail("duration was zero");
        }
        assertEquals(alRec.getQuantityPicked(), new Integer(3), "Quantity picked ");

        //Return from the break.  Verify that we opened a selection labor record 
        //and an assignment labor record.
        response = executeCommand(this.getCmdDateMS(), "1", "1", "1");
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        // There should now be 4 labor records in operator labor.
        // Verify that the last one is an open selection Selection labor record.  
        olList = olManager.listAllRecordsByOperatorId(opertorId);
        assertEquals(olList.size(), 4, "Bad operator labor count after returning from break ");
        olRec = olList.get(3);
        assertEquals(olRec.getActionType(), OperatorLaborActionType.Selection, "Bad Labor Record ");
        assertNull(olRec.getEndTime(), "Selection record not opened ");
        // Verify that we opened two new assignment labor records.
        alList = alManager.listOpenRecordsByOperatorId(opertorId);
        assertEquals(alList.size(), 2, "Bad Assignment Labor Record Count ");
        alRec = alList.get(0); 
        assertNull(alRec.getEndTime(), "Assignment labor record not opened ");
    }  
    
    /**
     * Picks all picks in specified assignment.
     * 
     * @param groupNumber - group number to fetch picks for.
     * @param howMany - number of picks to short.
     * 
     * @throws Exception - Any Exception
     */
    private void pickSomePicks(String groupNumber, int howMany) throws Exception {
 
        int pickCount = 1;
        String quantityToPick = "0";
        
        Response pickResponse = getResponseForCommand("cmdPrTaskLUTGetPicks");
        for (ResponseRecord r : pickResponse.getRecords()) {
             if (pickCount++ <= howMany) {
                quantityToPick = r.get("quantityToPick").toString();

                executeLutCmd("cmdPrTaskLUTPicked",
                    new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, 
                                  groupNumber, //group number
                                  r.get("assignmentPK").toString(), //Assignment ID 
                                  r.get("locationID").toString(), //Location ID 
                                  quantityToPick, //Quantity Picked 
                                  "1", //End of Pick
                                  "", //Container ID
                                  r.get("pickID").toString(), //Pick ID 
                                  "", //Lot Number
                                  "", //variable Weight
                                  "" //Serial Number
                                  }); 
            }       
        }
    }
    
    
    /**
     * Prepares data to run test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        
        //Clear Database and load data
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();
    }
    
    
    /**
     * Prepares data to run test.
     * 
     * @throws Exception - Exception
     */
   
    private void initializeManual() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();
        
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, "3"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, "13", "3"});
        //Request two assignments
        executeLutCmd("cmdPrTaskLUTRequestWork", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, "7285", "1", "1"});
        executeLutCmd("cmdPrTaskLUTRequestWork", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, "7289", "1", "1"});
       
    }
    
    
    
    /**
     * Get core configuration and sign on the operator.
     * 
     * @throws Exception - Exception
     */
    private void signOnOperator() throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
            makeStringFromDateMS(this.getCmdDateMS()), sn, operId, "1234" });
    }
    
    
    /**
     * Gets the picking region by calling prTaskLUTPickingRegion.
     * Calling this procedure causes a selection labor record to be created.
     * 
     * @throws Exception - Exception
     */
    private void getPickingRegion() throws Exception {
        
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, "3"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDateMS(this.getCmdDateMS()), sn, operId, "2", "3" });
    }
    
    
    
    /**
     * Gets an assignment by calling prTaskLUTPickingRegion.
     * Calling this procedure causes assignment labor records to be created.
     * @param numberOfAssignments - the number of assignments to get
     * @throws Exception - Exception
     */
    private void getAssignment(String numberOfAssignments) throws Exception {
        
        executeLutCmd("cmdPrTaskLUTGetAssignment", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, numberOfAssignments, "1"});
    }
    
    /**
     * Gets picks by calling prTaskLutGetPicks.
     * 
     * 
     * @param groupNumber - the group number
     * @throws Exception - Exception
     */
    private void getPicks(String groupNumber) throws Exception {
    
        executeLutCmd("cmdPrTaskLUTGetPicks",
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, 
           groupNumber, "0", "1", "0"});
    }
    
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param breakType - break type operator selected
     * @param startEndFlag - 0 = star break, 1 = end break
     * @param description - does not appear to be used
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String breakType, 
                                    String startEndFlag, 
                                    String description) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDateMS(cmdDate), sn, operId, 
                breakType, startEndFlag, description}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
