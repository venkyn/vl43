/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationDetail;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for PostNotificationCmd.
 *
 * @author estoll
 */
@Test(groups = { FAST, TASK, CORE })

public class PostNotificationCmdTest extends TaskCommandTestCase {

    // injected
    private NotificationManager notificationManager;
    
    private PostNotificationCmd cmd;
    private String operId = "PostNotificationOper";
    private String sn = "PostNotificationSerial";

    /**
     * @return a bean for testing.
     */
    private PostNotificationCmd getCmdBean() {
        return (PostNotificationCmd) getBean("cmdPrTaskODRCorePostNotification");
    }
    
    /**
     * Auto Setup notificationManager by Spring.
     * @param nManager - the notification manager
     */
    @Test(enabled = false)
    public void setNotificationManager(NotificationManager nManager) {
        this.notificationManager = nManager;
    }
    
    /**
     * Getter for the notificationManager property.
     * @return the value of the property
     */
    private NotificationManager getNotificationManager() {
        return this.notificationManager;
    }
    
     
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetReasonCodesCmd#testExecute()}.
     * @throws Exception any
     */
    public void testExecute() throws Exception {  

        initialize();
        //Test posting a valid notification
        String detailString = "task.license=1234" + this.cmd.getPairDelimiter()
            + "task.version=yyyy" + this.cmd.getPairDelimiter() + "a=b";
        
        executeCommand(this.getCmdDateSec(), "pts.unexpected.residuals", "1", detailString);

        // Get the first Notification
        List<Notification> nList = notificationManager.getAll();
        Notification first = nList.get(0);
        Set<NotificationDetail> details = first.getDetails();
        assertEquals(6, details.size(), "Good details - map size is wrong");
        assertEquals(first.getPriority().toString(), "WARNING", "Good detail - priority is wrong");

        // blow it away - we're done with it
        getNotificationManager().delete(first.getId());

        //Test mal-formed notification
        executeCommand(this.getCmdDateSec(), "pts.unexpected.residuals", "1", "task.license");

        // Get the first Notification
        nList = notificationManager.getAll();
        first = nList.get(0);
        details = first.getDetails();
        assertEquals(details.size(), 4, "Malformed detail - map size is wrong");
        // blow it away - we're done with it
        getNotificationManager().delete(first.getId());
        //Test no details -- should still get 3
        executeCommand(this.getCmdDateSec(), "pts.unexpected.residuals",  "1", "");
        // Get the first Notification
        nList = notificationManager.getAll();
        first = nList.get(0);
        details = first.getDetails();
        assertEquals(details.size(), 3, "no detail parameters - map size is wrong");
        // blow it away - we're done with it
        getNotificationManager().delete(first.getId());
    }
    

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
    }
    
    /**
     * Execute the command.
     * @param cmdDate string of datetime
     * @param messageKey a string of a resource key
     * @param priority a string of a notification priority
     * @param detailPairs pipe delimited, key=value pairs
     * @throws Exception
     */
    private void executeCommand(Date cmdDate,
                                    String messageKey,
                                    String priority,
                                    String detailPairs) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId, messageKey, 
                            priority, detailPairs}, this.cmd);
        getTaskCommandService().executeCommand(this.cmd);
    }
}
