/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorStatus;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.task.GetAssignmentCmd;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for SignOffCmd.
 *
 * @author Mark Koenig
 */
@Test(groups = { FAST, TASK, CORE })

public class SignOffCmdTest extends TaskCommandTestCase {

    private SignOffCmd cmd;
    private String operId = "SignOffOper";
    private String sn = "SignOffSerial";
    
    /**
     * @return a bean for testing.
     */
    private SignOffCmd getCmdBean() {
        return (SignOffCmd) getBean("cmdPrTaskLUTCoreSignOff");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SignOffCmd#testExecute()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();
        //Test Sign operator off
        Date testDate = this.getCmdDateSec();
        Response response = executeCommand(testDate);

        //Verify Operator in Database Update Correctly
        Operator oper = this.cmd.getOperatorManager().findByIdentifier(operId);

        assertEquals(makeStringFromDate(testDate), 
            makeStringFromDate(oper.getSignOffTime()), "SignOffTime");
        assertEquals(OperatorStatus.SignedOff, oper.getStatus(), "SignOffStatus");
        
        //Verify Successful return results
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        
        //Test Already Signed off
        response = executeCommand(this.getCmdDateSec());
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), "97", "ErrorCodeResponse");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SignOffCmd#testExecuteSuspendAssignments()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testExecuteSuspendAssignments() throws Exception {

        //Call commands
        initialize();
        initializeSelection();

        //Test Sign operator off
        Response response = executeCommand(this.getCmdDateSec());
        
        //Verify Successful return results
        List<ResponseRecord> records = response.getRecords();
        assertEquals(1, records.size(), "recordSize");
        
        GetAssignmentCmd testCmd = (GetAssignmentCmd) 
            getBean("cmdPrTaskLUTGetAssignment");

        Operator oper = this.cmd.getOperatorManager().findByIdentifier(operId);
        List<Assignment> a = testCmd.getAssignmentManager().listActiveAssignments(oper);
        assertEquals(a.size(), 1, "OneAssignment");
        assertEquals(a.get(0).getStatus(), AssignmentStatus.Suspended, "AssignmentSuspended");
    }

    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initializeSelection() throws Exception {
        final Locale locale = Locale.US;

        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();
        
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "3" });
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "2", "3" });
        executeLutCmd("cmdPrTaskLUTGetAssignment", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1" });
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
