/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.voicelink.core.model.Device;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for GetConfigurationCmd.
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, TASK, CORE })

public class GetConfigurationCmdTest extends TaskCommandTestCase {

    private GetConfigurationCmd cmd;

    /**
     * @return a bean for testing.
     */
    private GetConfigurationCmd getCmdBean() {
        return (GetConfigurationCmd) getBean("cmdPrTaskLUTCoreConfiguration");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetConfigurationCmd#getLocale()}.
     */
    @Test()
    public void testGetSetLocale() {
        this.cmd.setLocale(Locale.US);
        assert Locale.US.equals(this.cmd.getLocale());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetConfigurationCmd#getLocale()}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {
        final String sn = "GetConfigurationCmdSerial";
        final String operId = "GetConfigurationCmdOper";
        //Failing to change this between task version will result in a failure of 
        //assertNotNull(record.get("customerName"), "customerName");
        String version = "CT-31-03-072";
        Locale locale = Locale.US;
        Device term;
        Operator oper;
        
        // First determine whether or not the terminal and operator 
        // are already in the DB.
        term = this.cmd.getTerminalManager().findBySerialNumber(sn);
        oper = this.cmd.getOperatorManager().findByIdentifier(operId);
        boolean operExisted = oper != null;
        
        // Now put args in the command and execute it.
        getCommandDispatcher().mapArgumentsToCommand(
                new String[] {makeStringFromDate(this.getCmdDateSec()), 
                sn, operId, locale.toString(), "Default", version}, this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);
        
        // Make sure the response matched basic criteria
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertNotNull(record.get("customerName"), "customerName");
        assertEquals((String) record.get("operatorId"), operId, "operatorid");
        if (operExisted) {
            // Since the operator isn't new, this field should be 0
            assertEquals(record.get("confirmPassword"), 0, "Password");
        } else {
            // Since the operator is new, this field should be 2
            assertEquals(record.get("confirmPassword"), 
                    GetConfigurationCmd.NEW_OPERATOR_CONFIRM_PASSWORD, "confirm Password");
        }
        
        // Device should now exist (if it didn't), and have correct Locale
        term = this.cmd.getTerminalManager().findBySerialNumber(sn);
        assertNotNull(term, "Null terminal");
        assertEquals(term.getTaskLocale(), locale, "locale");
        
        // Try it again, with a different locale
        this.cmd = getCmdBean();
        locale = Locale.CANADA;
        getCommandDispatcher().mapArgumentsToCommand(
                new String[] {makeStringFromDate(this.getCmdDateSec()), 
                sn, operId, locale.toString(), "Default", version}, this.cmd);
        getTaskCommandService().executeCommand(this.cmd);
        term = this.cmd.getTerminalManager().findBySerialNumber(sn);
        assertEquals(term.getTaskLocale(), locale, "Locale Canada"); 
        
        // Try it again, with a different locale
        this.cmd = getCmdBean();
        locale = Locale.US;
        getCommandDispatcher().mapArgumentsToCommand(
                new String[] {makeStringFromDate(this.getCmdDateSec()), 
                sn, operId, locale.toString(), "Default-fail", version}, this.cmd);
        response = getTaskCommandService().executeCommand(this.cmd);

        records = response.getRecords();
        assertEquals(1, records.size(), "recordSize");
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()),
                TaskErrorCode.SITE_UNDEFINED.getErrorCode(), "Invalid site ");

        // Try it again, with a task version string that 
        // does not have 4 parts
        this.cmd = getCmdBean();
        locale = Locale.US;
        version = "${ct.ver}-XXX";
        getCommandDispatcher().mapArgumentsToCommand(
                new String[] {makeStringFromDate(this.getCmdDateSec()), 
                sn, operId, locale.toString(), "Default", version}, this.cmd);
        response = getTaskCommandService().executeCommand(this.cmd);

        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), "98", "Task Version Parts");
        
        // Try it again, with an incompatible TaskVersion
        this.cmd = getCmdBean();
        locale = Locale.US;
        version = "CT-30-99-015";
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), 
                sn, operId, locale.toString(), "Default", version}, this.cmd);
        response = getTaskCommandService().executeCommand(this.cmd);

        records = response.getRecords();
        assertEquals(1, records.size(), "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), "98", "Work Task Version ");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetConfigurationCmd#getLocale()}.
     * @throws Exception any
     */
    @Test(enabled = false)
    public void testCustomVersionStringExecute() throws Exception {
        /*
         * Before executing this test, 
         * disable the test testExecute 
         * and enable this one
         * 
         * Then set TaskVersion.properties
         * ct.ver=CT-30-01.01
         * ct.minVersion=01.01
         * ct.maxVersion=01.01
         */
        final String sn = "serialConfig";
        final String operId = "operConfig";
        String version = "CT-30-01.01-015";
        Locale locale = Locale.US;
        Device term;
        Operator oper;
        
        // First determine whether or not the terminal and operator 
        // are already in the DB.
        term = this.cmd.getTerminalManager().findBySerialNumber(sn);
        oper = this.cmd.getOperatorManager().findByIdentifier(operId);
        boolean operExisted = oper != null;
        
        // Now put args in the command and execute it.
        getCommandDispatcher().mapArgumentsToCommand(
                new String[] {makeStringFromDate(this.getCmdDateSec()), 
                sn, operId, locale.toString(), "Default", version}, this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);
        
        // Make sure the response matched basic criteria
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertNotNull(record.get("customerName"), "customerName");
        assertEquals((String) record.get("operatorId"), operId, "Operatorid");
        if (operExisted) {
            // Since the Operator isn't new, this field should be 0
            assertEquals(record.get("confirmPassword"), 0, "Confirm password");
        } else {
            // Since the Operator is new, this field should be 2
            assertEquals(record.get("confirmPassword"), 
                    GetConfigurationCmd.NEW_OPERATOR_CONFIRM_PASSWORD,
                    "Confirm Password");
        }
        
        // Device should now exist (if it didn't), and have correct Locale
        term = this.cmd.getTerminalManager().findBySerialNumber(sn);
        assertNotNull(term, "Null terminal");
        assertEquals(term.getTaskLocale(), locale, "Locale");
        
        // Try it again, with an incompatible TaskVersion
        this.cmd = getCmdBean();
        locale = Locale.US;
        version = "CT-30-01.02-015";
        getCommandDispatcher().mapArgumentsToCommand(
                new String[] {makeStringFromDate(this.getCmdDateSec()), 
                sn, operId, locale.toString(), "Default", version}, this.cmd);
        response = getTaskCommandService().executeCommand(this.cmd);

        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), "98", "Work Task Version ");
    }
    
}
