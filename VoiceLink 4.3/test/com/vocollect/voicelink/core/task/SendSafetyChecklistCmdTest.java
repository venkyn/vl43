/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.service.VehicleManager;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for SendSafetyChecklistCmd.
 *
 * @author kudupi
 */
@Test(groups = { FAST, TASK, CORE, VSC })

public class SendSafetyChecklistCmdTest extends TaskCommandTestCase {

    private SendSafetyChecklistCmd cmd;
    
    private VehicleManager vehicleManager;
    /**
     * @return a bean for testing.
     */
    private SendSafetyChecklistCmd getCmdBean() {
        return (SendSafetyChecklistCmd) 
            getBean("cmdPrTaskLUTCoreSafetyCheck");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendSafetyChecklistCmd#doExecute()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testExecuteAllSafetyChecksPassed() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_Core_VSC_data.xml");
        
        final String operId = "SendSafetyChecklistOper";
        String sn = "SendSafetyChecklistSerial";

        initialize(sn, operId);
        
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "", "0"}, 
            this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendSafetyChecklistCmd#doExecute()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testExecuteQuickRepairScenario() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_Core_VSC_data.xml");
        
        final String operId = "SendSafetyChecklistOper";
        String sn = "SendSafetyChecklistSerial";

        initialize(sn, operId);
        
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "0", "Tires", "2"}, 
            this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendSafetyChecklistCmd#doExecute()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testExecuteQuickRepairFailed() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_Core_VSC_data.xml");
        
        final String operId = "SendSafetyChecklistOper";
        String sn = "SendSafetyChecklistSerial";

        initialize(sn, operId);
        
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "0", "Tires", "1"}, 
            this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        
        //Vehicle -operator association should be wiped off onb vehicle failure
        Vehicle vehicle = vehicleManager.get(-12L);
        assertTrue(vehicle.getOperators().isEmpty());
    }    

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendSafetyChecklistCmd#doExecute()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testVLINK4755() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_Core_VSC_data.xml");
        
        final String operId = "SendSafetyChecklistOper";
        String sn = "SendSafetyChecklistSerial";

        initialize(sn, operId);
        
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "-2", "Tires", "2"}, 
            this.cmd);
        getTaskCommandService().executeCommand(this.cmd);
        
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "-2", "Tires", "1"}, 
            this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
    }  
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @param sn - Serial Number
     * @param operId - Operator
     * @throws Exception - Exception
     */
    private void initialize(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTCoreValidVehicleTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "0"});
        executeLutCmd("cmdPrTaskLUTCoreSendVehicleIDs", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "7", "TT02"});
        
    }

    
    /**
     * @return the vehicleManager
     */
    @Test(enabled = false)
    public VehicleManager getVehicleManager() {
        return vehicleManager;
    }

    
    /**
     * @param vehicleManager the vehicleManager to set
     */
    @Test(enabled = false)
    public void setVehicleManager(VehicleManager vehicleManager) {
        this.vehicleManager = vehicleManager;
    }
    
    
}
