/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for SendVehicleIDsCmd.
 *
 * @author kudupi
 */
@Test(groups = { FAST, TASK, CORE, VSC })

public class SendVehicleIDsCmdTest extends TaskCommandTestCase {

    private SendVehicleIDsCmd cmd;
    
    /**
     * @return a bean for testing.
     */
    private SendVehicleIDsCmd getCmdBean() {
        return (SendVehicleIDsCmd) 
            getBean("cmdPrTaskLUTCoreSendVehicleIDs");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendVehicleIDsCmd#doExecute()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testExecuteWithVehicleIDExists() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_Core_VSC_data.xml");
        
        final String operId = "SendVehicleIDsOper";
        String sn = "SendVehicleIDsSerial";

        initialize(sn, operId);
        
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "7", "TT02"}, 
            this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 5, "recordSize");
        // Checking the order in which safety checks are retrieved.
        assertEquals(records.get(0).get("safetyCheck"), "Breaks");
        assertEquals(records.get(1).get("safetyCheck"), "Tires");
        assertEquals(records.get(2).get("safetyCheck"), "Mirrors");
        assertEquals(records.get(3).get("safetyCheck"), "Miles run");
        assertEquals(records.get(3).get("responseType"), "N");
        assertEquals(records.get(4).get("safetyCheck"), "Lights");
        assertEquals(records.get(4).get("responseType"), "B");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendVehicleIDsCmd#doExecute()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testExecuteWithVehicleIDIsBlank() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_Core_VSC_data.xml");
        
        final String operId = "SendVehicleIDsOper";
        String sn = "SendVehicleIDsSerial";

        initialize(sn, operId);
        
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "7", ""}, 
            this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 5, "recordSize");
        // Checking the order in which safety checks are retrieved.
        assertEquals(records.get(0).get("safetyCheck"), "Breaks");
        assertEquals(records.get(1).get("safetyCheck"), "Tires");
        assertEquals(records.get(2).get("safetyCheck"), "Mirrors"); 
        assertEquals(records.get(3).get("safetyCheck"), "Miles run");
        assertEquals(records.get(3).get("responseType"), "N");
        assertEquals(records.get(4).get("safetyCheck"), "Lights");
        assertEquals(records.get(4).get("responseType"), "B");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendVehicleIDsCmd#doExecute()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testExecuteWithVehicleIDNotExists() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_Core_VSC_data.xml");
        
        final String operId = "SendVehicleIDsOper";
        String sn = "SendVehicleIDsSerial";

        initialize(sn, operId);
        
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "7", "1234"}, 
            this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);
        assertEquals(response.getRecords().get(0).getErrorCode(),
            TaskErrorCode.VEHICLE_NOT_FOUND.getErrorCode() + "");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendVehicleIDsCmd#doExecute()}.
     * @throws Exception - Exception
     */
    @Test()
    public void testVLINK4741() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_Core_VSC_data.xml");
        
        final String operId = "SendVehicleIDsOper";
        String sn = "SendVehicleIDsSerial";

        initialize(sn, operId);
        
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "5", "PPT02"}, 
            this.cmd);
        Response response = getTaskCommandService().executeCommand(this.cmd);

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        // Checking that safety value is "".
        assertEquals(records.get(0).get("safetyCheck"), "");
    }
    
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @param sn - Serial Number
     * @param operId - Operator
     * @throws Exception - Exception
     */
    private void initialize(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTCoreValidVehicleTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "0"});
        
    }
}
