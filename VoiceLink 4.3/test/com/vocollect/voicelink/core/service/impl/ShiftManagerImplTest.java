/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.model.Shift;
import com.vocollect.voicelink.core.model.ShiftTime;
import com.vocollect.voicelink.core.service.ShiftManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.epp.test.TestGroups.SHIFT;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;

import org.testng.annotations.Test;

/**
 * Unit test class for testing shift manager.
 * @author smittal
 * 
 */
@Test(groups = { FAST, SECURITY, CORE, SHIFT })
public class ShiftManagerImplTest extends BaseServiceManagerTestCase {

    private ShiftManager shiftManager = null;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#
     * initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Core_Shift_data.xml");
    }

    /**
     * Set the site context.
     * @param siteId - the site ID
     * @throws Exception
     * 
     */
    protected void setSiteContext(Long siteId) throws Exception {
        this.setUpSiteContextNoDefaultSite(true);
        SiteContextHolder.getSiteContext().setCurrentSite(siteId);
    }

    /**
     * Getter for the shiftManager property.
     * @return ShiftManager value of the property
     */
    @Test(enabled = false)
    public ShiftManager getShiftManager() {
        return shiftManager;
    }

    /**
     * Setter for the shiftManager property.
     * @param shiftManager the new shiftManager value
     */
    @Test(enabled = false)
    public void setShiftManager(ShiftManager shiftManager) {
        this.shiftManager = shiftManager;
    }
    
    @Test(enabled = false)
    public void testGetAllShifts() {
        try {
            List<Shift> shifts = this.shiftManager.getAll();

            // We are not testing how many shifts have been returned just
            // because
            // the data XML file will keep growing and we have to maintain this
            // test case very time we update the data XML
            assert (shifts.size() > 0);
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }

    @Test(enabled = false)
    public void testUniquenessByName() {

        ShiftTime shiftTime = new ShiftTime();
        shiftTime.setHours(0);
        shiftTime.setMinutes(45);

        Shift shift1 = new Shift();

        shift1.setName("shift");
        shift1.setStartTime(shiftTime);
        shift1.setEndTime(shiftTime);

        try {
            this.shiftManager.verifyUniquenessByName(shift1);
        } catch (Exception e) {
            assert (e instanceof FieldValidationException);
            fail(e.getLocalizedMessage(), e);
        }
    }

    @Test(enabled = false)
    public void testSave() throws Exception {
        setSiteContext(-1L);

        ShiftTime shiftTime = new ShiftTime();
        shiftTime.setHours(0);
        shiftTime.setMinutes(45);

        Shift shift1 = null;

        try {
            shift1 = new Shift();

            shift1.setName("shift");
            shift1.setStartTime(shiftTime);
            shift1.setEndTime(shiftTime);

            this.shiftManager.save(shift1);

            getShiftManager().getPrimaryDAO().flushSession();
            getShiftManager().getPrimaryDAO().clearSession();

            List<Shift> shifts = getShiftManager().getAll();

            // Check 1 of the shifts in the system should match the above
            // persisted
            for (Shift shift : shifts) {
                if (shift.getName().equals("shift")) {
                    assertEquals(shift.getName(), shift.getName());
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        } finally {
            // Clean
            try {
                getShiftManager().delete(shift1);
                getShiftManager().getPrimaryDAO().flushSession();
                getShiftManager().getPrimaryDAO().clearSession();
            } catch (Exception e) {

            }
        }

    }
}
