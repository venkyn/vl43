/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import org.testng.annotations.Test;
/**
 * 
 * @author asinha
 */

@Test(groups = { FAST, SECURITY, CORE })
public class SiteManagerImplTest extends BaseServiceManagerTestCase {

    private SiteManager siteManager;

    /**
     * {@inheritDoc}
     * 
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {

    }

    /**
     * Set the manager.
     * 
     * @param impl The manager to use.
     */
    @Test(enabled = false)
    public void setSiteManager(SiteManager siteManager) {
        this.siteManager = siteManager;
    }

    /**
     * Test 'com.vocollect.voicelink.core.service.impl.SiteManagerImpl.save' to
     * create a site.
     * 
     * @throws BusinessRuleException
     * @throws DataAccessException - database exceptions.
     */
    @Test
    public void testSaveDeleteSite() throws DataAccessException,
        BusinessRuleException {
        String siteName = "Test Site Save";
        
        Site site1 = siteManager.findByName(siteName);

        assertNull(site1);

        Site newSite = new Site();
        newSite.setName(siteName);
        siteManager.save(newSite);
        
        site1 = siteManager.findByName(siteName);
        
        assertNotNull(site1);
        assertEquals(site1.getName(), siteName);
        
        siteManager.delete(site1);
        
        site1 = siteManager.findByName(siteName);

        assertNull(site1);
    }
}
