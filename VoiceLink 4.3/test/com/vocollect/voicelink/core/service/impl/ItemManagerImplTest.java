/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.service.ItemManager;

import static com.vocollect.epp.test.TestGroups.PERFORMANCE;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.testng.annotations.Test;

/**
 * 
 * 
 * @author snalan
 */
@Test(groups = { CORE })
public class ItemManagerImplTest extends BaseServiceManagerTestCase {

    private ItemManager manager = null;

    private int recordCount = 50;

    /**
     * @param impl - the impl
     */
    @Test(enabled = false)
    public void setItemManager(ItemManager impl) {
        this.manager = impl;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        this.recordCount = getRecordCount();
        adapter.resetInstallationData();

    }

    /**
     * @throws Exception - any exception
     */
    @Test(groups = { PERFORMANCE })
    public void testSaveItems() throws Exception {

        for (int i = 1; i <= recordCount; i++) {
            Item item = getItemObj(i);
            manager.save(item);
        }
        logTimeTaken("Time taken to save " + recordCount + " Item records : ");

    }

    /**
     * @throws Exception - any exception
     */
    @Test(groups = { PERFORMANCE })
    public void testSaveAllItems() throws Exception {
        List<Item> itemList = new ArrayList<Item>();
        for (int i = 1; i <= recordCount; i++) {
            Item item = getItemObj(i);
            itemList.add(item);
        }
        manager.save(itemList);
        logTimeTaken("Time taken to saveAll " + recordCount
                                        + " Item records : ");

    }

    /**
     * @throws Exception - any exception
     */
    @Test(groups = { PERFORMANCE }, dependsOnMethods = { "testSaveItems" })
    public void testGetItems() throws Exception {

        manager.getAll();
        logTimeTaken("Time taken to retreive " + recordCount
            + " Item records : ");
    }

    /**
     * @throws Exception - any exception
     */
    @Test(groups = { PERFORMANCE }, dependsOnMethods = { "testGetItems" })
    public void testDeleteItems() throws Exception {

        List<Item> items = manager.getAll();
        for (Iterator<Item> itr = items.iterator(); itr.hasNext();) {
            Item item = itr.next();
            manager.delete(item.getId());
        }
        logTimeTaken("Time taken to delete " + recordCount + " Item records : ");
    }

    /**
     * @param number - the number
     * @return Item obj
     */
    @Test(enabled = false)
    private Item getItemObj(int number) {

        Item item = new Item();
        item.setCreatedDate(new Date());
        item.setCube(new Double(10));
        item.setDescription("Description");
        item.setIsVariableWeightItem(false);
        item.setNumber("" + number);
        item.setPack("Pack");
        item.setPhoneticDescription("Phonetic Descriptiona");
        item.setScanVerificationCode("ScanVerificationCode");
        item.setSize("Size");
        item.setSpokenVerificationCode("SVCdd");
        item.setUpc("Upc");
        item.setVariableWeightTolerance(10);
        item.setVersion(10);
        item.setWeight(new Double(10));

        return item;

    }

    /**
     * @return the record count
     */
    private int getRecordCount() {
        String recordNumber = System.getProperty("test.parameter.records");
        int recCount = 0;
        try {
            recCount = Integer.parseInt(recordNumber);
        } catch (NumberFormatException ex) {
            recCount = recordCount;
        }

        return recCount;
    }

}
