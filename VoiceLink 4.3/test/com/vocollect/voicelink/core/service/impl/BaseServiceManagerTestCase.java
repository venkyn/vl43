/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;

import static com.vocollect.epp.test.TestGroups.PERFORMANCE;

import org.testng.annotations.BeforeMethod;

/**
 * A base test case for ServiceManagers (i.e. descendents of
 * com.vocollect.epp.service.GenericManager). This base class will initialize a
 * DbUnitAdapter loading sample data. Override the abstract initData() to do so.
 * 
 * @author bnorthrop
 */
public abstract class BaseServiceManagerTestCase extends VoiceLinkDAOTestCase {

    /**
     * The time the test case was started (only for PERFORMANCE test cases.
     */
    private long startTime;

    /**
     * 1000 milliseconds = 1 second.
     */
    private static final int ONE_SECOND = 1000;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter dbUnitAdapter = new DbUnitAdapter();
        initData(dbUnitAdapter);
    }

    /**
     * Override this method if any specific test data needs to be loaded before
     * the test class is run. This method will be called before the test class
     * is loaded, by classSetUp().
     * @param adapter - use to load dbunit data
     * @throws Exception - because of DBUnitAdapter
     */
    protected abstract void initData(DbUnitAdapter adapter) throws Exception;

    /**
     * Set the start time of a test case.
     */
    @BeforeMethod(groups = { PERFORMANCE })
    protected final void startTime() {
        startTime = System.currentTimeMillis();
    }

    /**
     * Call this method at the end of a Test Case to log the time it took to
     * execute the test case.
     * @param message - the message to be displayed
     */
    protected final void logTimeTaken(String message) {
        long endTime = System.currentTimeMillis();
        Long diff = endTime - startTime;
        logger.info(message + ": " + diff.floatValue() / ONE_SECOND
            + " seconds");
    }

}
