/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.model.Lot;
import com.vocollect.voicelink.core.service.ItemManager;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.core.service.LotManager;

import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;

import org.testng.annotations.Test;


/**
 * Unit tests for lot manager. 
 *
 * @author mkoenig
 */
@Test(groups = { CORE })
public class LotManagerImplTest extends BaseServiceManagerTestCase {

    private LotManager lotManager;
    private LocationManager locationManager;
    private ItemManager itemManager;
    
    /**
     * @param lotManager - the lot manager
     */
    @Test(enabled = false)
    public void setLotManager(LotManager lotManager) {
        this.lotManager = lotManager;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
    }

    /**
     * @throws Exception
     */
    public void testValidations() throws Exception {

        resetCoredata();
        
        //Test Empty lot on create
        try {
            lotManager.executeCreateUpdateLot("", "itemNumber", "locationId", 
                    "speakableLotNumber", new Date());
            assertTrue(false, "should have thrown error");
        } catch (BusinessRuleException e) {
            assertEquals(CoreErrorCode.LOT_NUMBER_EMTPY, 
                e.getErrorCode(), "Expected Empty Lot Number error code");
        }

        //test lot number too long
        try {
            lotManager.executeCreateUpdateLot("123456789012345678901234567890123456789012345678901", 
                    "itemNumber", "locationId", 
                    "speakableLotNumber", new Date());
            assertTrue(false, "should have thrown error");
        } catch (BusinessRuleException e) {
            assertEquals(CoreErrorCode.LOT_NUMBER_TO_LONG, 
                e.getErrorCode(), "Expected Lot to long  error code");
        }

        //test empty speakable lot number
        try {
            lotManager.executeCreateUpdateLot("lotNumber", "itemNumber", 
                "locationId", "", new Date());
            assertTrue(false, "should have thrown error");
        } catch (BusinessRuleException e) {
            assertEquals(CoreErrorCode.SPEAKABLE_LOT_NUMBER_EMTPY, 
                e.getErrorCode(), "Expected Empty Speakable Lot Number error code");
        }
        
        //test speakable lot number too long
        try {
            lotManager.executeCreateUpdateLot("lotNumber", "itemNumber", 
                "locationId", "123456789012345678901234567890123456789012345678901", 
                new Date());
            assertTrue(false, "should have thrown error");
        } catch (BusinessRuleException e) {
            assertEquals(CoreErrorCode.SPEAKABLE_LOT_NUMBER_TO_LONG, 
                e.getErrorCode(), "Expected Speakable Lot to long  error code");
        }
        
        //test item number not found
        try {
            lotManager.executeCreateUpdateLot("lotNumber", "itemNumber", 
                "locationId", "speakableLotNumber", new Date());
            assertTrue(false, "should have thrown error");
        } catch (BusinessRuleException e) {
            assertEquals(CoreErrorCode.ITEM_NOT_FOUND, 
                e.getErrorCode(), "Expected Item not found error code");
        }
        
        //test location number not found
        try {
            lotManager.executeCreateUpdateLot("lotNumber", "99838", 
                "locationId", "speakableLotNumber", new Date());
            assertTrue(false, "should have thrown error");
        } catch (BusinessRuleException e) {
            assertEquals(CoreErrorCode.LOCATION_NOT_FOUND, 
                e.getErrorCode(), "Expected Location not found error code");
        }
        
        //test lot not found
        try {
            lotManager.executeDeleteLot("123", "456", "L789");
            assertTrue(false, "should have thrown error");
        } catch (BusinessRuleException e) {
            assertEquals(CoreErrorCode.LOT_NOT_FOUND, 
                e.getErrorCode(), "Expected lot not found error code");
        }
        
    }

    /**
     * test the create update and delete of lot numbers.
     * 
     * @throws Exception
     */
    public void testCreateAndDelete() throws Exception {
        String lotNumber = new Date().toString();
        String item = "99838";
        String location = "0814207";
        Date expire = new Date();
        
        Lot lot = null;
        
        //reset data
        resetCoredata();
        
        //Make sure the initial lot counts are 0
        assertEquals(itemManager.findItemByNumber(item).getLotCount(), 0);
        assertEquals(locationManager.findLocationByScanned(location).getLotCount(), 0);

        //Create with a location
        lotManager.executeCreateUpdateLot(lotNumber, item, location, 
            "speakableLotNumber", expire);

        lot = lotManager.findLotByNumberItemLocation(lotNumber, item, location);
        assertEquals(lotNumber, lot.getNumber(), "lot number not correct");
        assertEquals(item, lot.getItem().getNumber(), "lot item not correct");
        assertEquals(location, lot.getLocation().getScannedVerification(), 
            "lot location not correct");
        assertEquals("speakableLotNumber", lot.getSpeakableNumber(), 
            "speakbale lot number not correct");
        assertTrue(lot.getExpirationDate() != null, "expiration date not saved");
        assertEquals(lot.getItem().getLotCount(), 1);
        assertEquals(lot.getLocation().getLotCount(), 1);
        
        //create without a location
        lotManager.executeCreateUpdateLot(lotNumber, item, 
            null, "speakableLotNumber", null);
        
        lot = lotManager.findLotByNumberItemLocation(lotNumber, item, null);
        assertEquals(lotNumber, lot.getNumber(), "lot number not correct");
        assertEquals(item, lot.getItem().getNumber(), "lot item not correct");
        assertTrue(lot.getLocation() == null, "lot location is null");
        assertEquals("speakableLotNumber", lot.getSpeakableNumber(), 
            "speakbale lot number not correct");
        assertTrue(lot.getExpirationDate() == null, "expiration not null");
        assertEquals(lot.getItem().getLotCount(), 2);
        
        
        //update with location
        lotManager.executeCreateUpdateLot(lotNumber, item, 
            location, "new speakableLotNumber", null);
        
        lot = lotManager.findLotByNumberItemLocation(lotNumber, item, location);
        assertEquals("new speakableLotNumber", lot.getSpeakableNumber(), 
            "speakbale lot number not correct");
        assertTrue(lot.getExpirationDate() == null, "expiration date not null");
        assertEquals(lot.getItem().getLotCount(), 2);
        assertEquals(lot.getLocation().getLotCount(), 1);
        
        //update with location
        lotManager.executeCreateUpdateLot(lotNumber, item, 
            null, "new speakableLotNumber", expire);
        
        lot = lotManager.findLotByNumberItemLocation(lotNumber, item, null);
        assertEquals("new speakableLotNumber", lot.getSpeakableNumber(), 
            "speakbale lot number not correct");
        assertTrue(lot.getExpirationDate() != null, "expiration date not null");
        assertEquals(lot.getItem().getLotCount(), 2);
        
        //delete without location
        lotManager.executeDeleteLot(lotNumber, item, null);
        itemManager.getPrimaryDAO().refresh(itemManager.findItemByNumber(item));
        assertEquals(itemManager.findItemByNumber(item).getLotCount(), 1);
        assertEquals(locationManager.findLocationByScanned(location).getLotCount(), 1);
        
        lot = lotManager.findLotByNumberItemLocation(lotNumber, item, null);
        assertTrue(lot == null, "lot was found");

        
        //delete with location
        lotManager.executeDeleteLot(lotNumber, item, location);
        itemManager.getPrimaryDAO().refresh(itemManager.findItemByNumber(item));
        locationManager.getPrimaryDAO().refresh(locationManager.findLocationByScanned(location));        
        assertEquals(itemManager.findItemByNumber(item).getLotCount(), 0);
        assertEquals(locationManager.findLocationByScanned(location).getLotCount(), 0);        
        
        lot = lotManager.findLotByNumberItemLocation(lotNumber, item, location);
        assertTrue(lot == null, "lot was found");
        
    }
    
    /**
     * test the create update and delete of lot numbers.
     * 
     * @throws Exception
     */
    public void testCreateDeleteFlag() throws Exception {
        String lotNumber = new Date().toString();
        String item = "99838";
        String location = "0814207";
        Date expire = new Date();
        
        Lot lot = new Lot();
        
        //reset data
        resetCoredata();
        
        //Make sure the initial lot counts are 0
        assertEquals(itemManager.findItemByNumber(item).getLotCount(), 0);
        assertEquals(locationManager.findLocationByScanned(location).getLotCount(), 0);

        //Create without a location
        lot.setNumber(lotNumber);
        lot.setExpirationDate(expire);
        lot.setItem(itemManager.findItemByNumber(item));
        lot.setLocation(locationManager.findLocationByScanned(location));
        lot.setSpeakableNumber("speakableLotNumber");
        lot.setImportQueryType("A");
        lotManager.save(lot);

        lot = lotManager.findLotByNumberItemLocation(lotNumber, item, location);
        assertEquals(lotNumber, lot.getNumber(), "lot number not correct");
        assertEquals(item, lot.getItem().getNumber(), "lot item not correct");
        assertEquals(location, lot.getLocation().getScannedVerification(), 
        "lot location not correct");        
        assertEquals("speakableLotNumber", lot.getSpeakableNumber(), 
            "speakbale lot number not correct");
        assertTrue(lot.getExpirationDate() != null, "expiration date not saved");
        assertEquals(lot.getItem().getLotCount(), 1);
        assertEquals(lot.getLocation().getLotCount(), 1);
                
        //delete with location
        lot.setImportQueryType("D");
        lotManager.save(lot);
        itemManager.getPrimaryDAO().refresh(itemManager.findItemByNumber(item));        
        locationManager.getPrimaryDAO().refresh(locationManager.findLocationByScanned(location));
        assertEquals(itemManager.findItemByNumber(item).getLotCount(), 0);
        assertEquals(locationManager.findLocationByScanned(location).getLotCount(), 0);
        
        lot = lotManager.findLotByNumberItemLocation(lotNumber, item, location);
        assertTrue(lot == null, "lot was found");
        
    }
    
    /**
     * reset core location and item data.
     * 
     * @throws Exception
     */    
    @Test(enabled = false)    
    private void resetCoredata() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml");
    }

    
    /**
     * Getter for the locationManager property.
     * @return LocationManager value of the property
     */
    @Test(enabled = false)    
    public LocationManager getLocationManager() {
        return locationManager;
    }

    
    /**
     * Setter for the locManager property.
     * @param locationManager the new locManager value
     */
    @Test(enabled = false)    
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    
    /**
     * Getter for the itemManager property.
     * @return ItemManager value of the property
     */
    @Test(enabled = false)    
    public ItemManager getItemManager() {
        return itemManager;
    }

    
    /**
     * Setter for the itemManager property.
     * @param itemManager the new itemManager value
     */
    @Test(enabled = false)    
    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
    }
}
