/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.SafetyCheckType;
import com.vocollect.voicelink.core.model.VehicleSafetyCheck;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponseType;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckManager;

import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Unit test class for testing Vehicle safety check manager.
 * 
 * @author khazra
 */
@Test(groups = { CORE, VSC })
public class VehicleSafetyCheckManagerImplTest extends
    BaseServiceManagerTestCase {

    private VehicleSafetyCheckManager manager = null;


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Core_VSC_data.xml",
            DatabaseOperation.INSERT);
    }

    /**
     * Setter for VehicleSafetyCheckManager.
     * @param impl - implementation of Vehicle Type manager.
     */
    @Test(enabled = false)
    public void setVehicleSafetyCheckManager(VehicleSafetyCheckManager impl) {
        this.manager = impl;
    }

    /**
     * Method to perform data clean up after all unit test cases are executed.
     * @throws Exception
     */
    @AfterClass()
    protected void cleanUp() throws Exception {
        classSetUp();
    }

    /**
     * Class-wide setup.
     * @throws Exception any
     */
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * Test case method to test the size of VehicleTypes existing the in the
     * system.
     * 
     * @throws Exception - any exception
     */
    @Test()
    public void testGetSequencedCheckList() throws Exception {
        List<VehicleSafetyCheck> listSafetyChecks = manager
            .listAllCheckByVehicleType(1);
        assertEquals(5, listSafetyChecks.size());
        assertEquals(SafetyCheckType.STOPONNO, listSafetyChecks.get(1).getResponseType());
        assertEquals("Tires", listSafetyChecks.get(1).getSpokenDescription());
        
        assertEquals(SafetyCheckType.NUMERIC, listSafetyChecks.get(3).getResponseType());
        assertEquals("Miles run", listSafetyChecks.get(3).getSpokenDescription());
    }

}
