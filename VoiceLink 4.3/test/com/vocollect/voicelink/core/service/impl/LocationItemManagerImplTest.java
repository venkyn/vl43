/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationItem;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.service.ItemManager;
import com.vocollect.voicelink.core.service.LocationItemManager;
import com.vocollect.voicelink.core.service.LocationManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 *
 *
 */
@Test(groups = { FAST, CORE })
public class LocationItemManagerImplTest extends BaseServiceManagerTestCase {

    private LocationItemManager manager = null;

    private LocationManager locationManager = null;

    private ItemManager itemManager = null;

    /**
     * Getter for the itemManager property.
     * @return ItemManager value of the property
     */
    private ItemManager getItemManager() {
        return itemManager;
    }


    /**
     * Setter for the itemManager property.
     * @param itemManager the new itemManager value
     */
    @Test(enabled = false)
    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
    }


    /**
     * Getter for the locationManager property.
     * @return LocationManager value of the property
     */
    private LocationManager getLocationManager() {
        return locationManager;
    }


    /**
     * Setter for the locationManager property.
     * @param locationManager the new locationManager value
     */
    @Test(enabled = false)
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * @param impl -
     */
    @Test(enabled = false)
    public void setLocationItemManager(LocationItemManager impl) {
        this.manager = impl;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        //adapter.resetInstallationData();
        adapter.resetInstallationData();

        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_Core.xml",
            DatabaseOperation.CLEAN_INSERT);

    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void testExecute() throws Exception {
        Location l = getLocationManager().get(-1L);
        Item i = getItemManager().get(-28L);
        Item i2 = getItemManager().get(-36L);

        //==================================================
        //Test location gets set to multiple
        LocationItem li = new LocationItem();
        li.setLocation(l);
        li.setItem(i);
        manager.save(li);

        assertEquals(l.getStatus(), LocationStatus.Multiple,
        "Location Status Multiple");

        //==================================================
        //Add a second mapping
        LocationItem li2 = new LocationItem();
        li2.setLocation(l);
        li2.setItem(i2);
        manager.save(li2);

        assertEquals(l.getStatus(), LocationStatus.Multiple,
        "Location Status Multiple");

        //==================================================
        //Short first Location
        getLocationManager().executeReportShort(l, i);
        assertEquals(li.getStatus(), LocationStatus.Empty,
        "Report Short");


        //==================================================
        //Start replenishment
        getLocationManager().executeStartReplenishment(l, i);
        assertEquals(li.getStatus(), LocationStatus.InProgress,
        "Start Replenishment");


        //==================================================
        //Report replenishment
        getLocationManager().executeReportReplenished(l, i);
        assertEquals(li.getStatus(), LocationStatus.Replenished,
        "Report Replenishment");

        //==================================================
        //Complete replenishment
        getLocationManager().executeReportShort(l, i);
        assertEquals(li.getStatus(), LocationStatus.Empty,
        "Report Short");

        getLocationManager().executeCompleteReplenishment(l, i);
        assertEquals(li.getStatus(), LocationStatus.Replenished,
        "Complete Replenishment");

        //==================================================
        //Trigger replenishment
        getLocationManager().executeTriggerReplenishment(l, i);
        assertEquals(li.getStatus(), LocationStatus.Replenished,
        "Trigger Replenishment");

        assertEquals(l.getStatus(), LocationStatus.Multiple,
        "Location Status Multiple");

        //==================================================
        //Test Error null Item
        try {
            getLocationManager().executeReportReplenished(l, null);
        } catch (BusinessRuleException e) {
            assertEquals(e.getErrorCode(),
                CoreErrorCode.ITEM_LOCATION_ITEM_MUST_BE_DEFINED,
            "Trigger Replenishment");
        }

        //==================================================
        //Test Error wrong item
        try {
            getLocationManager().executeReportReplenished(l, i2);
        } catch (BusinessRuleException e) {
            assertEquals(e.getErrorCode(),
                CoreErrorCode.ITEM_LOCATION_ITEM_NOT_MAPPED,
            "Trigger Replenishment");
        }


        //==================================================
        //Delete First Mapping make sure status still multiple
        manager.delete(li);
        assertEquals(l.getStatus(), LocationStatus.Multiple,
            "Location Status Multiple");

        //==================================================
        //Delete Second mapping make sure status set to replenished
        manager.delete(li2);
        assertEquals(l.getStatus(), LocationStatus.Replenished,
            "Location Status Replenished");


    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void testCreateMappings() throws Exception {

        // create a location/item mapping at two different
        // sites with the same locationId and ItemID.

        // Create another site 'Site2' using the data file

        DbUnitAdapter adapter = new DbUnitAdapter();
        // Setup Database
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_LocationItemManager.xml",
            DatabaseOperation.INSERT);

        SiteContext sc = SiteContextHolder.getSiteContext();

        // Get location and item in current site.
        Location l1 = getLocationManager().get(-99999L);
        Item i1 = getItemManager().get(-99999L);

        // ==================================================
        // Create a mapping in the default site
        LocationItem li1 = new LocationItem();
        li1.setLocation(l1);
        li1.setItem(i1);
        manager.executeCreateMapping(li1);

        // Change the site from the default to 'Site1'
        SiteContextHolder.setSiteContext(sc);
        sc.setCurrentSite(sc.getSiteByName("Site1"));
        sc.setHasAllSiteAccess(false);
        sc.setFilterBySite(true);

        // Get location and item in 'Site 2'.
        Location l2 = getLocationManager().get(-99999L);
        Item i2 = getItemManager().get(-99999L);

        // ==================================================
        // Create a mapping in 'Site2'
        LocationItem li2 = new LocationItem();
        li2.setLocation(l2);
        li2.setItem(i2);
        manager.executeCreateMapping(li2);

        //==================================================
        //Test create already existing mapping
        try {
            manager.executeCreateMapping(li2);
        } catch (BusinessRuleException e) {
            assertEquals(e.getErrorCode(),
                CoreErrorCode.ITEM_LOCATION_ALREADY_EXISTS,
            "Item/Location Already exists!");
        }

        //==================================================
        //Test create mapping for non-existent items
        try {
            li2.setLocation(null);
            manager.executeCreateMapping(li2);
        } catch (BusinessRuleException e) {
            assertEquals(e.getErrorCode(),
                CoreErrorCode.ITEM_LOCATION_ITEM_NOT_DEFINED,
            "Item/Location Already exists!");
        }

    }
}
