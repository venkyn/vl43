/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.service.DataTranslationManager;
import com.vocollect.epp.test.DbUnitAdapter;

import com.vocollect.voicelink.core.model.BreakType;
import com.vocollect.voicelink.core.model.Operator;

import com.vocollect.voicelink.core.service.BreakTypeManager;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.annotations.Test;

/**
 *
 * @author krishna.udupi
 *
 */
@Test(groups = { FAST, CORE, SECURITY })

public class OperatorBreakReportImplTest extends BaseServiceManagerTestCase {

     private String operatorID;
     private String name;
     private String breakType;
     private Long durationHours;
     private Long durationMinutes;
     private Date startTime;
     private Date endTime;

     private static final Long HOURS = 3600000L;
     private static final Long MINUTES = 60000L;

     private BreakTypeManager            breakTypeManager = null;
     private OperatorLaborManager        operatorLaborManager = null;
     private OperatorManager             operatorManager = null;
     private DataTranslationManager      dataTranslationManager = null;

     // common variables used by tests.
     private List<Object[]>    oblRecords;

     /**
      * Getter for the dataTranslationManager property.
      * @return DataTranslationManager value of the property
      */
     @Test(enabled = false)
     public DataTranslationManager getDataTranslationManager() {
         return dataTranslationManager;
     }

     /**
      * Setter for the dataTranslationManager property.
      * @param dataTranslationManager the new dataTranslationManager value
      */
     @Test(enabled = false)
     public void setDataTranslationManager(DataTranslationManager dataTranslationManager) {
         this.dataTranslationManager = dataTranslationManager;
     }

     /**
      * Getter for the breakTypeManager property.
      * @return BreakTypeManager value of the property
      */
     @Test(enabled = false)
     public BreakTypeManager getBreakTypeManager() {
         return breakTypeManager;
     }

     /**
      * Setter for the breakTypeManager property.
      * @param breakTypeManager the new breakTypeManager value
      */
     @Test(enabled = false)
     public void setBreakTypeManager(BreakTypeManager breakTypeManager) {
         this.breakTypeManager = breakTypeManager;
     }

     /**
      * Getter for the operatorLaborManager property.
      * @return OperatorLaborManager value of the property
      */
     @Test(enabled = false)
     public OperatorLaborManager getOperatorLaborManager() {
         return operatorLaborManager;
     }

     /**
      * Setter for the operatorLaborManager property.
      * @param operatorLaborManager the new operatorLaborManager value
      */
     @Test(enabled = false)
     public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
         this.operatorLaborManager = operatorLaborManager;
     }

     /**
      * Getter for the operatorManager property.
      * @return OperatorManager value of the property
      */
     @Test(enabled = false)
     public OperatorManager getOperatorManager() {
         return operatorManager;
     }

     /**
      * Setter for the operatorManager property.
      * @param operatorManager the new operatorManager value
      */
     @Test(enabled = false)
     public void setOperatorManager(OperatorManager operatorManager) {
         this.operatorManager = operatorManager;
     }

     /**
      * {@inheritDoc}
      * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
      */
     @Override
     protected void initData(DbUnitAdapter adapter) throws Exception {
         // We are not using this method to load data.
         // We are using a loadData method so we can seperate the tests into multiple
         // test methods and not have to load the data before each test.
      }

     /**
      * Test
      * This test simulates Operator Breaks Report Cases.
      *
      * @throws Exception - database exceptions.
      */
     @Test(enabled = false)
    public void testExecuteOperatorBreaksReportCases() throws Exception {

         //Parameter values
         Operator operator = this.getOperatorManager().get(7L);
         Set<String> operators = new HashSet<String>();
         operators.add(operator.getOperatorIdentifier());

         Set<Long> opTeam = new HashSet<Long>();
         opTeam.add(1L);
         BreakType breakType1 = this.getBreakTypeManager().get(11L);

         Date startTime1 = operator.getSignOnTime();
         Date endTime1 = operator.getSignOffTime();
         String breakId = breakType1.getId().toString();
         Long durationInMillis = 10L;
        oblRecords = getOperatorLaborManager().listBreaksLongerThan(
            startTime1, endTime1, durationInMillis, operators, breakId,
            null);
         for (Object[] obj : oblRecords) {
              Timestamp startTimestamp = (Timestamp) obj[0];
              Timestamp endTimeStamp = (Timestamp) obj[1];
              Long durationInMilliSeconds = (Long) obj[2];
              Long durationInMinutes = getMinutes(durationInMilliSeconds);
              Long durationInHours = getHours(durationInMilliSeconds);
              Long operID = (Long) obj[3];
              String operatorName = (String) obj[4];
              Long breakTypeID = (Long) obj[5];
              String breakTypeName = getDataTranslationManager().
                                            translate(getBreakTypeManager().get(breakTypeID).getName());

              this.breakType = breakTypeName;
              this.name = operatorName;
              this.operatorID = Long.toString(operID);
              this.durationMinutes = durationInMinutes;
              this.durationHours = durationInHours;
              this.startTime = startTimestamp;
              this.endTime = endTimeStamp;
         }
         // This never tests anything.  What is the point?
     }

     /**
      * Test
      * This test simulates Operator Breaks Report Exception Cases.
      * @throws Exception
      */
     @Test(enabled = false)
     public void testExecuteOperatorBreaksReportExceptionCases() throws Exception {
          //Parameter values
         Operator operator = this.getOperatorManager().get(7L);
         Set<String> operators = new HashSet<String>();
         operators.add(operator.getOperatorIdentifier());

         Set<Long> opTeam = new HashSet<Long>();
         opTeam.add(1L);
          BreakType breakType1 = this.getBreakTypeManager().get(11L);
          int objectCounter = 0;
          Date startTime1 = operator.getSignOnTime();
          Date endTime1 = operator.getSignOffTime();
          String breakId = breakType1.getId().toString();
          Long durationInMillis = 10L;
          oblRecords = getOperatorLaborManager().listBreaksLongerThan(
            startTime1, endTime1, durationInMillis, operators, breakId,
            null);
          try {
               for (Object[] obj : oblRecords) {
                    Timestamp startTimestamp = (Timestamp) obj[objectCounter];
                    Timestamp endTimeStamp = (Timestamp) obj[++objectCounter];
                    Long durationInMilliSeconds = (Long) obj[++objectCounter];
                    Long durationInMinutes = getMinutes(durationInMilliSeconds);
                    Long durationInHours = getHours(durationInMilliSeconds);
                    Long operID = (Long) obj[++objectCounter];
                    String operatorName = (String) obj[++objectCounter];
                    Long breakTypeID = (Long) obj[++objectCounter];
                    String breakTypeName = getDataTranslationManager().
                                                  translate(getBreakTypeManager().get(breakTypeID).getName());

                    this.breakType = breakTypeName;
                    this.name = operatorName;
                    this.operatorID = Long.toString(operID);
                    this.durationMinutes = durationInMinutes;
                    this.durationHours = durationInHours;
                    this.startTime = startTimestamp;
                    this.endTime = endTimeStamp;
               }
               fail("ClassCastException was not thrown");
          } catch (Exception e) {
               assertTrue(true, "Exception caught");
          }
     }

     /**
      * Convert milliseconds to total hours.
      *
      * @param duration - number of milliseconds
      * @return - total number of hours
      */
     @Test(enabled = false)
     public Long getHours(Long duration) {
         return duration / HOURS;
     }

     /**
      * Convert millisecods to minutes (minus hours).
      * @param duration - total duration
      * @return - minutes (minus total hours)
      */
     @Test(enabled = false)
     private Long getMinutes(Long duration) {
         Long minutes = duration - (getHours(duration) * HOURS);
         return minutes / MINUTES;
     }



}
