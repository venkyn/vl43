/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Iterator;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * 
 * 
 * @author estoll
 */
@Test(groups = { FAST, SECURITY, CORE })

public class OperatorManagerImplTest extends BaseServiceManagerTestCase {

    private OperatorManager manager = null;

    private WorkgroupManager wgManager = null;

    /**
     * @param impl WorkgroupManager
     */
    @Test(enabled = false)
    public void setWorkgroupManager(WorkgroupManager impl) {
        this.wgManager = impl;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_SelectionRegions.xml",
            DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkSampleOperators.xml", DatabaseOperation.REFRESH);

    }

    /**
     * Test for setter.
     * 
     * @param impl - operator manager impl
     */
    @Test(enabled = false)
    public void setOperatorManager(OperatorManager impl) {
        this.manager = impl;
    }

    /**
     * Test
     * 'com.vocollect.voicelink.core.service.impl.RegionManagerImpl.listAuthorized'
     * to return a list of regions.
     * @throws Exception - database exceptions.
     */
    public void testExecute() throws Exception {
        Workgroup wg = wgManager.getPrimaryDAO().get(-1L);

        Long regionId = -1L;
        int operatorCount = 0;

        // Test Changing work group
        List<Operator> operators = this.manager.getAll();
        operatorCount = operators.size();

        for (Iterator<Operator> itr = operators.iterator(); itr.hasNext();) {
            Operator operator = itr.next();

            try {
                this.manager.executeChangeWorkgroup(operator, wg);
                this.manager.save(operator);
            } catch (BusinessRuleException e) {
                assertEquals(0, 1, "Business rule changing operator workgroup");
            } catch (DataAccessException e) {
                assertEquals(
                    0, 1, "Data Access error changing operator workgroup");
            }
        }

        // Check all operator autorized
        operators = this.manager.listAuthorizedForRegion(regionId);
        assertEquals(
            operators.size(), operatorCount, "All operators Authorized");

        // Remove work group from first operator
        Operator op90 = this.manager.get(-90L);
        // Save the workgroup so we can set it back after this test.
        Workgroup op90Wg = op90.getWorkgroup();
        this.manager.executeChangeWorkgroup(op90, null);

        // Get operator list again, this time should be one less than total
        operators = this.manager.listAuthorizedForRegion(regionId);

        assertEquals(
            operators.size(), operatorCount - 1,
            "All but 1 operator Authorized");

        //test the find operators by Workgroup
        operators = this.manager.listForWorkgroup(wg);
        assertEquals(operators.size(), operatorCount - 1, "Operators by Workgroup did not return the correct count");

        // Set op90 back...
        this.manager.executeChangeWorkgroup(op90, op90Wg);
        
        
        ///////////////////////////////////////////////////////
        // test retrieving operators available to work in region
        // At this point, we should have 
        // 3 operators in region -2
        // 3 operators in region -1
        // 2 operators in region -3
        // And all operators have permissions to all regions
        //
        ///////////////////////////////////////////////////////
        
        // First test for available work
        // if we check who's available to work in region -3
        // we should get a list of 6 operators
        ///////////////////////////////////////////////////////
        operators = this.manager.listAvailableToWorkInRegion(-3L);
        assertEquals(operators.size(), operatorCount - 2, 
            "Operators available for region -3 has incorrect count");
        
        // Next test is to get a list of operators already in region
        operators = this.manager.listCurrentlyInRegion(-3L);
        assertEquals(operators.size(), 2, 
            "Operators currently in region -3 has incorrect count");

        ///////////////////////////////////////////////////////
        // Now change an operators workgroup so he doesn't have
        // access to region -3
        ///////////////////////////////////////////////////////
        Workgroup wgtest = wgManager.getPrimaryDAO().get(-3L);
        op90 = this.manager.get(-90L);
        this.manager.executeChangeWorkgroup(op90, wgtest);

        operators = this.manager.listAvailableToWorkInRegion(-3L);
        if (operators.contains(op90)) {
            fail("Operator -90 is still available for region -3");
        }
        // double check the size of the list tooooooo
        assertEquals(operators.size(), operatorCount - 3, 
            "Operator -90 is still available for region -3");

        
        ///////////////////////////////////////////////////////
        // End of test cases for assign operator feature needs
        ///////////////////////////////////////////////////////

        
        
        ///////////////////////////////////////////////////////
        //
        // test deleting workgroups and reassiging operators
        // at this point all operators are in the default workgroup 
        // except 2.
        // Let's move them all to workgroup -3 to start the test
        //
        ///////////////////////////////////////////////////////
        Workgroup wg3 = wgManager.getPrimaryDAO().get(-3L);
        operators.clear();
        operators = this.manager.getAll();
        operatorCount = operators.size();

        for (Iterator<Operator> itr = operators.iterator(); itr.hasNext();) {
            Operator operator = itr.next();

            try {
                this.manager.executeChangeWorkgroup(operator, wg3);
                this.manager.save(operator);
            } catch (BusinessRuleException e) {
                assertEquals(0, 1, "Business rule changing operator workgroup");
            } catch (DataAccessException e) {
                assertEquals(
                    0, 1, "Data Access error changing operator workgroup");
            }
        }

        //Now delete wg3 - and verify all operators are moved into 
        //the default workgroup (id -1L)
        UserMessage userMessage = (UserMessage) wgManager.delete(wg3);
        assertEquals(userMessage.getKey(), 
            "workgroup.delete.reassign.operator.default",
            "Workgroup delete message key is wrong");

        operators = this.manager.listForWorkgroup(wg);
        assertEquals(operators.size(), operatorCount, 
            "Operators assigned to default workgroup count is wrong");
        
        //Now delete the default workgroup and verify that all
        //operators are not assigned to a workgroup
        userMessage = (UserMessage) wgManager.delete(wg);
        assertEquals(userMessage.getKey(), 
            "workgroup.delete.reassign.operator.null",
            "Workgroup delete message key is wrong for default workgroup deletion");

        ///////////////////////////////////////////////////////
        //
        // End of deleting workgroups and reassiging operators test
        //
        ///////////////////////////////////////////////////////
        
    }
}
