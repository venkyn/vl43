/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.model.Printer;
import com.vocollect.voicelink.core.service.PrinterManager;
import com.vocollect.voicelink.printserver.AbstractPrintServer;
import com.vocollect.voicelink.printserver.PrintServerFactory;
import com.vocollect.voicelink.puttostore.model.PtsContainer;
import com.vocollect.voicelink.puttostore.service.PtsContainerManager;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.service.AssignmentManager;
import com.vocollect.voicelink.selection.service.PickManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.ArrayList;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * 
 * 
 * @author estoll
 */
@Test(groups = { FAST, SECURITY, CORE })

public class PrinterManagerImplTest extends BaseServiceManagerTestCase {

    private PrinterManager manager = null;

    private AssignmentManager aManager = null;
    private PickManager pickManager = null;
    private PtsContainerManager ptsContainerManager = null;
    
    
    /**
     * @param impl AssignmentManager
     */
    @Test(enabled = false)
    public void setAssignmentManager(AssignmentManager impl) {
        this.aManager = impl;
    }

    /**
     * @param impl PickManager
     */
    @Test(enabled = false)
    public void setPickManager(PickManager impl) {
        this.pickManager = impl;
    }
    
    /**
     * @param impl PrintManager
     */
    @Test(enabled = false)
    public void setPrinterManager(PrinterManager impl) {
        this.manager = impl;
    }

    /**
     * @param impl PtsContainerManager
     */
    @Test(enabled = false)
    public void setPtsContainerManager(PtsContainerManager impl) {
        this.ptsContainerManager = impl;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
        
        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_Printers.xml",
            DatabaseOperation.INSERT);
        
    }
    
    /**
     * @throws DataAccessException on database exceptions
     * @throws VocollectException on PrintServer exceptions
     */
    public void testExecuteRefreshPrinters() 
        throws DataAccessException, VocollectException {
        // this one test will verify getting printers and if their name is
        // not known, add the printer with the next number
        List<Printer> printers = manager.getAll();
        assertFalse(printers.size() == 0, "Expected to add at least one printer");

        int numberOfPrinters = printers.size();
        // second call - no new printers so we should still have 10
        List<Printer> p2 = manager.getAll();
        assertEquals(p2.size(), numberOfPrinters, "Printers count mismatch");
    }

    /**
     * @throws DataAccessException on database exceptions
     * @throws VocollectException on print server exceptions
     */
    

    @Test(enabled = false, dependsOnMethods = { "testExecuteRefreshPrinters" })
    public void testGenerateAssignmentLabels() 
    throws DataAccessException, VocollectException {
        List<Assignment> printThese = new ArrayList<Assignment>();
        List<Assignment> assignments = this.aManager.getAll();
        
        for (int i = 0; i < 1; i++) {
            printThese.add(assignments.get(i));
        }
        
        //Print these babies
        try {
            this.manager.generateAssignmentLabels(printThese, 
                this.manager.findByNumber(2));
        } catch (BusinessRuleException e) {
            assertEquals(e.getErrorCode(), 
                CoreErrorCode.PRINT_ASSIGNMENT_DOESNOT_HAVE_CONTAINERS,
                "Expected No Labels to Print");
        }
        
        AbstractPrintServer server = PrintServerFactory.getPrintServer();
        while (server.getJobQueue().size() > 0) {
            // Wait for job to process
            // this is only needed for testing purposes.
        }
    }


    
    /**
     * @throws DataAccessException on database exceptions
     * @throws VocollectException on print server exceptions
     */
    @Test(enabled = false, dependsOnMethods = { "testExecuteRefreshPrinters" })
    public void testGeneratePickLabels() 
    throws DataAccessException, VocollectException {
        List<Pick> printThese = new ArrayList<Pick>();
        List<Pick> picks = this.pickManager.getAll();
        
        for (int i = 0; i < 5; i++) {
            printThese.add(picks.get(i));
        }
        
        //Print these babies
        try {
            this.manager.generatePickLabels(printThese, 
                this.manager.findByNumber(2));

            AbstractPrintServer server = PrintServerFactory.getPrintServer();
            while (server.getJobQueue().size() > 0) {
                // Wait for job to process
                // this is only needed for testing purposes.
            }
        } catch (BusinessRuleException e) {
            assertEquals(e.getErrorCode(), 
                CoreErrorCode.PRINT_ASSIGNMENT_DOESNOT_HAVE_CONTAINERS,
                "expected error to be no containers");
        }
    }

    /**
     * @throws DataAccessException on database exceptions
     * @throws VocollectException on print server exceptions
     */
    @Test(enabled = false, dependsOnMethods = { "testExecuteRefreshPrinters" })
    public void testGeneratePtsContainerLabels() 
    throws DataAccessException, VocollectException {
        List<PtsContainer> printThese = new ArrayList<PtsContainer>();
        List<PtsContainer> containers = this.ptsContainerManager.getAll();
        
        for (int i = 0; i < 5; i++) {
            printThese.add(containers.get(i));
        }
        
        //Print these babies
        try {
            this.manager.generatePtsContainerLabels(containers,
                this.manager.findByNumber(2));

            AbstractPrintServer server = PrintServerFactory.getPrintServer();
            while (server.getJobQueue().size() > 0) {
                // Wait for job to process
                // this is only needed for testing purposes.
            }
        } catch (BusinessRuleException e) {
            assertEquals(e.getErrorCode(), 
                CoreErrorCode.PRINT_ASSIGNMENT_DOESNOT_HAVE_CONTAINERS,
                "expected error to be no containers");
        }
    }

}
