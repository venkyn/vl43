/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.TaskFunctionType;
import com.vocollect.voicelink.core.service.RegionManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * 
 * 
 * @author estoll
 */
@Test(groups = { FAST, SECURITY, CORE })

public class RegionManagerImplTest extends BaseServiceManagerTestCase {

    private RegionManager manager = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_ResetAll.xml",
            DatabaseOperation.DELETE_ALL);
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_SelectionRegions.xml",
            DatabaseOperation.INSERT);
    }

    /**
     * Set the manager.
     * @param impl The manager to use.
     */
    @Test(enabled = false)
    public void setRegionManager(RegionManager impl) {
        this.manager = impl;
    }

    /**
     * Test
     * 'com.vocollect.voicelink.core.service.impl.RegionManagerImpl.listAuthorized'
     * to return a list of regions.
     * @throws DataAccessException - database exceptions.
     */
    public void testListAuthorizedReturnsMany() throws DataAccessException {
        Long workgroupId = -1L;

        List<Region> regions = this.manager.listAuthorized(
            TaskFunctionType.NormalAssignments, workgroupId);

        assertEquals((Integer) 15, (Integer) regions.size());

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.service.impl.RegionManagerImpl.listAuthorized'
     * when zero regions are authroized.
     * @throws DataAccessException - database exceptions.
     */
    public void testListByFunctionNumberAndWorkGroupIdReturnsNone()
        throws DataAccessException {
        Long workgroupId = -1L;

        List<Region> regions = this.manager.listAuthorized(
            TaskFunctionType.Replenishment, workgroupId);

        assertEquals((Integer) 0, (Integer) regions.size());

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.server.impl.RegionManagerImpl.listByTypeOrderByName'.
     * @throws DataAccessException - database exceptions.
     */
    public void testListByTypeOrderByName() throws DataAccessException {
        List<Region> selRegions = this.manager
            .listByTypeOrderByName(RegionType.Selection);
        assertEquals(15, selRegions.size(), "Selection Region count failed");

        List<Region> paRegions = this.manager
            .listByTypeOrderByName(RegionType.PutAway);
        assertEquals(0, paRegions.size(), "Putaway Region count failed");

    }
}
