/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.SafetyCheckType;
import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.model.VehicleSafetyCheck;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckRepairActionType;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponse;
import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponseType;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.VehicleManager;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckManager;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckResponseManager;

import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * Test class for unit testing various VehicleSafety check responses fetch/update scenarios
 * @author mraj
 *
 */
@Test(groups = { CORE, VSC })
public class VehicleSafetyCheckResponseManagerImplTest extends
    BaseServiceManagerTestCase {

    private VehicleManager vehicleManager = null;
    private OperatorManager operatorManager = null;
    private VehicleSafetyCheckManager vehicleSafetyCheckManager = null;
    private VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager = null;
    
    /**
     * Class-wide setup.
     * @throws Exception any
     */
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }
    
    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Core_VSC_data.xml",
            DatabaseOperation.INSERT);

    }
    
    
    
    /**
     * Test quick repair of a safety check
     * @throws Exception - any exception
     */
    @Test()
    public void testExecuteCheckResponseAllYes() throws Exception {
        Operator operator = operatorManager.get(-1L);
        Vehicle vehicle =  vehicleManager.get(-11L);
        List<VehicleSafetyCheck> checks = vehicle.getVehicleType().getSafetyChecks();
        List<VehicleSafetyCheckResponse> responses = null;
        try {
            responses = vehicleSafetyCheckResponseManager.executeCheckResponse(
                operator, "",
                VehicleSafetyCheckRepairActionType.NoAction.toValue(), vehicle,
                checks, VehicleSafetyCheckResponseType.YES.toString(), new Date());
        } catch (Exception e) {
            fail();
        }
        
        assertNotNull(responses);
        assertEquals(responses.size(), checks.size());
        for (VehicleSafetyCheckResponse vehicleSafetyCheckResponse : responses) {
            assertEquals(vehicleSafetyCheckResponse.getRepairAction(), VehicleSafetyCheckRepairActionType.NoAction);
            assertEquals(vehicleSafetyCheckResponse.getCheckResponse()
                .intValue(), VehicleSafetyCheckResponseType.YES.toValue());
            assertEquals(vehicleSafetyCheckResponse.getOperator(), operator);
            assertEquals(vehicleSafetyCheckResponse.getVehicle(), vehicle);
        }
    }
    
    /**
     * Test for a NO response
     * @throws Exception - any exception
     */
    @Test()
    public void testExecuteCheckResponseForNo() throws Exception {
        Operator operator = operatorManager.get(-1L);
        Vehicle vehicle =  vehicleManager.get(-11L);
        List<VehicleSafetyCheck> checks = getVehicleSafetyCheckManager()
            .listAllCheckByVehicleType(vehicle.getVehicleType().getNumber()); 
        List<VehicleSafetyCheckResponse> responses = null;
        try {
            responses = vehicleSafetyCheckResponseManager.executeCheckResponse(
                operator, "Breaks",
                VehicleSafetyCheckRepairActionType.NewEquipment.toValue(), vehicle,
                checks, VehicleSafetyCheckResponseType.NO.toString(), new Date());
        } catch (Exception e) {
            fail();
        }
        
        assertNotNull(responses);
        assertEquals(responses.size(), 1);
        VehicleSafetyCheckResponse vehicleSafetyCheckResponse = responses.get(0);
        assertEquals(vehicleSafetyCheckResponse.getRepairAction(), VehicleSafetyCheckRepairActionType.NewEquipment);
        assertEquals(
            vehicleSafetyCheckResponse.getCheckResponse().intValue(),
            VehicleSafetyCheckResponseType.NO.toValue());
        assertEquals(vehicleSafetyCheckResponse.getOperator(), operator);
        assertEquals(vehicleSafetyCheckResponse.getVehicle(), vehicle);
    }
    
    /**
     * Test for a quick repair response
     * @throws Exception - any exception
     */
    @Test()
    public void testExecuteCheckResponseForQuickRepair() throws Exception {
        Operator operator = operatorManager.get(-1L);
        Vehicle vehicle =  vehicleManager.get(-11L);
        List<VehicleSafetyCheck> checks = vehicle.getVehicleType().getSafetyChecks();
        List<VehicleSafetyCheckResponse> responses = null;
        try {
            responses = vehicleSafetyCheckResponseManager.executeCheckResponse(
                operator, "Breaks",
                VehicleSafetyCheckRepairActionType.QuickRepair.toValue(), vehicle,
                checks, VehicleSafetyCheckResponseType.NO.toString(), new Date());
        } catch (Exception e) {
            fail();
        }
        
        assertNotNull(responses);
        assertEquals(responses.size(), 1);
        VehicleSafetyCheckResponse vehicleSafetyCheckResponse = responses.get(0);
        assertEquals(vehicleSafetyCheckResponse.getRepairAction(), VehicleSafetyCheckRepairActionType.QuickRepair);
        assertEquals(
            vehicleSafetyCheckResponse.getCheckResponse().intValue(),
            VehicleSafetyCheckResponseType.INITIAL.toValue());
        assertEquals(vehicleSafetyCheckResponse.getOperator(), operator);
        assertEquals(vehicleSafetyCheckResponse.getVehicle(), vehicle);
    }
    
    /**
     * Test for a quick repair response
     * @throws Exception - any exception
     */
    @Test()
    public void testNumericVLINK4829() throws Exception {
        Operator operator = operatorManager.get(-1L);
        Vehicle vehicle =  vehicleManager.get(-11L);
        String numericVal = "12345";
        List<VehicleSafetyCheck> checks = vehicle.getVehicleType().getSafetyChecks();
        List<VehicleSafetyCheckResponse> responses = null;
        try {
            responses = vehicleSafetyCheckResponseManager.executeCheckResponse(
                operator, "Miles run",
                VehicleSafetyCheckRepairActionType.NoAction.toValue(), vehicle,
                checks, numericVal, new Date());
        } catch (Exception e) {
            fail();
        }
        
        assertNotNull(responses);
        assertEquals(responses.size(), 1);
        VehicleSafetyCheckResponse vehicleSafetyCheckResponse = responses.get(0);
        assertEquals(vehicleSafetyCheckResponse.getRepairAction(), VehicleSafetyCheckRepairActionType.NoAction);
        assertEquals(
            vehicleSafetyCheckResponse.getCheckResponse(),
            new Integer(numericVal));
        assertEquals(vehicleSafetyCheckResponse.getOperator(), operator);
        assertEquals(vehicleSafetyCheckResponse.getVehicle(), vehicle);
        assertEquals(vehicleSafetyCheckResponse.getSafetyCheck()
            .getResponseType(), SafetyCheckType.NUMERIC);
    }
    
    /**
     * Test for a quick repair response performed twice
     * @throws Exception - any exception
     */
    @Test()
    public void testTwiceQuickRepairVLINK4869() throws Exception {
        Operator operator = operatorManager.get(-1L);
        Vehicle vehicle =  vehicleManager.get(-11L);
        List<VehicleSafetyCheck> checks = vehicle.getVehicleType().getSafetyChecks();
        List<VehicleSafetyCheckResponse> responses = null;
        try {
            responses = vehicleSafetyCheckResponseManager.executeCheckResponse(
                operator, "Breaks",
                VehicleSafetyCheckRepairActionType.QuickRepair.toValue(), vehicle,
                checks, VehicleSafetyCheckResponseType.NO.toString(), new Date());
        } catch (Exception e) {
            fail();
        }
        
        assertNotNull(responses);
        assertEquals(responses.size(), 1);
        VehicleSafetyCheckResponse vehicleSafetyCheckResponse = responses.get(0);
        assertEquals(vehicleSafetyCheckResponse.getRepairAction(), VehicleSafetyCheckRepairActionType.QuickRepair);
        assertEquals(
            vehicleSafetyCheckResponse.getCheckResponse().intValue(),
            VehicleSafetyCheckResponseType.INITIAL.toValue());
        assertEquals(vehicleSafetyCheckResponse.getOperator(), operator);
        assertEquals(vehicleSafetyCheckResponse.getVehicle(), vehicle);
        
        try {
            responses = vehicleSafetyCheckResponseManager.executeCheckResponse(
                operator, "Breaks",
                VehicleSafetyCheckRepairActionType.QuickRepair.toValue(), vehicle,
                checks, VehicleSafetyCheckResponseType.NO.toString(), new Date());
        } catch (Exception e) {
            fail();
        }
        
        assertNotNull(responses);
        assertEquals(responses.size(), 0);
    }
    
    /**
     * Test for a quick repair response
     * @throws Exception - any exception
     */
    @Test()
    public void testCompleteFlow() throws Exception {
        Operator operator = operatorManager.get(-1L);
        Vehicle vehicle =  vehicleManager.get(-11L);
        List<VehicleSafetyCheck> checks = vehicle.getVehicleType().getSafetyChecks();
        List<VehicleSafetyCheckResponse> responses = null;
        
        //Quick Repair
        try {
            responses = vehicleSafetyCheckResponseManager.executeCheckResponse(
                operator, "Breaks",
                VehicleSafetyCheckRepairActionType.QuickRepair.toValue(), vehicle,
                checks, VehicleSafetyCheckResponseType.NO.toString(), new Date());
        } catch (Exception e) {
            fail();
        }
        
        assertNotNull(responses);
        assertEquals(responses.size(), 1);
        VehicleSafetyCheckResponse vehicleSafetyCheckResponse = responses.get(0);
        assertEquals(vehicleSafetyCheckResponse.getRepairAction(), VehicleSafetyCheckRepairActionType.QuickRepair);
        assertEquals(
            vehicleSafetyCheckResponse.getCheckResponse().intValue(),
            VehicleSafetyCheckResponseType.INITIAL.toValue());
        assertEquals(vehicleSafetyCheckResponse.getOperator(), operator);
        assertEquals(vehicleSafetyCheckResponse.getVehicle(), vehicle);
        
        //Numeric
        String numericVal = "12345";
        try {
            responses = vehicleSafetyCheckResponseManager.executeCheckResponse(
                operator, "Miles run",
                VehicleSafetyCheckRepairActionType.NoAction.toValue(), vehicle,
                checks, numericVal, new Date());
        } catch (Exception e) {
            fail();
        }
        
        assertNotNull(responses);
        assertEquals(responses.size(), 1);
        vehicleSafetyCheckResponse = responses.get(0);
        assertEquals(vehicleSafetyCheckResponse.getRepairAction(), VehicleSafetyCheckRepairActionType.NoAction);
        assertEquals(
            vehicleSafetyCheckResponse.getCheckResponse(),
            new Integer(numericVal));
        assertEquals(vehicleSafetyCheckResponse.getOperator(), operator);
        assertEquals(vehicleSafetyCheckResponse.getVehicle(), vehicle);
        assertEquals(vehicleSafetyCheckResponse.getSafetyCheck()
            .getResponseType(), SafetyCheckType.NUMERIC);
        
        //Final YES
        try {
            responses = vehicleSafetyCheckResponseManager.executeCheckResponse(
                operator, "",
                VehicleSafetyCheckRepairActionType.NoAction.toValue(), vehicle,
                checks, VehicleSafetyCheckResponseType.YES.toString(), new Date());
        } catch (Exception e) {
            fail();
        }
        
        assertNotNull(responses);
        assertEquals(responses.size(), checks.size());
        for (VehicleSafetyCheckResponse vehicleSafetyCheckResponse1 : responses) {
            if (vehicleSafetyCheckResponse1.getSafetyCheck()
                .getSpokenDescription().equals("Breaks")) {
                assertEquals(
                    vehicleSafetyCheckResponse1.getRepairAction(),
                    VehicleSafetyCheckRepairActionType.QuickRepair);
            } else {
                assertEquals(
                    vehicleSafetyCheckResponse1.getRepairAction(),
                    VehicleSafetyCheckRepairActionType.NoAction);
            }
            if (vehicleSafetyCheckResponse1.getSafetyCheck()
                .getSpokenDescription().equals("Miles run")) {
                assertEquals(vehicleSafetyCheckResponse1.getCheckResponse()
                    .toString(), numericVal);
            } else {
                assertEquals(vehicleSafetyCheckResponse1.getCheckResponse()
                    .intValue(), VehicleSafetyCheckResponseType.YES.toValue());
            }
            assertEquals(vehicleSafetyCheckResponse1.getOperator(), operator);
            assertEquals(vehicleSafetyCheckResponse1.getVehicle(), vehicle);
        }
    }
    
    /**
     * @param vehicleManager the vehicleManager to set
     */
    @Test(enabled = false)
    public void setVehicleManager(VehicleManager vehicleManager) {
        this.vehicleManager = vehicleManager;
    }

    
    /**
     * @param operatorManager the operatorManager to set
     */
    @Test(enabled = false)
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    
    /**
     * @param vehicleSafetyCheckResponseManager the vehicleSafetyCheckResponseManager to set
     */
    @Test(enabled = false)
    public void setVehicleSafetyCheckResponseManager(
    VehicleSafetyCheckResponseManager vehicleSafetyCheckResponseManager) {
        this.vehicleSafetyCheckResponseManager = vehicleSafetyCheckResponseManager;
    }

    
    /**
     * @return the vehicleSafetyCheckManager
     */
    @Test(enabled = false)
    public VehicleSafetyCheckManager getVehicleSafetyCheckManager() {
        return vehicleSafetyCheckManager;
    }

    
    /**
     * @param vehicleSafetyCheckManager the vehicleSafetyCheckManager to set
     */
    @Test(enabled = false)
    public void setVehicleSafetyCheckManager(VehicleSafetyCheckManager vehicleSafetyCheckManager) {
        this.vehicleSafetyCheckManager = vehicleSafetyCheckManager;
    }
    
    
}
