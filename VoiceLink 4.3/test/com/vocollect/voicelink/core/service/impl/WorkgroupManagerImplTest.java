/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.TaskFunction;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.TaskFunctionManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * 
 * 
 * @author estoll
 */
@Test(groups = { FAST, SECURITY, CORE })

public class WorkgroupManagerImplTest extends BaseServiceManagerTestCase {

    private WorkgroupManager manager = null;

    private TaskFunctionManager tfManager = null;

    private RegionManager regionManager = null;

    protected static final String DATA_DIR = "data/dbunit/voicelink/";

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {

        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_SelectionRegions.xml",
            DatabaseOperation.INSERT);

    }

    /**
     * Setter for workgroup manager.
     * @param impl a manager implementation
     */
    @Test(enabled = false)
    public void setWorkgroupManager(WorkgroupManager impl) {
        this.manager = impl;
    }

    /**
     * Setter for task function manager.
     * @param impl a manager implementation
     */
    @Test(enabled = false)
    public void setTaskFunctionManager(TaskFunctionManager impl) {
        this.tfManager = impl;
    }

    /**
     * Setter for region manager.
     * @param impl a manager implementation
     */
    @Test(enabled = false)
    public void setRegionManager(RegionManager impl) {
        this.regionManager = impl;
    }

    /**
     * @throws BusinessRuleException - any business rule exception
     * @throws DataAccessException - any database exception
     */
    public void testSaveDefaultWorkgroup() throws BusinessRuleException,
        DataAccessException {
        Workgroup wg = new Workgroup();
        Long newWgId;
        wg.setGroupName("Test Saving with Default On");
        wg.setIsDefault(true);
        wg.setVersion(getAutowireMode());
        manager.save(wg);
        newWgId = wg.getId();

        wg = manager.findDefaultWorkgroup();
        assertNotNull(wg, "Did not find a default workgroup");
        assertEquals(newWgId, wg.getId(), "New Workgroup is not default");
    }

    /**
     * @throws BusinessRuleException - any business rule exception
     * @throws DataAccessException - any database exception
     */
    public void testSaveWorkgroup() throws BusinessRuleException,
        DataAccessException {
        Workgroup wg = new Workgroup();
        Long newWgId;
        wg.setGroupName("Test Saving with Default Off");
        wg.setIsDefault(false);
        wg.setVersion(getAutowireMode());
        manager.save(wg);
        newWgId = wg.getId();

        wg = manager.findDefaultWorkgroup();
        assertNotNull(wg, "Did not find a default workgroup");
        assertFalse(newWgId == wg.getId(), "New Workgroup is the default");
    }

    /**
     * @throws BusinessRuleException - any business rule exception
     * @throws DataAccessException - any database exception
     */
    public void testSaveWorkgroupWithFunctions() throws BusinessRuleException,
        DataAccessException {
        Workgroup wg = new Workgroup();
        TaskFunction tf = tfManager.getPrimaryDAO().get(-4L);
        List<Region> regions = regionManager.getPrimaryDAO()
            .listByTypeOrderByName(RegionType.Selection);
        wg.setGroupName("Saving with a Workgroup Function");
        wg.setIsDefault(false);
        wg.setVersion(getAutowireMode());
        wg.setAutoAddRegions(false);

        wg.addTaskFunction(tf, regions);
        manager.save(wg);
        Long newId = wg.getId();

        Workgroup wg2 = manager.getPrimaryDAO().get(newId);
        List<WorkgroupFunction> wgf = wg2.getWorkgroupFunctions();
        assertEquals(1, wgf.size(), "Workgroup Function count mismatch");

    }

    /**
     * @throws BusinessRuleException - any business rule exception
     * @throws DataAccessException - any database exception
     */
    public void testSaveWorkgroupWithChangingFunctions()
        throws BusinessRuleException, DataAccessException {
        Workgroup wg = new Workgroup();
        TaskFunction tf = tfManager.getPrimaryDAO().get(-4L);
        List<Region> regions = regionManager.getPrimaryDAO()
            .listByTypeOrderByName(RegionType.Selection);

        wg.setGroupName("Saving with Workgroup with changing Function");
        wg.setIsDefault(false);
        wg.setVersion(getAutowireMode());
        wg.setAutoAddRegions(false);

        wg.addTaskFunction(tf, regions);
        manager.save(wg);
        Long newId = wg.getId();

        Workgroup wg2 = manager.getPrimaryDAO().get(newId);
        List<WorkgroupFunction> wgf = wg2.getWorkgroupFunctions();
        assertEquals(1, wgf.size(), "Workgroup Function count mismatch");

        wg.removeTaskFunction(tf);
        manager.save(wg);

        List<WorkgroupFunction> wgf2 = wg2.getWorkgroupFunctions();
        assertEquals(0, wgf2.size(), "Workgroup Function count mismatch");

    }

    /**
     * @throws BusinessRuleException on BusinessRuleExceptions
     * @throws DataAccessException on database exceptions
     */
    public void testListAutoAddWorkgroupsFunction() 
        throws BusinessRuleException, DataAccessException {
        List<Workgroup> wgStart = manager.getAll();
        for (Workgroup workgroup : wgStart) {
            workgroup.setAutoAddRegions(true);
            manager.save(workgroup);
        }
        
        List<Workgroup> wgs = manager.listAutoAddWorkgroups();
        assertEquals(wgStart.size(), wgs.size(), "Expected number of AutoAddWorkgroups failed");
    }
    
}
