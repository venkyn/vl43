/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingType;
import com.vocollect.voicelink.core.service.DeliveryLocationMappingSettingManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import org.dbunit.operation.DatabaseOperation;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Test for the DeliveryLocationMappingSettingManagerImpl.
 * 
 * @author Ben Northrop
 */
@Test(groups = { FAST, SELECTION })

public class DeliveryLocationMappingSettingManagerImplTest extends
    BaseServiceManagerTestCase {

    /**
     * Helper manager to get mapping types.
     */
    private DeliveryLocationMappingSettingManager mappingSettingManager = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.test.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_DeliveryLocationMapping.xml",
            DatabaseOperation.CLEAN_INSERT);
    }

    /**
     * Test creating a mapping setting for a given site.
     * @throws Exception - any exception.
     */
    @Test
    public void testChangeMappingSetting() throws Exception {
        
        // default should be route
        DeliveryLocationMappingType mappingType = getManager().getMappingType();
        assertEquals(mappingType, DeliveryLocationMappingType.Route);

        // change to route
        getManager()
            .executeChangeMappingType(DeliveryLocationMappingType.Route);
        mappingType = getManager().getMappingType();
        Assert.assertEquals(DeliveryLocationMappingType.Route, mappingType);

        // change to customer number
        getManager().executeChangeMappingType(
            DeliveryLocationMappingType.CustomerNumber);
        mappingType = getManager().getMappingType();
        Assert.assertEquals(
            DeliveryLocationMappingType.CustomerNumber, mappingType);

        // change back to route
        getManager()
            .executeChangeMappingType(DeliveryLocationMappingType.Route);
        mappingType = getManager().getMappingType();
        Assert.assertEquals(DeliveryLocationMappingType.Route, mappingType);

    }

    /**
     * Getter for the deliveryLocationMappingTypeManager property.
     * @return DeliveryLocationMappingSettingManager value of the property
     */
    public DeliveryLocationMappingSettingManager getDeliveryLocationMappingSettingManager() {
        return this.mappingSettingManager;
    }

    /**
     * Convenience getter for the deliveryLocationMappingTypeManager property.
     * @return DeliveryLocationMappingSettingManager value of the property
     */
    public DeliveryLocationMappingSettingManager getManager() {
        return this.mappingSettingManager;
    }

    /**
     * Setter for the deliveryLocationMappingTypeManager property.
     * @param dlms the new deliveryLocationMappingSettingManager value
     */
    @Test(enabled = false)
    public void setDeliveryLocationMappingSettingManager(DeliveryLocationMappingSettingManager dlms) {
        this.mappingSettingManager = dlms;
    }

    /**
     * Convenience method for getting the default mapping type.
     * @return DeliveryLocationMappingType - the default.
     */
    public DeliveryLocationMappingType getDefaultMappingType() {
        return DeliveryLocationMappingType.getDefault();
    }

}
