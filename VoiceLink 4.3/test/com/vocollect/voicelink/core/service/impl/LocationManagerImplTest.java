/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationDescription;
import com.vocollect.voicelink.core.service.LocationManager;

import static com.vocollect.epp.test.TestGroups.PERFORMANCE;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;

import org.testng.annotations.Test;

/**
 * 
 * 
 * @author snalan
 */
@Test(groups = { CORE })

public class LocationManagerImplTest extends BaseServiceManagerTestCase {

    private LocationManager manager = null;

    private int recordCount = 50;

    protected static final Logger logger = new Logger(
        LocationManagerImplTest.class);

    /**
     * @param impl -
     */
    @Test(enabled = false)
    public void setLocationManager(LocationManager impl) {
        this.manager = impl;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        this.recordCount = getRecordCount();
        adapter.resetInstallationData();
    }

    /**
     * @throws Exception - any exception
     */
    @Test(groups = { PERFORMANCE })
    public void testSaveLocations() throws Exception {

        for (int i = 1; i <= recordCount; i++) {
            Location location = getLocationObj(i);
            manager.save(location);
        }
        logTimeTaken("Time taken to save " + recordCount
            + " Location records : ");

    }

    /**
     * @throws Exception - any exception
     */
    @Test(groups = { PERFORMANCE }, dependsOnMethods = { "testSaveLocations" })
    public void testGetLocations() throws Exception {

        manager.getAll();
        logTimeTaken("Time taken to retreive " + recordCount
            + " Location records : ");
    }

/*    *//**
     * @throws Exception - any exception
     *//*
    @Test(groups = { PERFORMANCE }, dependsOnMethods = { "testGetLocations" })
    public void testDeleteLocations() throws Exception {
        List<Location> locations = manager.getAll();
        for (Iterator<Location> itr = locations.iterator(); itr.hasNext();) {
            Location location = itr.next();
            manager.delete(location.getId());
        }
        logTimeTaken("Time taken to delete " + recordCount
            + " Location records : ");
    }
*/
    /**
     * @param number -
     * @return Location obj
     */
    @Test(enabled = false)
    private Location getLocationObj(int number) {

        Location location = new Location();
        location.setCheckDigits("");
        location.setCreatedDate(new Date());
        location.setDescription(new LocationDescription());
        location.setScannedVerification("Scanned Verify" + number);
        location.setSpokenVerification("Spoken Verify");
        location.setVersion(100);
        return location;

    }

    /**
     * @return int - count
     */
    private int getRecordCount() {
        String recordNumber = System.getProperty("test.parameter.records");
        int recCount = 0;
        try {
            recCount = Integer.parseInt(recordNumber);
        } catch (NumberFormatException ex) {
            recCount = recordCount;
        }

        return recCount;
    }

}
