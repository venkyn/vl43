/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.CoreErrorCode;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.SignOffManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * 
 * 
 * @author pfunyak
 */
@Test(groups = { FAST, SECURITY, CORE })

public class SignOffManagerImplTest extends BaseServiceManagerTestCase {

    private SignOffManager   signOffmanager = null;
    
    private OperatorManager  operatorManager = null;
    private LaborManager     laborManager = null;


    /**
     * 
     * @param impl - manager
     */
    @Test(enabled = false)
    public void setSignOffManager(SignOffManager impl) {
        this.signOffmanager = impl;
    }
    
      
    /**
     * Setter for the operatorManager property.
     * @param operatorManager the new operatorManager value
     */
    @Test(enabled = false)
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }
    
    
    
    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    @Test(enabled = false)
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
        
        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_SelectionRegions.xml", DatabaseOperation.INSERT);
        // add data specific to this test.
       
        adapter.handleFlatXmlResource(
                DATA_DIR + "VoiceLinkSampleOperators.xml",
                DatabaseOperation.INSERT);

    }

    /**
     * Test
     * This test simulates signing off an operator through the UI.
     * 
     * @throws Exception - database exceptions.
     */
    public void testExecute() throws Exception {
                
        // Get the operator that is currently signed off.
        Operator oper = this.operatorManager.findByIdentifier("092");
        this.signOffmanager.executeSignOffOperator(oper, new Date());
        
        try { //  try to sign off an operator that is already signed off (operator 92)
          this.signOffmanager.executeSignOffOperator(oper, new Date());
          // if we get here fail the test because an exception should have been thrown.
          fail("Operator already signed off exception was not thrown");
        } catch (BusinessRuleException bre) {
            assertEquals(CoreErrorCode.OPERATOR_ALREADY_SIGNED_OFF, bre.getErrorCode(),
                "Business rule sign off oper that is not signed on");
        }
        
        
        // get the operator that is signed on.
        oper = this.operatorManager.findByIdentifier("091");
        try { //  try to sign off an operator that is signed on (operator 91)
            this.signOffmanager.executeSignOffOperator(oper, new Date());
          } catch (BusinessRuleException bre) {
              if (bre.getErrorCode() == CoreErrorCode.OPERATOR_ALREADY_SIGNED_OFF) {
                  fail("Operator already signed off exception should not have been thrown");
              }    
          }
          // verify that we created an open signoff record.
         List <OperatorLabor> olList = laborManager.getOperatorLaborManager().listAllRecordsByOperatorId(oper.getId());
         assertEquals(1, olList.size(), "Operator labor records");
         OperatorLabor olRec = olList.get(0);
         assertEquals(OperatorLaborActionType.SignOff, olRec.getActionType(), "Invalid Action Type");
         assertNull(olRec.getEndTime(), "Operator Labor record is not open");

         // get another operator that is signed on and let
         // verify we can sign him off from the GUI
         oper = this.operatorManager.findByIdentifier("090");
         try { //  try to sign off an operator that is signed on (operator 91)
             this.signOffmanager.executeSignOffOperatorFromUI(oper, new Date());
           } catch (BusinessRuleException bre) {
               if (bre.getErrorCode() == CoreErrorCode.OPERATOR_ALREADY_SIGNED_OFF) {
                   fail("Operator already signed off exception should not have been thrown");
               }    
           }
    }

}
