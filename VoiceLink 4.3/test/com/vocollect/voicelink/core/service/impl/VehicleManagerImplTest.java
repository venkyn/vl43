/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;


import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Vehicle;
import com.vocollect.voicelink.core.model.VehicleCategory;
import com.vocollect.voicelink.core.service.VehicleManager;

import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;


/**
 * Unit test class for testing Vehicle manager.
 * @author mraj
 *
 */
@Test(groups = { CORE, VSC })
public class VehicleManagerImplTest extends BaseServiceManagerTestCase {

    private VehicleManager vehicleManager = null;
    
    /* (non-Javadoc)
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Core_VSC_data.xml", DatabaseOperation.INSERT);
    }

    
    /**
     * @return the vehicleManager
     */
    @Test(enabled = false)
    public VehicleManager getVehicleManager() {
        return vehicleManager;
    }

    
    /**
     * @param vehicleManager - the vehicle manager
     */
    @Test(enabled = false)
    public void setVehicleManager(VehicleManager vehicleManager) {
        this.vehicleManager = vehicleManager;
    }
    
    
    /**
     * test for existing vehicle with all matching criteria
     */
    @Test()
    public void testGetVehicleForExistingVehicle() throws Exception {
        String vehicleNumber = "PPT0002";
        String spokenVehicleNumber = "PPT02";
        int vehicleTypeNumber = 5;

        //Get vehicle by type and number
        Vehicle vehicle = getVehicleManager().findVehicleByNumberAndType(
            vehicleNumber, vehicleTypeNumber);
        assertNotNull(vehicle);
        assertEquals(vehicleNumber, vehicle.getVehicleNumber());
        assertEquals(spokenVehicleNumber, vehicle.getSpokenVehicleNumber());
        assertEquals(vehicleTypeNumber, vehicle.getVehicleType().getNumber()
            .intValue());
        assertEquals(VehicleCategory.Normal, vehicle.getVehicleCategory());
        
        //Get vehicle by type and spoken number
        vehicle = getVehicleManager().findVehicleBySpokenNumberAndType(
            spokenVehicleNumber, vehicleTypeNumber);
        assertNotNull(vehicle);
        assertEquals(vehicleNumber, vehicle.getVehicleNumber());
        assertEquals(spokenVehicleNumber, vehicle.getSpokenVehicleNumber());
        assertEquals(vehicleTypeNumber, vehicle.getVehicleType().getNumber()
            .intValue());
        assertEquals(VehicleCategory.Normal, vehicle.getVehicleCategory());
    }

    /**
     * test for not existing vehicle with category = default
     */
    @Test()
    public void testGetVehicleForDefaultVehicle() throws Exception {
        String vehicleNumber = "CFT01"; //Default vehicle
        int vehicleTypeNumber = 1;

        Vehicle vehicle = getVehicleManager().findVehicleByNumberAndType(
            vehicleNumber, vehicleTypeNumber);
        assertNull(vehicle);
    }
    
    /**
     * test for not existing vehicle with mismatching VT number
     */
    @Test()
    public void testGetVehicleForMisMatchingVehicleType() throws Exception {
        String vehicleNumber = "PPT02"; 
        int vehicleTypeNumber = 4; //MisMatching Vehicle Type number

        Vehicle vehicle = getVehicleManager().findVehicleByNumberAndType(
            vehicleNumber, vehicleTypeNumber);
        assertNull(vehicle);
    }
    
    /**
     * test for existing vehicle with all matching criteria
     */
    @Test()
    public void testGetVehicleForNonExistentVehcile() throws Exception {
        String vehicleNumber = "ALIEN"; //Doesn't exist 
        int vehicleTypeNumber = 4; 

        Vehicle vehicle = getVehicleManager().findVehicleByNumberAndType(
            vehicleNumber, vehicleTypeNumber);
        assertNull(vehicle);
    }
}
