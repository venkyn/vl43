/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.VehicleType;
import com.vocollect.voicelink.core.service.VehicleTypeManager;

import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;
import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Unit test class for testing Vehicle Type manager.
 * 
 * @author kudupi
 */
@Test(groups = { CORE, VSC })
public class VehicleTypeManagerImplTest extends BaseServiceManagerTestCase {

    private VehicleTypeManager vehicleTypeManager = null;

    protected static final Logger logger = new Logger(
        VehicleTypeManagerImplTest.class);

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Core_VSC_data.xml", DatabaseOperation.INSERT);
    }
    
    /**
     * Setter for vehicleTypeManager.
     * @param impl
     *            - implementation of Vehicle Type manager.
     */
    @Test(enabled = false)
    public void setVehicleTypeManager(VehicleTypeManager impl) {
        this.vehicleTypeManager = impl;
    }    
    
    /**
     * Method to perform data clean up after all unit test
     * cases are executed.
     * @throws Exception
     */
    @AfterClass()
    protected void cleanUp() throws Exception {
        classSetUp();
    }
    
    /**
     * Class-wide setup.
     * @throws Exception any
     */
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }     

    /**
     * Test case method to test the size of VehicleTypes 
     * existing the in the system.
     * 
     * @throws Exception - any exception
     */
    @Test()
    public void testGetVehicleTypes() throws Exception {
        List<VehicleType> listVehicleType = vehicleTypeManager.listAllVehicleTypesByNumber();
        assertEquals(8, listVehicleType.size());
    }
    
    /**
     * Test case method to test 0 VehicleTypes
     * returned from the system.
     *  
     * @throws Exception - any exception
     */
    @Test()
    public void testNoVehicleTypes() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        List<VehicleType> listVehicleType = vehicleTypeManager.listAllVehicleTypesByNumber();
        assertEquals(0, listVehicleType.size());
    }    
    
    /**
     * Test case method to test 0 Vehicles for a Vehicle Type
     * returned from the system.
     *  
     * @throws Exception - any exception
     */
    @Test()
    public void testNoVehiclesForVehicleType() throws Exception {
        VehicleType vehicleType1 = new VehicleType();
        assertNotNull(vehicleType1.getVehicleCount());
        assertEquals(Integer.valueOf(0), vehicleType1.getVehicleCount());
    }    

    
}
