/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.DeliveryLocationMapping;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingRoot;
import com.vocollect.voicelink.core.model.DeliveryLocationMappingType;
import com.vocollect.voicelink.core.service.DeliveryLocationMappingManager;
import com.vocollect.voicelink.core.service.DeliveryLocationMappingSettingManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.core.model.DeliveryLocationMappingType.Route;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * Testing the functionality of the DeliveryLocationMappingManagerImpl.
 * 
 * @author Ben Northrop
 */
@Test(groups = { FAST, SELECTION })

public class DeliveryLocationMappingManagerImplTest extends
    BaseServiceManagerTestCase {

    private DeliveryLocationMappingManager deliveryLocationMappingManager = null;

    private DeliveryLocationMappingSettingManager deliveryLocationMappingSettingManager = null;

    // Test data inserted from DbUnit on init
    private static final Long SAMPLE_MAPPING_ID1 = new Long(-1L);

    private static final Long SAMPLE_MAPPING_ID2 = new Long(-2L);

    private static final String SAMPLE_ROUTE_1 = "20";

    private static final String SAMPLE_DELIVERY_LOCATION_1 = "-60";

    private static final int SAMPLE_NUM_MAPPINGS = 1;

    private static final int SAMPLE_NUM_MAPPINGS_ROUTE = 1;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.test.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_DeliveryLocationMapping.xml",
            DatabaseOperation.CLEAN_INSERT);
    }

    /**
     * Test a simple insert and delete of a mapping.
     * @throws Exception - any exception
     */
    @Test
    public void testMappings() throws Exception {
        
        //==================================================================
        //Test a simple insert and delete of a mapping.
        DeliveryLocationMapping mapping = new DeliveryLocationMapping(
            "2222", "5", DeliveryLocationMappingType.CustomerNumber);
        this.getDeliveryLocationMappingManager().save(mapping);

        this.getDeliveryLocationMappingManager().delete(mapping);

        //==================================================================
        // Test inserting a new delivery location mapping and ensuring that two
        // duplicates cannot be inserted.
        final DeliveryLocationMappingManager customerNumberManager = this
            .getDeliveryLocationMappingManager();

        mapping = new DeliveryLocationMapping(
            "1234", "1", DeliveryLocationMappingType.CustomerNumber);
        DeliveryLocationMapping mappingDuplicate = new DeliveryLocationMapping(
            "1234", "2", DeliveryLocationMappingType.CustomerNumber);

        customerNumberManager.save(mapping);

        try {
            customerNumberManager.save(mappingDuplicate);
            fail("Should have received exception for duplicate insert!");
        } catch (BusinessRuleException bre) {
            log.debug("  received business rule exception, as expected");
        }

        customerNumberManager.delete(mapping);
        log.debug("  deleted mapping.");

        //==================================================================
        // Test the modification of some delivery location mappings.
        final DeliveryLocationMappingManager manager = this
            .getDeliveryLocationMappingManager();

        Collection<Long> ids = new ArrayList<Long>();
        ids.add(SAMPLE_MAPPING_ID1);
        ids.add(SAMPLE_MAPPING_ID2);

        String newDeliveryLocation = "10";
        manager.executeModifyDeliveryLocation(ids, newDeliveryLocation);

        DeliveryLocationMappingRoot dlm1 = manager.get(SAMPLE_MAPPING_ID1);
        DeliveryLocationMappingRoot dlm2 = manager.get(SAMPLE_MAPPING_ID2);
        assertEquals(dlm1.getDeliveryLocation(), newDeliveryLocation);
        assertEquals(dlm2.getDeliveryLocation(), newDeliveryLocation);

        //==================================================================
        //Find a delivery location given a customer number.
        initData(new DbUnitAdapter());
        String deliveryLocation = this.getDeliveryLocationMappingManager()
            .findDeliveryLocation(SAMPLE_ROUTE_1);
        assertEquals(deliveryLocation, SAMPLE_DELIVERY_LOCATION_1);

        deliveryLocation = this.getDeliveryLocationMappingManager()
            .findDeliveryLocation("-12341234");
        assertNull(deliveryLocation);

        deliveryLocation = this.getDeliveryLocationMappingManager()
            .findDeliveryLocation("");
        assertNull(deliveryLocation);

        deliveryLocation = this.getDeliveryLocationMappingManager()
            .findDeliveryLocation("");
        assertNull(deliveryLocation);

        //==================================================================
        //Test that a given mapping rule exists or doesn't.
        mapping = this.getDeliveryLocationMappingManager().get(SAMPLE_MAPPING_ID1);
        
        // test existing mapping
        try {
            this.getDeliveryLocationMappingManager()
                .validateMappingRuleDoesNotExist(mapping);
            fail("Should not have reached here, mapping should exist!");
        } catch (BusinessRuleException bre) {
            // expected exception
        }
        
        // test null mapping
        try {
            mapping = null;
            this.getDeliveryLocationMappingManager()
                .validateMappingRuleDoesNotExist(mapping);
            fail("Should not have reached here, mapping is null!");
        } catch (InvalidParameterException ipe) {
            // exception expected
        }

        // test non-existent mapping
        try {
            mapping = new DeliveryLocationMapping();
            mapping.setDeliveryLocation(SAMPLE_ROUTE_1);
            mapping.setMappingValue(SAMPLE_DELIVERY_LOCATION_1);
            this.getDeliveryLocationMappingManager()
                .validateMappingRuleDoesNotExist(mapping);
        } catch (BusinessRuleException bre) {
            fail("Should not have reached here, mapping should not exist!");
        }

        //==================================================================
        //Test getting all delivery locations - assuming sample data installed, so
        //should be 4, all customer number mappings.
        List<DeliveryLocationMapping> mappings = this
            .getDeliveryLocationMappingManager().getMappings();
        assertNotNull(mappings);
        assertEquals(mappings.size(), SAMPLE_NUM_MAPPINGS);
        for (DeliveryLocationMapping dlm : mappings) {
            assertEquals(dlm.getMappingType(), Route);
        }

        //==================================================================
        //Test getting all delivery locations - assuming sample data installed, so
        //should be 1 if set to route.
        this.getDeliveryLocationMappingSettingManager()
            .executeChangeMappingType(Route);
        mappings = this.getDeliveryLocationMappingManager().getMappings();
        assertNotNull(mappings);
        assertEquals(mappings.size(), SAMPLE_NUM_MAPPINGS_ROUTE);
        for (DeliveryLocationMapping dlm : mappings) {
            assertEquals(dlm.getMappingType(), Route);
        }
    }

    /**
     * Getter for the deliveryLocationMappingManager property.
     * @return DeliveryLocationMappingCustomerNumberManager value of the
     *         property
     */
    public DeliveryLocationMappingManager getDeliveryLocationMappingManager() {
        return this.deliveryLocationMappingManager;
    }

    /**
     * Setter for the deliveryLocationMappingManager property.
     * @param deliveryLocationMappingManager the new
     *            deliveryLocationMappingManager value
     */
    @Test(enabled = false)
    public void setDeliveryLocationMappingManager(DeliveryLocationMappingManager deliveryLocationMappingManager) {
        this.deliveryLocationMappingManager = deliveryLocationMappingManager;
    }

    /**
     * Getter for the deliveryLocationMappingSettingManager property.
     * @return DeliveryLocationMappingSettingManager value of the property
     */
    public DeliveryLocationMappingSettingManager getDeliveryLocationMappingSettingManager() {
        return this.deliveryLocationMappingSettingManager;
    }

    /**
     * Setter for the deliveryLocationMappingSettingManager property.
     * @param deliveryLocationMappingSettingManager the new
     *            deliveryLocationMappingSettingManager value
     */
    @Test(enabled = false)
    public void setDeliveryLocationMappingSettingManager(
           DeliveryLocationMappingSettingManager deliveryLocationMappingSettingManager) {
        this.deliveryLocationMappingSettingManager = deliveryLocationMappingSettingManager;
    }

}
