/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.TaskFunction;
import com.vocollect.voicelink.core.service.TaskFunctionManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * 
 * 
 * @author estoll
 */
@Test(groups = { FAST, SECURITY, CORE })

public class TaskFunctionManagerImplTest extends BaseServiceManagerTestCase {

    private TaskFunctionManager manager = null;

    protected static final String DATA_DIR = "data/dbunit/voicelink/";

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {

        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_SelectionRegions.xml",
            DatabaseOperation.INSERT);

    }

    /**
     * @param impl TaskFunctionManager
     */
    @Test(enabled = false)
    public void setTaskFunctionManager(TaskFunctionManager impl) {
        this.manager = impl;
    }

    /**
     * Test
     * 'com.vocollect.voicelink.core.service.impl.TaskFunctionManagerImpl.listFunctionsByWorkgroup'
     * to return a list of regions.
     * @throws DataAccessException - database exceptions.
     */
    public void testListFunctionsByWorkgroupReturnsMany()
        throws DataAccessException {
        Long workgroupId = -1L;

        List<TaskFunction> allFunctions = this.manager.getAll();
        List<TaskFunction> taskFunctions = this.manager
            .listFunctionsByWorkgroup(workgroupId);

        assertEquals((Integer) allFunctions.size(), (Integer) taskFunctions.size());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.service.impl.TaskFunctionManagerImpl.listFunctionsByWorkgroup'
     * when zero taskfunctions are authroized.
     * @throws DataAccessException - database exceptions.
     */
    public void testListFunctionsByWorkGroupReturnsNone()
        throws DataAccessException {
        Long workgroupId = -2L;

        List<TaskFunction> taskFunctions = this.manager
            .listFunctionsByWorkgroup(workgroupId);

        assertEquals((Integer) 0, (Integer) taskFunctions.size());

    }

}
