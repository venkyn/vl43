/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.BreakType;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.service.BreakTypeManager;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.LaborManagerRoot;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.SignOffManager;
import com.vocollect.voicelink.lineloading.model.CartonDetail;
import com.vocollect.voicelink.lineloading.service.CartonDetailManager;
import com.vocollect.voicelink.loading.model.LoadingContainer;
import com.vocollect.voicelink.loading.model.LoadingRouteLabor;
import com.vocollect.voicelink.loading.service.LoadingContainerManager;
import com.vocollect.voicelink.loading.service.LoadingRouteLaborManager;
import com.vocollect.voicelink.putaway.model.LicenseDetail;
import com.vocollect.voicelink.putaway.service.LicenseDetailManager;
import com.vocollect.voicelink.putaway.service.LicenseManager;
import com.vocollect.voicelink.puttostore.model.PtsLicense;
import com.vocollect.voicelink.puttostore.model.PtsLicenseLabor;
import com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManager;
import com.vocollect.voicelink.puttostore.service.PtsLicenseManager;
import com.vocollect.voicelink.replenishment.model.Replenishment;
import com.vocollect.voicelink.replenishment.model.ReplenishmentDetail;
import com.vocollect.voicelink.replenishment.service.ReplenishmentDetailManager;
import com.vocollect.voicelink.replenishment.service.ReplenishmentManager;
import com.vocollect.voicelink.selection.model.AssignmentLabor;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;
import com.vocollect.voicelink.selection.service.PickDetailManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * 
 * 
 * @author pfunyak
 */
@Test(groups = { FAST, SECURITY, CORE })

public class LaborManagerImplTest extends BaseServiceManagerTestCase {

    private LaborManagerRoot            laborManager = null;
    private OperatorManager             operatorManager = null;
    private SignOffManager              signOffManager = null;
    private PickDetailManager           pickDetailManager = null;
    private LicenseManager              licenseManager = null;
    private LicenseDetailManager        licenseDetailManager = null;
    private ReplenishmentManager        replenManager = null;
    private ReplenishmentDetailManager  replenishmentDetailManager = null;
    private CartonDetailManager         cartonDetailManager = null;
    private PtsLicenseManager           ptsLicenseManager = null;
    private BreakTypeManager            breakTypeManager = null;
    private LoadingRouteLaborManager    loadingLaborManager = null;
    private LoadingContainerManager     loadingContainerManager = null; 
    
    // common variables used by tests.
    private OperatorLaborManager        olManager;
    private AssignmentLaborManager      alManager;
    private OperatorLabor               olRec;
    private List <AssignmentLabor>      alList; 
    private AssignmentLabor             alRec;    
    private PtsLicenseLaborManager      llManager;
    
    
    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    @Test(enabled = false)
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    
    /**
     * Setter for the operatorManager property.
     * @param operatorManager the new operatorManager value
     */
    @Test(enabled = false)
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    
    /**
     * Setter for the signOffManager property.
     * @param signOffManager the new signOffManager value
     */
    @Test(enabled = false)
    public void setSignOffManager(SignOffManager signOffManager) {
        this.signOffManager = signOffManager;
    }

    
    /**
     * Setter for the pickDetailManager property.
     * @param pickDetailManager the new pickDetailManager value
     */
    @Test(enabled = false)
    public void setPickDetailManager(PickDetailManager pickDetailManager) {
        this.pickDetailManager = pickDetailManager;
    }

    
    /**
     * Getter for the licenseDetailManager property.
     * @return LicenseDetailManager value of the property
     */
    @Test(enabled = false)
    public LicenseDetailManager getLicenseDetailManager() {
        return this.licenseDetailManager;
    }


    
    /**
     * Setter for the licenseDetailManager property.
     * @param licenseDetailManager the new licenseDetailManager value
     */
    @Test(enabled = false)
    public void setLicenseDetailManager(LicenseDetailManager licenseDetailManager) {
        this.licenseDetailManager = licenseDetailManager;
    }


    
    /**
     * Getter for the licenseManager property.
     * @return LicenseManager value of the property
     */
    @Test(enabled = false)
    public LicenseManager getLicenseManager() {
        return this.licenseManager;
    }


    
    /**
     * Setter for the licenseManager property.
     * @param licenseManager the new licenseManager value
     */
    @Test(enabled = false)
    public void setLicenseManager(LicenseManager licenseManager) {
        this.licenseManager = licenseManager;
    }        

    /**
     * Getter for the replenManager property.
     * @return ReplenishmentManager value of the property
     */
    @Test(enabled = false)
    public ReplenishmentManager getReplenishmentManager() {
        return this.replenManager;
    }


    
    /**
     * Setter for the replenManager property.
     * @param replenishmentManager the new replenManager value
     */
    @Test(enabled = false)
    public void setReplenishmentManager(ReplenishmentManager replenishmentManager) {
        this.replenManager = replenishmentManager;
    }    

    /**
     * Getter for the replenishmentDetailManager property.
     * @return ReplenishmentDetailManager value of the property
     */
    @Test(enabled = false)
    public ReplenishmentDetailManager getReplenishmentDetailManager() {
        return this.replenishmentDetailManager;
    }


    
    /**
     * Setter for the replenishmentDetailManager property.
     * @param replenishmentDetailManager the new replenishmentDetailManager value
     */
    @Test(enabled = false)
    public void setReplenishmentDetailManager(ReplenishmentDetailManager replenishmentDetailManager) {
        this.replenishmentDetailManager = replenishmentDetailManager;
    }
    
    /**
     * Getter for the cartonDetailManager property.
     * @return CartonDetailManager value of the property
     */
    @Test(enabled = false)
    public CartonDetailManager getCartonDetailManager() {
        return this.cartonDetailManager;
    }
    
    /**
     * Setter for the cartonDetailManager property.
     * @param cartonDetailManager the new cartonDetailManager value
     */
    @Test(enabled = false)
    public void setCartonDetailManager(CartonDetailManager cartonDetailManager) {
        this.cartonDetailManager = cartonDetailManager;
    }    

    
    /**
     * Getter for the ptsLicenseManager property.
     * @return PtsLicenseManager value of the property
     */
    @Test(enabled = false)
    public PtsLicenseManager getPtsLicenseManager() {
        return this.ptsLicenseManager;
    }


    
    /**
     * Setter for the ptsLicenseManager property.
     * @param ptsLicenseManager the new ptsLicenseManager value
     */
    @Test(enabled = false)
    public void setPtsLicenseManager(PtsLicenseManager ptsLicenseManager) {
        this.ptsLicenseManager = ptsLicenseManager;
    }


    
    /**
     * Getter for the breakTypeManager property.
     * @return BreakTypeManager value of the property
     */
    @Test(enabled = false)
    public BreakTypeManager getBreakTypeManager() {
        return this.breakTypeManager;
    }


    
    /**
     * Setter for the breakTypeManager property.
     * @param breakTypeManager the new breakTypeManager value
     */
    @Test(enabled = false)
    public void setBreakTypeManager(BreakTypeManager breakTypeManager) {
        this.breakTypeManager = breakTypeManager;
    }

    
    /**
     * Getter for the loadingLaborManager property.
     * @return LoadingRouteLaborManager value of the property
     */
    @Test(enabled = false)
    public LoadingRouteLaborManager getLoadingLaborManager() {
        return loadingLaborManager;
    }
    

    /**
     * Setter for the loadingLaborManager property.
     * @param loadingLaborManager the new loadingLaborManager value
     */
    @Test(enabled = false)
    public void setLoadingLaborManager(LoadingRouteLaborManager loadingLaborManager) {
        this.loadingLaborManager = loadingLaborManager;
    }

    
    /**
     * Getter for the loadingContainerManager property.
     * @return LoadingContainerManager value of the property
     */
    @Test(enabled = false)
    public LoadingContainerManager getLoadingContainerManager() {
        return loadingContainerManager;
    }

    
    /**
     * Setter for the loadingContainerManager property.
     * @param loadingContainerManager the new loadingContainerManager value
     */
    @Test(enabled = false)
    public void setLoadingContainerManager(LoadingContainerManager loadingContainerManager) {
        this.loadingContainerManager = loadingContainerManager;
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override   
    protected void initData(DbUnitAdapter adapter) throws Exception {
        // We are not using this method to load data.  
        // We are using a loadData method so we can seperate the tests into multiple
        // test methods and not have to load the data before each test.
     }
    
    
    /**
     * @throws Exception
     */
    @BeforeClass()
    public void loadData() throws Exception {
        
        DbUnitAdapter adapter = new DbUnitAdapter();
        
        adapter.resetInstallationData();
        
        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_Core.xml",
            DatabaseOperation.CLEAN_INSERT);
        
        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_ForkAppWorkGroups.xml",
            DatabaseOperation.INSERT);
        
        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_SelectionRegions.xml",
            DatabaseOperation.INSERT);
        
        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_PutawayRegions.xml",
            DatabaseOperation.INSERT);
        
        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_ReplenRegions.xml",
            DatabaseOperation.INSERT);
        
        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_LineLoadRegions.xml",
            DatabaseOperation.INSERT);
             
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_PtsRegions.xml",
                DatabaseOperation.REFRESH);
        
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_LoadingRegions.xml",
            DatabaseOperation.INSERT);

        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_LaborManager.xml",
            DatabaseOperation.INSERT);
        
        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataUnitTest_PtsData.xml",
            DatabaseOperation.REFRESH);
    }
    
    /**
     * Test
     * This test simulates editing the end date from the PutAway UI.
     * 
     * @throws Exception - database exceptions.
     */
    @Test()
    public void testExecuteSelectionCases() throws Exception {
        
        // test for open operator labor with no assignment labor
        // records and nothing picked. 
        // Should return start time of Operator labor record.
        olManager = laborManager.getOperatorLaborManager();
        alManager = laborManager.getAssignmentLaborManager();
       
        Operator operator =  this.operatorManager.get(7L);
        Date signOffDate = this.signOffManager.getEarliestSignOffTime(operator);
        olRec = olManager.findOpenRecordByOperatorId(7L);
        assertEquals(signOffDate.getTime(), olRec.getStartTime().getTime(),
                    "Dates not equal");
        
        // test for open operator labor with purged assignment labor records 
        // with items picked.
        operator =  this.operatorManager.get(6L);
        signOffDate = this.signOffManager.getEarliestSignOffTime(this.operatorManager.get(6L));
        olRec = olManager.findOpenRecordByOperatorId(6L);
        assertNotNull(signOffDate.getTime(), "Date should not be null");
        
        // Test for open operator labor with a closed assignment labor record and
        // no pick details.  Should return assignment labor start time
        olRec = olManager.findOpenRecordByOperatorId(8L);
        alList = alManager.listAllRecordsByOperatorLaborId(olRec.getId());
        // expecting 2 assignment labor records
        assertEquals(alList.size(), 2, " not enough records");
        alRec = alList.get(1);
        operator =  this.operatorManager.get(8L);
        signOffDate = this.signOffManager.getEarliestSignOffTime(operator);
        assertEquals(alRec.getStartTime().getTime(), signOffDate.getTime(), "Dates don't equal");    
                       
        // Test for open operator labor with an open assignment labor record.
        // Should return last pick detail time
        olRec = olManager.findOpenRecordByOperatorId(1L);
        alList = alManager.listAllRecordsByOperatorLaborId(olRec.getId());
        // expecting 2 assignment labor records
        assertEquals(alList.size(), 2, " not enough records");
        alRec = alList.get(1);
        operator = this.operatorManager.get(1L);
        signOffDate = this.signOffManager.getEarliestSignOffTime(operator);
        Date pickDate = 
            this.pickDetailManager.maxEndTime(operator.getId(), olRec.getStartTime(), olRec.getEndTime()); 
        assertEquals(signOffDate.getTime(), pickDate.getTime(), "Dates don't equal");
        
        // Test for open operator labor with an open assignment labor record.
        // and no pick details,
        // Should return start time of the assignment labor record
        olRec = olManager.findOpenRecordByOperatorId(2L);
        alList = alManager.listAllRecordsByOperatorLaborId(olRec.getId());
        // expecting 2 assignment labor records
        assertEquals(alList.size(), 2, " not enough records");
        alRec = alList.get(1);
        operator = this.operatorManager.get(2L);
        signOffDate = this.signOffManager.getEarliestSignOffTime(operator);
        assertEquals(signOffDate.getTime(), alRec.getStartTime().getTime(), "Dates don't equal");
        
        // test for open operator labor with a closed assignment labor record
        // that has pick details. should return the time from the last pick detail.
        // this is the min date that would be returned when changing the end time
        // from the operator labor view after the operator was previously
        // signed off from the GUI.
        olRec = olManager.findOpenRecordByOperatorId(3L);
        assertEquals(olRec.getActionType(), OperatorLaborActionType.SignOff, "operator should be signed off");
        operator =  this.operatorManager.get(3L);
        alList = alManager.listAllRecordsByOperatorLaborId(-32L);
        alRec = alList.get(0);
        
        signOffDate = laborManager.getEarliestEndTime(-32L);
        pickDate = 
            this.pickDetailManager.maxEndTime(operator.getId(), alRec.getStartTime(), alRec.getEndTime()); 
        assertEquals(signOffDate.getTime(), alRec.getEndTime().getTime(), "Dates don't equal");

        // test for open operator labor with a closed assignment labor record
        // that has pick details. should return the assignment end time.
        // this is the min date that would be returned when signing an operator 
        // off from the GUI.
        olRec = olManager.findOpenRecordByOperatorId(4L);
        operator =  this.operatorManager.get(4L);
        alList = alManager.listAllRecordsByOperatorLaborId(olRec.getId());
        alRec = alList.get(0);
        
        signOffDate = this.signOffManager.getEarliestSignOffTime(operator);
        assertEquals(signOffDate.getTime(), alRec.getEndTime().getTime(), "Dates don't equal");

        // Operator started assignment and picked some but did not sign off and  
        // was subsequently signed off from the GUI. 
        // the earliest end time returned in this case should be the latest pick
        // detail end time.
        olRec = olManager.findOpenRecordByOperatorId(5L);
        assertEquals(olRec.getActionType(), OperatorLaborActionType.SignOff, "operator should be signed off");
        operator =  this.operatorManager.get(5L);
        alList = alManager.listAllRecordsByOperatorLaborId(-52L);
        alRec = alList.get(0);
        
        signOffDate = laborManager.getEarliestEndTime(-52L);
        pickDate = 
            this.pickDetailManager.maxEndTime(operator.getId(), alRec.getStartTime(), alRec.getEndTime()); 
        assertEquals(signOffDate.getTime(), pickDate.getTime(), "Dates don't equal");
    }

    /**
     * Test
     * This test simulates editing the end date from the PutAway UI.
     * 
     * @throws Exception - database exceptions.
     */
    @Test()
    public void testExecutePutAwayCases() throws Exception {
        // PUT AWAY

        olManager = laborManager.getOperatorLaborManager();

        // test for open operator labor with no closed license records. 
        // This case will occur if the operator starts a put, does not complete
        // it and does not sign off.
        olRec = olManager.findOpenRecordByOperatorId(13L);
        Operator operator = this.operatorManager.get(13L);
                
        Date signOffDate = this.signOffManager.getEarliestSignOffTime(operator);
        assertEquals(signOffDate.getTime(), olRec.getStartTime().getTime(), "Dates not equal");
        
        // test for open operator labor with completed replen record. 
        // Should return put time of license detail record.
        // In this case the operator completes a partial put but does not 
        // complete the entire put (In Progress status) and does not sign off.
        olRec = olManager.findOpenRecordByOperatorId(10L);
        operator = this.operatorManager.get(10L);
        
        List<LicenseDetail> details = this.getLicenseDetailManager()
            .listMostRecentRecordsByOperator(operator, olRec.getStartTime());
        LicenseDetail detail = details.get(0);

        signOffDate = this.signOffManager.getEarliestSignOffTime(operator);
        assertEquals(signOffDate.getTime(), detail.getPutTime().getTime(), "Dates not equal");
        
        // test for open operator labor with open license records. 
        // Should return put time of license detail labor record.
        // In this case the operator started a put, completed it
        // and did not sign off.
        olRec = olManager.findOpenRecordByOperatorId(11L);
        operator = this.operatorManager.get(11L);
        
        details = 
            this.getLicenseDetailManager().listMostRecentRecordsByOperator(operator, olRec.getStartTime());
        detail = details.get(0);
        
        signOffDate = this.signOffManager.getEarliestSignOffTime(operator);
        assertEquals(signOffDate.getTime(), detail.getPutTime().getTime(), "Dates not equal");

        // get End Time test for closed operator labor with closed license records. 
        // Should return start time of license detail record.
        // the operator starts a put, completes it but does not sign off and 
        // is subsequently signed off from the GUI.
        operator = this.operatorManager.get(12L);
        olRec = olManager.get(-302L);
        
        details = 
            this.getLicenseDetailManager().listMostRecentRecordsByOperator(operator, olRec.getStartTime());
        detail = details.get(0);
        
        signOffDate = laborManager.getEarliestEndTime(olRec.getId());
        assertEquals(signOffDate.getTime(), detail.getPutTime().getTime(), "Dates not equal");
        
        // get End Time test for closed operator labor with closed license records. 
        // Should return start time of license detail record.
        // the operator starts a put, completes it but does not sign off and 
        // is subsequently signed off from the GUI.
        operator = this.operatorManager.get(14L);
        olRec = olManager.get(-502L);
        
        details = 
            this.getLicenseDetailManager().listMostRecentRecordsByOperator(operator, olRec.getStartTime());
        detail = details.get(0);
        
        signOffDate = laborManager.getEarliestEndTime(-502L);
        assertEquals(signOffDate.getTime(), detail.getPutTime().getTime(), "Dates not equal");
    }    

    /**
     * Test This test simulates editing the end date from the PutAway UI.
     * 
     * @throws Exception - database exceptions.
     */
    @Test()
    public void testExecuteReplenishmentCases() throws Exception {
        // Replenishment

        olManager = laborManager.getOperatorLaborManager();

        // test for open operator labor with no replen records.
        // This case will occur if the operator starts a replen, does not complete
        // it and does not sign off.
        Operator operator = this.operatorManager.get(221L);
        olRec = olManager.findOpenRecordByOperatorId(operator.getId());

        Date signOffDate = this.signOffManager.getEarliestSignOffTime(operator);
        assertEquals(signOffDate.getTime(), olRec.getStartTime().getTime(), "Dates not equal");

        // test for open operator labor with open replen records.
        // Should return replen time of replen detail record.
        // In this case the operator completes a partial replen but does not
        // complete the entire assignment (In Progress status) and does not sign off.
        operator = this.operatorManager.get(223L);
        olRec = olManager.findOpenRecordByOperatorId(operator.getId());


        List<ReplenishmentDetail> replenDetails = this.getReplenishmentDetailManager()
            .listMostRecentRecordsByOperator(operator, olRec.getStartTime());
        ReplenishmentDetail replenDetail = replenDetails.get(0);

        signOffDate = this.signOffManager.getEarliestSignOffTime(operator);
        assertEquals(
            signOffDate.getTime(), replenDetail.getReplenishTime().getTime(), "Dates not equal");

        // test for open operator labor with completed replen record.
        // Should return end time of replen record.
        // In this case the operator started a replenishment, completed it
        // and did not sign off.
        operator = this.operatorManager.get(222L);
        olRec = olManager.findOpenRecordByOperatorId(operator.getId());

        List<Replenishment> replens = this.getReplenishmentManager()
            .listRecordsByOperatorGreaterThanDate(operator, olRec.getStartTime());

        signOffDate = this.signOffManager.getEarliestSignOffTime(operator);
        assertEquals(
            signOffDate.getTime(), replens.get(0).getEndTime().getTime(), "Dates not equal");

        // Get earliest End Time test for closed operator labor record.
        // Should return start time of replen record.
        // The operator starts a replenishment, completes it but does not
        // sign off and is subsequently signed off from the GUI.
        operator = this.operatorManager.get(220L);
        olRec = olManager.get(-112L);

        replens = this.getReplenishmentManager().listMostRecentRecordsByOperator(
            operator, olRec.getStartTime(), olRec.getEndTime());

        signOffDate = laborManager.getEarliestEndTime(-112L);
        assertEquals(
            signOffDate.getTime(), replens.get(0).getEndTime().getTime(), "Dates not equal");

        // Get earliest End Time test for closed operator labor record.
        // Should return replen time of replen detail record.
        // The operator starts a replenishment, does a parial and does not
        // sign off then is subsequently signed off from the GUI. Now the GUI
        // user decides they want to update the labor record for the operator.

        // get the original closed replen record
        operator = this.operatorManager.get(224L);
        olRec = olManager.get(-512L);

        replens = this.getReplenishmentManager().listMostRecentRecordsByOperator(
            operator, olRec.getStartTime(), olRec.getEndTime());

        replenDetails = this.getReplenishmentDetailManager().listMostRecentRecordsByOperator(
            operator, olRec.getStartTime());
        replenDetail = replenDetails.get(0);

        signOffDate = laborManager.getEarliestEndTime(olRec.getId());
        assertEquals(
            signOffDate.getTime(), replenDetail.getReplenishTime().getTime(), "Dates not equal");

    }

    /**
     * Test This test simulates editing the end date from the PutAway UI.
     * 
     * @throws Exception - database exceptions.
     */
    @Test()
    public void testExecuteLineLoadingCases() throws Exception {
        // Replenishment

        olManager = laborManager.getOperatorLaborManager();

        // get earliest end time test for open operator labor record with
        // carton detail records. This is the case where an assignment is
        // completed but the operator has not signed off.
        Operator operator = this.operatorManager.get(232L);
        olRec = olManager.findOpenRecordByOperatorId(operator.getId());

        List<CartonDetail> cartonDetails = this.getCartonDetailManager()
            .listRecordsByOperatorGreaterThanDate(operator, olRec.getStartTime());
        CartonDetail cartonDetail = cartonDetails.get(0);

        Date signOffDate = this.signOffManager.getEarliestSignOffTime(operator);
        assertEquals(
            signOffDate.getTime(), cartonDetail.getActionTime().getTime(), "Dates not equal");

        // get Earliest End Time test for closed operator labor record with
        // carton detail records.
        // Should return actionTime of carton detail record.
        // The operator starts an assignment, does not complete it, does not
        // sign off and is subsequently signed off from the GUI.
        operator = this.operatorManager.get(231L);
        olRec = olManager.get(-122L);

        cartonDetails = this.getCartonDetailManager().listMostRecentRecordsByOperator(
            operator, olRec.getStartTime(), olRec.getEndTime());

        signOffDate = laborManager.getEarliestEndTime(olRec.getId());
        assertEquals(
            signOffDate.getTime(), cartonDetails.get(0).getActionTime().getTime(),
            "Dates not equal");
    }

    /**
     * Test
     * This test simulates operator actions in put to store.
     * 
     * @throws Exception - database exceptions.
     */
    @Test()
    // PMF - 6-9-2008
    // These tests should have been handled in the task command testing.
    // The purpose of the tests in this file are to test the getEarilestEndTime method
    // for the given application.
    public void testExecutePutToStoreCases() throws Exception {
        
        olManager = laborManager.getOperatorLaborManager();
        llManager = laborManager.getPtsLicenseLaborManager();
        
        Long operatorId = 7L;
        Long licenseId = -98L;
       
        Operator operator =  this.operatorManager.get(operatorId);
        
        /***********************************************************************************
         * simulates (labor only) an operator signing on, getting a region configuration, 
         * getting an assignment, taking a break, passing an assignment and then signing off.
        ************************************************************************************/
        // close any existing labor
        olManager.closeLaborRecord(new Date(), operator);
        
        //Confirm operator has no open labor
        assertNull(olManager.findOpenRecordByOperatorId(operatorId));
        //Simulate sign on
        this.laborManager.openOperatorLaborRecord(new Date(), operator, OperatorLaborActionType.SignOn);
        // Confirm labor reflects sign on.
        OperatorLabor operatorLaborToTest = olManager.findOpenRecordByOperatorId(operatorId);
        assertNotNull(operatorLaborToTest);
        assertEquals(operatorLaborToTest.getActionType(), OperatorLaborActionType.SignOn);
        
        //Simulate getting a region configuration
        this.laborManager.openOperatorLaborRecord(new Date(), operator, OperatorLaborActionType.PutToStore);
        // Confirm operator labor reflects starting PTS.
        operatorLaborToTest = olManager.findOpenRecordByOperatorId(operatorId);
        assertNotNull(operatorLaborToTest);
        assertEquals(operatorLaborToTest.getActionType(), OperatorLaborActionType.PutToStore);
        
        //Simulate getting an assignment
        PtsLicense ptsLicense = getPtsLicenseManager().get(licenseId);
        this.laborManager.openPtsLicenseLaborRecord(new Date(), operator, ptsLicense);
        // Confirm PTS license labor reflects starting PTS.
        operatorLaborToTest = olManager.findOpenRecordByOperatorId(operatorId);
        assertNotNull(operatorLaborToTest);
        assertEquals(operatorLaborToTest.getActionType(), OperatorLaborActionType.PutToStore);
        PtsLicenseLabor ptsLicenseLaborToTest = llManager.findOpenRecordByLicenseId(licenseId);
        assertNotNull(ptsLicenseLaborToTest);
        
        // Simulate taking a break
        BreakType batteryChangeBreakType = getBreakTypeManager().findByNumber(-1);
        this.laborManager.openBreakLaborRecord(new Date(), operator, batteryChangeBreakType);
        // Confirm operator labor is break and PTS license labor is closed. 
        operatorLaborToTest = olManager.findOpenRecordByOperatorId(operatorId);
        assertNotNull(operatorLaborToTest);
        assertEquals(operatorLaborToTest.getActionType(), OperatorLaborActionType.Break);
        ptsLicenseLaborToTest = llManager.findOpenRecordByLicenseId(licenseId);
        assertNull(ptsLicenseLaborToTest);
        
        //Coming back from the break.
        this.laborManager.closeBreakLaborRecord(new Date(), operator);
        operatorLaborToTest = olManager.findOpenRecordByOperatorId(operatorId);
        // Confirm operator labor is back to PTS and a new PTS license labor is open.
        assertNotNull(operatorLaborToTest);
        assertEquals(operatorLaborToTest.getActionType(), OperatorLaborActionType.PutToStore);
        ptsLicenseLaborToTest = llManager.findOpenRecordByLicenseId(licenseId);
        assertNotNull(ptsLicenseLaborToTest);
        
        //Simulate passing the assignment
        this.laborManager.closePtsLicenseLaborRecord(new Date(), ptsLicense);
        operatorLaborToTest = olManager.findOpenRecordByOperatorId(operatorId);
        // Confirm operator labor is still in PTS but PTS license labor is clsoed.
        assertNotNull(operatorLaborToTest);
        assertEquals(operatorLaborToTest.getActionType(), OperatorLaborActionType.PutToStore);
        ptsLicenseLaborToTest = llManager.findOpenRecordByLicenseId(licenseId);
        assertNull(ptsLicenseLaborToTest);
        
        //Simulate signing off
        this.laborManager.openOperatorLaborRecord(new Date(), operator, OperatorLaborActionType.SignOff);
        operatorLaborToTest = olManager.findOpenRecordByOperatorId(operatorId);
        //Confirm operator labor indicates the sign off.
        assertNotNull(operatorLaborToTest);
        assertEquals(operatorLaborToTest.getActionType(), OperatorLaborActionType.SignOff);
        ptsLicenseLaborToTest = llManager.findOpenRecordByLicenseId(licenseId);
        assertNull(ptsLicenseLaborToTest);
        
        /***********************************************************************************
         * simulates (labor only) an operator signing on, getting a region configuration, 
         * getting an assignment, and then signing off.
        ************************************************************************************/
        // close any existing labor
        olManager.closeLaborRecord(new Date(), operator);
        
        //Confirm operator has no open labor
        assertNull(olManager.findOpenRecordByOperatorId(operatorId));
        //Simulate sign on
        this.laborManager.openOperatorLaborRecord(new Date(), operator, OperatorLaborActionType.SignOn);
        // Confirm labor reflects sign on.
        operatorLaborToTest = olManager.findOpenRecordByOperatorId(operatorId);
        assertNotNull(operatorLaborToTest);
        assertEquals(operatorLaborToTest.getActionType(), OperatorLaborActionType.SignOn);
        
        //Simulate getting a region configuration
        this.laborManager.openOperatorLaborRecord(new Date(), operator, OperatorLaborActionType.PutToStore);
        // Confirm operator labor reflects starting PTS.
        operatorLaborToTest = olManager.findOpenRecordByOperatorId(operatorId);
        assertNotNull(operatorLaborToTest);
        assertEquals(operatorLaborToTest.getActionType(), OperatorLaborActionType.PutToStore);
        
        //Sumulate getting an assignment
        ptsLicense = getPtsLicenseManager().get(licenseId);
        this.laborManager.openPtsLicenseLaborRecord(new Date(), operator, ptsLicense);
        // Confirm PTS license labor reflects starting PTS.
        operatorLaborToTest = olManager.findOpenRecordByOperatorId(operatorId);
        assertNotNull(operatorLaborToTest);
        assertEquals(operatorLaborToTest.getActionType(), OperatorLaborActionType.PutToStore);
        ptsLicenseLaborToTest = llManager.findOpenRecordByLicenseId(licenseId);
        assertNotNull(ptsLicenseLaborToTest);
        
        //Simulate signing off
        this.laborManager.openOperatorLaborRecord(new Date(), operator, OperatorLaborActionType.SignOff);
        operatorLaborToTest = olManager.findOpenRecordByOperatorId(operatorId);
        //Confirm operator labor indicates the sign off.
        assertNotNull(operatorLaborToTest);
        assertEquals(operatorLaborToTest.getActionType(), OperatorLaborActionType.SignOff);
        ptsLicenseLaborToTest = llManager.findOpenRecordByLicenseId(licenseId);
        assertNull(ptsLicenseLaborToTest);
        
        return;
    }
    
    /**
     * Test
     * This test modify end date from the Loading UI.
     * 
     * @throws Exception - database exceptions.
     */
    @Test()
    public void testLoadingLaborModifyEndTime() throws Exception {
        olManager = laborManager.getOperatorLaborManager();
        olRec = olManager.get(-601L);
        
        List<LoadingRouteLabor> llRecords = this.getLoadingLaborManager()
            .listAllRecordsByOperatorLaborId(olRec.getId());
        LoadingRouteLabor loadingLabor = llRecords.get(llRecords.size() - 1);
        
        Date earliestEndTime = laborManager.getEarliestEndTime(olRec.getId());
        assertEquals(earliestEndTime, loadingLabor.getEndTime());
        
        olRec = olManager.get(-602L);
        earliestEndTime = laborManager.getEarliestEndTime(olRec.getId());
        assertEquals(earliestEndTime, olRec.getStartTime());
    }

 }
