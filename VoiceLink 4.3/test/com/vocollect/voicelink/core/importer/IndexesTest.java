/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;



/**
 *   This tests both Indexes type and the underlying
 *   Index and GroupingIndex based on BaseIndex.
 *
 * @author jtauberg
 */
public class IndexesTest {
    private Long[][] myProperOrder = null; 
    private Indexes myIndexes = new Indexes();
    
    private FixedLengthFieldMap fields = new FixedLengthFieldMap();

    private FixedLengthField aField1 = new FixedLengthField();
    private FixedLengthField aField2 = new FixedLengthField();
    private FixedLengthField aField3 = new FixedLengthField();
    private FixedLengthField aField4 = new FixedLengthField();
    private FixedLengthField aField5 = new FixedLengthField();

    private ArrayList <Field> index1List = new ArrayList<Field>();
    private ArrayList <Field> index2List = new ArrayList<Field>();
    private ArrayList <Field> index3List = new ArrayList<Field>();
    private ArrayList <Field> index4List = new ArrayList<Field>();
        
    /**
     *  Simulates reading a record by loading fields with data for Glen Adams.
     */
    private void loadRecord1() {
        //Name
        aField1.setFieldData("Glen Adams");
        // isCool (boolean)
        aField2.setFieldData("1");
        //Age (Integer)
        aField3.setFieldData("39");
        //Weight (double)
        aField4.setFieldData("205.0");
        //FavoriteColor
        aField5.setFieldData("Green");
    }

    /**
     *  Simulates reading a record by loading fields with data for Amanda Jones.
     */
    private void loadRecord2() {
        //Name
        aField1.setFieldData("Amanda Jones");
        // isCool (boolean)
        aField2.setFieldData("1");
        //Age (Integer)
        aField3.setFieldData("24");
        //Weight (double)
        aField4.setFieldData("115.5");
        //FavoriteColor
        aField5.setFieldData("Blue");
    }

    /**
     *  Simulates reading a record by loading fields with data for Tom Kirk.
     */
    private void loadRecord3() {
        //Name
        aField1.setFieldData("Tom Kirk");
        // isCool (boolean)
        aField2.setFieldData("0");
        //Age (Integer)
        aField3.setFieldData("33");
        //Weight (double)
        aField4.setFieldData("190.0");
        //FavoriteColor
        aField5.setFieldData("Red");
    }


    /**
     * {@inheritDoc}
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass
    protected void setUp() throws Exception {
        // sets 5 fields.
        aField1.setDataType("java.lang.String");
        aField1.setFieldName("Name");
        
        aField2.setDataType("boolean");
        aField2.setFieldName("isCool");
        
        aField3.setDataType("java.lang.Integer");
        aField3.setFieldName("Age");
        
        aField4.setDataType("double");
        aField4.setFieldName("Weight");
        
        aField5.setDataType("java.lang.String");
        aField5.setFieldName("FavoriteColor");


        // creates a fieldmap with the 5 fields
        fields.addField(aField1);
        fields.addField(aField2);
        fields.addField(aField3);
        fields.addField(aField4);
        fields.addField(aField5);

        // also creates a field list to make 3 indexes...
        // #1 Name|, #2 Age|Weight|, #3 Name|FavoriteColor|
        index1List.add(fields.get("Name"));

        index2List.add(fields.get("Age"));
        index2List.add(fields.get("Weight"));

        index3List.add(fields.get("Name"));
        index3List.add(fields.get("FavoriteColor"));
        
        // create a field list for a grouping index #4 Name|Age
        index4List.add(fields.get("Name"));
        index4List.add(fields.get("Age"));
        
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Indexes#Indexes()}.
     */
    @Test()
    public void testIndexes() {
        Indexes theIndexes = null;
        assertNull(theIndexes, "An indexes was created and set to null, but isn't null.");
        theIndexes = new Indexes();
        org.testng.Assert
            .assertNotNull(
                theIndexes,
                "The indexes constructor ran but the indexes object is still null.");
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Indexes#setCharCount(java.lang.Long)}.
     * Test method for {@link com.vocollect.voicelink.core.importer.Indexes#getCharCount()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.Indexes#addIndex(com.vocollect.voicelink.core.importer.GroupingIndex)}.
     * Test method for {@link com.vocollect.voicelink.core.importer.Indexes#getGroupingIndex()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.Indexes#containsGroupingIndex()}.
     * 
     */
    @Test()
    public void testSetCharCount() {
        //make sure the myIndexes object is clear.
        myIndexes.clear();
        assertTrue(myIndexes.isEmpty(), "Just cleared myIndexes but it isn't empty!");
        
        //Since we haven't added a grouping Index, get should return null.
        assertNull(myIndexes.getGroupingIndex(), "With no grouping index set, getGroupingIndex did not retunr null.");
        //Test that containsGroupingIndex works right in this case
        assertFalse(
            myIndexes.containsGroupingIndex(),
            "With no grouping index set containsGrouping index was not false!");
        
        //charCount should not be able to be set if no grouping index.
        try {
        myIndexes.setCharCount(12345L);
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("setCharCount without adding a groupingIndex should cause a RuntimeException, but it didn't.");
        }
        //charCount should not be able to be get (got) if no grouping index.
        try {
        myIndexes.getCharCount();
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("getCharCount without adding a groupingIndex should cause a RuntimeException, but it didn't.");
        }
       
        //Create a grouping index.
        GroupingIndex myGroupingIndex = new GroupingIndex(index4List);
        // Now add the grouping index and re-test this...
        myIndexes.addIndex(myGroupingIndex);

        //Test that containsGroupingIndex works right in this case
        assertTrue(myIndexes.containsGroupingIndex(), "With a grouping index set containsGrouping index was false!");

        //Test getting the grouping index and see that it is not null
        assertNotNull(myIndexes.getGroupingIndex(), "Grouping Index was set up, but getGroupingIndex returned a null.");
        //Test that we can access index after get.
        assertEquals(myIndexes.getGroupingIndex().indexFieldsName(), "Name|Age|",
            "Grouping Index was set up but myIndexes.getGroupingIndex().index"
            + "FieldsName() did not return proper value.");
        
        //Test get (getting default)
        assertTrue(myIndexes.getCharCount() == 0L, "The default charCount returned"
            + " from getCharCount was not zero!");

        // Set a value 12345
        myIndexes.setCharCount(12345L);
        // Get that 12345 value
        assertTrue(myIndexes.getCharCount() == 12345L, 
            "We set a value for charCount and did not get back same value (12345L)!");
    }


    
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Indexes#addIndex(com.vocollect.voicelink.core.importer.Index)}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Indexes#updateIndexes()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Indexes#isRecordDataDuped()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Indexes#isRecordDataDupedIgnoreFlags()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Indexes#isFileDataDuped()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Indexes#isFileDataDupedIgnoreFlags()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Indexes#getRecordDataDuped()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Indexes#getRecordDataDupedIgnoreFlags()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Indexes#clear()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Indexes#isEmpty()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Indexes#containsGroupingIndex()}.

     */
    @Test()
    public void testAddIndex() {
        //make sure the myIndexes object is clear.
        myIndexes.clear();
        assertTrue(myIndexes.isEmpty(), "Just cleared myIndexes but it isn't empty!");

        //create 3 indexes and then add them to the indexes object
        Index myIndex = new Index(index1List);
        assertEquals(myIndex.indexFieldsName(), "Name|", "indexFieldsName didn't return updated value.");
        Index myIndex2 = new Index(index2List);
        assertEquals(myIndex2.indexFieldsName(), "Age|Weight|", "indexFieldsName didn't return updated value.");
        Index myIndex3 = new Index(index3List);
        assertEquals(myIndex3.indexFieldsName(), "Name|FavoriteColor|", "indexFieldsName didn't return updated value.");
        //Create a grouping index.
        GroupingIndex myGroupingIndex = new GroupingIndex(index4List);
        assertEquals(myGroupingIndex.indexFieldsName(), "Name|Age|",
            "indexFieldsName didn't return updated value for groupingIndex.");
        
        //Add all indexes...
        assertTrue(myIndexes.addIndex(myIndex), "Failed to add index1.");
        assertTrue(myIndexes.addIndex(myIndex2), "Failed to add index2.");
        assertTrue(myIndexes.addIndex(myIndex3), "Failed to add index3.");
        // Now add the grouping index and re-test this...
        assertTrue(myIndexes.addIndex(myGroupingIndex), "Failed to add"
            + " groupingIndex.");
        
        loadRecord1();
        //Going to simulate records at intervals of 100 characters for varying length (10,20,30).
        myIndexes.setCharCount(0L);
        myIndexes.setRecordLength(10L);
        //Check update count on groupingIndex
        assertTrue(myIndexes.getGroupingIndex().getUpdateCount() == 0L, "Didn't"
            + " index yet, but groupingIndex count isn't zero.");
        
        //Update all indexes...
        myIndexes.updateIndexes();

        //Test individual values
        assertTrue(myIndexes.get(0).getCount() == 1, "Data in index, count should be 1 but wasn't.");
        assertTrue(myIndexes.get(1).getCount() == 1, "Data in index2, count should be 1 but wasn't.");
        assertTrue(myIndexes.get(2).getCount() == 1, "Data in index3, count should be 1 but wasn't.");
        assertTrue(myIndexes.getGroupingIndex().getUpdateCount() == 1L, "Data in"
            + "groupingIndex, count should be 1 but wasn't.");

        //Get indexed values.
        assertEquals(myIndexes.get(0).currentFieldsIndexValue(), 
            "Glen Adams|", "currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndexes.get(1).currentFieldsIndexValue(), 
            "39|205.0|", "2.currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndexes.get(2).currentFieldsIndexValue(), 
            "Glen Adams|Green|", "3.currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndexes.getGroupingIndex().currentFieldsIndexValue(), 
            "Glen Adams|39|", "4.currentFieldsIndexValue returned wrong result!");
        
        //Test containsGroupingIndex() - returns true when answer is yes.
        assertTrue(myIndexes.containsGroupingIndex(), 
            "Indexes has grouping index, but containsGroupingIndex() doesn't think so.");
        
        
        loadRecord2();
        //Going to simulate records at intervals of 100 characters for varying length (10,20,30).
        myIndexes.setCharCount(100L);
        myIndexes.setRecordLength(20L);
        
        //Update all indexes...
        myIndexes.updateIndexes();

        //Test individual values
        assertTrue(myIndexes.get(0).getCount() == 1, "Data in index, count should be 1 but wasn't.");
        assertTrue(myIndexes.get(1).getCount() == 1, "Data in index2, count should be 1 but wasn't.");
        assertTrue(myIndexes.get(2).getCount() == 1, "Data in index3, count should be 1 but wasn't.");
        assertTrue(myIndexes.getGroupingIndex().getUpdateCount() == 2L, "Data in"
            + "groupingIndex, count should be 2 but wasn't.");

        assertEquals(myIndexes.get(0).currentFieldsIndexValue(), 
            "Amanda Jones|", "currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndexes.get(1).currentFieldsIndexValue(), 
            "24|115.5|", "2.currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndexes.get(2).currentFieldsIndexValue(), 
            "Amanda Jones|Blue|", "3.currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndexes.getGroupingIndex().currentFieldsIndexValue(), 
            "Amanda Jones|24|", "4.currentFieldsIndexValue returned wrong result!");

        loadRecord3();
        //Going to simulate records at intervals of 100 characters for varying length (10,20,30).
        myIndexes.setCharCount(200L);
        myIndexes.setRecordLength(30L);
       
        //Update all indexes...
        myIndexes.updateIndexes();

        //Test individual values
        assertTrue(myIndexes.get(0).getCount() == 1, "Data in index, count should be 1 but wasn't.");
        assertTrue(myIndexes.get(1).getCount() == 1, "Data in index2, count should be 1 but wasn't.");
        assertTrue(myIndexes.get(2).getCount() == 1, "Data in index3, count should be 1 but wasn't.");
        assertTrue(myIndexes.getGroupingIndex().getUpdateCount() == 3L, "Data in"
            + "groupingIndex, count should be 3 but wasn't.");

        assertEquals(myIndexes.get(0).currentFieldsIndexValue(), 
            "Tom Kirk|", "currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndexes.get(1).currentFieldsIndexValue(), 
            "33|190.0|", "2.currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndexes.get(2).currentFieldsIndexValue(), 
            "Tom Kirk|Red|", "3.currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndexes.getGroupingIndex().currentFieldsIndexValue(), 
            "Tom Kirk|33|", "4.currentFieldsIndexValue returned wrong result!");

        //test record dup tests (ignoreFlags)
        assertFalse(myIndexes.isRecordDataDupedIgnoreFlags(), 
            "Record data was not duped, but isRecordDataDupedIgnoreFlags thought it was!");
        assertTrue(myIndexes.getRecordDataDupedIgnoreFlags() == "", 
            "Record data was not duped, but getRecordDataDupedIgnoreFlags thought it was!");
        //test record dup tests (with flags)
        assertFalse(myIndexes.isRecordDataDuped(), "Record data was not duped, but isRecordDataDuped thought it was!");
        assertTrue(myIndexes.getRecordDataDuped() == "", 
            "Record data was not duped, but getRecordDataDuped thought it was!");

        //test file dup tests (ignoreFlags)
        assertFalse(myIndexes.isFileDataDupedIgnoreFlags(), 
            "File data was not duped, but isFileDataDupedIgnoreFlags thought it was!");
        //test file dup tests (with flags)
        assertFalse(myIndexes.isFileDataDuped(), "File data was not duped, but isFileDataDuped thought it was!");
        
        //Test to see that Grouping Index contains all data
        this.myProperOrder = null;
        assertTrue(this.myProperOrder == null, "myProperOrder was set to null, but isn't.");
        //Get properOrder from groupingIndex
        this.myProperOrder = myIndexes.getGroupingIndex().getProperOrder();
        assertFalse(this.myProperOrder == null, "myProperOrder shouldn't be null, but is.");

//Test code to spit out myProperOrder data        
        for (int recordIndex = 0; recordIndex < myProperOrder[0].length; recordIndex++) {
            System.out.println("myProperOrder [0][" + recordIndex + "] is:" + myProperOrder[0][recordIndex]);
            System.out.println("myProperOrder [1][" + recordIndex + "] is:" + myProperOrder[1][recordIndex]);
        }
        
        //Check all values in array to be grouped properly (natural order, but like-indexed items together)
        assertTrue(this.myProperOrder[0][0] == 0L, "myProperOrder [0][0] value was wrong.");
        assertTrue(this.myProperOrder[1][0] == 10L, "myProperOrder [1][0] value was wrong.");
        assertTrue(this.myProperOrder[0][1] == 100L, "myProperOrder [0][1] value was wrong.");
        assertTrue(this.myProperOrder[1][1] == 20L, "myProperOrder [1][1] value was wrong.");
        assertTrue(this.myProperOrder[0][2] == 200L, "myProperOrder [0][2] value was wrong.");
        assertTrue(this.myProperOrder[1][2] == 30L, "myProperOrder [1][2] value was wrong.");
        
        
        // LOADING DUPED DATA  !!!
        loadRecord2();
        //Going to simulate records at intervals of 100 characters for varying length (10,20,30).
        myIndexes.setCharCount(300L);
        myIndexes.setRecordLength(40L);
        
        //Update all 3 indexes...
        myIndexes.updateIndexes();

        //Test individual values
        assertTrue(myIndexes.get(0).getCount() == 2, "Data in index, count should be 2 but wasn't.");
        assertTrue(myIndexes.get(1).getCount() == 2, "Data in index2, count should be 2 but wasn't.");
        assertTrue(myIndexes.get(2).getCount() == 2, "Data in index3, count should be 2 but wasn't.");
        assertTrue(myIndexes.getGroupingIndex().getUpdateCount() == 4L, "Data in"
            + "groupingIndex, count should be 4 but wasn't.");

        assertEquals(myIndexes.get(0).currentFieldsIndexValue(), 
            "Amanda Jones|", "currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndexes.get(1).currentFieldsIndexValue(), 
            "24|115.5|", "2.currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndexes.get(2).currentFieldsIndexValue(), 
            "Amanda Jones|Blue|", "3.currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndexes.getGroupingIndex().currentFieldsIndexValue(), 
            "Amanda Jones|24|", "4.currentFieldsIndexValue returned wrong result!");

        //test Record dup tests (IgnoreFlags)
        assertTrue(myIndexes.isRecordDataDupedIgnoreFlags(), 
            "Record data was duped, but isRecordDataDuped thought it wasn't!");
        assertTrue(myIndexes.getRecordDataDupedIgnoreFlags() != "", 
            "Record data was duped, but getRecordDataDuped thought it wasn't!");
        System.out.println("The IGNORE FLAGS index duplications are:  ");
        System.out.println(myIndexes.getRecordDataDupedIgnoreFlags());

        //test Record dup tests (with flags)
        assertFalse(myIndexes.isRecordDataDuped(), "Record data was not duped, but isRecordDataDuped thought it was!");
        assertTrue(myIndexes.getRecordDataDuped() == "", 
            "Record data was not duped, but getRecordDataDuped thought it was!");

        //test file dup tests (ignoreFlags)
        assertTrue(myIndexes.isFileDataDupedIgnoreFlags(), 
            "File data IS duped, but isFileDataDupedIgnoreFlags thought it wasn't!");
        //test file dup tests (with flags)
        assertFalse(myIndexes.isFileDataDuped(), "File data was not duped, but isFileDataDuped thought it was!");


        
        //Test to see that Grouping Index contains all data
        this.myProperOrder = null;
        assertTrue(this.myProperOrder == null, "myProperOrder was set to null second time, but isn't.");
        //Get properOrder from groupingIndex
        this.myProperOrder = myIndexes.getGroupingIndex().getProperOrder();
        assertFalse(this.myProperOrder == null, "second time myProperOrder shouldn't be null, but is.");

//Test code to spit out myProperOrder data        
        for (int recordIndex = 0; recordIndex < myProperOrder[0].length; recordIndex++) {
            System.out.println("Dup data myProperOrder [0][" + recordIndex + "] is:" + myProperOrder[0][recordIndex]);
            System.out.println("Dup data myProperOrder [1][" + recordIndex + "] is:" + myProperOrder[1][recordIndex]);
        }
        //Check all values in array to be grouped properly (natural order, but like-indexed items together)
        assertTrue(this.myProperOrder[0][0] == 0L, "myProperOrder [0][0] value was wrong.");
        assertTrue(this.myProperOrder[1][0] == 10L, "myProperOrder [1][0] value was wrong.");
        assertTrue(this.myProperOrder[0][1] == 100L, "myProperOrder [0][1] value was wrong.");
        assertTrue(this.myProperOrder[1][1] == 20L, "myProperOrder [1][1] value was wrong.");
        assertTrue(this.myProperOrder[0][2] == 300L, "myProperOrder [0][2] value was wrong.");
        assertTrue(this.myProperOrder[1][2] == 40L, "myProperOrder [1][2] value was wrong.");
        assertTrue(this.myProperOrder[0][3] == 200L, "myProperOrder [0][2] value was wrong.");
        assertTrue(this.myProperOrder[1][3] == 30L, "myProperOrder [1][2] value was wrong.");

        
        assertFalse(myIndexes.isEmpty(), "myIndexes is NOT empty, but isEmpty thinks so.");
    
        myIndexes.clear();
        
        assertTrue(myIndexes.isEmpty(), "myIndexes is empty, but isEmpty didn't think so.");
    }

    

    /**
     * test the markup for the Indexes object.
     */
    @Test()
    public void testIndexesConfig() {
        Indexes i = null;
        CastorConfiguration cc = 
            new CastorConfiguration("import-definition.xml", "baseIndexes.xml");
        try {
            i = (Indexes) cc.configure();
        } catch (ConfigurationException e) {
            fail("configuration threw an exception.");
        }

        //Do some assertions to prove the grouping Index was created properly
        assertTrue(i.getGroupingIndex().getGrowBy() == 7, "Grouping Index growBy did not return proper value.");
        assertTrue(i.getGroupingIndex().getInitialSize() == 7, "Grouping Index "
            + "initialSize did not return proper value.");
        assertTrue(i.containsGroupingIndex(), "The indexes should have had a grouping index, but didn't.");
        assertEquals(i.getGroupingIndex().indexFieldsName(), "assignmentNumber|",
            "GroupingIndex didn't contain proper index value.");
    
        //Do some assertions to prove the first Index was created properly
        assertEquals(i.get(0).indexFieldsName(), "MyField|", "The first Index "
            + "(MyField) didn't have the proper index value.");
        assertTrue(i.get(0).isFailFile(), "The first Index (MyField) should have"
            + " been a failFile but wasn't.");
        assertFalse(i.get(0).isFailRecord(), "The first Index (MyField) should "
            + "not have been a failRecord but was.");
        
        //Do some assertions to prove the 2nd Index was created properly
        assertEquals(i.get(1).indexFieldsName(), "Operator|", "The 2nd "
            + " Index (Operator) didn't have the proper index value.");
        assertFalse(i.get(1).isFailFile(), "The 2nd Index (Operator) should not"
            + " have been a failFile but was.");
        assertTrue(i.get(1).isFailRecord(), "The 2nd Index (Operator) should "
            + "have been a failRecord but wasn't.");
    }


}
