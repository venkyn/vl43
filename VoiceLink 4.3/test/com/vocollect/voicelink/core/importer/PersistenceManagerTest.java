/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.model.User;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Operator;

import java.util.HashSet;

import org.testng.annotations.Test;

/**
 * @author astein
 */
public class PersistenceManagerTest extends BaseImportServiceManagerTestCase {

    private PersistenceManager        aPm = new PersistenceManager();

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        // Nothing to do
        
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.PersistenceManager()'.
     */
    @Test()
    public void testPersistenceManager() {

        PersistenceManager pm = new PersistenceManager();
        assertNotNull(pm);

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.clearMap()'.
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.getPersistenceManagerMap()'
     */
    @Test()
    public void testClearMap() {
        PersistenceManager pm = new PersistenceManager();

        // Get at least one entry in the map
        try {
            pm.getCount(User.class);
        } catch (DataAccessException e) {
            e.printStackTrace();
            assertFalse(true, "getCount for Item failed ");
        }

        assertFalse(pm.getPersistenceManagerMap().isEmpty(), "Map is already empty");
        pm.clearMap();
        assertTrue(pm.getPersistenceManagerMap().isEmpty(), "Map is not empty");

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.setPersistenceManagerMap(LinkedHashMap)'.
     */
    @Test()
    public void testSetPersistenceManagerMap() {
        PersistenceManager pm = new PersistenceManager();

        pm.setPersistenceManagerMap(aPm.getPersistenceManagerMap());
        assertEquals(pm.getPersistenceManagerMap(), aPm
                .getPersistenceManagerMap());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.get(Long,
     * Class)'.
     */
    @Test()
    public void testGet() {
        Long id = new Long(-1);
        try {
            aPm.get(id, User.class);
        } catch (DataAccessException e) {
            e.printStackTrace();
            assertFalse(true, "get for User, id = -1,  failed ");
        }

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.delete(Long,
     * Class)'.
     */
    @Test(dependsOnMethods = "testGet")
    @SuppressWarnings("unchecked")
    public void testDeleteLongClass() {
        try {
            User newGuy = new User();
            newGuy.setEmailAddress("test@test.com");
            newGuy.setEnabled(false);
            newGuy.setName("pmTestUser");
            newGuy.setPassword("testtest");
            newGuy.setAllSitesAccess(false);
            
            try {
                newGuy.setRoles(new HashSet<Role>(aPm.getAll(Role.class)));
            } catch (DataAccessException e) {
                e.printStackTrace();
                assertFalse(true, "get all roles failed ");
            }

            try {
                aPm.save(newGuy);
            } catch (Exception e) {
                e.printStackTrace();
                assertFalse(true, "save user failed ");
            }


            Long l = new Long(newGuy.getId());
            assertTrue(aPm.delete(l, User.class));
        } catch (DataAccessException e) {
            e.printStackTrace();
            assertFalse(true, "deleteLongClass for User failed ");
        } catch (BusinessRuleException ex) {
            ex.printStackTrace();
            assertFalse(true, "deleteLongClass for User failed ");
        }
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.delete(Object)'.
     */
    @SuppressWarnings("unchecked")
    @Test()
    public void testDeleteObject() {
        User newGuy = new User();
        newGuy.setEmailAddress("test@test.com");
        newGuy.setEnabled(false);
        newGuy.setName("pmTestUser");
        newGuy.setPassword("testtest");
        newGuy.setAllSitesAccess(false);

        try {
            newGuy.setRoles(new HashSet<Role>(aPm.getAll(Role.class)));
        } catch (DataAccessException e) {
            e.printStackTrace();
            assertFalse(true, "get all roles failed ");
        }

        try {
            aPm.save(newGuy);
            aPm.delete(newGuy);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(true, "save user failed ");
        }
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.get(Long,
     * Class)' with no manager for the object.
     */
    @Test()
    public void testGetNoManager() {
        Long id = new Long(-1);
        try {
            assertNull(
                aPm.get(id, TestInput.class), "Got an object with no manager!");
        } catch (DataAccessException e) {
            e.printStackTrace();
            assertFalse(true, "get for User, id = -1,  failed ");
        }

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.getAll(Class)'.
     */
    @Test()
    public void testGetAll() {
        try {
            aPm.getAll(User.class);
        } catch (DataAccessException e) {
            e.printStackTrace();
            assertFalse(true, "getAll for User failed ");
        }
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.getCount(Class)'.
     */
    @Test()
    public void testGetCount() {
        try {
            aPm.getCount(User.class);
        } catch (DataAccessException e) {
            e.printStackTrace();
            assertFalse(true, "getCount for Item failed ");
        }
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.getPrimaryDAO(Class)'.
     */
    @Test()
    public void testGetPrimaryDAO() {
        aPm.getPrimaryDAO(User.class);

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.queryByExample(Object)'.
     */
    @Test()
    public void testQueryByExample() {
        User example = new User();
        example.setEnabled(true);
        try {
            aPm.queryByExample(example);
        } catch (DataAccessException e) {
            e.printStackTrace();
            assertFalse(true, "queryByExample for User failed ");
        }

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.save(Object)'.
     */
    @SuppressWarnings("unchecked")
    @Test()
    public void testSave() {
        User newUser = new User();
        newUser.setEmailAddress("test@test.com");
        newUser.setEnabled(false);
        newUser.setName("test user");
        newUser.setPassword("testtest");
        try {
            newUser.setRoles(new HashSet<Role>(aPm.getAll(Role.class)));
        } catch (DataAccessException e) {
            e.printStackTrace();
            assertFalse(true, "get all roles failed ");
        }

        try {
            aPm.save(newUser);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(true, "save user failed ");
        }
        Operator newGuy = new Operator();
        newGuy.setOperatorIdentifier("just a test operator");
        newGuy.setPassword("testtest");

        try {
            aPm.save(newGuy);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(true, "save user failed ");
        }
}

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.PersistenceManager.getCurrentType()'.
     */
    @Test()
    public void testGetCurrentType() {

        try {
            aPm.getCount(User.class);
        } catch (DataAccessException e) {
            e.printStackTrace();
            assertFalse(true, "getCount user failed ");
        }
        assertEquals(aPm.getCurrentType(), "user", "current type incorrect");

    }

}
