/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;


/**
 * This tests the Range validator.
 * 
 * @author jtauberg
 */
public class RangeValidatorTest {

    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.RangeValidator#getMax()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.RangeValidator#setMax(java.lang.String)}.
     */
    @Test()
    public void testSetMax() {
        RangeValidator testObj = new RangeValidator();
        // set max to 9000
        testObj.setMax(9000.0);
        // as for max and make sure it is 9000
        assertEquals("Set a max value and did not get back same value.",
                9000.0, testObj.getMax());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.RangeValidator#setMin(java.lang.String)}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.RangeValidator#getMin()}.
     */
    @Test()
    public void testSetMin() {
        RangeValidator testObj = new RangeValidator();

        // Check that setMin works with valid input and that getMin returns the
        // value.
        testObj.setMin(-55.0);
        assertEquals("Set a min value and did not get back same value.", -55.0,
                testObj.getMin());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.RangeValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        RangeValidator testObj = new RangeValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("doubleData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // set min to 35.5 and max to 410 and number to validate at 36.36662.
        testObj.setMin(35.5);
        testObj.setMax(410.0);
        valObj.setDoubleData(36.36662);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "doubleData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Test low out of range... Set value to validate at 5.34 (min and max
        // same).
        valObj.setDoubleData(5.34);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("Low out of bounds data did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "doubleData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Test high out of range... Set value to validate at 55555.34 (min and
        // max same).
        valObj.setDoubleData(55555.34);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("High out of bounds data did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "doubleData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Test high boundary... Set value to validate at 410 = max (min and max
        // same).
        valObj.setDoubleData(410);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("High Boundary data bounds data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "doubleData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Test lower boundary... Set value to validate at 35.5 = min (min and
        // max same).
        valObj.setDoubleData(35.5);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("Lower boundary data bounds data triggered errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "doubleData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        //set validator to validate stringData field so that we can test null.
        testObj.setFieldName("stringData");
        // Test null... Set value to validate at null (min and
        // max same).
        valObj.setStringData(null);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on null data.");
        }
        assertFalse("null value for data triggered errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

    
    
    }
}
