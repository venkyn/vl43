/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.ListValidationTestObject;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.ValidationException;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.fail;


/**
 * Test to verify scenarios in which MainAndDependentDataListValidator can be used
 *
 */
public class MainAndDependentDataListValidatorTest {
    private  MainAndDependentDataListValidator validator = null;
    private ListValidatorTestData valObj = null;
    /**
     * @throws Exception
     */
    @BeforeTest()
    protected void setUp() throws Exception {
        validator = new MainAndDependentDataListValidator();
        valObj = new ListValidatorTestData();
        validator.setMainField("xxx");
        validator.setDependentField("yyy");
        validator.setDependentFieldValue("");
    }

    /**
     * Test to see if we can validate on the data in the ObjectContext. Pass:
     * 1). Single element in the O.C 2). Multiple elements
     * 
     * Fail: 1). Multiple elements with one different value 2). Multiple
     * elements with several different values.
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateUniformMainAndNoDependentData() {

        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(
            resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        for (int i = 0; i < 3; i++) {
            fields.setFieldMaps(setUpMaps(Boolean.TRUE, null));
        }
        vContext.setFields(fields);

        // Set up the validation to pass
        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse(validator.getValidatorContext().getFieldErrors()
            .containsKey("xxx"), "Valid data triggered errors.");

        // reset field errors for next test case.
        vContext.setFieldErrors(null);
    }
    
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateUniformMainAndWithDependentData() {

        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(
            resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        for (int i = 1; i < 5; i++) {
            fields.setFieldMaps(setUpMaps(Boolean.TRUE, Boolean.TRUE));
        }
        
        vContext.setFields(fields);

        // Set up the validation to pass
        try {
            validator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse(validator.getValidatorContext().getFieldErrors()
            .containsKey("xxx"), "Valid data triggered errors.");

        // reset field errors for next test case.
        vContext.setFieldErrors(null);
    }
    
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateNonUniformMainWithNoDependentData() {

        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(
            resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        for (int i = 1; i < 5; i++) {
            fields.setFieldMaps(setUpMaps(Boolean.TRUE, null));
        }
        
        vContext.setFields(fields);

        // Set up the validation to pass
        try {
            validator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse(validator.getValidatorContext().getFieldErrors()
            .containsKey("xxx"), "Valid data triggered errors.");

        // reset field errors for next test case.
        vContext.setFieldErrors(null);
    }
    
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateNonUniformMainWithDependentData() {

        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(
            resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        for (int i = 1; i < 5; i++) {
            fields.setFieldMaps(setUpMaps(Boolean.FALSE, Boolean.TRUE));
        }
        
        vContext.setFields(fields);

        // Set up the validation to pass
        try {
            validator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse(validator.getValidatorContext().getFieldErrors()
            .containsKey("xxx"), "Valid data triggered errors.");

        // reset field errors for next test case.
        vContext.setFieldErrors(null);
    }
    
    /**
     * utility method to create Field object
     * @param fieldName - the field to set
     * @param data - the data string
     * @return Field
     */
    private Field setUpElement(String fieldName, String data) {
        Field o = new Field();
        o.setFieldData(data);
        o.setFieldName(fieldName);
        return o;
    }

    /**
     * @param isMainUniform - the main field name
     * @param isDependentUniform - the dependent field name
     * @return FieldMap
     */
    @SuppressWarnings("unchecked")
    private FieldMap setUpMaps(
                               Boolean isMainUniform,
                               Boolean isDependentUniform) {
        FieldMap fm = new FieldMap();

        if (isMainUniform == null) {
            fm.addField(setUpElement(validator.getMainField(), null));
        } else if (isMainUniform) {
            fm.addField(setUpElement(validator.getMainField(), "same"));
        } else {
            fm.addField(setUpElement(validator.getMainField(), "same"
                + String.valueOf(Math.random())));
        }

        if (isDependentUniform == null) {
            fm.addField(setUpElement(validator.getDependentField(), null));
        } else if (isDependentUniform) {
            fm.addField(setUpElement(validator.getDependentField(), "same"));
        } else {
            fm.addField(setUpElement(validator.getDependentField(), "same"
                + String.valueOf(Math.random())));
        }

        return fm;
    }

    /**
     * Inner class.
     */
    private class ListValidatorTestData {

        private String stringData = null;

        private int intData = 0;

        private List<ListValidationTestObject> list = new ArrayList<ListValidationTestObject>();

        /**
         * @param o - the validation object
         */
        public void addElement(ListValidationTestObject o) {
            list.add(o);
        }

        /**
         * 
         */
        public void clearList() {
            list = new ArrayList<ListValidationTestObject>();
        }

        /**
         * @return List<ListValidationTestObject>
         */
        public List<ListValidationTestObject> getList() {
            return list;
        }

        /**
         * @param list - validation list
         */
        public void setList(List<ListValidationTestObject> list) {
            this.list = list;
        }

        /**
         * Gets the value of stringData.
         * @return the stringData
         */
        public String getStringData() {
            return stringData;
        }

        /**
         * Sets the value of the stringData.
         * @param stringData the stringData to set
         */
        public void setStringData(String stringData) {
            this.stringData = stringData;
        }

        /**
         * Gets the value of intData.
         * @return the intData
         */
        public int getIntData() {
            return intData;
        }

        /**
         * Sets the value of the intData.
         * @param intData the intData to set
         */
        public void setIntData(int intData) {
            this.intData = intData;
        }
    }
}
