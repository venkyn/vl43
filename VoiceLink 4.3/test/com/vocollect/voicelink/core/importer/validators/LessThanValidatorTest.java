/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;


/**
 * This tests the LessThanValidator.
 *
 * @author jtauberg
 */
public class LessThanValidatorTest {

    /**
     * {@inheritDoc}
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     * LessThanValidator#setMax(java.lang.String)}. Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.
     * LessThanValidator#getMax()}.
     */
    @Test()
    public void testSetMax() {
        LessThanValidator testObj = new LessThanValidator();
        // set max to 123
        testObj.setMax(123.);
        // get max and make sure it is 123.
        assertEquals("Set a max value and did not get back same value.", 123.,
                testObj.getMax());

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.LessThanValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        LessThanValidator testObj = new LessThanValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("intData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // set max to 123.
        testObj.setMax(123.);
        // set value to test at 5.
        valObj.setIntData(5);
        try {
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // set value to check at 500 (max still 123)
        valObj.setIntData(500);
        try {
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on out of range data.");
        }
        assertTrue("Above max data didn't trigger error.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // set value to check at 123 (max still 123)
        valObj.setIntData(123);
        try {
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on out of range data.");
        }
        assertTrue("At upper bounds data did not trigger error.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

    }
}
