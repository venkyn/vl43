/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.CheckDigitsValidationTestObject;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.SelectionRegion;
import com.vocollect.voicelink.selection.model.SelectionRegionProfile;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.ValidationException;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.fail;

/**
 * This tests the CheckDigitsValidator.
 *
 * @author vsubramani
 */
public class CheckDigitsValidatorTest {

    /**
     * {@inheritDoc}
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeTest()
    protected void setUp() throws Exception {
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators
     *          .ListValidator#validate(java.lang.Object)}.
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateObject() {
        List l = new ArrayList();
        CheckDigitsValidator testValidator = new CheckDigitsValidator();

        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vR = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vR.new VocollectValidatorContext(
                resultHolder);

        Field notNullField = new FixedLengthField();
        notNullField.setFieldName("list");
        FieldMap fields = new FixedLengthFieldMap();
        fields.addField(notNullField);
        ObjectContext oc = new ObjectContext(fields, new String(
                "No real object here"));
        vContext.setFields(oc);
        //vContext.setFields(fields);

        testValidator.setValidatorContext(vContext);

        CheckDigitsValidationTestObject valObj = new CheckDigitsValidationTestObject();
        valObj.setIntData(1);
        notNullField.setFieldData(String.valueOf(valObj.getIntData()));

        l.add(valObj);

        SelectionRegionProfile srp = new SelectionRegionProfile();
        srp.setRequireCaseLabelCheckDigits(true);

        SelectionRegion sr = new SelectionRegion();
        sr.setProfileNormalAssignment(srp);

        Assignment a = new Assignment();
        a.setRegion(sr);
        a.setPicks(l);
        a.setNumber(-1L);

        testValidator.setFieldName("assignment");
        testValidator.setTarget("intData");
        testValidator.setTargetFieldValue("1");

        try {
            testValidator.validate(a);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse("Valid data triggered errors.", vContext.getFieldErrors()
                .containsKey("assignment"));

        vContext.setFieldErrors(null);
        l.clear();
        valObj.setIntData(12);
        notNullField.setFieldData(String.valueOf(valObj.getIntData()));
        l.add(valObj);
        a.setPicks(l);
        testValidator.setTarget("intData");
        testValidator.setTargetFieldValue("1");

        testValidator.setFieldName("list");
        try {
            testValidator.validate(a);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse("Invalid data triggered errors.", vContext.getFieldErrors()
                .containsKey("assignment"));
    }
}
