/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * This class tests the AlphaOnlyValidator.
 *
 * @author jtauberg
 */
public class AlphaOnlyValidatorTest {

    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.
     * validators.AlphaOnlyValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        AlphaOnlyValidator testObj = new AlphaOnlyValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("stringData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // set to all alpha data.
        valObj
                .setStringData("ABCDEFGHIJKabcLMdefNghiOjklPmnoQpqrRstuSvwxTyzUVWXYZ");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to include non alphas.
        valObj
                .setStringData("ABCDEFGH*IJKabcLMdefNghiOjklPmnoQpqrRstuSvwxTyzUVWXYZ");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("String with non alpha data did not trigger errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to be empty string... should pass.
        valObj.setStringData("");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("empty string triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to include non alphas (at beginning).
        valObj
                .setStringData("123ABCDEFGHIJKabcLMdefNghiOjklPmnoQpqrRstuSvwxTyzUVWXYZ");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("String with non alpha data did not trigger errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to include non alphas (at end).
        valObj
                .setStringData("ABCDEFGHIJKabcLMdefNghiOjklPmnoQpqrRstuSvwxTyzUVWXYZ@");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("String with non alpha data did not trigger errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to include non alphas (in middle).
        valObj
                .setStringData("ABCDEFGH*IJKabcLMdefNghiOjklPm%!$%^&*()noQpqrRstuSvwxTyzUVWXYZ");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("String with non alpha data did not trigger errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

    }
}
