/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * This is a test class for the DateValidatorTest.
 * 
 * @author jtauberg
 */
public class DateValidatorTest {

    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.DateValidator#setDateFormat(java.lang.String)}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.DateValidator#getDateFormat()}.
     */
    @Test()
    public void testSetDateFormat() {
        boolean expectedException = false;
        DateValidator testObj = new DateValidator();

        // Check that setDateFormat works with valid input and that
        // getsetDateFormat returns the string.
        testObj.setDateFormat("yyMMdd");
        assertEquals(
                "Set a date format string and did not get back same string.",
                "yyMMdd", testObj.getDateFormat());

        // Pass setDateFormat a string that can't be turned into a simple date
        // format (abc).
        try {
            testObj.setDateFormat("abc");
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue(
                "Failed to throw expected exception setting abc to a SimpleDateFormat",
                expectedException);

        // Pass setDateFormat a string that can't be turned into a simple date
        // format ("").
        try {
            testObj.setDateFormat("");
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue(
                "Failed to throw expected exception setting empty string to a SimpleDateFormat",
                expectedException);

        // Pass setDateFormat a string that can't be turned into a simple date
        // format (null).
        try {
            testObj.setDateFormat("");
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue(
                "Failed to throw expected exception setting null to a SimpleDateFormat",
                expectedException);

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.DateValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        DateValidator testObj = new DateValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("stringData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // Set date format to yyyyMMdd and put a legit date 20041212 in to
        // validate.
        testObj.setDateFormat("yyyyMMdd");
        valObj.setStringData("20041212");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to legit date in wrong format.
        valObj.setStringData("12/12/2004");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data with wrong format.");
        }
        assertTrue("Valid date with invalid format did not trigger errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to correct format but impossible date data
        // (there is no December 45, 2004).
        valObj.setStringData("20041245");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on invalid date with proper format.");
        }
        assertTrue(
                "Invalid date December 45, 2004 with proper format did not trigger errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to null string... should not fail.
        valObj.setStringData(null);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on null date string.");
        }
        assertFalse("Null date string did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to empty string... should not fail.
        valObj.setStringData("");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on empty date string.");
        }
        assertFalse("Empty date string did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));
    }
}
