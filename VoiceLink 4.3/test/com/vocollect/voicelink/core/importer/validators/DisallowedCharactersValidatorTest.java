/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * This class tests the DisallowedCharactersValidator.
 * 
 * @author jtauberg
 */
public class DisallowedCharactersValidatorTest {

    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.
     * DisallowedCharactersValidator#setDisallowedCharacters(java.lang.String)}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.
     * DisallowedCharactersValidator#getDisallowedCharacters()}.
     */
    @Test()
    public void testSetDisallowedCharacters() {
        DisallowedCharactersValidator testObj = new DisallowedCharactersValidator();

        // Check that setDisallowedCharacters works with valid input and that
        // getDisallowedCharacters returns the string.
        testObj.setDisallowedCharacters("t*-6");
        assertEquals(
                "Set a disallowed character string and did not get back same string.",
                "t*-6", testObj.getDisallowedCharacters());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.
     * DisallowedCharactersValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        DisallowedCharactersValidator testObj = new DisallowedCharactersValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("stringData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // Check that setDisallowedCharacters works with valid input and that
        // getDisallowedCharacters returns the string.
        testObj.setDisallowedCharacters("t*-6");
        valObj
                .setStringData("ABCDEFGHIJKabcLMdefNghiOjklPmnoQpqrRsuSvwxTyzUVWXYZ"
                        + "123457890~!@#$%^&()_+|}{[]:');,.></?=");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to include bad characters (middle).
        valObj.setStringData("ABCDEF-GH1*2345");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data with disallowed characters in middle.");
        }
        assertTrue("String with disallowed characters did not trigger errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to be empty string... should pass.
        valObj.setStringData("");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid empty stringtocheck data.");
        }
        assertFalse("empty string triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set disallowed characters to be empty string.
        testObj.setDisallowedCharacters("");
        valObj
                .setStringData("ABCDEFGHIJKabcLMdefNghiOjklPmnoQpqrRstuSvwxTyzUVWXYZ"
                        + "1234567890*-~!@#$%^&()_+|}{[]:');,.></?=");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse(
                "Test had empty string of disallowed chars, but failed a string!",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set disallowed characters to be empty string and the string to check
        // as empty too.
        testObj.setDisallowedCharacters("");
        valObj.setStringData("");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse(
                "Test had empty string of disallowed chars, but failed a string, empty though it was!",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set disallowed characters to be null.
        testObj.setDisallowedCharacters(null);
        valObj
                .setStringData("ABCDEFGHIJKabcLMdefNghiOjklPmnoQpqrRstuSvwxTyzUVWXYZ"
                        + "1234567890*-~!@#$%^&()_+|}{[]:');,.></?=");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse(
                "Test was sent null for disallowed chars, but failed a string!",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set disallowed characters to be empty string and the string to check
        // as empty too.
        testObj.setDisallowedCharacters(null);
        valObj.setStringData(null);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse(
                "Test was sent null for disallowed chars, but failed a string, null though it was!",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));
    }

    /**
     * 
     *
     */
    @Test()
    public void testValidateMoreBadCharacters() {
        DisallowedCharactersValidator testObj = new DisallowedCharactersValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("stringData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // Check that setDisallowedCharacters works with valid input and that
        // getDisallowedCharacters returns the string.
        testObj.setDisallowedCharacters("t*-6");
        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to include bad characters (begin).
        valObj.setStringData("tABCDEFGH12345");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data with disallowed characters at beginning.");
        }
        assertTrue("String with disallowed characters did not trigger errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));
    }
    
    /**
     * 
     *
     */
    @Test()
    public void testValidateYetMoreBadCharacters() {
        DisallowedCharactersValidator testObj = new DisallowedCharactersValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("stringData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // Check that setDisallowedCharacters works with valid input and that
        // getDisallowedCharacters returns the string.
        testObj.setDisallowedCharacters("t*-6");
        // reset field errors for next test case.
        vContext.setFieldErrors(null);
    
        // Set value to validate to include bad characters (end).
        valObj.setStringData("ABCDEFGH123456");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data with disallowed characters at end.");
        }
        assertTrue("String with disallowed characters did not trigger errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));
    }

}
