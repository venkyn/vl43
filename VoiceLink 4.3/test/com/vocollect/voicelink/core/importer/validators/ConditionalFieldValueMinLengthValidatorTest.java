/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;


/**
 * @author jtauberg
 *
 */
public class ConditionalFieldValueMinLengthValidatorTest {

    /**
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass
    protected void setUp() throws Exception {
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     *  ConditionalFieldValueMinLengthValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidate() {
        //Create a new validator
        ConditionalFieldValueMinLengthValidator validator
        = new ConditionalFieldValueMinLengthValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        // Set up the validation to say that the datat in StringData must be
        // at least the size of the number in intData.
        validator.setFieldName("StringData");
        validator.setControllingField("intData");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);

        // Set up the validation to pass
        valObj.setIntData(10);
        valObj.setStringData("MyStringIsLongerThan10Chars");
        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse(validator.getValidatorContext().getFieldErrors().containsKey("StringData"),
                "Valid data triggered errors.");

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set up the validation to fail
        valObj.setIntData(40);
        valObj.setStringData("will fail");
        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            fail("Invalid data didn't throw exception.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertTrue(validator.getValidatorContext().getFieldErrors().containsKey("StringData"),
                "Valid data triggered errors.");
        
        // reset field errors for next test case.
        vContext.setFieldErrors(null);
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     *  ConditionalFieldValueMinLengthValidator#getControllingField()}.
     */
    @Test()
    public void testGetSetControllingField() {
        //Create a new validator
        ConditionalFieldValueMinLengthValidator validator
            = new ConditionalFieldValueMinLengthValidator();
        //Set a value
        validator.setControllingField("MyControllingField");
        //Read back same value
        assertEquals("MyControllingField", validator.getControllingField(),
            "Value set in ControllingField was not same value returned (get).");
    }
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     *  ConditionalFieldValueMinLengthValidator#getMinDataLength()}.
     */
    @Test()
    public void testGetMinDataLength() {
        //Create a new validator
        ConditionalFieldValueMinLengthValidator validator
            = new ConditionalFieldValueMinLengthValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        //Set a field to be controllingValue
        validator.setControllingField("StringData");
        //set the simulated minLength.
        valObj.setStringData("6");
        //Set the value of the thing to validate
        validator.setFieldName("StringData2");
        //set the value to check.
        valObj.setStringData2("A string with a valid length.");

        
        //Before validation, value will be null returning <null>.
        assertEquals(validator.getMinDataLength(), "<null>", 
            "Value returned from getMinDataLength before validation was not <null>.");
        
        //Do the validation
        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            fail("Exception thrown on valid data.");
        }
        //After validation, value will be 6.
        assertEquals(validator.getMinDataLength(), "6", 
            "Value returned from getMinDataLength after validation was not 6.");
    
    }


}
