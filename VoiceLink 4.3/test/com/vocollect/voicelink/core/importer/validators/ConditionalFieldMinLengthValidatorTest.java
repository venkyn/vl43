/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;


/**
 * @author jtauberg
 *
 */
public class ConditionalFieldMinLengthValidatorTest {

    /**
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass
    protected void setUp() throws Exception {
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators
     *  .ConditionalFieldMinLengthValidator#setControllingField(java.lang.String)}.
     * Test method for {@link com.vocollect.voicelink.core.importer.validators
     *  .ConditionalFieldMinLengthValidator#getControllingField()}.
     */
    @Test()
    public void testGetSetControllingField() {
        //Create a new validator
        ConditionalFieldMinLengthValidator validator
            = new ConditionalFieldMinLengthValidator();
        //Set a value
        validator.setControllingField("MyControllingField");
        //Read back same value
        assertEquals("MyControllingField", validator.getControllingField(),
            "Value set in ControllingField was not same value returned (get).");
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators
     *  .ConditionalFieldMinLengthValidator#getControllingFieldValue()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.validators
     *  .ConditionalFieldMinLengthValidator#setControllingFieldValue(java.lang.String)}.
     */
    @Test()
    public void testGetSetControllingFieldValue() {
        //Create a new validator
        ConditionalFieldMinLengthValidator validator
        = new ConditionalFieldMinLengthValidator();
        //Set a value
        validator.setControllingFieldValue("MyValue");
        //Read back same value
        assertEquals("MyValue", validator.getControllingFieldValue(),
            "Value set in ControllingFieldValue was not same value returned (get).");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators
     *  .ConditionalFieldMinLengthValidator#getMinDataLength()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.validators
     *  .ConditionalFieldMinLengthValidator#setMinDataLength(java.lang.String)}.
     */
    @Test()
    public void testGetSetMinDataLength() {
        //Create a new validator
        ConditionalFieldMinLengthValidator validator
        = new ConditionalFieldMinLengthValidator();
        //Set a value
        validator.setMinDataLength("10");
        //Read back same value
        assertEquals("10", validator.getMinDataLength(),
            "Value set in minDataLength was not same value returned (get).");
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators
     *  .ConditionalFieldMinLengthValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidate() {
        //Create a new validator
        ConditionalFieldMinLengthValidator validator
        = new ConditionalFieldMinLengthValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        // Set up the validation to say that if the value in intData == 1, then the 
        // value in StringData must be at least 10 characters.
        validator.setFieldName("StringData");
        validator.setControllingField("intData");
        validator.setControllingFieldValue("1");
        validator.setMinDataLength("10");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);

        // Set up the validation to pass
        valObj.setIntData(1);
        valObj.setStringData("MyStringIsLongerThan10Chars");
        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse(validator.getValidatorContext().getFieldErrors().containsKey("StringData"),
                "Valid data triggered errors.");

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set up the validation to fail
        valObj.setIntData(1);
        valObj.setStringData("will fail");
        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            fail("Invalid data didn't throw exception.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertTrue(validator.getValidatorContext().getFieldErrors().containsKey("StringData"),
                "Valid data triggered errors.");
        
        // reset field errors for next test case.
        vContext.setFieldErrors(null);
    }

}
