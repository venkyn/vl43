/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * This is a test for the MaxDataLenghtValidation.
 * 
 * @author jtauberg
 */
public class MaxDataLengthValidatorTest {

    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.
     * MaxDataLengthValidator#setMaxDataLength(java.lang.String)}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.
     * MaxDataLengthValidator#getMaxDataLength()}.
     */
    @Test()
    public void testSetMaxDataLength() {
        boolean expectedException = false;
        MaxDataLengthValidator testObj = new MaxDataLengthValidator();

        // Check that setMaxDataLength works with valid input and that
        // getMaxDataLength returns the value.
        testObj.setMaxDataLength("6");
        assertEquals(
                "Set a data length value and did not get back same value.",
                "6", testObj.getMaxDataLength());

        // Check that setDataLength throws exception with invalid input (char
        // begin).
        try {
            testObj.setMaxDataLength("a10");
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue("Failed to throw expected exception", expectedException);

        // Check that setDataLength throws exception with invalid input (char
        // end).
        try {
            testObj.setMaxDataLength("10a");
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue("Failed to throw expected exception", expectedException);

        // Check that setDataLength throws exception with invalid input (char
        // mid).
        try {
            testObj.setMaxDataLength("1d0");
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue("Failed to throw expected exception", expectedException);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.MaxDataLengthValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        MaxDataLengthValidator testObj = new MaxDataLengthValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("stringData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // set MaxDataLength to 7 and the string to validate at 6 characters.
        testObj.setMaxDataLength("7");
        valObj.setStringData("123456");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate at 3 chars.
        valObj.setStringData("12345678");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("Above upper bounds data did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate at boundary (max) of 7 characters.
        valObj.setStringData("1234567");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("Data at boundary max triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));
    }

    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.MaxDataLengthValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateDoubleObject() {
        MaxDataLengthValidator testObj = new MaxDataLengthValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("doubleData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // set MaxDataLength to 10 and the double to validate at 9 characters.
        testObj.setMaxDataLength("10");
        valObj.setDoubleData(9999999.9);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "doubleData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate at 11 chars.
        valObj.setDoubleData(123456789.1);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("Above upper bounds data did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "doubleData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate at boundary (max) of 10 characters.
        valObj.setDoubleData(12345.6789);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("Data at boundary max triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "doubleData"));
    }

    
    
    
    
    
    
}
