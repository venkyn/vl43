/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ListValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * @author dgold
 *
 */
public class ConditionalFieldValueListValidatorTest {

    /**
     * {@inheritDoc}
     * @throws java.lang.Exception
     */
    @BeforeClass()
    protected void setUp() throws Exception {

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.ConditionalFieldValueListValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidate() {
        ConditionalFieldValueListValidator validator = new ConditionalFieldValueListValidator();
        ListValidatorTestData valObj = new ListValidatorTestData();
        // Set up the validation to say that if the value in intData == 1, then the 
        // value in stringData must be present
        validator.setFieldName("list");
        validator.setControllingField("intData");
        validator.setControllingFieldValue("1");
        validator.setDependentField("stringData");
        validator.setTargetFieldValue("1");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);

        // Set up the validation to pass
        valObj.setIntData(1);
        
        valObj.addElement(setUpElement("1"));
        valObj.addElement(setUpElement("1"));

        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse(validator.getValidatorContext().getFieldErrors().containsKey(validator.getFieldName()),
                "Valid data triggered errors.");

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        valObj.addElement(setUpElement("2"));
        valObj.addElement(setUpElement("1"));

        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertTrue(validator.getValidatorContext().getFieldErrors().containsKey(validator.getFieldName()),
                "InValid data failed to trigger errors.");

        // reset field errors for next test case.
        vContext.setFieldErrors(null);
    }

    
    /**
     * @param data - data string
     * @return - ListValidationTestObject
     */
    private ListValidationTestObject setUpElement(String data) {
        ListValidationTestObject o = new ListValidationTestObject();
        o.setStringData(data);
        return o;
    }

    /** 
     * Class that holds a list, so we can do list validations.
     * @author dgold
     *
     */
    private class ListValidatorTestData {
        
        private String stringData = null;
        
        private int intData = 0;
        
        private List<ListValidationTestObject> list = new ArrayList<ListValidationTestObject>();
        
        /**
         * @param o - validation test object
         */
        public void addElement(ListValidationTestObject o) {
            list.add(o);
        }
        
        /**
         * 
         */
        public void clearList() {
            list = new ArrayList<ListValidationTestObject>();
        }
        
        /**
         * @return List<ListValidationTestObject>
         */
        public List<ListValidationTestObject> getList() {
            return list;
        }

        /**
         * @param list - validation test list
         */
        public void setList(List<ListValidationTestObject> list) {
            this.list = list;
        }

        /**
         * Gets the value of stringData.
         * @return the stringData
         */
        public String getStringData() {
            return stringData;
        }

        /**
         * Sets the value of the stringData.
         * @param stringData the stringData to set
         */
        public void setStringData(String stringData) {
            this.stringData = stringData;
        }

        /**
         * Gets the value of intData.
         * @return the intData
         */
        public int getIntData() {
            return intData;
        }

        /**
         * Sets the value of the intData.
         * @param intData the intData to set
         */
        public void setIntData(int intData) {
            this.intData = intData;
        }
    }


}
