/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * @author jtauberg
 */
public class DataLengthValidatorTest {

    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     * DataLengthValidator#setDataLength(java.lang.String)}. Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.
     * DataLengthValidator#getDataLength()}.
     */
    @Test()
    public void testSetDataLength() {
        boolean expectedException = false;
        DataLengthValidator testObj = new DataLengthValidator();

        // Check that setDataLength works with valid input ansd that
        // getDataLength returns the value.
        testObj.setDataLength("10");
        assertEquals(
                "Set a data length value and did not get back same value.",
                "10", testObj.getDataLength());

        // Check that setDataLength throws exception with invalid input (char
        // begin).
        try {
            testObj.setDataLength("a10");
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue("Failed to throw expected exception", expectedException);

        // Check that setDataLength throws exception with invalid input (char
        // end).
        try {
            testObj.setDataLength("10a");
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue("Failed to throw expected exception", expectedException);

        // Check that setDataLength throws exception with invalid input (char
        // mid).
        try {
            testObj.setDataLength("1d0");
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue("Failed to throw expected exception", expectedException);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.DataLengthValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        DataLengthValidator testObj = new DataLengthValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("stringData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // set DataLength to 8 and the string to validate at8 characters.
        testObj.setDataLength("8");
        valObj.setStringData("12345678");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate at 5 chars.
        valObj.setStringData("12345");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("Below lower bounds data did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate at 10 characters.
        valObj.setStringData("1234567890");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("Greater than limit data did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

    }

}
