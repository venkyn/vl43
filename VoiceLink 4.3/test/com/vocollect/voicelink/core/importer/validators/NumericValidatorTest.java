/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import java.util.HashMap;
import java.util.List;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * @author astein
 */
public class NumericValidatorTest {
    
    /**
     * 
     * @author astein
     *
     */
    private class NumericValidatorTestData {
        private String day     = null;

        private String fieldh = null;

        /**
         * @return the day
         */
        public String getDay() {
            return day;
        }

        /**
         * @param day
         *            the day to set
         */
        public void setDay(String day) {
            this.day = day;
        }

        /**
         * @return the field_h
         */
        public String getFieldh() {
            return fieldh;
        }

        /**
         * @param fieldh
         *            the field_h to set
         */
        public void setFieldh(String fieldh) {
            this.fieldh = fieldh;
        }
    }

    private ValidationAwareSupport resultHolder    = new ValidationAwareSupport();

    // This is the thing that is used by the DelegatingValidatorContext to keep
    // the result of the validation

    private ValidatorContext       vContext        = new DelegatingValidatorContext(
                                                           resultHolder);

    // This is the thing that is used to do the validation

    private String                 errorFieldValue = "";

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.NumericValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidate() {
        // create the test object with the necessary data
        NumericValidatorTestData testData = new NumericValidatorTestData();
        testData.setDay("1234A");
        testData.setFieldh(null);

        NumericValidator numValidator = new NumericValidator();

        try {
            numValidator.setValidatorContext(vContext);
            numValidator.setDefaultMessage("Found an error");
            String thefield = "Day";
            numValidator.setFieldName(thefield);
            numValidator.validate(testData);
            this.validateTest(thefield);
            assertEquals("An error should have been flagged for " + thefield
                    + " field.", numValidator.getDefaultMessage(),
                    this.errorFieldValue);
            vContext.setFieldErrors(null);
            testData.setDay("34");
            this.errorFieldValue = "";
            numValidator.validate(testData);
            this.validateTest(thefield);
            assertEquals("An error should NOT have been flagged for "
                    + thefield + " field.", "", this.errorFieldValue);
            vContext.setFieldErrors(null);
            testData.setFieldh("45.30");
            thefield = "Field_h";
            numValidator.setFieldName(thefield);
            numValidator.validate(testData);
            this.validateTest(thefield);
            assertEquals("An error should NOT have been flagged for "
                    + thefield + " field.", "", this.errorFieldValue);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * @param thefield is the field
     */
    @SuppressWarnings("unchecked")
    private void validateTest(String thefield) {
        HashMap hmErrors = (HashMap) vContext.getFieldErrors();
        if (!hmErrors.isEmpty()) {
            if (hmErrors.containsKey(thefield)) {
                List errorList = (List) hmErrors.get(thefield);
                this.errorFieldValue = errorList.get(0).toString();
            }
        }
    }

}
