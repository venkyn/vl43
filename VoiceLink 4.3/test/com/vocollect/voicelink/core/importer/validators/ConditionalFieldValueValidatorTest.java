/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * @author dgold
 *
 */
public class ConditionalFieldValueValidatorTest {

    /**
     * {@inheritDoc}
     * @throws java.lang.Exception
     */
    @BeforeClass()
    protected void setUp() throws Exception {

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     * ConditionalFieldValueValidator#getControllingField()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     * ConditionalFieldValueValidator#setControllingField(java.lang.String)}.
     */
    @Test()
    public void testGetControllingField() {
        ConditionalFieldValueValidator validator = new ConditionalFieldValueValidator();
        validator.setControllingField("test");
        assertEquals("test", validator.getControllingField(), "Munged get/set");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     * ConditionalFieldValueValidator#getControllingFieldValue()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     * ConditionalFieldValueValidator#setControllingFieldValue(java.lang.String)}.
     */
    @Test()
    public void testGetControllingFieldValue() {
        ConditionalFieldValueValidator validator = new ConditionalFieldValueValidator();
        validator.setControllingFieldValue("test");
        assertEquals("test", validator.getControllingFieldValue(), "Munged get/set");
    }

    /**
     * 
     */
    @Test()
    public void testGetTargetValue() {
        ConditionalFieldValueValidator validator = new ConditionalFieldValueValidator();
        validator.setTargetValue("test");
        assertEquals("test", validator.getTargetValue(), "Munged get/set");
        
    }
    
    /**
     * Test validation for the object.
     */
    @Test()
    public void testValidateObject() {
        ConditionalFieldValueValidator validator = new ConditionalFieldValueValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        // Set up the validation to say that if the value in intData == 1, then the 
        // value in longData must be 1
        validator.setFieldName("longData");
        validator.setControllingField("intData");
        validator.setControllingFieldValue("1");
        validator.setTargetValue("1");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);

        // Set up the validation to pass
        valObj.setIntData(1);
        valObj.setLongData(1);
        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse(validator.getValidatorContext().getFieldErrors().containsKey("longData"),
                "Valid data triggered errors.");

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set up the validation to fail
        valObj.setIntData(1);
        valObj.setLongData(2);
        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // Nothing to do
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertTrue(validator.getValidatorContext().getFieldErrors().containsKey("longData"),
                "Valid data triggered errors.");

        // reset field errors for next test case.
        vContext.setFieldErrors(null);
    }
}
