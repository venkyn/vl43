/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * Tests the Minimum Validator.
 *
 * @author jtauberg
 */
public class MinimumValidatorTest {

    /**
     * {@inheritDoc}
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     * MinimumValidator#setMin(java.lang.String)}.
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     * MinimumValidator#getMin()}.
     */
    @Test()
    public void testSetMin() {
        MinimumValidator testObj = new MinimumValidator();

        // Check that setMin works with valid input and that getMin returns the value.
        testObj.setMin(100.);
        assertEquals("Set a min value and did not get back same value.", 100.,
                testObj.getMin());

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     * MinimumValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        MinimumValidator testObj = new MinimumValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("intData");
        // This is the thing that is used by the DelegatingValidatorContext to keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation 
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // set min to 4100 and number to validate at 4200.
        testObj.setMin(4100.);
        valObj.setIntData(4200);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            //Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered.  Hoping none were.
        assertTrue("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().isEmpty());

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate at 500 (min still 4100).
        valObj.setIntData(100);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("Below lower bounds data did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate at 4100 = min (min still 4100).
        valObj.setIntData(4100);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("Lower bounds data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));
    }
}
