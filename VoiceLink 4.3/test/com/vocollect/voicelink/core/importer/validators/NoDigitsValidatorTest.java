/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * Test class for NoDigitsValidator class.
 * 
 * @author jtauberg
 */
public class NoDigitsValidatorTest {

    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.NoDigitsValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        NoDigitsValidator testObj = new NoDigitsValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("stringData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // set to all everything but digits.
        valObj
                .setStringData("ABCDEFGHIJKabcLMdefNghiOjklPmnoQpqrRstuSvwxTyzUVWXYZ"
                        + "~!@#$%^&*()_+`-={}:<>?[];',./|\"\\");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // set to include digits too.... (middle)
        valObj
                .setStringData("ABCDEFGHIJKab2341567890cLMdefNghiOjklPmnoQpqrRstuSvwxTyzUVWXYZ"
                        + "~!@#$%^&*()_+`-={}:<>?[];',./|\"\\");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("String with digits in middle did not trigger errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // set to include digits too.... (start)
        valObj
                .setStringData("1ABCDEFGHIJKabcLMdefNghiOjklPmnoQpqrRstuSvwxTyzUVWXYZ"
                        + "~!@#$%^&*()_+`-={}:<>?[];',./|\"\\");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("String with digits at start did not trigger errors.",
                testObj.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // set to include digits too.... (end)
        valObj
                .setStringData("ABCDEFGHIJKabcLMdefNghiOjklPmnoQpqrRstuSvwxTyzUVWXYZ"
                        + "~!@#$%^&*()_+`-={}:<>?[];',./|\"\\7");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("String with digits at end did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // set to be null... should pass.
        valObj.setStringData(null);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on null string.");
        }
        assertFalse("Null string triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // set to be empty string...
        valObj.setStringData("");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on empty string.");
        }
        assertFalse("Empty string triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));
    }
}
