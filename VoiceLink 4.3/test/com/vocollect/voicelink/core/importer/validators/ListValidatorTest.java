/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.ListValidationTestObject;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.ValidationException;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.fail;

/**
* This tests the ListValidator.
*
* @author vsubramani
*/
public class ListValidatorTest {
    /**
     * {@inheritDoc}
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

    /**
     * Inner class.
     */
    private class ListValidatorTestData {

        private List<ListValidationTestObject> list = new ArrayList<ListValidationTestObject>();
        
        /**
         * @return List<ListValidationTestObject>
         */
        public List<ListValidationTestObject> getList() {
            return list;
        }

        /**
         * @param list - the validation list
         */
        public void setList(List<ListValidationTestObject> list) {
            this.list = list;
        }
    }


    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators
     *          .ListValidator#validate(java.lang.Object)}.
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateObject() {
        List l = new ArrayList();
        ListValidatorTestData testData = new ListValidatorTestData();
        ListValidator testValidator = new ListValidator();

        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vR = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext
            = vR.new VocollectValidatorContext(resultHolder);

        Field notNullField = new FixedLengthField();
        notNullField.setFieldName("list");
        FieldMap fields = new FixedLengthFieldMap();
        fields.addField(notNullField);
        ObjectContext oc = 
            new ObjectContext(fields, new String("No real object here"));
        vContext.setFields(oc);

        testValidator.setValidatorContext(vContext);

        ListValidationTestObject valObj = new ListValidationTestObject();
        valObj.setIntData(4200);
        notNullField.setFieldData(String.valueOf(valObj.getIntData()));

        l.add(valObj);
        testData.setList(l);
        testValidator.setFieldName("list");
        try {
            testValidator.validate(testData);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse("Valid data triggered errors.",
            vContext.getFieldErrors().containsKey("list"));

        vContext.setFieldErrors(null);
        valObj.setIntData(0);
        notNullField.setFieldData(String.valueOf(valObj.getIntData()));
        l.add(valObj);
        testData.setList(l);
        testValidator.setFieldName("list");
        try {
            testValidator.validate(testData);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse("Invalid data triggered errors.",
            vContext.getFieldErrors().containsKey("list"));
    }
}
