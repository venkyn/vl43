/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;



import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.ValidationException;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.ListValidationTestObject;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.List;
/**
 * @author mraj
 * 
 */
public class UniqueValueInDataListTest {

    private List<ListValidationTestObject> list = null;

    private ListValidatorTestData testData = null;

    private UniqueValueInDataList testValidator = null;

    private ValidationAwareSupport resultHolder = null;

    private ValidationResult vR = null;

    private ValidationResult.VocollectValidatorContext vContext = null;


    /**
     * @throws Exception
     */
    @BeforeMethod()
    protected void setUp() throws Exception {
        this.list = new ArrayList<ListValidationTestObject>();
        this.testData = new ListValidatorTestData();
        this.testValidator = new UniqueValueInDataList();

        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        this.resultHolder = new ValidationAwareSupport();
        this.vR = new ValidationResult();

        // This is the thing that is used to do the validation
        this.vContext = vR.new VocollectValidatorContext(resultHolder);

    }
    
    /**
     * 
     * @throws Exception
     */
    @AfterMethod()
    protected void tearDown() throws Exception {
        this.vContext.setFieldErrors(null);
        this.vContext = null;
        this.list.clear();
        this.testValidator = null;
        this.testData = null;
        this.vR = null;
        this.resultHolder = null;
    }
    
    

    /**
     * Test an invalid scenario for the SameKeySameValueListValidator.
     * 
     * This tests a scenario where we have 2 Keys but one has varying data.
     * 
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateMultipleSameObjects() {

        Field notNullField = new FixedLengthField();
        notNullField.setFieldName("list");
        FieldMap fields = new FixedLengthFieldMap();
        fields.addField(notNullField);
        ObjectContext oc = new ObjectContext(fields, new String(
            "No real object here"));
        vContext.setFields(oc);

        testValidator.setValidatorContext(vContext);
        // Create list of same objects
        for (int i = 0; i < 3; i++) {
            ListValidationTestObject valObj = new ListValidationTestObject();
            valObj.setStringData("same");
            list.add(valObj);
        }

        testData.setList(list);
        testValidator.setFieldName("list");
        testValidator.setTargetField("stringData");
        testValidator.setTargetFieldGetter("stringData");

        try {
            testValidator.validate(testData);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }

        // Since we had invalid data...
        // Make sure there was a validation error
        assertTrue(
            vContext.getFieldErrors().containsKey("stringData"),
            "Invalid data did not trigger validation error.");
    }

    /**
     * Test an invalid scenario for the SameKeySameValueListValidator.
     * 
     * This tests a scenario where we have 2 Keys but one has varying data.
     * 
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateUniqueObjects() {
//        setUp();
        Field notNullField = new FixedLengthField();
        notNullField.setFieldName("list");
        FieldMap fields = new FixedLengthFieldMap();
        fields.addField(notNullField);
        ObjectContext oc = new ObjectContext(fields, new String(
            "No real object here"));
        vContext.setFields(oc);

        testValidator.setValidatorContext(vContext);
        
        // Create list of same objects
        for (int i = 0; i < 3; i++) {
            ListValidationTestObject valObj = new ListValidationTestObject();
            valObj.setStringData("same" + i);
            list.add(valObj);
        }

        // Now put our list into the testData object.
        testData.setList(list);
        // Set the values needed by the validator.
        testValidator.setFieldName("list");
        testValidator.setTargetField("stringData");
        testValidator.setTargetFieldGetter("stringData");

        try {
            testValidator.validate(testData);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }

        // Since we had invalid data...
        // Make sure there was a validation error
        assertTrue(
            !vContext.getFieldErrors().containsKey("stringData"),
            "Invalid data did not trigger validation error.");
    }

    /**
     * Test an invalid scenario for the SameKeySameValueListValidator.
     * 
     * This tests a scenario where we have 2 Keys but one has varying data.
     * 
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateSingleObject() {

        Field notNullField = new FixedLengthField();
        notNullField.setFieldName("list");
        FieldMap fields = new FixedLengthFieldMap();
        fields.addField(notNullField);
        ObjectContext oc = new ObjectContext(fields, new String(
            "No real object here"));
        vContext.setFields(oc);

        testValidator.setValidatorContext(vContext);
        
        // Create list of same objects
        ListValidationTestObject valObj = new ListValidationTestObject();
        valObj.setStringData("single");
        list.add(valObj);

        // Now put our list into the testData object.
        testData.setList(list);
        // Set the values needed by the validator.
        testValidator.setFieldName("list");
        testValidator.setTargetField("stringData");
        testValidator.setTargetFieldGetter("stringData");

        try {
            testValidator.validate(testData);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }

        // Since we had invalid data...
        // Make sure there was a validation error with the TargetField
        // (longData)
        assertTrue(
            !vContext.getFieldErrors().containsKey("stringData"),
            "Invalid data did not trigger validation error.");
    }


    /**
     * Inner class.
     */
    private class ListValidatorTestData {

        private List<ListValidationTestObject> list = new ArrayList<ListValidationTestObject>();

        /**
         * @return List<ListValidationTestObject>
         */
        public List<ListValidationTestObject> getList() {
            return list;
        }

        /**
         * @param list - the validation list
         */
        public void setList(List<ListValidationTestObject> list) {
            this.list = list;
        }
    }
}
