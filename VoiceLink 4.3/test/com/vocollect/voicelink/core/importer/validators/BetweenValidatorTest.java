/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * This tests the Between Validator.
 *
 * @author jtauberg
 */
public class BetweenValidatorTest {

    /**
     * {@inheritDoc}
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }


    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.BetweenValidator#setMin(java.lang.String)}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.BetweenValidator#getMin()}.
     */
    @Test()
    public void testSetMin() {
        BetweenValidator testObj = new BetweenValidator();
        // set min to 90
        testObj.setMin(90.);
        // ask for min and make sure it is 90
        assertEquals("Set a min value and did not get back same value.", 90.,
                testObj.getMin());

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.BetweenValidator#setMax(java.lang.String)}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.BetweenValidator#getMax()}.
     */
    @Test()
    public void testSetMax() {
        BetweenValidator testObj = new BetweenValidator();
        // set max to 159
        testObj.setMax(159.);
        // ask for max and make sure it is 159
        assertEquals("Set a max value and did not get back same value.", 159.,
                testObj.getMax());

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.BetweenValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        BetweenValidator testObj = new BetweenValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("intData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // set min to -31 and max to -29 and number to validate at -30.
        testObj.setMin(-31.);
        testObj.setMax(-29.);
        valObj.setIntData(-30);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Out of range above... Set value to validate at 12 (min and max still
        // same).
        valObj.setIntData(12);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("High Out of bounds data did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Out of range below... Set value to validate at -212 (min and max
        // still same).
        valObj.setIntData(-212);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("Low Out of bounds data did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // on upper boundary... Set value to validate at -29 = max.
        valObj.setIntData(-29);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("Valid data = max did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // on lower boundary... Set value to validate at -31 = min.
        valObj.setIntData(-31);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("Valid data = min did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

    }
}
