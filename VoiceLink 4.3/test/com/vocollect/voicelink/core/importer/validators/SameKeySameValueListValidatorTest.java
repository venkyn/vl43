/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.ListValidationTestObject;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.ValidationException;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;


/**
 * Test for SameKeySameValueListValidator.
 *
 * @author jtauberg
 */
public class SameKeySameValueListValidatorTest {


    /**
     * Inner class.
     */
    private class ListValidatorTestData {

        private List<ListValidationTestObject> list = new ArrayList<ListValidationTestObject>();

        /**
         * @return List<ListValidationTestObject>
         */
        public List<ListValidationTestObject> getList() {
            return list;
        }

        /**
         * @param list - the validation list
         */
        public void setList(List<ListValidationTestObject> list) {
            this.list = list;
        }
    }



    /**
     * Test a valid scenario for the SameKeySameValueListValidator.
     *
     *  This tests a scenario where we have 2 Keys and good data.
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateGoodObject() {
        List l = new ArrayList();
        ListValidatorTestData testData = new ListValidatorTestData();
        SameKeySameValueListValidator testValidator = new SameKeySameValueListValidator();

        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vR = new ValidationResult();
        // This is the thing that is used to do the validation
        ValidationResult.VocollectValidatorContext vContext
            = vR.new VocollectValidatorContext(resultHolder);

        Field notNullField = new FixedLengthField();
        notNullField.setFieldName("list");
        FieldMap fields = new FixedLengthFieldMap();
        fields.addField(notNullField);
        ObjectContext oc =
            new ObjectContext(fields, new String("No real object here"));
        vContext.setFields(oc);

        testValidator.setValidatorContext(vContext);

        //Set up a scenario where we have 2 Keys and good data

        //Create the first object to be validated in the list
        ListValidationTestObject valObj1 = new ListValidationTestObject();
        //We will use this as the key
        valObj1.setIntData(4200);
        //We will use this as the value.
        valObj1.setLongData(55L);
        notNullField.setFieldData(String.valueOf(valObj1.getIntData()));
        //Add this object to be validated into the list.
        l.add(valObj1);

        //Create the second object to be validated in the list
        ListValidationTestObject valObj2 = new ListValidationTestObject();
        //We will use this as the key
        valObj2.setIntData(4200);
        //We will use this as the value.
        valObj2.setLongData(55L);
        //Add this object to be validated into the list.
        l.add(valObj2);

        //Create the third object to be validated in the list
        ListValidationTestObject valObj3 = new ListValidationTestObject();
        //We will use this as the key
        valObj3.setIntData(12);
        //We will use this as the value.
        valObj3.setLongData(7L);
        //Add this object to be validated into the list.
        l.add(valObj3);

        //Create the fourth object to be validated in the list
        ListValidationTestObject valObj4 = new ListValidationTestObject();
        //We will use this as the key
        valObj4.setIntData(12);
        //We will use this as the value.
        valObj4.setLongData(7L);
        //Add this object to be validated into the list.
        l.add(valObj4);


        //Now put our list into the testData object.
        testData.setList(l);
        //Set the values needed by the validator.
        testValidator.setFieldName("list");
        testValidator.setKeyField("intData");
        testValidator.setKeyFieldGetter("intData");
        testValidator.setTargetField("longData");
        testValidator.setTargetFieldGetter("longData");

        try {
            testValidator.validate(testData);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        //Since we had valid data...
        //Make sure there was no validation error with the TargetField (longData)
        assertFalse(vContext.getFieldErrors().containsKey("longData"),
            "Valid data triggered errors.");
    }



    /**
     * Test an invalid scenario for the SameKeySameValueListValidator.
     *
     *  This tests a scenario where we have 2 Keys but one has varying data.
     *
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateBadObject() {
        List l = new ArrayList();
        ListValidatorTestData testData = new ListValidatorTestData();
        SameKeySameValueListValidator testValidator = new SameKeySameValueListValidator();

        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vR = new ValidationResult();
        // This is the thing that is used to do the validation
        ValidationResult.VocollectValidatorContext vContext
            = vR.new VocollectValidatorContext(resultHolder);

        Field notNullField = new FixedLengthField();
        notNullField.setFieldName("list");
        FieldMap fields = new FixedLengthFieldMap();
        fields.addField(notNullField);
        ObjectContext oc =
            new ObjectContext(fields, new String("No real object here"));
        vContext.setFields(oc);

        testValidator.setValidatorContext(vContext);

        //Set up a scenario where we have 2 Keys but one has varying data.

        //Create the first object to be validated in the list
        ListValidationTestObject valObj1 = new ListValidationTestObject();
        //We will use this as the key
        valObj1.setIntData(4200);
        //We will use this as the value.
        valObj1.setLongData(55L);
        notNullField.setFieldData(String.valueOf(valObj1.getIntData()));
        //Add this object to be validated into the list.
        l.add(valObj1);

        //Create the second object to be validated in the list
        ListValidationTestObject valObj2 = new ListValidationTestObject();
        //We will use this as the key
        valObj2.setIntData(4200);
        //We will use this as the value.
        valObj2.setLongData(55L);
        //Add this object to be validated into the list.
        l.add(valObj2);

        //Data with second key below:

        //Create the third object to be validated in the list
        ListValidationTestObject valObj3 = new ListValidationTestObject();
        //We will use this as the key
        valObj3.setIntData(12);
        //We will use this as the value.
        valObj3.setLongData(7L);
        //Add this object to be validated into the list.
        l.add(valObj3);

        //Create the fourth object to be validated in the list
        ListValidationTestObject valObj4 = new ListValidationTestObject();
        //We will use this as the key
        valObj4.setIntData(12);
        //We will use this as the value.  NOTE IT IS DIFFERENT THAN 7.
        valObj4.setLongData(9L);
        //Add this object to be validated into the list.
        l.add(valObj4);


        //Now put our list into the testData object.
        testData.setList(l);
        //Set the values needed by the validator.
        testValidator.setFieldName("list");
        testValidator.setKeyField("intData");
        testValidator.setKeyFieldGetter("intData");
        testValidator.setTargetField("longData");
        testValidator.setTargetFieldGetter("longData");

        try {
            testValidator.validate(testData);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        //Since we had invalid data...
        //Make sure there was a validation error with the TargetField (longData)
        assertTrue(vContext.getFieldErrors().containsKey("longData"),
            "Invalid data did not trigger validation error.");
    }








}
