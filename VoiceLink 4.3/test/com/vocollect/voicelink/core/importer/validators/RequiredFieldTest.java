/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.ValidationTestObject;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * @author dgold
 *
 */
public class RequiredFieldTest {

    /**
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.RequiredField#validate(java.lang.Object)}.
     */
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.
     * validators.AlphaOnlyValidator#validate(java.lang.Object)}.
     */

    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateObject() {
        RequiredFieldValidator testValidator = new RequiredFieldValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testValidator.setFieldName("stringData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        
        ValidationResult vR = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vR.new VocollectValidatorContext(resultHolder);
        
        Field notNullField = new FixedLengthField();
        notNullField.setFieldName("stringData");
        
        FieldMap fields = new FixedLengthFieldMap();
        
        fields.addField(notNullField);
        
        ObjectContext oc = new ObjectContext(fields, new String("No real object here"));
        vContext.setFields(oc);
        
        testValidator.setValidatorContext(vContext);
        
        // set to all alpha data.
        try {
            // try to validate it
            testValidator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertTrue("null data failed to trigger errors.", testValidator
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to include non alphas.
        valObj
                .setStringData("");
        try {
            // try to validate it
            testValidator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("String with non alpha data did not trigger errors.",
                testValidator.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to be empty string... should pass.
        valObj.setStringData("        ");
        try {
            // try to validate it
            testValidator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("empty string failed to trigger errors.", testValidator
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to include non alphas (at beginning).
        valObj
                .setStringData("1");
        notNullField.setFieldData("1");
        try {
            // try to validate it
            testValidator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("String with 1 char triggered errors.",
                testValidator.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to include non alphas (at end).
        valObj
                .setStringData("ABCDEFGHIJKabcLMdefNghiOjklPmnoQpqrRstuSvwxTyzUVWXYZ@");
        notNullField.setFieldData(valObj.getStringData());
        try {
            // try to validate it
            testValidator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("String with many characters triggered errors.",
                testValidator.getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));
        
        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate to include non alphas (at end).
        valObj.setStringData("ABCD");
        notNullField.setFieldData(valObj.getStringData());
        ValidatorContext context = new DelegatingValidatorContext(resultHolder);
        context.setFieldErrors(null);
        testValidator.setValidatorContext(context);
        try {
            testValidator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse("String ABCD - valid data triggered errors.",
            context.getFieldErrors().containsKey("stringData"));
        
        // reset field errors for next test case.
        context.setFieldErrors(null);
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.RequiredField#validate(java.lang.Object)}.
     * This is used to test with the targetFieldName setup
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateObject1() {
        RequiredFieldValidator testValidator = new RequiredFieldValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testValidator.setFieldName("stringData1");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        
        ValidationResult vR = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vR.new VocollectValidatorContext(resultHolder);
        
        Field notNullField = new FixedLengthField();
        notNullField.setFieldName("stringData");
        
        FieldMap fields = new FixedLengthFieldMap();
        
        fields.addField(notNullField);

        ObjectContext oc = new ObjectContext(fields, new String("No real object here"));
        vContext.setFields(oc);
        
        testValidator.setValidatorContext(vContext);
        
        // Set value to validate to include non alphas.
        valObj.setStringData("");
        
        // set to all alpha data.
        try {
            // try to validate it
            testValidator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. 
        // there won't be any as the field mismatch
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse("Field mismatch still null data triggers error.", testValidator
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);
        
        testValidator.setFieldName("stringData");
        
        // set to all alpha data.
        try {
            // try to validate it
            testValidator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. 
        // there won't be any as the field mismatch
        // Check to see if any errors were triggered. Hoping none were.
        assertTrue("null data failed to trigger errors.", testValidator
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));
        // reset field errors for next test case.
        vContext.setFieldErrors(null); 

    }    
}
