/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.validators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 *  This is a test of the MinDataLengthValidator.
 *
 * @author jtauberg
 */
public class MinDataLengthValidatorTest {

    /**
     * {@inheritDoc}
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
     }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     * MinDataLengthValidator#setMinDataLength(java.lang.String)}. Test method
     * for {@link com.vocollect.voicelink.core.importer.validators.
     * MinDataLengthValidator#getMinDataLength()}.
     */
    @Test()
    public void testSetMinDataLength() {
        boolean expectedException = false;
        MinDataLengthValidator testObj = new MinDataLengthValidator();

        // Check that setMinDataLength works with valid input ansd that
        // getMinDataLength returns the value.
        testObj.setMinDataLength("6");
        assertEquals(
                "Set a data length value and did not get back same value.",
                "6", testObj.getMinDataLength());

        // Check that setDataLength throws exception with invalid input (char
        // begin).
        try {
            testObj.setMinDataLength("a10");
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue("Failed to throw expected exception", expectedException);

        // Check that setDataLength throws exception with invalid input (char
        // end).
        try {
            testObj.setMinDataLength("10a");
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue("Failed to throw expected exception", expectedException);

        // Check that setDataLength throws exception with invalid input (char
        // mid).
        try {
            testObj.setMinDataLength("1d0");
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue("Failed to throw expected exception", expectedException);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.MinDataLengthValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        MinDataLengthValidator testObj = new MinDataLengthValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("stringData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // set MinDataLength to 4 and the string to validate at 6 characters.
        testObj.setMinDataLength("4");
        valObj.setStringData("123456");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate at 3 chars.
        valObj.setStringData("123");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("Below lower bounds data did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate at boundary min of 4 characters.
        valObj.setStringData("1234");
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("Data at boundary min triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));
    }
    
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.validators.MaxDataLengthValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateDoubleObject() {
        MinDataLengthValidator testObj = new MinDataLengthValidator();
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("doubleData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // set MinDataLength to 10 and the double to validate at 11 characters.
        testObj.setMinDataLength("10");
        valObj.setDoubleData(123456789.1);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it didn't validate
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "doubleData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate at 9 chars.
        valObj.setDoubleData(1234567.8);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertTrue("Above upper bounds data did not trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "doubleData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Set value to validate at boundary (max) of 10 characters.
        valObj.setDoubleData(9876.54321);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("Data at boundary max triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "doubleData"));
    }

}
