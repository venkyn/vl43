/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;

import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.mapping.MappingException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.fail;



/**
 * @author dgold
 */
public class CastorConfigurationTest {

    private String importDefinition = "import-definition.xml";

    private String importSetup      = "import-setup.xml";

    private String importerSetup    = "test-importer-setup.xml";

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeClass()
    public void setUp() throws Exception {
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#CastorConfiguration()}.
     */
    @Test()
    public void testCastorConfiguration() {
        CastorConfiguration cc = new CastorConfiguration();
        assertNotNull(cc, "Null from c'tor");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration
     * #CastorConfiguration(java.lang.String, java.lang.String)}.
     */
    @Test()
    public void testCastorConfigurationStringString() {
        CastorConfiguration cc = new CastorConfiguration(importDefinition,
                importSetup);
        assertNotNull(cc, "CC constructor with Strings should not be null");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#loadMapping()}.
     */
    @Test()
    public void testLoadMapping() {
        CastorConfiguration cc = new CastorConfiguration(importDefinition,
                importerSetup);
        try {
            cc.loadMapping();
        } catch (ConfigurationException e) {
            assertFalse(true, "Configuration exception" + e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#loadMapping(java.lang.String)}.
     */
    @Test()
    public void testLoadMappingString() {
        CastorConfiguration cc = new CastorConfiguration();
        try {
            cc.loadMapping(importDefinition);
        } catch (ConfigurationException e) {
            assertFalse(true, "Configuration exception" + e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#configure(java.lang.Class)}.
     */
    @Test()
    public void testConfigureClass() {
        CastorConfiguration cc = new CastorConfiguration(importDefinition, importSetup);
        try {
            cc.configure(LinkedList.class);
        } catch (ConfigurationException e) {
            assertFalse(true, "Configuration exception " + e.getMessage());
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#configure()}.
     */
    @Test()
    public void testConfigure() {
        CastorConfiguration cc = new CastorConfiguration(importDefinition,
                importerSetup);
        try {
            cc.configure();
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#getMappingFileName()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#setMappingFileName(java.lang.String)}.
     */
    @Test()
    public void testGetSetMappingFileName() {
        CastorConfiguration cc = new CastorConfiguration();
        assertNull(cc.getMappingFileName(),
                "Mapping File Name should not be set yet");
        cc.setMappingFileName(importDefinition);
        assertNotNull(cc.getMappingFileName(),
                "Mapping File Name should be set");
        assertEquals(cc.getMappingFileName(), importDefinition,
                "Mapping File Name should be " + importDefinition);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#getConfigFileName()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#setConfigFileName(java.lang.String)}.
     */
    @Test()
    public void testGetSetConfigFileName() {
        CastorConfiguration cc = new CastorConfiguration();
        assertNull(cc.getConfigFileName(),
                "Config File Name should not be set yet");
        cc.setConfigFileName(importerSetup);
        assertNotNull(cc.getConfigFileName(), "Config File Name should be set");
        assertEquals(importerSetup, cc.getConfigFileName(),
                "Config filename changed in set/get");

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#getMapping()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#setMapping(org.exolab.castor.mapping.Mapping)}.
     */
    @Test()
    public void testGetSetMapping() {
        CastorConfiguration cc = new CastorConfiguration();
        assertNull(cc.getMapping(), "Mapping object should not be set yet");
        cc.setMapping(new Mapping());

        URL unmarshalResource = ClassLoader.getSystemResource(importDefinition);

        try {
            cc.getMapping().loadMapping(unmarshalResource);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            fail("MalformedURLException in testGetSetMapping() method for CastorConfigurationTest");
        } catch (IOException e) {
            e.printStackTrace();
            fail("IOException in testGetSetMapping() method for CastorConfigurationTest");
        } catch (MappingException e) {
            e.printStackTrace();
            fail("MappingException in testGetSetMapping() method for CastorConfigurationTest");
        }
        assertNotNull(cc.getMapping(), "Mapping object should not be null");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#getMarshaller()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#
     * setMarshaller(org.exolab.castor.xml.Marshaller)}.
     */
    @Test()
    public void testGetSetMarshaller() {
        // nothing needed here yet
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#getUnmarshaller()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CastorConfiguration#
     * setUnmarshaller(org.exolab.castor.xml.Unmarshaller)}.
     */
    @Test()
    public void testGetSetUnmarshaller() {
        // nothing needed here yet
    }
}
