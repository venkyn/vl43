/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.voicelink.core.importer.parsers.FixedLengthDataSourceParser;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;
import com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger;

import java.util.ArrayList;
import java.util.Iterator;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * @author astein
 */
public class XmlTaggerTest {

    private XmlTagger xmlTagger = null;

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeClass
    public void setUp() throws Exception {
        xmlTagger = new XmlTagger();
    }

    private final String testPrologue = "<?xml version=\"1.0\" encoding='UTF-8'?>";

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.XmlTagger#XmlTagger()}.
     */
    @Test
    public final void testXmlTagger() {
        xmlTagger = new XmlTagger();
        assertEquals(xmlTagger.getXmlPrologue(), testPrologue,
                "XmlTagger class did not instantiate correctly.");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.XmlTagger#clearXmlRepresentation()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.XmlTagger#setXmlRepresentation(java.lang.String)}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.XmlTagger#getXmlRepresentation()}.
     */
    @Test
    public final void testGetSetClearXmlRepresentation() {
        String testXML = "<test>this is a test</test>";
        xmlTagger.setXmlRepresentation(testXML);
        assertEquals(xmlTagger.getXmlRepresentation(), testXML,
                "The XML did not set or get correctly.");
        xmlTagger.clearXmlRepresentation();
        assertEquals(xmlTagger.getXmlRepresentation(), "",
                "The XML did not clear correctly.");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.XmlTagger#
     * markUpFields(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test
    public final void testMarkUpFields() {
        try {
            String mappingFileName = "import-field-mapping.xml";
            String configFileName = "test-input-fields.xml";
            FixedLengthDataSourceParser dsp = new FixedLengthDataSourceParser();
            CastorConfiguration cc = new CastorConfiguration(mappingFileName,
                    configFileName);
            dsp.setFieldList((FixedLengthFieldMap) cc.configure());
            xmlTagger.markUpFields(dsp.getFieldList());
            FixedLengthFieldMap flfMap = dsp.getFieldList();
            Iterator<String> fieldIt = flfMap.keySet().iterator();
            if (fieldIt.hasNext()) {
                String fieldname = fieldIt.next().toString();
                FixedLengthField flf = (FixedLengthField) flfMap.get(fieldname);
                assertNotNull(flf.getCloseTag(),
                        "Close Tag should not be null.");
                assertNotNull(flf.getOpenTag(), "Open Tag should not be null.");
            }
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.XmlTagger#
     * makeXmlStream(com.vocollect.voicelink.core.importer.FieldMap, java.lang.String)}.
     */
    @SuppressWarnings("unchecked")
    @Test
    public final void testMakeXmlStream() {
        try {
            String mappingFileName = "import-field-mapping.xml";
            String configFileName = "test-input-fields.xml";
            FixedLengthDataSourceParser dsp = new FixedLengthDataSourceParser();
            CastorConfiguration cc = new CastorConfiguration(mappingFileName,
                    configFileName);
            dsp.setFieldList((FixedLengthFieldMap) cc.configure());
            xmlTagger.markUpFields(dsp.getFieldList());
            FixedLengthFieldMap flfMap = dsp.getFieldList();
            CreateObjectTrigger objTrigger1 = new EveryRecordCreateObjectTrigger();
            objTrigger1.setNewObjectName("test1");
            ArrayList alTriggers = new ArrayList();
            alTriggers.add(objTrigger1);
            xmlTagger.setNewObjectTriggers(alTriggers);
            assertEquals(xmlTagger.getXmlRepresentation().length(),
                    testPrologue.length(),
                    "XMLTagger object not instantiated correctly.");
            assertTrue("MakeXMLStream method not working correctly.", xmlTagger
                    .makeXmlStream(flfMap, "").length() > testPrologue.length());
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.XmlTagger#getXmlPrologue()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.XmlTagger#setXmlPrologue(java.lang.String)}.
     */
    @Test
    public final void testGetSetXmlPrologue() {
        String testXML = "<test>this is a test</test>";
        xmlTagger.setXmlPrologue(testXML);
        assertEquals(xmlTagger.getXmlPrologue(), testXML,
                "The XML did not set or get correctly.");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.XmlTagger#getNewObjectTriggers()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.XmlTagger#setNewObjectTriggers(java.util.ArrayList)}.
     */
    @SuppressWarnings("unchecked")
    @Test
    public final void testGetSetNewObjectTriggers() {
        CreateObjectTrigger objTrigger1 = new CreateObjectTriggerImpl();
        objTrigger1.setNewObjectName("test1");
        ArrayList alTriggers = new ArrayList();
        alTriggers.add(objTrigger1);
        xmlTagger.setNewObjectTriggers(alTriggers);
        ArrayList alResult = xmlTagger.getNewObjectTriggers();
        if (!alResult.isEmpty()) {
            CreateObjectTrigger objTriggerResult = (CreateObjectTrigger) alResult
                    .get(0);
            assertNotNull(objTriggerResult.getOpenTag());
            assertNotNull(objTriggerResult.getCloseTag());
        } else {
            fail("Not testing getsetnewobject triggers.");
        }
    }
}
