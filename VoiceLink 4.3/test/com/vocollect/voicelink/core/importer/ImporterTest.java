/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.test.DepInjectionSpringContextNGTests;
import com.vocollect.voicelink.core.importer.adapters.FileDataSourceAdapter;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthDataSourceParser;
import com.vocollect.voicelink.test.DbUnitAdapterVoiceLink;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import org.dbunit.operation.DatabaseOperation;
import org.exolab.castor.mapping.Mapping;
import org.exolab.castor.mapping.MappingException;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.ValidationException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test class for the Importer.
 */
@Test()
public class ImporterTest extends DepInjectionSpringContextNGTests {
    
    private Marshaller   failedValidationMarshaller = null;

    private FileWriter failedOutput = null;

    private Mapping myMapping = null;
    protected static final String DATA_DIR = "data/dbunit/voicelink/";
    
    private String testDir = null;

    private boolean overrideSite = false;

    private String siteName = "";

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.test.DepInjectionSpringContextNGTests#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return BaseImportServiceManagerTestCase.getImportTestConfigLocations();
    }

    /**
     * @throws Exception
     *             if unsuccessful
     */
    @BeforeClass()
        protected void importerSetUp() throws Exception {

        testDir = System.getProperty("test.input.dir");
        if (null == testDir) {
            testDir = "./";
        } else {
            testDir = testDir + "/";
        }

//        setSiteName("Site 1");
//      setSiteName("Site 2");
      setSiteName("Default");


//        org.exolab.castor.tools.MappingTool mapper = new MappingTool();
//        mapper.addClass( Validator.class );
//        mapper.addClass( ErrorRecord.class );
//        mapper.addClass(  EveryRecordCreateObjectTrigger.class );
//        mapper.addClass(  CreateObjectTrigger.class );
//        mapper.addClass(  Importer.class );
        
//        mapper.addClass(  junklist.class );
//        mapper.addClass( Item.class );
//        mapper.addClass( Location.class );
//        mapper.addClass(Assignment.class);
//        mapper.addClass( Operator.class );
        
//        mapper.write(new FileWriter("C:/temp/validator.mapping.xml"));
        
//        PersistenceManager pM = new PersistenceManager();
//        List<Location> objects = pM.getAll(Location.class);
//        marshalObject( objects.get(0) );;
//        Class cls = java.lang.String.class;
//        Method method = cls.getMethods()[0];
//        java.lang.reflect.Field field = (cls.getFields())[0];
//        Constructor constructor = cls.getConstructors()[0];
//        String name;
        
//        RTSI classFinder = new RTSI();
//        
////        classFinder.find(com.vocollect.epp.errors.ErrorCode.class.getName());
//        classFinder.find("com.vocollect",com.vocollect.epp.errors.ErrorCode.class.getName());
     
        
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        
    }
    

//    private void resetDB() {
//        DbUnitAdapter adapter = new DbUnitAdapter();
//        try {
//        adapter.resetInstallationData();
//        adapter.handleFlatXmlResource(
//            DATA_DIR + "VoiceLinkDataUnitTest_ResetAll.xml", DatabaseOperation.DELETE_ALL);
//        } catch (Exception e) {
//            assertFalse(true, "db reset failed.");
//        }
//    }
    
    /**
     * 
     *
     */
    @Test()
    public void testImportItems() throws Exception {
//        importByDatatype("item", testDir + "data/import", "itm.*dat",
//                Importer.SUCCESSFUL_STATE, false, EncodingSupport.UTF8);
//        resetDB();
        importByDatatypeAndConfig("item");
    }
    
    /**
     * 
     *
     */
    @Test()
    public void testImportDataTranslations() throws Exception {
//        importByDatatype("item", testDir + "data/import", "itm.*dat",
//                Importer.SUCCESSFUL_STATE, false, EncodingSupport.UTF8);
//        resetDB();
        importByDatatypeAndConfig("datatranslations");
    }
    
    /**
     * 
     *
     */
    @Test()
    public void testImportOperators() throws Exception {
//        importByDatatype("operator", testDir + "data/import", "oper.*dat",
//                Importer.SUCCESSFUL_STATE, false, EncodingSupport.UTF8);
//        resetDB();
        importByDatatypeAndConfig("operator");
    }
    
    /**
     * 
     *
     */
    @Test()
    public void testPurgeImportFiles() throws Exception {
        importByDatatypeAndConfig("purge");
    }
    
    /**
     * 
     *
     */
    @Test()
    public void testImportAssignments() throws Exception {
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();         
        adapter.resetInstallationData();
        adapter.loadSelectionMultiSiteData();
        
        importByDatatypeAndConfig("assignment");
    }
    
    
    /**
     * This is very similar to ImportAssignments except that the
     * import setup file explicitly asks for apSampleRegion6scrambled.dat
     * which is the same file as apSampleRegion6.dat except that it has
     * picks within an assigmnent in a non-contiguous (scrambled) order.
     */
    @Test()
    public void testImportAssignmentsScrambled() throws Exception {
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();         
        adapter.resetInstallationData();
        adapter.loadSelectionMultiSiteData();
        importByDatatypeAndConfig("assignmentScrambled");
    }

    
    /**
     * Put To Store License Import test.
     *
     */
    @Test()
    public void testImportPtsLicenses() throws Exception {
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();         
        //Note that this xml file is the same used for DB import of this object.
        // It sets up items, locs, custlocs, regions, and puts dbimport data into the "import_data" tables too.
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoicelinkDataUnitTest_PtsLicense_PtsPut_import_data.xml", DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("ptsLicense");
    }

    /**
     * Customer Location import test - used in Put To Store to map customers
     * to delivery locations. 
     *
     */
    @Test(enabled = true)
    public void testImportPtsCustomerLocations() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();       
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml", DatabaseOperation.REFRESH);     
        importByDatatypeAndConfig("customer-location");
        importByDatatypeAndConfig("customer-location");        
    }
    

    /**
     * 
     *
     */
    @Test()
    public void testImportLocations() throws Exception {
        importByDatatypeAndConfig("location");
    }

    
    /**
     * @throws Exception
     */
    @Test()
    public void testImportLots() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml", DatabaseOperation.REFRESH);
        
        importByDatatypeAndConfig("lot");
    }
    
    /**
     * @throws Exception
     */
    @Test()
    public void testImportReplenishLocations() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml", DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("location-replenish");
    }


    /**
     * @throws Exception
     */
    @Test()
    public void testImportReplenishments() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_ReplenReset.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_ForkAppWorkGroups.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_ReplenRegions.xml", DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("replenishment");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testImportPutawayLicense() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_PutawayReset.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_ForkAppWorkGroups.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_PutawayRegions.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_PutawayData.xml", DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("putaway-license");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testImportLineLoading() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_LineLoadReset.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_LineLoadRegions.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_LineLoadData.xml", DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("lineloading");
    }
    
    /**
     * @param datatype - the data type
     * @return - boolean
     * @throws Exception
     */
    private boolean importByDatatypeAndConfig(String datatype) throws Exception {
        CastorConfiguration cc = new CastorConfiguration("import-definition.xml", datatype + "-import-" + "setup.xml");
        Importer i = null;
            i = (Importer) cc.configure();

        if (overrideSite) {
            i.setSiteName(getSiteName());
        }

        i.prepare();
        i.start();
        
        // TODO Make a separate performance test.
        Calendar start = Calendar.getInstance();

        boolean result = i.runDatatypeJob();
        Calendar end = Calendar.getInstance();
        
        System.err.println(end.getTimeInMillis() - start.getTimeInMillis());
        
        return result;

    }

    
    /**
     * Generic launch for the import. Check a datatype definition at a time.
     * 
     * @param datatype
     *            the type to import
     * @param importDir
     *            where to look for the import file
     * @param filenamePattern
     *            pattern to use when looking for this (these) files
     * @param expectedState
     *            What state to expect upon completion
     * @param moveFilesWhenDone
     *            flag to move the files appropriately when done.
     * @param encoding
     *            the encoding for the import
     *            
     * @return true if the import was successful.            
     */
    private boolean importByDatatype(String datatype, String importDir,
            String filenamePattern, InternalState expectedState,
                                     boolean moveFilesWhenDone, String encoding) {
        Importer i = new Importer();
        i.setFailedValidationFileName("import-failures.xml");
        i.setMappingFileName("import-object-mapping.xml");

        FixedLengthDataSourceParser dsp = null;
        FileDataSourceAdapter fdsa = null;
        
        try {
            dsp = new FixedLengthDataSourceParser();
            dsp.setMappingFileName("import-field-mapping.xml");
            dsp.setConfigFileName(datatype + "-fields.xml");
            // TODO Move this and trigger with a flag to run diagnostics.
//            for (FixedLengthField f : dsp.getFieldList().values()){
//                                      f.getLength() + " : " + 
//                                      f.getStart() + " : " + 
//                                      f.getEnd());
//            }
            fdsa = new FileDataSourceAdapter();
            fdsa.setParentDirectory(importDir);
            // filenamePattern.toUpperCase()
            fdsa.setFileNamePattern(filenamePattern);
            fdsa.setFailureDirectory(importDir + "/failedImport");
            fdsa.setSuccessDirectory(importDir + "/successfulImport");
            fdsa.setMoveFilesOnCompletion(moveFilesWhenDone);
            fdsa.setEncoding(encoding);
            dsp.setSourceAdapter(fdsa);
            i.setSourceParser(dsp);
        } catch (ConfigurationException e1) {
            assertTrue(false, "Threw an exception during configuration");
        } catch (VocollectException e1) {
            assertTrue(
                    false,
                    "Threw an exception during configuration of data source"
                    );
        }
        ArrayList<CreateObjectTrigger> triggers = new ArrayList<CreateObjectTrigger>();
        CreateObjectTrigger lineTrigger = 
            new com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger();
        lineTrigger.setNewObjectName(datatype);
        lineTrigger.setOpenTag("<" + datatype + ">");
        lineTrigger.setCloseTag("</" + datatype + ">");
        triggers.add(lineTrigger);
        i.setTriggers(triggers);
        
        i.setDatatype(datatype);
        
        i.prepare();
        i.start();
        // TODO Make a separate performance test.
        Calendar start = Calendar.getInstance();

        boolean result = i.runDatatypeJob();
        Calendar end = Calendar.getInstance();
        
        System.err.println(end.getTimeInMillis() - start.getTimeInMillis());
        
        return result;
        
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Importer.Importer()'
     */
    @Test()
    public void testImporter() {
        Importer i = new Importer();
        
        assertNull(i.getSourceParser(), "Source Parser is null");
        assertEquals(DataTransferBase.INITIALIZED_STATE, i
                .getCurrentState(), "Incorrect State");
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Importer.getCurrentState()'.
     */
    @Test()
    public void testGetCurrentState() {
        DataTransferBase i = new Importer();
        assertEquals(DataTransferBase.INITIALIZED_STATE, i
                .getCurrentState(), "Incorrect state ");
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Importer.getState()'.
     */
    @Test()
    public void testGetState() {
        // start->finish
        DataTransferBase i = new Importer();
        assertEquals(DataTransferBase.INITIALIZED_STATE, i
                .getCurrentState(), "Incorrect state ");
        assertEquals(DataTransferBase.INITIALIZED_STATE
                , i.getCurrentState(), "Incorrect state description");
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Importer.getSourceParser()'.
     */
    @Test()
    public void testGetSourceParser() {
        DataTransferBase i = new Importer();
        assertNull(i.getSourceParser(), "Incorrect state description");

        i.setMappingFileName("import-object-mapping.xml");
        
        makeWorkingImporter(i, "test-input-fields.xml");
        
        assertNotNull(
                i.getSourceParser(),
                "Source parser Should Not be null after calling setConfig() "                
                );
        try {
            i.configure();
        } catch (VocollectException e) {
            assertFalse(true, "Configure threw an exception ");
        }
        assertNotNull(i.getSourceParser(), "Source parser Should not be null after calling configure()");
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Importer.setSourceParser(DataSourceParser)'.
     */
    @Test()
    public void testSetSourceParser() {
        DataTransferBase i = new Importer();
        EmptyTestParser e = null;
        try {
            e = new EmptyTestParser();
        } catch (ConfigurationException e1) {
            assertTrue(false, "EmptyTestParser Constructor threw exception"
                    + e1.getMessage());
        }
        assertNull(i.getSourceParser(), "Source parser should be null before being set. ");
        i.setSourceParser(e);
        assertNotNull(i.getSourceParser(), "Source parser should not be null after being set. ");
        assertSame(i.getSourceParser(), e, "Source parser should be the same as the one passed in");
        
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Importer.getBatchSize()'.
     */
    @Test()
    public void testGetBatchSize() {
        DataTransferBase i = new Importer();
        assertEquals(i.getBatchSize(), 1,  "Default Batch size incorrect ");
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Importer.setBatchSize(long)'.
     */
    @Test()
    public void testSetBatchSize() {
        DataTransferBase i = new Importer();
        i.setBatchSize(100);
        assertEquals(100, i.getBatchSize(), "Batch size incorrect ");
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Importer.configure()'.
     */
    @Test()
    public void testConfigure() {
        DataTransferBase i = new Importer();

        
        i.setMappingFileName("import-object-mapping.xml");
        
        makeWorkingImporter(i, "test-input-fields.xml");
        assertFalse(i.getSourceParser() == null, "Source Parser is null");
        assertEquals(DataTransferBase.CONFIGURED_STATE, i.getCurrentState(), "Incorrect State");
    }
    
    /**
     * Test method for 'com.vocollect.voicelink.core.importer.Importer.start()'.
     */
    @Test()
    public void testStart() {
        DataTransferBase i = new Importer();

        i.setMappingFileName("import-object-mapping.xml");
        makeWorkingImporter(i, "test-input-fields.xml");
        i.start();
        assertEquals(DataTransferBase.PENDING_STATE, i.getCurrentState(), "Incorrect State");
        i.start();
        assertEquals(DataTransferBase.STARTED_STATE, i.getCurrentState(), "Incorrect State");
        
    }
    
    /**
     * Test method for 'com.vocollect.voicelink.core.importer.Importer.stop()'.
     */
    @Test()
    public void testStop() {
        DataTransferBase i = new Importer();
        i.setMappingFileName("import-object-mapping.xml");
        makeWorkingImporter(i, "test-input-fields.xml");
        assertEquals(DataTransferBase.CONFIGURED_STATE, i.getCurrentState(), "Threw an exception during configuration");
        i.start();
        i.stop();
        assertEquals(DataTransferBase.NOT_RUN_STATE, i.getCurrentState(), "Incorrect State");
        
    }
    
    /*
     * Test method for
     'com.vocollect.voicelink.core.importer.Importer.importDatatype()'
     * TODO Get this to work. We should be able to accept either a URL or a
     filename
     */
//    @Test()
//    public void testImportDatatypeAcrossNetwork() {
//        Importer i = new Importer();
//        i.setFailedValidationFileName("import-failures.xml");
//        i.setMappingFileName("import-object-mapping.xml");
//        i.setConfigFileName("test.importer.config");
//        FixedLengthDataSourceParser dsp = null;
//        FileDataSourceAdapter fdsa = null;
//        
//        try {
//            dsp = new FixedLengthDataSourceParser();
//            dsp.setMappingFileName("field-mapping.xml");
//            dsp.setConfigFileName("test-input-fields.xml");
//            fdsa = new FileDataSourceAdapter();
//            File f = new File( "C:/temp/");
//            
//            URL parentDirectory = null;
//            try {
//                parentDirectory = f.toURL();
//            } catch ( MalformedURLException e ) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
////            parentDirectory = new URL( f );
////            try {
////                try {
////                    parentDirectory = new URI("C:/temp/");
////                } catch ( URISyntaxException e ) {
////                    // TODO Auto-generated catch block
////                    e.printStackTrace();
////                }
////            } catch ( MalformedURLException e ) {
////                e.printStackTrace();
    // // assertTrue( "Threw an exception trying to convert c:/temp to a URL",
    // false);
////            }
//            fdsa.setParentDirectory( parentDirectory.toString() );
//            fdsa.setFileNamePattern( "onerecordtest.dat" );
//            dsp.setSourceAdapter(fdsa);
//            i.setSourceParser(dsp);
//        } catch (ConfigurationException e1) {
//            assertTrue( "Threw an exception during configuration", false );
//        } catch (VocollectException e1) {
    // assertTrue( "Threw an exception during configuration of data source",
    // false );
//        }
//        
//        
//        i.setDatatype( "test" );
//        
//        i.start();
//        i.start();
//        Calendar start = Calendar.getInstance();
//        
//        
//        boolean result = i.importDatatype();
//        Calendar end = Calendar.getInstance();
//        
//        System.err.println( end.getTimeInMillis() - start.getTimeInMillis() );
//        
//        assertTrue( "import failed ", result );
    // assertEquals( "Incorrect State", Importer.FAILED_STATE,
    // i.getCurrentState() );
//        
//    }
//
    /*
     * Test method for 'com.vocollect.voicelink.core.importer.Importer.finish()'
     * This also tests the state transitions TODO Fix the import configuration
     * so we can specify a file to get for this test
     */
    
    /**
     * 
     */
    @Test()
    public void testFinish() {
            // start->stop: 
            DataTransferBase i = new Importer();
            i.setMappingFileName("import-object-mapping.xml");
            i.setDescription("test");
            makeWorkingImporter(i, "test-input-fields.xml");
            i.prepare();
            assertEquals(DataTransferBase.PENDING_STATE, i.getCurrentState(), "Incorrect state");
            i.start();
            i.stop();
        assertEquals(DataTransferBase.NOT_RUN_STATE, i.getCurrentState(), "Incorrect state");
            i.finish();
        assertEquals(DataTransferBase.NOT_RUN_STATE, i.getCurrentState(), "Incorrect state");
            // start->finish
        i = new Importer();
        i.setMappingFileName("import-object-mapping.xml");
        i.setDescription("test");
        makeWorkingImporter(i, "test-input-fields.xml");
        i.prepare();
        assertEquals(DataTransferBase.PENDING_STATE, i.getCurrentState(), "Incorrect state");
            i.start();
            i.stop();
        assertEquals(DataTransferBase.NOT_RUN_STATE, i.getCurrentState(), "Incorrect state");
            i.finish();
        assertEquals(DataTransferBase.NOT_RUN_STATE, i.getCurrentState(), "Incorrect state");
    }
    
    /**
     * @param i
     *            Importer to set
     * @param fieldConfigFile
     *            specifies config file to set
     */
    private void makeWorkingImporter(DataTransferBase i, String fieldConfigFile) {
        i.setMappingFileName("import-object-mapping.xml");
        
        try {
            FixedLengthDataSourceParser dsp = new FixedLengthDataSourceParser();
            dsp.setMappingFileName("import-field-mapping.xml");
            dsp.setConfigFileName(fieldConfigFile);
            FileDataSourceAdapter fdsa = new FileDataSourceAdapter();
            dsp.setSourceAdapter(fdsa);
            i.setSourceParser(dsp);
        } catch (ConfigurationException e1) {
            assertFalse(true,  "Configuration error");
        } catch (VocollectException e1) {
            assertFalse(true,  "Configuration error");
        }
    }
    
    /**
     * @param o
     *            Objext to marshal
     * @return Returns the failedValidationMarshaller.
     */
    private Marshaller marshalObject(Object o) {
        if (null == this.failedValidationMarshaller) {
            try {
                CastorConfiguration cc = new CastorConfiguration();
                cc.setMappingFileName("import-object-mapping.xml");
                try {
                    myMapping = cc.loadMapping();
                } catch (ConfigurationException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                failedOutput = new FileWriter(o.getClass().getSimpleName()
                        + "-marshslling.xml", true);
                this.failedValidationMarshaller = new Marshaller(failedOutput);
                //this.failedValidationMarshaller.setRootElement("");
                this.failedValidationMarshaller.setMapping(myMapping);
                failedOutput.write("<?xml version=\"1.0\" encoding='UTF-8'?>");
                this.failedValidationMarshaller.setSupressXMLDeclaration(true);
            } catch (IOException e) {
                throw new RuntimeException(
                        "Unable to open failed validation file");
            } catch (MappingException e) {
                // This should have happened during configuration - we're using
                // the same mapping file...
                throw new RuntimeException("Bad or missing mapping file");
            }
        }
        try {
            failedValidationMarshaller.marshal(o);
        } catch (MarshalException me) {
            me.printStackTrace();
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        return this.failedValidationMarshaller;
    }


    /**
     * @return the siteName
     */
    public String getSiteName() {
        return siteName;
    }


    /**
     * @param siteName the siteName to set
     */
    private void setSiteName(String siteName) {
        this.siteName = siteName;
        this.overrideSite = true;
    }

}
