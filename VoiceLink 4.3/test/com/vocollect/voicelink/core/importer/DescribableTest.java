/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;


import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;

/**
 * 
 * @author astein
 *
 */
public class DescribableTest {

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Description.Describable()'.
     */
    @Test()
    public void testDescribable() {
        Describable d = null;
        d = new Description();
        assertNotNull("constructor failed ", d);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Description.getDescription()'. Test
     * method for
     * 'com.vocollect.voicelink.core.importer.Description.setDescription(String)'
     */
    @Test()
    public void testGetDescription() {
        String testDescription = "Just a description";
        Describable d = new Description();
        assertNotNull("default description is null", d.getDescription());
        assertEquals("Default is not an empty string", 0, "".compareTo(""));
        d.setDescription(testDescription);
        assertNotNull("setDescription failed", d.getDescription());
        assertEquals("Set or get failed ", testDescription, d.getDescription());
    }

}
