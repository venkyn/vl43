/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;

/**
 * @author astein
 */
public class ValidationResultTest {
    private TestInput tiGood            = null;

    private TestInput tiBad             = null;

    private String    testContextString = "Just a test context string";
    
    private FieldMap junkFields = new FixedLengthFieldMap();

    /**
     * 
     *
     */
    public ValidationResultTest() {
        tiGood = new TestInput();
        tiGood.setFather(8);
        tiGood.setDay("25");
        tiGood.setMonth("06");
        tiGood.setYear("1957");
        tiGood.setFirstName("Duff");
        tiGood.setLastName("Gold");
        tiGood.setField_h(new Integer(30));

        tiBad = new TestInput();
        tiBad.setFather(22);
        tiBad.setDay("25");
        tiBad.setMonth("06");
        tiBad.setYear("1957");
        tiBad.setFirstName("Duff");
        tiBad.setLastName("Gold");
        tiBad.setField_h(new Integer(30));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.ValidationResult()'.
     */
    @Test()
    public void testValidationResult() {
        ValidationResult vr = new ValidationResult();
        assertNotNull(vr);
        try {
            vr.validate();
        } catch (RuntimeException e) {
            assertTrue(
                    "Should not throw an exception validating a null object",
                    false);
        }
        assertNull("The thing to validate is not null", vr.getThingToValidate());
        assertNotNull(
                "The default import context was null, should be an empty string",
                vr.getObjectImportContext());

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.ValidationResult(Object)'.
     */
    @Test()
    public void testValidationResultObjectnull() {
        ValidationResult vr = new ValidationResult(null);

        assertNotNull(vr);
        try {
            vr.validate();
        } catch (RuntimeException e) {
            assertTrue(
                    "Should not throw an exception validating a null object",
                    false);
        }
        assertNull("The thing to validate is not null", vr.getThingToValidate());
        assertNotNull(
                "The default import context was null, should be an empty string",
                vr.getObjectImportContext());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.ValidationResult(Object)'.
     */
    @Test()
    public void testValidationResultObjectNotNull() {
        ValidationResult vr = new ValidationResult(tiGood);

        assertNotNull(vr);
        try {
            assertTrue("Purportedly good object did not pass validation.", vr
                    .validate());
        } catch (RuntimeException e) {
            assertTrue(
                    "Should not throw an exception validating a real object",
                    false);
        }
        assertNotNull("The thing to validate is null", vr.getThingToValidate());
        assertNotNull(
                "The default import context was null, should be an empty string",
                vr.getObjectImportContext());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.ValidationResult(String,
     * Object)'.
     */
    @Test()
    public void testValidationResultStringObject() {
        ValidationResult vr = new ValidationResult(testContextString, tiGood);
        assertNotNull(vr);
        try {
            assertTrue("Purportedly good object did not pass validation.", vr
                    .validate());
        } catch (RuntimeException e) {
            assertTrue(
                    "Should not throw an exception validating a real object",
                    false);
        }
        assertNotNull("The thing to validate is null", vr.getThingToValidate());
        assertNotNull(
                "The default import context was null, should be an empty string",
                vr.getObjectImportContext());
        assertEquals("Munged import context", this.testContextString, vr
                .getObjectImportContext());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.validate()'.
     */
    @Test(enabled = false)
    public void testValidate() {
        ValidationResult vr = new ValidationResult(testContextString, tiGood);
        assertTrue("Good object failed validation", vr.validate());
        vr.setThingToValidate(tiBad);
        assertFalse("Bad object passed validation", vr.validate());

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.validate(Object)'.
     */
    @Test()
    public void testValidateObjectGood() {
        ValidationResult vr = new ValidationResult();
        vr.setObjectImportContext(testContextString);
        assertTrue("Good object failed validation", vr.validate(tiGood));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.validate(Object)'.
     */
    @Test(enabled = false)
    public void testValidateObjectBad() {
        ValidationResult vr = new ValidationResult();
        vr.setObjectImportContext(testContextString);
        assertFalse("Bad object passed validation", vr.validate(tiBad));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.validate(String,
     * Object)'.
     */
    @Test()
    public void testValidateStringObjectGood() {
        ValidationResult vr = new ValidationResult();
        assertTrue("Good object failed validation", vr.validate(
                testContextString, junkFields, tiGood));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.validate(String,
     * Object)'.
     */
    @Test(enabled = false)
    public void testValidateStringObjectBad() {
        ValidationResult vr = new ValidationResult();
        assertFalse("Bad object passed validation", vr.validate(
                testContextString, junkFields, tiBad));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.getObjectImportContext()'.
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.setObjectImportContext(String)'
     */
    @Test()
    public void testGetObjectImportContext() {
        ValidationResult vr = new ValidationResult();
        vr.setObjectImportContext(testContextString);
        assertEquals("Munged import context", testContextString, vr
                .getObjectImportContext());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.getThingToValidate()'.
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.setThingToValidate(Object)'
     */
    @Test()
    public void testGetThingToValidate() {
        ValidationResult vr = new ValidationResult();
        vr.setThingToValidate(tiGood);
        assertEquals("Wrong object to validate", tiGood, vr
                .getThingToValidate());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ValidationResult.getErrorList()'.
     */
    @Test(enabled = false)
    public void testGetErrorList() {
        ValidationResult vr = new ValidationResult(testContextString, tiGood);
        assertTrue("Good object failed validation", vr.validate());
        assertTrue("Non-empty error list for good object", vr.getErrorList()
                .isEmpty());
        assertFalse("Bad object passed validation", vr.validate(tiBad));
        assertFalse("Empty error list for good object", vr.getErrorList()
                .isEmpty());
        assertTrue("Good object failed validation", vr.validate(tiGood));
        assertTrue(
                "Non-empty error list for good object following validation of bad object.",
                vr.getErrorList().isEmpty());

    }

}
