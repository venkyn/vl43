/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * @author astein
 */
public class StateChangesTest {

    private StateChanges  states                 = null;

    private InternalState successfulTestIntState = new InternalState(
                                                 1,
                                                 "Importer.Successful",
                                                 InternalState.FinalStateType.Success);

    private InternalState failedTestIntState     = new InternalState(
                                                 2,
                                                 "Importer.Failed",
                                                 InternalState.FinalStateType.Failed);

    private InternalState configuredTestIntState = new InternalState(3,
                                                 "Importer.Configured");

    private StateChange   stateChange            = new StateChange(
                                                 configuredTestIntState,
                                                 successfulTestIntState);

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeClass
    public void setUp() throws Exception {
        states = new StateChanges();
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.StateChanges#StateChanges()}.
     */
    @Test
    public final void testStateChanges() {
        assertNotNull(states,
                "StateChanges class did not instantiate correctly.");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.StateChanges#StateChanges(int)}.
     */
    @Test
    public final void testStateChangesInt() {
        states = new StateChanges(1);
        assertNotNull(states,
                "StateChanges class did not instantiate correctly.");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.StateChanges
     * #add(com.vocollect.voicelink.core.importer.StateChange)}.
     */
    @Test
    public final void testAddStateChange() {
        states.add(stateChange);
        assertNotNull(states,
                "StateChanges class did not instantiate correctly.");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.StateChanges
     * #add(com.vocollect.voicelink.core.importer.InternalState, 
     * com.vocollect.voicelink.core.importer.InternalState)}.
     */
    @Test
    public final void testAddInternalStateInternalState() {
        states.add(this.configuredTestIntState, this.successfulTestIntState);
        assertNotNull(states,
                "StateChanges class did not instantiate correctly.");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.StateChanges
     * #setCurrentState(com.vocollect.voicelink.core.importer.InternalState)}.
     */
    @Test
    public final void testSetCurrentState() {
        states = new StateChanges();
        states.add(this.configuredTestIntState, this.successfulTestIntState);
        assertTrue(states.setCurrentState(this.configuredTestIntState),
                "Not a legal state change to switch to.");
        assertTrue(states.setCurrentState(this.successfulTestIntState),
                "Not a legal state change to switch to.");
        assertFalse(states.setCurrentState(this.failedTestIntState),
                "This should not be a legal state change to switch to.");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.StateChanges#getCurrentState()}.
     */
    @Test
    public final void testGetCurrentState() {
        states = new StateChanges();
        states.setCurrentState(this.configuredTestIntState);
        assertEquals(states.getCurrentState(), this.configuredTestIntState,
                "Not returning correct state.");
    }

}
