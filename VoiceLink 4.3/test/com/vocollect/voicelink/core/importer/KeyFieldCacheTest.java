/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

/**
 * 
 * @author astein
 *
 */
public class KeyFieldCacheTest {

    private int    initialKeyCount         = 12;

    private int    initialDataElementCount = 10000;

    private String keyFieldName            = "test";

    private String keyFieldName2           = "unassociated";

    private String keyFieldName3           = "test ";

    private String data1                   = "junk";

    private String data2                   = "more junk";

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.KeyFieldCache'.
     */
    @Test()
    public void testKeyFieldCache() {
        KeyFieldCache kfc = new KeyFieldCache();
        assertEquals("Wrong initial cache size ", kfc
                .getDefaultInitialCacheSize(), kfc.getInitialCacheSize());
        assertEquals("Wrong initial key count ", kfc
                .getDefaultInitialKeyCount(), kfc.getInitialKeyCount());
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.KeyFieldCache(int,
     * int)'.
     */
    @Test()
    public void testKeyFieldCacheIntInt() {

        KeyFieldCache kfc = new KeyFieldCache(initialKeyCount,
                initialDataElementCount);
        assertEquals("Wrong initial cache size ", initialDataElementCount, kfc
                .getInitialCacheSize());
        assertEquals("Wrong initial key count ", initialKeyCount, kfc
                .getInitialKeyCount());

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.KeyFieldCache.addNewKeyField(String)'.
     */
    @Test()
    public void testAddNewKeyField() {
        KeyFieldCache kfc = new KeyFieldCache();
        assertTrue("Unable to add key field", kfc.addNewKeyField(keyFieldName));
        assertFalse("Missing key field ", kfc.addNewKeyField(keyFieldName));

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.KeyFieldCache.getInitialCacheSize()'.
     * Test method for
     * 'com.vocollect.voicelink.core.importer.KeyFieldCache.setInitialCacheSize(int)'
     */
    @Test()
    public void testGetInitialCacheSize() {
        KeyFieldCache kfc = new KeyFieldCache();
        assertEquals("Wrong initial cache size ", kfc
                .getDefaultInitialCacheSize(), kfc.getInitialCacheSize());
        assertEquals("Wrong initial key count ", kfc
                .getDefaultInitialKeyCount(), kfc.getInitialKeyCount());
        kfc.setInitialCacheSize(initialDataElementCount);
        assertEquals("Wrong initial cache size ", initialDataElementCount, kfc
                .getInitialCacheSize());
        assertEquals("Wrong initial key count ", kfc
                .getDefaultInitialKeyCount(), kfc.getInitialKeyCount());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.KeyFieldCache.find(String,
     * String)'.
     */
    @Test()
    public void testFind() {
        KeyFieldCache kfc = new KeyFieldCache();
        assertTrue("Unable to add key field", kfc.addNewKeyField(keyFieldName));
        assertFalse("Should not have found this key", kfc.find(keyFieldName,
                data1));

        assertFalse("Missing key field ", kfc.addNewKeyField(keyFieldName));
        assertFalse("Should not have found this key", kfc.find(keyFieldName,
                data1));

        assertTrue("Should be able to add field data ", kfc.addKey(
                keyFieldName, data1));
        assertTrue("Should have found this key", kfc.find(keyFieldName, data1));

        assertTrue("Unable to add field data ", kfc
                .addKey(keyFieldName2, data1));
        assertTrue("Should have found this key", kfc.find(keyFieldName2, data1));
        assertFalse("Should not have found this key", kfc.find(keyFieldName3,
                data1));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.KeyFieldCache.addKey(String,
     * String)'.
     */
    @Test()
    public void testAddKey() {
        KeyFieldCache kfc = new KeyFieldCache();
        assertTrue("Unable to add key field", kfc.addNewKeyField(keyFieldName));
        assertFalse("Missing key field ", kfc.addNewKeyField(keyFieldName));
        assertTrue("Unable to add field data ", kfc.addKey(keyFieldName, data1));
        assertEquals("Initial count of elements should be zero?", 0, kfc.getKeyCount(keyFieldName, data1));
        assertTrue("Should be able to add a key even if the field's not there",
                kfc.addKey(keyFieldName2, data2));
        assertEquals("Initial count of elements should be zero?", 0, kfc.getKeyCount(keyFieldName, data2));

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.KeyFieldCache.incrementKeyCount(String,
     * String)'.
     */
    @Test()
    public void testIncrementKeyCount() {
        KeyFieldCache kfc = new KeyFieldCache();
        assertTrue("Should be able to add a key even if the field's not there",
                kfc.addKey(keyFieldName2, data2));
        assertEquals("initial count of elements in key should be zero", 0, kfc.getKeyCount(keyFieldName2, data2));
        assertFalse("Should have some element count", (0 == kfc
                .incrementKeyCount(keyFieldName2, data2)));
        assertEquals("count of elements in key should now be 1", 1, kfc.getKeyCount(keyFieldName2, data2));

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.KeyFieldCache.getKeyCount(String,
     * String)'.
     */
    @Test()
    public void testGetKeyCount() {
        KeyFieldCache kfc = new KeyFieldCache();
        assertTrue("Should be able to add a key even if the field's not there",
                kfc.addKey(keyFieldName2, data2));
        assertEquals("Should be a zero count", 0, kfc.getKeyCount(
                keyFieldName2, data2));
        assertFalse("Should have some element count", (0 == kfc
                .incrementKeyCount(keyFieldName2, data2)));
        assertEquals("Should be a zero count", 1, kfc.getKeyCount(
                keyFieldName2, data2));

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.KeyFieldCache.getInitialKeyCount()'.
     * Test method for
     * 'com.vocollect.voicelink.core.importer.KeyFieldCache.setInitialKeyCount(int)'
     */
    @Test()
    public void testGetInitialKeyCount() {
        KeyFieldCache kfc = new KeyFieldCache();
        assertEquals("Wrong initial cache size ", kfc
                .getDefaultInitialCacheSize(), kfc.getInitialCacheSize());
        assertEquals("Wrong initial key count ", kfc
                .getDefaultInitialKeyCount(), kfc.getInitialKeyCount());
        kfc.setInitialKeyCount(initialDataElementCount);
        assertEquals("Wrong initial cache size ", kfc
                .getDefaultInitialCacheSize(), kfc.getInitialCacheSize());
        assertEquals("Wrong initial key count ", initialDataElementCount, kfc
                .getInitialKeyCount());

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.KeyFieldCache.clear()'.
     */
    @Test()
    public void testClear() {
        KeyFieldCache kfc = new KeyFieldCache();
        assertTrue("Should be able to add a key even if the field's not there",
                kfc.addKey(keyFieldName, data2));
        assertFalse("Should have some element count", (0 == kfc
                .incrementKeyCount(keyFieldName, data2)));
        kfc.clear();
        assertFalse("Should not have found this key", kfc.find(keyFieldName,
                data2));
        assertTrue("Unable to add key field after clear", kfc
                .addNewKeyField(keyFieldName));

    }

}








