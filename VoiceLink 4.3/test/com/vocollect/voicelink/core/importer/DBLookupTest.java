 /*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorActionsExportTransport;
import com.vocollect.voicelink.selection.model.ContainerExportTransport;
import com.vocollect.voicelink.selection.model.PickExportTransport;
import com.vocollect.voicelink.selection.service.AssignmentManager;
import com.vocollect.voicelink.test.DbUnitAdapterVoiceLink;

import java.lang.reflect.Method;
import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test for the DBLookup component.
 * @author dgold
 *
 */
public class DBLookupTest extends BaseImportServiceManagerTestCase {

    private PersistenceManager        persistenceManager;
    
    private String siteName = Site.DEFAULT_SITE_NAME;

    private DBLookup finder;
    
    private OperatorDAO dao = null;
    
    private AssignmentManager assignmentManager = null;
    

    /**
     * {@inheritDoc}
     * @param adapter
     * @throws Exception
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        setSiteName("Site 1");
    }

    /**
     * @throws java.lang.Exception just because.
     */
    @BeforeMethod
    protected void setUpBeforeTest() throws Exception {
        finder = new DBLookup(persistenceManager);
    }
    
    /** 
     * @param operatorDAO the user data access object.
     */
    @Test(enabled = false)
    public void setOperatorDAO(OperatorDAO operatorDAO) {
        this.dao = operatorDAO;
    }

    /**
     * @param pm - the persistence manager
     */
    @Test(enabled = false)
    public void setPersistenceManager(PersistenceManager pm) {
        this.persistenceManager = pm;
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.DBLookup#DBLookup()}.
     */
    @Test()
    public void testDBLookup() {
        assertNotNull(finder, "constructor delivered a null");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.DBLookup#lookup(java.lang.Object)}.
     */
    @Test()
    public void testLookup() {

        DbUnitAdapter adapter = new DbUnitAdapter();

        try {
            adapter.resetInstallationData();
            adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkDataSmall.xml");
        } catch (Exception e2) {
            e2.printStackTrace();
            fail("DBUnit adapter threw an EXCEPTION.");
        }

        Operator testOperator = new Operator();
        @SuppressWarnings("unused")
        Object testResult = testOperator;
        // This might bring back all items.
        List<Operator> allOperators = null;
        List<Object> oneItem = null;
        // First, we will try an existing finder: OperatorDAO.findByIdentifier(String operatorId)
        String findByIdentifier = "findByIdentifier";

        // Set up the field to contain the find-by-identifier query parameter(s)
        Field lookupParameterField = new Field();
        lookupParameterField.setFieldName("operatorIdentifier");
        // This should always have keyField on.
        lookupParameterField.setKeyField(true);
        
        // We are passing a FieldMap in as the list of parameters.
        FieldMap fieldsForLookup = new FieldMap(); 
        fieldsForLookup.addField(lookupParameterField);
        
        // Go get something from the database - we will use this ID  to do the lookup
        try {
            allOperators = this.dao.getAll();
        } catch (DataAccessException e1) {
            assertFalse(true, "getAll using the DAO threw an exception");
        }
        String testID = allOperators.get(0).getOperatorIdentifier();        

        // See if we can actually get the operator by this ID using the finder we chose to use
        Operator expectedResult = null;
        try {
            expectedResult = this.dao.findByIdentifier(testID);
        } catch (DataAccessException e) {
            assertFalse(true, "Lookup using the DAO threw an exception");
        }
        
        // OK, good, so we know the finder works. Set the ID in the testOperator
        testOperator.setOperatorIdentifier(testID);
        
        // Ask the persistence manager to go get the matching POS
        oneItem = finder.lookup(testOperator, findByIdentifier, fieldsForLookup);

        // We always get a list back. Make sure we got what we are looking for
        testResult = oneItem.get(0);
        assertEquals(1, oneItem.size(), "Should have only found one item");
        assertEquals(testOperator.getOperatorIdentifier(), expectedResult.getOperatorIdentifier());

    }
    
    /**
     * @throws Exception
     */
    @Test()
    public void lookupExportTransport() throws Exception {
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();         
        adapter.resetInstallationData();
        adapter.loadFullMultiSiteData();
        
        PickExportTransport pet = new PickExportTransport(); 
        ContainerExportTransport cet = new ContainerExportTransport();
        OperatorActionsExportTransport oaet = new OperatorActionsExportTransport(); 
        
        FieldMap fieldsForLookup = new FieldMap();
        String listPicked = "listPickedPicks";
        String listLabor  = "listExportAssignmentLabor";
        String listClosedContainers  = "listClosedContainers";
        String listOperatorActions = "listOperatorActions";
        
        DBLookup reporter = new DBLookup(this.persistenceManager);

        List<Object> l = null;
        l = reporter.lookup(pet, listPicked, fieldsForLookup);
        assertTrue(l != null , "Export lookup for listPicked Failed");
        
        l = reporter.lookup(pet, listLabor, fieldsForLookup);
        assertTrue(l != null , "Export lookup for listLabor Failed");

        l = reporter.lookup(cet, listClosedContainers, fieldsForLookup);
        assertTrue(l != null , "Export lookup for listClosedContainers Failed");
        
        l = reporter.lookup(oaet, listOperatorActions, fieldsForLookup);
        assertTrue(l != null , "Export lookup for listOperatorActions Failed");

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.DBLookup#conditionalInsert(java.lang.Object)}.
     */
    @Test()
    public void testConditionalInsert() {
        DbUnitAdapter adapter = new DbUnitAdapter();
        try {
            adapter.resetInstallationData();
            adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkDataSmall.xml");
        } catch (Exception e2) {
            e2.printStackTrace();
            fail("DBUnit adapter threw an EXCEPTION.");
        }

        // Test to see if we can just get the existing element
        Operator testOperator = new Operator();
        Operator testResult = null;
        // This might bring back all items.
        List<Operator> allOperators = null;
        // First, we will try an existing finder: OperatorDAO.findByIdentifier(String operatorId)
        String findByIdentifier = "findByIdentifier";

        // Set up the field to contain the find-by-identifier query parameter(s)
        Field lookupParameterField = new Field();
        lookupParameterField.setFieldName("operatorIdentifier");
        // This should always have keyField on.
        lookupParameterField.setKeyField(true);
        
        // We are passing a FieldMap in as the list of parameters.
        FieldMap fieldsForLookup = new FieldMap(); 
        fieldsForLookup.addField(lookupParameterField);
        
        // Go get something from the database - we will use this ID  to do the lookup
        try {
            allOperators = this.dao.getAll();
        } catch (DataAccessException e1) {
            assertFalse(true, "getAll using the DAO threw an exception");
        }
        String testID = allOperators.get(0).getOperatorIdentifier();        

        // See if we can actually get the operator by this ID using the finder we chose to use
        Operator expectedResult = null;
        try {
            expectedResult = this.dao.findByIdentifier(testID);
        } catch (DataAccessException e) {
            assertFalse(true, "Lookup using the DAO threw an exception");
        }
        
        // OK, good, so we know the finder works. Set the ID in the testOperator
        testOperator.setOperatorIdentifier(testID);
        
        // Ask the persistence manager to go get the matching POS
        try {
            testResult = (Operator) finder.conditionalInsert(testOperator, findByIdentifier, fieldsForLookup);
        } catch (VocollectException e) {
            e.printStackTrace();
            assertFalse(true, "finder threw an exception");
        }

        // OK, we should have a copy of the element in the database
        // The DAO doesn't muck with the original
        assertEquals(expectedResult.getOperatorIdentifier(), 
                     testResult.getOperatorIdentifier(), "Retrieved the wrong element");
        
        // Setup to acutally insert an element that is missing
        
        Operator newOperator = new Operator();
        newOperator.setAssignedRegion(testResult.getAssignedRegion());
        newOperator.setCurrentRegion(testResult.getCurrentRegion());
        newOperator.setLastLocation(testResult.getLastLocation());
        newOperator.setOperatorIdentifier("testConditionalInsert");
        newOperator.setPassword(testResult.getPassword());
        newOperator.setSignOffTime(testResult.getSignOffTime());
        newOperator.setSignOnTime(testResult.getSignOnTime());
        newOperator.setStatus(testResult.getStatus());
        
        testResult = null;
        
        try {
            testResult = (Operator) finder.conditionalInsert(newOperator, findByIdentifier, fieldsForLookup);
        } catch (VocollectException e) {
            e.printStackTrace();
            assertFalse(true, "finder threw an exception");
        }
        assertEquals(newOperator.getOperatorIdentifier(), 
                testResult.getOperatorIdentifier(), "Retrieved the wrong element");
        assertEquals(newOperator.getPassword(), 
                testResult.getPassword(), "Retrieved the wrong element");
        assertNotNull(testResult.getId(), "ID is still null in inserted object");
        assertEquals(newOperator.getId(), testResult.getId(), "IDs for the inserted element differ");
        try {
            this.persistenceManager.delete(newOperator);
        } catch (BusinessRuleException e) {
            e.printStackTrace();
            assertFalse(true, "pm threw an exception");
        } catch (DataAccessException e) {
            e.printStackTrace();
            assertFalse(true, "pm threw an exception");
        }
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.DBLookup#conditionalUpdate(java.lang.Object)}.
     */
    @Test()
    public void testConditionalUpdate() {
        DbUnitAdapter adapter = new DbUnitAdapter();
        try {
            adapter.resetInstallationData();
            adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkDataSmall.xml");
        } catch (Exception e2) {
            e2.printStackTrace();
            fail("DBUnit adapter threw an EXCEPTION.");
        }
        // Test to see if we can just get the existing element
        Operator testOperator = new Operator();
        Operator testResult = null;
        // This might bring back all items.
        List<Operator> allOperators = null;
        // First, we will try an existing finder: OperatorDAO.findByIdentifier(String operatorId)
        String findByIdentifier = "findByIdentifier";

        // Set up the field to contain the find-by-identifier query parameter(s)
        Field lookupParameterField = new Field();
        lookupParameterField.setFieldName("operatorIdentifier");
        // This should always have keyField on.
        lookupParameterField.setKeyField(true);
        
        // We are passing a FieldMap in as the list of parameters.
        FieldMap fieldsForLookup = new FieldMap(); 
        fieldsForLookup.addField(lookupParameterField);
        
        FieldMap allFields = new FieldMap();
        Field dataField = null;
        
        dataField = new Field();
        dataField.setFieldName("operatorIdentifier");
        allFields.addField(dataField);
        
        dataField = new Field();
        dataField.setFieldName("password");
        allFields.addField(dataField);
        
        
        // Go get something from the database - we will use this ID  to do the lookup
        try {
            allOperators = this.dao.getAll();
        } catch (DataAccessException e1) {
            assertFalse(true, "getAll using the DAO threw an exception");
        }
        String testID = allOperators.get(0).getOperatorIdentifier();        

        // See if we can actually get the operator by this ID using the finder we chose to use
        Operator expectedResult = null;
        try {
            expectedResult = this.dao.findByIdentifier(testID);
        } catch (DataAccessException e) {
            assertFalse(true, "Lookup using the DAO threw an exception");
        }
        
        // OK, good, so we know the finder works. Set the ID in the testOperator
        testOperator.setOperatorIdentifier(testID);
        testOperator.setPassword("22222");
        
        // Ask the persistence manager to go get the matching POS
        try {
            testResult = (Operator) 
                finder.conditionalUpdate(testOperator, findByIdentifier, fieldsForLookup, allFields, false);
        } catch (VocollectException e) {
            e.printStackTrace();
            assertFalse(true, "finder threw an exception");
        }

        // OK, we should have a copy of the element in the database
        // The DAO doesn't muck with the original
        assertEquals(expectedResult.getOperatorIdentifier(), 
                     testResult.getOperatorIdentifier(), "Retrieved the wrong element");
        
        // Verify that this will add the appropriate element
        
        Operator newOperator = new Operator();
        newOperator.setAssignedRegion(testResult.getAssignedRegion());
        newOperator.setCurrentRegion(testResult.getCurrentRegion());
        newOperator.setLastLocation(testResult.getLastLocation());
        newOperator.setOperatorIdentifier("testConditionalUpdate");
        newOperator.setPassword(testResult.getPassword());
        newOperator.setSignOffTime(testResult.getSignOffTime());
        newOperator.setSignOnTime(testResult.getSignOnTime());
        newOperator.setStatus(testResult.getStatus());
        
        testResult = null;
        
        try {
            testResult = (Operator) 
                finder.conditionalUpdate(newOperator, findByIdentifier, fieldsForLookup, allFields, false);
        } catch (VocollectException e) {
            e.printStackTrace();
            assertFalse(true, "finder threw an exception");
        }
        assertEquals(newOperator.getOperatorIdentifier(), 
                testResult.getOperatorIdentifier(), "Retrieved the wrong element");
        assertEquals(newOperator.getPassword(), 
                testResult.getPassword(), "Retrieved the wrong element");
        assertNotNull(testResult.getId(), "ID is still null in inserted object");
        assertEquals(newOperator.getId(), testResult.getId(), "IDs for the inserted element differ");
        
        // OK. Now change something, and go find & update it.
        newOperator.setPassword("22222");
        try {
            testResult = (Operator) 
                finder.conditionalUpdate(newOperator, findByIdentifier, fieldsForLookup, allFields, false);
        } catch (VocollectException e) {
            e.printStackTrace();
            assertFalse(true, "finder threw an exception");
        }
        assertEquals(newOperator.getOperatorIdentifier(), 
                testResult.getOperatorIdentifier(), "Retrieved the wrong element");
        assertEquals(newOperator.getPassword(), 
                testResult.getPassword(), "Retrieved the wrong element");
        assertNotNull(testResult.getId(), "ID is still null in inserted object");
        assertEquals(newOperator.getId(), testResult.getId(), "IDs for the inserted element differ");
        try {
            this.persistenceManager.delete(newOperator);
        } catch (BusinessRuleException e) {
            e.printStackTrace();
            assertFalse(true, "pm threw an exception");
        } catch (DataAccessException e) {
            e.printStackTrace();
            assertFalse(true, "pm threw an exception");
        }



    }

    /**
     * Test method for 
     * {@link com.vocollect.voicelink.core.importer.DBLookup#getCompositePart(java.lang.Object, java.lang.String)}.
     */
    @Test()
    public void testGetCompositePart() {
        MultiLevelTestObj mlt = new MultiLevelTestObj();
        
        Object retval = null;
        
        // Check to see that we can get an object from the DBLookup
        try {
            retval = DBLookup.getCompositePart(mlt, "operator");
        } catch (VocollectException e) {
            assertFalse(true, "DBLookup.getCompositePart threw an exception.");
        }
        try {
            @SuppressWarnings("unused")
            Operator junk = (Operator) retval;
        } catch (ClassCastException e) {
            assertFalse(true, "DBLookup.getCompositePart returned wrong type.");
        }
        
        // Check to see that we throw an exception when we go after a private member
        
        boolean thrown = false;
        try {
            retval = DBLookup.getCompositePart(mlt, "privateStringAccess");
        } catch (VocollectException e) {
            // expected
            thrown = true;
        }
        assertTrue(thrown, "DBLookup.getCompositePart failed to throw an exception when getting a private accessor.");
        
        // Check to see we throw an exception when we try to get a nonexistant member
        thrown = false;
        try {
            retval = DBLookup.getCompositePart(mlt, "nonexistantMember");
        } catch (VocollectException e) {
            // expected
            thrown = true;
        }
        assertTrue(thrown, 
                "DBLookup.getCompositePart failed to throw an exception when getting a nonexistant accessor.");
    }

    /**
     * Test method for 
     * {@link com.vocollect.voicelink.core.importer.DBLookup#setCompositePart(
     * java.lang.Object, java.lang.Object, java.lang.String)}.
     */
    @Test()
    public void testSetCompositePart() {
        MultiLevelTestObj mlt = new MultiLevelTestObj();
        
        Operator newGuy = new Operator();
        newGuy.setOperatorIdentifier("replacement");
        
        // Check to see that we can set an object from the DBLookup
        try {
            DBLookup.setCompositePart(mlt, newGuy, "operator");
        } catch (VocollectException e) {
            assertFalse(true, "DBLookup.setCompositePart threw an exception.");
        }
        
        Operator junk = mlt.getOperator();
        assertEquals(newGuy.getOperatorIdentifier(), junk.getOperatorIdentifier(), 
                        "Set didn't set the operator correctly.");
        
        // Check to see that we throw an exception when we go after a private member
        
        boolean thrown = false;
        try {
             DBLookup.setCompositePart(mlt, "a different value", "privateStringAccess");
        } catch (VocollectException e) {
            // expected
            thrown = true;
        }
        assertTrue(thrown, "DBLookup.setCompositePart failed to throw an exception when setting a private accessor.");
//        assertFalse(mlt.didStringChange(), "Accessing a private setter changed the value");
        // Check to see we throw an exception when we try to set a nonexistant member
        thrown = false;
        try {
            DBLookup.setCompositePart(mlt, null, "nonexistantMember");
        } catch (VocollectException e) {
            // expected
            thrown = true;
        }
        assertTrue(thrown, 
                "DBLookup.setCompositePart failed to throw an exception when getting a nonexistant accessor.");
    }

    /**
     * Test method for 
     * {@link com.vocollect.voicelink.core.importer.DBLookup#findGetter(java.lang.Object, java.lang.String)}.
     */
    @Test()
    public void testFindGetter() {
        MultiLevelTestObj mlt = new MultiLevelTestObj();
        
        Operator newGuy = new Operator();
        newGuy.setOperatorIdentifier("replacement");

        // Find an existing getter
        Method getter = DBLookup.findGetter(mlt, "operator", null);
        assertEquals("getOperator", getter.getName(), "Got wrong getter");
        
        // find a private getter
        getter = DBLookup.findGetter(mlt, "privateStringAccess", null);
        assertNull(getter, "Managed to get a private getter.");
        
        // find a non-existant getter
        getter = DBLookup.findGetter(mlt, "nonexistantGetter", null);
        assertNull(getter, "Managed to get a nonexistant getter.");
        
    }

    /**
     * Test method for 
     * {@link com.vocollect.voicelink.core.importer.DBLookup#findSetter(java.lang.Object, java.lang.String)}.
     */
    @Test()
    public void testFindSetter() {
        MultiLevelTestObj mlt = new MultiLevelTestObj();
        
        Operator newGuy = new Operator();
        newGuy.setOperatorIdentifier("replacement");
        
        Class<?>[] newGuyClassArray = new Class<?>[1];
        newGuyClassArray[0] = newGuy.getClass();
        
        // Find an existing setter
        Method setter = DBLookup.findSetter(mlt, "operator", newGuyClassArray);
        assertEquals("setOperator", setter.getName(), "Got wrong setter");
        
        // find a private setter
        setter = DBLookup.findSetter(mlt, "privateStringAccess", newGuyClassArray);
        assertNull(setter, "Managed to get a private setter.");
        
        // find a non-existant setter
        setter = DBLookup.findSetter(mlt, "nonexistantSetter", newGuyClassArray);
        assertNull(setter, "Managed to get a nonexistant setter.");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.DBLookup#findMethodByName(
     * java.lang.Object, java.lang.String)}.
     */
    @Test()
    public void testFindMethodByName() {
        MultiLevelTestObj mlt = new MultiLevelTestObj();
        
        Operator newGuy = new Operator();
        newGuy.setOperatorIdentifier("replacement");

        // Find an existing setter
        Method setter = DBLookup.findMethodByName(mlt, "getOperator", null);
        assertEquals("getOperator", setter.getName(), "Got wrong setter");
        
        // find a private setter
        setter = DBLookup.findMethodByName(mlt, "getPrivateStringAccess", null);
        assertNull(setter, "Managed to get a private setter.");
        
        // find a non-existant setter
        setter = DBLookup.findMethodByName(mlt, "nonexistantGetter", null);
        assertNull(setter, "Managed to get a nonexistant setter.");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.DBLookup#uppercaseFirstLetter(java.lang.String)}.
     */
    @Test()
    public void testUppercaseFirstLetter() {
        String lowerCase = "junkString";
        String expectedResult = "JunkString";
        String actualResult = DBLookup.uppercaseFirstLetter(lowerCase);
        assertEquals(expectedResult, actualResult, "Did not capitalize the first letter correctly.");
         
        lowerCase = "a";
        expectedResult = "A";
        actualResult = DBLookup.uppercaseFirstLetter(lowerCase);
        assertEquals(expectedResult, actualResult, "Did not capitalize the only letter correctly.");
        
        lowerCase = "";
        expectedResult = "";
        actualResult = DBLookup.uppercaseFirstLetter(lowerCase);
        assertEquals(expectedResult, actualResult, "Did not capitalize no letter string correctly.");
        
    }

    /**
     * Gets the value of assignmentManager.
     * @return the assignmentManager
     */
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * Sets the value of the assignmentManager.
     * @param assignmentManager the assignmentManager to set
     */
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * Gets the value of siteName.
     * @return the siteName
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * Sets the value of the siteName.
     * @param siteName the siteName to set
     */
    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

}

/**
 * This is a class that allows us to exercise the ability to get & set whole objects in another class. 
 * 
 * @author dgold
 *
 */
class MultiLevelTestObj {
    private Operator operator = new Operator();
    private Field another = new Field();
    private String privateStringAccess = "this should fail";
    
    /**
     * @return boolean
     */
    public boolean didStringChange() {
        return (0 != this.privateStringAccess.compareTo("this should fail"));
    }
    
    /**
     * Default constructor for test only.
     */
    MultiLevelTestObj() {
        this.setPrivateStringAccess("This is to stimulate failure");
        this.getPrivateStringAccess();
        operator.setOperatorIdentifier("original");
    }
    /**
     * Gets the value of another.
     * @return the another
     */
    public Field getAnother() {
        return another;
    }
    /**
     * Sets the value of the another.
     * @param another the another to set
     */
    public void setAnother(Field another) {
        this.another = another;
    }
    /**
     * Gets the value of operator.
     * @return the operator
     */
    public Operator getOperator() {
        return operator;
    }
    /**
     * Sets the value of the operator.
     * @param operator the operator to set
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }
    /**
     * Gets the value of privateStringAccess.
     * @return the privateStringAccess
     */
    private String getPrivateStringAccess() {
        return privateStringAccess;
    }
    /**
     * Sets the value of the privateStringAccess.
     * @param privateStringAccess the privateStringAccess to set
     */
    private void setPrivateStringAccess(String privateStringAccess) {
        this.privateStringAccess = privateStringAccess;
    }
    

}
