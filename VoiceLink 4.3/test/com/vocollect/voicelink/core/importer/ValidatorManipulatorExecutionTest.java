/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import java.util.Collection;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

/**
 * This class tests executing the Validators and Manipulators.
 * 
 * @author jtauberg
 */
public class ValidatorManipulatorExecutionTest {

    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

    /***
     * Test method for {@link com.vocollect.voicelink.core.importer.validators.
     * AlphaOnlyValidator#validate(java.lang.Object)}..
     */
    @Test(enabled = false)
    public void testValidatorExecution() {

        ValidationTestObject valObj = new ValidationTestObject();
        valObj.setDoubleData(12.43);
        valObj.setIntData(42);
        valObj.setStringData("Hi Mom. 1cheer ");
        valObj.setStringData2("");
        valObj.setDateStringData("20040145");
        valObj.setDateData(null);
        ValidationResult vr = new ValidationResult(valObj);

        assertNotNull(vr);
        try {
            assertFalse("Flawed data passed validation!", vr.validate());
        } catch (RuntimeException e) {
            fail("Should not throw an exception validating a real object with bad data.");
        }
        assertNotNull("The thing to validate is null", vr.getThingToValidate());
        assertNotNull(
                "The default import context was null, should be an empty string",
                vr.getObjectImportContext());
        assertTrue("The default import context wasn't an empty string", vr
                .getObjectImportContext().compareTo("") == 0);

        // Check that each of the four import fields failed.
        assertTrue(
                "stringData field had invalid data but did not result in errors.",
                vr.getErrorList().containsKey("stringData"));
        assertTrue(
                "intData field had invalid data but did not result in errors.",
                vr.getErrorList().containsKey("intData"));
        assertTrue(
                "doubleData field had invalid data but did not result in errors.",
                vr.getErrorList().containsKey("doubleData"));
        assertTrue(
                "DateStringData field had invalid data but did not result in errors.",
                vr.getErrorList().containsKey("dateStringData"));

        // Below are several ideas that didn't work
        // Object myObj = vr.getErrorList().get("stringData");
        // //assertTrue("Did not work", vr.getErrorList().containsValue("You
        // must enter a data length of 100 for stringData."));


        // Get list of errors for each field.
        Collection<String> arStringData = vr.getErrorList().get("stringData");
        Collection<String> arIntData = vr.getErrorList().get("intData");
        Collection<String> arDoubleData = vr.getErrorList().get("doubleData");
        Collection<String> arDateStringData = vr.getErrorList().get(
            "dateStringData");

        /*
         * Helper code below to list the errors returned for a given field int
         * i. for (i=0; i<arStringData.size();i++ ) {
         */

        // Check each stringData validation produced an error.
        // assertTrue("stringData did not contain a message for Data Length
        // failure.", ar.contains("You must enter a data length of 100 for
        // stringData."));
        assertTrue(
                "stringData did not contain a message for Alpha Only failure.",
                arStringData
                        .contains("stringData may consist only of alphabetic characters a-z and A-Z."));
        assertTrue(
                "stringData did not contain a message for Min Data Length failure.",
                arStringData
                        .contains("You must enter a data length of at least 16 for stringData."));
        assertTrue(
                "stringData did not contain a message for Max Data Length failure.",
                arStringData
                        .contains("The data length of stringData  can not exceed 12 characters."));
        assertTrue(
                "stringData did not contain a message for Disallowed Character failure.",
                arStringData
                        .contains("stringData may not contain any of the following characters: iM."));
        assertTrue(
                "stringData did not contain a message for Numeric failure.",
                arStringData
                        .contains("You must enter a numeric value for stringData."));
        assertTrue(
                "stringData did not contain a message for No Digits failure.",
                arStringData
                        .contains("You must enter a value for stringData that does not contain any digits."));

        // Check each intData validation produced an error.
        assertTrue(
                "intData did not contain a message for int failure.",
                arIntData
                        .contains("intData value must be between 38 and 41, current value is 42."));
        assertTrue(
                "intData did not contain a message for Between failure.",
                arIntData
                        .contains("Between error: intData value must be between 38.0 and 42.0, current value is 42."));
        assertTrue(
                "intData did not contain a message for Minimum failure.",
                arIntData
                        .contains("intData value must be a minimum of 43.0.  Current value is 42."));
        assertTrue(
                "intData did not contain a message for Maximum failure.",
                arIntData
                        .contains("intData value must be a maximum of 41.0.  Current value is 42."));
        assertTrue(
                "intData did not contain a message for LessOrEqual failure.",
                arIntData
                        .contains("intData value must be less than or equal to 41.0.  Current value is 42."));
        assertTrue(
                "intData did not contain a message for GreaterOrEqual failure.",
                arIntData
                        .contains("intData value must be breater than or equal to 43.0.  Current value is 42."));

        // Check each doubleData validation produced an error.
        assertTrue(
                "doubleData did not contain a message for Range failure.",
                arDoubleData
                        .contains("doubleData value must be in the range of 12.44 and "
                                + "241.152 inclusive, current value is 12.43."));
        assertTrue(
                "doubleData did not contain a message for LessThan failure.",
                arDoubleData
                        .contains("doubleData value must be less than 12.43.  Current value is 12.43."));
        assertTrue(
                "doubleData did not contain a message for GreaterThan failure.",
                arDoubleData
                        .contains("doubleData value must be greater than 12.43.  Current value is 12.43."));

        // Check each dateStringData validation produced an error.
        assertTrue(
                "dateStringData did not contain a message for Date Validation failure.",
                arDateStringData
                        .contains("dateStringData value must be a valid date in the format yyyyMMdd.  "
                            + "Current value is 20040145"));

        // TEST MANIPULATORS BELOW

        // Test that default manipulator set the field value to "Hi There."
        assertEquals("Default Manipulator did not set the default value.",
                "Hi There.", valObj.getStringData2());

        // Check that the Default date manipulator set the dateData field.
        assertNotNull(
                "Default Date Manipulator did not set the default date value.",
                valObj.getDateData());

        // Check that the Trim Manipulator trimmed the stringData field.
        assertEquals("Trim Manipulator did not trim the stringData field.",
                "Hi Mom. 1cheer", valObj.getStringData());
    }
}
