/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import java.util.Date;

/**
 * This class will be used in Validation tests to
 * stub out different types of data.
 * This will negate the need for the tests to be marked up in XML.
 *  A data type will be set with a value and the object
 *  will be passed to the validator.
 *  Note that the validator will need to have its field name set
 *  to the member that holds the data.
 *
 * @author vsubramani
 */
public class ListValidationTestObject {

    private int intData = 0;
    private double doubleData = 0.0;
    private String stringData = null;
    private char charData = ' ';
    private long longData = 0;
    private Date dateData = null;
    private String dateStringData = null;
    private String stringData2 = null;


    /**
     * Getter for the stringData2 property.
     * @return String value of the property
     */
    public String getStringData2() {
        return this.stringData2;
    }

    /**
     * Setter for the stringData2 property.
     * @param stringData2 the new stringData2 value
     */
    public void setStringData2(String stringData2) {
        this.stringData2 = stringData2;
    }

    /**
     * Getter for the dateStringData property.
     * @return String value of the property
     */
    public String getDateStringData() {
        return this.dateStringData;
    }

    /**
     * Setter for the dateStringData property.
     * @param dateStringData the new dateStringData value
     */
    public void setDateStringData(String dateStringData) {
        this.dateStringData = dateStringData;
    }


    /**
     * Getter for the charData property.
     * @return char value of the property
     */
    public char getCharData() {
        return this.charData;
    }

    /**
     * Setter for the charData property.
     * @param charData the new charData value
     */
    public void setCharData(char charData) {
        this.charData = charData;
    }

    /**
     * Getter for the doubleData property.
     * @return double value of the property
     */
    public double getDoubleData() {
        return this.doubleData;
    }

    /**
     * Setter for the doubleData property.
     * @param doubleData the new doubleData value
     */
    public void setDoubleData(double doubleData) {
        this.doubleData = doubleData;
    }

    /**
     * Getter for the data property.
     * @return String value of the property
     */
    public int getIntData() {
        return this.intData;
    }

    /**
     * Setter for the data property.
     * @param data the new data value
     */
    public void setIntData(int data) {
        this.intData = data;
    }


    /**
     * Getter for the longData property.
     * @return long value of the property
     */
    public long getLongData() {
        return this.longData;
    }


    /**
     * Setter for the longData property.
     * @param longData the new longData value
     */
    public void setLongData(long longData) {
        this.longData = longData;
    }


    /**
     * Getter for the stringData property.
     * @return String value of the property
     */
    public String getStringData() {
        return this.stringData;
    }


    /**
     * Setter for the stringData property.
     * @param stringData the new stringData value
     */
    public void setStringData(String stringData) {
        this.stringData = stringData;
    }


    /**
     * The ValidationTestObject.
     */
    public ListValidationTestObject() {
        // nothing to do
    }


    /**
     * Gets the value of dateData.
     * @return the dateData
     */
    public Date getDateData() {
        return dateData;
    }


    /**
     * Sets the value of the dateData.
     * @param dateData the dateData to set
     */
    public void setDateData(Date dateData) {
        this.dateData = dateData;
    }
}
