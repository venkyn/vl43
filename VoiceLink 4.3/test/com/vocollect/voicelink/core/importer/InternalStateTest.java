/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * @author astein
 */
public class InternalStateTest {

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeClass
    public void setUp() throws Exception {
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.InternalState
     * #InternalState(int, java.lang.String, com.vocollect.voicelink.core.importer.InternalState.FinalStateType)}.
     */
    @Test
    public final void testInternalStateIntStringFinalStateType() {
        InternalState successfulTestIntState = new InternalState(1,
                "Importer.Successful", InternalState.FinalStateType.Success);
        assertEquals(successfulTestIntState.getCode(), 1);
        assertEquals(successfulTestIntState.getDescription(),
                "Importer.Successful");
        assertEquals(successfulTestIntState.getTypeOfState(),
                InternalState.FinalStateType.Success);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.InternalState#InternalState(int, java.lang.String)}.
     */
    @Test
    public final void testInternalStateIntString() {
        InternalState configuredTestIntState = new InternalState(3,
                "Importer.Configured");
        assertEquals(configuredTestIntState.getCode(), 3);
        assertEquals(configuredTestIntState.getDescription(),
                "Importer.Configured");
        assertEquals(configuredTestIntState.getTypeOfState(),
                InternalState.FinalStateType.NotPermittedAsFinalState);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.InternalState#compareTo(java.lang.Object)}.
     */
    @Test
    public final void testCompareTo() {
        InternalState failedTestIntState = new InternalState(2,
                "Importer.Failed", InternalState.FinalStateType.Failed);
        InternalState failedTest2IntState = new InternalState(2,
                "Importer.FailedAgain", InternalState.FinalStateType.Failed);
        assertEquals(failedTestIntState.compareTo(failedTest2IntState), 0);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.InternalState#toString()}.
     */
    @Test
    public final void testToString() {
        InternalState configuredTestIntState = new InternalState(3,
                "Importer.Configured");
        assertEquals(configuredTestIntState.toString(), "Importer.Configured");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.InternalState#isFinalState()}.
     */
    @Test
    public final void testIsFinalState() {
        InternalState successfulTestIntState = new InternalState(1,
                "Importer.Successful", InternalState.FinalStateType.Success);
        InternalState configuredTestIntState = new InternalState(3,
                "Importer.Configured");
        assertFalse(configuredTestIntState.isFinalState());
        assertTrue(successfulTestIntState.isFinalState());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.InternalState#isFailedState()}.
     */
    @Test
    public final void testIsFailedState() {
        InternalState failedTestIntState = new InternalState(2,
                "Importer.Failed", InternalState.FinalStateType.Failed);
        InternalState successfulTestIntState = new InternalState(1,
                "Importer.Successful", InternalState.FinalStateType.Success);
        assertFalse(successfulTestIntState.isFailedState());
        assertTrue(failedTestIntState.isFailedState());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.InternalState#isSuccessState()}.
     */
    @Test
    public final void testIsSuccessState() {
        InternalState failedTestIntState = new InternalState(2,
                "Importer.Failed", InternalState.FinalStateType.Failed);
        InternalState successfulTestIntState = new InternalState(1,
                "Importer.Successful", InternalState.FinalStateType.Success);
        assertTrue(successfulTestIntState.isSuccessState());
        assertFalse(failedTestIntState.isSuccessState());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.InternalState#isImperfectState()}.
     */
    @Test
    public final void testIsImperfectState() {
        InternalState imperfectTestIntState = new InternalState(4,
                "Importer.Imperfect", InternalState.FinalStateType.Imperfect);
        InternalState failedTestIntState = new InternalState(2,
                "Importer.Failed", InternalState.FinalStateType.Failed);
        assertTrue(imperfectTestIntState.isImperfectState());
        assertFalse(failedTestIntState.isImperfectState());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.InternalState#isNoWorkState()}.
     */
    @Test
    public final void testIsNoWorkState() {
        InternalState noworkTestIntState = new InternalState(5,
                "Importer.NoWork", InternalState.FinalStateType.NoWork);
        InternalState failedTestIntState = new InternalState(2,
                "Importer.Failed", InternalState.FinalStateType.Failed);
        assertTrue(noworkTestIntState.isNoWorkState());
        assertFalse(failedTestIntState.isNoWorkState());
    }

}
