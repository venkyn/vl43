/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;
import com.vocollect.voicelink.core.model.Item;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * @author dgold
 */
public class CreateObjectTriggerImplTest {

    private String              openContext;

    private String              expectedResult;
    
    private String              expectedResultNoFields;   

    private String              expectedResultNoOpenTag;   
    
    private String              expectedFieldoutput;

    private String              fieldName;

    private String              objectName;

    private String              fieldData;

    private String              openTag;

    private String              closeTag;

    private FixedLengthFieldMap fields;

    private FixedLengthField    f;

    /**
     * @throws java.lang.Exception
     *             if unsuccessful
     */
    @BeforeClass()
    protected void setUp() throws Exception {
        openContext = "";
        fieldName = "field1";
        fieldData = "just text";
        objectName = "test";
        
        expectedResult = "<" + objectName + ">" + "<" + fieldName + ">"
                + fieldData + "</" + fieldName + ">" + "</" + objectName + ">";
        expectedResultNoFields = "<" + objectName + ">" 
        + "</" + objectName + ">";
        
        f = new FixedLengthField();
        f.setFieldName(fieldName);
        f.setFieldData(fieldData);
        f.setOpenTag("<" + fieldName + ">");
        f.setCloseTag("</" + fieldName + ">");
        expectedFieldoutput = f.getOpenTag() + f.getFieldData() + f.getCloseTag();
        fields = new FixedLengthFieldMap();
        fields.addField(f);

        openTag = "<" + objectName + ">";
        closeTag = "</" + objectName + ">";

        expectedResultNoOpenTag = expectedFieldoutput + closeTag;

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#CreateObjectTriggerImpl()}.
     */
    @Test()
    public void testCreateObjectTriggerImpl() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        assertNotNull("Constructor failed", c);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setUseAllFields()}.
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getUseAllFields()}.
     */
    @Test()
    public void testgetUseAllFields() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        assertTrue("default for useAllFields should be true", c.getUseAllFields());
        c.setUseAllFields(false);
        assertFalse("useAllFields should now be false", c.getUseAllFields());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl
     * #closeContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testCloseContext() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        c.setNewObjectName(objectName);
        c.setOpenTag(openTag);
        c.setCloseTag(closeTag);
        assertEquals("closeContext did not write the fields", expectedResultNoOpenTag, c
                .closeContext(openContext, fields));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl
     * #createObjectIfTriggered(com.vocollect.voicelink.core.importer.FieldMap, java.lang.String)}.
     */
    @Test()
    public void testCreateObjectIfTriggered() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        c.setUseAllFields(true);
        c.setNewObjectName(objectName);
        c.setOpenTag(openTag);
        c.setCloseTag(closeTag);
        assertEquals("Did not get the expected result", expectedResult, c
                .createObjectIfTriggered(fields, openContext));
        c.setUseAllFields(false);
        assertEquals("Did not get the expected result", expectedResultNoFields, c
                .createObjectIfTriggered(fields, openContext));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl
     * #detectTriggeringConditions(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testDetectTriggeringConditions() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        assertTrue("Trigger should always return true.", c
                .detectTriggeringConditions(fields));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getNewObjectName()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setNewObjectName(java.lang.String)}.
     */
    @Test()
    public void testGetNewObjectName() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        c.setNewObjectName(objectName);
        assertEquals("new object name was changed", objectName, c
                .getNewObjectName());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getOpenTag()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setOpenTag(java.lang.String)}.
     */
    @Test()
    public void testGetOpenTag() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        c.setOpenTag(openTag);
        assertEquals("open tag changed", openTag, c.getOpenTag());
        c.setNewObjectName(objectName);
        assertEquals("open tag changed", openTag, c.getOpenTag());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#isOpen()}.
     */
    @Test()
    public void testIsOpen() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        assertFalse("isOpen should initially return false ", c.isOpen());
        c.openContext(openContext, fields);
        assertTrue("isOpen should go true ", c.isOpen());
        c.setIsOpen(false);
        assertFalse("isOpen should return as set ", c.isOpen());

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#
     * openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testOpenContext() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        c.setUseAllFields(true);
        c.setNewObjectName(objectName);
        c.setOpenTag(openTag);
        c.setCloseTag(closeTag);

        assertEquals("Did not get the expected result from openContext",
                openTag, c.openContext(openContext, fields));
        
        c.setUseAllFields(false);
        assertEquals("Did not get the expected result", openTag, c
                .openContext(openContext, fields));

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getCloseTag()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setCloseTag(java.lang.String)}.
     */
    @Test()
    public void testGetCloseTag() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        c.setCloseTag(closeTag);
        assertEquals("close tag was changed", closeTag, c.getCloseTag());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setIsOpen(boolean)}.
     */
    @Test()
    public void testSetIsOpen() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        assertFalse("isOpen should initially be false false", c.isOpen());
        c.setIsOpen(true);
        assertTrue("isOpen should now return true", c.isOpen());
        c.setIsOpen(false);
        assertFalse("isOpen should now return false", c.isOpen());
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#writeFields(com.vocollect.voicelink.core.importer.FieldMap, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testWriteFields() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        c.setFieldMap(fields);
        String result = c.writeFields(fields, fields);  // should write all fields
        assertEquals("Field markup shoukd equal the writeFields markup", 
                this.expectedFieldoutput, result);
        FieldMap fm = new FieldMap();
        Field f1 = new Field();
        f1.setFieldName("Other Field");
        fm.addField(f1);
        c.setUseAllFields(false);
        result = c.writeFields(fm, fields);
        assertEquals("no field data should be present", "", result);
        result = c.writeFields(fields, fm);
        assertEquals("no field data should be present", "", result);
        
        
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getFieldMap()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setFieldMap(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testGetSetFieldMap() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        assertNotNull("Field map should not be null", c.getFieldMap());
        FieldMap fm = new FieldMap();
        c.setFieldMap(fm);
        assertNotNull("FieldMap should not be null now.", c.getFieldMap());
        assertEquals("Should be the same field map I passed in.", fm, c.getFieldMap());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#getUseAllFields()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl#setUseAllFields(boolean)}.
     */
    @Test()
    public void testGetSetUseAllFields() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();
        assertTrue("useAllFields should default to true.", c.getUseAllFields());
        c.setUseAllFields(false);
        assertFalse("useAllFields should be false now.", c.getUseAllFields());
        c.setUseAllFields(true);
        assertTrue("useAllFields should be true now.", c.getUseAllFields());
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.
     * CreateObjectTriggerImpl#isLookup()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.
     * CreateObjectTriggerImpl#setLookup(boolean)}.
     */
    @Test()
    public void testGetSetLookup() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();        
        assertFalse("Lookup should default to false", c.isLookup());
        c.setLookup(true);
        assertTrue("Lookup should be true now", c.isLookup());
        c.setLookup(false);
        assertFalse("Lookup should be false now", c.isLookup());
        
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.
     * CreateObjectTriggerImpl#lookupObject(Object, DBLookup) }.
     */
    @Test()
    public void testLookupObject() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();        
        assertFalse("Lookup should default to false", c.isLookup());
        Item anItem = new Item();
        Object retval = null;
        Item returnItem = null;
        try {
            retval = c.lookupObject(anItem, null);
        } catch (VocollectException e) {
            e.printStackTrace();
            fail("Threw an exception when doing a lookup, this trigger is not a lookup trigger.");
        }
        try {
            returnItem = (Item) retval;
        } catch (Exception e) {
            e.printStackTrace();
            fail("Conversion to Item threw an exception.");
        }
        assertEquals("Expecting the lookup to just return the same item we started with.", returnItem, anItem);
    }
    
}
