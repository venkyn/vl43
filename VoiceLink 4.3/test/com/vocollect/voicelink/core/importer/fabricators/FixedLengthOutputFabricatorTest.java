/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.fabricators;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

/**
 * 
 * Test class for OutputFabricator.
 *
 * @author Kalpna
 */

public class FixedLengthOutputFabricatorTest {
                
    private FixedLengthOutputFabricator oFabricator = null;
    
    private String mappingFileName = "export-field-mapping.xml";

    private String configFileName  = "test-input-fields-export.xml";
    
    private String                testDir   = null;

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeMethod
    public void setUp() throws Exception {
        oFabricator = new FixedLengthOutputFabricator();   
        if (System.getProperty("test.input.dir") != null) {
            testDir = System.getProperty("test.input.dir") + "/";
        } else {
            testDir = "./";
        }
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.fabricators.FixedLengthOutputFabricator.createRecord()'.
     * This method test for writting records to destination
     */
    @Test()
    public void testCreateRecord() {
        
        //fieldList to create the record
        FixedLengthFieldMap fieldListData = new FixedLengthFieldMap();
        
        //Create all the fields and add those to fieldList
        FixedLengthField testField = new FixedLengthField();
        testField.setFieldName("firstName");
        testField.setDataType("String");
        testField.setFieldData("abcb123456");                          
        fieldListData.addField(testField);
        
        FixedLengthField testField1 = new FixedLengthField();
        testField1.setFieldName("lastName");
        testField1.setDataType("String");
        testField1.setFieldData("abcdgfrtyo");
        fieldListData.addField(testField1);

        FixedLengthField testField2 = new FixedLengthField();
        testField2.setFieldName("age");
        testField2.setDataType("Integer");
        testField2.setFieldData("23");
        fieldListData.addField(testField2);
 
        //Set the fieldlist markup
        ExportFixedLengthFieldMap fieldList = new ExportFixedLengthFieldMap();
        oFabricator.setFieldList(fieldList);

        //Set the mapping and configuration this will call the fabricator.configure
        //so don't need to call it explicitly
        try {
            oFabricator.setConfigFileName(configFileName);
            oFabricator.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            assertTrue(false, "config threw exception" + e1.getMessage());
        }
        
        //Open the destination with output Adapter
        FileOutputAdapter fileOutputAdapter = null;
        try {            
            fileOutputAdapter = new FileOutputAdapter();
            fileOutputAdapter.setFileMode("writeUnique");
            fileOutputAdapter.setEncoding("UTF-8");
            fileOutputAdapter.setFileName("TestCreate");
            fileOutputAdapter.setFilenameExtension(".dat");
            fileOutputAdapter.setFilenamePrefix("OFab");
            fileOutputAdapter.setMoveToDirectory(testDir + "Export");
            fileOutputAdapter.setOutputDirectory(testDir + "OFabTemp");
            oFabricator.setOutputAdapter(fileOutputAdapter);  
            oFabricator.getOutputAdapter().openDestination(); 
        } catch (ConfigurationException e) {
            assertTrue(false, "configure threw exception: " + e.getMessage());
        } catch (VocollectException e) {
            assertTrue(false, "FileOutputAdapter constructor threw exception"
                    + e.getMessage());
        }
               
        // Check config
        assertNotNull(oFabricator.getOutputAdapter());
        assertFalse(oFabricator.getFieldList().isEmpty(), "Empty field list");
        assertFalse(oFabricator.getRecordLength() <= 0, "Record length <= 0");
        
        //Create record and close on success
        try {
            oFabricator.createRecord(fieldListData);
            oFabricator.closeOnSuccess();           
        } catch (VocollectException e) {
            assertTrue(false, e.getMessage());
        }
        
    }
    
        
    /**
     * Test method for 'com.vocollect.voicelink.core.importer.fabricators.FixedLengthOutputFabricator.createRecord()'.
     * This method test for appending records to the destination
     */
    @Test()
    public void testCreateRecord1() {
        
        //fieldList to create the record
        FixedLengthFieldMap fieldListData = new FixedLengthFieldMap();
        
        //Create all the fields and add those to fieldList
        FixedLengthField testField = new FixedLengthField();
        testField.setFieldName("firstName");
        testField.setDataType("String");
        testField.setFieldData("XASDFASFAS");                          
        fieldListData.addField(testField);
        
        FixedLengthField testField1 = new FixedLengthField();
        testField1.setFieldName("lastName");
        testField1.setDataType("String");
        testField1.setFieldData("XYZQPDABCD");
        fieldListData.addField(testField1);

        FixedLengthField testField2 = new FixedLengthField();
        testField2.setFieldName("age");
        testField2.setDataType("Integer");
        testField2.setFieldData("23");
        fieldListData.addField(testField2);
 
        //Set the fieldlist markup
        ExportFixedLengthFieldMap fieldList = new ExportFixedLengthFieldMap();
        oFabricator.setFieldList(fieldList);

        //Set the mapping and configuration this will call the fabricator.configure
        //so don't need to call it explicitly
        try {
            oFabricator.setConfigFileName(configFileName);
            oFabricator.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            assertTrue(false, "config threw exception" + e1.getMessage());
        }
        
        //Open the destination with output Adapter
        FileOutputAdapter fileOutputAdapter = null;
        try {            
            fileOutputAdapter = new FileOutputAdapter();
            fileOutputAdapter.setFileMode("append");
            fileOutputAdapter.setEncoding("UTF-8");
            fileOutputAdapter.setFileName("TestAppend");
            fileOutputAdapter.setFilenameExtension(".dat");
            fileOutputAdapter.setFilenamePrefix("OFab");
            fileOutputAdapter.setMoveToDirectory(testDir + "Export");
            fileOutputAdapter.setOutputDirectory(testDir + "OFabTemp");
            oFabricator.setOutputAdapter(fileOutputAdapter);  
            oFabricator.getOutputAdapter().openDestination(); 
        } catch (ConfigurationException e) {
            assertTrue(false, "configure threw exception: " + e.getMessage());
        } catch (VocollectException e) {
            assertTrue(false, "FileOutputAdapter constructor threw exception"
                    + e.getMessage());
        }
               
        // Check config
        assertNotNull(oFabricator.getOutputAdapter());
        assertFalse(oFabricator.getFieldList().isEmpty(), "Empty field list");
        assertFalse(oFabricator.getRecordLength() <= 0, "Record length <= 0");
        
        //Create record and close on success
        try {
            oFabricator.createRecord(fieldListData);
            oFabricator.closeOnSuccess();
            
        } catch (VocollectException e) {
            assertTrue(false, e.getMessage());
        }
                
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.fabricators.FixedLengthOutputFabricator.createRecord()'.
     * This method test for logging is sufficient for recordlengthException when try to write the
     * record to the destination
     */
    @Test()
    public void testCreateRecord2() {
        
        //fieldList to create the record
        FixedLengthFieldMap fieldListData = new FixedLengthFieldMap();
        
        //Create all the fields and add those to fieldList
        FixedLengthField testField = new FixedLengthField();
        testField.setFieldName("firstName");
        testField.setDataType("String");
        testField.setFieldData("Xasd123456");                          
        fieldListData.addField(testField);
        
        FixedLengthField testField1 = new FixedLengthField();
        testField1.setFieldName("lastName");
        testField1.setDataType("String");
        testField1.setFieldData("XYZQPDABCD");
        fieldListData.addField(testField1);

        FixedLengthField testField2 = new FixedLengthField();
        testField2.setFieldName("age");
        testField2.setDataType("Integer");
        testField2.setFieldData("23123");
        fieldListData.addField(testField2);
 
        //Set the fieldlist
        ExportFixedLengthFieldMap fieldList = new ExportFixedLengthFieldMap();
        oFabricator.setFieldList(fieldList);

        //Set the mapping and configuration this will call the fabricator.configure
        //so don't need to call it explicitly
        try {
            oFabricator.setConfigFileName(configFileName);
            oFabricator.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            assertTrue(false, "config threw exception" + e1.getMessage());
        }
        
        //Open the destination with output Adapter
        FileOutputAdapter fileOutputAdapter = null;
        try {            
            fileOutputAdapter = new FileOutputAdapter();
            fileOutputAdapter.setFileMode("writeUnique");
            fileOutputAdapter.setEncoding("UTF-8");
            fileOutputAdapter.setFileName("TestExceptionLogging");
            fileOutputAdapter.setFilenameExtension(".dat");
            fileOutputAdapter.setFilenamePrefix("OFab");
            fileOutputAdapter.setMoveToDirectory(testDir + "Export");
            fileOutputAdapter.setOutputDirectory(testDir + "OFabTemp");
            oFabricator.setOutputAdapter(fileOutputAdapter);  
            oFabricator.getOutputAdapter().openDestination(); 
        } catch (ConfigurationException e) {
            assertTrue(false, "configure threw exception: " + e.getMessage());
        } catch (VocollectException e) {
            assertTrue(false, "FileOutputAdapter constructor threw exception"
                    + e.getMessage());
        }
               
        // Check config
        assertNotNull(oFabricator.getOutputAdapter());
        assertFalse(oFabricator.getFieldList().isEmpty(), "Empty field list");
        assertFalse(oFabricator.getRecordLength() <= 0, "Record length <= 0");
        
        //Create record and close on success
        try {
            oFabricator.createRecord(fieldListData);
            oFabricator.closeOnSuccess();
            
        } catch (VocollectException e) {
            assertTrue(true, e.getMessage());
        }
        
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.fabricators.FixedLengthOutputFabricator.createRecord()'.
     * This method test if the write can handle the multibyte characters
     */
    @Test()
    public void testCreateRecord3() {
        
        //fieldList to create the record
        FixedLengthFieldMap fieldListData = new FixedLengthFieldMap();
        
        //Create all the fields and add those to fieldList
        FixedLengthField testField = new FixedLengthField();
        testField.setFieldName("firstName");
        testField.setDataType("String");
        testField.setFieldData(
            "\u092a\u092a\u092a\u092a\u092a\u092a\u092a\u092a\u092a\u092a");                          
        fieldListData.addField(testField);
        
        FixedLengthField testField2 = new FixedLengthField();
        testField2.setFieldName("age");
        testField2.setDataType("Integer");
        testField2.setFieldData("23");
        fieldListData.addField(testField2);
 
        //Set the fieldlist
        ExportFixedLengthFieldMap fieldList = new ExportFixedLengthFieldMap();
        oFabricator.setFieldList(fieldList);

        //Set the mapping and configuration this will call the fabricator.configure
        //so don't need to call it explicitly
        try {
            oFabricator.setConfigFileName(configFileName);
            oFabricator.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            assertTrue(false, "config threw exception" + e1.getMessage());
        }
        
        //Open the destination with output Adapter
        FileOutputAdapter fileOutputAdapter = null;
        
        try {            
            fileOutputAdapter = new FileOutputAdapter();
            fileOutputAdapter.setFileMode("append");
            fileOutputAdapter.setEncoding("UTF-8");
            fileOutputAdapter.setFileName("TestMultiByte");
            fileOutputAdapter.setFilenameExtension(".dat");
            fileOutputAdapter.setFilenamePrefix("OFab");
            //fileOutputAdapter.setMoveToDirectory("Export");
            fileOutputAdapter.setOutputDirectory(testDir + "OFabTemp");
            oFabricator.setOutputAdapter(fileOutputAdapter);  
            oFabricator.getOutputAdapter().openDestination(); 
        } catch (ConfigurationException e) {
            assertTrue(false, "configure threw exception: " + e.getMessage());
        } catch (VocollectException e) {
            assertTrue(false, "FileOutputAdapter constructor threw exception"
                    + e.getMessage());
        }
               
        // Check config
        assertNotNull(oFabricator.getOutputAdapter());
        assertFalse(oFabricator.getFieldList().isEmpty(), "Empty field list");
        assertFalse(oFabricator.getRecordLength() <= 0, "Record length <= 0");
        
        //Create record and close on success
        try {
            oFabricator.createRecord(fieldListData);
            oFabricator.closeOnSuccess();
            
        } catch (VocollectException e) {
            assertTrue(true, e.getMessage());
        }
        
        String testString = "\u092a\u092a\u092a\u092a\u092a\u092a\u092a\u092a\u092a\u092a" + "23";
        byte[] testByte = null;
        
        try {
            //ISO-8859-1
            testByte = testString.getBytes("UTF-8");    
        } catch (UnsupportedEncodingException e) {
            assertTrue(false, "Unsupported coding exception"
                + e.getMessage());
        }
        
        //Read the file just created
        File f = new File(testDir + "\\OFabTemp\\TestMultiByte");
        StringBuffer buffer = new StringBuffer();
        
        try {
            FileInputStream fis = new FileInputStream(f);
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            Reader in = new BufferedReader(isr);
            int ch;        
            while ((ch = in.read()) > -1) {
               buffer.append((char) ch);
            }
            in.close();  
            //Verify bytes
            byte[] verifyByte = buffer.toString().getBytes("UTF-8");        
            for (int i = 0; i < verifyByte.length; i++) {
               if ((verifyByte[i] == 13 || verifyByte[i] == 10)) {
               //Do nothing
               } else if (!(verifyByte[i] == testByte[i])) {
                 assertTrue(false, "Inconsistent bytes found");             
               }
            }
        } catch (IOException e) {
            assertTrue(false, "Exception caught in file reading" + e.getMessage());     
        }

        assertTrue(f.delete(), "Test for multibyte character write is successful"); 
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.fabricators.FixedLengthOutputFabricator.configure()'.
     */
    @Test()
    public void testConfigure() {
        try {
            oFabricator.setMappingFileName(mappingFileName);
            oFabricator.setConfigFileName(configFileName);
            oFabricator.configure();
        } catch (ConfigurationException e) {
            assertTrue(false, "configure threw exception: " + e.getMessage());
        }
        assertFalse(oFabricator.getFieldList().isEmpty() , "Empty field list");      
    }

    /**
     * Test method for 
     * 'com.vocollect.voicelink.core.importer.fabricators.FixedLengthOutputFabricator.getRecordLength()'.
     * Test method for 
     * 'com.vocollect.voicelink.core.importer.fabricators.FixedLengthOutputFabricator.setRecordLength()'.
     */
    @Test()
    public void testGetSetRecordLength() {

    }

}
