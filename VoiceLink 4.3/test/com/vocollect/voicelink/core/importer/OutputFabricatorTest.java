/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter;
import com.vocollect.voicelink.core.importer.fabricators.FixedLengthOutputFabricator;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertTrue;


/**
 * 
 * Test class for OutputFabricator.
 *
 * @author Kalpna
 */

public class OutputFabricatorTest {

    
    private OutputFabricator oFabricator = null;
    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeMethod
    public void setUp() throws Exception {
        oFabricator = new FixedLengthOutputFabricator();        
    }
    
    /**
     * Test method for 'com.vocollect.voicelink.core.importer.OutputFabricator.close()'.
     */
    @Test()
    public void testClose() {
        oFabricator.close();
        if (oFabricator.getOutputAdapter() != null) {
            assertFalse(oFabricator.getOutputAdapter().isOpen(), "Connection should be closed");
        }
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.OutputFabricator.closeOnSuccess()'.
     */
    @Test()
    public void testCloseOnSuccess() {
        try {            
            FileOutputAdapter fileOutputAdapter = new FileOutputAdapter();
            fileOutputAdapter.setFileName("TestFile");
            fileOutputAdapter.setFilenameExtension(".dat");
            fileOutputAdapter.setFilenamePrefix("OFab_");
            fileOutputAdapter.setMoveToDirectory("Export");
            fileOutputAdapter.setOutputDirectory("OFab_Temp");
            fileOutputAdapter.setEncoding("UTF-8");
            oFabricator.setOutputAdapter(fileOutputAdapter);    
            oFabricator.getOutputAdapter().openDestination();
            oFabricator.closeOnSuccess();
            assertFalse(oFabricator.getOutputAdapter().isOpen(), "Connection should be closed");
        } catch (ConfigurationException e) {
            e.printStackTrace();
        } catch (VocollectException e) {
            e.printStackTrace();
        }

    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.OutputFabricator.getOutputAdapter()'.
     */
    @Test()
    public void testGetSetOutputAdapter() {
        try {
            OutputAdapter outputAdapter = new FileOutputAdapter();
            oFabricator.setOutputAdapter(outputAdapter);
            assertTrue(
                    "Setting source adapter for DataSourceParser did not work.",
                    oFabricator.getOutputAdapter().getCurrentState().equals(
                            OutputAdapter.INITIALIZEDSTATE));
        } catch (VocollectException e) {
            e.printStackTrace();
        }

    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.OutputFabricator.getFieldList()'.
     * Test method for 'com.vocollect.voicelink.core.importer.OutputFabricator.setFieldList()'.
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testGetSetFieldList() {
        FieldMap fm = new FixedLengthFieldMap();
        oFabricator.setFieldList(fm);
        assertNotNull("Setting the FieldList did not work.", oFabricator
                .getFieldList());

    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.OutputFabricator.getConfigFileName()'.
     * Test method for 'com.vocollect.voicelink.core.importer.OutputFabricator.setConfigFileName()'.
     */
    @Test()
    public void testGetSetConfigFileName() {
        try {
            oFabricator.setConfigFileName("theconfigfilename");
            assertTrue(
                    "Setting configfilename in OutputFabricator did not work.",
                    oFabricator.getConfigFileName().equals("theconfigfilename"));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.OutputFabricator#getDescription()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.OutputFabricator#setDescription(java.lang.String)}.
     */
    @Test
    public final void testGetSetDescription() {
        oFabricator.setDescription("description for parser object");
        assertTrue(
                "Setting the description in the DataSourceParser did not work.",
                oFabricator.getDescription().equals("description for parser object"));
    }


    /**
     * Test method for 'com.vocollect.voicelink.core.importer.OutputFabricator.getMappingFileName()'.
     */
    @Test()
    public void testGetSetMappingFileName() {
        try {
            oFabricator.setMappingFileName("themappingfile");
            assertTrue(
                    "Setting the mapping file in the OutputFabricator did not work.",
                    oFabricator.getMappingFileName().equals("themappingfile"));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

}
