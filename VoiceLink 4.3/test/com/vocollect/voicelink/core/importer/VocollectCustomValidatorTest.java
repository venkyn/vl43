/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.voicelink.core.importer.validators.BetweenValidator;
import com.vocollect.voicelink.core.importer.validators.NumericValidator;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * @author astein
 */
public class VocollectCustomValidatorTest {

    private VocollectCustomValidator vocValidator = null;

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeClass
    public void setUp() throws Exception {
        vocValidator = new NumericValidator();
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.VocollectCustomValidator#setInvertResult(boolean)}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.VocollectCustomValidator#getInvertResult()}.
     */
    @Test()
    public void testSetInvertResult() {
        assertEquals("Default value for InvertResult is false.", false,
            vocValidator.getInvertResult());
        // set invertResult to true
        vocValidator.setInvertResult(true);
        // ask for invertResult and make sure it is true
        assertEquals("Set a invertResult to true and get the same value back.", true,
            vocValidator.getInvertResult());
        // set invertResult to true
        vocValidator.setInvertResult(false);
        // ask for invertResult and make sure it is true
        assertEquals("Set a invertResult to true and get the same value back.", false,
            vocValidator.getInvertResult());

    }

    /**
     * This method test for the inversion of validator.
     * When InvertResult is set to true validator will add error when validation is 
     * successful
    */
    @Test()
    public void testInversionResult() {
        BetweenValidator testObj = new BetweenValidator();
        testObj.setInvertResult(true);
        ValidationTestObject valObj = new ValidationTestObject();
        testObj.setFieldName("intData");
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        testObj.setValidatorContext(vContext);

        // set min to -31 and max to -29 and number to validate at -30.
        testObj.setMin(-31.);
        testObj.setMax(-29.);
        valObj.setIntData(-30);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertTrue("Valid data did not triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Out of range above... Set value to validate at 12 (min and max still
        // same).
        valObj.setIntData(12);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("High Out of bounds data did trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Out of range below... Set value to validate at -212 (min and max
        // still same).
        valObj.setIntData(-212);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("Low Out of bounds data did trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // on upper boundary... Set value to validate at -29 = max.
        valObj.setIntData(-29);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("Valid data = max did trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));

        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // on lower boundary... Set value to validate at -31 = min.
        valObj.setIntData(-31);
        try {
            // try to validate it
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid out of bounds data.");
        }
        assertFalse("Valid data = min did trigger errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey("intData"));
        
    }
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.VocollectCustomValidator#isStringNumeric()}.
     */
    @Test
    public final void testIsStringNumeric() {
        assertTrue(
                "isValidNumber method is not validating correctly: 1234 is a number.",
                vocValidator.isStringNumeric("1234"));
        assertFalse(
                "isValidNumber method is not validating correctly: 12A34 is not a number.",
                vocValidator.isStringNumeric("12A34"));
        assertTrue(
                "isValidNumber method is not validating correctly: 1234.4321 is a number.",
                vocValidator.isStringNumeric("1234.4321"));
        assertFalse(
                "isValidNumber method is not validating correctly: 1234.234.12 is not a number.",
                vocValidator.isStringNumeric("1234.234.12"));
    }

}
