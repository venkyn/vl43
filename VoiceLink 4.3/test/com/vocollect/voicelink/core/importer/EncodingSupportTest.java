/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

/**
 * @author KalpnaT
 */
public class EncodingSupportTest {

    private String myUTF8Encoding = "UTF-8";
    private String myUSASCIIEncoding = "US-ASCII";
    private String myISO88591Encoding = "ISO-8859-1";
    private String defaultEncoding = "UTF-8";
    private String unsupportedEncoding = "JUNK_ENCODING";
    private char encodingBOM = '\uFEFF';   
    
    /**
     * Validate the list of encoding supported by VoiceLink imports.
     */
   @Test() 
   public void testIsEncodingSupported() {
       
       assertTrue(EncodingSupport.isEncodingSupported(myUTF8Encoding), "EncodingSupported " + myUTF8Encoding);
       assertTrue(EncodingSupport.isEncodingSupported(myUSASCIIEncoding), 
                  "EncodingSupported " + myUSASCIIEncoding);
       assertTrue(EncodingSupport.isEncodingSupported(myISO88591Encoding), 
                  "EncodingSupported " + myISO88591Encoding);
       assertFalse(EncodingSupport.isEncodingSupported(unsupportedEncoding), 
           "EncodingNotSupported " + unsupportedEncoding);
       assertTrue(EncodingSupport.isEncodingSupported(defaultEncoding), "Default Encoding " + defaultEncoding);
       assertTrue(EncodingSupport.BOM == encodingBOM);
     
   }

}
