/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.voicelink.core.importer.CastorConfiguration;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * Test case.
 * @author dgold
 *
 */
public class FieldChangeLookupTriggerTest {
    
    private Field  nonKeyField = null;
    private Field  keyField    = null;
    private String keyFieldName;
    private String nonKeyFieldName;
    
    /**
     * Set up the field passed in with the data passed in. Just to clean up the setup code.
     * @param fld field.
     * @param fieldName as it says
     * @param fieldData as it says
     * @return the filled in field.
     */
    private Field setupField(Field fld, String fieldName, String fieldData) {
        fld.setFieldName(fieldName);
        String tmpFieldData = fieldData;
        if (0 == fieldData.compareTo("")) {
            tmpFieldData = fieldName + ".data";
        }
        fld.setFieldData(tmpFieldData);
        fld.setOpenTag("<" + fieldName + ">");
        fld.setCloseTag("</" + fieldName + ">");
        fld.setFieldData(tmpFieldData);
        fld.setKeyField(true);
        return fld;
    }
 
    /**
     * @throws java.lang.Exception just because
     */
    @BeforeClass()
    protected void setUp() throws Exception {
        nonKeyField = new Field();
        nonKeyFieldName = "nonKeyField";
        keyFieldName = "keyField";
        setupField(nonKeyField, nonKeyFieldName, "");
        nonKeyField.setKeyField(false);
        
        keyField = new Field();
        keyFieldName = "keyField";
        setupField(keyField, keyFieldName, "");
        
//      org.exolab.castor.tools.MappingTool mapper = new MappingTool();
//      mapper.addClass( FieldChangeLookupTrigger.class);
//      
//      mapper.write(new FileWriter("C:/temp/FieldChangeLookupTrigger.mapping.xml"));


    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeLookupTrigger#
     * detectTriggeringConditions(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @SuppressWarnings("unchecked")
    @Test()
    public void testDetectTriggeringConditions() {
        FieldChangeLookupTrigger fclt = new FieldChangeLookupTrigger();
        fclt.addField(nonKeyField);
        fclt.addField(keyField);
        Field extraField = setupField(new Field(), keyFieldName, "");
        extraField.setFieldData("one");
        FieldMap temp = new FieldMap();
        temp.addField(extraField);
        assertTrue(fclt.detectTriggeringConditions(temp), "triggers failed");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeLookupTrigger#
     * addField(com.vocollect.voicelink.core.importer.Field)}.
     */
    @Test()
    public void testAddField() {
        FieldChangeLookupTrigger fclt = new FieldChangeLookupTrigger();
        fclt.addField(nonKeyField);
        // there should be one field
        assertEquals(fclt.getFieldMap().size(), 1, "There should be one field");
        // there should be no key fields
        assertEquals(fclt.getTriggeringFields().size(), 0, "There shoupld be no triggering fields.");

        fclt.addField(keyField);
        // There should be 2 fields
        assertEquals(fclt.getFieldMap().size(), 2, "There should be two fields");

        // There should be one triggering field
        assertEquals(fclt.getTriggeringFields().size(), 1, "There shoupld be one triggering field.");

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeLookupTrigger#reset()}.
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testReset() {
        FieldChangeLookupTrigger fclt = new FieldChangeLookupTrigger();
        fclt.addField(nonKeyField);
        fclt.addField(keyField);
        Field extraField = setupField(new Field(), keyFieldName, "");
        extraField.setFieldData("one");
        FieldMap temp = new FieldMap();
        temp.addField(extraField);
        assertTrue(fclt.detectTriggeringConditions(temp), "triggers failed");
        fclt.reset();
        assertTrue(fclt.detectTriggeringConditions(temp), "triggers failed");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeLookupTrigger#
     * FieldChangeLookupTrigger()}.
     */
    @Test()
    public void testFieldChangeLookupTrigger() {
        FieldChangeLookupTrigger fclt = new FieldChangeLookupTrigger();
        assertNotNull(fclt, "constructor yielded null");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeLookupTrigger#
     * getTriggeringFields()}.
     */
    @Test()
    public void testGetTriggeringFields() {
        FieldChangeLookupTrigger fclt = new FieldChangeLookupTrigger();
        fclt.addField(nonKeyField);
        // there should be one field
        assertEquals(fclt.getFieldMap().size(), 1, "There should be one field");
        // there should be no key fields
        assertEquals(fclt.getTriggeringFields().size(), 0, "There shoupld be no triggering fields.");

        fclt.addField(keyField);
        // There should be 2 fields
        assertEquals(fclt.getFieldMap().size(), 2, "There should be two fields");

        // There should be one triggering field
        assertEquals(fclt.getTriggeringFields().size(), 1, "There shoupld be one triggering field.");
        assertNotNull(fclt.getTriggeringFields().get(keyField.getFieldName()), "Couldn't retrieve trigger field");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeLookupTrigger#
     * addTriggeringField(com.vocollect.voicelink.core.importer.Field)}.
     */
    @Test()
    public void testAddTriggeringField() {
        FieldChangeLookupTrigger fclt = new FieldChangeLookupTrigger();
        fclt.addField(nonKeyField);
        // there should be one field
        assertEquals(fclt.getFieldMap().size(), 1, "There should be one field");
        // there should be no key fields
        assertEquals(fclt.getTriggeringFields().size(), 0, "There shoupld be no triggering fields.");

        fclt.addTriggeringField(keyField);
        // There should be 1 fields
        assertEquals(fclt.getFieldMap().size(), 1, "There should be one fields");

        // There should be one triggering field
        assertEquals(fclt.getTriggeringFields().size(), 1, "There shoupld be one triggering field.");
        assertNotNull(fclt.getTriggeringFields().get(keyField.getFieldName()), "Couldn't retrieve trigger field");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeLookupTrigger#
     * setTriggeringFields(java.util.HashMap)}.
     */
    @Test()
    public void testSetTriggeringFields() {
//        fail("Not yet implemented");
    }
    
    /**
     * 
     */
    @Test()
    public void testConfigCreation() {
        CastorConfiguration cc = new CastorConfiguration("import-definition.xml", "baseFCLookupTrigger.xml");
        try {
            cc.configure();
        } catch (ConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            fail("configuration failed");
        }
    }

}
