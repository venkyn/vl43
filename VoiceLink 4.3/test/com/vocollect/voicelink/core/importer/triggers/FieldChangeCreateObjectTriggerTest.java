/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.voicelink.core.importer.CastorConfiguration;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import java.util.HashMap;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;


/**
 * @author dgold
 */
public class FieldChangeCreateObjectTriggerTest {

    private String                  field1Name;

    private String                  field2Name;

    private String                  field1Data;

    private String                  field1NewData;

    private String                  field2Data;

    private String                  field1OpenTag;

    private String                  field1CloseTag;

    private String                  field2OpenTag;

    private String                  field2CloseTag;

    private String                  openContext;

    private String                  openTag;

    private String                  closeTag;

    private String                  objectName;

    private String                  expectedOpenFieldlessContext;

    private String                  expectedClosedFieldlessContext;

    private String                  expectedClosedAllFieldContext;
    
    private String                  expectedFieldoutput;
    
//    private String                  expectedResult;

    private FixedLengthField        field1;

    private FixedLengthField        field2;

    private FixedLengthFieldMap     fields;

    private FieldMap openContextFields;

    private FieldMap closeContextFields;

    private HashMap<String, String> triggers;

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @SuppressWarnings("unchecked")
    @BeforeMethod()
    protected void setUp() throws Exception {
        this.field1Name = "field1";
        this.field2Name = "field2";
        this.field1Data = "original";
        this.field1NewData = "changed";
        this.field2Data = "other";
        this.field1OpenTag = "<" + field1Name + ">";
        this.field1CloseTag = "</" + field1Name + ">";
        this.field2OpenTag = "<" + field2Name + ">";
        this.field2CloseTag = "</" + field2Name + ">";
        
        this.field1 = new FixedLengthField();
        this.field1.setFieldName(field1Name);
        this.field1.setFieldData(field1Data);
        this.field1.setOpenTag(field1OpenTag);
        this.field1.setCloseTag(field1CloseTag);
        this.field1.setWriteOnOpenContext(true);

        this.field2 = new FixedLengthField();
        this.field2.setFieldName(field2Name);
        this.field2.setFieldData(field2Data);
        this.field2.setOpenTag(field2OpenTag);
        this.field2.setCloseTag(field2CloseTag);
        this.field2.setWriteOnOpenContext(false);
        
        this.fields = new FixedLengthFieldMap();
        this.fields.addField(field1);
        this.fields.addField(field2);
        assertEquals("Dropped an entry from the field map", 2, fields.size());

        this.triggers = new HashMap<String, String>();
        this.triggers.put(field1.getFieldName(), null);
        this.objectName = "testObj";
        this.openContext = "";
        this.openTag = "<" + objectName + ">";
        this.closeTag = "</" + objectName + ">";

        this.expectedOpenFieldlessContext = openTag;
        this.expectedClosedFieldlessContext = openTag + closeTag;
        
        this.expectedFieldoutput = field1.getOpenTag()
                                    + field1.getFieldData() + field1.getCloseTag()
                                    + field2.getOpenTag() + field2.getFieldData()
                                    + field2.getCloseTag();

        this.expectedClosedAllFieldContext = openTag + field1.getOpenTag()
                + field1.getFieldData() + field1.getCloseTag()
                + field2.getOpenTag() + field2.getFieldData()
                + field2.getCloseTag() + closeTag;
        this.openContextFields = new FieldMap();
        this.openContextFields.put(field1.getFieldName(), field1);

        this.closeContextFields = new FieldMap();
        this.closeContextFields.put(field2.getFieldName(), field2);
        
//        org.exolab.castor.tools.MappingTool mapper = new MappingTool();
//        mapper.addClass(FieldChangeCreateObjectTrigger.class);
//        
//        mapper.write(new FileWriter("data/importvalidator.mapping.xml"));

        
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#FieldChangeCreateObjectTrigger()}.
     */
    @Test()
    public void testFieldChangeCreateObjectTrigger() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        assertNotNull("Construictor failed", fc);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger
     * #detectTriggeringConditions(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @SuppressWarnings("unchecked")
    @Test()
    public void testDetectTriggeringConditions() {
        boolean result = false;
        assertEquals("Dropped an entry from the field map", 2, fields.size());
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        try {
            assertFalse("Detect should initially be false", fc
                    .detectTriggeringConditions(fields));
        } catch (RuntimeException e) {
           // should throw a runtime (null pointer) -can't use the trigger without list of fields.
            result = true;
        }
        assertTrue("Using the trigger without setting the triggering fields should fail", result);
        HashMap<String, String> tempTriggers = new HashMap<String, String>();
        fc.setTriggeringFields(tempTriggers);
        assertFalse("Detect should be false with empty trigger list", fc
                .detectTriggeringConditions(fields));
        tempTriggers.put(field1.getFieldName(), field1.getFieldData());
        assertFalse("Detect should not trigger with same data", fc
                .detectTriggeringConditions(fields));
        field1.setFieldData(field1NewData);
        fields.put(field1.getFieldName(), field1);
        assertEquals("Dropped an entry from the field map", 2, fields.size());
        assertTrue("Detect should trigger with new data", fc
                .detectTriggeringConditions(fields));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger
     * #openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testOpenContext() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        fc.setTriggeringFields(triggers);
        fc.setOpenTag(openTag);
        fc.setCloseTag(closeTag);
        String newContext = fc.openContext(openContext, fields);
        assertEquals("Open context gave unexpected result",
                expectedOpenFieldlessContext, newContext);

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger
     * #closeContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testCloseContext() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        fc.setTriggeringFields(triggers);
        fc.setOpenTag(openTag);
        fc.setCloseTag(closeTag);
        String newContext = fc.openContext(openContext, fields);
        assertEquals("Open context gave unexpected result",
                expectedOpenFieldlessContext, newContext);
        newContext += fc.closeContext(openContext, fields);
        assertEquals("Close context gave unexpected result",
                expectedClosedFieldlessContext, newContext);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger
     * #createObjectIfTriggered(com.vocollect.voicelink.core.importer.FieldMap, java.lang.String)}.
     */
    @Test()
    public void testCreateObjectIfTriggered() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        fc.setTriggeringFields(triggers);
        triggers.put(field1.getFieldName(), null);
        fc.setOpenTag(openTag);
        fc.setCloseTag(closeTag);
        String newContext = fc
                .createObjectIfTriggered(fields, this.openContext);
        assertEquals("Open context gave unexpected result",
                expectedOpenFieldlessContext, newContext);
        newContext += fc.closeContext(openContext, fields);
        assertEquals("Close context gave unexpected result",
                expectedClosedFieldlessContext, newContext);
        fc.setFieldMap(fields);
        fc.reset();
        newContext = fc.createObjectIfTriggered(fields, this.openContext);
        newContext += fc.closeContext(openContext, fields);
        assertEquals("Wrong result for triggered fields", expectedClosedAllFieldContext, newContext);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger
     * #getOpenContextFields()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger
     * #setOpenContextFields(java.util.HashMap)}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger
     * #setOpenContextFields(java.util.HashMap)}.
     */
    @Test()
    public void testGetOpenContextFields() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        fc.setTriggeringFields(triggers);
        triggers.put(field1.getFieldName(), null);
        fc.setOpenTag(openTag);
        fc.setCloseTag(closeTag);
        assertNotNull("open context field list was null upon init", fc.getOpenContextFields());
        assertTrue("Open context field list was not empty on init", fc.getOpenContextFields().isEmpty());
        fc.setFieldMap(fields);
        assertEquals("Should only have the field1 field", openContextFields, fc.getOpenContextFields());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger
     * #getCloseContextFields()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger
     * #setCloseContextFields(java.util.HashMap)}.
     */
    @Test()
    public void testGetCloseContextFields() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        fc.setTriggeringFields(triggers);
        triggers.put(field1.getFieldName(), null);
        fc.setOpenTag(openTag);
        fc.setCloseTag(closeTag);
        assertNotNull("Close context field list was null upon init", fc
                .getCloseContextFields());
        assertTrue("Close context field list was not empty on init", fc
                .getCloseContextFields().isEmpty());
        fc.setFieldMap(closeContextFields);
        assertEquals("Messed up list ", closeContextFields, fc
                .getCloseContextFields());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger
     * #getTriggeringFields()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger
     * #setTriggeringFields(java.util.HashMap)}.
     */
    @Test()
    public void testGetTriggeringFields() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        fc.setTriggeringFields(triggers);
        assertEquals("messed up triggering fields", triggers, fc
                .getTriggeringFields());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#getCloseTag()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#setCloseTag(java.lang.String)}.
     */
    @Test()
    public void testGetCloseTag() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        fc.setCloseTag(closeTag);
        assertEquals("Messed up close tag", closeTag, fc.getCloseTag());

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#getNewObjectName()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#setNewObjectName(java.lang.String)}.
     */
    @Test()
    public void testGetNewObjectName() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        fc.setNewObjectName(this.objectName);
        assertEquals("Messed up object name", objectName, fc.getNewObjectName());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#getOpenTag()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#setOpenTag(java.lang.String)}.
     */
    @Test()
    public void testGetOpenTag() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        fc.setOpenTag(openTag);
        assertEquals("Messed up open tag", openTag, fc.getOpenTag());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#isOpen()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#setIsOpen(boolean)}.
     */
    @Test()
    public void testIsOpen() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        assertFalse("Default for isOpen() is true!", fc.isOpen());
        fc.setIsOpen(true);
        assertTrue("Set open to true and is false", fc.isOpen());
        fc.setIsOpen(false);
        assertFalse("Set open to false and is true", fc.isOpen());
        fc.setIsOpen(true);
        assertTrue("Set open to true and is false", fc.isOpen());
    }
    
    /**
     * Test to make sure that the field suppression is working correctly.
     * If suppressed, the field should never show up in the output.
     * If remapped, the field should only show up in the output if the 
     * trigger is not set to use all fields, and has the field in its output list. 
     * 
     */
    @Test()
    public void testFieldSuppression() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        fc.setTriggeringFields(triggers);
        triggers.put(field1.getFieldName(), null);
        fc.setOpenTag(openTag);
        fc.setCloseTag(closeTag);
        String newContext = fc.createObjectIfTriggered(fields, this.openContext);
        assertEquals("Open context gave unexpected result",
                        expectedOpenFieldlessContext, newContext);
        newContext += fc.closeContext(openContext, fields);
        assertEquals("Close context gave unexpected result",
                        expectedClosedFieldlessContext, newContext);
        fc.setUseAllFields(false);
        //set remapped on - the field should show only when fc has it as part of its fields.
        
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#setUseAllFields()}.
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#getUseAllFields()}.
     */
    @Test()
    public void testgetUseAllFields() {
        FieldChangeCreateObjectTrigger fc  = new FieldChangeCreateObjectTrigger();
        assertFalse("default for useAllFields should be false", fc.getUseAllFields());
        fc.setUseAllFields(true);
        assertTrue("useAllFields should now be false", fc.getUseAllFields());
        fc.setUseAllFields(false);
        assertFalse("default for useAllFields should be false", fc.getUseAllFields());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#setIsOpen(boolean)}.
     */
    @Test()
    public void testSetIsOpen() {
        FieldChangeCreateObjectTrigger fc  = new FieldChangeCreateObjectTrigger();
        assertFalse("isOpen should initially be false false", fc.isOpen());
        fc.setIsOpen(true);
        assertTrue("isOpen should now return true", fc.isOpen());
        fc.setIsOpen(false);
        assertFalse("isOpen should now return false", fc.isOpen());
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#writeFields(com.vocollect.voicelink.core.importer.FieldMap, 
     * com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testWriteFields() {
        FieldChangeCreateObjectTrigger fc  = new FieldChangeCreateObjectTrigger();
        fc.setFieldMap(fields);
        String result = fc.writeFields(fields, fields);  // should write all fields
        assertEquals("Field markup should equal the writeFields markup", 
                this.expectedFieldoutput, result);
        FieldMap fm = new FieldMap();
        Field f = new Field();
        f.setFieldName("Other Field");
        fm.addField(f);
        fc.setUseAllFields(false);
        result = fc.writeFields(fm, fields);
        assertEquals("no field data should be present", "", result);
        result = fc.writeFields(fields, fm);
        assertEquals("no field data should be present", "", result);
        
        
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#getFieldMap()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#setFieldMap(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testGetSetFieldMap() {
        FieldChangeCreateObjectTrigger fc  = new FieldChangeCreateObjectTrigger();
        assertNotNull("Field map should not be null", fc.getFieldMap());
        FieldMap fm = new FieldMap();
        fc.setFieldMap(fm);
        assertNotNull("FieldMap should not be null now.", fc.getFieldMap());
        assertEquals("Should be the same field map I passed in.", fm, fc.getFieldMap());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#getUseAllFields()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#setUseAllFields(boolean)}.
     */
    @Test()
    public void testGetSetUseAllFields() {
        FieldChangeCreateObjectTrigger fc  = new FieldChangeCreateObjectTrigger();
        assertFalse("useAllFields should default to false.", fc.getUseAllFields());
        fc.setUseAllFields(true);
        assertTrue("useAllFields should be true now.", fc.getUseAllFields());
        fc.setUseAllFields(false);
        assertFalse("useAllFields should be false now.", fc.getUseAllFields());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#setTriggeringFields(java.util.HashMap)}.
     */
    @Test()
    public void testSetTriggeringFields() {
        FieldChangeCreateObjectTrigger fc  = new FieldChangeCreateObjectTrigger();
        assertNull("Triggering fields not null  on init.", fc.getTriggeringFields());
        fc.setTriggeringFields(triggers);
        assertNotNull("Triggering fields should not be null after set", fc.getTriggeringFields());
        assertEquals("List of triggering fields changed by set.", triggers, fc.getTriggeringFields());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#reset()}.
     */
    @Test()
    public void testReset() {
        // This clears the triggering fields data to null.
        String result = "";
        FieldChangeCreateObjectTrigger fc  = new FieldChangeCreateObjectTrigger();
        fc.setTriggeringFields(triggers);
        fc.setOpenTag(openTag);
        fc.setCloseTag(closeTag);
        fc.setFieldMap(fields);
        result = fc.createObjectIfTriggered(fields, openContext);
        if (fc.isOpen()) {
            result += fc.closeContext(openContext, fields);
        }
        assertEquals("unexpected result creating xml object", expectedClosedAllFieldContext, result);
        result = "";
        result = fc.createObjectIfTriggered(fields, openContext);
        if (fc.isOpen()) {
            result += fc.closeContext(openContext, fields);
        }
        assertEquals("Should not have created object", "", result);
        fc.reset();
        result = fc.createObjectIfTriggered(fields, openContext);
        if (fc.isOpen()) {
            result += fc.closeContext(openContext, fields);
        }
        assertEquals("unexpected result creating xml object", expectedClosedAllFieldContext, result);
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#isLookup()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.FieldChangeCreateObjectTrigger#setLookup(boolean)}.
     */
    @Test()
    public void testGetSetLookup() {
        FieldChangeCreateObjectTrigger fc = new FieldChangeCreateObjectTrigger();
        assertFalse("Lookup should default to false", fc.isLookup());
        fc.setLookup(true);
        assertTrue("Lookup should be true now", fc.isLookup());
        fc.setLookup(false);
        assertFalse("Lookup should be false now", fc.isLookup());
        
    }

    /**
     * Test that the markup we use is good.
     */
    @Test()
    public void testConfigSetup() {
        CastorConfiguration cc = new CastorConfiguration("import-definition.xml", "fieldChangeTrigger.xml");
        Object obj = null;
        try {
            obj = cc.configure();
        } catch (ConfigurationException e1) {
            org.testng.Assert.fail("Mapping and markup seem to be at odds.");
        }
        FieldChangeCreateObjectTrigger e = (FieldChangeCreateObjectTrigger) obj; 
        
        org.testng.Assert.assertEquals(2, e.getFieldMap().size(), "There should be two fields in the trigger fields");
        
//        assertNotNull(e.getFieldMap().get("operatorIdentifier"));
//        assertNotNull((Field) e.getFieldMap().get("operatorIdentifier"));
//        assertNotNull(e.getFieldMap().get("password"));
//        assertNotNull((Field) e.getFieldMap().get("password"));
        
//        org.testng.Assert.assertTrue(((Field) e.getFieldMap().get("operatorIdentifier")).isKeyField(),
//            "Field named 'operatorIdentifier' should be a lookup field");
        
    }

}
