/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.core.importer.CastorConfiguration;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.DBLookup;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.PersistenceManager;
import com.vocollect.voicelink.core.model.Operator;

import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test the lookup trigger.
 * @author dgold
 *
 */
public class LookupTriggerTest extends VoiceLinkDAOTestCase {

    private PersistenceManager  pm;

    private DBLookup            finder = null; //new DBLookup(pm);

    private LookupTrigger       lookupOperator = null;

    private FieldMap  inputDataFields = null;

    private Operator            testOperator   = null;

    private List<Operator>                allOperators   = null;

    private String              openContext;

    private String              expectedResult;

    private String              expectedResultNoFields;

    private String              expectedResultNoOpenTag;

    private String              expectedFieldoutput;

    private String              objectName;

    private String              openTag;

    private String              closeTag;

    private String              keyFieldName;

    private String              finderName;

    private FieldMap fields;

    private Field    f;

    /**
     * @param pmInjected the persistence manager injected
     */
    public void setPersistenceManager(PersistenceManager pmInjected) {
        this.pm = pmInjected;
    }

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        String[] superConfig = super.getConfigLocations();
        String[] config = new String[superConfig.length + 1];
        System.arraycopy(superConfig, 0, config, 0, superConfig.length);
        config[superConfig.length] = "applicationContext-voicelink-import.xml";
        return config;
    }

    /**
     * @throws java.lang.Exception if the database takes exception to the init.
     */
    @BeforeClass()
    protected void setUpLocal() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        System.err.println("initializing small sample");
        adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkDataSmall.xml", DatabaseOperation.REFRESH);
        System.err.println("initializing operators");
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkSampleOperators.xml", DatabaseOperation.REFRESH);
        System.err.println("DB Initialization complete");
    }

    /**
     * Set up test stuff.
     */
    @BeforeMethod()
    @SuppressWarnings("unchecked")
    protected void getOperatorID() throws Exception {
        if (null == testOperator) {
            // Go get a list of all the operators


            try {
                allOperators = pm.getAll(Operator.class);
            } catch (DataAccessException e) {
                assertTrue(false, "getting all operators threw an exception");
            }
            if (0 >= allOperators.size()) {
                setUpLocal();
                allOperators = pm.getAll(Operator.class);
            }

            testOperator = new Operator();
            // Set the operator id to something we know is in the database
            testOperator.
            setOperatorIdentifier(allOperators.get(0).getOperatorIdentifier());

            lookupOperator = new LookupTrigger();

            keyFieldName = "operatorIdentifier";
            Field aField = new Field();
            aField = setupField(aField, keyFieldName, testOperator.getOperatorIdentifier());
            lookupOperator.addField(aField);
            finderName = "findByIdentifier";
            lookupOperator.setFinderName(finderName);
            lookupOperator.setNewObjectName("Operator");

            Field bField = new Field();
            bField = setupField(bField, keyFieldName, "Nobody");
            bField.setKeyField(false);
            inputDataFields = new FieldMap();
            inputDataFields.addField(bField);

            openContext = "";
            objectName = "operator";

            expectedResult = "<" + objectName + ">" + "<" + aField.getFieldName() + ">"
            + aField.getFieldData() + "</" + aField.getFieldName() + ">" + "</" + objectName + ">";
            expectedResultNoFields = "<" + objectName + ">" 
            + "</" + objectName + ">";

            f = new Field();
            f = setupField(f, aField.getFieldName(), aField.getFieldData());
            expectedFieldoutput = f.getOpenTag() + f.getFieldData() + f.getCloseTag();
            fields = new FieldMap();
            fields.addField(f);

            openTag = "<" + objectName + ">";
            closeTag = "</" + objectName + ">";

            expectedResultNoOpenTag = expectedFieldoutput + closeTag;
            finder = new DBLookup(pm);

        } else {
            testOperator = new Operator();
            // Set the operator id to something we know is in the database
            testOperator.
            setOperatorIdentifier(allOperators.get(0).getOperatorIdentifier());
        }

    }

    /**
     * Set up the field passed in with the data passed in. Just to clean up the setup code.
     * @param fld field.
     * @param fieldName as it says
     * @param fieldData as it says
     * @return the filled in field.
     */
    private Field setupField(Field fld, String fieldName, String fieldData) {
        fld.setFieldName(fieldName);
        fld.setFieldData(fieldData);
        fld.setOpenTag("<" + fieldName + ">");
        fld.setCloseTag("</" + fieldName + ">");
        fld.setFieldData(fieldData);
        fld.setKeyField(true);
        return fld;
    }
    
    /**
     * Clean up after the test - clear out the DB.
     */
    @AfterTest()
    protected void tearDownLocal() {
        DbUnitAdapter adapter = new DbUnitAdapter();
        try {
          adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkDataUnitTest_ResetAll.xml");
      } catch (Exception e) {
          e.printStackTrace();
          assertFalse(true, "Threw an exception setting up database");
      }

    }

    /**
     * Test method for 
     * {@link com.vocollect.voicelink.core.importer.triggers.LookupTrigger#lookupObject(
     * java.lang.Object, com.vocollect.voicelink.core.importer.DBLookup)}.
     */
    @Test()
    public void testLookupObject() {
        Operator op = null;
        Object obj = null;
        // Default is to expect to find the thing we're looking for.
        try {
            obj = lookupOperator.lookupObject(testOperator, finder);
        } catch (VocollectException e1) {
            assertTrue(false, "lookup threw an exception");
        }
        assertNotNull(obj, "drew a null with the lookup");
        try {
            op = (Operator) obj;
        } catch (ClassCastException e) {
            assertTrue(false, "retrieved object is not an operator");
        }
        assertEquals(testOperator.getOperatorIdentifier(), 
                op.getOperatorIdentifier(), "Got a different operator");
        
        // Let's set the flag to fail if we find the thing we just found...
        lookupOperator.setFailIfFound(true);
        boolean thrown = false;
        try {
            obj = lookupOperator.lookupObject(testOperator, finder);
        } catch (VocollectException e1) {
            thrown = true;
        }
        assertTrue(thrown, "lookup failed to throw an exception");
        
        // OK, let's look for something that isn't there.
        testOperator.setOperatorIdentifier("Nobody");
        try {
            obj = lookupOperator.lookupObject(testOperator, finder);
        } catch (VocollectException e1) {
            assertTrue(false, "lookup threw an exception");
        }
        
//        // OK, let's reset the flag, and we should again throw an exception
//        lookupOperator.setFailIfFound(false);
//        thrown = false;
//        try {
//            obj = lookupOperator.lookupObject(testOperator, finder);
//        } catch (VocollectException e1) {
//            thrown = true;
//        }
//        assertTrue(thrown, "lookup failed to throw an exception");
//
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.LookupTrigger#LookupTrigger()}.
     */
    @Test()
    public void testLookupTrigger() {
        LookupTrigger lt = new LookupTrigger();
        assertNotNull(lt, "Constructor returned a null");
       
    }

    /**
     * Test method for 
     * {@link com.vocollect.voicelink.core.importer.triggers.LookupTrigger#
     * detectTriggeringConditions(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testDetectTriggeringConditions() {
        assertTrue(lookupOperator.detectTriggeringConditions(inputDataFields), 
                    "Failed to detect valid triggering conditions");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.LookupTrigger#
     * addField(com.vocollect.voicelink.core.importer.Field)}.
     */
    @Test()
    public void testAddField() {
        LookupTrigger lt = new LookupTrigger();
        Field someField = new Field();
        someField.setFieldName("junk");
        lt.addField(someField);
        assertNotNull(lt.getFieldMap().get("junk"), "Could not retrieve field by name");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.LookupTrigger#
     * closeContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testCloseContext() {
        LookupTrigger c = new LookupTrigger();
        c.setNewObjectName(objectName);
        c.setOpenTag(openTag);
        c.setCloseTag(closeTag);
        assertEquals(expectedResultNoOpenTag, c.closeContext(openContext, fields),
                        "closeContext did not write the fields");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.LookupTrigger#
     * createObjectIfTriggered(com.vocollect.voicelink.core.importer.FieldMap, java.lang.String)}.
     */
    @Test()
    public void testCreateObjectIfTriggered() {
        LookupTrigger c = new LookupTrigger();
        c.setUseAllFields(true);
        c.setNewObjectName(objectName);
        c.setOpenTag(openTag);
        c.setCloseTag(closeTag);
        assertEquals(expectedResult, c.createObjectIfTriggered(fields, openContext),
                        "Did not get the expected result");
        c.setUseAllFields(false);
        assertEquals(expectedResultNoFields, c.createObjectIfTriggered(fields, openContext),
                        "Did not get the expected result");

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.LookupTrigger#getCloseContextFields()}.
     */
    @Test()
    public void testGetCloseContextFields() {
        FieldMap ccf = lookupOperator.getCloseContextFields(); 
        assertNotNull(ccf, "Close context fields are null");
        assertFalse(ccf.isEmpty(), "Close context fields are empty");
        assertEquals(1, ccf.size(), "Should have exactly one field");
        assertNotNull(ccf.get(keyFieldName), "Cannot get field by name: " + keyFieldName);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.LookupTrigger#setUseAllFields()}.
     * {@link com.vocollect.voicelink.core.importer.LookupTrigger#getUseAllFields()}.
     */
    @Test()
    public void testgetUseAllFields() {
        LookupTrigger c = new LookupTrigger();
        assertTrue(c.getUseAllFields(), "default for useAllFields should be true");
        c.setUseAllFields(false);
        assertFalse(c.getUseAllFields(), "useAllFields should now be false");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.LookupTrigger#getNewObjectName()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.LookupTrigger#setNewObjectName(java.lang.String)}.
     */
    @Test()
    public void testGetNewObjectName() {
        LookupTrigger c = new LookupTrigger();
        c.setNewObjectName(objectName);
        assertEquals(objectName, c.getNewObjectName(), "new object name was changed");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.LookupTrigger#getOpenTag()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.LookupTrigger#setOpenTag(java.lang.String)}.
     */
    @Test()
    public void testGetOpenTag() {
        LookupTrigger c = new LookupTrigger();
        c.setOpenTag(openTag);
        assertEquals(openTag, c.getOpenTag(), "open tag changed");
        c.setNewObjectName(objectName);
        assertEquals(openTag, c.getOpenTag(), "open tag changed");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.LookupTrigger#isOpen()}.
     */
    @Test()
    public void testIsOpen() {
        LookupTrigger c = new LookupTrigger();
        assertFalse(c.isOpen(), "isOpen should initially return false ");
        c.openContext(openContext, fields);
        assertTrue(c.isOpen(), "isOpen should be true ");
        c.setIsOpen(false);
        assertFalse(c.isOpen(), "isOpen should return as set ");

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.LookupTrigger#
     * openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testOpenContext() {
        LookupTrigger c = new LookupTrigger();
        c.setUseAllFields(true);
        c.setNewObjectName(objectName);
        c.setOpenTag(openTag);
        c.setCloseTag(closeTag);

        assertEquals(openTag, c.openContext(openContext, fields), "Did not get the expected result from openContext");
        
        c.setUseAllFields(false);
        assertEquals(openTag, c.openContext(openContext, fields), "Did not get the expected result");

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.LookupTrigger#getCloseTag()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.LookupTrigger#setCloseTag(java.lang.String)}.
     */
    @Test()
    public void testGetCloseTag() {
        LookupTrigger c = new LookupTrigger();
        c.setCloseTag(closeTag);
        assertEquals(closeTag, c.getCloseTag(), "close tag was changed");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.LookupTrigger#setIsOpen(boolean)}.
     */
    @Test()
    public void testSetIsOpen() {
        LookupTrigger c = new LookupTrigger();
        assertFalse(c.isOpen(), "isOpen should initially be false false");
        c.setIsOpen(true);
        assertTrue(c.isOpen(), "isOpen should now return true");
        c.setIsOpen(false);
        assertFalse(c.isOpen(), "isOpen should now return false");
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.LookupTrigger#writeFields(
     * com.vocollect.voicelink.core.importer.FieldMap, 
     * com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testWriteFields() {
        LookupTrigger c = new LookupTrigger();
        c.setFieldMap(fields);
        String result = c.writeFields(fields, fields);  // should write all fields
        assertEquals(this.expectedFieldoutput, result, "Field markup shoukd equal the writeFields markup");
        FieldMap fm = new FieldMap();
        Field fld = new Field();
        fld.setFieldName("Other Field");
        fm.addField(fld);
        c.setUseAllFields(false);
        result = c.writeFields(fm, fields);
        assertEquals("", result, "no field data should be present");
        result = c.writeFields(fields, fm);
        assertEquals("", result, "no field data should be present");
        
        
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.LookupTrigger#getFieldMap()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.LookupTrigger#
     * setFieldMap(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testGetSetFieldMap() {
        LookupTrigger c = new LookupTrigger();
        assertNotNull(c.getFieldMap(), "Field map should not be null");
        FieldMap fm = new FieldMap();
        c.setFieldMap(fm);
        assertNotNull(c.getFieldMap(), "FieldMap should not be null now.");
        assertEquals(fm, c.getFieldMap(), "Should be the same field map I passed in.");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.LookupTrigger#getUseAllFields()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.LookupTrigger#setUseAllFields(boolean)}.
     */
    @Test()
    public void testGetSetUseAllFields() {
        LookupTrigger c = new LookupTrigger();
        assertTrue(c.getUseAllFields(), "useAllFields should default to true.");
        c.setUseAllFields(false);
        assertFalse(c.getUseAllFields(), "useAllFields should be false now.");
        c.setUseAllFields(true);
        assertTrue(c.getUseAllFields(), "useAllFields should be true now.");
    }

    /**
     * 
     */
    @Test()
    public void testConfigErrorDetection() {
        LookupTrigger e = new LookupTrigger();
        for (Field field : inputDataFields.values()) {
            assertFalse(field.isKeyField(), "Field in field map is a key field");
        }
        e.setFieldMap(inputDataFields);
        boolean thrown = false;
        // the inputdatafields don't have any key fields, and we didn't name a finder. This should pitch a fit.
        try {
            e.lookupObject(this.testOperator, this.finder);
        } catch (Exception e1) {
            thrown = true;
        }
        assertTrue(thrown, "Failed to throw expected config error");
        
        thrown = false;
        e.setFinderName(finderName);
        try {
            e.lookupObject(this.testOperator, this.finder);
        } catch (Exception e1) {
            thrown = true;
        }
        assertTrue(thrown, "Failed to throw expected config error");

        thrown = false;
        inputDataFields.values().iterator().next().setKeyField(true);
        e.setFieldMap(inputDataFields);
        inputDataFields.values().iterator().next().setKeyField(false);

        try {
            e.lookupObject(this.testOperator, this.finder);
        } catch (Exception e1) {
            e1.printStackTrace();
            thrown = true;
        }
        assertFalse(thrown, "Should not have thrown this config error");


    }
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.
     * LookupTrigger#isLookup()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.
     * LookupTrigger#setLookup(boolean)}.
     */
    @Test()
    public void testGetSetLookup() {
        LookupTrigger c = new LookupTrigger();        
        assertFalse(c.isLookup(), "Lookup should default to false");
        c.setLookup(true);
        assertTrue(c.isLookup(), "Lookup should be true now");
        c.setLookup(false);
        assertFalse(c.isLookup(), "Lookup should be false now");
        
    }

    /**
     * Test that the markup we use is good.
     */
    @Test()
    public void testConfigSetup() {
        CastorConfiguration cc = new CastorConfiguration("import-definition.xml", "baseLookupTrigger.xml");
        Object obj = null;
        try {
            obj = cc.configure();
        } catch (ConfigurationException e1) {
            fail("Mapping and markup seem to be at odds.");
        }
        LookupTrigger e = (LookupTrigger) obj; 
        
        assertEquals(2, e.getFieldMap().size(), "There should be two fields in the trigger fields");
        
        assertNotNull(e.getFieldMap().get("operatorIdentifier"));
        assertNotNull(e.getFieldMap().get("operatorIdentifier"));
        assertNotNull(e.getFieldMap().get("password"));
        assertNotNull(e.getFieldMap().get("password"));
        
        assertTrue((e.getFieldMap().get("operatorIdentifier")).isKeyField(),
            "Field named 'operatorIdentifier' should be a lookup field");
        
        // OK, use it...
        Object result = null;
        try {
            result = e.lookupObject(this.testOperator, this.finder);
        } catch (VocollectException e1) {
            assertFalse(true, "Threw an exception on lookup");
        }
        assertNotNull(result, "Retrieved nothing from the lookup");
        assertNotNull(result, "Did not get an operator...");
        
    }
}







