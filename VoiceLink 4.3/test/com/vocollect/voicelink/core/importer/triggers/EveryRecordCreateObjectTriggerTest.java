/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.CastorConfiguration;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.CreateObjectTrigger;
import com.vocollect.voicelink.core.importer.CreateObjectTriggerImpl;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;
import com.vocollect.voicelink.core.model.Item;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

/**
 * @author dgold
 */
public class EveryRecordCreateObjectTriggerTest {

    private String           testFieldName1;
    private String           testFieldName2;
    private String           testFieldName3;

    private String           openContext;
    
    private String           expectedCloseContextResult;
    
    private String           expectedFieldContent;
    
    private String           expectedSuppressedContext;
    
    private String           expectedField2Context;
    
    private String           expectedField2Content;

    private FixedLengthField field1;
    private FixedLengthField field2;
    private FixedLengthField field3;
    
    private FieldMap         fields = null; 

    private String           testObjectName;

    private String           testObjectOpenTag;

    private String           testObjectCloseTag;

    private String           expectedContext;
    
    private String           junkChangeString = "X";

    /**
     * 
     *
     */
    @BeforeMethod()
    @SuppressWarnings("unchecked")
    public void setUp() {
        openContext = "";
        testFieldName1 = "junk";
        testFieldName2 = "mo-junk";
        testFieldName3 = "password";
        testObjectName = "testObject";
        testObjectOpenTag = "<" + testObjectName + ">";
        testObjectCloseTag = "</" + testObjectName + ">";
        
        field1 = new FixedLengthField();

        field1.setFieldName(testFieldName1);
        field1.setOpenTag("<" + field1.getFieldName() + ">");
        field1.setCloseTag("</" + field1.getFieldName() + ">");
        field1.setFieldData("data");
        
        field2 = new FixedLengthField();

        field2.setFieldName(testFieldName2);
        field2.setOpenTag("<" + field2.getFieldName() + ">");
        field2.setCloseTag("</" + field2.getFieldName() + ">");
        field2.setFieldData("data");
        
        field3 = new FixedLengthField();

        field3.setFieldName(testFieldName3);
        field3.setOpenTag("<" + field3.getFieldName() + ">");
        field3.setCloseTag("</" + field3.getFieldName() + ">");
        field3.setFieldData("data");

        fields = new FieldMap();
        fields.addField(field1);
        fields.addField(field2);
        fields.addField(field3);
        
        expectedContext = testObjectOpenTag + field1.getOpenTag()
                + field1.getFieldData() + field1.getCloseTag()
                + testObjectCloseTag;
        
        expectedCloseContextResult = testObjectCloseTag;
        expectedFieldContent = field1.getOpenTag() + field1.getFieldData() + field1.getCloseTag();
        expectedField2Content = field2.getOpenTag() + field2.getFieldData() + field2.getCloseTag();
        expectedSuppressedContext = testObjectOpenTag + testObjectCloseTag;
        expectedField2Context = testObjectOpenTag + expectedField2Content + testObjectCloseTag;
        
//      org.exolab.castor.tools.MappingTool mapper = new MappingTool();
//      mapper.addClass( EveryRecordCreateObjectTrigger.class );
      
//      mapper.write(new FileWriter("C:/temp/validator.mapping.xml"));

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#EveryRecordCreateObjectTrigger()}.
     */
    @Test()
    public void testEveryRecordCreateObjectTrigger() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        assertNotNull("Constructor failed", e);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger
     * #closeContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testCloseContext() {

        FixedLengthFieldMap fields1 = null;
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        e.setCloseTag(testObjectCloseTag);

        assertEquals("Close context changed the string.", expectedCloseContextResult, e
                .closeContext(openContext, fields1));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger
     * #createObjectIfTriggered(com.vocollect.voicelink.core.importer.FieldMap, java.lang.String)}.
     */
    @Test()
    public void testCreateObjectIfTriggered() {

        FixedLengthFieldMap fields1 = new FixedLengthFieldMap();
        fields1.addField(field1);
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        e.setFieldMap(fields1);
        e.setOpenTag(this.testObjectOpenTag);
        e.setCloseTag(this.testObjectCloseTag);
        String result = e.createObjectIfTriggered(fields1, openContext);
        assertFalse("Context did not get changed", 0 == openContext
                .compareTo(result));
        assertEquals("Wrong context", expectedContext, result);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger
     * #detectTriggeringConditions(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testDetectTriggeringConditions() {
        FixedLengthFieldMap fields1 = null;
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        assertTrue("trigger detect returned false", e
                .detectTriggeringConditions(fields1));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#getNewObjectName()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#setNewObjectName(java.lang.String)}.
     */
    @Test()
    public void testGetNewObjectName() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        e.setNewObjectName(testObjectName);
        assertEquals("Object name changed", testObjectName, e
                .getNewObjectName());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#getOpenTag()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#setOpenTag(java.lang.String)}.
     */
    @Test()
    public void testGetOpenTag() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        e.setOpenTag(this.testObjectOpenTag);
        assertEquals("Open tag changed", testObjectOpenTag, e.getOpenTag());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#isOpen()}.
     */
    @Test()
    public void testIsOpen() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        assertFalse("IsOpen is true", e.isOpen());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger
     * #openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testOpenContext() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        FixedLengthFieldMap fields1 = new FixedLengthFieldMap();
        fields1.addField(field1);
        e.setFieldMap(fields1);
        e.setOpenTag(this.testObjectOpenTag);
        e.setCloseTag(this.testObjectCloseTag);
        String result = e.openContext(openContext, fields1);
        assertFalse("Context did not get changed", 0 == openContext
                .compareTo(result));
        assertEquals("Wrong context", expectedContext, result);
        assertEquals("Context does not append properly", expectedContext
                + expectedContext, e.openContext(result, fields1));

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#getCloseTag()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#setCloseTag(java.lang.String)}.
     */
    @Test()
    public void testGetCloseTag() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        e.setCloseTag(this.testObjectCloseTag);
        assertEquals("close tag changed", testObjectCloseTag, e.getCloseTag());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#setIsOpen(boolean)}.
     */
    @Test()
    public void testSetIsOpen() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        assertFalse("isOpen is true", e.isOpen());
        e.setIsOpen(false);
        assertFalse("isOpen is true", e.isOpen());
        e.setIsOpen(true);
        assertFalse("isOpen is true", e.isOpen());

    }


    

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#getFieldMap()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#setFieldMap(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testSetGetFieldMap() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        FieldMap fm = new FieldMap();
        e.setFieldMap(fm);
        assertNotNull("Field map is null", e.getFieldMap());
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#getUseAllFields()}.
     * Test method for 
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#setUseAllFields(boolean)}.
     */
    @Test()
    public void testSetGetUseAllFields() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        e.setUseAllFields(false);
        assertFalse("could not set useAllFields to False", e.getUseAllFields());
        e.setUseAllFields(true);
        assertTrue("Could not set use all fields to True", e.getUseAllFields());
    }


    /**
     * Test method for 
     * {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#setCloseTag(java.lang.String)}.
     */
    @Test()
    public void testSetGetCloseTag() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        e.setCloseTag(testObjectCloseTag);
        assertEquals("Different close tag than was set", testObjectCloseTag, e.getCloseTag());
        e.setCloseTag(junkChangeString);
        assertEquals("Could not change close tag", junkChangeString, e.getCloseTag());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#setNewObjectName(java.lang.String)}.
     */
    @Test()
    public void testSetGetNewObjectName() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        e.setNewObjectName(testObjectName);
        assertEquals("Improper set for new object name", testObjectName, e.getNewObjectName());
        e.setNewObjectName(junkChangeString);
        assertEquals("Improper change for new object name", junkChangeString, e.getNewObjectName());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#setOpenTag(java.lang.String)}.
     */
    @Test()
    public void testSetGetOpenTag() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        e.setOpenTag(testObjectOpenTag);
        assertEquals("Improper set for open tag", testObjectOpenTag, e.getOpenTag());
        e.setOpenTag(junkChangeString);
        assertEquals("Improper set for open tag", junkChangeString, e.getOpenTag());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#writeFields(
     * com.vocollect.voicelink.core.importer.FieldMap, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testWriteFields() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        FieldMap fm = new FieldMap();
        FieldMap fm2 = new FieldMap();
        fm.addField(field1);
        fm2.addField(field2);
        
        e.setUseAllFields(false);
        
        String result = "";
        result = e.writeFields(fm, fm); 
        assertEquals("Wrote fields incorrectly", expectedFieldContent, result);
        
        result = "";
        result = e.writeFields(fm, null);
        assertEquals("Wrote fields incorrectly", "", result);
        
        result = "";
        result = e.writeFields(fm, fm2);
        assertEquals("Wrote fields incorrectly", "", result);
        
        fm2.addField(field1);
        result = "";
        result = e.writeFields(fm, fm2);
        assertEquals("Wrote fields incorrectly", expectedFieldContent, result);
        
        
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#getCloseContextFields()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#setCloseContextFields(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testSetGetCloseContextFields() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        FieldMap fm = new FieldMap();
        fm.addField(field1);

        e.setCloseContextFields(fm);
        assertEquals("Close context map set incorrectly", fm, e.getCloseContextFields());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#getOpenContextFields()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#setOpenContextFields(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testSetGetOpenContextFields() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        FieldMap fm = new FieldMap();
        fm.addField(field1);

        e.setOpenContextFields(fm);
        assertEquals("Close context map set incorrectly", fm, e.getOpenContextFields());
    }
    
    /**
     * Test to make sure that the field suppression is working correctly.
     * If suppressed, the field should never show up in the output.
     * If remapped, the field should only show up in the output if the 
     * trigger is not set to use all fields, and has the field in its output list. 
     * 
     */
    @Test()
    public void testFieldSuppression() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        e.setOpenTag(this.testObjectOpenTag);
        e.setCloseTag(this.testObjectCloseTag);

        FieldMap fm = new FieldMap();
        fm.addField(field1);

        FieldMap fm2 = new FieldMap();
        fm2.addField(field2);

        e.setOpenContextFields(fm);
        
        // Sanity check
        // Set suppress off, remapped = false, useallfields = false
        assertEquals(expectedContext, e.createObjectIfTriggered(fm, openContext));
        // Not obvious, but if useAllFields is true, we ignore any field list and
        // just produce output for the fields passed in.
        assertEquals(expectedField2Context, e.createObjectIfTriggered(fm2, openContext));
        e.setUseAllFields(false);
        field1.setSuppress(true);
        // After the field is suppressed, it should not show up in the output.
        assertEquals(expectedSuppressedContext, e.createObjectIfTriggered(fm, openContext));
        e.setUseAllFields(true);
        assertEquals(expectedSuppressedContext, e.createObjectIfTriggered(fm, openContext));
        field1.setRemapped(true);
        assertEquals(expectedSuppressedContext, e.createObjectIfTriggered(fm, openContext));
        e.setUseAllFields(false);
        assertEquals(expectedSuppressedContext, e.createObjectIfTriggered(fm, openContext));
        // Set suppress off, remapped = true, useallfields = false
        field1.setSuppress(false);
        assertEquals(expectedContext, e.createObjectIfTriggered(fm, openContext));
        // Set suppress off, remapped = true, useallfields = true
        e.setUseAllFields(true);
        assertEquals(expectedSuppressedContext, e.createObjectIfTriggered(fm, openContext));
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#isLookup()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.EveryRecordCreateObjectTrigger#setLookup(boolean)}.
     */
    @Test()
    public void testGetSetLookup() {
        EveryRecordCreateObjectTrigger e = new EveryRecordCreateObjectTrigger();
        assertFalse("Lookup should default to false", e.isLookup());
        e.setLookup(true);
        assertTrue("Lookup should be true now", e.isLookup());
        e.setLookup(false);
        assertFalse("Lookup should be false now", e.isLookup());
        
    }

    /**
     * Test that the markup we use is good.
     */
    @Test()
    public void testConfigSetup() {
        CastorConfiguration cc = new CastorConfiguration("import-definition.xml", "baseTrigger.xml");
        Object obj = null;
        try {
            obj = cc.configure();
        } catch (ConfigurationException e1) {
            fail("Mapping and markup seem to be at odds.");
        }
        EveryRecordCreateObjectTrigger e = (EveryRecordCreateObjectTrigger) obj; 
        assertTrue("Trigger should show lookup=true", e.isLookup());
        
        assertFalse("Trigger should show main object = false", e.isMainObject());
        
        assertTrue("Trigger should show useList = true", e.isUseList());
        
        assertEquals("There should be two fields in the trigger fields", 2, e.getFieldMap().size());
        
        assertTrue("Lookup field should contain TRUE", e.isLookup());
        
        assertNotNull(e.getFieldMap().get("junk"));
        assertNotNull(e.getFieldMap().get("junk"));
        assertNotNull(e.getFieldMap().get("mo-junk"));
        assertNotNull(e.getFieldMap().get("mo-junk"));
        
        assertTrue("Field named 'junk' should be a lookup field", (e.getFieldMap().get("junk")).isKeyField());
    }

    /**
     * Test that the markup we use is good.
     */
    @SuppressWarnings("unchecked")
    @Test()
    public void testConfigSetupSympatheticTriggers() {
        CastorConfiguration cc = new CastorConfiguration("import-definition.xml", "baseSympatheticTrigger.xml");
        Object obj = null;
        try {
            obj = cc.configure();
        } catch (ConfigurationException e1) {
            fail("Mapping and markup seem to be at odds.");
        }
        EveryRecordCreateObjectTrigger e = (EveryRecordCreateObjectTrigger) obj; 
        field1.setFieldData("data1");
        field2.setFieldData("data2");
        field3.setFieldData("data3");
        String xmlString = "";
        e.setOpenTag("<" + e.getNewObjectName() + ">");
        e.setCloseTag("</" + e.getNewObjectName() + ">");
        
        for (CreateObjectTrigger trigger : e.getTriggers()) {
            trigger.setOpenTag("<" + trigger.getNewObjectName() + ">");
            trigger.setCloseTag("</" + trigger.getNewObjectName() + ">");
        }

        assertFalse("This configuration declared at least one sympathetic trigger", e.getTriggers().isEmpty());
        System.out.println(e.createObjectIfTriggered(fields, xmlString));
        xmlString = "";
        System.out.println(e.openContext(xmlString, fields));
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.
     * EveryRecordCreateObjectTrigger#lookupObject(Object, DBLookup) }.
     */
    @Test()
    public void testLookupObject() {
        CreateObjectTriggerImpl c = new CreateObjectTriggerImpl();        
        assertFalse("Lookup should default to false", c.isLookup());
        Item anItem = new Item();
        Object retval = null;
        Item returnItem = null;
        try {
            retval = c.lookupObject(anItem, null);
        } catch (VocollectException e) {
            e.printStackTrace();
            fail("Threw an exception when doing a lookup, this trigger is not a lookup trigger.");
        }
        try {
            returnItem = (Item) retval;
        } catch (Exception e) {
            e.printStackTrace();
            fail("Conversion to Item threw an exception.");
        }
        assertEquals("Expecting the lookup to just return the same item we started with.", returnItem, anItem);
    }
    

}

