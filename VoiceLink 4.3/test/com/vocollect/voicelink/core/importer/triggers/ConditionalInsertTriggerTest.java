/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.triggers;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.test.DepInjectionSpringContextNGTests;
import com.vocollect.voicelink.core.importer.BaseImportServiceManagerTestCase;
import com.vocollect.voicelink.core.importer.CastorConfiguration;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.DBLookup;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.PersistenceManager;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;
import com.vocollect.voicelink.core.model.Operator;

import java.util.HashMap;
import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;



/**
 * Test for ConditionalInsertTrigger.
 * @author Kalpna
 *
 */
public class ConditionalInsertTriggerTest extends DepInjectionSpringContextNGTests {
    private String                  field1Name;

    private String                  field2Name;

    private String                  field1Data;

    private String                  field1NewData;

    private String                  field2Data;

    private String                  field1OpenTag;

    private String                  field1CloseTag;

    private String                  field2OpenTag;

    private String                  field2CloseTag;

    private String                  openContext;

    private String                  openTag;

    private String                  closeTag;

    private String                  objectName;

    private String                  expectedOpenFieldlessContext;

    private String                  expectedClosedFieldlessContext;

    private String                  expectedClosedAllFieldContext;
    
    private String                  expectedFieldoutput;
    
//    private String                  expectedResult;

    private FixedLengthField        field1;

    private FixedLengthField        field2;

    private FixedLengthFieldMap     fields;

    private FieldMap openContextFields;

    private FieldMap closeContextFields;

    private HashMap<String, String> triggers;

    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.test.DepInjectionSpringContextNGTests#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return BaseImportServiceManagerTestCase.getImportTestConfigLocations();
    }

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @SuppressWarnings("unchecked")
    @BeforeMethod()
    protected void setUpLocal() throws Exception {
        this.field1Name = "field1";
        this.field2Name = "field2";
        this.field1Data = "original";
        this.field1NewData = "changed";
        this.field2Data = "other";
        this.field1OpenTag = "<" + field1Name + ">";
        this.field1CloseTag = "</" + field1Name + ">";
        this.field2OpenTag = "<" + field2Name + ">";
        this.field2CloseTag = "</" + field2Name + ">";
        
        this.field1 = new FixedLengthField();
        this.field1.setFieldName(field1Name);
        this.field1.setFieldData(field1Data);
        this.field1.setOpenTag(field1OpenTag);
        this.field1.setCloseTag(field1CloseTag);
        this.field1.setWriteOnOpenContext(true);

        this.field2 = new FixedLengthField();
        this.field2.setFieldName(field2Name);
        this.field2.setFieldData(field2Data);
        this.field2.setOpenTag(field2OpenTag);
        this.field2.setCloseTag(field2CloseTag);
        this.field2.setWriteOnOpenContext(false);
        
        this.fields = new FixedLengthFieldMap();
        this.fields.addField(field1);
        this.fields.addField(field2);
        assertEquals(2, fields.size(), "Dropped an entry from the field map");

        this.triggers = new HashMap<String, String>();
        this.triggers.put(field1.getFieldName(), null);
        this.objectName = "testObj";
        this.openContext = "";
        this.openTag = "<" + objectName + ">";
        this.closeTag = "</" + objectName + ">";

        this.expectedOpenFieldlessContext = openTag;
        this.expectedClosedFieldlessContext = openTag + closeTag;
        
        this.expectedFieldoutput = field1.getOpenTag()
                                    + field1.getFieldData() + field1.getCloseTag()
                                    + field2.getOpenTag() + field2.getFieldData()
                                    + field2.getCloseTag();

        this.expectedClosedAllFieldContext = openTag + field1.getOpenTag()
                + field1.getFieldData() + field1.getCloseTag()
                + field2.getOpenTag() + field2.getFieldData()
                + field2.getCloseTag() + closeTag;
        this.openContextFields = new FieldMap();
        this.openContextFields.put(field1.getFieldName(), field1);

        this.closeContextFields = new FieldMap();
        this.closeContextFields.put(field2.getFieldName(), field2);
        
//        org.exolab.castor.tools.MappingTool mapper = new MappingTool();
//        mapper.addClass(FieldChangeCreateObjectTrigger.class);
//        
//        mapper.write(new FileWriter("data/importvalidator.mapping.xml"));

        
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #ConditionalInsertTrigger()}.
     */
    @Test()
    public void testConditionalInsertTrigger() {
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        assertNotNull(cit, "Construictor failed");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #detectTriggeringConditions(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @SuppressWarnings("unchecked")
    @Test()
    public void testDetectTriggeringConditions() {
        boolean result = false;
        assertEquals(2, fields.size(), "Dropped an entry from the field map");
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        try {
            assertFalse(cit
                .detectTriggeringConditions(fields), "Detect should initially be false");
        } catch (RuntimeException e) {
           // should throw a runtime (null pointer) -can't use the trigger without list of fields.
            result = true;
        }
        assertTrue(result, "Using the trigger without setting the triggering fields should fail");
        HashMap<String, String> tempTriggers = new HashMap<String, String>();
        cit.setTriggeringFields(tempTriggers);
        assertFalse(cit
            .detectTriggeringConditions(fields), "Detect should be false with empty trigger list");
        tempTriggers.put(field1.getFieldName(), field1.getFieldData());
        assertFalse(cit
            .detectTriggeringConditions(fields), "Detect should not trigger with same data");
        field1.setFieldData(field1NewData);
        fields.put(field1.getFieldName(), field1);
        assertEquals(2, fields.size(), "Dropped an entry from the field map");
        assertTrue(cit
            .detectTriggeringConditions(fields), "Detect should trigger with new data");
    }
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testOpenContext() {
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        cit.setTriggeringFields(triggers);
        cit.setOpenTag(openTag);
        cit.setCloseTag(closeTag);
        String newContext = cit.openContext(openContext, fields);
        assertEquals(expectedOpenFieldlessContext, newContext, "Open context gave unexpected result");

    }
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #closeContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testCloseContext() {
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        cit.setTriggeringFields(triggers);
        cit.setOpenTag(openTag);
        cit.setCloseTag(closeTag);
        String newContext = cit.openContext(openContext, fields);
        assertEquals(expectedOpenFieldlessContext, newContext, "Open context gave unexpected result");
        newContext += cit.closeContext(openContext, fields);
        assertEquals(expectedClosedFieldlessContext, newContext, "Close context gave unexpected result");
        
        cit.setFieldMap(fields);
        cit.reset();
        newContext = cit.createObjectIfTriggered(fields, this.openContext);
        newContext += cit.closeContext(openContext, fields);
        assertEquals(expectedClosedAllFieldContext, newContext, "Wrong result for triggered fields");
        
        newContext = "";
        String origContext = "";
        cit.setFieldMap(fields);
        cit.reset();
        origContext = cit.createObjectIfTriggered(fields, this.openContext);

        //Modify fields so that it doesn't trigger
        for (Field f : fields.values()) {
            f.setFieldData("Changed one");
            break;
        }               
        cit.setFieldMap(fields);
        
        //Modify trigger as well so that it doesn't trigger actually
        HashMap<String, String> triggers1 = new HashMap<String, String>();
        triggers1.put(field1.getFieldName(), field1.getFieldData());
        cit.setTriggeringFields(triggers1);
        
        newContext = cit.createObjectIfTriggered(fields, this.openContext);
        //pass the modify fields but should still return the original value
        origContext += cit.closeContext(openContext, fields);
        assertEquals(expectedClosedAllFieldContext, origContext, "Wrong result for triggered fields");
        
        //RevertBackvalue for rest of the tests
        //Modify fields so that it doesn't trigger
        for (Field f : fields.values()) {
            f.setFieldData("original");
            break;
        }               

 
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #createObjectIfTriggered(com.vocollect.voicelink.core.importer.FieldMap, java.lang.String)}.
     */
    @Test()
    public void testCreateObjectIfTriggered() {
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        cit.setTriggeringFields(triggers);
        triggers.put(field1.getFieldName(), null);

        cit.setOpenTag(openTag);
        cit.setCloseTag(closeTag);
        String newContext = cit
                .createObjectIfTriggered(fields, this.openContext);
        
        assertEquals(expectedOpenFieldlessContext, newContext, "Open context gave unexpected result");
        newContext += cit.closeContext(openContext, fields);
        assertEquals(expectedClosedFieldlessContext, newContext, "Close context gave unexpected result");
        cit.setFieldMap(fields);
        cit.reset();
        newContext = cit.createObjectIfTriggered(fields, this.openContext);
        newContext += cit.closeContext(openContext, fields);
        assertEquals(expectedClosedAllFieldContext, newContext, "Wrong result for triggered fields");
    }
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #getOpenContextFields()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #setOpenContextFields(java.util.HashMap)}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #setOpenContextFields(java.util.HashMap)}.
     */
    @Test()
    public void testGetOpenContextFields() {
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        cit.setTriggeringFields(triggers);
        triggers.put(field1.getFieldName(), null);
        cit.setOpenTag(openTag);
        cit.setCloseTag(closeTag);
        assertNotNull(cit.getOpenContextFields(), "open context field list was null upon init");
        assertTrue(cit.getOpenContextFields().isEmpty(), "Open context field list was not empty on init");
        cit.setFieldMap(fields);
        assertEquals(openContextFields, cit.getOpenContextFields(), "Should only have the field1 field");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #getCloseContextFields()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #setCloseContextFields(java.util.HashMap)}.
     */
    @Test()
    public void testGetCloseContextFields() {
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        cit.setTriggeringFields(triggers);
        triggers.put(field1.getFieldName(), null);
        cit.setOpenTag(openTag);
        cit.setCloseTag(closeTag);
        assertNotNull(cit.getCloseContextFields(), "Close context field list was null upon init");
        assertTrue(cit.getCloseContextFields().isEmpty(), "Close context field list was not empty on init");
        cit.setFieldMap(closeContextFields);
        assertEquals(cit.getCloseContextFields(), closeContextFields, "Messed up list ");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #getTriggeringFields()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #setTriggeringFields(java.util.HashMap)}.
     */
    @Test()
    public void testGetTriggeringFields() {
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        cit.setTriggeringFields(triggers);
        assertEquals(triggers, cit.getTriggeringFields(), "messed up triggering fields");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger#getCloseTag()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger#setCloseTag(java.lang.String)}.
     */
    @Test()
    public void testGetCloseTag() {
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        cit.setCloseTag(closeTag);
        assertEquals(closeTag, cit.getCloseTag(), "Messed up close tag");

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger#getNewObjectName()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger#setNewObjectName(java.lang.String)}.
     */
    @Test()
    public void testGetNewObjectName() {
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        cit.setNewObjectName(this.objectName);
        assertEquals(objectName, cit.getNewObjectName(), "Messed up object name");
    }
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger#getOpenTag()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger#setOpenTag(java.lang.String)}.
     */
    @Test()
    public void testGetOpenTag() {
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        cit.setOpenTag(openTag);
        assertEquals(openTag, cit.getOpenTag(), "Messed up open tag");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger#isOpen()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger#setIsOpen(boolean)}.
     */
    @Test()
    public void testIsOpen() {
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        assertFalse(cit.isOpen(), "Default for isOpen() is true!");
        cit.setIsOpen(true);
        assertTrue(cit.isOpen(), "Set open to true and is false");
        cit.setIsOpen(false);
        assertFalse(cit.isOpen(), "Set open to false and is true");
        cit.setIsOpen(true);
        assertTrue(cit.isOpen(), "Set open to true and is false");
    }
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger#setUseAllFields()}.
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger#getUseAllFields()}.
     */
    @Test()
    public void testgetUseAllFields() {
        ConditionalInsertTrigger cit  = new ConditionalInsertTrigger();
        assertFalse(cit.getUseAllFields(), "default for useAllFields should be false");
        cit.setUseAllFields(true);
        assertTrue(cit.getUseAllFields(), "useAllFields should now be false");
        cit.setUseAllFields(false);
        assertFalse(cit.getUseAllFields(), "default for useAllFields should be false");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger#setIsOpen(boolean)}.
     */
    @Test()
    public void testSetIsOpen() {
        ConditionalInsertTrigger cit  = new ConditionalInsertTrigger();
        assertFalse(cit.isOpen(), "isOpen should initially be false false");
        cit.setIsOpen(true);
        assertTrue(cit.isOpen(), "isOpen should now return true");
        cit.setIsOpen(false);
        assertFalse(cit.isOpen(), "isOpen should now return false");
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #writeFields(com.vocollect.voicelink.core.importer.FieldMap, 
     * com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testWriteFields() {
        ConditionalInsertTrigger cit  = new ConditionalInsertTrigger();
        cit.setFieldMap(fields);
        String result = cit.writeFields(fields, fields);  // should write all fields
        assertEquals(this.expectedFieldoutput, result, "Field markup should equal the writeFields markup");
        FieldMap fm = new FieldMap();
        Field f = new Field();
        f.setFieldName("Other Field");
        fm.addField(f);
        cit.setUseAllFields(false);
        result = cit.writeFields(fm, fields);
        assertEquals("", result, "no field data should be present");
        result = cit.writeFields(fields, fm);
        assertEquals("", result, "no field data should be present");
        
        
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger#getFieldMap()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #setFieldMap(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testGetSetFieldMap() {
        ConditionalInsertTrigger cit  = new ConditionalInsertTrigger();
        assertNotNull(cit.getFieldMap(), "Field map should not be null");
        FieldMap fm = new FieldMap();
        cit.setFieldMap(fm);
        assertNotNull(cit.getFieldMap(), "FieldMap should not be null now.");
        assertEquals(fm, cit.getFieldMap(), "Should be the same field map I passed in.");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #getUseAllFields()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #setUseAllFields(boolean)}.
     */
    @Test()
    public void testGetSetUseAllFields() {
        ConditionalInsertTrigger cit  = new ConditionalInsertTrigger();
        assertFalse(cit.getUseAllFields(), "useAllFields should default to false.");
        cit.setUseAllFields(true);
        assertTrue(cit.getUseAllFields(), "useAllFields should be true now.");
        cit.setUseAllFields(false);
        assertFalse(cit.getUseAllFields(), "useAllFields should be false now.");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #setTriggeringFields(java.util.HashMap)}.
     */
    @Test()
    public void testSetTriggeringFields() {
        ConditionalInsertTrigger cit  = new ConditionalInsertTrigger();
        assertNull(cit.getTriggeringFields(), "Triggering fields not null  on init.");
        cit.setTriggeringFields(triggers);
        assertNotNull(cit.getTriggeringFields(), "Triggering fields should not be null after set");
        assertEquals(cit.getTriggeringFields(), triggers, "List of triggering fields changed by set.");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.ConditionalInsertTrigger
     * #lookupObject(java.lang.Object, com.vocollect.voicelink.core.importer.DBLookup)}.
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testLookupObject() {
        setUpDB();
        Operator            testOperator   = new Operator();
        List<Operator>                allOperators   = null;
        ConditionalInsertTrigger cit = new ConditionalInsertTrigger();
        PersistenceManager  pm  = new PersistenceManager();
        DBLookup            finder = new DBLookup(pm);        

        //Get all the operator
        try {
            allOperators = pm.getAll(Operator.class);
        } catch (DataAccessException e) {
            assertTrue(false, "getting all operators threw an exception");
        }

        // Set the operator id to something we know is in the database
        testOperator.setOperatorIdentifier((allOperators.get(0)).getOperatorIdentifier());
        cit.setFinderName("findByIdentifier");
        cit.setNewObjectName("Operator");
        Field aField = new Field();
        aField = setupField(aField, "operatorIdentifier", testOperator.getOperatorIdentifier());
        cit.addField(aField);
        
        Field password = new Field();
        password.setFieldName("password");
        password = setupField(password, "password", "");
        password.setKeyField(false);
        cit.addField(password);
        cit.setUseAllFields(true);
        cit.setMainObject(true);


        Object obj = null;
        Operator op = null;
        try {
            // This lookup finds by ID. This return the object from the database
            obj = cit.lookupObject(testOperator, finder);
        } catch (VocollectException e1) {
            assertTrue(false, "lookup threw an exception");
        }
        
        assertNotNull(obj, "drew a null with the lookup");
        
        try {
            op = (Operator) obj;
        } catch (ClassCastException e) {
            assertTrue(false, "retrieved object is not an operator");
        }
        assertEquals(testOperator.getOperatorIdentifier(), 
                op.getOperatorIdentifier(), "Got a different operator");
        
    }
    
    /**
     * To setup the database
     * used only in on method.
     */
    private void setUpDB() {
        DbUnitAdapter adapter = new DbUnitAdapter();
        try {
            adapter.resetInstallationData();
            System.err.println("initializing small sample");
            adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkDataSmall.xml");
            System.err.println("initializing operators");
            adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkSampleOperators.xml");
            System.err.println("DB Initialization complete");
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse(true, "Threw an exception setting up database");
        }

    }
    
    /**
     * Set up the field passed in with the data passed in. Just to clean up the setup code.
     * @param fld field.
     * @param fieldName as it says
     * @param fieldData as it says
     * @return the filled in field.
     */
    private Field setupField(Field fld, String fieldName, String fieldData) {
        fld.setFieldName(fieldName);
        fld.setFieldData(fieldData);
        fld.setOpenTag("<" + fieldName + ">");
        fld.setCloseTag("</" + fieldName + ">");
        fld.setFieldData(fieldData);
        fld.setKeyField(true);
        return fld;
    }

    
    /**
     * Test that the markup we use is good.
     */
    @Test()
    public void testConfigSetup() {
        CastorConfiguration cc = new CastorConfiguration("import-definition.xml", "ConditionalInsertTrigger.xml");
        Object obj = null;
        try {
            obj = cc.configure();
        } catch (ConfigurationException e1) {
            fail("Mapping and markup seem to be at odds.");
        }
        ConditionalInsertTrigger e = (ConditionalInsertTrigger) obj; 
        
        assertEquals(2, e.getFieldMap().size(), "There should be two fields in the trigger fields");
        
        assertNotNull(e.getFieldMap().get("operatorIdentifier"));
        assertNotNull(e.getFieldMap().get("operatorIdentifier"));
        assertNotNull(e.getFieldMap().get("password"));
        assertNotNull(e.getFieldMap().get("password"));
        
        assertTrue((e.getFieldMap().get("operatorIdentifier")).isKeyField(), 
            "Field named 'operatorIdentifier' should be a lookup field");
        

    }

}    
