/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.exceptions.VocollectException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * @author astein
 */

public class FixedLengthFieldTest {

    private FixedLengthField f = null;

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeClass
    public void setUp() throws Exception {
        f = new FixedLengthField();
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FixedLengthField#FixedLengthField()}.
     */
    @Test
    public final void testFixedLengthField() {
        f = new FixedLengthField();
        assertNotNull(f.getEnd());
        assertNotNull(f.getLength());
        assertNotNull(f.getStart());
        assertEquals(f.getLength(), 0);
        assertEquals(f.getEnd(), 0);
        assertEquals(f.getStart(), 0);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FixedLengthField#FixedLengthField(int, int)}.
     */
    @Test
    public final void testFixedLengthFieldIntInt() {
        try {
            f = new FixedLengthField(10, 20);
            assertEquals(f.getStart(), 10);
            assertEquals(f.getLength(), 20);
            assertEquals(f.getEnd(), 30);
        } catch (VocollectException e) {
            fail("caught VocollectException: " + e.getMessage());
        }
        // now test VocollectException
        try {
            f = new FixedLengthField(10, -2);
        } catch (VocollectException e) {
            assertTrue(true);
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FixedLengthField#getEnd()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FixedLengthField#setEnd(int)}.
     */
    @Test
    public final void testGetSetEnd() {
        f = new FixedLengthField();
        assertEquals(f.getEnd(), 0);
        f.setEnd(30);
        assertEquals(f.getEnd(), 30);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FixedLengthField#getLength()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FixedLengthField#setLength(int)}.
     */
    @Test
    public final void testGetSetLength() {
        f = new FixedLengthField();
        assertEquals(f.getLength(), 0);
        f.setLength(20);
        assertEquals(f.getLength(), 20);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FixedLengthField#getStart()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FixedLengthField#setStart(int)}.
     */
    @Test
    public final void testGetSetStart() {
        f = new FixedLengthField();
        assertEquals(f.getStart(), 0);
        f.setStart(10);
        assertEquals(f.getStart(), 10);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FixedLengthField#valid()}.
     */
    @Test
    public final void testValid() {
        try {
            assertTrue(f.valid(), "FixedLengthField should be valid.");
            f = new FixedLengthField(0, 0);
            f.setLength(-1);
            assertFalse(f.valid(), "FixedLengthField should not be valid.");
            // set back to valid
            f.setLength(0);
            assertTrue(f.valid(), "FixedLengthField should now be valid.");
            f.setStart(10);
            f.setEnd(5);
            assertFalse(f.valid(), "FixedLengthField should not be valid.");
        } catch (VocollectException e) {
            e.printStackTrace();
        }
    }
}
