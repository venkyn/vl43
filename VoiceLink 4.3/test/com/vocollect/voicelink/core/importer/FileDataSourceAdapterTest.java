/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.adapters.FileDataSourceAdapter;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * @author astein
 */
public class FileDataSourceAdapterTest {

    private FileDataSourceAdapter myAdapter = null;

    private String                testDir   = null;

    private String             testURLDir   = null;

    /**
     *
     *
     */
    @BeforeMethod
    protected void setUp() {
        if (System.getProperty("test.input.dir") != null) {
            testDir = System.getProperty("test.input.dir") + "/";
        } else {
            testDir = "./";
            testURLDir = "http://" + "localhost" + ":" + "8080"
                + "/VoiceLink/dev/importexport/main/data/import";
        }

        try {
            myAdapter = new FileDataSourceAdapter();
            myAdapter.setParentDirectory(testDir + "data/import/");
            myAdapter.setFileNamePattern("test.dat");
            myAdapter.setEncoding(EncodingSupport.DEFAULT_ENCODING);
        } catch (Exception e) {
            org.testng.AssertJUnit.assertTrue("Constructor failed", false);
        }
        File whereAmI = new File("test.config");
        try {
            System.out.println("getCanonicalPath"
                + whereAmI.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *Test method for
     *'com.vocollect.voicelink.core.importer
     *          .FileDataSourceAdapter.isEndOfRecord()'.
     */
    @Test()
    public void testIsEndOfRecord() {
        assertTrue("isEndOfRecord method for FileDataSourceAdapter "
               + "is not returning correctly.", myAdapter.isEndOfRecord());
    }

    /**
     *Test method for
     *'com.vocollect.voicelink.core
     *      .importer.FileDataSourceAdapter.openSource()'.
     */
    @Test()
    public void testOpenSource() {
        System.err.println("testOpenSource");
        try {
            myAdapter.setParentDirectory(testDir + "data/import/");
        } catch (ConfigurationException e1) {
            e1.printStackTrace();
        }
        if (!myAdapter.isOpen()) {
            try {
                assertTrue("open source failed, looking for "
                    + myAdapter.getFullFilename(), myAdapter.openSource());
            } catch (NoWorkException e) {
                assertTrue("NoWorkException Thrown", true);
            } catch (VocollectException e) {
                assertTrue("VocollectException Thrown", true);
                e.printStackTrace();
            }
        }
        assertTrue("Source open, no data read yet, should be end of record",
                myAdapter.isEndOfRecord());
        // assertFalse( "Source open, no data read yet, should not be end of
        // data", myAdapter.isEndOfData());
        assertTrue("Source open, no data read yet, should show open", myAdapter
                .isOpen());
    }

    /**
     * 
     */
    @Test()
    public void testURLOpenSource() {
        System.err.println("testURLOpenSource");
        try {
            String inputLine;
            URL myUrl = new URL(testURLDir + "/Test.dat");
            BufferedReader in = new BufferedReader(
                new InputStreamReader(myUrl.openStream()));
            while ((inputLine = in.readLine()) != null) {
                System.out.println(inputLine);
            }
        } catch (IOException e) {
            assertTrue("IOException Thrown", true);
        }
    }


    /**
     *Test method for
     *'com.vocollect.voicelink.core.importer.FileDataSourceAdapter.read()'.
     */
    @Test()
    public void testRead() {

        System.err.println("testRead");
        try {
            myAdapter.setParentDirectory(testDir + "data/import");
        } catch (ConfigurationException e1) {
            e1.printStackTrace();
        }
        if (!myAdapter.isOpen()) {
            try {
                myAdapter.openSource();
            } catch (NoWorkException e) {
                assertTrue("NoWorkException Thrown", true);
            } catch (VocollectException e) {
                e.printStackTrace();
                fail("VocollectException thrown: " + e.getMessage());
            }
        }

        // Read before readRecord should return false
        try {
            assertEquals("Read before readRecord should return false",
                    myAdapter.read(), 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Read a record, then read should return a char
        assertTrue("readRecord failed", myAdapter.readRecord());
        try {
            int temp = myAdapter.read();
            System.err.println(temp);
            assertTrue("Premature end of data", temp != 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            char[] cbuf = new char[1000];
            myAdapter.read(cbuf, 0, myAdapter.getBufferedCharsRemaining());
            for (int i = 0; i < myAdapter.getBufferedCharsRemaining(); ++i) {
                System.err.print(cbuf[i]);
            }
            System.err.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int lines = 0;
        char[] cbuf = new char[1000];
        myAdapter.readRecord();
        while (!myAdapter.isEndOfData()) {
            try {
                myAdapter.read(cbuf, 0, myAdapter.getBufferedCharsRemaining());
            } catch (IOException e) {
                e.printStackTrace();
            }
            lines++;
            myAdapter.readRecord();
        }
        System.err.println("Lines: " + lines);
    }

    /**
     *Test method for
     *'com.vocollect.voicelink.core
     *          .importer.FileDataSourceAdapter.isEndOfData()'.
     */
    @Test()
    public void testIsEndOfData() {
        while (!myAdapter.isEndOfData()) {
            assertFalse(
                    "isEndOfData method in FileDataSourceAdapter "
                    + "is not returning correctly.",
                    myAdapter.isEndOfData());
            myAdapter.readRecord();
        }
        assertTrue(
                "isEndOfData method in FileDataSourceAdapter "
                + "is not returning correctly.",
                myAdapter.isEndOfData());
    }

    /**
     *Test method for
     *'com.vocollect.voicelink.core
     *          .importer.FileDataSourceAdapter.FileDataSourceAdapter()'.
     */
    @Test()
    public void testFileDataSourceAdapter() {
        assertEquals(
                "FileDataSourceAdapter empty constructor "
                + "not initializing correctly.",
                myAdapter.getCurrentState(),
                DataSourceAdapter.INITIALIZEDSTATE);
    }

    /**
     *Test method for
     *'com.vocollect.voicelink.core.importer
     *          .FileDataSourceAdapter.getParentDirectory()'.
     *Test method for
     *'com.vocollect.voicelink.core
     *          .importer.FileDataSourceAdapter.setParentDirectory(String)'
     */
    @Test()
    public void testGetSetParentDirectory() {
        try {
            myAdapter.setParentDirectory(testDir + "data");
            assertTrue(
                    "Setting parent directory not "
                    + "working with FileDataSourceAdapter.",
                    myAdapter.getParentDirectory().equals(testDir + "data"));
            myAdapter.setParentDirectory(testDir + "data/import");
            assertTrue(
                    "Getting parent directory not "
                    + "working with FileDataSourceAdapter.",
                    myAdapter.getParentDirectory().equals(
                            testDir + "data/import"));
            myAdapter.setParentDirectory(testDir + "data/test/abc");
            assertTrue(
                    "Getting parent directory not "
                    + "working with FileDataSourceAdapter.",
                    myAdapter.getParentDirectory().equals(
                            testDir + "data/test/abc"));
            try {
                myAdapter.setParentDirectory(null);    
            } catch (ConfigurationException e) {
                assertTrue("Parent Directory could not be created as passed as empty string", true);   
            }
            
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     *Test method for
     *'com.vocollect.voicelink.core.importer
     *  .FileDataSourceAdapter.getFullFilename()'.
     */
    @Test()
    public void testGetFullFilename() {
        try {
            myAdapter.setParentDirectory(testDir + "data/import/");
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
        assertTrue(
                "Getting full filename not working with FileDataSourceAdapter.",
                myAdapter.getFullFilename()
                        .startsWith(testDir + "data/import/"));
    }

    /**
     *Test method for
     *'com.vocollect.voicelink.core.importer
     *        .FileDataSourceAdapter.getLineLength()'.
     *Test method for
     *'com.vocollect.voicelink.core
     *        .importer.FileDataSourceAdapter.setLineLength(int)'
     */
    @Test()
    public void testGetSetLineLength() {
        myAdapter.setLineLength(48);
        assertTrue(
                "Getting linelength not working with FileDataSourceAdapter.",
                myAdapter.getLineLength() == 48);
    }
}
