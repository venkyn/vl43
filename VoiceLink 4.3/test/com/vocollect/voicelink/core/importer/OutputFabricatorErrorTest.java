/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;

/**
 * @author Kalpna
 */
public class OutputFabricatorErrorTest {

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.OutputFabricatorError'.
     */
    @Test()
    public void testOutputFabricatorError() {
        try {
            OutputFabricatorError.CLOSE_CONNECTION_ERROR.toString();
        } catch (RuntimeException e) {
            assertFalse(true , e.getMessage());
        }
        // This checks that the error codes are unique within the
        // OutputFabricatorError and are within bounds
    }

}
