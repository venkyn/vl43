/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidatorContext;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;


/**
 * @author astein
 */
public class ErrorRecordTest {

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeClass
    public void setUp() throws Exception {
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.ErrorRecord#ErrorRecord()}.
     */
    @Test
    public final void testErrorRecord() {
        // nothing to test...
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.ErrorRecord
     * #ErrorRecord(com.opensymphony.xwork2.validator.ValidatorContext, java.lang.Object, long, java.lang.String)}.
     */
    @Test
    public final void testErrorRecordValidatorContextObjectLongString() {
        Object newGuy = new Object();
        ValidatorContext vcontext = new DelegatingValidatorContext(newGuy);
        ErrorRecord errorRecord = null;
        errorRecord = new ErrorRecord(vcontext, newGuy, 1, "thedatasource");
        assertNotNull(errorRecord.getDataSource(),
                "Created the ErrorRecord object incorrectly.");
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.ErrorRecord#getBadguy()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.ErrorRecord#setBadguy(java.lang.Object)}.
     */
    @Test
    public final void testGetSetBadguy() {
        ErrorRecord errorRecord = new ErrorRecord();
        String thebadguy = "badguy";
        errorRecord.setBadguy(thebadguy);
        assertEquals(thebadguy, errorRecord.getBadguy().toString());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.ErrorRecord#getDataSource()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.ErrorRecord#setDataSource(java.lang.String)}.
     */
    @Test
    public final void testGetSetDataSource() {
        ErrorRecord errorRecord = new ErrorRecord();
        String thedatasource = "datasource";
        errorRecord.setDataSource(thedatasource);
        assertEquals(thedatasource, errorRecord.getDataSource());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.ErrorRecord#getErrors()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.ErrorRecord#setErrors(java.util.Map)}.
     */
    @SuppressWarnings("unchecked")
    @Test
    public final void testGetSetErrors() {
        ErrorRecord errorRecord = new ErrorRecord();
        Map errorMap = new HashMap();
        errorMap.put("error", "Freddie Sanchez");
        errorRecord.setErrors(errorMap);
        assertFalse("Getting or setting the Errors didn't work.", errorRecord
                .getErrors().isEmpty());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.ErrorRecord#getLineNumber()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.ErrorRecord#setLineNumber(long)}.
     */
    @Test
    public final void testGetSetLineNumber() {
        long l = 350L;
        ErrorRecord errorRecord = new ErrorRecord();
        errorRecord.setLineNumber(l);
        assertEquals(l, errorRecord.getLineNumber());

    }
}
