/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

/**
 * @author astein
 */
public class FieldMapTest {

    private FieldMap fm = null;

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeClass
    public void setUp() throws Exception {
        this.fm = new FixedLengthFieldMap();
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FieldMap#FieldMap(int)}.
     */
    @Test
    public final void testFieldMapInt() {
        // ???
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FieldMap#addField(com.vocollect.voicelink.core.importer.Field)}.
     */
    @SuppressWarnings("unchecked")
    @Test
    public final void testAddField() {
        FixedLengthField aField = new FixedLengthField();
        aField.setFieldName("Field_1");
        aField.setFieldData("testing field 1");
        fm.addField(aField);
        assertTrue("AddField method didn't populate the FieldMap correctly.",
                fm.containsKey("Field_1"));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FieldMap#getStartingPosition()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FieldMap#setStartingPosition(int)}.
     */
    @Test
    public final void testGetSetStartingPosition() {
        fm.setStartingPosition(38);
        assertTrue("SetStartingPosition method not working.", fm
                .getStartingPosition() == 38);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.FieldMap#values()}.
     */
    @SuppressWarnings("unchecked")
    @Test
    public final void testValues() {
        FixedLengthField aField = new FixedLengthField();
        aField.setFieldName("Field_2");
        aField.setFieldData("testing field 2");
        fm.addField(aField);
        assertTrue("Values method didn't return the FieldMap correctly.", !fm
                .values().isEmpty());
    }

}
