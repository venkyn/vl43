/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.parsers;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.CastorConfiguration;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.DataSourceAdapter;
import com.vocollect.voicelink.core.importer.NoWorkException;
import com.vocollect.voicelink.core.importer.OutOfDataException;
import com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;


/**
 * @author dgold
 *
 */
public class PurgeParserTest {

    private String                testDir   = null;

    private String             parentDirectory    = null;

    /**
     * {@inheritDoc}
     * @throws java.lang.Exception
     */
    @BeforeMethod()
    protected void setUp() throws Exception {
        if (System.getProperty("test.input.dir") != null) {
            testDir = System.getProperty("test.input.dir") + "/";
        } else {
            testDir = "./";
        }
        parentDirectory = testDir + "data/purge/Default/";
        File junkDir = new File(parentDirectory);
        if (junkDir.exists()) {
            FilePurgeAdapter tfpa = new FilePurgeAdapter();
            tfpa.addDirectory(parentDirectory);
            tfpa.setFileNamePattern("junk.*dat");
            boolean delFiles = true;
            try {
                tfpa.openSource();
            } catch (NoWorkException n) {
                delFiles  = false;
            }
            
            if (delFiles) {
                while (tfpa.readRecord()) {
                    // nothing to do
                }
            }
            junkDir.delete();
        }
    }


    /**
     * @param numberOfFiles the number of files
     */
    private void setUpFiles(int numberOfFiles) {

        String fnameRoot = "junk";
        String ext = ".dat";
        String fname = "";
        File junk = new File(parentDirectory);
        junk.mkdirs();
        for (int i = 0; i < numberOfFiles; i++) {
            fname = fnameRoot + i;
            junk = new File(parentDirectory + fname + ext);
            try {
                junk.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                fail("Unable to create junk file " + fname + " to delete");
            }
        }
    }

    /**
     * @return - DataSourceAdapter
     * @throws VocollectException
     */
    DataSourceAdapter setUpPurgeAdapter() throws VocollectException {
        FilePurgeAdapter tfpa = new FilePurgeAdapter(); 
        tfpa.addDirectory(parentDirectory);
        tfpa.setFileNamePattern("junk.*dat");
        return tfpa;
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.parsers.PurgeParser#getRecord()}.
     */
    @Test()
    public void testGetRecord() throws VocollectException {
        PurgeParser pp = new PurgeParser();
        
        pp.setSourceAdapter(setUpPurgeAdapter());
        assertNull(pp.getRecord());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.parsers.PurgeParser#configure()}.
     */
    @Test()
    public void testConfigure()  throws VocollectException {
        PurgeParser pp = new PurgeParser();
        
        pp.setSourceAdapter(setUpPurgeAdapter());

        assertTrue(pp.configure(), "configure returned false");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.parsers.PurgeParser#isOutOfData()}.
     */
    @Test()
    public void testIsOutOfData()   throws VocollectException {
        PurgeParser pp = new PurgeParser();
        
        pp.setSourceAdapter(setUpPurgeAdapter());

        assertTrue(pp.isOutOfData());
        setUpFiles(1);
        
        boolean doneRight = false;
        try {
            pp.getElement();    
        } catch (OutOfDataException o) {
            doneRight = true;
        }
        assertTrue(doneRight, "failed to process all files");
        assertTrue(pp.isOutOfData());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.parsers.PurgeParser#getElement()}.
     */
    @Test()
    public void testGetElement() throws VocollectException {
        PurgeParser pp = new PurgeParser();
        
        pp.setSourceAdapter(setUpPurgeAdapter());

        assertTrue(pp.isOutOfData());
        setUpFiles(0);
        
        boolean doneRight = false;
        try {
            pp.getElement();    
        } catch (NoWorkException o) {
            doneRight = true;
        }
        assertTrue(doneRight, "failed to process all files");
        assertTrue(pp.isOutOfData());

        setUpFiles(1);
        
        doneRight = false;
        try {
            pp.getElement();    
        } catch (OutOfDataException o) {
            doneRight = true;
        }
        assertTrue(doneRight, "failed to process all files");
        assertTrue(pp.isOutOfData());

        setUpFiles(10);
        
        doneRight = false;
        try {
            pp.getElement();    
        } catch (OutOfDataException o) {
            doneRight = true;
        }
        assertTrue(doneRight, "failed to process all files");
        assertTrue(pp.isOutOfData());

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.parsers.PurgeParser#getRecordsRead()}.
     */
    @Test()
    public void testGetRecordsRead() throws VocollectException {
        PurgeParser pp = new PurgeParser();
        
        pp.setSourceAdapter(setUpPurgeAdapter());

        assertTrue(pp.isOutOfData());
        setUpFiles(0);
        
        boolean doneRight = false;
        try {
            pp.getElement();    
        } catch (NoWorkException o) {
            doneRight = true;
        }
        assertTrue(doneRight, "failed to process all files");
        assertTrue(pp.isOutOfData());
        assertEquals(pp.getRecordsRead(), 0, "Wrong record count");

        setUpFiles(1);
        FilePurgeAdapter fpa = (FilePurgeAdapter) pp.getSourceAdapter();
        fpa.addDirectory(parentDirectory);
        
        doneRight = false;
        try {
            pp.getElement();    
        } catch (OutOfDataException o) {
            doneRight = true;
        }
        assertTrue(doneRight, "failed to process all files");
        assertTrue(pp.isOutOfData());
        assertEquals(pp.getRecordsRead(), 1, "Wrong record count");

        setUpFiles(10);
        fpa.addDirectory(parentDirectory);
        
        doneRight = false;
        try {
            pp.getElement();    
        } catch (OutOfDataException o) {
            doneRight = true;
        }
        assertTrue(doneRight, "failed to process all files");
        assertTrue(pp.isOutOfData());
        // Record count is cumulative, throughout all directories served.
        assertEquals(pp.getRecordsRead(), 11, "Wrong record count");

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.parsers.PurgeParser#PurgeParser()}.
     */
    @Test()
    public void testPurgeParser() throws VocollectException {
        new PurgeParser();
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.parsers.PurgeParser#getNextField()}.
     */
    @Test()
    public void testGetNextField() throws VocollectException {
        PurgeParser pp = new PurgeParser();
        pp.getNextField();
    }

    /**
     * Test that the markup we use is good.
     */
  
    @Test()
    public void testConfigSetup() throws VocollectException {
        CastorConfiguration cc = new CastorConfiguration("import-definition.xml", "PurgeParserSetup.xml");
        Object obj = null;
        try {
            obj = cc.configure();
        } catch (ConfigurationException e1) {
            fail("Mapping and markup seem to be at odds.");
        }

        PurgeParser pp = (PurgeParser) obj;
        
        assertTrue(pp.isOutOfData());
        setUpFiles(0);
        
        boolean doneRight = false;
        try {
            pp.getElement();    
        } catch (NoWorkException o) {
            doneRight = true;
        }
        assertTrue(doneRight, "failed to process all files");
        assertTrue(pp.isOutOfData());

        setUpFiles(1);
        
        doneRight = false;
        try {
            pp.getElement();    
        } catch (OutOfDataException o) {
            doneRight = true;
        }
        assertTrue(doneRight, "failed to process all files");
        assertTrue(pp.isOutOfData());

        setUpFiles(10);
        
        doneRight = false;
        try {
            pp.getElement();    
        } catch (OutOfDataException o) {
            doneRight = true;
        }
        assertTrue(doneRight, "failed to process all files");
        assertTrue(pp.isOutOfData());
    }

}
