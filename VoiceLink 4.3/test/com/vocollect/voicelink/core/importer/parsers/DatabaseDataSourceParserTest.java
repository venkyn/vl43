/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.parsers;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.test.DepInjectionSpringContextNGTests;
import com.vocollect.voicelink.core.importer.BaseImportServiceManagerTestCase;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.NoWorkException;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.OutOfDataException;
import com.vocollect.voicelink.test.DbUnitAdapterVoiceLink;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * 
 *
 * @author jtauberg
 */
public class DatabaseDataSourceParserTest extends DepInjectionSpringContextNGTests {
 
    protected static final String DATA_DIR = "data/dbunit/voicelink/";

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.test.DepInjectionSpringContextNGTests#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return BaseImportServiceManagerTestCase.getImportTestConfigLocations();
    }


    /**
     * setUpLocal is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void setUpLocal() throws Exception {
        
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();         
        adapter.resetInstallationData();
        adapter.loadFullMultiSiteData();

        System.err.println("DB Initialization complete");
    }
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.parsers.DatabaseDataSourceParser#configure()}.
     */
    @Test()
    public void testConfigure() {
        DatabaseDataSourceParser test = null;
        try {
            test = new DatabaseDataSourceParser();
        } catch (ConfigurationException e) {
            fail("DatabaseDataSourceParser Constructor threw exception.");
        }

        try {
            test.setMappingFileName("import-field-mapping.xml");
            test.setConfigFileName("export-operator-fields.xml");
            test.configure();
        } catch (ConfigurationException e) {
            fail("DatabaseDataSourceParser configure method threw exception.");
        }
        assertFalse(test.getFieldList().isEmpty(), "Empty field list after configuration!");
    }
 
    /**
     * Test method for 
     * {@link com.vocollect.voicelink.core.importer.parsers.DatabaseDataSourceParser#lookup
     * (com.vocollect.epp.model.DataObject, java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)}.
     * This tests based on the DataObject
     */
    @Test()
    public void testLookup1() {
        DatabaseDataSourceParser ddsp = null;
        FieldMap myFieldMap = new FieldMap();
        Field myField = new Field();
        myField.setFieldName("number");
        myField.setFieldData("99838");
        myField.setKeyField(true);
        myFieldMap.addField(myField);
        try {
            ddsp = new DatabaseDataSourceParser();
            ddsp.setMappingFileName("import-field-mapping.xml");
            ddsp.setConfigFileName("export-PickResultsMarkout-fields.xml");
            ddsp.configure();
        } catch (ConfigurationException e) {
            fail("DatabaseDataSourceParser Constructor threw exception.");
        }
        //ddsp.setQueryAll(false);
        ddsp.setKeyFields(myFieldMap);
        ddsp.setQueryName("findItemByNumber");
        ddsp.setObjectNameString(true);
        ddsp.setQueryResultObjectName("com.vocollect.voicelink.core.model.Item");
        ddsp.lookup();
    }


    /**
     * Test method for 
     * {@link com.vocollect.voicelink.core.importer.parsers.DatabaseDataSourceParser#lookupAll
     * (com.vocollect.epp.model.DataObject)}.
     * This is just for initial testing. lookupAll is made private now.
     * In order to do lookUpAll change in the databaseDataSourceParser 
     * make the queryAll and lookUpAll public and then run it.
     */
    /*@Test()
    
    public void testLookupAll() {
        PickExportTransport testPick = new PickExportTransport();
        List<Object> resultList = null;
        DatabaseDataSourceParser ddsp = null;
        assertNull(resultList, "resultList set to null, but is not null... wow, that's wierd.");
        try {
            ddsp = new DatabaseDataSourceParser();
            ddsp.setMappingFileName("import-field-mapping.xml");
            ddsp.setConfigFileName("export-PickResultsMarkout-fields.xml");
            ddsp.configure();
        } catch (ConfigurationException e) {
            fail("DatabaseDataSourceParser Constructor threw exception.");
        }
        ddsp.setQueryAll(true);
        
        try {
            resultList = ddsp.lookupAll(testPick);
        } catch (DataAccessException e) {
            fail("lookupAll threw a DataAccessException.");
        }
        assertFalse(resultList.isEmpty(), "LookupAll did not return any records!");
    }*/


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.parsers.DatabaseDataSourceParser#getRecord()}.
     * test for lookup without setting up the object
     */
    @Test()
    public void testGetRecord() {
        ObjectContext<?, ?> objectContext = null;
        DatabaseDataSourceParser ddsp = null;        
        try {
            ddsp = new DatabaseDataSourceParser();
            ddsp.setMappingFileName("import-field-mapping.xml");
            ddsp.setConfigFileName("export-PickResultsMarkout-fields.xml");
            ddsp.configure();
        } catch (ConfigurationException e) {
            fail("DatabaseDataSourceParser Constructor threw exception.");
        }       
        ddsp.setSiteName("Default");
 
        //Try to lookup object without setting up keyField, Object and queryName
        try {
            objectContext = ddsp.getRecord();    
            objectContext.getFieldMaps().get(0);
        } catch (OutOfDataException e) {
            // Occasional
        } catch (VocollectException e) {
            fail("getRecord caused a VocollectException: should have thrown RuntimeException here " + e.getMessage());
        } catch (RuntimeException e) {
            assertTrue(true, " We are trying to do the look up without setting up the object");
        }        
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.parsers.DatabaseDataSourceParser#getRecord()}.
     * This calls the lookUp to get the record
     */
    @Test()
    public void testGetRecord1() {
        FixedLengthFieldMap myFieldMap = null;
        DatabaseDataSourceParser ddsp = null;  
        ObjectContext<?, ?> objectContext = null;
        try {
            ddsp = new DatabaseDataSourceParser();
            ddsp.setMappingFileName("import-field-mapping.xml");
            ddsp.setConfigFileName("export-PickResultsMarkout-fields.xml");
            ddsp.configure();
        } catch (ConfigurationException e) {
            fail("DatabaseDataSourceParser Constructor threw exception.");
        }                
        
        ddsp.setQueryName("listPickedPicks");
        ddsp.setObjectNameString(true);
        ddsp.setQueryResultObjectName("com.vocollect.voicelink.selection.model.PickExportTransport");
        ddsp.setSiteName("Default");


        try {
            objectContext = ddsp.getRecord();
            myFieldMap = (FixedLengthFieldMap) objectContext.getFieldMaps().get(0);
                 System.out.println(myFieldMap.toString());
        } catch (NoWorkException e) {
            // Occasional
            return;
        } catch (VocollectException e) {
            fail("getRecord caused a VocollectException:  " + e.toString());
        }
        assertNotNull(myFieldMap, "FixedLengthFieldMap was empty after getRecord!");        
    }

    /**
     * Test method for 
     * {@link com.vocollect.voicelink.core.importer.parsers.DatabaseDataSourceParser#getField(java.lang.String)}.
     */
    @Test()
    public void testGetField() {
        ObjectContext<?, ?> objectContext = null;
        Field myField = null;
        DatabaseDataSourceParser ddsp = null;

        try {
            ddsp = new DatabaseDataSourceParser();
            ddsp.setMappingFileName("import-field-mapping.xml");
            ddsp.setConfigFileName("export-PickResultsMarkout-fields.xml");
            ddsp.configure();
        } catch (ConfigurationException e) {
            fail("DatabaseDataSourceParser Constructor threw exception.");
        }
        //ddsp.setQueryAll(false);
        ddsp.setQueryName("listPickedPicks");
        ddsp.setObjectNameString(true);
        ddsp.setQueryResultObjectName("com.vocollect.voicelink.selection.model.PickExportTransport");
        ddsp.setSiteName("Default");

        try {
            objectContext = ddsp.getRecord();
            objectContext.getFieldMaps().get(0);
        } catch (NoWorkException e) {
            // Occasional
            return;
        } catch (Exception e) {
            e.printStackTrace();
            fail("getRecord caused an unexpected Exception:  " + e.toString());
        }
        try {
            myField = ddsp.getField("RecordType");
        } catch (Exception e) {
            fail("getField threw an unexpeted exception: " + e.toString());
        }
        assertNotNull(myField, "Field was empty after getField!");
        assertEquals(myField.getFieldData().trim(),
            "MARKOUT", "getField did not contain the proper RecordType value MARKOUT.");
        assertEquals(myField.getFieldName().trim(),
            "RecordType", "getField did not contain the proper fieldName RecordType.");
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.parsers.DatabaseDataSourceParser#getNextField()}.
     */
    @Test()
    public void testGetNextField() {
        
        ObjectContext<?, ?> objectContext = null;
        Field myField = null;
        DatabaseDataSourceParser ddsp = null;
        
        try {
            ddsp = new DatabaseDataSourceParser();
            ddsp.setMappingFileName("import-field-mapping.xml");
            ddsp.setConfigFileName("export-PickResultsMarkout-fields.xml");
            ddsp.configure();
        } catch (ConfigurationException e) {
            fail("DatabaseDataSourceParser Constructor threw exception.");
        }
        //ddsp.setQueryAll(false);
        ddsp.setQueryName("listPickedPicks");
        ddsp.setObjectNameString(true);
        ddsp.setQueryResultObjectName("com.vocollect.voicelink.selection.model.PickExportTransport");
        ddsp.setSiteName("Default");
        
        try {
            objectContext = ddsp.getRecord();
            objectContext.getFieldMaps().get(0);
        } catch (NoWorkException e) {
            // Occasional
            return;
        } catch (Exception e) {
            fail("getRecord caused an unexpected Exception:  " + e.toString());
        }
        try {
            myField = ddsp.getNextField();
        } catch (OutOfDataException e) {
            // Occasional
            return;
        } catch (Exception e) {
            fail("getNextField threw an unexpeted exception: " + e.toString());
        }
        assertNotNull(myField, "Field was empty after getNextField!");
        assertEquals(myField.getFieldData().trim(), "MARKOUT", "The field returned wasn't the one expected.");
        try {
            myField = ddsp.getNextField();
            myField = ddsp.getNextField();
            System.out.println(myField);
        } catch (OutOfDataException e) {
            // Occasional
        } catch (Exception e) {
            fail("getNextField threw an unexpeted exception: " + e.toString());
        }
        assertNotNull(myField.getFieldData(), "get next field data was unexpectedly null.");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.parsers.DatabaseDataSourceParser#isOutOfData()}.
     */
    @Test(enabled = false)
    public void testIsOutOfData() {
        ObjectContext<?, ?> objectContext = null;
        DatabaseDataSourceParser ddsp = null;
        try {
            ddsp = new DatabaseDataSourceParser();
            ddsp.setMappingFileName("import-field-mapping.xml");
            ddsp.setConfigFileName("export-PickResultsMarkout-fields.xml");
            ddsp.configure();
        } catch (ConfigurationException e) {
            fail("DatabaseDataSourceParser Constructor threw exception.");
        }
        //ddsp.setQueryAll(false);
        ddsp.setQueryName("listCompletedPicks");
        ddsp.setObjectNameString(true);
        ddsp.setQueryResultObjectName("com.vocollect.voicelink.selection.model.PickExportTransport");
        ddsp.setSiteName("Default");

        int x;
        ddsp.setSiteName("Default");
        try {
            objectContext = ddsp.getRecord();
            objectContext.getFieldMaps().get(0);
        } catch (NoWorkException e) {
            // Occasional
        } catch (Exception e) {
            fail("getRecord caused an Exception:  " + e.toString());
        }
        
        //iterate through the records and count them... there are 17 records.. count should be 17
        int recordcount = 0;
        for (x = 1, recordcount = 1; !ddsp.isOutOfData(); x++, recordcount++) {
            try {
                objectContext = ddsp.getRecord();
                objectContext.getFieldMaps().get(0);
            } catch (NoWorkException e) {
//                recordcount = 0;
            } catch (OutOfDataException e) {
//                recordcount = 0;
            } catch (Exception e) {
                fail("getRecord caused an unexpected Exception:  " + e.toString());
            }
            
        }
        assertTrue(x == recordcount, "Encountered out-of-data when not expected.");
    }
 
}
