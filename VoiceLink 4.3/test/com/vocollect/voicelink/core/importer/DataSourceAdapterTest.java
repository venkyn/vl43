/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.core.importer.adapters.FileDataSourceAdapter;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * @author astein
 */
public class DataSourceAdapterTest {

    private DataSourceAdapter dsAdapter = null;
    private static final Logger log = 
        new Logger(DataSourceAdapterTest.class);


    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeClass
    public void setUp() throws Exception {
        dsAdapter = new FileDataSourceAdapter();
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#DataSourceAdapter()}.
     */
    @Test
    public final void testDataSourceAdapter() {
        assertNotNull(dsAdapter.getCurrentState());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#close()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#isOpen()}.
     */
    @Test
    public final void testClose() {
        try {
            dsAdapter.setOpen(true);
            dsAdapter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#getCurrentState()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#setCurrentState(InternalState)}.
     */
    @Test
    public final void testGetSetCurrentState() {
        dsAdapter.setCurrentState(DataSourceAdapter.CONFIGUREDSTATE);
        assertTrue(dsAdapter.getCurrentState() == DataSourceAdapter.CONFIGUREDSTATE);
        dsAdapter.setCurrentState(DataSourceAdapter.MISCONFIGUREDSTATE);
        assertTrue(dsAdapter.getCurrentState() == DataSourceAdapter.MISCONFIGUREDSTATE);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#getInternalDataSource()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#setInternalDataSource(BufferedReader)}.
     */
    @Test
    public final void testGetSetInternalDataSource() {
        try {
            dsAdapter.setInternalDataSource(new BufferedReader(new FileReader(
                "data/import/test.dat")));
            assertNotNull(dsAdapter.getInternalDataSource());
        } catch (FileNotFoundException e) {
            // Nothing to do, this is acceptable as well.
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#getCharsRead()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#setCharsRead(long)}.
     */
    @Test
    public final void testGetSetCharsRead() {
        assertTrue(dsAdapter.getCharsRead() == 0);
        dsAdapter.setCharsRead(32L);
        assertTrue(dsAdapter.getCharsRead() == 32L);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#getCurrentRecordLength()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#setCurrentRecordLength(long)}.
     */
    @Test
    public final void testGetSetCurrentRecordLength() {
        dsAdapter.setCurrentRecordLength(40L);
        assertTrue(dsAdapter.getCurrentRecordLength() == 40L);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#getConfigFileName()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#setConfigFileName(String)}.
     */
    @Test
    public final void testGetSetConfigFileName() {
        dsAdapter.setConfigFileName("configfile");
        assertTrue(dsAdapter.getConfigFileName().equals("configfile"));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#getBufferedCharsRemaining()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#setBufferedCharsRemaining(int)}.
     */
    @Test
    public final void testGetSetBufferedCharsRemaining() {
        dsAdapter.setBufferedCharsRemaining(27);
        assertTrue(dsAdapter.getBufferedCharsRemaining() == 27);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#useSourceChar()}.
     */
    @Test
    public final void testUseSourceChar() {
        dsAdapter.setBufferedCharsRemaining(27);
        dsAdapter.useSourceChar();
        assertTrue(dsAdapter.getBufferedCharsRemaining() == 26);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#getDataSourceName()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#setDataSourceName(String)}.
     */
    @Test
    public final void testGetSetDataSourceName() {
        dsAdapter.setDataSourceName("thedatasourcename");
        assertTrue(dsAdapter.getDataSourceName().equals("thedatasourcename"));
    }
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#getEncoding()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#setEncoding(String)}.
     */
    @Test
    public final void testGetSetEncoding() {
        String utf8Encoding = "UTF-8";
        String inValidEncoding = "UTF-22";
        String emptyEncoding = "";
        try {
            dsAdapter.setEncoding(utf8Encoding);    
            assertTrue(dsAdapter.getEncoding().equals(utf8Encoding));            
        } catch (ConfigurationException e) {
            fail("Valid Encoding " + utf8Encoding + " Should be able to set, "
                + "Exception: " + e.getMessage());
        }
        try {
            dsAdapter.setEncoding(emptyEncoding);    
            assertTrue(dsAdapter.getEncoding().equals(utf8Encoding));            
        } catch (ConfigurationException e) {
            fail("When no input encoding provided, it should be able to set "
                + " to default encoding UTF-8, error: " + e.getMessage());
        }
        try {
            dsAdapter.setEncoding(inValidEncoding);    
            fail("Invalid Encoding must throw the Configuration exception");
        } catch (ConfigurationException e) {
            log.debug("expected exception: " + "Invalid Encoding " + inValidEncoding 
                + "Not Supported for VoiceLink imports");
        }

    }

    
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#getRecordsInProperOrder()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceAdapter#setRecordsInProperOrder()}.
     */
    @Test()
    public void testSetGetRecordsInProperOrder() {
        //create an empty array
        Long[][] myProperOrder2 = null;
        
        //Create an array myProperOrder and set some values
        Long[][] myProperOrder = new Long[2][3]; 
        myProperOrder[0][0] = 0L;
        myProperOrder[1][0] = 10L;
        myProperOrder[0][1] = 100L;
        myProperOrder[1][1] = 20L;
        myProperOrder[0][2] = 200L;
        myProperOrder[1][2] = 30L;
        
        //Since we haven't set RecordsInProperOrder yet, get should return null.
        org.testng.Assert.assertTrue(dsAdapter.getRecordsInProperOrder() == null, "Default state of "
            + "getRecordsInProperOrder did not retunr null.");
        
        //Set value of RecordsInProperOrder to myProperOrder
        dsAdapter.setRecordsInProperOrder(myProperOrder);
        // Get the array back
        myProperOrder2 = dsAdapter.getRecordsInProperOrder();

        //Check all values in array to be grouped properly (natural order, but like-indexed items together)
        org.testng.Assert.assertTrue(myProperOrder2[0][0] == 0L, "Result from "
            + "getRecordsInProperOrder() in myProperOrder2 [0][0] value was wrong.");
        org.testng.Assert.assertTrue(myProperOrder2[1][0] == 10L, "Result from "
            + "getRecordsInProperOrder() in myProperOrder2 [1][0] value was wrong.");
        org.testng.Assert.assertTrue(myProperOrder2[0][1] == 100L, "Result from "
            + "getRecordsInProperOrder() in myProperOrder2 [0][1] value was wrong.");
        org.testng.Assert.assertTrue(myProperOrder2[1][1] == 20L, "Result from "
            + "getRecordsInProperOrder() in myProperOrder2 [1][1] value was wrong.");
        org.testng.Assert.assertTrue(myProperOrder2[0][2] == 200L, "Result from "
            + "getRecordsInProperOrder() in myProperOrder2 [0][2] value was wrong.");
        org.testng.Assert.assertTrue(myProperOrder2[1][2] == 30L, "Result from "
            + "getRecordsInProperOrder() in myProperOrder2 [1][2] value was wrong.");
    }
    
    
    
}
