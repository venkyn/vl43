/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.importer.triggers.ConditionalUpdateLineTrigger;
import com.vocollect.voicelink.core.model.Operator;

import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;



/**
 * Test case.
 * @author dgold
 *
 */
public class ConditionalUpdateLineTriggerTest extends BaseImportServiceManagerTestCase {
    private PersistenceManager  pm  = new PersistenceManager();

    private DBLookup            finder = null; //new DBLookup(pm);

    private ConditionalUpdateLineTrigger       lookupOperator = null;

    private FieldMap  inputDataFields = null;

    private Operator            testOperator   = null;

    private List<Operator>                allOperators   = null;

    private String              openContext;

    private String              expectedResult;

    private String              expectedResultNoFields;

    private String              objectName;

    private String              openTag;

    private String              closeTag;

    private String              keyFieldName;

    private String              finderName;

    private FieldMap fields;

    private Field    f;


    /**
     * @throws java.lang.Exception if the database takes exception to the init.
     */
    @BeforeClass()
    protected void setUpLocal() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        System.err.println("initializing small sample");
        adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkDataSmall.xml", DatabaseOperation.REFRESH);
        System.err.println("initializing operators");
        adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkSampleOperators.xml", DatabaseOperation.REFRESH);
        System.err.println("DB Initialization complete");


    }

   /**
    * {@inheritDoc}
    * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
    */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        // Nothing to do here, setupLocal() handles it.
        
    }

    /**
     * Set up test stuff.
     */
    @BeforeMethod()
    protected void getOperatorID() throws Exception {
        if ((null == allOperators) || (0 == allOperators.size())) {
            // Go get a list of all the operators
            allOperators = pm.getAll(Operator.class);
            
            if (0 == allOperators.size()) {
                setUpLocal();
                allOperators = pm.getAll(Operator.class);
                assertTrue((0 < allOperators.size()));
            }

            testOperator = new Operator();
            // Set the operator id to something we know is in the database
            testOperator.
            setOperatorIdentifier(allOperators.get(0).getOperatorIdentifier());

            lookupOperator = new ConditionalUpdateLineTrigger();

            keyFieldName = "operatorIdentifier";
            Field aField = new Field();
            aField = setupField(aField, keyFieldName, testOperator.getOperatorIdentifier());
            lookupOperator.addField(aField);
            finderName = "findByIdentifier";
            lookupOperator.setFinderName(finderName);
            lookupOperator.setNewObjectName("Operator");
            Field password = new Field();
            password.setFieldName("password");
            password = setupField(password, "password", "");
            password.setKeyField(false);
            lookupOperator.addField(password);
            lookupOperator.setUseAllFields(true);
            lookupOperator.setMainObject(true);

            Field bField = new Field();
            bField = setupField(bField, keyFieldName, "Nobody");
            bField.setKeyField(false);
            inputDataFields = new FieldMap();
            inputDataFields.addField(bField);

            openContext = "";
            objectName = "operator";

            expectedResult = "<" + objectName + ">" + "<" + aField.getFieldName() + ">"
            + aField.getFieldData() + "</" + aField.getFieldName() + ">" + "</" + objectName + ">";
            expectedResultNoFields = "<" + objectName + ">" 
            + "</" + objectName + ">";

            f = new Field();
            f = setupField(f, aField.getFieldName(), aField.getFieldData());
            fields = new FieldMap();
            fields.addField(f);

            openTag = "<" + objectName + ">";
            closeTag = "</" + objectName + ">";

            finder = new DBLookup(pm);

        } else {
            testOperator = new Operator();
            // Set the operator id to something we know is in the database
            testOperator.
            setOperatorIdentifier(allOperators.get(0).getOperatorIdentifier());
        }

    }

    /**
     * Set up the field passed in with the data passed in. Just to clean up the setup code.
     * @param fld field.
     * @param fieldName as it says
     * @param fieldData as it says
     * @return the filled in field.
     */
    private Field setupField(Field fld, String fieldName, String fieldData) {
        fld.setFieldName(fieldName);
        fld.setFieldData(fieldData);
        fld.setOpenTag("<" + fieldName + ">");
        fld.setCloseTag("</" + fieldName + ">");
        fld.setFieldData(fieldData);
        fld.setKeyField(true);
        return fld;
    }
    
    /**
     * Clean up after the test - clear out the DB.
     */
    @AfterTest()
    protected void tearDownLocal() {
        DbUnitAdapter adapter = new DbUnitAdapter();
        try {
          adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkDataUnitTest_ResetAll.xml");
      } catch (Exception e) {
          e.printStackTrace();
          assertFalse(true, "Threw an exception setting up database");
      }

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.ConditionalUpdateLineTrigger#lookupObject(java.lang.Object, com.vocollect.voicelink.core.importer.DBLookup)}.
     */
    @Test()
    public void testLookupObject() {
        Operator op = null;
        Object obj = null; 
        // We should be able to update the thing we're looking for. So we will change the password.
        testOperator.setPassword("changed");
        try {
            // This lookup finds by ID.
            obj = lookupOperator.lookupObject(testOperator, finder);
        } catch (VocollectException e1) {
            assertTrue(false, "lookup threw an exception");
        }
        assertNotNull(obj, "drew a null with the lookup");
        try {
            op = (Operator) obj;
        } catch (ClassCastException e) {
            assertTrue(false, "retrieved object is not an operator");
        }
        assertEquals(testOperator.getOperatorIdentifier(), 
                op.getOperatorIdentifier(), "Got a different operator");
        assertEquals("changed", op.getPassword(), "Password in the retrieved object is incorrect");
        
        // OK, so updating a found object works, let's see how an insert works.
        Operator newGuy = new Operator();
        
        newGuy.setOperatorIdentifier("NewGuy");
        newGuy.setPassword("xxxx");
        
        try {
            // This lookup finds by ID.
            obj = lookupOperator.lookupObject(newGuy, finder);
        } catch (VocollectException e1) {
            assertTrue(false, "lookup threw an exception");
        }
        assertNotNull(obj, "drew a null with the lookup");
        try {
            op = (Operator) obj;
        } catch (ClassCastException e) {
            assertTrue(false, "retrieved object is not an operator");
        }
        assertEquals(newGuy.getOperatorIdentifier(), 
                op.getOperatorIdentifier(), "Got a different operator");
        assertEquals("xxxx", op.getPassword(), "Password in the retrieved object is incorrect");
        
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.ConditionalUpdateLineTrigger#openContext(java.lang.String, com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test()
    public void testOpenContext() {
        ConditionalUpdateLineTrigger cultt = new ConditionalUpdateLineTrigger();
        cultt.setUseAllFields(true);
        cultt.setNewObjectName(objectName);
        cultt.setOpenTag(openTag);
        cultt.setCloseTag(closeTag);

        assertEquals(
            expectedResult, cultt.openContext(openContext, fields),
            "Did not get the expected result from openContext");
        
        cultt.setUseAllFields(false);
        assertEquals(expectedResultNoFields, cultt.openContext(openContext, fields), "Did not get the expected result");

    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.triggers.ConditionalUpdateLineTrigger#ConditionalUpdateLineTrigger()}.
     */
    @Test()
    public void testConditionalUpdateLineTrigger() {
        ConditionalUpdateLineTriggerTest cultt = new ConditionalUpdateLineTriggerTest();
        assertNotNull(cultt, "Constructor yielded a null");
    }
    
    /**
     * Test that the markup we use is good.
     */
    @Test()
    public void testConfigSetup() {
        CastorConfiguration cc = new CastorConfiguration("import-definition.xml", "baseConditionalUpdateTrigger.xml");
        Object obj = null;
        try {
            obj = cc.configure();
        } catch (ConfigurationException e1) {
            fail("Mapping and markup seem to be at odds.");
        }
        ConditionalUpdateLineTrigger e = (ConditionalUpdateLineTrigger) obj; 
        
        assertEquals(2, e.getFieldMap().size(), "There should be two fields in the trigger fields");
        
        assertNotNull(e.getFieldMap().get("operatorIdentifier"));
        assertNotNull(e.getFieldMap().get("operatorIdentifier"));
        assertNotNull(e.getFieldMap().get("password"));
        assertNotNull(e.getFieldMap().get("password"));
        
        assertTrue(
            (e.getFieldMap().get("operatorIdentifier")).isKeyField(),
            "Field named 'operatorIdentifier' should be a lookup field");
        
        // OK, use it...
        Object result = null;
        testOperator.setPassword("9999");
        try {
            result = e.lookupObject(this.testOperator, this.finder);
        } catch (VocollectException e1) {
            assertFalse(true, "Threw an exception on lookup");
        }
        assertNotNull(result, "Retrieved nothing from the lookup");
        assertNotNull(result, "Did not get an operator...");

    }


}
