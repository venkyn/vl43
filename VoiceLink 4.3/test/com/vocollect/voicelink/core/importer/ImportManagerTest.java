/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.test.DbUnitAdapterVoiceLink;

import java.util.ArrayList;
import java.util.List;

import org.quartz.UnableToInterruptJobException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test for ImportManager object.
 * 
 * @author astein
 */
public class ImportManagerTest extends VoiceLinkDAOTestCase  {
    private String testDir = null;

    /**
     * @throws Exception
     */
    @BeforeClass()
    protected void importerSetUp() throws Exception {

        testDir = System.getProperty("test.input.dir");
        if (null == testDir) {
            testDir = "./";
        } else {
            testDir = testDir + "/";
        }

        resetDB();    
    }


    /**
     * @throws Exception
     */
    private void resetDB() throws Exception {
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();         
        adapter.resetInstallationData();
        adapter.loadCoreMultiSiteData();
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.ImportManager()'.
     */
    @Test()
    public void testImportManager() {
        ImportManager im = new ImportManager();
        assertNotNull("Constructor failed");
        assertEquals(im.getCurrentState(), ImportManager.INITIALIZEDSTATE, 
                  "Wrong initial state :" + im.getCurrentState()
                + " should be " + ImportManager.INITIALIZEDSTATE);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.setDescription(String)'.
     */
    @Test()
    public void testSetDescription() {
        String defaultImporterDescription = "\nImporter.defaultDescription";
        String description = "TestImportManager";
        String importerDescription = "TestImport: test import to import the test class TestInput";
        ImportManager im = new ImportManager();
        assertNotNull("Description is null", im.getDescription());
        assertEquals(0, defaultImporterDescription.compareTo(im.getDescription()), 
                  "Default description is wrong:" + im.getDescription()
                + " should be " + defaultImporterDescription);
        im.setDescription(description);
        assertEquals(0, (defaultImporterDescription + "\n" + description).compareTo(im
                .getDescription()), "Description is wrong: " + im.getDescription()
                + " should be " + defaultImporterDescription + "\n"
                + description);
        im.setDescription(importerDescription);
        assertEquals(0, (defaultImporterDescription + "\n" + description + "\n" + importerDescription)
            .compareTo(im.getDescription()),
                "Added description is wrong");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.getConfig()'. Test
     * method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.setConfig(String)'
     */
    @Test()
    public void testGetConfig() {
        String testString = "testConfig";
        ImportManager im = new ImportManager();
        assertNull(im.getConfigFileName());
        im.setConfigFileName(testString);
        assertNotNull(im.getConfigFileName());
        assertEquals(0, testString.compareTo(im.getConfigFileName()), "Wrong config name: " + im.getConfigFileName()
                + " should be " + testString);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.getStatus()'. Test
     * method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.setStatus(String)'
     */
    @Test()
    public void testGetStatus() {
        ImportManager im = new ImportManager();
        assertNotNull("Status is null.", im.getStatus());
        assertEquals(0, im.getStatus().compareTo("Initialized"), "Wrong status description: " + im.getStatus()
                + " should be " + "Initialized");
        im.setStatus("test");
        assertEquals(0, im.getStatus()
            .compareTo("Initializedtest"), "Wrong status description: " + im.getStatus()
                + " should be " + "Initializedtest");

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.start()'.
     */
    @Test()
    public void testStart() {
        // Import manager should go to Started, then Running, then some final
        // state
        ImportManager im = new ImportManager();
        im.start();
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.stop()'.
     */
    @Test()
    public void testStop() {
        ImportManager im = new ImportManager();
        im.stop();
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.configure()'.
     */
    @Test()
    public void testConfigure() {
        ImportManager im = new ImportManager();
        im.setConfigFileName("import-setup.xml");
        im.setMappingFileName("import-definition.xml");
        try {
            im.configure();
        } catch (VocollectException e) {
            assertTrue(false, "import configure threw an exception");
        }
        assertNotNull(im.getBaseDataType());
        im = new ImportManager();
        im.setConfigFileName("export-setup.xml");
        im.setMappingFileName("import-definition.xml");
        try {
            im.configure();
        } catch (VocollectException e) {
            assertTrue(false, "export configure threw an exception");
        }
        assertNotNull(im.getBaseDataType());
    }
    
    /**
     * 
     */
    @Test()
    public void testExports() {
        ImportManager im = new ImportManager("import-definition.xml", "export-setup.xml");
        im.start();
        assertEquals(im.getCurrentState(), ImportManager.INITIALIZEDSTATE, 
            "CurrentState should equal INITIALIZED_STATE");
        try {
            im.configure();
            im.prepare();
            im.start();
            im.runImports();
            assertTrue(im.getCurrentState().isFinalState(), 
                "Any state after the imports run should be a final state. Current state: " + im.getCurrentState());
        } catch (UnableToInterruptJobException e) {
            // Expected
        } catch (RuntimeException e1) {
            // Expected.
            // If this is expected, why throw the exception?
            // No asserts for this expected behavior.
            // e1.printStackTrace();
        } catch (VocollectException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.getImportDatatype()'.
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.setImportDatatype(List)'
     */
    @Test()
    public void testImportDatatype() {
        // tests both the getter and setter of ImportDatatype
        ImportManager im = new ImportManager();
        List<Importer> l = new ArrayList<Importer>();
        Importer imp = new Importer();
        l.add(imp);
        im.setBaseDataType(l);
        assertEquals(im.getBaseDataType(), l, "These lists should equal each other");

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.executeInternal(JobExecutionContext
     * jobExecutionContext)'.
    @SuppressWarnings("static-access")
    @Test()
    public void testExecuteInternal() {
        ImportManager im = new ImportManager();
        im.clearStatus();
        JobDetail jobDetail = new JobDetail();
        jobDetail.setJobClass(im.getClass());
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("configfile", "test-import-setup.xml");
        jobDataMap.put("mappingfile", "import-definition.xml");
        jobDetail.setJobDataMap(jobDataMap);
        try {
            Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
            Trigger trigger = new SimpleTrigger("testTrigger",
                    scheduler.DEFAULT_GROUP, new java.util.Date(), null, 0, 0L);
            TriggerFiredBundle triggerFiredBundle = new TriggerFiredBundle(
                    jobDetail, trigger, new BaseCalendar(), true,
                    new java.util.Date(), new java.util.Date(),
                    new java.util.Date(), new java.util.Date());
            JobExecutionContext jobContext = new JobExecutionContext(scheduler,
                    triggerFiredBundle, new NativeJob());
            im.executeInternal(jobContext);
        } catch (SchedulerException e) {
            e.printStackTrace();
        } catch (RuntimeException r) {
            r.printStackTrace();
        }
        assertNotNull("Status should not be null", im.getStatus());
    }
     */

    /**
     * TODO Make this test right. We should be forcing some result.
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.runImports()'.
     */
    @Test()
    public void testRunImports() {
        ImportManager im = new ImportManager("import-definition.xml", "import-setup.xml");
        im.start();
        assertEquals(im.getCurrentState(), ImportManager.INITIALIZEDSTATE, 
            "CurrentState should equal INITIALIZED_STATE");
        try {
            im.configure();
            im.prepare();
            im.start();
            im.runImports();
            assertTrue(im.getCurrentState().isFinalState(), 
                "Any state after the imports run should be a final state. Current state: " + im.getCurrentState());
//            assertEquals("Status should equal Initialized instead of "
//                    + im.getStatus(), 0, ("Initialized").compareTo(im
//                    .getStatus()));
//            assertEquals("CurrentState should equal "
//                    + ImportManager.INITIALIZEDSTATE + " instead of "
//                    + im.getCurrentState(), im.getCurrentState(),
//                    ImportManager.INITIALIZEDSTATE);
        } catch (UnableToInterruptJobException e) {
            // Expected
        } catch (RuntimeException e1) {
            // Expected.
            // If this is expected, then why throw the error so it looks like
            // something went wrong?
            // e1.printStackTrace();
        } catch (VocollectException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.setAllJobsFailed(boolean)'.
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.isAllJobsFailed()'
     */
    @Test()
    public void testSetAllJobsFailed() {
        ImportManager im = new ImportManager();
        im.setAllJobsFailed(true);
        assertTrue(im.isAllJobsFailed(), " AllJobsFailed should be true ");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.setState(Importer)'.
     * Test method for
     * 'com.vocollect.voicelink.core.importer.ImportManager.getState()'
     */
    @Test()
    public void testSetState() {
        ImportManager im = new ImportManager();
        DataTransferBase imp = new Importer();
        im.setState(imp);
        assertNotNull(im.getCurrentState(), " CurrentState should not be null");
        String thestate = "ImportManager.Initialized";
        assertEquals(0, thestate.compareTo(im
            .getCurrentState().toString()), "Wrong CurrentState: " + im.getCurrentState()
                + " should be " + thestate);
    }

    /**
     * testing stateChanges.
     */
    @Test()
    public void testStateChanges() {
        ImportManager im = new ImportManager();
        String endState = "Success";
        assertEquals(im.getCurrentState(),
            ImportManager.INITIALIZEDSTATE, 
            "Should be at " + ImportManager.INITIALIZEDSTATE
                + " instead of " + im.getCurrentState());
        try {
            im.setConfigFileName("test-import-setup.xml");
            im.setMappingFileName("import-definition.xml");
            im.configure();
            assertEquals(im
                .getCurrentState(), ImportManager.CONFIGUREDSTATE, 
                "Should be at " + ImportManager.CONFIGUREDSTATE
                    + " instead of " + im.getCurrentState());
            im.prepare();
            assertEquals(im
                .getCurrentState(), ImportManager.STARTEDSTATE, 
                "Should be at " + ImportManager.STARTEDSTATE
                    + " instead of " + im.getCurrentState());
            im.start();
            assertEquals(im
                .getCurrentState(), ImportManager.RUNNINGSTATE, 
                "Should be at " + ImportManager.RUNNINGSTATE
                    + " instead of " + im.getCurrentState());
            im.determineRunningNoWork();
            if (im.isRunningNoWork()) {
                endState = "NoWork";
                assertEquals(im
                    .getCurrentState(), ImportManager.RUNNINGNOWORKSTATE,
                    "Should be at " + ImportManager.RUNNINGNOWORKSTATE
                        + " instead of " + im.getCurrentState());
            }
//            for (Importer importer : im.getImportDatatype()) {
//                importer.start();
//                if (!importer.importDatatype()) {
//                    im.interrupt();
//                    assertEquals("Should be at " + ImportManager.STOPPEDSTATE
//                            + " instead of " + im.getCurrentState(), im
//                            .getCurrentState(), ImportManager.STOPPEDSTATE);
//                }
//                im.setState(importer);
//                if (importer.getCurrentState().isFailedState()) {
//                    if (importer.isCritical()) {
//                        assertEquals("Should be at "
//                                + ImportManager.FAILEDSTATE + " instead of "
//                                + im.getCurrentState(), im.getCurrentState(),
//                                ImportManager.FAILEDSTATE);
//                    } else {
//                        endState = "PartialSuccess";
//                        assertEquals("Should be at "
//                                + ImportManager.RUNNINGFAILEDSTATE
//                                + " instead of " + im.getCurrentState(), im
//                                .getCurrentState(),
//                                ImportManager.RUNNINGFAILEDSTATE);
//                    }
//                } else if (importer.getCurrentState().isImperfectState()) {
//                    endState = "Imperfect";
//                    assertEquals("Should be at "
//                            + ImportManager.RUNNINGIMPERFECTSTATE
//                            + " instead of " + im.getCurrentState(), im
//                            .getCurrentState(),
//                            ImportManager.RUNNINGIMPERFECTSTATE);
//                } else if (importer.getCurrentState().isNoWorkState()) {
//                    endState = "NoWork";
//                    assertEquals("Should be at "
//                            + ImportManager.RUNNINGNOWORKSTATE + " instead of "
//                            + im.getCurrentState(), im.getCurrentState(),
//                            ImportManager.RUNNINGNOWORKSTATE);
//                }
//            }
            if (im.isAllJobsFailed()) {
                // This is normally done internally, but since we externalized
                // the loop that does the work...
                im.setCurrentState(ImportManager.FAILEDSTATE);
                endState = "Failed";
                assertEquals(im
                    .getCurrentState(), ImportManager.FAILEDSTATE, 
                    "Should be at " + ImportManager.FAILEDSTATE
                        + " instead of " + im.getCurrentState());
            }
            im.determineSuccess();
            if (endState.equals("Success")) {
                assertEquals(im
                    .getCurrentState(), ImportManager.SUCCESSFULSTATE, 
                    "Should be at " + ImportManager.SUCCESSFULSTATE
                        + " instead of " + im.getCurrentState());
            } else if (endState.equals("Imperfect")) {
                assertEquals(im
                    .getCurrentState(), ImportManager.IMPERFECTSTATE, 
                    "Should be at " + ImportManager.IMPERFECTSTATE
                        + " instead of " + im.getCurrentState());
            } else if (endState.equals("NoWork")) {
                assertEquals(im
                    .getCurrentState(), ImportManager.NOWORKSTATE, 
                    "Should be at " + ImportManager.NOWORKSTATE
                        + " instead of " + im.getCurrentState());
            } else if (endState.equals("PartialSuccess")) {
                assertEquals(im
                    .getCurrentState(), ImportManager.PARTIALSUCCESS, 
                    "Should be at " + ImportManager.PARTIALSUCCESS
                        + " instead of " + im.getCurrentState());
            }
        } catch (VocollectException e) {
            e.printStackTrace();
//        } catch (UnableToInterruptJobException e1) {
//            e1.printStackTrace();
        }
    }
}
