/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;


/**
 * Base class for import tests that need the persistence manager.
 *
 * @author ddoubleday
 */
public abstract class BaseImportServiceManagerTestCase extends
    BaseServiceManagerTestCase {

    /**
     * @return Spring config locations required for Import/Export tests.
     */
    public static final String[] getImportTestConfigLocations() {
        String[] daoConfigs = VoiceLinkDAOTestCase.getDAOTestConfigLocations();
        String[] configs = new String[daoConfigs.length + 1];
        System.arraycopy(daoConfigs, 0, configs, 0, daoConfigs.length);
        configs[configs.length -  1] = 
            "classpath*:/applicationContext-voicelink-import.xml";
        return configs;        
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return getImportTestConfigLocations();
    }

}
