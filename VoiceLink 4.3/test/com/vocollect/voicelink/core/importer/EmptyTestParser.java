/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.exceptions.VocollectException;

/**
 * Just an instantiatable thing to test with.
 * 
 * @author dgold
 */

public class EmptyTestParser extends DataSourceParser {

//    private static final Logger log          = new Logger(
//        EmptyTestParser.class);

    /**
     * 
     * @throws ConfigurationException if unsuccessful
     */
    public EmptyTestParser() throws ConfigurationException {
        super();
        // Nothing to do
    }

    /**
     * @throws VocollectException if unsuccessful
     * @return FieldMap 
     */
    @Override
    public ObjectContext getRecord() throws VocollectException {
        // Nothing to do
        return null;
    }

    /**
     * @throws OutOfDataException if out of date
     * @return Field  
     */
    @Override
    public Field getNextField() throws OutOfDataException {
        // Nothing to do
        return null;
    }

    /**
     * @return false if configured improperly
     */
    @Override
    public boolean configure() {
        // Nothing to do
        return false;
    }

    /**
     * @return true if out of data
     */
    @Override
    public boolean isOutOfData() {
        // Nothing to do
        return false;
    }

    /**
     * {@inheritDoc}
     * @return null
     * @throws VocollectException part of interface - will not throw.
     */
    @Override
    public ObjectContext getElement() throws VocollectException {
        // Nothinf to do
        return null;
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public long getRecordsRead() {
        // TODO Auto-generated method stub
        return 0;
    }
    
}
