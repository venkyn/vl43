/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;

/**
 * Test for LocalizedMessage object.
 * 
 * @author astein
 */
public class LocalizedMessageTest {

    private static final String TEST_KEY = "task.operator";
    private static final String EXPECTED_VALUE = "Operator";
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.LocalizedMessage.formatMessage()'.
     */
    @Test()
    public void testFormatMessage() {
        assertNotNull("Test key not null",
                LocalizedMessage.formatMessage(TEST_KEY));
        assertEquals("Test key correct value", EXPECTED_VALUE,
                LocalizedMessage.formatMessage(TEST_KEY));
        assertEquals("Should be 'Bad Config.'", "Bad Config.",
                LocalizedMessage
                        .formatMessage("errorCode.message.VoiceLink5001"));
        assertNotNull("Test key not null default locale",
                LocalizedMessage.formatMessage(TEST_KEY,
                        LocalizedMessage.getDefaultLocale()));
        assertEquals("Test key correct value default locale", EXPECTED_VALUE,
                LocalizedMessage.formatMessage(TEST_KEY,
                        LocalizedMessage.getDefaultLocale()));
        assertEquals("Should be 'Bad Config.'", "Bad Config.",
                LocalizedMessage.formatMessage(
                        "errorCode.message.VoiceLink5001", LocalizedMessage
                                .getDefaultLocale()));
        Object[] obj = new Object[4];
        assertNotNull("Test key not null with object",
                LocalizedMessage.formatMessage(TEST_KEY, obj,
                        LocalizedMessage.getDefaultLocale()));
        assertEquals("Test key correct value with object", EXPECTED_VALUE,
                LocalizedMessage.formatMessage(TEST_KEY, obj,
                        LocalizedMessage.getDefaultLocale()));
        assertEquals("Should be 'Bad Config.'", "Bad Config.",
                LocalizedMessage.formatMessage(
                        "errorCode.message.VoiceLink5001", obj,
                        LocalizedMessage.getDefaultLocale()));

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.LocalizedMessage.getMessageString()'.
     */
    @Test()
    public void testGetMessageString() {
        assertNotNull("Test message string not null",
                LocalizedMessage.getMessageString(TEST_KEY,
                        null, null));
        assertEquals("Test message string correct value", EXPECTED_VALUE,
                LocalizedMessage.getMessageString(TEST_KEY,
                        null, null));
        assertEquals("Should be 'Bad Config.'", "Bad Config.",
                LocalizedMessage.getMessageString(
                        "errorCode.message.VoiceLink5001", null, null));
    }
}
