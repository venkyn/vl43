/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * @author astein
 */
public class StateChangeTest {

    private InternalState successfulTestIntState = new InternalState(
                                                 1,
                                                 "Importer.Successful",
                                                 InternalState.FinalStateType.Success);

    private InternalState failedTestIntState     = new InternalState(
                                                 2,
                                                 "Importer.Failed",
                                                 InternalState.FinalStateType.Failed);

    private InternalState configuredTestIntState = new InternalState(3,
                                                 "Importer.Configured");

    /**
     * 
     * @throws Exception if unsuccessful
     */
    @BeforeClass
    public void setUp() throws Exception {
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.StateChange#testHashCode()'.
     */
    @Test
    public final void testHashCode() {
        StateChange stateChange = new StateChange(configuredTestIntState,
                successfulTestIntState);
        StateChange stateChange2 = new StateChange(configuredTestIntState,
                successfulTestIntState);
        assertEquals("" + stateChange.hashCode(), "" + stateChange2.hashCode());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.StateChange#StateChange()'.
     */
    @Test
    public final void testStateChange() {
        StateChange stateChange = new StateChange(configuredTestIntState,
                successfulTestIntState);
        assertEquals(stateChange.getFrom().toString(), configuredTestIntState
                .toString());
        assertEquals(stateChange.getTo().toString(), successfulTestIntState
                .toString());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.StateChange#compareTo(Object)'.
     */
    @Test
    public final void testCompareTo() {
        StateChange stateChange = new StateChange(configuredTestIntState,
                successfulTestIntState);
        StateChange stateChange2 = new StateChange(configuredTestIntState,
                successfulTestIntState);
        assertEquals("" + stateChange.compareTo(stateChange2), "" + 0);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.StateChange#equals(Object)'.
     */
    @Test
    public final void testEqualsObject() {
        StateChange stateChange = new StateChange(configuredTestIntState,
                successfulTestIntState);
        StateChange stateChange2 = new StateChange(configuredTestIntState,
                successfulTestIntState);
        assertTrue(stateChange.equals(stateChange2),
                "StateChange objects should be equal");
    }

}
