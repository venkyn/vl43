/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.adapters;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.CastorConfiguration;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.NoWorkException;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;


/**
 * @author dgold
 *
 */
public class FilePurgeAdapterTest {

    private String                testDir   = null;

    private String             testURLDir   = null;
    
    private String             parentDirectory    = null;
    
    private String             parentDirectory2 = null;

    /**
     * {@inheritDoc}
     * @throws java.lang.Exception
     */
    @BeforeMethod()
    protected void setUp() throws Exception {
        if (System.getProperty("test.input.dir") != null) {
            testDir = System.getProperty("test.input.dir") + "/";
        } else {
            testDir = "./";
            testURLDir = "http://" + "localhost" + ":" + "8080"
                + "/VoiceLink/dev/importexport/main/data/import";
        }
        parentDirectory = testDir + "data/purge/Default/";
        parentDirectory2 = testDir + "data/purge/Default/failedImport/";
        
        File junkDir = new File(parentDirectory);
        if (junkDir.exists()) {
            FilePurgeAdapter tfpa = new FilePurgeAdapter();
            tfpa.addDirectory(parentDirectory);
            tfpa.setFileNamePattern("junk.*dat");
            boolean delFiles = true;
            try {
                tfpa.openSource();
            } catch (NoWorkException n) {
                delFiles  = false;
            }

            if (delFiles) {
                while (tfpa.readRecord()) {
                    // nothing to do
                }
            }
            junkDir.delete();
        }
        
        junkDir = new File(parentDirectory2);
        if (junkDir.exists()) {
            FilePurgeAdapter tfpa = new FilePurgeAdapter();
            tfpa.addDirectory(parentDirectory);
            tfpa.setFileNamePattern("junk.*dat");
            boolean delFiles = true;
            try {
                tfpa.openSource();
            } catch (NoWorkException n) {
                delFiles  = false;
            }

            if (delFiles) {
                while (tfpa.readRecord()) {
                    // nothing to do
                }
            }
            junkDir.delete();
        }
        
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#isEndOfRecord()}.
     */
    @Test()
    public void testIsEndOfRecord() throws VocollectException {
        FilePurgeAdapter tfpa = new FilePurgeAdapter();
        assertTrue(tfpa.isEndOfRecord());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#isEndOfData()}.
     */
    @Test()
    public void testIsEndOfData() throws VocollectException {
        FilePurgeAdapter tfpa = new FilePurgeAdapter();
        assertTrue(tfpa.isEndOfRecord());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#openSource()}.
     */
    @Test()
    public void testOpenSource() throws VocollectException {
        FilePurgeAdapter tfpa = new FilePurgeAdapter();
        openSourceGuts(tfpa);
    }
    
    /**
     * @param tfpa - the file purge adapter
     * @throws VocollectException
     */
    private void openSourceGuts(FilePurgeAdapter tfpa) throws VocollectException {
        assertTrue(tfpa.isEndOfRecord());
        tfpa.addDirectory(parentDirectory);
        tfpa.setFileNamePattern("junk.*dat");
        setUpFiles(parentDirectory, 0);
        boolean noWork = false;
        try {
            assertFalse(tfpa.openSource());
        } catch (NoWorkException n) {
            noWork  = true;
        }
        assertTrue(noWork, "No work dd not throw a NoWorkException");
        assertTrue(tfpa.isEndOfData(), "Not end of data and should be");
        assertFalse(tfpa.readRecord(), "readrecord at end of data failed");
        assertTrue(tfpa.isEndOfData(), "Not end of data and should be");
        setUpFiles(parentDirectory, 1);
        tfpa.addDirectory(parentDirectory);
        verifyResult(tfpa);
        tfpa.addDirectory(parentDirectory);
        setUpFiles(parentDirectory, 2);
        verifyResult(tfpa);
        tfpa.addDirectory(parentDirectory);
        setUpFiles(parentDirectory, 10);
        verifyResult(tfpa);
    }
    
    /**
     * @param newParentDir - the new parent directory string
     * @param numberOfFiles - the number of files
     */
    private void setUpFiles(String newParentDir, int numberOfFiles) {

        String fnameRoot = "junk";
        String ext = ".dat";
        String fname = "";
        File junk = new File(newParentDir);
        junk.mkdirs();
        for (int i = 0; i < numberOfFiles; i++) {
            fname = fnameRoot + i;
            junk = new File(newParentDir + fname + ext);
            try {
                junk.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                fail("Unable to create junk file " + fname + " to delete");
            }
            try {
                System.out.println("Created file : " + junk.getCanonicalPath());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /**
     * @param tfpa - the file purge adapter
     * @throws VocollectException
     */
    private void verifyResult(FilePurgeAdapter tfpa) throws VocollectException {
        assertTrue(tfpa.openSource());
        assertTrue(tfpa.readRecord(), "readRecord failed");
        while (tfpa.readRecord()) {
            // Nothing to do
        }
        assertTrue(tfpa.isEndOfData(), "Not end of data and should be");
        assertFalse(tfpa.readRecord(), "readrecord at end of data failed");
        assertTrue(tfpa.isEndOfData(), "Not end of data and should be");
    }
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#readRecord()}.
     */
    @Test(enabled = false)
    public void testReadRecord() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#closeOnSuccess()}.
     */
    @Test(enabled = false)
    public void testCloseOnSuccess()  throws VocollectException {
        FilePurgeAdapter tfpa = new FilePurgeAdapter();
        assertTrue(tfpa.isEndOfRecord());
        tfpa.addDirectory(parentDirectory);
        tfpa.setFileNamePattern("junk.*dat");
        setUpFiles(parentDirectory, 1);
        assertTrue(tfpa.closeOnSuccess(), "closeOnSuccess failed");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#closeOnFailure()}.
     */
    @Test(enabled = false)
    public void testCloseOnFailure()  throws VocollectException {
        FilePurgeAdapter tfpa = new FilePurgeAdapter();
        assertTrue(tfpa.isEndOfRecord());
        tfpa.addDirectory(parentDirectory);
        tfpa.setFileNamePattern("junk.*dat");
        setUpFiles(parentDirectory, 1);
        assertTrue(tfpa.closeOnFailure(), "closeOnFailure failed");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#close()}.
     */
    @Test(enabled = false)
    public void testClose()  throws VocollectException {
        FilePurgeAdapter tfpa = new FilePurgeAdapter();
        assertTrue(tfpa.isEndOfRecord());
        tfpa.addDirectory(parentDirectory);
        tfpa.setFileNamePattern("junk.*dat");
        setUpFiles(parentDirectory, 1);
        try {
            tfpa.close();
        } catch (IOException e) {
            e.printStackTrace();
            fail("close threw an exception");
        }
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#FilePurgeAdapter()}.
     */
    @Test(enabled = false)
    public void testFilePurgeAdapter()  throws VocollectException { 
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#getParentDirectory()}.
     */
    @Test()
    public void testGetParentDirectory()  throws VocollectException {
        // Test to see how multiple directories are handled.
        FilePurgeAdapter tfpa = new FilePurgeAdapter();
        assertTrue(tfpa.isEndOfRecord());
        tfpa.addDirectory(parentDirectory);
        tfpa.addDirectory(parentDirectory2);
        tfpa.setFileNamePattern(".*dat");
        setUpFiles(parentDirectory, 1);
        setUpFiles(parentDirectory2, 1);
        try {
            tfpa.close();
        } catch (IOException e) {
            e.printStackTrace();
            fail("close threw an exception");
        }

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#addDirectory(java.lang.String)}.
     */
    @Test(enabled = false)
    public void testaddDirectory() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#getFullFilename()}.
     */
    @Test(enabled = false)
    public void testGetFullFilename() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#getFileNamePattern()}.
     */
    @Test(enabled = false)
    public void testGetFileNamePattern() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#setFileNamePattern(java.lang.String)}.
     */
    @Test(enabled = false)
    public void testSetFileNamePattern() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#getPurgeCutoff()}.
     */
    @Test(enabled = true)
    public void testGetPurgeCutoff() throws VocollectException {
        FilePurgeAdapter tfpa = new FilePurgeAdapter();
        assertTrue(tfpa.isEndOfRecord());
        tfpa.addDirectory(parentDirectory);
        tfpa.setFileNamePattern("junk.*dat");
        setUpFiles(parentDirectory, 1);
        tfpa.setPurgeCutoff(1); // Set purge cutoff to 1 day
        assertEquals(1, tfpa.getPurgeCutoff(), "munged purge cutoff");
        boolean noWork = false;
        try {
            assertFalse(tfpa.openSource());
        } catch (NoWorkException n) {
            noWork  = true;
        }
        assertTrue(noWork, "No work dd not throw a NoWorkException");
        tfpa.addDirectory(parentDirectory);
        tfpa.setPurgeCutoff(0); // Set purge cutoff to right now.
        assertEquals(0, tfpa.getPurgeCutoff(), "didn't reset purge cutoff");
        verifyResult(tfpa);
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FilePurgeAdapter#getLastModifiedPurgeCutoff()}.
     */
    @Test(enabled = true)
    public void testGetLastModifiedPurgeCutoff() throws VocollectException {
        FilePurgeAdapter tfpa = new FilePurgeAdapter();
        tfpa.setPurgeCutoff(1); // Set purge cutoff to 1 day
        long equivalentInMilliseconds = tfpa.getLastModifiedPurgeCutoff();
        assertEquals(1 * 86400 * 1000, equivalentInMilliseconds, "Conversion to last modified equivalent is off");
    }

    /**
     * Test that the markup we use is good.
     * @throws InterruptedException 
     */
  
    @Test()
    public void testConfigSetup() throws VocollectException, InterruptedException {

        CastorConfiguration cc = new CastorConfiguration("import-definition.xml", "FilePurgeAdapterSetup.xml");
        Object obj = null;
        try {
            obj = cc.configure();
        } catch (ConfigurationException e1) {
            e1.printStackTrace();
            fail("Mapping and markup seem to be at odds.");
        }
        FilePurgeAdapter tfpa = (FilePurgeAdapter) obj;
        for (String dir : tfpa.getParentDirectories()) {
            setUpFiles(dir, 1);
        }
        assertTrue(tfpa.openSource());
        assertTrue(tfpa.readRecord(),  "readrecord at end of data failed");
        assertTrue(tfpa.isEndOfData(), "Not end of data and should be");
        assertTrue(tfpa.openSource(),  "failed to open second directory");
        assertTrue(tfpa.readRecord(),  "empty/no directory");
        assertFalse(tfpa.readRecord(), "readrecord at end of data failed");
        assertTrue(tfpa.isEndOfData(), "Not end of data and should be");
    }
    
}
