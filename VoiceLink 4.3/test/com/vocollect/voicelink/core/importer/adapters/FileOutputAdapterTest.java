/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.adapters;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.CastorConfiguration;
import com.vocollect.voicelink.core.importer.ConfigurationException;

import java.io.IOException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

 
/**
 * 
 *
 * @author jtauberg
 */
public class FileOutputAdapterTest {
    private FileOutputAdapter myFileOutputAdapter = null;
//    private String                testDir   = "C:" + java.io.File.separator;
    
    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeMethod()
    protected void setUp() throws Exception {
        try {
            myFileOutputAdapter = new FileOutputAdapter();
        } catch (Exception e) {
            org.testng.AssertJUnit.assertTrue("Constructor failed", false);
        }
        //Set encoding
        try {
           myFileOutputAdapter.setEncoding("UTF-8");  
        } catch (ConfigurationException e) {
            assertTrue("Encoding not supported", false);
        }
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#FileOutputAdapter()}.
     */
    @Test()
    public void testFileOutputAdapter() {
        assertTrue("Constructor for File Output Adapter failed!",
            (myFileOutputAdapter.getClass().getName() 
                == "com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter"));
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#getFileName()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#setFileName(java.lang.String)}.
     */
    @Test()
    public void testSetFileName() {
        //get fileName.  Should be null to start with.
        assertEquals("Initial value of fileName did not default to null.",
            null, myFileOutputAdapter.getFileName());

        //set filename to 'myfile.dat'
        myFileOutputAdapter.setFileName("myfile.dat");
        // ask for filename and make sure it is myfile.dat
        assertEquals("Set a filename value and did not get back same value.",
            "myfile.dat", myFileOutputAdapter.getFileName());

        //filename should not be able to be changed after file is open.  Will test this in Open test.
    }


    

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#setFilenameExtension(java.lang.String)}.
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#getFilenameExtension()}.
     */
    @Test()
    public void testSetFilenameExtension() {
        //get fileNameExtension.  Should be null to start with.
        assertEquals("Initial value of fileNameExtension did not default to null.",
            null, myFileOutputAdapter.getFilenameExtension());

        //set filenameExtension to '.xyz'
        myFileOutputAdapter.setFilenameExtension(".xyz");
        // ask for filenameExtension and make sure it is .xyz
        assertEquals("Set a filenameExtension value and did not get back same value.",
            ".xyz", myFileOutputAdapter.getFilenameExtension());

        //filenameExtension should not be able to be changed after file is open.  Will test this in Open test.
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#setFilenamePrefix(java.lang.String)}.
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#getFilenamePrefix()}.
     */
    @Test()
    public void testSetFilenamePrefix() {
        //get fileNamePrefix.  Should be null to start with.
        assertEquals("Initial value of fileNamePrefix did not default to null.",
            null, myFileOutputAdapter.getFilenamePrefix());

        //set filename to 'callitthis'
        myFileOutputAdapter.setFilenamePrefix("callitthis");
        // ask for filename and make sure it is myfile.dat
        assertEquals("Set a filenamePrefix value and did not get back same value.",
            "callitthis", myFileOutputAdapter.getFilenamePrefix());

        //filenamePrefix should not be able to be changed after file is open.  Will test this in Open test.
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#setFileMode(java.lang.String)}.
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#getFileMode()}.
     */
     @Test()
     public void testGetSetFileMode() {
         FileOutputAdapter aFileOutputAdapter = null; 
         try {
             aFileOutputAdapter = new FileOutputAdapter();
         } catch (Exception e) {
             org.testng.AssertJUnit.assertTrue("Constructor failed", false);
         }

         // ask for fileMode and see that it is writeUnique.
         assertTrue("Initial value of fileMode should be 'writeUnique', but was not.",
             aFileOutputAdapter.getFileMode() == "writeUnique");

         //set fileMode to append
         aFileOutputAdapter.setFileMode("append");
         // ask for fileMode and see that it is right.
         assertTrue("Set fileMode and did not get back same value (append).",
             aFileOutputAdapter.getFileMode().equals("append"));

         //set fileMode to overwrite (case insensitive)
         aFileOutputAdapter.setFileMode("OvErWrItE");
         // ask for fileMode and see that it is right.
         assertTrue("Set fileMode and did not get back same value (overwrite).",
             aFileOutputAdapter.getFileMode().equals("overwrite"));

         //set fileMode to writeUnique (case insensitive)
         aFileOutputAdapter.setFileMode("WRITEunique");
         // ask for fileMode and see that it is right.
         assertTrue("Set fileMode and did not get back same value (writeUnique).",
             aFileOutputAdapter.getFileMode().equals("writeUnique"));
         
         // fileMode should not be able to be changed after file is open.  Will test this in Open test.
     }    
    
    

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#setDeleteOnFailure(java.lang.boolean)}.
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#isDeleteOnFailure()}.
     */
    @Test()
    public void testSetDeleteOnFailure() {
        // ask for DeleteOnFailure and see that it is false.
        assertFalse("Initial value of appendToFile should have been false, but was not.",
            myFileOutputAdapter.isDeleteOnFailure());

        //set DeleteOnFailure to true
        myFileOutputAdapter.setDeleteOnFailure(true);
        // ask for DeleteOnFailure and see that it is true.
        assertTrue("Set deleteOnFailure vale and did not get back same value.",
            myFileOutputAdapter.isDeleteOnFailure());

        //set DeleteOnFailure to false
        myFileOutputAdapter.setDeleteOnFailure(false);
        // ask for DeleteOnFailure and see that it is false
        assertFalse("Set deleteOnFailure vale and did not get back same value.",
            myFileOutputAdapter.isDeleteOnFailure());

        // DeleteOnFailure should not be able to be changed after file is open.  Will test this in Open test.
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#setOutputDirectory(java.lang.String)}.
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#getOutputDirectory()}.
     */
    @Test()
    public void testSetOutputDirectory() {
        //get OutputDirectory.  Should be null to start with.
        assertEquals("Initial value of OutputDirectory did not default to null.",
            null, myFileOutputAdapter.getOutputDirectory());

        //set OutputDirectory to 'export'
        myFileOutputAdapter.setOutputDirectory("export");
        // ask for OutputDirectory and make sure it is export
        assertEquals("Set an OutputDirectory value and did not get back same value.",
            "export", myFileOutputAdapter.getOutputDirectory());

        //OutputDirectory should not be able to be changed after file is open.  Will test this in Open test.
    }

    /**
    
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#setMoveToDirectory(java.lang.String)}.
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#getMoveToDirectory()}.
     */
    @Test()
    public void testSetMoveToDirectory() {
        //get MoveToDirectory.  Should be null to start with.
        assertEquals("Initial value of MoveToDirectory did not default to null.",
            null, myFileOutputAdapter.getMoveToDirectory());

        //set MoveToDirectory to 'MoveTo'
        myFileOutputAdapter.setMoveToDirectory("MoveTo");
        // ask for MoveToDirectory and make sure it is MoveTo
        assertEquals("Set a MoveToDirectory value and did not get back same value.",
            "MoveTo", myFileOutputAdapter.getMoveToDirectory());

        //MoveToDirectory should not be able to be changed after file is open.  Will test this in Open test.
    }
    

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#setRenameOnMove(java.lang.boolean)}.
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#isRenameOnMove()}.
     */
    @Test()
    public void testSetRenameOnMove() {
        // ask for RenameOnMove and see that it is false.
        assertFalse("Initial value of RenameOnMove should have been false, but was not.",
            myFileOutputAdapter.isRenameOnMove());

        //set RenameOnMove to true
        myFileOutputAdapter.setRenameOnMove(true);
        // ask for RenameOnMove and see that it is true.
        assertTrue("Set RenameOnMove vale and did not get back same value.",
            myFileOutputAdapter.isRenameOnMove());

        //set RenameOnMove to false
        myFileOutputAdapter.setRenameOnMove(false);
        // ask for RenameOnMove and see that it is false
        assertFalse("Set RenameOnMove vale and did not get back same value.",
            myFileOutputAdapter.isRenameOnMove());

        // RenameOnMove should not be able to be changed after file is open.  Will test this in Open test.
    }
    
    

    /**
     * 
     * Test NEWFILE for:
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#openDestination()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#closeOnSuccess()}.
     */
    @Test()
    public void testOpenDestination() {
        //set filenameExtension to '.dat'
        myFileOutputAdapter.setFilenameExtension(".dat");
       //set filename to 'callitthis'
        myFileOutputAdapter.setFilenamePrefix("callitthis");
        //set fileMode = writeUnique -  default so not really necessary.
        myFileOutputAdapter.setFileMode("writeUnique");
        //set OutputDirectory to 'TempOut'
        myFileOutputAdapter.setOutputDirectory("TempOut");
        //set MoveToDirectory to 'export'
        myFileOutputAdapter.setMoveToDirectory("export");        
      
        //Open the destination file as a NewFile.
        try {
            myFileOutputAdapter.openDestination();
        } catch (VocollectException e1) {
            e1.printStackTrace();
        }

        //Todo:  some tests on the opened file maybe?
        
        
        //filename should not be able to be changed after file is open.
        //try to set filename to 'myfile.dat'
        try {
        myFileOutputAdapter.setFileName("myfile.dat");
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting filename while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for filename and make sure it is NOT myfile.dat
        assertFalse("Filename was able to be re-set while file was open.",
            myFileOutputAdapter.getFileName() == "myfile.dat");


        // fileMode should not be able to be changed after file is open.
        //try to set to overwrite
        try {
            myFileOutputAdapter.setFileMode("overwrite");
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting fileMode while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for fileMode and make sure it is writeUnique
        assertTrue("fileMode was able to be re-set while file was open.",
            myFileOutputAdapter.getFileMode() == "writeUnique");
        
        //filenameExtension should not be able to be changed after file is open.
        //try to set filenameExtension to '.abc'
        try {
            myFileOutputAdapter.setFilenameExtension(".abc");
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting filenameExtension while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for filenameExtension and make sure it is NOT .abc
        assertFalse("FilenameExtension was able to be re-set while file was open.",
            myFileOutputAdapter.getFilenameExtension() == ".abc");

        
        //filenamePrefix should not be able to be changed after file is open.  Will test this in Open test.
        //try to set filenamePrefix to 'BadPrefix'
        try {
            myFileOutputAdapter.setFilenamePrefix("BadPrefix");
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting filenamePrefix while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for filenamePrefix and make sure it is NOT BadPrefix
        assertFalse("FilenamePrefix was able to be re-set while file was open.",
            myFileOutputAdapter.getFilenamePrefix() == "BadPrefix");

        
        //OutputDirectory should not be able to be changed after file is open.  Will test this in Open test.
        //try to set OutputDirectory to 'Baddir'
        try {
            myFileOutputAdapter.setOutputDirectory("BadDir");
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting OutputDirectory while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for OutputDirectory and make sure it is NOT BadDir
        assertFalse("OutputDirectory was able to be re-set while file was open.",
            myFileOutputAdapter.getOutputDirectory() == "BadDir");

        
        //MoveToDirectory should not be able to be changed after file is open.  Will test this in Open test.
        //try to set MoveToDirectory to 'Baddir'
        try {
            myFileOutputAdapter.setMoveToDirectory("BadDir");
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting MoveToDirectory while file open should cause a RuntimeException, but it didn't.");
        }

        
        // DeleteOnFailure should not be able to be changed after file is open.
        //try to set DeleteOnFailure to true
        try {
            myFileOutputAdapter.setDeleteOnFailure(true);
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting DeleteOnFailure while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for DeleteOnFailure and make sure it is false
        assertFalse("DeleteOnFailure was able to be re-set while file was open.",
            myFileOutputAdapter.isDeleteOnFailure());
        

        // RenameOnMove should not be able to be changed after file is open.
        //try to set RenameOnMove to true
        try {
            myFileOutputAdapter.setRenameOnMove(true);
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting RenameOnMove while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for RenameOnMove and make sure it is false
        assertFalse("RenameOnMove was able to be re-set while file was open.", myFileOutputAdapter.isRenameOnMove());

        
        
        
        // ask for MoveToDirectory and make sure it is NOT BadDir
        assertFalse("MoveToDirectory was able to be re-set while file was open.",
            myFileOutputAdapter.getMoveToDirectory() == "BadDir");
        myFileOutputAdapter.writeRecord("This is a test record.");
        myFileOutputAdapter.closeOnSuccess();
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#writeRecord(java.lang.String)}.
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#closeOnSuccess()}.
     */
    @Test()
    public void testWriteRecord() {

        //set filenameExtension to '.dat'
        myFileOutputAdapter.setFilenameExtension(".dat");
       //set filename to 'callitthis'
        myFileOutputAdapter.setFilenamePrefix("WriteTests");
        //set fileMode writeUnique -- this is default, so not really needed.
        myFileOutputAdapter.setFileMode("writeUnique");
        //set OutputDirectory to 'TempOut'
        myFileOutputAdapter.setOutputDirectory("TempOut");
        //set MoveToDirectory to 'export'
        myFileOutputAdapter.setMoveToDirectory("export");
  
        //Open the destination file as a NewFile.
        try {
            myFileOutputAdapter.openDestination();
        } catch (VocollectException e) {
            e.printStackTrace();
        }
        myFileOutputAdapter.writeRecord("WriteRecord.");
        myFileOutputAdapter.writeRecord("This is a test 2.");
        myFileOutputAdapter.writeRecord("This is a test 3.");
        myFileOutputAdapter.writeRecord("This is a test 4.");
        myFileOutputAdapter.closeOnSuccess();
    }

    
    /**
     * Append test for:
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#openDestination()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#closeOnSuccess()}.
     */
    @Test()
    public void testOpenDestinationAPPEND() {
        
        //set filename to "MyAppendFile.dat"
        myFileOutputAdapter.setFileName("MyAppendFile.dat");
        //set appendToFile to true (APPEND)
        myFileOutputAdapter.setFileMode("append");
        //set OutputDirectory to 'export'
        myFileOutputAdapter.setOutputDirectory("export");
        
        //set TempOutputDirectory to 'TempOut'... this is not used with append.
        //myFileOutputAdapter.setMoveToDirectory("TempOut");
 
        //Open the destination file to APPEND.
        try {
            myFileOutputAdapter.openDestination();
        } catch (VocollectException e1) {
           e1.printStackTrace();
        }

        //Todo:  some tests on opened file might be good.
      
        
        //filename should not be able to be changed after file is open.
        //try to set filename to 'myfile.dat'
        try {
        myFileOutputAdapter.setFileName("myfile.dat");
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting filename while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for filename and make sure it is NOT myfile.dat
        assertFalse("Filename was able to be re-set while file was open.",
            myFileOutputAdapter.getFileName() == "myfile.dat");


        // fileMode should not be able to be changed after file is open.
        //try to set fileMode to overwrite
        try {
            myFileOutputAdapter.setFileMode("overwrite");
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting fileMode while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for fileMode and make sure it is APPEND
        assertTrue("fileMode was able to be re-set while file was open.",
            myFileOutputAdapter.getFileMode() == "append");

        
        //filenameExtension should not be able to be changed after file is open.
        //try to set filenameExtension to '.abc'
        try {
            myFileOutputAdapter.setFilenameExtension(".abc");
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting filenameExtension while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for filenameExtension and make sure it is NOT .abc
        assertFalse("FilenameExtension was able to be re-set while file was open.",
            myFileOutputAdapter.getFilenameExtension() == ".abc");

        
        //filenamePrefix should not be able to be changed after file is open.
        //try to set filenamePrefix to 'BadPrefix'
        try {
            myFileOutputAdapter.setFilenamePrefix("BadPrefix");
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting filenamePrefix while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for filenamePrefix and make sure it is NOT BadPrefix
        assertFalse("FilenamePrefix was able to be re-set while file was open.",
            myFileOutputAdapter.getFilenamePrefix() == "BadPrefix");

        
        //OutputDirectory should not be able to be changed after file is open.
        //try to set OutputDirectory to 'Baddir'
        try {
            myFileOutputAdapter.setOutputDirectory("BadDir");
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting OutputDirectory while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for OutputDirectory and make sure it is NOT BadDir
        assertFalse("OutputDirectory was able to be re-set while file was open.",
            myFileOutputAdapter.getOutputDirectory() == "BadDir");

        
        //MoveToDirectory should not be able to be changed after file is open.
        //try to set MoveToDirectory to 'Baddir'
        try {
            myFileOutputAdapter.setMoveToDirectory("BadDir");
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting MoveToDirectory while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for MoveToDirectory and make sure it is NOT BadDir
        assertFalse("MoveToDirectory was able to be re-set while file was open.",
            myFileOutputAdapter.getMoveToDirectory() == "BadDir");

        
        // DeleteOnFailure should not be able to be changed after file is open.
        //try to set DeleteOnFailure to true
        try {
            myFileOutputAdapter.setDeleteOnFailure(true);
        } catch (RuntimeException e) {
            //Expected this exceptioin
        } catch (Exception e) {
            // wrong exception
            fail("Resetting DeleteOnFailure while file open should cause a RuntimeException, but it didn't.");
        }
        // ask for appendToFile and make sure it is false (NEW FILE)
        assertFalse("DeleteOnFailure was able to be re-set while file was open.",
            myFileOutputAdapter.isDeleteOnFailure());
        
        myFileOutputAdapter.writeRecord("This is a test record.");
        myFileOutputAdapter.closeOnSuccess();

    }


    
    /**
     * Append test for RenameOnMove feature.
     * 
     * This test creates a file in TempOut\MyAppendFile2.dat
     * and then moves it to \export with a new name like:
     * Renamed-Wow-YYYYMMDDHHMMSS.dat
     */
    @Test()
    public void testRenameOnMove() {
        //set OutputDirectory to 'TempOut'
        myFileOutputAdapter.setOutputDirectory("TempOut");
        //set tempout filename to "MyAppendFile2.dat"
        myFileOutputAdapter.setFileName("MyAppendFile2.dat");
        //set fileMode to append.
        myFileOutputAdapter.setFileMode("append");

        //set MoveToDirectory to 'export'
        myFileOutputAdapter.setMoveToDirectory("export");      
        //set RenameOnMove to true
        myFileOutputAdapter.setRenameOnMove(true);
        //set filenameExtension to '.dat'
        myFileOutputAdapter.setFilenameExtension(".dat");
       //set filename to 'callitthis'
        myFileOutputAdapter.setFilenamePrefix("Renamed-Wow-");

        
        //Open the destination file to APPEND.
        try {
            myFileOutputAdapter.openDestination();
        } catch (VocollectException e) {
            e.printStackTrace();
        }

        myFileOutputAdapter.writeRecord("This is a test record for the file to be"
                + "Renamed on move from TemoOut/MyAppendFile2.dat to "
                + "export/Renamed-Wow-YYYYMMDDHHMMSS.dat.");
        myFileOutputAdapter.closeOnSuccess();

    }
    
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter#closeOnFailure()}.
     * This method makes two files in the TempOut directory.  The file that
     * begins with NotDeleteMe should remain, but the file called DeleteMe
     * should be deleted.
     */
    @Test()
    public void testDeleteOnFailure() {
        //set filenameExtension to '.dat'
        myFileOutputAdapter.setFilenameExtension(".dat");
        //set filename to 'DeleteMe'
        myFileOutputAdapter.setFilenamePrefix("IfYouSeeMeThenDeleteFailed");
        //set fileMode to writeUnique -- default so not necessary.
        myFileOutputAdapter.setFileMode("writeUnique");
        //set OutputDirectory to 'TempOut'
        myFileOutputAdapter.setOutputDirectory("TempOut");
        //set MoveToDirectory to 'export'
        myFileOutputAdapter.setMoveToDirectory("export");
        //set DeleteOnFailure to true.
        myFileOutputAdapter.setDeleteOnFailure(true);
        
        //Open the destination file as a NewFile.
        try {
            myFileOutputAdapter.openDestination();
        } catch (VocollectException e) {
            e.printStackTrace();
        }
        myFileOutputAdapter.writeRecord("This file should be deleted.");
        myFileOutputAdapter.writeRecord("If you found this file the DeleteOnFailure test FAILED!!!!");
        myFileOutputAdapter.closeOnFailure();

        //set filename to 'NotDeleteMe'
        myFileOutputAdapter.setFilenamePrefix("NotDeleteMe");
        //set DeleteOnFailure to false.
        myFileOutputAdapter.setDeleteOnFailure(false);

        //Open the destination file as a NewFile.
        try {
            myFileOutputAdapter.openDestination();
        } catch (VocollectException e) {
            e.printStackTrace();
        }
        myFileOutputAdapter.writeRecord("This file is part of the test for "
            + "DeleteOnFailure.  It should NOT have been deleted.");
        myFileOutputAdapter.closeOnFailure();
   
    }

    
    /**
     * Test that the markup we use is good.
     */
  
    @Test()
    public void testConfigSetup() {
        CastorConfiguration cc = new CastorConfiguration("import-definition.xml", "FileOutputAdapterTest.xml");
        Object obj = null;
        try {
            obj = cc.configure();
        } catch (ConfigurationException e1) {
            fail("Mapping and markup seem to be at odds.");
        }
        FileOutputAdapter foa = (FileOutputAdapter) obj;
        assertTrue("deleteOnFailure was not read properly from the XML markup.",
            foa.isDeleteOnFailure());
        assertTrue("fileMode was not read properly from the XML markup.",
            foa.getFileMode() == "append");
        assertEquals("The filename was not read properly from the XML markup.",
            "MyFilename.txt", foa.getFileName());
        assertEquals("The filenamePrefix was not read properly from the XML markup.",
            "MarkupTest", foa.getFilenamePrefix());
        assertEquals("The filenameExtension was not read properly from the XML markup.",
            ".txt", foa.getFilenameExtension());
        assertEquals("The outputDirectory was not read properly from the XML markup.",
            "TempOut", foa.getOutputDirectory());
        assertEquals("The moveToDirectory was not read properly from the XML markup.",
            "export", foa.getMoveToDirectory());
     }

    
    
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.adapters.OutputAdapter#writeRecord(java.lang.String)}.
     */
    @Test()
    public void testDelegateWriteMethods() {
        long charCount = 0;
        long recCount = 0;
        
        //set filenameExtension to '.dat'
        myFileOutputAdapter.setFilenameExtension(".txt");
       //set filename to 'callitthis'
        myFileOutputAdapter.setFilenamePrefix("DelegateWriteMethodTests");
        //set fileMode to writeUnique -- default so not really necessary.
        myFileOutputAdapter.setFileMode("writeUnique");
        //set OutputDirectory to 'TempOut'
        myFileOutputAdapter.setOutputDirectory("TempOut");
        //set MoveToDirectory to 'export'
        myFileOutputAdapter.setMoveToDirectory("export");
  
        //Open the destination file as a NewFile.
        try {
            myFileOutputAdapter.openDestination();
        } catch (VocollectException e1) {
            e1.printStackTrace();
        }
        
        //Write a line
        myFileOutputAdapter.writeRecord(
            "DelegateWriteMethodTests... the following 3 lines should each say 1234567890ABCDEFG.");
        recCount = recCount + 1;
        //Recordlength should be zero after writeRecord
        assertEquals("writeRecord did not reset recordLength properly.", myFileOutputAdapter.getRecordLength(), 0);
        //RecordCount should be zero after writeRecord
        assertEquals("writeRecord did not increment recordCount properly.",
            myFileOutputAdapter.getRecordCount(), recCount);


        // test java.io.BufferedWriter#write(java.lang.String, int, int)
        String myString = "This is a test 1234567890";
        try {
            myFileOutputAdapter.write(myString, 15, 10);
        } catch (IOException e) {
            fail("write(String, int, int) threw an IOException.");
        }
        charCount = charCount + 10;
        //Recordlength should be 10 after write
        assertEquals("write(string, int, int) did not reset recordLength properly.",
            myFileOutputAdapter.getRecordLength(), charCount);
        assertEquals("write(string, int, int) did not increment recordCount properly.",
            myFileOutputAdapter.getRecordCount(), recCount);
        

        // test java.io.BufferedWriter#write(int)
        try {
            //write an "A"
            myFileOutputAdapter.write(65);
        } catch (IOException e) {
            fail("write(int) threw an IOException.");
        }
        charCount = charCount + 1;
        //Recordlength should be 11 after write
        assertEquals("write(int) did not reset recordLength properly.",
            myFileOutputAdapter.getRecordLength(), charCount);
        assertEquals("write(int) did not increment recordCount properly.",
            myFileOutputAdapter.getRecordCount(), recCount);


        
        // test java.io.Writer#append(char)
        try {
            //Append "BC"
            myFileOutputAdapter.append('B').append('C').append('D');
        } catch (IOException e) {
            fail("append(char) threw an IOException.");
        }
        charCount = charCount + 3;
        //Recordlength should be 14 after write
        assertEquals("append(char) did not reset recordLength properly.",
            myFileOutputAdapter.getRecordLength(), charCount);
        assertEquals("append(char) did not increment recordCount properly.",
            myFileOutputAdapter.getRecordCount(), recCount);
      

        // test java.io.Writer#write(String)
        try {
            //write "EFG"
            myFileOutputAdapter.write("EFG");
        } catch (IOException e) {
            fail("write(String) threw an IOException.");
        }
        charCount = charCount + 3;
        //Recordlength should be 17 after write
        assertEquals("write(String) did not reset recordLength properly.",
            myFileOutputAdapter.getRecordLength(), charCount);
        assertEquals("write(String) did not increment recordCount properly.",
            myFileOutputAdapter.getRecordCount(), recCount);

        
        // test java.io.Writer#newLine
        try {
            //End this line.
            myFileOutputAdapter.newLine();
        } catch (IOException e) {
            fail("newLine() threw an IOException.");
        }
        charCount = 0;
        recCount = recCount + 1;
        //Recordlength should be 0 after newline
       assertEquals("newLine() did not reset recordLength properly.",
           myFileOutputAdapter.getRecordLength(), charCount);
       assertEquals("newLine() did not increment recordCount properly.", 
           myFileOutputAdapter.getRecordCount(), recCount);


       // test java.io.BufferedWriter#write(char[])
       char[] testData = {'1', '2', '3', '4'};
       try {
           //End this line.
           myFileOutputAdapter.write(testData);
       } catch (IOException e) {
           fail("write(int) threw an IOException.");
       }
       charCount = charCount + 4;
       //Recordlength should be 4
       assertEquals("write(char[]) did not reset recordLength properly.",
           myFileOutputAdapter.getRecordLength(), charCount);
       assertEquals("write(char[]) did not increment recordCount properly.",
           myFileOutputAdapter.getRecordCount(), recCount);
       
       

       // test java.io.Writer#append(java.lang.CharSequence)
       try {
           //test the append for a charsequence
           myFileOutputAdapter.append("567").append("890A").append("BCD").append("EFG");
       } catch (IOException e) {
           fail("append(CharSequence) threw an IOException.");
       }
       charCount = charCount + 13;
       //Recordlength should be 17
      assertEquals("append(CharSequence) did not reset recordLength properly.",
          myFileOutputAdapter.getRecordLength(), charCount);
      assertEquals("append(CharSequence) did not increment recordCount properly.",
          myFileOutputAdapter.getRecordCount(), recCount);


      
      // test java.io.Writer#newLine some more...
      try {
          //End this line.
          myFileOutputAdapter.newLine();
      } catch (IOException e) {
          fail("newLine() threw an IOException.");
      }
      charCount = 0;
      recCount = recCount + 1;
      //Recordlength should be 0 after newline
     assertEquals("newLine() did not reset recordLength properly.", myFileOutputAdapter.getRecordLength(), charCount);
     assertEquals("newLine() did not increment recordCount properly.", myFileOutputAdapter.getRecordCount(), recCount);
      
      
     // test java.io.BufferedWriter#write(char[], int, int)
      char[] testDataB = {'#', '%', '1', '2', '3', 'A', '$', '^'};
      try {
          //End this line.
          myFileOutputAdapter.write(testDataB, 2, 3);
      } catch (IOException e) {
          fail("write(int) threw an IOException.");
      }
      charCount = charCount + 3;
      //Recordlength should be 3
      assertEquals("write(char[]) did not reset recordLength properly.", 
          myFileOutputAdapter.getRecordLength(), charCount);
      assertEquals("write(char[]) did not increment recordCount properly.", 
          myFileOutputAdapter.getRecordCount(), recCount);


      // test flush()
      try {
           myFileOutputAdapter.flush();
      } catch (IOException e) {
          fail("flush() threw an IOException.");
      }
      //Recordlength should be same and line count should be same.
      assertEquals("append(CharSequence, int, int) did not reset recordLength properly.", 
          myFileOutputAdapter.getRecordLength(), charCount);
      assertEquals("append(CharSequence, int, int) did not increment recordCount properly.", 
          myFileOutputAdapter.getRecordCount(), recCount);
    

      // test append(java.lang.CharSequence, int, int)
      String testDataC = "XX456XX789XX0ABCDEFGXXXXX";
      try {
          //End this line.
          myFileOutputAdapter.append(testDataC, 2, 5).append(testDataC, 7, 10).append(testDataC, 12, 20);
      } catch (IOException e) {
          fail("write(int) threw an IOException.");
      }
      charCount = charCount + 3 + 3 + 8;
      //Recordlength should be 17
      assertEquals("append(CharSequence, int, int) did not reset recordLength properly.", 
          myFileOutputAdapter.getRecordLength(), charCount);
      assertEquals("append(CharSequence, int, int) did not increment recordCount properly.", 
          myFileOutputAdapter.getRecordCount(), recCount);

 
        // Note issue with last line of count.... should close check buffer and add one?
        
        
        
        myFileOutputAdapter.closeOnSuccess();

    }
    
    
    
}
