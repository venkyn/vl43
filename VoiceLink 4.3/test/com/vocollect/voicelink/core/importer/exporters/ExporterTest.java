/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.exporters;

import com.vocollect.epp.test.DepInjectionSpringContextNGTests;
import com.vocollect.voicelink.core.importer.BaseImportServiceManagerTestCase;
import com.vocollect.voicelink.core.importer.CastorConfiguration;
import com.vocollect.voicelink.core.importer.ConfigurationException;
import com.vocollect.voicelink.core.importer.DataSourceParser;
import com.vocollect.voicelink.core.importer.EncodingSupport;
import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.OutputFabricator;
import com.vocollect.voicelink.core.importer.adapters.FileDataSourceAdapter;
import com.vocollect.voicelink.core.importer.adapters.FileOutputAdapter;
import com.vocollect.voicelink.core.importer.fabricators.ExportFixedLengthFieldMap;
import com.vocollect.voicelink.core.importer.fabricators.FixedLengthOutputFabricator;
import com.vocollect.voicelink.core.importer.parsers.DatabaseDataSourceParser;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthDataSourceParser;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;
import com.vocollect.voicelink.test.DbUnitAdapterVoiceLink;

import java.util.Calendar;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test class for Exporter.
 * @author ktinguria
 */
public class ExporterTest extends DepInjectionSpringContextNGTests {

    private Exporter exporter = null;
    
    private String mappingFileName = "import-field-mapping.xml";

    private String configFileName  = "test-input-fields.xml";

    private String testDir         = null;

    private boolean overrideSite = false;
    
    private String siteName = "";

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.test.DepInjectionSpringContextNGTests#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return BaseImportServiceManagerTestCase.getImportTestConfigLocations();
    }

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeClass()
    public void setUpLocal() throws Exception {
        exporter = new Exporter();   
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();         
        adapter.resetInstallationData();
        adapter.loadFullMultiSiteData();
    }
    
    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeMethod()
    protected void setup() throws Exception {
        testDir = System.getProperty("test.input.dir");
        if (null == testDir) {
            testDir = "./";
        } else {
            testDir = testDir + "/";
        }
        setSiteName("Site 1");
    }
    
    /**
     *  Exporter test for Picked file.
     *
     */
    @Test()
    public void testExportPicked() {
        exportByDatatypeAndConfig("picked");
    }
  
    /**
     *  Exporter test for put file.
     *
     */
    @Test()
    public void testExportPut() {
        exportByDatatypeAndConfig("put");
    }
  
    /**
     *  Exporter test for Results LABOR Record type.
     *
     */
    @Test()
    public void testExportResultsLabor() {
        exportByDatatypeAndConfig("resultsLabor");
    }      

    /**
     *  Exporter test for Results COMPLETE Record type.
     *
     */
    @Test()
    public void testExportResultsComplete() {
        exportByDatatypeAndConfig("resultsComplete");
    }    
    
    /**
     *  Exporter test for Results COMPLETE Record type.
     *
     */
    @Test()
    public void testExportPutsResultsComplete() {
        exportByDatatypeAndConfig("ptsPutResultsComplete");
    }    
   
    /**
     *  Exporter test for CONTAINERS EXPORT.
     *
     */
    @Test()
    public void testExportContainers() {
        exportByDatatypeAndConfig("containers");
    }
    
    /**
     *  Exporter test for Pts CONTAINERS EXPORT.
     *
     */
    @Test()
    public void testExportPtsContainers() {
        exportByDatatypeAndConfig("ptsContainers");
    }
  
    /**
     *  Exporter test for Results ADJUSTQUANTITY Record type.
     *
     */
    @Test()
    public void testExportResultsFinish() {
        exportByDatatypeAndConfig("resultsFinish");
    }    
    
    /**
     *  Exporter test for OPERATORACTIONS EXPORT.
     *
     */
    @Test()
    public void testExportOperatorActions() {
        exportByDatatypeAndConfig("operatorActions");
    }    
    
    /**
     *  Exporter test for PUTAWAY EXPORT.
     *
     */
    @Test()
    public void testExportPutaway() {
       setSiteName("Default");
       exportByDatatypeAndConfig("putaway");
    }    
   
    /**
     *  Exporter test for Loading Route Complete EXPORT.
     *
     */
    @Test()
    public void testExportLoadingResultsComplete() {
       setSiteName("Default");
       exportByDatatypeAndConfig("loadingResultsComplete");
    }  
    
    /**
     *  Exporter test for Loading Labor EXPORT.
     *
     */
    @Test()
    public void testExportLoadingResultsLabor() {
       setSiteName("Default");
       exportByDatatypeAndConfig("loadingResultsLabor");
    }  
    
    /**
     *  Exporter test for Loading stop Complete EXPORT.
     *
     */
    @Test()
    public void testExportLoadingStops() {
       setSiteName("Default");
       exportByDatatypeAndConfig("loadingStop");
    }  
    
    /**
     *  Exporter test for Loading stop Complete EXPORT.
     *
     */
    @Test()
    public void testExportLoadingContainers() {
       setSiteName("Default");
       exportByDatatypeAndConfig("loadingContainer");
    }  
    
    
    /**
     *  Exporter test for REPLENISHMENT EXPORT.
     *
     */
    @Test()
    public void testExportReplenishment() throws Exception {
        setSiteName("Default");
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_ReplenishmentsCompleted.xml", 
            DatabaseOperation.REFRESH);
       exportByDatatypeAndConfig("replenishment");
       adapter.resetInstallationData();
       adapter.loadFullMultiSiteData();
    }    
    
    /**
     *  Exporter test for Results LABOR Record type.
     *
     */
    @Test()
    public void testExportResultsPtsLabor() {
        exportByDatatypeAndConfig("ptsResultsLabor");
    }    
    
    /**
     *  Exporter test for LINELOADING EXPORT.
     *
     */
    @Test()
    public void testExportLineloading() {
        setSiteName("Site 2");
        exportByDatatypeAndConfig("lineloading");
    }    
    
    /**
     *  Exporter test for Results COMPLETE Record type.
     *
     */
    @Test()
    public void testExportpurge() {
        exportByDatatypeAndConfig("purge");
    }    
    

    /**
     * Test method for 
     * 'com.vocollect.voicelink.core.importer.exporter.runDatatypeJob()'.
     * This test is using the FixedLengthDataSourceParser. Once
     * DatabaseDataSourceParser is ready we can add more tests.
     */
    private void testRunDatatypeJob() throws Exception {
        DataSourceParser<FixedLengthFieldMap, FixedLengthField> t = null;
        OutputFabricator<ExportFixedLengthFieldMap, FixedLengthField> of = null;
        t = new FixedLengthDataSourceParser();
        of = new FixedLengthOutputFabricator();
        of.setConfigFileName("test-input-fields.xml");
        of.setMappingFileName("export-field-mapping.xml");

        t.setConfigFileName(configFileName);
        t.setMappingFileName(mappingFileName);

        // Check config
        assertFalse(of.getFieldList().isEmpty(), "Empty field list");

        FileDataSourceAdapter dataSourceAdapter = new FileDataSourceAdapter();
        dataSourceAdapter.setParentDirectory(testDir + "data/import");
        dataSourceAdapter.setFileNamePattern("test.dat");
        dataSourceAdapter.setEncoding(EncodingSupport.DEFAULT_ENCODING);
        t.setSourceAdapter(dataSourceAdapter);

        // Check config
        assertNotNull(t.getSourceAdapter(), "SourceAdapter is null");
        assertFalse(t.getFieldList().isEmpty(), "Empty field list");

        // Open the destination with output Adapter
        FileOutputAdapter fileOutputAdapter = null;
        fileOutputAdapter = new FileOutputAdapter();
        fileOutputAdapter.setFileMode("writeUnique");
        fileOutputAdapter.setEncoding("UTF-8");
        fileOutputAdapter.setFileName("TestCreate");
        fileOutputAdapter.setFilenameExtension(".dat");
        fileOutputAdapter.setFilenamePrefix("OFab");
        fileOutputAdapter.setMoveToDirectory(testDir + "Export");
        fileOutputAdapter.setOutputDirectory(testDir + "OFabTemp");
        of.setOutputAdapter(fileOutputAdapter);
        of.getOutputAdapter().openDestination();

        exporter.setSourceParser(t);
        exporter.setOutputFabricator(of);
        exporter.prepare();
        exporter.start();
        // assertTrue(exporter.runDatatypeJob(), "Export successful");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.exporter.runDatatypeJob()'.
     */
    

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.Exporter.getOutputFabricator()'.
     */
    @Test()
    public void testGetOutputFabricator() {
        try {
            OutputFabricator<ExportFixedLengthFieldMap, FixedLengthField> of = 
                new FixedLengthOutputFabricator();    
            exporter.setOutputFabricator(of);
            assertNotNull(exporter.getOutputFabricator(), "Output fabricator is null");
       } catch (ConfigurationException e) {
            e.printStackTrace();
       }
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.Exporter.getDataSourceParser()'.
     * Also Test method for 'com.vocollect.voicelink.core.importer.Exporter.setDataSourceParser()'.
     */
    @Test()
     public void testGetSetDataSourceParser() {
        try {
            DataSourceParser<FixedLengthFieldMap, FixedLengthField> dsp = 
                new DatabaseDataSourceParser();    
            exporter.setSourceParser(dsp);
            assertNotNull(exporter.getSourceParser(), "Database data source parser is null");
       } catch (ConfigurationException e) {
            e.printStackTrace();
       }
    }

    /**
     *  Exporter test for Results MARKOUT Record type.
     *  This was for VL 2.2 only.
     */
    @Test(enabled = false)
    public void testExportResultsMarkout() {
        exportByDatatypeAndConfig("resultsMarkout");
    }    

    /**
     *  Exporter test for Results ADJUSTQUANTITY Record type.
     *  This was for VL 2.2 results export only
     */
    @Test(enabled = false)
    public void testExportResultsAdjustQuantity() {
        exportByDatatypeAndConfig("resultsAdjustQuantity");
    }    

    /**
     *  Exporter test for Results CANCEL Record type.
     *  This was for VL 2.2 results export only
     */
    @Test(enabled = false)
    public void testExportResultsCancel() {
        exportByDatatypeAndConfig("resultsCancel");
    }      

    /**
     *  Exporter test for Results VARIABLE Record type.
     *  This was for VL 2.2 results export only.
     */
    @Test(enabled = false)
    public void testExportResultsVariable() {
        exportByDatatypeAndConfig("resultsVariable");
    }        
    
    /**
     * @param datatype - the data type string
     * @return boolean
     */
    private boolean exportByDatatypeAndConfig(String datatype) {
        CastorConfiguration cc = new CastorConfiguration("import-definition.xml", datatype + "-export-setup.xml");
        Exporter i = null;
        try {
            i = (Exporter) cc.configure();
        } catch (ConfigurationException e) {
            assertFalse(true, "configuration blew a lung.");
        }

        if (isOverrideSite()) {
            i.setSiteName(getSiteName());
        }

        i.prepare();
        i.start();
        
        // TODO Make a separate performance test.
        Calendar start = Calendar.getInstance();

        boolean result = i.runDatatypeJob();
        Calendar end = Calendar.getInstance();
        
        //System.err.println(end.getTimeInMillis() - start.getTimeInMillis());
        
        return result;

    }

    /**
     * @return the overrideSite
     */
    private boolean isOverrideSite() {
        return overrideSite;
    }
  
 
    /**
     * @param overrideSite the overrideSite to set
     */
    private void setOverrideSite(boolean overrideSite) {
        this.overrideSite = overrideSite;
    }    
    
    
    /**
     * @return the siteName
     */
    private String getSiteName() {
        return siteName;
    }    
    

    /**
     * @param siteName the siteName to set
     */
    private void setSiteName(String siteName) {
        this.siteName = siteName;
        this.setOverrideSite(true);
    }    
    
    

}
