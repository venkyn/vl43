/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.exporters;

import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;

/**
 * @author Kalpna
 */
public class ExporterErrorTest {

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.exporter.ExporterError'.
     */
    @Test()
    public void testExporterError() {
        try {
            ExporterError.FAILED_EXPORT.toString();
        } catch (RuntimeException e) {
            assertFalse(true , e.getMessage());
        }
        // This checks that the error codes are unique within the
        // OutputFabricatorError and are within bounds
    }

}
