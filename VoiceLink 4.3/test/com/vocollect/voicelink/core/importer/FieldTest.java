/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;

/**
 * 
 * @author astein
 *
 */
public class FieldTest {

    private Field f = null;

    /**
     * @throws java.lang.Exception if unsuccessful
     */

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {

//      org.exolab.castor.tools.MappingTool mapper = new MappingTool();
//      mapper.addClass(Field.class);
      
//      mapper.write(new FileWriter("data/importvalidator.mapping.xml"));
//      StringWriter xmlOut = new StringWriter();
//      mapper.write(xmlOut);


    }
    
    /**
     * Make a new field for every method call.
     */
    @BeforeMethod()
    public void methodSetUp() {
        this.f = new Field();
    }



    /**
     * Test method for 'com.vocollect.voicelink.core.importer.Field.Field()'.
     */
    @Test()
    public final void testField() {
        assertNotNull(f);

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.Field(String)'.
     */
    @Test()
    public final void testFieldString() {
        this.f = new Field("testFieldData");
        assertNotNull(f);
        assertEquals("Should be 'testFieldData'", f.getFieldData(),
                "testFieldData");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.addFieldFilter(Filter)'.
     */
//    @Test()
//    public final void testAddFieldFilter() {
//        assertEquals("Should be an empty list of filters.", f.getFilters()
//                .size(), 0);
//        // add a filter to test
//        // f.addFieldFilter(new Filter());
//    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.addFieldManipulator(Manipulator)'.
     */
    @Test()
    public final void testAddFieldManipulator() {
        // add a manipulator to test
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.getFieldData()'. Test method
     * for 'com.vocollect.voicelink.core.importer.Field.setFieldData(String)'
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.setFieldData(char[])' Test
     * method for
     * 'com.vocollect.voicelink.core.importer.Field.setFieldData(char[], int,
     * int)'
     */
    @Test()
    public final void testGetSetFieldData() {
        assertNull("Field Data should be null", f.getFieldData());
        f.setFieldData("this is a String test");
        assertEquals("Field data should be 'this is a String test'", f
                .getFieldData(), "this is a String test");
        char[] chararray = { 'a', 'b', 'c', 'd', 'e' };
        f.setFieldData(chararray);
        assertEquals("Field data should be 'abcde'", f.getFieldData(), "abcde");
        f.setFieldData(chararray, 2, 3);
        assertEquals("Field data should be 'cde'", f.getFieldData(), "cde");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.getDataType()'. Test method
     * for 'com.vocollect.voicelink.core.importer.Field.setDataType(String)'
     */
    @Test()
    public final void testGetSetDataType() {
        assertEquals("DataType should initialize at 'String'", f.getDataType(),
                "String");
        f.setDataType("int");
        assertEquals("Datatype should be 'int'", f.getDataType(), "int");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.getFieldName()'. Test method
     * for 'com.vocollect.voicelink.core.importer.Field.setFieldName(String)'
     */
    @Test()
    public final void testGetSetFieldName() {
        assertNull("FieldName should be null.", f.getFieldName());
        f.setFieldName("TestingFieldName");
        assertEquals("FieldName should be 'TestingFieldName'",
                f.getFieldName(), "TestingFieldName");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.getFilters()'.
     */
    @Test()
    public final void testGetFilters() {
        // deferred

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.setFilters(List)'.
     */
    @Test()
    public final void testSetFilters() {
        // deferred

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.getManipulators()'.
     */
    @Test()
    public final void testGetManipulators() {
        // deferred

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.setManipulators(List)'.
     */
    @Test()
    public final void testSetManipulators() {
        // deferred

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.getTagName()'. Test method
     * for 'com.vocollect.voicelink.core.importer.Field.setTagName(String)'
     */
    @Test()
    public final void testGetSetTagName() {
        f.setTagName("TestingTagName");
        assertEquals("TagName should be 'TestingTagName'", f.getTagName(),
                "TestingTagName");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.getValidationList()'.
     */
    @Test()
    public final void testGetValidationList() {
        // Deferred
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.setValidationList(FieldValidator)'.
     */
    @Test()
    public final void testSetValidationList() {
        // deferred
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.getCloseTag()'. Test method
     * for 'com.vocollect.voicelink.core.importer.Field.setCloseTag(String)'
     */
    @Test()
    public final void testGetSetCloseTag() {
        assertNull("CloseTag should be null.", f.getCloseTag());
        f.setCloseTag("TestingCloseTag");
        assertEquals("CloseTag should be 'TestingCloseTag'", f.getCloseTag(),
                "TestingCloseTag");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.Field.getOpenTag()'. Test method
     * for 'com.vocollect.voicelink.core.importer.Field.setOpenTag(String)'
     */
    @Test()
    public final void testGetSetOpenTag() {
        assertNull("OpenTag should be null.", f.getOpenTag());
        f.setOpenTag("TestingOpenTag");
        assertEquals("OpenTag should be 'TestingOpenTag'", f.getOpenTag(),
                "TestingOpenTag");
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Field#isIndexable()}.
     */
    @Test()
    public void testIsIndexable() {
        assertFalse("isIndexible should be false by default", f.isIndexable());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Field#setIndexable(boolean)}.
     */
    @Test()
    public void testSetIndexable() {
        assertFalse("isIndexible should be false by default", f.isIndexable());
        f.setIndexable(true);
        assertTrue("isIndexible should now be true", f.isIndexable());
        f.setIndexable(false);
        assertFalse("isIndexible should now be false ", f.isIndexable());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Field#isRemapped()}.
     */
    @Test()
    public void testIsRemapped() {
        assertFalse("isRemapped should be false by default", f.isRemapped());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Field#setRemapped(boolean)}.
     */
    @Test()
    public void testSetRemapped() {
        assertFalse("isRemapped should be false by default", f.isRemapped());
        f.setRemapped(true);
        assertTrue("isRemapped should be true now", f.isRemapped());
        f.setRemapped(false);
        assertFalse("isRemapped should be false now", f.isRemapped());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Field#isSuppress()}.
     */
    @Test()
    public void testIsSuppress() {
        assertFalse("isSuppress should be false by default", f.isSuppress());
        
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Field#setSuppress(boolean)}.
     */
    @Test()
    public void testSetSuppress() {
        assertFalse("isSuppress should be false by default", f.isSuppress());
        f.setSuppress(true);
        assertTrue("isSuppress should be true now", f.isSuppress());
        f.setSuppress(false);
        assertFalse("isSuppress should be false now", f.isSuppress());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Field#isKeyField()}.
     */
    @Test()
    public void testIsKeyField() {
        assertFalse("isKeyField should be false by default", f.isKeyField());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Field#setKeyField(boolean)}.
     */
    @Test()
    public void testSetKeyField() {
        assertFalse("isKeyField should be false by default", f.isKeyField());
        f.setKeyField(true);
        assertTrue("isKeyField should be true now", f.isKeyField());
        f.setKeyField(false);
        assertFalse("isKeyField should be false now", f.isKeyField());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Field#isWriteOnOpenContext()}.
     */
    @Test()
    public void testIsWriteOnOpenContext() {
        assertFalse("isWriteOnOpenContext should be false by default", f.isWriteOnOpenContext());
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Field#setWriteOnOpenContext(boolean)}.
     */
    @Test()
    public void testSetWriteOnOpenContext() {
        assertFalse("isWriteOnOpenContext should be false by default", f.isWriteOnOpenContext());
        f.setWriteOnOpenContext(true);
        assertTrue("isWriteOnOpenContext should be true now", f.isWriteOnOpenContext());
        f.setWriteOnOpenContext(false);
        assertFalse("isWriteOnOpenContext should be false now", f.isWriteOnOpenContext());
    }
    
    /**
     * 
     */
    @Test()
    public void testSetGetSetterAndGetter() {
        Field g = new Field();
        g.setFieldName("test");
        assertEquals("Did not set getter name appropriately", g.getFieldName(), g.getGetterName());
        assertEquals("Did not set getter name appropriately", g.getFieldName(), g.getSetterName());
        g.setSetterName("newsetter");
        assertEquals("Unable to set setter name", "newsetter", g.getSetterName());
        g.setGetterName("newgetter");
        assertEquals("Unable to set getter name", "newgetter", g.getGetterName());
    }
    
    /**
     * test the markup for the field objects.
     */
    @Test()
    public void testFieldConfig() {
        CastorConfiguration cc = 
            new CastorConfiguration("import-definition.xml", "baseField.xml");
        try {
            f = (Field) cc.configure();
        } catch (ConfigurationException e) {
            assertFalse("configuration threw an exception.", true);
        }
        // Now we assert that all of the defaults have been changed by the config.
        assertTrue("isIndexable should be true", f.isIndexable());
        assertTrue("isRemapped should be true", f.isRemapped());
        assertTrue("isSupress should be true", f.isSuppress());
        assertTrue("isKeyField should be true", f.isKeyField());
        assertTrue("isRemapped should be true", f.isWriteOnOpenContext());
        assertEquals("Field name should be 'junk'", "junk", f.getFieldName());
        assertEquals("Field tag should be 'junk-tag'", "junk-tag", f.getTagName());
    }

}
