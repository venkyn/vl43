/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.test.DepInjectionSpringContextNGTests;
import com.vocollect.voicelink.test.DbUnitAdapterVoiceLink;

import com.opensymphony.xwork2.ObjectFactory;

import java.util.Calendar;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

/**
 * @author dgold
 *
 */
public class ImporterDatabaseTest extends DepInjectionSpringContextNGTests {

    protected static final String DATA_DIR = "data/dbunit/voicelink/";
    
    private String testDir = null;

    private boolean overrideSite = false;

    private String siteName = "";

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.test.DepInjectionSpringContextNGTests#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return BaseImportServiceManagerTestCase.getImportTestConfigLocations();
    }
    
    /**
     * Method to ensure struts is intialized correctly for all validator tests.
     * If the OnjectFactory is not create then NoClassDefError will be generated.
     * This was done with BeforeSuite so it will be intialized before any tests 
     * that need it are ran. 
     */
    @BeforeSuite
    protected void strutsValidatorSetup() {
        ObjectFactory.setObjectFactory(new ObjectFactory()); 
    }
    
    /**
     * @throws Exception if unsuccessful
     */
    @BeforeClass()
    protected void importerSetUp() throws Exception {

        testDir = System.getProperty("test.input.dir");
        if (null == testDir) {
            testDir = "./";
        } else {
            testDir = testDir + "/";
        }

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();

    }
    
    /**
     * @return the siteName
     */
    public String getSiteName() {
        return siteName;
    }

    /**
     * @param datatype - the data type string
     * @return boolean
     * @throws Exception
     */
    private boolean importByDatatypeAndConfig(String datatype) throws Exception {
        CastorConfiguration cc = 
            new CastorConfiguration("import-definition.xml", datatype + "-dbimport-" + "setup.xml");
        Importer i = null;
            i = (Importer) cc.configure();

        if (overrideSite) {
            i.setSiteName(getSiteName());
        }

        i.prepare();
        i.start();
        
        Calendar start = Calendar.getInstance();

        boolean result = i.runDatatypeJob();
        Calendar end = Calendar.getInstance();
        
        System.err.println(end.getTimeInMillis() - start.getTimeInMillis());
        
        return result;

    }

    /**
     * 
     *
     */
    @Test()
    public void testDBImportItems() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_Item_Import_Data.xml",
            DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("item");
    }
    
    /**
     * 
     *
     */
    @Test()
    public void testDBImportLots() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml", DatabaseOperation.REFRESH);        
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_Lot_Import_Data.xml",
            DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("lot");
    }    

    /**
     * 
     *
     */
    @Test()
    public void testImportAssignments() throws Exception {
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();         
        adapter.resetInstallationData();
        adapter.loadSelectionMultiSiteData();

        //Load database import table with Assignment import data
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoicelinkDataUnitTest_Assignment_DB_Import.xml", DatabaseOperation.REFRESH);
        
        importByDatatypeAndConfig("assignment");
    }

    
    /**
     * This tests the database Location import.
     *
     */
    @Test()
    public void testDBImportLocation() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_Location_import_data.xml",
            DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("location");
    }

    /**
     * Customer Location import test - used in Put To Store to map customers
     * to delivery locations. 
     *
     */
    @Test(enabled = true)
    public void testImportPtsCustomerLocations() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();       
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml", DatabaseOperation.REFRESH);   
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoicelinkDataUnitTest_Pts_Customer_Location_import_data.xml",
            DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("customer-location");      
    }

    
    
    /**
     * PtsLicense / PtsPut import test for Put To Store Licenses and Puts.
     *
     */
    @Test(enabled = true)
    public void testImportPtsLicenses() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();       
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoicelinkDataUnitTest_PtsLicense_PtsPut_import_data.xml", DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("PtsLicense");      
    }

    
    
    /**
     * This tests the database Operator import.
     *
     */
    @Test()
    public void testDBImportOperator() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
       adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_Core_Operator_Import_Data.xml",
            DatabaseOperation.REFRESH);
       importByDatatypeAndConfig("operator");
    }

    /**
     * This tests the database LineLoading (Carton) import.
     *  Note that LineLoading = carton
     */
    @Test()
    public void testDBImportLineLoadingCarton() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_LineLoadReset.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_LineLoadRegions.xml", DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_LineLoad_TableImport.xml", DatabaseOperation.REFRESH);

        importByDatatypeAndConfig("lineloading");
    }

    
    /**
     * This tests the database (Putaway) License import.
     *
     */
    @Test()
    public void testDBImportPutawayLicense() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml",
            DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_PutawayReset.xml",
            DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_ForkAppWorkGroups.xml",
                DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_PutawayRegions.xml",
            DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_PutawayData.xml",
            DatabaseOperation.REFRESH);
        // Data to be imported
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_PutawayLicense_import_data.xml",
                DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("putaway-license");
    }
    
    /**
     * This tests the database Replenishment import.
     *
     */
    @Test()
    public void testDBImportReplenishments() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_Core.xml",
            DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_ReplenReset.xml",
            DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_ForkAppWorkGroups.xml",
                DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_ReplenRegions.xml",
            DatabaseOperation.REFRESH);
        // Data to be imported
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_Replenishment_import_data.xml",
                DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("replenishment");
    }

    /**
     * This tests the database LocationItem import.
     *
     */
    @Test()
    public void testDBImportLocationReplenish() throws Exception {
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();         
        adapter.resetInstallationData();
        adapter.loadReplenishmentMultiSiteData();
        // Data to be imported
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_Location_Item_import_data.xml",
                DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("location-replenish");
    }
    
    /**
     * This tests the database Loading route stops import.
     *
     */
    @Test()
    public void testDBImportLoadingRouteStops() throws Exception {
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();         
        adapter.resetInstallationData();
        // Data to be imported
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_Loading_Route_Stop_import_data.xml",
                DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("loadingRoute");
    }
    
    /**
     * This tests the database Loading route stops import.
     *
     */
    @Test()
    public void testDBImportLoadingContainers() throws Exception {
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();         
        adapter.resetInstallationData();
        // Data to be imported
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_Loading_Container_import_data.xml",
                DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("loadingContainer");
    }
    
    /**
     * This tests the database DataTranslation import.
     *
     */
    @Test()
    public void testDBImportDataTranslation() throws Exception {
        DbUnitAdapterVoiceLink adapter = new DbUnitAdapterVoiceLink();         
        adapter.resetInstallationData();
        adapter.loadCoreMultiSiteData();
        // Load basic stuff
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_DataTranslationImport.xml",
                DatabaseOperation.CLEAN_INSERT);
        adapter.handleFlatXmlResource(
                "data/dbunit/voicelink/VoiceLinkDataUnitTest_DataTranslationImport.xml",
                DatabaseOperation.REFRESH);
        importByDatatypeAndConfig("datatranslations");
    }

}
