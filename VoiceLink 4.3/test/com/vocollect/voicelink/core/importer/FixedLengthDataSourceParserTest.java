/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;


import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.adapters.FileDataSourceAdapter;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthDataSourceParser;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import java.util.ArrayList;
import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;
import static org.testng.Assert.fail;

/**
 * TestNG test for FixedLengthDataSourceParser object and methods.
 * 
 * IMPORTANT NOTE:  A few of these tests ( testIndexesClear() and testPrescan())
 *  are also testing the DataSourceParser's Indexes features.  If these tests
 *  are modified, they must maintain the existing tests, or at least move those
 *  into the DataSourceparserTest.java file.
 * 
 * 
 * @author dgold
 */
@SuppressWarnings("unchecked")
public class FixedLengthDataSourceParserTest {
    
    private String mappingFileName = "import-field-mapping.xml";

    private String configFileName  = "test-input-fields.xml";

    private String testDir         = null;

    //used to keep ProperOrder array for testing.
    private Long[][] myProperOrder = null; 

    /**
     * setup - this sets the test input directory from the test setup XML file.
     */
    @BeforeClass()
    protected void setUp() {

      testDir = System.getProperty("test.input.dir");
      if (null == testDir) {
          testDir = "./test/";
      } else {
          testDir = testDir + "/";
      }
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.FixedLengthDataSourceParser.configure()'.
     */
    @Test()
    public void testConfigure() {
        FixedLengthDataSourceParser t = null;
        try {
            t = new FixedLengthDataSourceParser();
        } catch (ConfigurationException e) {
            fail("Constructor threw exception :" + e.getMessage());
        }

        try {
            t.setMappingFileName(mappingFileName);
            t.setConfigFileName(configFileName);
            t.configure();
        } catch (ConfigurationException e) {
            fail("configure threw exception: " + e.getMessage());
        }
        try {
            t.setSourceAdapter(new FileDataSourceAdapter());
        } catch (VocollectException e) {
            fail("FileDataSourceAdapter constructor threw exception"
                    + e.getMessage());
        }

        org.testng.AssertJUnit.assertNotNull("SourceAdapter is null", t
                .getSourceAdapter());
        org.testng.AssertJUnit.assertFalse("Empty field list", t.getFieldList()
                .isEmpty());
        org.testng.AssertJUnit.assertFalse("Record length <= 0", t
                .getRecordLength() <= 0);
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.FixedLengthDataSourceParser.getRecord()'.
     */
    @Test()
    public void testGetRecord() {
        FixedLengthDataSourceParser t = null;
        try {
            t = new FixedLengthDataSourceParser();
        } catch (ConfigurationException e) {
            fail("Constructor threw exception" + e.getMessage());
        }
        try {
            t.setConfigFileName(configFileName);
            t.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            fail("config threw exception" + e1.getMessage());
        }
        try {
            FileDataSourceAdapter dataSourceAdapter = new FileDataSourceAdapter();
            dataSourceAdapter.setParentDirectory(testDir + "data/import");
            dataSourceAdapter.setFileNamePattern("test.dat");
            dataSourceAdapter.setEncoding(EncodingSupport.DEFAULT_ENCODING);
            t.setSourceAdapter(dataSourceAdapter);
        } catch (VocollectException e) {
            fail("FileDataSourceAdapter constructor threw exception"
                    + e.getMessage());
        }
        // We didn't bother to do a prescan here, so 
        t.setPreScan(false);
        // Check config
        assertNotNull(t.getSourceAdapter(), "SourceAdapter is null");
        assertFalse(t.getFieldList().isEmpty(), "Empty field list");
        assertFalse(t.getRecordLength() <= 0, "Record length <= 0");
        HashMap<String, ? extends Field> fields = null;
        ObjectContext objectContext = null;
        try {
            objectContext = t.getRecord();
            fields = (FieldMap) objectContext.getFieldMaps().get(0);
        } catch (VocollectException e) {
            e.printStackTrace();
            org.testng.AssertJUnit
                    .assertFalse("Exception reading record", true);
        }
        assertTrue(t.getSourceAdapter().isEndOfRecord(),
            "End of record, remaining: "
            + ((FileDataSourceAdapter) (t.getSourceAdapter()))
                        .getBufferedCharsRemaining());
        assertFalse(t.getSourceAdapter().isEndOfData(), "End of data");
        for (Field x : fields.values()) {
            assertTrue(x.getFieldData().length() > 0, "Null field data");
        }
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.FixedLengthDataSourceParser.getNextField()'.
     */
    @Test()
    public void testGetNextField() {
        DataSourceParser<FixedLengthFieldMap, FixedLengthField> t = null;
        try {
            t = new FixedLengthDataSourceParser();
        } catch (ConfigurationException e) {
            fail("Constructor threw exception" + e.getMessage());
        }
        try {
            t.setConfigFileName(configFileName);
            t.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            fail("Constructor threw exception" + e1.getMessage());
        }
        try {
            FileDataSourceAdapter dataSourceAdapter = new FileDataSourceAdapter();
            dataSourceAdapter.setParentDirectory(testDir + "data/import");
            dataSourceAdapter.setFileNamePattern("test.dat");
            dataSourceAdapter.setEncoding(EncodingSupport.DEFAULT_ENCODING);
            t.setSourceAdapter(dataSourceAdapter);
        } catch (VocollectException e) {
            fail("FileDataSourceAdapter constructor threw exception"
                    + e.getMessage());
        }
        Field f = null;
        Field lastField = null;
        // we didn't do a prescan here, so turn this off.
        t.setPreScan(false);
        try {
            try {
                t.getRecord();
                f = t.getNextField();
            } catch (VocollectException e) {
                org.testng.AssertJUnit.assertFalse("Exception reading record: "
                        + e.getMessage(), true);
            }
            org.testng.AssertJUnit.assertNotNull(f);
            org.testng.AssertJUnit.assertEquals("Wrong first field", 0, f
                    .getFieldName().compareTo("father"));
            f = t.getNextField();
            org.testng.AssertJUnit.assertEquals("Wrong fname field", 0, f
                    .getFieldName().compareTo("firstName"));
            f = t.getNextField();
            org.testng.AssertJUnit.assertEquals("Wrong lname field", 0, f
                    .getFieldName().compareTo("lastName"));
            f = t.getNextField();
            org.testng.AssertJUnit.assertEquals("Wrong month field", 0, f
                    .getFieldName().compareTo("month"));
            f = t.getNextField();
            org.testng.AssertJUnit.assertEquals("Wrong day field", 0, f
                    .getFieldName().compareTo("day"));
            f = t.getNextField();
            org.testng.AssertJUnit.assertEquals("Wrong year field", 0, f
                    .getFieldName().compareTo("year"));
            while (null != (f = t.getNextField())) {
                lastField = f;
                // Just getting the rest of the fields, expecting to end up with 
                // field_h as the last field.
            }
            org.testng.AssertJUnit.assertEquals("Wrong name for last field", "field_h", lastField.getFieldName());
        } catch (OutOfDataException e) {
            fail("File not present or empty test file " + t.getSourceAdapter().getDataSourceName());
        }
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.importer.FixedLengthDataSourceParser.FixedLengthDataSourceParser(String)'.
     */
    @Test()
    public void testFixedLengthDataSourceParser() {
            DataSourceParser<FixedLengthFieldMap, FixedLengthField> t = null;
            try {
                t = new FixedLengthDataSourceParser();
            } catch (ConfigurationException e) {
                fail("Constructor threw exception" + e.getMessage());
            }
            // Check config
            org.testng.AssertJUnit.assertNotNull("SourceAdapter is null", t);
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.FixedLengthDataSourceParser.getField(String)'.
     */
    @Test()
    public void testGetField() {
        FixedLengthDataSourceParser t = null;
        try {
            t = new FixedLengthDataSourceParser();
        } catch (ConfigurationException e) {
            fail("Constructor threw exception" + e.getMessage());
        }
        try {
            t.setConfigFileName(configFileName);
            t.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            fail("configure threw exception" + e1.getMessage());
        }

        try {
            FileDataSourceAdapter dataSourceAdapter = new FileDataSourceAdapter();
            dataSourceAdapter.setParentDirectory(testDir + "data/import");
            dataSourceAdapter.setFileNamePattern("test.dat");
            dataSourceAdapter.setEncoding(EncodingSupport.DEFAULT_ENCODING);
            t.setSourceAdapter(dataSourceAdapter);
        } catch (VocollectException e) {
            fail("FileDataSourceAdapter constructor threw exception"
                    + e.getMessage());
        }
        // we didn't do a prescan so turn prescan off..
        t.setPreScan(false);
        Field f = t.getField("father");
        org.testng.AssertJUnit.assertNotNull(f);
        try {
            t.getRecord();
        } catch (VocollectException e) {
            e.printStackTrace();
            org.testng.AssertJUnit
                    .assertFalse("Exception reading record", true);
        }
        f = t.getField("father");
        org.testng.AssertJUnit.assertEquals(
                "Wrong field name, looking for recnum got " + f.getFieldName(),
                0, f.getFieldName().compareTo("father"));
        org.testng.AssertJUnit.assertEquals(
                "Wrong field data, looking for recnum got " + f.getFieldData(),
                0, f.getFieldData().compareTo("01"));
    }

    
    
    /**
     * Test method for 'com.vocollect.voicelink.core.importer.FixedLengthDataSourceParser.preScan()'.
     * ALSO Test method for 'com.vocollect.voicelink.core.importer.DataSourceParser.setIndexes()'.
     * ALSO Test method for 'com.vocollect.voicelink.core.importer.DataSourceParser.isIndexesEmpty()'.
     * ALSO Test method for 'com.vocollect.voicelink.core.importer.DataSourceParser.isAnyFailFileIndexBlown()'.
     * ALSO Test method for 'com.vocollect.voicelink.core.importer.DataSourceParser.isAnyFailRecordIndexBlown()'.
     * ALSO Test method for 'com.vocollect.voicelink.core.importer.DataSourceParser.getRecordDataDuped()'.
     *
     * IMPORTANT NOTE:  A few of these tests ( testIndexesClear() and testPrescan())
     *  are also testing the DataSourceParser's Indexes features.  If these tests
     *  are modified, they must maintain the existing tests, or at least move those
     *  into the DataSourceparserTest.java file.
     */
    @Test()
    public void testPreScan() {
        /*
         * This prescan is always on.
         */
        Indexes myIndexes = new Indexes();
        ArrayList <Field> index1List = new ArrayList<Field>();
        ArrayList <Field> index2List = new ArrayList<Field>();
        ArrayList <Field> index3List = new ArrayList<Field>();
        
        FixedLengthDataSourceParser t = null;
        try {
            t = new FixedLengthDataSourceParser();
            t.setConfigFileName("test.config");
        } catch (ConfigurationException e) {
            fail("Constructor threw exception" + e.getMessage());
        }
        try {
            t.setConfigFileName(configFileName);
            t.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            fail("configure threw exception" + e1.getMessage());
        }
        try {
            assertTrue(t.configure(), "Bad config");
        } catch (ConfigurationException e) {
            fail("configure threw exception" + e.getMessage());
        }

       
        //------ This section sets up the Indexes
        
        // create a field list to make 2 indexes...
        // #1 father|, #2 firstName|lastName|
        index1List.add(t.getField("father"));

        index2List.add(t.getField("firstName"));
        index2List.add(t.getField("lastName"));

        index3List.add(t.getField("month"));

        
        Index myIndex = new Index(index1List);
        assertEquals(myIndex.indexFieldsName(), "father|", "indexFieldsName didn't return updated value.");
        myIndex.setFailFile(true);
        Index myIndex2 = new Index(index2List);
        assertEquals(myIndex2.indexFieldsName(), "firstName|lastName|", "indexFieldsName didn't return updated value.");
        myIndex.setFailRecord(true);
        Index myIndex3 = new Index(index3List);
        assertEquals(myIndex3.indexFieldsName(), "month|", "indexFieldsName didn't return updated value.");
        
        //Add all indexes... to external Indexes object myIndexes
        assertTrue(myIndexes.addIndex(myIndex), "Failed to add index1.");
        assertTrue(myIndexes.addIndex(myIndex2), "Failed to add index2.");
        assertTrue(myIndexes.addIndex(myIndex3), "Failed to add index3.");

        // set indexes and ensure it is NOT empty
        t.setIndexes(myIndexes);
        assertFalse(t.isIndexesEmpty(), "Indexes were just set, but isIndexesEmpty returns true... boy, that's bad.");
        try {
            FileDataSourceAdapter dataSourceAdapter = new FileDataSourceAdapter();
            dataSourceAdapter.setParentDirectory(testDir + "data/import");
            dataSourceAdapter.setFileNamePattern("test.dat");
            dataSourceAdapter.setEncoding(EncodingSupport.DEFAULT_ENCODING);
            t.setSourceAdapter(dataSourceAdapter);
        } catch (VocollectException e) {
            fail("FileDataSourceAdapter constructor threw exception"
                    + e.getMessage());
        }

        try {
            t.preScan();
        } catch (PrescanException e) {
            assertTrue(true, "Expected prescan exception - file failure for duplicate data");
        } catch (VocollectException e) {
            fail("Prescan threw exception" + e.getMessage());
        }

        // parser should know that file is bad since prescan is done and failFile
        // was set to true and indexes were duped for that index.
        assertTrue(t.isAnyFailFileIndexBlown(), "The first index is failFile and"
            + "was blown, but isAnyFailFileIndexBlown() returned false.");
        
        // test getIndexes
        Indexes anIndexesObj = t.getIndexes();
        assertFalse(anIndexesObj == null, "used getIndexes to load an Indexes object, but it is still null!");
        
        t.setPreScan(false);
        //Now get the first record and do some tests
        ObjectContext objectContext = null;
        try {
            objectContext = t.getRecord();
            objectContext.getFieldMaps().get(0);
        } catch (VocollectException e) {
           e.printStackTrace();
            fail("Exception reading record during prescan tests");
        }
        
        assertTrue(t.isAnyFailRecordIndexBlown(), "duped record did not register with isAnyFailRecordIndexBlown().");
        System.out.println("The duped record data will be spit out now:");
        System.out.println(t.getRecordDataDuped());
        assertEquals("Index named: father| indicates duplicated data: 01|; ",
            t.getRecordDataDuped(), "getRecordDataDuped didn't output the right value!");
        
        //Now set prescan to false and try a prescan... should be happy.
        t.setPreScan(false);
        try {
            assertTrue(t.preScan(), "Pre scan failed");
        } catch (VocollectException e) {
            fail("Prescan threw exception" + e.getMessage());
        }
    }

    /**
     * This is a prescan test that uses index and grouping index. 
     *
     */
    @Test()
    public void testPreScanWithIndexAndGroupingIndex() {
        
        /*
         * This prescan is always on.
         */
        Indexes myIndexes = new Indexes();
        ArrayList <Field> index1List = new ArrayList<Field>();
        ArrayList <Field> index2List = new ArrayList<Field>();
        ArrayList <Field> index3List = new ArrayList<Field>();
        ArrayList <Field> index4List = new ArrayList<Field>();
        
        FixedLengthDataSourceParser t = null;
        try {
            t = new FixedLengthDataSourceParser();
            t.setConfigFileName("test.config");
        } catch (ConfigurationException e) {
            fail("Constructor threw exception" + e.getMessage());
        }
        try {
            t.setConfigFileName(configFileName);
            t.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            fail("configure threw exception" + e1.getMessage());
        }
        try {
            assertTrue(t.configure(), "Bad config");
        } catch (ConfigurationException e) {
            fail("configure threw exception" + e.getMessage());
        }

       
        //------ This section sets up the Indexes
        
        // create a field list to make indexes...
        // #1 father|, #2 firstName|lastName|
        index1List.add(t.getField("firstName"));

        index2List.add(t.getField("firstName"));
        index2List.add(t.getField("lastName"));

        index3List.add(t.getField("month"));

        //This sets up the fields for the GroupingIndex lastName|month|
        index4List.add(t.getField("lastName"));
        index4List.add(t.getField("month"));
        
        
        Index myIndex = new Index(index1List);
        assertEquals(myIndex.indexFieldsName(), "firstName|", "indexFieldsName didn't return updated value.");
        myIndex.setFailFile(true);
        Index myIndex2 = new Index(index2List);
        assertEquals(myIndex2.indexFieldsName(), "firstName|lastName|", "indexFieldsName didn't return updated value.");
        myIndex.setFailRecord(true);
        Index myIndex3 = new Index(index3List);
        assertEquals(myIndex3.indexFieldsName(), "month|", "indexFieldsName didn't return updated value.");

        //Make a grouping index
        GroupingIndex myGroupingIndex = new GroupingIndex(index4List);
        assertEquals(myGroupingIndex.indexFieldsName(), "lastName|month|",
            "myGroupingIndex indexFieldsName didn't return updated value.");
        
        
        //Add all indexes... to external Indexes object myIndexes
        assertTrue(myIndexes.addIndex(myIndex), "Failed to add index1.");
        assertTrue(myIndexes.addIndex(myIndex2), "Failed to add index2.");
        assertTrue(myIndexes.addIndex(myIndex3), "Failed to add index3.");
        assertTrue(myIndexes.addIndex(myGroupingIndex), "Failed to add myGroupingIndex.");

        
        // set indexes and ensure it is NOT empty
        t.setIndexes(myIndexes);
        assertFalse(t.isIndexesEmpty(), "Indexes were just set, but isIndexesEmpty returns true... boy, that's bad.");
        try {
            FileDataSourceAdapter dataSourceAdapter = new FileDataSourceAdapter();
            dataSourceAdapter.setParentDirectory(testDir + "data/import");
            dataSourceAdapter.setFileNamePattern("test2.dat");
            dataSourceAdapter.setEncoding(EncodingSupport.DEFAULT_ENCODING);
            
//            //If you want to test that files are kept, uncomment this
//            dataSourceAdapter.setMoveFilesOnCompletion(true);
//            dataSourceAdapter.setKeepIntermediateUTF16File(true);
//            dataSourceAdapter.setKeepSortedFile(true);
            
            t.setSourceAdapter(dataSourceAdapter);
        } catch (VocollectException e) {
            fail("FileDataSourceAdapter constructor threw exception"
                    + e.getMessage());
        }
 
        try {
            assertFalse(t.preScan(), "PreScan did not fail on data that should cause"
                + " the file to fail... ");
        } catch (PrescanException pe) {
            //Expected error, becaues the prescan above now throws this.
        } catch (VocollectException e) {
            //  expected this because failFile is true and there are dups.
            fail("Prescan threw exception" + e.getMessage());
        }
        

        // parser should know that file is bad since prescan is done and failFile
        // was set to true and indexes were duped for that index.
        assertTrue(t.isAnyFailFileIndexBlown(), "The first index is failFile and"
            + "was blown, but isAnyFailFileIndexBlown() returned false.");


        //Test to see that Grouping Index contains all data
        this.myProperOrder = null;
        assertTrue(this.myProperOrder == null, "myProperOrder was set to null, but isn't.");
        //Get properOrder from groupingIndex
        this.myProperOrder = myIndexes.getGroupingIndex().getProperOrder();
        assertFalse(this.myProperOrder == null, "myProperOrder shouldn't be null, but is.");

//Test code to spit out myProperOrder data        
//        for (int recordIndex = 0; recordIndex < myProperOrder[0].length; recordIndex++) {
//            System.out.println("myProperOrder [0][" + recordIndex + "] is:" + myProperOrder[0][recordIndex]);
//            System.out.println("myProperOrder [1][" + recordIndex + "] is:" + myProperOrder[1][recordIndex]);
//        }
        
        //Check all values in array to be grouped properly (natural order, but like-indexed items together)
        assertTrue(this.myProperOrder[0][0] == 0L, "myProperOrder [0][0] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[1][0] == 239L, "myProperOrder [1][0] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[0][1] == 478L, "myProperOrder [0][1] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[1][1] == 239L, "myProperOrder [1][1] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[0][2] == 717L, "myProperOrder [0][2] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[1][2] == 239L, "myProperOrder [1][2] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[0][3] == 1195L, "myProperOrder [0][3] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[1][3] == 239L, "myProperOrder [1][3] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[0][4] == 1673L, "myProperOrder [0][4] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[1][4] == 239L, "myProperOrder [1][4] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[0][5] == 2151L, "myProperOrder [0][5] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[1][5] == 239L, "myProperOrder [1][5] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[0][6] == 239L, "myProperOrder [0][6] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[1][6] == 239L, "myProperOrder [1][6] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[0][7] == 956L, "myProperOrder [0][7] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[1][7] == 239L, "myProperOrder [1][7] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[0][8] == 1434L, "myProperOrder [0][8] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[1][8] == 239L, "myProperOrder [1][8] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[0][9] == 1912L, "myProperOrder [0][9] value"
            + " was wrong.");
        assertTrue(this.myProperOrder[1][9] == 239L, "myProperOrder [1][9] value"
            + " was wrong.");
        
        // test getIndexes
        Indexes anIndexesObj = t.getIndexes();
        assertFalse(anIndexesObj == null, "used getIndexes to load an Indexes object, but it is still null!");
        
        t.setPreScan(false);
        
        //Now get the first record and do some tests
        ObjectContext objectContext = null;
        try {
            objectContext = t.getRecord();
            objectContext.getFieldMaps().get(0);
        } catch (VocollectException e) {
            e.printStackTrace();
            fail("Exception reading record during prescan tests");
        }
        
        
        assertTrue(t.isAnyFailRecordIndexBlown(), "duped record did not register with isAnyFailRecordIndexBlown().");
        //System.out.println("The duped record data will be spit out now:");
        //System.out.println(t.getRecordDataDuped());
        assertEquals("Index named: firstName| indicates duplicated data: duff      |; ",
            t.getRecordDataDuped(), "getRecordDataDuped didn't output the right value!");
        
        //Now set prescan to false and try a prescan... should be happy.
        t.setPreScan(false);
        try {
            assertTrue(t.preScan(), "Pre scan failed");
        } catch (VocollectException e) {
            fail("Prescan threw exception" + e.getMessage());
        }

        //TODO:  Test that files are moved or not moved... 
        // I did this manually (uncomment below and way above) and it worked
        
        //Move the temp files to success.
        //t.getSourceAdapter().closeOnSuccess();
        
    }

    
    
    /**
     * Test method for 'com.vocollect.voicelink.core.importer.DataSourceParser.clearIndexes()'.
     * ALSO Test method for 'com.vocollect.voicelink.core.importer.DataSourceParser.addIndex()'.
     * 
     * IMPORTANT NOTE:  A few of these tests ( testIndexesClear() and testPrescan())
     *  are also testing the DataSourceParser's Indexes features.  If these tests
     *  are modified, they must maintain the existing tests, or at least move those
     *  into the DataSourceparserTest.java file.
     */
    @Test()
    public void testIndexesClear() {
        /*
         * This prescan is always on.
         */

        ArrayList <Field> index1List = new ArrayList<Field>();
        ArrayList <Field> index2List = new ArrayList<Field>();
        ArrayList <Field> index3List = new ArrayList<Field>();
        FixedLengthDataSourceParser t = null;
        try {
            t = new FixedLengthDataSourceParser();
            t.setConfigFileName("test.config");
        } catch (ConfigurationException e) {
            fail("Constructor threw exception" + e.getMessage());
        }
        try {
            t.setConfigFileName(configFileName);
            t.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            fail("configure threw exception" + e1.getMessage());
        }
        try {
            assertTrue(t.configure(), "Bad config");
        } catch (ConfigurationException e) {
            fail("configure threw exception" + e.getMessage());
        }

        //------ This section sets up the Indexes
        // create a field list to make 3 indexes...
        // #1 father|, #2 firstName|lastName|
        index1List.add(t.getField("father"));

        index2List.add(t.getField("firstName"));
        index2List.add(t.getField("lastName"));

        index3List.add(t.getField("month"));


        Index myIndex = new Index(index1List);
        assertEquals(myIndex.indexFieldsName(), "father|", "indexFieldsName didn't return updated value.");
        myIndex.setFailFile(true);
        Index myIndex2 = new Index(index2List);
        assertEquals(myIndex2.indexFieldsName(), "firstName|lastName|", "indexFieldsName didn't return updated value.");
        myIndex.setFailRecord(true);
        Index myIndex3 = new Index(index3List);
        assertEquals(myIndex3.indexFieldsName(), "month|", "indexFieldsName didn't return updated value.");
        
        
        //Use the AddIndex feature to individually add indexes to the Parser
        assertTrue(t.addIndex(myIndex), "Failed to add index1 to the parser.");
        assertTrue(t.addIndex(myIndex2), "Failed to add index2 to the parser..");
        assertTrue(t.addIndex(myIndex3), "Failed to add index3 to the parser..");

        // test if indexes is NOT empty
        assertFalse(t.isIndexesEmpty(), "Indexes were just set, but isIndexesEmpty returns true... boy, that's bad.");

        
        // test clear indexes
        t.clearIndexes();
        assertTrue(t.isIndexesEmpty(), "Indexes were just cleared, but isIndexesEmpty returns false...");
        
    }
    
    
    
    
    
    /**
     * Test method for 'com.vocollect.voicelink.core.importer.FixedLengthDataSourceParser.getRecordLength()'.
     */
    @Test()
    public void testGetRecordLength() {
        FixedLengthDataSourceParser t = null;
        try {
            t = new FixedLengthDataSourceParser();
        } catch (ConfigurationException e) {
            fail("Constructor threw exception" + e.getMessage());
        }
        try {
            t.setConfigFileName(configFileName);
            t.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            fail("Constructor threw exception" + e1.getMessage());
        }
        try {
            t.setSourceAdapter(new FileDataSourceAdapter());
        } catch (VocollectException e) {
            fail("FileDataSourceAdapter constructor threw exception"
                    + e.getMessage());
        }
        t.getSourceAdapter().setCurrentRecordLength(t.getRecordLength());
        org.testng.AssertJUnit.assertTrue("got record length", t
                .getRecordLength() == t.getSourceAdapter()
                .getCurrentRecordLength());

        // now remove one item from the record length and test it
        t.setRecordLength(t.getRecordLength() - 1);
        org.testng.AssertJUnit.assertTrue("got record length - 1", t
                .getRecordLength() == t.getSourceAdapter()
                .getCurrentRecordLength() - 1);

    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.FixedLengthDataSourceParser.setRecordLength(int)'.
     */
    @Test()
    public void testSetRecordLength() {
        FixedLengthDataSourceParser t = null;
        try {
            t = new FixedLengthDataSourceParser();
        } catch (ConfigurationException e) {
            fail("Constructor threw exception" + e.getMessage());
        }
        try {
            t.setConfigFileName(configFileName);
            t.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            fail("Constructor threw exception" + e1.getMessage());
        }
        try {
            t.setSourceAdapter(new FileDataSourceAdapter());
        } catch (VocollectException e) {
            fail("FileDataSourceAdapter constructor threw exception"
                    + e.getMessage());
        }
        int newreclength = 2;
        t.setRecordLength(newreclength);
        org.testng.AssertJUnit.assertTrue("set record length", t
                .getRecordLength() == 2);

    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.FixedLengthDataSourceParser.isUnordered()'.
     */
    @Test()
    public void testIsUnordered() {

    }

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.FixedLengthDataSourceParser.setUnordered(boolean)'.
     */
    @Test()
    public void testSetUnordered() {

    }

    /**
     * Real all the records in the test data file. 
     */
    @Test()
    public void testReadAll() {
        DataSourceParser<FixedLengthFieldMap, FixedLengthField> t = null;
        try {
            t = new FixedLengthDataSourceParser();
        } catch (ConfigurationException e) {
            fail("Constructor threw exception" + e.getMessage());
        }
        try {
            t.setConfigFileName(configFileName);
            t.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            fail("configure threw exception" + e1.getMessage());
        }

        try {
            FileDataSourceAdapter dataSourceAdapter = new FileDataSourceAdapter();
            dataSourceAdapter.setParentDirectory(testDir + "data/import");
            dataSourceAdapter.setFileNamePattern("test.dat");
            dataSourceAdapter.setEncoding(EncodingSupport.DEFAULT_ENCODING);
            t.setSourceAdapter(dataSourceAdapter);
        } catch (VocollectException e) {
            fail("FileDataSourceAdapter constructor threw exception"
                    + e.getMessage());
        }

// The commented out lines can be used as a rudimentary performance test.
        //        Calendar start = Calendar.getInstance();
        int count = 0;
        // we aren't doing a prescan here so turn this off.
        t.setPreScan(false);
        do {
            try {
                t.getRecord();
            } catch (VocollectException e) {
                org.testng.AssertJUnit.assertFalse("Exception reading record",
                        true);
            }
            count++;
        } while (!t.isOutOfData());
//        Calendar end = Calendar.getInstance();
//        System.err.println(count);
//        System.err.println(end.getTimeInMillis() - start.getTimeInMillis());
    }
    
    /**
     * Test to verify that we can read a file with exactly one line of a different length at the end of the file.
     */
    @Test()
    public void testReadAllWithExtraLine() {
        DataSourceParser<FixedLengthFieldMap, FixedLengthField> t = null;
        try {
            t = new FixedLengthDataSourceParser();
        } catch (ConfigurationException e) {
            fail("Constructor threw exception" + e.getMessage());
        }
        try {
            t.setConfigFileName(configFileName);
            t.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            fail("configure threw exception" + e1.getMessage());
        }

        try {
            FileDataSourceAdapter dataSourceAdapter = new FileDataSourceAdapter();
            dataSourceAdapter.setParentDirectory(testDir + "data/import");
            dataSourceAdapter.setFileNamePattern("test-extra.dat");
            dataSourceAdapter.setEncoding(EncodingSupport.DEFAULT_ENCODING);
            t.setSourceAdapter(dataSourceAdapter);
        } catch (VocollectException e) {
            fail("FileDataSourceAdapter constructor threw exception"
                    + e.getMessage());
        }
        t.setLastLineDiffers(true);
        t.setPreScan(true);

// The commented out lines can be used as a rudimentary performance test.
        //        Calendar start = Calendar.getInstance();
        int count = 0;
        // we aren't doing a prescan here so turn this off.
        t.setPreScan(false);
        do {
            try {
                t.getRecord();
            } catch (OutOfDataException e) {
                // expected
            } catch (VocollectException e) {
                org.testng.AssertJUnit.assertFalse("Exception reading record",
                        true);
            }
            count++;
        } while (!t.isOutOfData());
//        Calendar end = Calendar.getInstance();
//        System.err.println(count);
//        System.err.println(end.getTimeInMillis() - start.getTimeInMillis());
    }

    /**
     * Test to verify that we can read a file with exactly one line of a different length at the end of the file.
     */
    @Test()
    public void testReadAllWithExtraEmptyLine() {
        DataSourceParser<FixedLengthFieldMap, FixedLengthField> t = null;
        try {
            t = new FixedLengthDataSourceParser();
        } catch (ConfigurationException e) {
            fail("Constructor threw exception" + e.getMessage());
        }
        try {
            t.setConfigFileName(configFileName);
            t.setMappingFileName(mappingFileName);
        } catch (ConfigurationException e1) {
            fail("configure threw exception" + e1.getMessage());
        }

        try {
            FileDataSourceAdapter dataSourceAdapter = new FileDataSourceAdapter();
            dataSourceAdapter.setParentDirectory(testDir + "data/import");
            dataSourceAdapter.setFileNamePattern("test-extra.dat");
            dataSourceAdapter.setEncoding(EncodingSupport.DEFAULT_ENCODING);
            t.setSourceAdapter(dataSourceAdapter);
        } catch (VocollectException e) {
            fail("FileDataSourceAdapter constructor threw exception"
                    + e.getMessage());
        }
        t.setLastLineDiffers(true);
        t.setLastLineLength(0L);

// The commented out lines can be used as a rudimentary performance test.
        //        Calendar start = Calendar.getInstance();
        int count = 0;
        // we aren't doing a prescan here so turn this off.
        t.setPreScan(false);
        do {
            try {
                t.getRecord();
            } catch (OutOfDataException e) {
                // expected
            } catch (VocollectException e) {
                org.testng.AssertJUnit.assertFalse("Exception reading record",
                        true);
            }
            count++;
        } while (!t.isOutOfData());
//        Calendar end = Calendar.getInstance();
//        System.err.println(count);
//        System.err.println(end.getTimeInMillis() - start.getTimeInMillis());
    }

}
