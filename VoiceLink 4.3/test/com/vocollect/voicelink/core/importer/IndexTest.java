/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;



/**
 * Test for Index class.
 *
 * @author jtauberg
 */
public class IndexTest {
    private FixedLengthFieldMap fields = new FixedLengthFieldMap();

    private FixedLengthField aField1 = new FixedLengthField();
    private FixedLengthField aField2 = new FixedLengthField();
    private FixedLengthField aField3 = new FixedLengthField();
    private FixedLengthField aField4 = new FixedLengthField();
    private FixedLengthField aField5 = new FixedLengthField();

    private ArrayList <Field> index1List = new ArrayList<Field>();
    private ArrayList <Field> index2List = new ArrayList<Field>();
    private ArrayList <Field> index3List = new ArrayList<Field>();
        

    /**
     *  Simulates reading a record by loading fields with data for Glen Adams.
     */
    private void loadRecord1() {
        //Name
        aField1.setFieldData("Glen Adams");
        // isCool (boolean)
        aField2.setFieldData("1");
        //Age (Integer)
        aField3.setFieldData("39");
        //Weight (double)
        aField4.setFieldData("205.0");
        //FavoriteColor
        aField5.setFieldData("Green");
    }

    /**
     *  Simulates reading a record by loading fields with data for Karen Jones.
     */
    private void loadRecord2() {
        //Name
        aField1.setFieldData("Karen Jones");
        // isCool (boolean)
        aField2.setFieldData("1");
        //Age (Integer)
        aField3.setFieldData("24");
        //Weight (double)
        aField4.setFieldData("115.5");
        //FavoriteColor
        aField5.setFieldData("Blue");
    }

    /**
     *  Simulates reading a record by loading fields with data for Tom Kirk.
     */
    private void loadRecord3() {
        //Name
        aField1.setFieldData("Tom Kirk");
        // isCool (boolean)
        aField2.setFieldData("0");
        //Age (Integer)
        aField3.setFieldData("33");
        //Weight (double)
        aField4.setFieldData("190.0");
        //FavoriteColor
        aField5.setFieldData("Red");
    }

    
    
    /**
     * {@inheritDoc}
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass
    protected void testSetUp() throws Exception {
        // sets 5 fields.
        aField1.setDataType("java.lang.String");
        aField1.setFieldName("Name");
        
        aField2.setDataType("boolean");
        aField2.setFieldName("isCool");
        
        aField3.setDataType("java.lang.Integer");
        aField3.setFieldName("Age");
        
        aField4.setDataType("double");
        aField4.setFieldName("Weight");
        
        aField5.setDataType("java.lang.String");
        aField5.setFieldName("FavoriteColor");


        // creates a fieldmap with the 5 fields
        fields.addField(aField1);
        fields.addField(aField2);
        fields.addField(aField3);
        fields.addField(aField4);
        fields.addField(aField5);

        // also creates a field list to make 3 indexes...
        // #1 name, #2 Age|Weight, #3 Name|FavoriteColor
        index1List.add(fields.get("Name"));

        index2List.add(fields.get("Age"));
        index2List.add(fields.get("Weight"));

        index3List.add(fields.get("Name"));
        index3List.add(fields.get("FavoriteColor"));
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Index#Index()}.
     */
    @Test()
    public void testDefaultIndexConstructor() {
        Index myIndex = null;
        assertNull(myIndex, "An index was created and set to null, but isn't null.");
        myIndex = new Index();
        org.testng.Assert.assertNotNull(myIndex, "The index constructor ran but the index is still null.");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Index#Index(java.util.Collection)}.
     */
    @Test()
    public void testMyIndexConstructor() {
        Index myIndex = null;
        assertNull(myIndex, "An index was created and set to null, but isn't null.");
        myIndex = new Index(index1List);
        assertEquals(myIndex.indexFieldsName(), "Name|", "indexFieldsName didn't return updated value.");
        Index myIndex2 = null;
        assertNull(myIndex2, "An index was created and set to null, but isn't null.");
        myIndex2 = new Index(index2List);
        assertEquals(myIndex2.indexFieldsName(), "Age|Weight|", "indexFieldsName didn't return updated value.");
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Index#indexFieldsName()}.
     */
    @Test()
    public void testIndexFieldsName() {
        Index myIndex = new Index(index1List);
        assertEquals(myIndex.indexFieldsName(), "Name|", "indexFieldsName didn't return updated value.");
        Index myIndex2 = new Index(index2List);
        assertEquals(myIndex2.indexFieldsName(), "Age|Weight|", "indexFieldsName didn't return updated value.");
    }
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Index#setFailFile()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.Index#isFailFile()}.
    */
   @Test()
   public void testSetFailFile() {
       Index myIndex = new Index(index1List);
       assertEquals(myIndex.indexFieldsName(), "Name|", "indexFieldsName didn't return updated value.");
       assertFalse(myIndex.isFailFile(), "isFailFile() did not return proper default value.");
       myIndex.setFailFile(true);
       assertTrue(myIndex.isFailFile(), "isFailFile() did not return proper value.");
   }

   
   /**
    * Test method for {@link com.vocollect.voicelink.core.importer.Index#setFailRecord()}.
    * Test method for {@link com.vocollect.voicelink.core.importer.Index#isFailRecord()}.
   */
  @Test()
  public void testSetFailRecord() {
      Index myIndex = new Index(index1List);
      assertEquals(myIndex.indexFieldsName(), "Name|", "indexFieldsName didn't return updated value.");
      assertFalse(myIndex.isFailRecord(), "isFailRecord() did not return proper default value.");
      myIndex.setFailRecord(true);
      assertTrue(myIndex.isFailRecord(), "isFailRecord() did not return proper value.");
  }
  
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Index#updateIndex()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Index#getCount()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Index#isDataDuped()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Index#isIndexBlown()}.
      * Also Test method for {@link com.vocollect.voicelink.core.importer.Index#isDataDupedIgnoreFlags()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Index#isIndexBlownIgnoreFlags()}.
    * Also Test method for {@link com.vocollect.voicelink.core.importer.Index#getDupItems()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Index#getDupSets()}.
     * Also Test method for {@link com.vocollect.voicelink.core.importer.Index#currentFieldsIndexValue()}.
   */
    @Test()
    public void testUpdateIndex() {
        Index myIndex = new Index(index1List);
        Index myIndex2 = new Index(index2List);
        assertEquals(myIndex.currentFieldsIndexValue(), "|", "currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndex2.currentFieldsIndexValue(), "||", "2.currentFieldsIndexValue returned wrong result!");

        loadRecord1();
        assertNull(myIndex.getCount(), "Data not yet in index, count should be null but wasn't.");
        assertNull(myIndex2.getCount(), "Data not yet in index2, count should be null but wasn't.");
        myIndex.updateIndex();
        myIndex2.updateIndex();
        assertTrue(myIndex.getCount() == 1, "Data in index, count should be 1 but wasn't.");
        assertTrue(myIndex2.getCount() == 1, "Data in index2, count should be 1 but wasn't.");
        assertEquals(myIndex.currentFieldsIndexValue(), 
            "Glen Adams|", "currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndex2.currentFieldsIndexValue(), 
            "39|205.0|", "2.currentFieldsIndexValue returned wrong result!");
        
        
        loadRecord2();
        assertNull(myIndex.getCount(), "Data not yet in index, count should be null but wasn't.");
        assertNull(myIndex2.getCount(), "Data not yet in index2, count should be null but wasn't.");
        myIndex.updateIndex();
        myIndex2.updateIndex();
        
        assertTrue(myIndex.getCount() == 1, "Data in index, count should be 1 but wasn't.");
        assertTrue(myIndex2.getCount() == 1, "Data in index2, count should be 1 but wasn't.");
        assertEquals(myIndex.currentFieldsIndexValue(), 
            "Karen Jones|", "currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndex2.currentFieldsIndexValue(), 
            "24|115.5|", "2.currentFieldsIndexValue returned wrong result!");
        loadRecord3();
        assertNull(myIndex.getCount(), "Data not yet in index, count should be null but wasn't.");
        assertNull(myIndex2.getCount(), "Data not yet in index2, count should be null but wasn't.");
        myIndex.updateIndex();
        myIndex2.updateIndex();
        assertTrue(myIndex.getCount() == 1, "Data in index, count should be 1 but wasn't.");
        assertTrue(myIndex2.getCount() == 1, "Data in index2, count should be 1 but wasn't.");
        assertEquals(myIndex.currentFieldsIndexValue(), "Tom Kirk|", "currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndex2.currentFieldsIndexValue(), 
            "33|190.0|", "2.currentFieldsIndexValue returned wrong result!");

        //Test value of isDataDuped
        assertFalse(myIndex.isDataDuped(), "Data wasn't duped but isDataDuped thinks it was.");
        assertFalse(myIndex2.isDataDuped(), "Data wasn't duped but 2.isDataDuped thinks it was.");

        //Test value of isIndexBlown
        assertFalse(myIndex.isIndexBlown(), "Data wasn't duped but isIndexBlown thinks it was.");
        assertFalse(myIndex2.isIndexBlown(), "Data wasn't duped but 2.isIndexBlown thinks it was.");

        //Test value of getDupItems
        assertEquals((long) myIndex.getDupItems(), 0L, "getDupItems returned wrong result!");
        assertEquals((long) myIndex2.getDupItems(), 0L, "2.getDupItems returned wrong result!");

        //Test value of getDupSets
        assertEquals((long) myIndex.getDupSets(), 0L, "getDupSets returned wrong result!");
        assertEquals((long) myIndex2.getDupSets(), 0L, "2.getDupSets returned wrong result!");

        
        // RE-LOAD AND INDEX RECORD2 (should be 1 dupSet and 2 dupItems)
        assertTrue(myIndex.getCount() == 1, "Data in index, count should be 1 but wasn't.");
        assertTrue(myIndex2.getCount() == 1, "Data in index2, count should be 1 but wasn't.");
        loadRecord2();
        myIndex.updateIndex();
        myIndex2.updateIndex();
        assertEquals((int) myIndex.getCount(), 2, "Data in index, count should be 2 but wasn't.");
        assertEquals((int) myIndex2.getCount(), 2, "Data in index2, count should be 2 but wasn't.");
        assertEquals(myIndex.currentFieldsIndexValue(), 
            "Karen Jones|", "currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndex2.currentFieldsIndexValue(), 
            "24|115.5|", "2.currentFieldsIndexValue returned wrong result!");

        
        //Re-Test value of isDataDuped
        assertFalse(myIndex.isDataDuped(), "flag says not to report duped data, but 1.isDataDuped did.");
        assertFalse(myIndex2.isDataDuped(), "flag says not to report duped data, but 2.isDataDuped did.");

        //Re-Test value of isDataDupedIgnoreFlags
        assertTrue(myIndex.isDataDupedIgnoreFlags(), "Data was duped but isDataDuped thinks it wasn't.");
        assertTrue(myIndex2.isDataDupedIgnoreFlags(), "Data was duped but 2.isDataDuped thinks it wasn't.");

        //Re-Test value of isIndexBlown
        assertFalse(myIndex.isIndexBlown(), "flag says not to report duped data, but isIndexBlown did.");
        assertFalse(myIndex2.isIndexBlown(), "flag says not to report duped data, but 2.isIndexBlown did.");

        //Re-Test value of isIndexBlownIgnoreFlags
        assertTrue(myIndex.isIndexBlownIgnoreFlags(), "Data was duped but isIndexBlownIgnoreFlags thinks it wasn't.");
        assertTrue(myIndex2.isIndexBlownIgnoreFlags(), 
            "Data was duped but 2.isIndexBlownIgnoreFlags thinks it wasn't.");

        
         //Test value of getDupItems
        assertEquals((long) myIndex.getDupItems(), 2L, "getDupItems returned wrong result!");
        assertEquals((long) myIndex2.getDupItems(), 2L, "2.getDupItems returned wrong result!");

        //Test value of getDupSets
        assertEquals((long) myIndex.getDupSets(), 1L, "getDupSets returned wrong result!");
        assertEquals((long) myIndex2.getDupSets(), 1L, "2.getDupSets returned wrong result!");

        
        // RE-LOAD AND INDEX RECORD2 for the 3rd time...  (should be 1 dup set 3 dup items)
        loadRecord2();
        myIndex.updateIndex();
        myIndex2.updateIndex();
        assertEquals((int) myIndex.getCount(), 3, "Data in index, count should be 3 but wasn't.");
        assertEquals((int) myIndex2.getCount(), 3, "Data in index2, count should be 3 but wasn't.");
        assertEquals(myIndex.currentFieldsIndexValue(), 
            "Karen Jones|", "currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndex2.currentFieldsIndexValue(), 
            "24|115.5|", "2.currentFieldsIndexValue returned wrong result!");

        //Test value of getDupItems
        assertEquals((long) myIndex.getDupItems(), 3L, "getDupItems returned wrong result!");
        assertEquals((long) myIndex2.getDupItems(), 3L, "2.getDupItems returned wrong result!");
        
        //Test value of getDupSets
        assertEquals((long) myIndex.getDupSets(), 1L, "getDupSets returned wrong result!");
        assertEquals((long) myIndex2.getDupSets(), 1L, "2.getDupSets returned wrong result!");

        
        // RE-LOAD AND INDEX RECORD1 for the 2nd time...  (should be 2 dupSet 5 dupItems)
        loadRecord1();
        myIndex.updateIndex();
        myIndex2.updateIndex();
        assertEquals((int) myIndex.getCount(), 2, "Data in index, count should be 2 but wasn't.");
        assertEquals((int) myIndex2.getCount(), 2, "Data in index2, count should be 2 but wasn't.");
        assertEquals(myIndex.currentFieldsIndexValue(), 
            "Glen Adams|", "currentFieldsIndexValue returned wrong result!");
        assertEquals(myIndex2.currentFieldsIndexValue(), 
            "39|205.0|", "2.currentFieldsIndexValue returned wrong result!");

        //Test value of getDupItems
        assertEquals((long) myIndex.getDupItems(), 5L, "getDupItems returned wrong result!");
        assertEquals((long) myIndex2.getDupItems(), 5L, "2.getDupItems returned wrong result!");
        
        //Test value of getDupSets
        assertEquals((long) myIndex.getDupSets(), 2L, "getDupSets returned wrong result!");
        assertEquals((long) myIndex2.getDupSets(), 2L, "2.getDupSets returned wrong result!");
        
        
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.Index#getCompoundIndexSeparator()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.Index#setCompoundIndexSeparator(java.lang.String)}.
     */
    @Test()
    public void testGetCompoundIndexSeparator() {
        Index myIndex = new Index(index1List);
        assertEquals(myIndex.indexFieldsName(), "Name|", "indexFieldsName didn't return updated value.");
        assertEquals(myIndex.getCompoundIndexSeparator(), 
            "|", "The getCompoundIndexSeparator test did not return the proper default.");
        myIndex.setCompoundIndexSeparator("$BOB$");
        assertEquals(myIndex.getCompoundIndexSeparator(), 
            "$BOB$", "The getCompoundIndexSeparator did not return what was set!");
    }



}
