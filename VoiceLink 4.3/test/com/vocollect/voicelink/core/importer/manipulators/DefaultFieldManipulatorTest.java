/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * @author KalpnaT
 */
public class DefaultFieldManipulatorTest {


    /**
     * Inner class to setup test data.
     */
    private class DefaultFieldManipulatorTestData {
        
        private String day          = null;
        
        private String defaultFieldName = "defaultField";
        
        private String defaultField        = null;

        private String fieldh       = null;

        /**
         * @return the day
         */
        public String getDay() {
            return day;
        }

        /**
         * @param day
         *            the day to set
         */
        public void setDay(String day) {
            this.day = day;
        }

        /**
         * @return the field_h
         */
        public String getFieldh() {
            return fieldh;
        }

        /**
         * @param fieldh
         * the field_h to set
         */
        public void setFieldh(String fieldh) {
            this.fieldh = fieldh;
        }

        /**
         * @param defaultFieldName to set
         */
        public void setDefaultFieldName(String defaultFieldName) {
            this.defaultFieldName = defaultFieldName;
        }

        /**
         * @return the defaultFieldName
         */
        public String getDefaultFieldName() {
            return this.defaultFieldName;
        }
        
        /**
         * @param defaultField to set
         */
        public void setDefaultField(String defaultField) {
            this.defaultField = defaultField;
        }

        /**
         * @return the defaultField
         */
        public String getDefaultField() {
            return this.defaultField;
        }

    }

    private ValidationAwareSupport resultHolder = new ValidationAwareSupport();

    // This is the thing that is used by the DelegatingValidatorContext to keep
    // the result of the validation

    private ValidatorContext       vContext     = new DelegatingValidatorContext(
                                                        resultHolder);

    // This is the thing that is used to do the validation

    private String                 fieldValue   = "";
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.manipulator.DefaultFieldManipulator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidate() {
        // create the test object with the necessary data
        DefaultFieldManipulatorTestData testData = new DefaultFieldManipulatorTestData();
        testData.setDefaultField("123");
        testData.setDay("1234");
        testData.setFieldh(null);

        DefaultFieldManipulator defaultFieldManipulator = new DefaultFieldManipulator();
        try {
            defaultFieldManipulator.setValidatorContext(vContext);
            defaultFieldManipulator.setDefaultFieldName(testData.getDefaultFieldName());
            String thefield = "fieldh";
            defaultFieldManipulator.setFieldName(thefield);
            defaultFieldManipulator.validate(testData);
            this.fieldValue = "123";
            assertEquals(thefield
                    + " field should have been replaced by the default value.",
                    testData.getDefaultField(), this.fieldValue);
            testData.setFieldh("45.30");
            this.fieldValue = "45.30";
            defaultFieldManipulator.validate(testData);
            assertFalse(thefield
                            + " field should NOT have been replaced by the default value.",
                            testData.getDefaultField().equals(this.fieldValue));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        try {
            //Invalid default field name should throw an error
            testData.setFieldh(null);
            testData.setDefaultFieldName("XYZ");
            defaultFieldManipulator.setDefaultFieldName(testData.getDefaultFieldName());
            defaultFieldManipulator.validate(testData);
            fail("This should throw an exception as " + testData.getDefaultFieldName() 
                 + " is not a valid field");                        
        } catch (RuntimeException e) {            
            assertTrue(testData.getFieldh() == null);
            assertTrue(testData.getDefaultFieldName().equals("XYZ"));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
        
        try {
            //Null default field name should throw an error
            testData.setFieldh(null);
            testData.setDefaultFieldName(null);
            defaultFieldManipulator.setDefaultFieldName(testData.getDefaultFieldName());
            defaultFieldManipulator.validate(testData);
            fail("This should throw an exception as " + testData.getDefaultFieldName() 
                 + " is not a valid field");                        
        } catch (RuntimeException e) {            
            assertTrue(testData.getFieldh() == null);
        } catch (ValidationException e) {
            e.printStackTrace();
        }


    }
}
