/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * @author astein
 */
public class TrimManipulatorTest {

    /**
     * 
     * @author astein
     *
     */
    private class TrimManipulatorTestData {
        private char   defaultTrimChar = ' ';

        private String fieldA         = null;

        /**
         * 
         * @return FieldA
         */
        public String getFieldA() {
            return this.fieldA;
        }

        /**
         * 
         * @param fieldA to set
         */
        public void setFieldA(String fieldA) {
            this.fieldA = fieldA;
        }

        /**
         * 
         * @param defaultTrimChar to set
         */
        public void setDefaultTrimChar(char defaultTrimChar) {
            this.defaultTrimChar = defaultTrimChar;
        }

        /**
         * 
         * @return char
         */
        public char getDefaultTrimChar() {
            return this.defaultTrimChar;
        }
    }

    private ValidationAwareSupport resultHolder = new ValidationAwareSupport();

    // This is the thing that is used by the DelegatingValidatorContext to keep
    // the result of the validation

    private ValidatorContext       vContext     = new DelegatingValidatorContext(
                                                        resultHolder);

    // This is the thing that is used to do the validation

    private String                 fieldValue   = "";

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.NumericValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidate() {
        // create the test object with the necessary data
        TrimManipulatorTestData testData = new TrimManipulatorTestData();
        testData.setDefaultTrimChar(' ');
        testData.setFieldA("     1234     ");

        TrimManipulator trimManipulator = new TrimManipulator();

        try {
            trimManipulator.setValidatorContext(vContext);
            String thefield = "fieldA";
            trimManipulator.setFieldName(thefield);
            trimManipulator.validate(testData);
            this.fieldValue = trimManipulator.getValue().toString();
            assertEquals(thefield + " field should have been trimmed.", "1234",
                    this.fieldValue);
            testData.setFieldA("4     ");
            trimManipulator.validate(testData);
            this.fieldValue = trimManipulator.getValue().toString();
            assertEquals(thefield + " field should have been trimmed.", "4",
                    this.fieldValue);
            testData.setFieldA("   3");
            trimManipulator.validate(testData);
            this.fieldValue = trimManipulator.getValue().toString();
            assertEquals(thefield + " field should have been trimmed.", "3",
                    this.fieldValue);
            testData.setFieldA("45.30");
            trimManipulator.validate(testData);
            this.fieldValue = trimManipulator.getValue().toString();
            assertEquals(thefield + " field should NOT have been trimmed.",
                    testData.getFieldA(), this.fieldValue);
            testData.setFieldA("------45-30-----");
            testData.setDefaultTrimChar('-');
            trimManipulator.setDefaultTrimChar(testData.getDefaultTrimChar());
            trimManipulator.validate(testData);
            this.fieldValue = trimManipulator.getValue().toString();
            assertEquals(thefield + " field should have been trimmed.",
                    "45-30", this.fieldValue);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }
}
