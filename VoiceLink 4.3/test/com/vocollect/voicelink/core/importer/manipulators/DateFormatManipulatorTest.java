/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.voicelink.core.importer.FixedLengthField;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.ValidationTestObject;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.ValidationException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

/**
 * This is a test class for the DateFormatManipulator.
 *
 */
public class DateFormatManipulatorTest {

    /**
     * {@inheritDoc}
     *
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

    private DateFormatManipulator testManipulator = new DateFormatManipulator();

    private ValidationTestObject valObj = new ValidationTestObject();

    private ValidationAwareSupport resultHolder = new ValidationAwareSupport();

    private ValidationResult vR = new ValidationResult();

    private ValidationResult.VocollectValidatorContext vContext =
     vR.new VocollectValidatorContext(resultHolder);

    private FixedLengthField notNullField = new FixedLengthField();

    private FixedLengthFieldMap fields = new FixedLengthFieldMap();

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer
     * .manipulators.DateFormatManipulator#validate(java.lang.Object)}.
     */
    @Test(enabled = true)
    @SuppressWarnings("unchecked")
    public void testValidateObject() {
        testManipulator.setFieldName("dateData");
        notNullField.setFieldName("dateData");
        fields.addField(notNullField);
        ObjectContext oc = new ObjectContext(fields, new String("No real object here"));
        vContext.setFields(oc);
        testManipulator.setValidatorContext(vContext);

        // Testing Null String
        try {
            testManipulator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on null date string.");
        }
        assertFalse("Null data failed to trigger errors.", testManipulator
            .getValidatorContext().getFieldErrors().containsKey("dateData"));

        // Testing Empty String
        vContext.setFieldErrors(null);
        testManipulator.setDateFormat("yyyyMMdd");
        valObj.setDateStringData("");
        notNullField.setFieldData(valObj.getDateStringData());
        try {
            testManipulator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on empty date string.");
        }
        assertFalse(
            "Empty date string did not trigger errors.", testManipulator
                .getValidatorContext().getFieldErrors()
                .containsKey("dateData"));

        // Testing Valid Date Format and Valid String Date
        vContext.setFieldErrors(null);
        valObj.setDateStringData("20061129");
        notNullField.setFieldData(valObj.getDateStringData());
        try {
            testManipulator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse("Valid data triggered errors.", testManipulator
            .getValidatorContext().getFieldErrors().containsKey("dateData"));

        // Testing Different Valid String Date and Date Format
        vContext.setFieldErrors(null);
        valObj.setDateStringData("11/29/2006");
        notNullField.setFieldData(valObj.getDateStringData());
        try {
            testManipulator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on differnt"
                   + " valid data with valid format.");
        }
        assertTrue(
            "Valid date with invalid format did not trigger errors.",
            testManipulator.getValidatorContext().getFieldErrors().containsKey(
                "dateData"));

        // Testing Invalid String Date and Valid Date Format
        vContext.setFieldErrors(null);
        valObj.setDateStringData("20061192");
        notNullField.setFieldData(valObj.getDateStringData());
        try {
            testManipulator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on invalid date with proper format.");
        }
        assertTrue(
            "Invalid date with proper format did not trigger errors.",
            testManipulator.getValidatorContext().getFieldErrors().containsKey(
                "dateData"));
    }

    /**
     * @throws ValidationException
     */
    @Test(enabled = true)
    @SuppressWarnings("unchecked")
    public void testDiffValidFormat() throws ValidationException {
        boolean expectedException = false;
        // Testing Valid String Date and Invalid Date Format
        testManipulator.setValidatorContext(vContext);
        testManipulator.setFieldName("dateData");
        notNullField.setFieldName("dateData");
        fields.addField(notNullField);
        ObjectContext oc = new ObjectContext(fields, new String("No real object here"));
        vContext.setFields(oc);

        try {
            testManipulator.setDateFormat("XXXXYYZZ");
            valObj.setDateStringData("20061192");
            notNullField.setFieldData(valObj.getDateStringData());
            testManipulator.validate(valObj);
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue(
            "Invalid date with proper format did not trigger errors.",
            expectedException);
    }
}
