/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;
import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * Test class for replace manipulator.
 * @author vsubramani
 *
 */
public class ReplaceManipulatorTest {

    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass()
    protected void setUp() throws Exception {
    }

//    private ReplaceManipulator testObj = new ReplaceManipulator();
//
//    private ValidationTestObject valObj = new ValidationTestObject();
//
//    private ValidationAwareSupport resultHolder = new ValidationAwareSupport();
//
//    private ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.manipulators.DateFormatManipulator#validate(java.lang.Object)}.
     */
    @Test
    public void testValidateObject() {
        ReplaceManipulator testObj = new ReplaceManipulator();
        ValidationTestObject valObj = new ValidationTestObject();
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);

        
        testObj.setFieldName("stringData");
        testObj.setValidatorContext(vContext);

        // --------------- Replace string with string --------------
        testObj.setTarget("abc");
        testObj.setReplacement("xyz");
        valObj.setStringData("abcdef");
        try {
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));
        assertEquals(
                "Set a date format string and did not get back same string.",
                0, testObj.getNewValue().compareTo("xyzdef"));

        // --------------- Replace string with empty string --------------
        vContext.setFieldErrors(null);
        testObj.setTarget("abc");
        testObj.setReplacement("");
        valObj.setStringData("abcdef");
        try {
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));
        assertEquals(
                "Set a date format string and did not get back same string.",
                0, testObj.getNewValue().compareTo("def"));
    }

    
    /**
     * Method to test null object.
     * @throws ValidationException.
     */
    @Test
    public void testNullObject() throws ValidationException {
        ReplaceManipulator testObj = new ReplaceManipulator();
        ValidationTestObject valObj = new ValidationTestObject();
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        
        boolean expectedException = false;
        testObj.setFieldName("stringData");
        testObj.setValidatorContext(vContext);
        // --------------- Replace string with null replacement --------------
        testObj.setTarget("abc");
        valObj.setStringData("abcdef");
        try {
            testObj.validate(valObj);
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue("Valid data triggered errors.", expectedException);
    }
}
