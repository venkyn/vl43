/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.fail;



/**
 * @author jtauberg
 *
 */
public class NonPrintableCharacterReplaceManipulatorTest {

    /**
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass
    protected void setUp() throws Exception {
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.manipulators.
     *   NonPrintableCharacterReplaceManipulator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        //make some non printable characters and put them in a string
        char c1 = 14;
        char c2 = 129;
        char c3 = 234;
        String badString = "Here" + c1 + "Is" + c2 + "The" + c3 + "String";
        
        //Create a new validator
        NonPrintableCharacterReplaceManipulator validator
        = new NonPrintableCharacterReplaceManipulator();
        ValidationTestObject valObj = new ValidationTestObject();
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);

        //Set the manipulator to mess with the StringData field.
        validator.setFieldName("StringData");

        
        // Put the string with bad characters in the field to manipulate.
        valObj.setStringData(badString);
        // Set up the validation to replace non printables in StringData with a space.
        validator.setReplacement(" ");
        try {
            // try to manipulate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse(validator.getValidatorContext().getFieldErrors().containsKey("StringData"),
                "Valid data triggered errors.");
        assertEquals(valObj.getStringData(), "Here Is The String",
                "Manipulator did not replace unprintables with a space!");
        
        // reset field errors for next test case.
        vContext.setFieldErrors(null);

        // Put the string with bad characters in the field to manipulate.
        valObj.setStringData(badString);
        // Set up the validation to replace non printables in StringData with a null string.
        validator.setReplacement("");
        try {
            // try to manipulate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertFalse(validator.getValidatorContext().getFieldErrors().containsKey("StringData"),
                "Valid data triggered errors.");
        assertEquals(valObj.getStringData(), "HereIsTheString",
                "Manipulator did not replace unprintables with a null string!");
        
        // reset field errors for next test case.
        vContext.setFieldErrors(null);

    
    
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.manipulators.
     *      NonPrintableCharacterReplaceManipulator#getReplacement()}.
     * Test method for {@link com.vocollect.voicelink.core.importer.manipulators.
     *      NonPrintableCharacterReplaceManipulator#setReplacement(java.lang.String)}.
     */
    @Test()
    public void testGetSetReplacement() {
        //Create a new validator
        NonPrintableCharacterReplaceManipulator validator
            = new NonPrintableCharacterReplaceManipulator();
        //assert that replacement starts as single space.
        assertEquals(" ", validator.getReplacement(), "replacement did not init as null.");
        //Set a value
        validator.setReplacement("MyReplacement");
        //Read back same value
        assertEquals("MyReplacement", validator.getReplacement(),
            "Value set in replacement was not same value returned (get).");
        
        //Set a value with a single set of double quotes for null
        validator.setReplacement("");
        //Read back same value
        assertEquals("", validator.getReplacement(),
            "Value set in replacement null string was not same value returned (get).");

        //Set a value with a single set of double quotes with a string within
        validator.setReplacement(" ");
        //Read back same value
        assertEquals(" ", validator.getReplacement(),
            "Value set in replacement string (quote space quote) was not same value returned (get).");

        //Set a value with a single set of double quotes with a string within
        validator.setReplacement("bob");
        //Read back same value
        assertEquals("bob", validator.getReplacement(),
            "Value set in replacement string (quote bob quote) was not same value returned (get).");

        //Set a value with a double set of double quotes with a string within
        validator.setReplacement("\"\"seeTheQuotes\"\"");
        //Read back same value
        assertEquals("\"seeTheQuotes\"", validator.getReplacement(),
            "Value set in replacement string (quote quote seeTheQuotes quote quote)"
            + " was not same value returned (get).");
       
    }


}
