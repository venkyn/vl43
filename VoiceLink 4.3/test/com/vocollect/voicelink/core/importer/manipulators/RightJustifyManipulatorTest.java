/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.fail;

/**
 * Test for RightJustifyManipulator.
 * 
 * @author vsubramani
 */
public class RightJustifyManipulatorTest {

    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass
    protected void setUp() throws Exception {
    }

    private RightJustifyManipulator testObj = new RightJustifyManipulator();

    private ValidationTestObject valObj = new ValidationTestObject();

    private ValidationAwareSupport resultHolder = new ValidationAwareSupport();

    private ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.manipulators
     * .RightJustifyManipulator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        testObj.setFieldName("stringData");
        testObj.setValidatorContext(vContext);

        testObj.setSize(10);
        valObj.setStringData("Hello");
        try {
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

        vContext.setFieldErrors(null);
        testObj.setSize(1);
        valObj.setStringData("Import");
        try {
            testObj.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on valid data.");
        }
        assertFalse("Valid data triggered errors.", testObj
                .getValidatorContext().getFieldErrors().containsKey(
                        "stringData"));

    }

    /**
     * @throws ValidationException
     */
    @Test()
    public void testNullObject() throws ValidationException {
        boolean expectedException = false;
        testObj.setFieldName("stringData");
        testObj.setValidatorContext(vContext);
        valObj.setStringData("abcdef");
        try {
            testObj.validate(valObj);
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertFalse("Invalid data triggered errors.", expectedException);
    }
}
