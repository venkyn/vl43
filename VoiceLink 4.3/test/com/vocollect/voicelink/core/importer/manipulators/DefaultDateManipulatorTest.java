/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;

import static java.lang.Math.abs;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.fail;


/**
 * @author dgold
 */
public class DefaultDateManipulatorTest {

    /**
     * 
     * @author dgold
     *
     */
    private class DateCompare implements Comparator<Date> {

        private int millisecondDelta = 0;

        // Number of milliseconds the dates are allowed to differ
        // and still be considered equal.
        /**
         * Default constructor.
         */
        DateCompare() {
            // Nothing to do
        }

        /**
         * 
         * @param millisecondDelta int to set
         */
        DateCompare(int millisecondDelta) {
            setMillisecondDelta(millisecondDelta);
        }

        /**
         * @param o1 object to compare
         * @param o2 object to compare
         * @return 0 if equal or -1 if result is less than 0 and 1 if greater than 0
         */
        public int compare(Date o1, Date o2) {
            Date lhsDate = o1;
            Date rhsDate = o2;
            long result = lhsDate.getTime() - rhsDate.getTime();
            if (abs(result) < this.getMillisecondDelta()) {
                return 0;
            }
            if (result < 0) {
                return -1;
            }
            return 1;
        }

        /**
         * Gets the value of millisecondDelta.
         * 
         * @return the millisecondDelta
         */
        public int getMillisecondDelta() {
            return millisecondDelta;
        }

        /**
         * Sets the value of the millisecondDelta.
         * 
         * @param millisecondDelta
         *            the millisecondDelta to set
         */
        public void setMillisecondDelta(int millisecondDelta) {
            this.millisecondDelta = millisecondDelta;
        }

    }

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeClass()
    protected void setUp() throws Exception {

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.manipulators.DefaultDateManipulator#DefaultDateManipulator()}.
     */
    @Test()
    public void testDefaultDateManipulator() {
        DefaultDateManipulator d = new DefaultDateManipulator();
        assertNotNull("Constructor returned a null", d);
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.manipulators.DefaultDateManipulator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidateObject() {
        DefaultDateManipulator ddm = new DefaultDateManipulator();
        ddm.setFieldName("dateData");
        // The default is set by the system, so no value to set
        ValidationTestObject vto = new ValidationTestObject();
        ValidationTestObject vto2 = new ValidationTestObject();
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        // This is the thing that is used to do the validation
        ValidatorContext vContext = new DelegatingValidatorContext(resultHolder);
        ddm.setValidatorContext(vContext);

        Date now = null;
        Date later = null;
        try {
            // try to validate it
            now = Calendar.getInstance().getTime();
            ddm.validate(vto);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check dates for equality. Allow a 1000ms delta.
        DateCompare dc = new DateCompare(1000);
        assertEquals("Unequal dates: expected " + now + " but got "
                + vto.getDateData(), 0, dc.compare(now, vto.getDateData()));

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e1) {
            // This should never happen
            e1.printStackTrace();
        }
        try {
            // try to validate it
            later = Calendar.getInstance().getTime();
            ddm.validate(vto2);
            ddm.validate(vto);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check dates for equality. Allow a 1000ms delta.
        assertEquals("Unequal dates: expected " + now + " but got "
                + vto.getDateData(), 0, dc.compare(now, vto.getDateData()));
        assertEquals("Unequal dates for 2nd vot instance: expected " + later
                + " but got " + vto2.getDateData(), 0, dc.compare(later, vto2
                .getDateData()));
        assertFalse("Equal dates: expected " + later + " to be different from "
                + vto.getDateData(),
                (0 == dc.compare(later, vto.getDateData())));
    }
}
