/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

/**
 * @author Kalpna
 */
public class TextManipulatorTest {

    // private Runnable runnable;

    /**
     * 
     */
    private class TextManipulatorTestData {
        private String day          = null;

        private String defaultValue = null;

        private String fieldh       = null;

        /**
         * @return the day
         */
        public String getDay() {
            return day;
        }

        /**
         * @param day
         *            the day to set
         */
        public void setDay(String day) {
            this.day = day;
        }

        /**
         * @return the field_h
         */
        public String getFieldh() {
            return fieldh;
        }

        /**
         * @param fieldh
         *            the field_h to set
         */
        public void setFieldh(String fieldh) {
            this.fieldh = fieldh;
        }

        /**
         * @param defaultValue to set
         */
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        /**
         * @return the defaultValue
         */
        public String getDefaultValue() {
            return this.defaultValue;
        }
    }

    private ValidationAwareSupport resultHolder = new ValidationAwareSupport();

    // This is the thing that is used by the DelegatingValidatorContext to keep
    // the result of the validation

    private ValidatorContext       vContext     = new DelegatingValidatorContext(
                                                        resultHolder);

    // This is the thing that is used to do the validation

    private String                 fieldValue   = "";

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.NumericValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidate() {
        // create the test object with the necessary data
        TextManipulatorTestData testData = new TextManipulatorTestData();
        testData.setDefaultValue("9");
        testData.setDay("1234");
        testData.setFieldh(null);

        TextManipulator textManipulator = new TextManipulator();
        try {
            textManipulator.setValidatorContext(vContext);
            textManipulator.setDefaultValue(testData.getDefaultValue());
            String thefield = "fieldh";
            textManipulator.setFieldName(thefield);
            textManipulator.validate(testData);
            this.fieldValue = "9";
            assertEquals(thefield
                    + " field should have been replaced by the default value.",
                    textManipulator.getDefaultValue(), this.fieldValue);
            testData.setFieldh("45.30");
            textManipulator.validate(testData);
            assertTrue(
                    thefield
                            + " field should have been replaced by the default value.",
                            textManipulator.getDefaultValue().toString().equals(
                            this.fieldValue));
            //Null should also replace the existing value
            testData.setDefaultValue(null);
            textManipulator.setDefaultValue(testData.getDefaultValue());
            testData.setFieldh("45.30");
            this.fieldValue = null;
            textManipulator.validate(testData);
            assertEquals(thefield
                    + " field should have been replaced by the default value.",
                    textManipulator.getDefaultValue(), this.fieldValue);
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }
}
