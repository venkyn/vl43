/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.ValidationResult;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.ValidationException;

import org.testng.annotations.Test;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;


/**
 * @author dgold
 *
 */
public class ConcatenateOriginalDataTest {
    
    

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.manipulators.
     * ConcatenateOriginalData#validate(java.lang.Object)}.
     */
    @SuppressWarnings("unchecked")
    @Test()
    public void testValidateObjectEmptyField() {
        ConcatenateOriginalData validator = new ConcatenateOriginalData();
        ConcatTestData valObj = new ConcatTestData();
        // Set up the validation to say that if the value in intData == 1, then the 
        // value in stringData must be present
        validator.setFieldName("targetField");
        validator.setSourceFieldName1("sourceString");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        
        FieldMap fm = new FieldMap();
        
        fm.addField(setUpElement("more"));
        
        fields.setFieldMaps(fm);

        vContext.setFields(fields);
        
        valObj.setTargetField("");

        // Set up the validation to pass
        

        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertEquals(valObj.getTargetField(), "more", "Unspeakable");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.manipulators.
     * ConcatenateOriginalData#validate(java.lang.Object)}.
     */
    @SuppressWarnings("unchecked")
    @Test()
    public void testValidateObjectNullField() {
        ConcatenateOriginalData validator = new ConcatenateOriginalData();
        ConcatTestData valObj = new ConcatTestData();
        // Set up the validation to say that if the value in intData == 1, then the 
        // value in stringData must be present
        validator.setFieldName("targetField");
        validator.setSourceFieldName1("sourceString");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        
        FieldMap fm = new FieldMap();
        
        fm.addField(setUpElement("more"));
        
        fields.setFieldMaps(fm);

        vContext.setFields(fields);
        valObj.setTargetField(null);

        // Set up the validation to pass
        

        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertEquals("more", valObj.getTargetField(), "Unspeakable");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.manipulators.
     * ConcatenateOriginalData#validate(java.lang.Object)}.
     */
    @SuppressWarnings("unchecked")
    @Test()
    public void testValidateObjectWithNullField() {
        ConcatenateOriginalData validator = new ConcatenateOriginalData();
        ConcatTestData valObj = new ConcatTestData();
        // Set up the validation to say that if the value in intData == 1, then the 
        // value in stringData must be present
        validator.setFieldName("targetField");
        validator.setSourceFieldName1("sourceString");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        
        FieldMap fm = new FieldMap();
        
        fm.addField(setUpElement("sourceString", null));
        
        fields.setFieldMaps(fm);

        vContext.setFields(fields);
        valObj.setTargetField("more");

        // Set up the validation to pass
        

        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertEquals("", valObj.getTargetField(), "Unspeakable");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.manipulators.
     * ConcatenateOriginalData#validate(java.lang.Object)}.
     */
    @SuppressWarnings("unchecked")
    @Test()
    public void testValidateObjectFullField() {
        ConcatenateOriginalData validator = new ConcatenateOriginalData();
        ConcatTestData valObj = new ConcatTestData();
        valObj.setTargetField("begin");
        // Set up the validation to say that if the value in intData == 1, then the 
        // value in stringData must be present
        validator.setFieldName("targetField");
        validator.setSourceFieldName1("sourceString");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        
        FieldMap fm = new FieldMap();
        
        fm.addField(setUpElement("more"));
        
        fields.setFieldMaps(fm);

        vContext.setFields(fields);

        // Set up the validation to pass
        

        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertEquals("more", valObj.getTargetField(), "Unspeakable");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.manipulators.
     * ConcatenateOriginalData#validate(java.lang.Object)}.
     */
    @SuppressWarnings("unchecked")
    @Test()
    public void testValidateObjectMultipleField() {
        ConcatenateOriginalData validator = new ConcatenateOriginalData();
        ConcatTestData valObj = new ConcatTestData();
        valObj.setTargetField("begin");
        // Set up the validation to say that if the value in intData == 1, then the 
        // value in stringData must be present
        validator.setFieldName("targetField");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        
        FieldMap fm = new FieldMap();
        
        fm.addField(setUpElement("field1", "more"));
        fm.addField(setUpElement("field2", " and more"));
        
        fields.setFieldMaps(fm);

        vContext.setFields(fields);
        validator.setSourceFieldName1("field1");
        validator.setSourceFieldName2("field2");

        // Set up the validation to pass
        

        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertEquals("moreand more", valObj.getTargetField(), "Unspeakable");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.manipulators.
     * ConcatenateOriginalData#validate(java.lang.Object)}.
     */
    @SuppressWarnings("unchecked")
    @Test()
    public void testValidateObjectPrePost() {
        ConcatenateOriginalData validator = new ConcatenateOriginalData();
        ConcatTestData valObj = new ConcatTestData();
        valObj.setTargetField("begin");
        // Set up the validation to say that if the value in intData == 1, then the 
        // value in stringData must be present
        validator.setFieldName("targetField");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        
        FieldMap fm = new FieldMap();
        
        fm.addField(setUpElement("field1", "more"));
        fm.addField(setUpElement("field2", " and more"));
        
        fields.setFieldMaps(fm);

        vContext.setFields(fields);
        vContext.setPreLookup(true);
        // essentially turn off the manip...
        validator.setCheckPreLookup(false);
        validator.setCheckPostLookup(false);

        // Set up the validation to pass
        validator.setSourceFieldName1("field1");
        validator.setSourceFieldName2("field2");
        

        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        vContext.setPreLookup(false);
        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertEquals("begin", valObj.getTargetField(), "Unspeakable");
        
        
        validator.setCheckPreLookup(true);
        vContext.setPreLookup(true);

        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        assertEquals(valObj.getTargetField(), "moreand more", "Unspeakable");
        vContext.setPreLookup(false);
        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertEquals(valObj.getTargetField(), "moreand more", "Unspeakable");

    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.manipulators.
     * ConcatenateOriginalData#getSourceFieldName()}.
     */
    @Test()
    public void testSetGetSourceFieldName() {
        ConcatenateOriginalData validator = new ConcatenateOriginalData();
        validator.setSourceFieldName1("testField");
        assertEquals("testField", validator.getSourceFieldName1(), "munged source field name");
    }

    /**
     * @param data - the data string
     * @return Field
     */
    private Field setUpElement(String data) {
        Field o = new Field();
        o.setFieldData(data);
        o.setFieldName("sourceString");
        return o;
    }
    /**
     * @param fieldName - the field name
     * @param data - the data string
     * @return Field
     */
    private Field setUpElement(String fieldName, String data) {
        Field o = new Field();
        o.setFieldData(data);
        o.setFieldName(fieldName);
        return o;
    }

    /** Just a test-able object.
     * 
     * @author dgold
     *
     */
    private class ConcatTestData {
        private String targetField = null;

        /**
         * Gets the value of targetField.
         * @return the targetField
         */
        public String getTargetField() {
            return targetField;
        }

        /**
         * Sets the value of the targetField.
         * @param targetField the targetField to set
         */
        public void setTargetField(String targetField) {
            this.targetField = targetField;
        }
    }

}
