/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.DelegatingValidatorContext;
import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.ValidatorContext;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;

/**
 * @author astein
 */
public class DefaultManipulatorTest {

    // private Runnable runnable;

    /**
     * 
     */
    private class DefaultManipulatorTestData {
        private String day          = null;

        private String defaultValue = null;

        private String fieldh       = null;

        /**
         * @param day
         *            the day to set
         */
        public void setDay(String day) {
            this.day = day;
        }

        /**
         * @param fieldh
         *            the field_h to set
         */
        public void setFieldh(String fieldh) {
            this.fieldh = fieldh;
        }

        /**
         * @return the field_h
         */
        public String getFieldh() {
            return fieldh;
        }

        /**
         * @param defaultValue to set
         */
        public void setDefaultValue(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        /**
         * @return the defaultValue
         */
        public String getDefaultValue() {
            return this.defaultValue;
        }
    }

    private ValidationAwareSupport resultHolder = new ValidationAwareSupport();

    // This is the thing that is used by the DelegatingValidatorContext to keep
    // the result of the validation

    private ValidatorContext       vContext     = new DelegatingValidatorContext(
                                                        resultHolder);

    // This is the thing that is used to do the validation

    private String                 fieldValue   = "";

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.NumericValidator#validate(java.lang.Object)}.
     */
    @Test()
    public void testValidate() {
        // create the test object with the necessary data
        DefaultManipulatorTestData testData = new DefaultManipulatorTestData();
        testData.setDefaultValue("9");
        testData.setDay("1234");
        testData.setFieldh(null);

        DefaultManipulator defaultManipulator = new DefaultManipulator();
        try {
            defaultManipulator.setValidatorContext(vContext);
            defaultManipulator.setDefaultValue(testData.getDefaultValue());
            String thefield = "fieldh";
            defaultManipulator.setFieldName(thefield);
            defaultManipulator.validate(testData);
            this.fieldValue = defaultManipulator.getValue().toString();
            assertEquals(thefield
                    + " field should have been replaced by the default value.",
                    defaultManipulator.getDefaultValue(), this.fieldValue);
            testData.setFieldh("45.30");
            defaultManipulator.validate(testData);
            this.fieldValue = defaultManipulator.getValue().toString();
            assertFalse(
                    thefield
                            + " field should NOT have been replaced by the default value.",
                    defaultManipulator.getDefaultValue().toString().equals(
                            this.fieldValue));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    /*
     * public class DelayedStart implements Runnable { private int count;
     * private Thread worker; private DelayedStart(int count) { this.count =
     * count; worker = new Thread(this); worker.start(); } public void run() {
     * TestInput testInput2 = new TestInput(); testInput2.setDay("123");
     * testInput2.setField_h(new Integer(30)); ValidationResult itemToValidate2 =
     * new ValidationResult(testInput2); DefaultManipulator defaultManipulator2 =
     * new DefaultManipulator(); assertNotNull("The field_h value for this
     * object is current NOT null", testInput2.getField_h()); try {
     * defaultManipulator2.validate(itemToValidate2.getThingToValidate());
     * assertFalse("Field_h should NOT be null",
     * defaultManipulator2.validateNull(testInput2.getField_h())); } catch
     * (ValidationException e) { e.printStackTrace(); } } } @Test() public void
     * testManipulatorMultithreading() throws Throwable { runnable = new
     * DelayedStart(1000); Thread.sleep(1000); TestInput testInput1 = new
     * TestInput(); testInput1.setDay("5678"); testInput1.setField_h(null);
     * ValidationResult itemToValidate1 = new ValidationResult(testInput1);
     * DefaultManipulator defaultManipulator1 = new DefaultManipulator(); try {
     * assertEquals("The field_h value should currently be null",
     * testInput1.getField_h(), null);
     * defaultManipulator1.validate(itemToValidate1.getThingToValidate());
     * assertTrue("Field_h should NOT be null", itemToValidate1.validate());
     * assertFalse("Field_h should NOT be null",
     * defaultManipulator1.validateNull(testInput1.getField_h()));
     * assertEquals("The field_h value should now be an Integer object of 9",
     * testInput1.getField_h(), new Integer(9)); } catch (ValidationException e) {
     * e.printStackTrace(); } }
     */

}
