/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.voicelink.core.importer.Field;
import com.vocollect.voicelink.core.importer.FieldMap;
import com.vocollect.voicelink.core.importer.ListValidationTestObject;
import com.vocollect.voicelink.core.importer.ObjectContext;
import com.vocollect.voicelink.core.importer.ValidationResult;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.ValidationException;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.fail;

/**
 * @author dgold
 *
 */
public class ReplaceIfNonUniformOriginaListDataTest {

    /**
     * {@inheritDoc}
     * @throws java.lang.Exception
     */
    @BeforeClass
    protected void setUp() throws Exception {
    }

    /**
     * Test to see if we can validate on the data in the ObjectContext.
     * Pass:
     * 1). Single element in the O.C
     * 2). Multiple elements
     * 
     * Fail:
     * 1). Multiple elements with one different value
     * 2). Multiple elements with several different values.
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateOneElement() {
        ReplaceIfNonUniformOriginaListData validator = new ReplaceIfNonUniformOriginaListData();
        ListValidatorTestData valObj = new ListValidatorTestData();
        // Set up the validation to say that if the value in intData == 1, then the 
        // value in stringData must be present
        validator.setFieldName("stringData");
        validator.setTargetField("xxx");
        validator.setReplacementValue("good");
        valObj.setStringData("jun k");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        
        FieldMap fm = new FieldMap();
        
        fm.addField(setUpElement("same"));
        
        fields.setFieldMaps(fm);

        vContext.setFields(fields);

        // Set up the validation to pass
        

        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertEquals("jun k", valObj.getStringData(), "Triggered when it shouldn't have");
    }
    
    /**
     * 
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testValidateMultipleElements() {
        ReplaceIfNonUniformOriginaListData validator = new ReplaceIfNonUniformOriginaListData();
        ListValidatorTestData valObj = new ListValidatorTestData();
        // Set up the validation to say that if the value in intData == 1, then the 
        // value in stringData must be present
        validator.setFieldName("stringData");
        validator.setTargetField("xxx");
        validator.setReplacementValue("good");
        valObj.setStringData("jun k");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        
         
        for (int i = 0; i < 4; i++) {
            fields.setFieldMaps(setUpMaps(3));
        }

        vContext.setFields(fields);

        // Set up the validation to pass
        

        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        assertEquals("jun k", valObj.getStringData(), "Triggered when it shouldn't have");
    }
    
    /**
     * 
     */
    @Test()
    @SuppressWarnings("unchecked")
    public void testRejectMultipleElements() {
        ReplaceIfNonUniformOriginaListData validator = new ReplaceIfNonUniformOriginaListData();
        ListValidatorTestData valObj = new ListValidatorTestData();
        // Set up the validation to say that if the value in intData == 1, then the 
        // value in stringData must be present
        validator.setFieldName("stringData");
        validator.setTargetField("xxx");
        validator.setReplacementValue("good");
        valObj.setStringData("jun k");
        
        // This is the thing that is used by the DelegatingValidatorContext to
        // keep the result of the validation
        ValidationAwareSupport resultHolder = new ValidationAwareSupport();
        ValidationResult vr = new ValidationResult();
        ValidationResult.VocollectValidatorContext vContext = vr.new VocollectValidatorContext(resultHolder);
        validator.setValidatorContext(vContext);
        ObjectContext fields = new ObjectContext();
        
         
        for (int i = 1; i < 5; i++) {
            fields.setFieldMaps(setUpBadMaps(i));
        }

        vContext.setFields(fields);

        // Set up the validation to pass
        

        try {
            // try to validate it
            validator.validate(valObj);
        } catch (ValidationException e) {
            // If you got in here, it blew up
            e.printStackTrace();
            // Since we expected it to succeed, this is a failure.
            fail("Threw an exception on valid data.");
        }
        // Check to see if any errors were triggered. Hoping none were.
        assertEquals("good", valObj.getStringData(), "Triggered when it shouldn't have");
    }
    

    /**
     * Test method for {@link com.vocollect.voicelink.core.importer.manipulators.ReplaceIfNonUniformOriginaListData#getReplacementValue()}.
     */
    @Test()
    public void testGetReplacementValue() {
        ReplaceIfNonUniformOriginaListData validator = new ReplaceIfNonUniformOriginaListData();
        validator.setReplacementValue("xxx");
        assertEquals(
            "xxx", validator.getReplacementValue(), "Munged replacement value");
    }

    /**
     * 
     */
    @Test()
    public void testGetTargetField() {
        ReplaceIfNonUniformOriginaListData validator = new ReplaceIfNonUniformOriginaListData();
        validator.setTargetField("xxx");
        assertEquals(
            "xxx", validator.getTargetField(), "Munged Target field name");
    }

    /**
     * @param data - the data string
     * @return Field
     */
    private Field setUpElement(String data) {
        Field o = new Field();
        o.setFieldData(data);
        o.setFieldName("xxx");
        return o;
    }

    /**
     * @param count - the count
     * @return FieldMap
     */
    @SuppressWarnings("unchecked")
    private FieldMap setUpMaps(int count) {
        FieldMap fm = new FieldMap();
        
        fm.addField(setUpElement("same"));
        return fm;
    }

    /**
     * @param count - the count
     * @return FieldMap
     */
    @SuppressWarnings("unchecked")
    private FieldMap setUpBadMaps(int count) {
        FieldMap fm = new FieldMap();
        
        fm.addField(setUpElement("same" + String.valueOf(count)));
        return fm;
    }



    /**
     * Inner class.
     */
    @SuppressWarnings("unchecked")
    private class ListValidatorTestData {
        
        private String stringData = null;
        
        private int intData = 0;
        
        private List<ListValidationTestObject> list = new ArrayList<ListValidationTestObject>();

        /**
         * @param o - the validation test object
         */
        public void addElement(ListValidationTestObject o) {
            list.add(o);
        }

        /**
         * 
         */
        public void clearList() {
            list = new ArrayList<ListValidationTestObject>();
        }

        /**
         * @return List<ListValidationTestObject>
         */
        public List<ListValidationTestObject> getList() {
            return list;
        }

        /**
         * @param list - the validation list
         */
        public void setList(List<ListValidationTestObject> list) {
            this.list = list;
        }

        /**
         * Gets the value of stringData.
         * @return the stringData
         */
        public String getStringData() {
            return stringData;
        }

        /**
         * Sets the value of the stringData.
         * @param stringData the stringData to set
         */
        public void setStringData(String stringData) {
            this.stringData = stringData;
        }

        /**
         * Gets the value of intData.
         * @return the intData
         */
        public int getIntData() {
            return intData;
        }

        /**
         * Sets the value of the intData.
         * @param intData the intData to set
         */
        public void setIntData(int intData) {
            this.intData = intData;
        }
    }

}
