/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer.manipulators;

import com.vocollect.voicelink.core.importer.ValidationResult;
import com.vocollect.voicelink.core.importer.ValidationTestObject;

import com.opensymphony.xwork2.ValidationAwareSupport;
import com.opensymphony.xwork2.validator.ValidationException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * This is a test class for the NumberFormatManipulator.
 * 
 * @author vsubramani
 */
public class NumberFormatManipulatorTest {

    /**
     * {@inheritDoc}
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @BeforeClass
    protected void setUp() throws Exception {
    }

    private NumberFormatManipulator testManipulator = new NumberFormatManipulator();

    private ValidationTestObject valObj = new ValidationTestObject();

    private ValidationAwareSupport resultHolder = new ValidationAwareSupport();

    private ValidationResult vR = new ValidationResult();

    private ValidationResult.VocollectValidatorContext vContext = vR.new VocollectValidatorContext(
            resultHolder);

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.manipulators.
     * NumberFormatManipulator#validate(java.lang.Object)}.
     */

    public void testValidateObject() {
        testManipulator.setValidatorContext(vContext);
        // No value is set for the field name
        testManipulator.setFieldName("intData");
        testManipulator.setFieldFormat("%d");
        try {
            testManipulator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on null data.");
        }
        assertFalse("Null data failed to trigger errors.", testManipulator
                .getValidatorContext().getFieldErrors().containsKey("intData"));
        vContext.setFieldErrors(null);

        // Valid name, value and format
        testManipulator.setFieldName("intData");
        testManipulator.setFieldFormat("%3d");
        valObj.setIntData(new Integer(1));
        try {
            testManipulator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on null data.");
        }
        assertFalse("Valid data triggered errors.", testManipulator
                .getValidatorContext().getFieldErrors().containsKey("intData"));
        vContext.setFieldErrors(null);

        // Valid name, value and format
        testManipulator.setFieldName("doubleData");
        testManipulator.setFieldFormat("%f");
        valObj.setDoubleData(new Double(9.9));
        try {
            testManipulator.validate(valObj);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("Threw an exception on null data.");
        }
        assertFalse("Valid data triggered errors.", testManipulator
                .getValidatorContext().getFieldErrors().containsKey(
                        "doubleData"));
    }

    /**
     * @throws ValidationException
     */
    @Test(enabled = true)
    public void testNullFormat() throws ValidationException {
        boolean expectedException = false;
        testManipulator.setValidatorContext(vContext);
        // No value and format are set for the field name
        testManipulator.setFieldName("intData");
        try {
            testManipulator.validate(valObj);
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue("Null data failed to trigger errors.", expectedException);
    }

    /**
     * @throws ValidationException
     */
    @Test(enabled = true)
    public void testDiffValidFormat() throws ValidationException {
        boolean expectedException = false;
        testManipulator.setValidatorContext(vContext);
        // Valid differnt value and format
        testManipulator.setFieldName("intData");
        testManipulator.setFieldFormat("%f");
        valObj.setIntData(new Integer(1));
        try {
            testManipulator.validate(valObj);
        } catch (RuntimeException e) {
            expectedException = true;
        }
        assertTrue(
                "Valid value with different valid format did not trigger errors.",
                expectedException);
    }

}
