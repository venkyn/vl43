/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.importer;

import com.vocollect.voicelink.core.importer.manipulators.DefaultManipulator;

import com.opensymphony.xwork2.validator.ValidationException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

/**
 * @author astein
 */
public class VocollectCustomManipulatorTest {

    private VocollectCustomManipulator vocManipulator = null;

    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeClass
    public void setUp() throws Exception {
        vocManipulator = new DefaultManipulator();
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.VocollectCustomManipulator#isEmptyField(java.lang.String)}.
     */
    @Test
    public final void testIsEmptyField() {
        assertTrue("The field should currently be empty.", vocManipulator
                .isEmptyField(""));
        assertFalse("The field should not currently be empty.", vocManipulator
                .isEmptyField("blah"));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.VocollectCustomManipulator#isNullField(java.lang.Object)}.
     */
    @Test
    public final void testIsNullField() {
        assertTrue("The field should currently be null.", vocManipulator
                .isNullField(null));
        assertFalse("The field should currently be not null.", vocManipulator
                .isNullField(new String("that")));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.VocollectCustomManipulator#needsTrimming(java.lang.String, char)}.
     */
    @Test
    public final void testNeedsTrimmingStringChar() {
        assertTrue("The field should currently need trimming.", vocManipulator
                .needsTrimming("    test", ' '));
        assertTrue("The field should currently need trimming.", vocManipulator
                .needsTrimming("trimthis     ", ' '));
        assertFalse("The field should currently not need trimmed.",
                vocManipulator.needsTrimming("thisandthat", ' '));
        assertTrue("The field should currently need trimming.", vocManipulator
                .needsTrimming("-----hello-----", '-'));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.VocollectCustomManipulator#needsTrimming(java.lang.String)}.
     */
    @Test
    public final void testNeedsTrimmingString() {
        vocManipulator.setDefaultChar(' ');
        assertTrue("The field needs trimmed.", vocManipulator
                .needsTrimming("this and that   "));
        assertFalse("The fields needs not to be trimmed.", vocManipulator
                .needsTrimming("this and that stuff"));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.VocollectCustomManipulator#validate(boolean)}.
     */
    @Test
    public final void testValidateBoolean() {
        try {
            vocManipulator.validate(true);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("validate(boolean) method did not work.");
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.VocollectCustomManipulator#validate(double)}.
     */
    @Test
    public final void testValidateDouble() {
        try {
            double d = 3042.333;
            vocManipulator.validate(d);
        } catch (ValidationException e) {
            e.printStackTrace();
            fail("validate(double) method did not work.");
        }
    }
}
