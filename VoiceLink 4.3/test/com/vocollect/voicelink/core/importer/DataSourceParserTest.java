/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.importer;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.voicelink.core.importer.adapters.FileDataSourceAdapter;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthDataSourceParser;
import com.vocollect.voicelink.core.importer.parsers.FixedLengthFieldMap;

import java.util.ArrayList;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;

/**
 * 
 * IMPORTANT NOTE:  Most of the tests for the Indexes features of
 * DataSourceParser are tested within the file:
 * 
 * FixedLengthDataSourceParserTest.java.
 * 
 * @author astein
 */
public class DataSourceParserTest {
    private FixedLengthFieldMap fields = new FixedLengthFieldMap();
    private Indexes myIndexes = new Indexes();
    private FixedLengthField aField1 = new FixedLengthField();
    private FixedLengthField aField2 = new FixedLengthField();
    private FixedLengthField aField3 = new FixedLengthField();
    private FixedLengthField aField4 = new FixedLengthField();
    private FixedLengthField aField5 = new FixedLengthField();

    private ArrayList<Field> index1List = new ArrayList<Field>();
    private ArrayList<Field> index2List = new ArrayList<Field>();
    private ArrayList<Field> index3List = new ArrayList<Field>();

    private FixedLengthDataSourceParser parser = null;

    
    /**
     */
    @BeforeClass
    public void setupBeforeTest() {
        // sets 5 fields.
        aField1.setDataType("java.lang.String");
        aField1.setFieldName("Name");
        
        aField2.setDataType("boolean");
        aField2.setFieldName("isCool");
        
        aField3.setDataType("java.lang.Integer");
        aField3.setFieldName("Age");
        
        aField4.setDataType("double");
        aField4.setFieldName("Weight");
        
        aField5.setDataType("java.lang.String");
        aField5.setFieldName("FavoriteColor");


        // creates a fieldmap with the 5 fields
        fields.addField(aField1);
        fields.addField(aField2);
        fields.addField(aField3);
        fields.addField(aField4);
        fields.addField(aField5);

        // also creates a field list to make 3 indexes...
        // #1 Name|, #2 Age|Weight|, #3 Name|FavoriteColor|
        index1List.add(fields.get("Name"));

        index2List.add(fields.get("Age"));
        index2List.add(fields.get("Weight"));

        index3List.add(fields.get("Name"));
        index3List.add(fields.get("FavoriteColor"));
 
    }
    
    
    /**
     * @throws java.lang.Exception if unsuccessful
     */
    @BeforeMethod
    public void setUp() throws Exception {
        parser = new FixedLengthDataSourceParser();

        //create 3 indexes and then add them to the indexes object
        Index myIndex = new Index(index1List);
        assertEquals(myIndex.indexFieldsName(), "Name|", "indexFieldsName didn't return updated value.");
        Index myIndex2 = new Index(index2List);
        assertEquals(myIndex2.indexFieldsName(), "Age|Weight|", "indexFieldsName didn't return updated value.");
        Index myIndex3 = new Index(index3List);
        assertEquals(myIndex3.indexFieldsName(), "Name|FavoriteColor|", "indexFieldsName didn't return updated value.");

        //Add all indexes...
        assertTrue(myIndexes.addIndex(myIndex), "Failed to add index1.");
        assertTrue(myIndexes.addIndex(myIndex2), "Failed to add index2.");
        assertTrue(myIndexes.addIndex(myIndex3), "Failed to add index3.");

    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#getFieldDefinitionFile()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#setFieldDefinitionFile(java.lang.String)}.
     */
    @Test
    public final void testGetSetFieldDefinitionFile() {
        parser.setFieldDefinitionFile("fieldfile");
        assertTrue("FieldDefinitionFile is not being set correctly.", parser
            .getFieldDefinitionFile().equals("fieldfile"));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#DataSourceParser()}.
     */
    @Test
    public final void testDataSourceParser() {
        try {
            parser = new FixedLengthDataSourceParser();
            assertNull(
                "DataSourceParser default constructor not initializing correctly.",
                parser.getConfigFileName());
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#close()}.
     */
    @Test
    public final void testClose() {
        parser.close();
        if (null != parser.getSourceAdapter()) {
            assertFalse("This should be closed!", parser.getSourceAdapter()
                .isOpen());
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#closeOnSuccess()}.
     */
    @Test
    public final void testCloseOnSuccess() {
        try {
            parser = new FixedLengthDataSourceParser();
            FileDataSourceAdapter fileAdapter = new FileDataSourceAdapter();
            fileAdapter.setDataSourceName("datasourcename");
            //Not a real file, so don't try and move it.
            fileAdapter.setMoveFilesOnCompletion(false);
            parser.setSourceAdapter(fileAdapter);
            parser.setIndexes(myIndexes);
            parser.closeOnSuccess();
            assertTrue("Indexes should be reset after closingOnSuccess.",
                parser.getIndexes().get(1).getDupItems() == 0);
            assertTrue("Indexes should be reset after closingOnSuccess.",
                parser.getIndexes().get(1).getDupSets() == 0);
        } catch (ConfigurationException e) {
            e.printStackTrace();
        } catch (VocollectException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#closeOnFailure()}.
     */
    @Test
    public final void testCloseOnFailure() {
        try {
            parser = new FixedLengthDataSourceParser();
            FileDataSourceAdapter fileAdapter = new FileDataSourceAdapter();
            fileAdapter.setDataSourceName("datasourcename");
            //Not a real file, so don't try and move it.
            fileAdapter.setMoveFilesOnCompletion(false);
            parser.setSourceAdapter(fileAdapter);
            parser.setIndexes(myIndexes);
            parser.closeOnFailure();
            assertTrue("Indexes should be reset after closingOnSuccess.",
                parser.getIndexes().get(1).getDupItems() == 0);
            assertTrue("Indexes should be reset after closingOnSuccess.",
                parser.getIndexes().get(1).getDupSets() == 0);
        } catch (ConfigurationException e) {
            e.printStackTrace();
        } catch (VocollectException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#getSourceAdapter()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser
     * #setSourceAdapter(com.vocollect.voicelink.core.importer.DataSourceAdapter)}.
     */
    @Test
    public final void testGetSetSourceAdapter() {
        try {
            DataSourceAdapter dataAdapter = new FileDataSourceAdapter();
            parser.setSourceAdapter(dataAdapter);
            assertTrue(
                "Setting source adapter for DataSourceParser did not work.",
                parser.getSourceAdapter().getCurrentState().equals(
                    DataSourceAdapter.INITIALIZEDSTATE));
        } catch (VocollectException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#getFieldList()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser
     * #setFieldList(com.vocollect.voicelink.core.importer.FieldMap)}.
     */
    @Test
    public final void testGetSetFieldList() {
        FixedLengthFieldMap fm = new FixedLengthFieldMap();
        parser.setFieldList(fm);
        assertNotNull("Setting the FieldList did not work.", parser
            .getFieldList());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#isPreScan()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#setPreScan(boolean)}.
     */
    @Test
    public final void testIsPreScan() {
        parser.setPreScan(true);
        assertTrue("Prescan set did not work.", parser.isPreScan());
        parser.setPreScan(false);
        assertFalse("Prescan set did not work.", parser.isPreScan());
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#getConfigFileName()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#setConfigFileName(java.lang.String)}.
     */
    @Test
    public final void testGetSetConfigFileName() {
        try {
            parser.setConfigFileName("theconfigfilename");
            assertTrue(
                "Setting configfilename in DataSourceParser did not work.",
                parser.getConfigFileName().equals("theconfigfilename"));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#getIndexes()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#setIndexes()}.
     */
    @Test
    public final void testGetSetIndexes() {
        Indexes myAIndexes = new Indexes();

        //create 3 indexes and then add them to the indexes object
        Index myAIndex = new Index(index1List);
        assertEquals(myAIndex.indexFieldsName(), "Name|", "indexFieldsName didn't return updated value.");
        Index myAIndex2 = new Index(index2List);
        assertEquals(myAIndex2.indexFieldsName(), "Age|Weight|", "indexFieldsName didn't return updated value.");
        Index myAIndex3 = new Index(index3List);
        assertEquals(myAIndex3.indexFieldsName(), "Name|FavoriteColor|",
            "indexFieldsName didn't return updated value.");

        
        //Add all indexes...
        assertTrue(myAIndexes.addIndex(myAIndex), "Failed to add index1.");
        assertTrue(myAIndexes.addIndex(myAIndex2), "Failed to add index2.");
        assertTrue(myAIndexes.addIndex(myAIndex3), "Failed to add index3.");

        parser.setIndexes(myAIndexes);
        assertNotNull(
            "Setting the Indexes in DataSourceParser did not work.",
            parser.getIndexes());
    }


    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#getDescription()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#setDescription(java.lang.String)}.
     */
    @Test
    public final void testGetSetDescription() {
        parser.setDescription("description for parser object");
        assertTrue(
            "Setting the description in the DataSourceParser did not work.",
            parser.getDescription().equals("description for parser object"));
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#getMappingFileName()}.
     * Test method for
     * {@link com.vocollect.voicelink.core.importer.DataSourceParser#setMappingFileName(java.lang.String)}.
     */
    @Test
    public final void testGetSetMappingFileName() {
        try {
            parser.setMappingFileName("themappingfile");
            assertTrue(
                "Setting the mapping file in the DataSourceParser did not work.",
                parser.getMappingFileName().equals("themappingfile"));
        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }

}
