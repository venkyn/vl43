/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.logging.Logger;

import static com.vocollect.voicelink.core.model.DeliveryLocationMappingType.CustomerNumber;
import static com.vocollect.voicelink.core.model.DeliveryLocationMappingType.Route;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * A test for the DeliveryLocationMapping model class.
 * 
 * @author bnorthrop
 */
public class DeliveryLocationMappingTest {

    /**
     * Logger for debuging.
     */
    private static final Logger log = new Logger(
        DeliveryLocationMappingTest.class);

    /**
     * Test creating a mapping object and checking for equality.
     */
    @Test
    public void testEquality() {
        log.debug(">testEquality()");

        DeliveryLocationMapping dlm1A = new DeliveryLocationMapping(
            "1234", "555", CustomerNumber);

        DeliveryLocationMapping dlm1B = new DeliveryLocationMapping();
        dlm1B.setMappingValue("1234");
        dlm1B.setDeliveryLocation("555");
        dlm1B.setMappingType(CustomerNumber);

        DeliveryLocationMapping dlm2 = new DeliveryLocationMapping(
            "5678", "555", CustomerNumber);
        DeliveryLocationMapping dlm3 = new DeliveryLocationMapping(
            "5678", "555", Route);

        Assert.assertEquals(dlm1A, dlm1B);
        Assert.assertEquals(dlm1A.hashCode(), dlm1B.hashCode());
        Assert.assertEquals(dlm1A, dlm1A);
        Assert.assertEquals(dlm1A.hashCode(), dlm1A.hashCode());

        Assert.assertNotSame(dlm2, dlm3);
        Assert.assertNotSame(dlm1A, dlm3);
        Assert.assertNotSame(dlm2, dlm3);

    }
}
