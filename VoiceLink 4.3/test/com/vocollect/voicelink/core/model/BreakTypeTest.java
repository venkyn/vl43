/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import org.testng.annotations.Test;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;

/**
 * 
 * @author sarumugam
 */

public class BreakTypeTest {

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.BreakType.hashCode()'.
     */
    @Test()
    public void testHashCode() {
        BreakType bt1 = new BreakType();
        bt1.setNumber(123);
        BreakType bt2 = new BreakType();
        bt2.setNumber(123);
        assertEquals("hashCode", bt1.hashCode(), bt2.hashCode());
        assertTrue("hashCodeEquals", bt1.equals(bt2));
        bt2.setNumber(321);
        assertFalse("hashCodeDiffer", bt1.hashCode() == bt2.hashCode());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.BreakType.equals(Object)'.
     */
    @Test()
    public void testEqualsObject() {
        BreakType bt1 = new BreakType();
        bt1.setNumber(123);
        BreakType bt2 = new BreakType();
        bt2.setNumber(123);
        assertTrue("hashCodeEquals", bt1.equals(bt2));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.BreakType.getDescriptiveText()'.
     */
    @Test()
    public void testGetDescriptiveText() {
        BreakType bt1 = new BreakType();
        bt1.setName("myname");
        assertEquals("getDescriptiveTest", bt1.getDescriptiveText(), "myname");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.BreakType.setNumber(int)'.
     */
    @Test()
    public void testSetNumber() {
        BreakType bt1 = new BreakType();
        bt1.setNumber(123);
        assertEquals("setNumber", bt1.getNumber(), new Integer(123));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.BreakType.setName(String)'.
     */
    @Test()
    public void testSetName() {
        BreakType bt1 = new BreakType();
        assertNull("setName", bt1.getName());
        bt1.setName("myname");
        assertEquals("setName", bt1.getName(), "myname");

    }

}
