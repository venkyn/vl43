/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.model;

import org.testng.annotations.Test;


import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;
import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import static org.testng.AssertJUnit.*;
import java.util.Date;



/**
 * @author mraj
 *
 */
@Test(groups = { FAST, MODEL, CORE, VSC })
public class VehicleSafetyCheckResponseTest {
    
    /**
     * Test method for equality testing of hashCode()
     */
    @Test()
    public void testHashCodeEqual() {
        
        Operator operator = new Operator();
        operator.setOperatorIdentifier("vehicleSafetyCheckOperator");

        VehicleSafetyCheck safetyCheck = new VehicleSafetyCheck();
        safetyCheck.setSequence(1);
        safetyCheck.setDescription("Brakes");
        
        Date createdDate = new Date();
        VehicleSafetyCheckResponse response1 = new VehicleSafetyCheckResponse();
        response1.setCheckResponse(-1);
        response1.setOperator(operator);
        response1.setSafetyCheck(safetyCheck);
        response1.setCreatedDate(createdDate);
        
        VehicleSafetyCheckResponse response2 = new VehicleSafetyCheckResponse();
        response2.setCheckResponse(-1);
        response2.setOperator(operator);
        response2.setSafetyCheck(safetyCheck);
        response2.setCreatedDate(createdDate);
        
        assertTrue(response1.hashCode() == response2.hashCode());
    }
    
    /**
     * Test method for non-equality testing of hashCode()
     */
    @Test()
    public void testHashCodeNotEqual() {
        
        Operator operator = new Operator();
        operator.setOperatorIdentifier("vehicleSafetyCheckOperator");

        VehicleSafetyCheck safetyCheck = new VehicleSafetyCheck();
        safetyCheck.setSequence(1);
        safetyCheck.setDescription("Brakes");
        
        VehicleSafetyCheckResponse response1 = new VehicleSafetyCheckResponse();
        response1.setCheckResponse(-1);
        response1.setOperator(operator);
        response1.setSafetyCheck(safetyCheck);
        response1.setCreatedDate(new Date());
        
        VehicleSafetyCheckResponse response2 = new VehicleSafetyCheckResponse();
        response2.setCheckResponse(-2);
        response2.setOperator(operator);
        response2.setSafetyCheck(safetyCheck);
        response2.setCreatedDate(new Date(System.currentTimeMillis() + 1000));
        
        assertTrue(response1.hashCode() != response2.hashCode());
    }
    
    /**
     * Test method for equality testing of equals()
     */
    @Test()
    public void testEquals() {
        
        Operator operator = new Operator();
        operator.setOperatorIdentifier("vehicleSafetyCheckOperator");

        VehicleSafetyCheck safetyCheck = new VehicleSafetyCheck();
        safetyCheck.setSequence(1);
        safetyCheck.setDescription("Brakes");
        
        Date createdDate = new Date();
        VehicleSafetyCheckResponse response1 = new VehicleSafetyCheckResponse();
        response1.setCheckResponse(-1);
        response1.setOperator(operator);
        response1.setSafetyCheck(safetyCheck);
        response1.setCreatedDate(createdDate);
        
        VehicleSafetyCheckResponse response2 = new VehicleSafetyCheckResponse();
        response2.setCheckResponse(-1);
        response2.setOperator(operator);
        response2.setSafetyCheck(safetyCheck);
        response2.setCreatedDate(createdDate);
        
        assertTrue(response1.equals(response2));
    }
    
    /**
     * Test method for non-equality testing of equals()
     */
    @Test()
    public void testNotEquals() {
        
        Operator operator = new Operator();
        operator.setOperatorIdentifier("vehicleSafetyCheckOperator");

        VehicleSafetyCheck safetyCheck = new VehicleSafetyCheck();
        safetyCheck.setSequence(1);
        safetyCheck.setDescription("Brakes");
        
        VehicleSafetyCheckResponse response1 = new VehicleSafetyCheckResponse();
        response1.setCheckResponse(-1);
        response1.setOperator(operator);
        response1.setSafetyCheck(safetyCheck);
        response1.setInternalGroupId(-1L);
        response1.setCreatedDate(new Date());
        
        VehicleSafetyCheckResponse response2 = new VehicleSafetyCheckResponse();
        response2.setCheckResponse(-2);
        response2.setOperator(operator);
        response2.setSafetyCheck(safetyCheck);
        response2.setInternalGroupId(-2L);
        response2.setCreatedDate(new Date(System.currentTimeMillis() + 1000));
        
        assertTrue(!response1.equals(response2));
    }
    
    
}
