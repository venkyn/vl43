/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

/**
 * 
 * @author sarumugam
 */
public class ItemTest {

    /**
     * Test method for 'com.vocollect.voicelink.core.model.Item.hashCode()'.
     */
    @Test()
    public void testHashCode() {
        Item item1 = new Item();
        item1.setNumber("123");
        Item item2 = new Item();
        item2.setNumber("123");
        assertEquals("hashCode", item1.hashCode(), item2.hashCode());
        assertTrue("hashCodeEqual", item1.equals(item2));
        item2.setNumber("321");
        assertFalse("hashCodeDiff", item1.hashCode() == item2.hashCode());
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.model.Item.equals(Object)'.
     */
    @Test()
    public void testEqualsObject() {
        Item item1 = new Item();
        item1.setNumber("123456");
        Item item2 = new Item();
        item2.setNumber("123456");
        assertTrue("equalObject", item1.equals(item2));

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Item.getDescriptiveText()'.
     */
    @Test()
    public void testGetDescriptiveText() {
        Item item1 = new Item();
        item1.setNumber("123");
        assertTrue("setNumberTrue", item1.getNumber().length() > 0);
        assertEquals("setNumberEqual", item1.getNumber(), "123");
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.model.Item.setCube(double)'.
     */
    @Test()
    public void testSetCube() {
        Item item1 = new Item();
        item1.setCube(123.0);
        assertEquals("setCubeNull", item1.getCube(), 123.0);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Item.setDescription(String)'.
     */
    @Test()
    public void testSetDescription() {
        Item item1 = new Item();
        item1.setDescription("desc");
        assertEquals("setDescription", item1.getDescription(), "desc");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Item.setIsVariableWeightItem(boolean)'.
     */
    @Test()
    public void testSetIsVariableWeightItem() {
        Item item1 = new Item();
        item1.setIsVariableWeightItem(true);
        assertTrue("setIsVariableWeightItemTrue", item1
            .getIsVariableWeightItem());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Item.setNumber(String)'.
     */
    @Test()
    public void testSetNumber() {
        Item item1 = new Item();
        assertNull("setNumberNull", item1.getNumber());
        item1.setNumber("123456");
        assertEquals("setNumberEquals", item1.getNumber(), "123456");
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.model.Item.setPack(String)'.
     */
    @Test()
    public void testSetPack() {
        Item item1 = new Item();
        assertNull("setpackNull", item1.getPack());
        item1.setPack("mypack");
        assertEquals("setPackEquals", item1.getPack(), "mypack");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Item.setPhoneticDescription(String)'.
     */
    @Test()
    public void testSetPhoneticDescription() {
        Item item1 = new Item();
        item1.setPhoneticDescription("phonedesc");
        assertEquals(
            "getPhoneticDescription", item1.getPhoneticDescription(),
            "phonedesc");

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Item.setScanVerificationCode(String)'.
     */
    @Test()
    public void testSetScanVerificationCode() {
        Item item1 = new Item();
        assertNull("setScanVerificationCodeNull", item1
            .getScanVerificationCode());
        item1.setScanVerificationCode("code123");
        assertEquals("setScanVerificationCodeEquals", item1
            .getScanVerificationCode(), "code123");
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.model.Item.setSize(String)'.
     */
    @Test()
    public void testSetSize() {
        Item item1 = new Item();
        assertNull("setSizeNull", item1.getSize());
        item1.setSize("SomeSize");
        assertEquals("setSizeEquals", item1.getSize(), "SomeSize");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Item.setSpokenVerificationCode(String)'.
     */
    @Test()
    public void testSetSpokenVerificationCode() {
        Item item1 = new Item();
        assertNull("setSpokenCodeNull", item1.getSpokenVerificationCode());
        item1.setSpokenVerificationCode("code123");
        assertEquals(
            "setSpokenCodeEquals", item1.getSpokenVerificationCode(), "code123");
    }

    /**
     * Test method for 'com.vocollect.voicelink.core.model.Item.setUpc(String)'.
     */
    @Test()
    public void testSetUpc() {
        Item item1 = new Item();
        assertNull("setUpcNull", item1.getUpc());
        item1.setUpc("upccode");
        assertEquals("setUpcEquals", item1.getUpc(), "upccode");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Item.setVariableWeightTolerance(Integer)'.
     */
    @Test()
    public void testSetVariableWeightTolerance() {
        Item item1 = new Item();
        item1.setVariableWeightTolerance(10);
        assertEquals("setVaribleWtTolEquals", item1
            .getVariableWeightTolerance().toString(), "10");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Item.setWeight(double)'.
     */
    @Test()
    public void testSetWeight() {
        Item item1 = new Item();
        assertTrue("setWeightTrue", item1.getWeight() == 0.0);
        item1.setWeight(10.0);
        assertEquals("getWeightEquals", item1.getWeight(), 10.0);
    }

}
