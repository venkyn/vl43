/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;
import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * 
 * 
 * @author khazra
 */
@Test(groups = { FAST, MODEL, CORE, VSC })
public class VehicleTypeTest {

    /**
     * Test the hash code generates equal number
     */
    @Test()
    public void testHashCodeEqual() {
        VehicleType type1 = new VehicleType();
        type1.setNumber(1);
        type1.setDescription("Fork Lift");

        VehicleType type2 = new VehicleType();
        type2.setNumber(1);
        type2.setDescription("Fork Lift");

        assertEquals("Hash code not same", type1.hashCode(), type2.hashCode());
    }

    /**
     * Test the hash code generates different number
     */
    @Test()
    public void testHashCodeDifferent() {
        VehicleType type1 = new VehicleType();
        type1.setNumber(1);
        type1.setDescription("Fork Lift");

        VehicleType type2 = new VehicleType();
        type2.setNumber(2);
        type2.setDescription("Automated Fork Lift");

        assertEquals("Hash code should be different",
            type1.hashCode() != type2.hashCode(), true);
    }

    /**
     * Tests two types are equal
     */
    @Test()
    public void testEqual() {
        VehicleType type1 = new VehicleType();
        type1.setNumber(1);
        type1.setDescription("Fork Lift");

        VehicleType type2 = new VehicleType();
        type2.setNumber(1);
        type2.setDescription("Fork Lift");

        assertEquals("Types should be equal", type1.equals(type2), true);
    }

    /**
     * Tests two types are not equal
     */
    @Test()
    public void testNotEqual() {
        VehicleType type1 = new VehicleType();
        type1.setNumber(1);
        type1.setDescription("Fork Lift");

        // When compared with other object
        Object obj = new Object();

        assertEquals("Objects should not be equal", type1.equals(obj), false);

        // Number of one type is null
        VehicleType typeNullNumber = new VehicleType();
        typeNullNumber.setNumber(null);
        typeNullNumber.setDescription("Automated Fork Lift");

        assertEquals("Types should not be equal", typeNullNumber.equals(type1),
            false);

        // When compared with different vehicle type

        VehicleType type2 = new VehicleType();
        type2.setNumber(2);
        type2.setDescription("Automated Fork Lift");

        assertEquals("Types should not be equal", type1.equals(type2), false);
    }
}
