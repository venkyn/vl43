/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import java.util.Locale;
import org.testng.annotations.Test;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;

/**
 * 
 * 
 * @author sarumugam
 */
public class TerminalTest {

    /**
     * Test method for 'com.vocollect.voicelink.core.model.Device.hashCode()'.
     */
    public void testHashCode() {

        Device term1 = new Device();
        term1.setSerialNumber("123456");
        Device term2 = new Device();
        term2.setSerialNumber("123456");
        assertEquals("hashCode", term1.hashCode(), term2.hashCode());
        assertTrue("hashCodeEquals", term1.equals(term2));
        term2.setSerialNumber("456789");
        assertFalse("hashCodeDiff", term1.hashCode() == term2.hashCode());

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Device.equals(Object)'.
     * 
     */
    @Test()
    public void testEqualsObject() {
        Device term1 = new Device();
        Device term2 = new Device();
        term1.setSerialNumber("123456");
        term2.setSerialNumber("123456");
        assertTrue("equals", term1.equals(term2));

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Device.getDescriptiveText()'.
     * 
     */
    @Test()
    public void testGetDescriptiveText() {
        Device term1 = new Device();
        term1.setSerialNumber("123456");
        assertTrue(
            "getDescriptiveLength", term1.getDescriptiveText().length() > 0);
        assertEquals("getDescriptiveText", term1.getDescriptiveText(), "123456");

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Device.setTaskLocale(Locale)'.
     * 
     */
    @Test()
    public void testSetTaskLocale() {
        Device term1 = new Device();
        Locale loc = new Locale("en");
        assertNull("setTaskLocaleNull", term1.getTaskLocale());
        term1.setTaskLocale(loc);
        assertNotNull("setTaskLocaleNotNull", term1.getTaskLocale());
        assertEquals("setTaskLocale", term1.getTaskLocale(), loc);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Device.setOperator(Operator)'.
     * 
     */
    @Test()
    public void testSetOperator() {
        Operator op = new Operator();
        Device term1 = new Device();
        assertNull("setOperatorNull", term1.getOperator());
        term1.setOperator(op);
        assertNotNull("setOperatorNotNull", term1.getOperator());
        assertEquals("setOperator", term1.getOperator(), op);

    }

    /**
     * Test method for.
     * 'com.vocollect.voicelink.core.model.Device.setSerialNumber(String)'
     */
    public void testSetSerialNumber() {
        Device term1 = new Device();
        assertNull("setSerialNumber", term1.getSerialNumber());
        term1.setSerialNumber("12345");
        String serNo = term1.getSerialNumber();
        assertNotNull("setSerialNumberNotNull", serNo);
        assertTrue("setSerialNumberTrue", serNo.length() > 0);
        assertEquals("getSerialNumber", term1.getSerialNumber(), "123456");
    }

}
