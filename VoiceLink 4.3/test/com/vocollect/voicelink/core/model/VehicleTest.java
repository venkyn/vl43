/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;
import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * 
 * 
 * @author khazra
 */
@Test(groups = { FAST, MODEL, CORE, VSC })
public class VehicleTest {

    /**
     * Test the hash code generates equal number
     */
    @Test()
    public void testHashCodeEqual() {
        VehicleType vehicleType = new VehicleType();
        Vehicle vehicle1 = new Vehicle();
        vehicle1.setVehicleNumber("1");
        vehicle1.setVehicleType(vehicleType);

        Vehicle vehicle2 = new Vehicle();
        vehicle2.setVehicleNumber("1");
        vehicle2.setVehicleType(vehicleType);

        assertEquals("Hash code not same", vehicle2.hashCode(),
            vehicle2.hashCode());
    }

    /**
     * Test the hash code generates different number
     */
    @Test()
    public void testHashCodeDifferent() {
        VehicleType vehicleType = new VehicleType();

        Vehicle vehicle1 = new Vehicle();
        vehicle1.setVehicleNumber("1");
        vehicle1.setVehicleType(vehicleType);

        Vehicle vehicle2 = new Vehicle();
        vehicle2.setVehicleNumber("2");
        vehicle2.setVehicleType(vehicleType);

        assertEquals("Hash code should be different",
            vehicle1.hashCode() != vehicle2.hashCode(), true);
    }

    /**
     * Tests two types are equal
     */
    @Test()
    public void testEqual() {
        VehicleType vehicleType = new VehicleType();

        Vehicle vehicle1 = new Vehicle();
        vehicle1.setVehicleNumber("1");
        vehicle1.setVehicleType(vehicleType);

        Vehicle vehicle2 = new Vehicle();
        vehicle2.setVehicleNumber("1");
        vehicle2.setVehicleType(vehicleType);

        assertEquals("Types should be equal", vehicle1.equals(vehicle2), true);
    }

    /**
     * Tests two types are not equal
     */
    @Test()
    public void testNotEqual() {
        VehicleType vehicleType = new VehicleType();

        Vehicle vehicle1 = new Vehicle();
        vehicle1.setVehicleNumber("1");
        vehicle1.setVehicleType(vehicleType);

        // When compared with other object
        Object obj = new Object();

        assertEquals("Objects should not be equal", vehicle1.equals(obj), false);

        // Number of one type is null
        Vehicle vehicle2 = new Vehicle();
        vehicle2.setVehicleNumber(null);
        vehicle2.setVehicleType(vehicleType);

        assertEquals("Types should not be equal", vehicle2.equals(vehicle1),
            false);

        // When compared with different vehicle type
        Vehicle vehicle3 = new Vehicle();
        vehicle3.setVehicleNumber("2");
        vehicle3.setVehicleType(vehicleType);

        assertEquals("Types should not be equal", vehicle1.equals(vehicle3),
            false);
    }
}
