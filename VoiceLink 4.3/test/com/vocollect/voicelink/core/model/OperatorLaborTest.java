/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import java.util.Date;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertTrue;

/**
 * 
 * @author sarumugam
 */
public class OperatorLaborTest {
    
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.OperatorLabor.getOperator()'.
     */
    @Test()
    public void testGetSetOperator() {
        OperatorLabor opLabor1 = new OperatorLabor();
        Operator op = new Operator();
        opLabor1.setOperator(op);
        assertEquals("getOperatorTestEquals", opLabor1.getOperator(), op);
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.OperatorLabor.getActionType()'
     * 'com.vocollect.voicelink.core.model.OperatorLabor.setActionType()'.
     */
    @Test()
    public void testGetSetActionType() {
        OperatorLabor opLabor1 = new OperatorLabor();
        opLabor1.setActionType(OperatorLaborActionType.SignOn);
        assertEquals(
            "getActionType", opLabor1.getActionType(), OperatorLaborActionType.SignOn);
    }
    

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.OperatorLabor.getStartTime()'
     * 'com.vocollect.voicelink.core.model.OperatorLabor.setStartTime()'.
     *      
     */
    @Test()
    public void testGetSetStartTime() {
        OperatorLabor opLabor1 = new OperatorLabor();
        Date startTime = new Date();
        opLabor1.setStartTime(startTime);
        assertEquals(
            "getStartTimeTestEquals", opLabor1.getStartTime(), startTime);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.OperatorLabor.getEndTime()'.
     * 'com.vocollect.voicelink.core.model.OperatorLabor.setEndTime()'.
     */
    @Test()
    public void testGetSetEndTime() {
        OperatorLabor opLabor1 = new OperatorLabor();
        Date endTime = new Date();
        opLabor1.setEndTime(endTime);
        assertEquals("getEndTimeTestEquals", opLabor1.getEndTime(), endTime);
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.OperatorLabor.getDuration()'.
     * 'com.vocollect.voicelink.core.model.OperatorLabor.setEndTime()'.
     */
    @Test()
    public void testGetSetDuration() {
        
        OperatorLabor opLabor1 = new OperatorLabor();

        Long duration = new Long(3600);
        opLabor1.setDuration(duration);
        assertEquals("getSetDuration", opLabor1.getDuration(), duration);
    }
    
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.OperatorLabor.hashCode()'.
     */
    @Test()
    public void testHashCode() {
        OperatorLabor opLabor1 = new OperatorLabor();
        Operator oper = new Operator();
        opLabor1.setOperator(oper);
        assertNotNull("hashCodeTestNotNull", opLabor1.hashCode());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.OperatorLabor.equals(Object)'.
     */
    @Test()
    public void testEqualsObject() {
        OperatorLabor opLabor1 = new OperatorLabor();
        OperatorLabor opLabor2 = new OperatorLabor();
        assertTrue("equalsObjectTestTrue", opLabor1.equals(opLabor2));

    }
}
