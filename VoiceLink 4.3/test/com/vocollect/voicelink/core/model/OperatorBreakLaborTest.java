/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * 
 * @author sarumugam
 */
public class OperatorBreakLaborTest {

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.OperatorLabor.getBreakType()'.
     */
    
    @Test()
    public void testGetBreakType() {
        OperatorBreakLabor opLabor1 = new OperatorBreakLabor();
        BreakType bt = new BreakType();
        bt.setNumber(1);
        bt.setName("lunch");
        opLabor1.setBreakType(bt);
        assertEquals("getBreakType", opLabor1.getBreakType(), bt);
    }

     /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.OperatorLabor.getPreviousOperatorLabor()'.
     */
    
    @Test()
    public void testGetPreviousOperatorLabor() {
        OperatorBreakLabor opLabor1 = new OperatorBreakLabor();
        OperatorBreakLabor opLabor2 = new OperatorBreakLabor();
        opLabor2.setPreviousOperatorLabor(opLabor1);
        assertEquals("getPreviousOperatorLaborTestEquals", 
                      opLabor2.getPreviousOperatorLabor(), opLabor1);

    }

}
