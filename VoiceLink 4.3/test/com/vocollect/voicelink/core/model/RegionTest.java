/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;


import org.testng.annotations.Test;


import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;

/**
 * 
 * @author sarumugam
 */
public class RegionTest {

    /**
     * Test method for 'com.vocollect.voicelink.core.model.Region.hashCode()'.
     */
    @Test()
    public void testHashCode() {
        Region reg1 = new Region();
        reg1.setNumber(123);
        Region reg2 = new Region();
        reg2.setNumber(123);
        assertEquals("hashCodeEquals", reg1.hashCode(), reg2.hashCode());
        assertTrue("hashCodeTrue", reg1.equals(reg2));

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Region.equals(Object)'.
     */
    @Test()
    public void testEqualsObject() {
        Region reg1 = new Region();
        reg1.setNumber(123);
        Region reg2 = new Region();
        reg2.setNumber(123);
        assertTrue("equalObjectTrue", reg1.equals(reg2));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Region.getDescriptiveText()'.
     */
    @Test()
    public void testGetDescriptiveText() {
        Region reg1 = new Region();
        reg1.setName("myname");
        assertEquals(
            "getDescriptiveTextEquals", reg1.getDescriptiveText(), "myname");

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Region.setDescription(String)'.
     */
    @Test()
    public void testSetDescription() {
        Region reg1 = new Region();
        assertNull("setDescriptionNull", reg1.getDescription());
        reg1.setDescription("SomeDesc");
        assertEquals("setDescriptionEquals", reg1.getDescription(), "SomeDesc");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Region.setGoalRate(Integer)'.
     */
    @Test()
    public void testSetGoalRate() {
        Region reg1 = new Region();
        assertNull("setGoalRateNull", reg1.getGoalRate());
        reg1.setGoalRate(123);
        assertEquals("setGoalRateEquals", reg1.getGoalRate().toString(), "123");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Region.setName(String)'.
     */
    @Test()
    public void testSetName() {
        Region reg1 = new Region();
        assertNull("setNameNull", reg1.getName());
        reg1.setName("Myname");
        assertEquals("setNameEquals", reg1.getName(), "Myname");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Region.setNumber(int)'.
     */
    @Test()
    public void testSetNumber() {
        Region reg1 = new Region();
        assertTrue("setNumberTrue", reg1.getNumber() == null);
        reg1.setNumber(123);
        assertEquals("setNumberEquals", reg1.getNumber(), (Integer) 123);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Region.setType(RegionType)'.
     */
    @Test()
    public void testSetType() {
        Region reg1 = new Region();
        RegionType type = reg1.getType();
        reg1.setType(type);
        assertEquals("setTypeEquals", reg1.getType(), type);
    }

}
