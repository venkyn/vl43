/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * 
 * @author sarumugam
 */
public class OperatorFunctionLaborTest {
  
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.OperatorFunctionLabor.getCount()'.
     * 'com.vocollect.voicelink.core.model.OperatorFunctionLabor.setCount()'.
     */
    @Test()
    public void testGetActionCount() {
        OperatorFunctionLaborRoot opLabor1 = new OperatorFunctionLabor();
        opLabor1.setCount(new Integer(10));
        assertEquals("getCountEquals", opLabor1.getCount(), new Integer(10));
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.OperatorFunctionLabor.getActualRate()'.
     * 'com.vocollect.voicelink.core.model.OperatorFunctionLabor.setActualRate()'.
     */
    @Test()
    public void testGetActualRate() {
        // verify that value gets rounded to two decimal places.
        OperatorFunctionLaborRoot opLabor1 = new OperatorFunctionLabor();
        Double value = new Double(10.15513);
        Double returnValue = new Double(10.16);
        opLabor1.setActualRate(value);
        assertEquals("getActualRate ", opLabor1.getActualRate(), returnValue);
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.OperatorFunctionLabor.getPercentOfGoal()'.
     * 'com.vocollect.voicelink.core.model.OperatorFunctionLabor.getPercentOfGoal()'.
     */
    @Test()
    public void testGetPercentOfGoal() {
        // verify that value gets rounded to two decimal places.
        OperatorFunctionLaborRoot opLabor1 = new OperatorFunctionLabor();
        Double value = new Double(10.15513);
        Double returnValue = new Double(10.16);
        opLabor1.setPercentOfGoal(value);
        assertEquals("getPrecentOfGoal ", opLabor1.getPercentOfGoal(), returnValue);
    } 
}
