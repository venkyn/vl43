/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import org.testng.annotations.Test;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;

/**
 * 
 * @author estoll
 */

public class PrinterTest {

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Printer.hashCode()'.
     */
    @Test()
    public void testHashCode() {
        Printer p1 = new Printer();
        p1.setNumber(123);
        Printer p2 = new Printer();
        p2.setNumber(123);
        assertEquals("hashCode", p1.hashCode(), p2.hashCode());
        assertTrue("hashCodeEquals", p1.equals(p2));
        p2.setNumber(321);
        assertFalse("hashCodeDiffer", p1.hashCode() == p2.hashCode());
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Printer.equals(Object)'.
     */
    @Test()
    public void testEqualsObject() {
        Printer p1 = new Printer();
        p1.setNumber(123);
        Printer p2 = new Printer();
        p2.setNumber(123);
        assertTrue("hashCodeEquals", p1.equals(p2));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Printer.getDescriptiveText()'.
     */
    @Test()
    public void testGetDescriptiveText() {
        Printer p1 = new Printer();
        p1.setName("myname");
        assertEquals("getDescriptiveTest", p1.getDescriptiveText(), "myname");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Printer.setNumber(int)'.
     */
    @Test()
    public void testSetNumber() {
        Printer p1 = new Printer();
        p1.setNumber(123);
        assertEquals("setNumber", p1.getNumber().intValue(), 123);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Printer.setName(String)'.
     */
    @Test()
    public void testSetName() {
        Printer p1 = new Printer();
        assertNull("setName", p1.getName());
        p1.setName("myname");
        assertEquals("setName", p1.getName(), "myname");

    }

}
