/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;


import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Test the enum DeliveryLocationMappingType.
 * 
 * @author Ben Northrop
 */
public class DeliveryLocationMappingTypeTest {

    private static final Integer MAPPING_VALUE_CUSTOMER_NUMBER = 1;
    
    private static final Integer MAPPING_VALUE_ROUTE = 2;
    
    private static final Integer RANDOM_NUMBER = 1234123;
    
    /**
     * Test that the default is Customer Number.
     */
    @Test
    public void testDefault() {
        DeliveryLocationMappingType mappingType = DeliveryLocationMappingType
            .getDefault();
        Assert.assertEquals(
            DeliveryLocationMappingType.Route, mappingType);
    }
    
    /**
     * Test that both types are findable.
     */
    @Test 
    public void testGet() { 
        DeliveryLocationMappingType customerNumberMappingType = 
            DeliveryLocationMappingType.get(MAPPING_VALUE_CUSTOMER_NUMBER);
        DeliveryLocationMappingType routeMappingType = DeliveryLocationMappingType.get(MAPPING_VALUE_ROUTE);
        
        Assert.assertEquals(customerNumberMappingType, DeliveryLocationMappingType.CustomerNumber);
        Assert.assertEquals(routeMappingType, DeliveryLocationMappingType.Route);
        
    }
    
    /**
     * Test invalid input to the get.
     */
    @Test
    public void testGetInvalidInput() { 
        try { 
            DeliveryLocationMappingType.get(RANDOM_NUMBER);
            Assert.fail("Should have received exception for get(RANDOM).");
        } catch (IllegalStateException ise) { 
            // expected exception
        }
        
        try { 
            DeliveryLocationMappingType.get(null);
            Assert.fail("Should have received exception for get(RANDOM).");
        } catch (IllegalStateException ise) { 
            // expected exception
        }
    }
    
}
