/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

/**
 * 
 * @author sarumugam
 */

public class OperatorTest {

    /**
     * Test method for 'com.vocollect.voicelink.core.model.Operator.hashCode()'.
     */
    @Test()
    public void testHashCode() {
        Operator oper1 = new Operator();
        oper1.setOperatorIdentifier("Id123");
        Operator oper2 = new Operator();
        oper2.setOperatorIdentifier("Id123");
        assertEquals("hashCodeEquals", oper1.hashCode(), oper2.hashCode());
        assertTrue("hashCodeTrue", oper1.equals(oper2));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.equals(Object)'.
     */
    @Test()
    public void testEqualsObject() {
        Operator oper1 = new Operator();
        oper1.setOperatorIdentifier("Id123");
        Operator oper2 = new Operator();
        oper2.setOperatorIdentifier("Id123");
        assertTrue("equalsObjectTrue", oper1.equals(oper2));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.getDescriptiveText()'.
     */
    @Test()
    public void testGetDescriptiveText() {
        Operator oper1 = new Operator();
        oper1.setOperatorIdentifier("Id123");
        assertEquals(
            "getDescriptiveTextEquals", oper1.getOperatorIdentifier(), "Id123");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.setAssignedRegion(Region)'.
     */
    @Test()
    public void testSetAssignedRegion() {
        Operator oper1 = new Operator();
        Region reg = new Region();
        oper1.setAssignedRegion(reg);
        assertEquals("setAssignedRegionEquals", oper1.getAssignedRegion(), reg);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.setCurrentRegion(Region)'.
     */
    @Test()
    public void testSetCurrentRegion() {
        Operator oper1 = new Operator();
        Region reg = new Region();
        oper1.setCurrentRegion(reg);
        assertEquals("setCurrentRegionEquals", oper1.getCurrentRegion(), reg);
    }

     /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.setLastLocation(Location)'.
     */
    @Test()
    public void testSetLastLocation() {
        Operator oper1 = new Operator();
        Location loc = new Location();
        oper1.setLastLocation(loc);
        assertEquals("setLastLocationTestEquals", oper1.getLastLocation(), loc);

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.setSignOffTime(Date)'.
     */
    @Test()
    public void testSetSignOffTime() {
        Operator oper1 = new Operator();
        Date date = new Date();
        oper1.setSignOffTime(date);
        assertEquals("setSignOffTimeEquals", oper1.getSignOffTime(), date);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.setSignOnTime(Date)'.
     */
    @Test()
    public void testSetSignOnTime() {
        Operator oper1 = new Operator();
        Date date = new Date();
        oper1.setSignOnTime(date);
        assertEquals("setSignOnTimeEquals", oper1.getSignOnTime(), date);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.setOperatorIdentifier(String)'.
     */
    @Test()
    public void testSetOperatorIdentifier() {
        Operator oper1 = new Operator();
        assertNull("setOperatorIdentifierNull", oper1.getOperatorIdentifier());
        oper1.setOperatorIdentifier("Id123");
        assertEquals("setOperatorIdentifierEquals", oper1
            .getOperatorIdentifier(), "Id123");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.setPassword(String)'.
     */
    @Test()
    public void testSetPassword() {
        Operator oper1 = new Operator();
        assertNull("setPasswordTestNull", oper1.getPassword());
        oper1.setPassword("pass");
        assertEquals("getPasswordTestEquals", oper1.getPassword(), "pass");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.setStatus(OperatorStatus)'.
     */
    @Test()
    public void testSetStatus() {
        Operator oper1 = new Operator();
        oper1.setStatus(OperatorStatus.SignedOn);
        assertEquals(
            "setStatusTestEquals", oper1.getStatus(), OperatorStatus.SignedOn);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.getTerminals()'.
     */
    @Test()
    public void testGetTerminals() {
        Operator oper1 = new Operator();
        Set<Device> terminal = new HashSet<Device>();
        Device term1 = new Device();
        terminal.add(term1);
        oper1.setTerminals(terminal);
        assertEquals("getTerminalsTestEquals", oper1.getTerminals(), terminal);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.setTerminals(Set)'.
     */
    @Test()
    public void testSetTerminals() {
        // TODO
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.signOn(Date)'.
     */
    @Test()
    public void testSignOn() {
        Operator oper1 = new Operator();
        Date time = new Date();
        assertEquals(
            "SignOnTestEquals1", oper1.getStatus(), OperatorStatus.SignedOff);
        oper1.signOn(time);
        assertEquals(
            "SignOnTestEquals2", oper1.getStatus(), OperatorStatus.SignedOn);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.signOff(Date)'.
     */
    @Test()
    public void testSignOff() {
        Operator oper1 = new Operator();
        Date time = new Date();
        oper1.signOff(time);
        assertEquals(
            "SignOnTestEquals2", oper1.getStatus(), OperatorStatus.SignedOff);
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.addTerminal(Device)'.
     */
    @Test()
    public void testAddTerminal() {
        Operator oper1 = new Operator();
        Device terminal1 = new Device();
        oper1.addTerminal(terminal1);
        assertEquals("addTerminalTestEquals", terminal1.getOperator(), oper1);
        assertNotNull("addTerminalTestNotNull", terminal1.getOperator());

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Operator.removeAllTerminals()'.
     */
    @Test()
    public void testRemoveAllTerminals() {
        Operator oper1 = new Operator();
        Device terminal1 = new Device();
        oper1.addTerminal(terminal1);
        assertEquals(
            "removeAllTerminalTestEquals", terminal1.getOperator(), oper1);
        oper1.removeAllTerminals();
        assertNull("removeAllTerminalTestEquals", terminal1.getOperator());
    }

}
