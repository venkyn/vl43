/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;


/**
 * 
 * @author sarumugam
 */
public class LocationTest {

    /**
     * Test method for 'com.vocollect.voicelink.core.model.Location.hashCode()'.
     */
    @Test()
    public void testHashCode() {
        Location loc1 = new Location();
        loc1.setScannedVerification("123");
        Location loc2 = new Location();
        loc2.setScannedVerification("123");
        assertEquals("hashCodeEquals", loc1.hashCode(), loc2.hashCode());
        assertTrue("hashCodeTrue", loc1.equals(loc2));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Location.equals(Object)'.
     */
    @Test()
    public void testEqualsObject() {
        Location loc1 = new Location();
        loc1.setScannedVerification("123");
        Location loc2 = new Location();
        loc2.setScannedVerification("123");
        assertTrue("equalsObjectTrue", loc1.equals(loc2));
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Location.getDescriptiveText()'.
     */
    @Test()
    public void testGetDescriptiveText() {
        Location loc1 = new Location();
        loc1.setScannedVerification("scan123");
        assertEquals(loc1.getScannedVerification(), "scan123");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Location.setCheckDigits(String)'.
     */
    @Test()
    public void testSetCheckDigits() {
        Location loc1 = new Location();
        // verify that check digit value is initially null
        assertEquals("setCheckDigitsNull", loc1.getCheckDigits(), "");
        loc1.setCheckDigits("123456");
        assertEquals("setCheckDigitsEquals", loc1.getCheckDigits(), "123456");
        loc1.setCheckDigits("");
        assertEquals("setCheckDigitsEqualsBlank", loc1.getCheckDigits(), "");
        loc1.setCheckDigits(null);
        assertEquals("setCheckDigitsEqualsBlank", loc1.getCheckDigits(), "");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Location.setDescription(LocationDescription)'.
     */
    @Test()
    public void testSetDescription() {
        Location loc1 = new Location();
        LocationDescription locdes = loc1.getDescription();
        loc1.setDescription(locdes);
        assertEquals("setDescriptionEquals", loc1.getDescription(), locdes);
    }


    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Location.setScannedVerification(String)'.
     */
    @Test()
    public void testSetScannedVerification() {
        Location loc1 = new Location();
        assertNull("setScannedVerificationNull", loc1.getScannedVerification());
        loc1.setScannedVerification("scan123");
        assertEquals("setScannedVerificationEquals", loc1
            .getScannedVerification(), "scan123");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Location.setSpokenVerification(String)'.
     */
    @Test()
    public void testSetSpokenVerification() {
        Location loc1 = new Location();
        assertNull("setSpokenVerificationNull", loc1.getSpokenVerification());
        loc1.setSpokenVerification("Speak123");
        assertEquals("setSpokenVerificationEquals", loc1
            .getSpokenVerification(), "Speak123");
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Location.setStatus(LocationStatus)'.
     */
    @Test()
    public void testSetStatus() {
        Location loc1 = new Location();
        loc1.setStatus(LocationStatus.Pending);
        assertEquals(
            "setStatusEquals", loc1.getStatus(), LocationStatus.Pending);
        
        assertEquals("status.fromValue", loc1.getStatus().fromValue(new Integer(1)), LocationStatus.Replenished);
        assertEquals("status.fromValue", loc1.getStatus().fromValue(1), LocationStatus.Replenished);

    }


    /**
     * Test method for
     * 'com.vocollect.voicelink.core.model.Location.getItemStatus(Item)'.
     */
    @Test()
    public void testGetItemStatus() {
        Location loc1 = new Location();
        Item item = new Item();
        loc1.setStatus(LocationStatus.Pending);
        assertEquals(
            "getitemStatus", loc1.getStatus(item), LocationStatus.Pending);
    }

}
