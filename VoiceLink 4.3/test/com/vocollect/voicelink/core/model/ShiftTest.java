/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;
import static com.vocollect.epp.test.TestGroups.VSC;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;


/**
 * 
 * 
 * @author smittal
 */
@Test(groups = { FAST, MODEL, CORE, VSC })
public class ShiftTest {

    /**
     * . Test the hash code generates equal number
     */
    @Test(enabled = false)
    public void testHashCodeEqual() {

        ShiftTime shiftTime = new ShiftTime();
        shiftTime.setHours(0);
        shiftTime.setMinutes(45);

        Shift shift1 = new Shift();
        Shift shift2 = new Shift();

        shift1.setName("shift1");
        shift1.setStartTime(shiftTime);
        shift1.setEndTime(shiftTime);

        shift2.setName("shift1");
        shift2.setStartTime(shiftTime);
        shift2.setEndTime(shiftTime);

        assertEquals("Hash codes are not same", shift2.hashCode(),
            shift1.hashCode());
    }

    /**
     * . Test the hash code generates different number
     */
    @Test(enabled = false)
    public void testHashCodeDifferent() {

        ShiftTime shiftTime = new ShiftTime();
        shiftTime.setHours(0);
        shiftTime.setMinutes(45);

        Shift shift1 = new Shift();
        Shift shift2 = new Shift();

        shift1.setName("shift1");
        shift1.setStartTime(shiftTime);
        shift1.setEndTime(shiftTime);

        shift2.setName("shift2");
        shift2.setStartTime(shiftTime);
        shift2.setEndTime(shiftTime);

        assertEquals("Hash code should be different",
            shift1.hashCode() != shift2.hashCode(), true);
    }

    /**
     * . Tests two types are equal
     */
    @Test(enabled = false)
    public void testEqual() {
        ShiftTime shiftTime = new ShiftTime();
        shiftTime.setHours(0);
        shiftTime.setMinutes(45);

        Shift shift1 = new Shift();
        Shift shift2 = new Shift();

        shift1.setName("shift1");
        shift1.setStartTime(shiftTime);
        shift1.setEndTime(shiftTime);

        shift2.setName("shift1");
        shift2.setStartTime(shiftTime);
        shift2.setEndTime(shiftTime);

        assertEquals("Types should be equal", shift1.equals(shift2), true);
    }

    /**
     * . Tests two types are not equal
     */
    @Test(enabled = false)
    public void testNotEqual() {
        ShiftTime shiftTime = new ShiftTime();
        shiftTime.setHours(0);
        shiftTime.setMinutes(45);

        Shift shift1 = new Shift();
        Shift shift2 = new Shift();

        shift1.setName("shift1");
        shift1.setStartTime(shiftTime);
        shift1.setEndTime(shiftTime);

        // When compared with other object
        Object obj = new Object();

        assertEquals("Objects should not be equal", shift1.equals(obj), false);

        // Name of one shift is null
        shift2.setName(null);
        shift2.setStartTime(shiftTime);
        shift2.setEndTime(shiftTime);

        assertEquals("Types should not be equal", shift1.equals(shift2), false);

        // When compared with different shifts
        shift2.setName("shift2");
        shift2.setStartTime(shiftTime);
        shift2.setEndTime(shiftTime);

        assertEquals("Types should not be equal", shift1.equals(shift2), false);
    }
}
