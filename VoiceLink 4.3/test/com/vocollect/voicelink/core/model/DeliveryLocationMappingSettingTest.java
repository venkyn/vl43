/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Test the model class: DeliveryLocationMappingSetting.
 * 
 * @author bnorthrop
 */
public class DeliveryLocationMappingSettingTest {

    /**
     * Test creating a mapping type object.
     */
    @Test
    public void testEquality() {
        final DeliveryLocationMappingSetting customerNumberSetting = new DeliveryLocationMappingSetting(
            DeliveryLocationMappingType.CustomerNumber);

        final DeliveryLocationMappingSetting customerNumberSetting2 = new DeliveryLocationMappingSetting(
            DeliveryLocationMappingType.CustomerNumber);

        final DeliveryLocationMappingSetting routeSetting = new DeliveryLocationMappingSetting(
            DeliveryLocationMappingType.Route);

        Assert.assertNotSame(customerNumberSetting, routeSetting);
        Assert.assertEquals(customerNumberSetting, customerNumberSetting);
        Assert.assertEquals(customerNumberSetting, customerNumberSetting2);

    }

}
