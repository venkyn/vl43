/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteLabor;
import com.vocollect.voicelink.loading.service.LoadingRouteLaborManager;
import com.vocollect.voicelink.loading.service.LoadingRouteManager;

import java.util.Calendar;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;


/**
 * @author mraj
 *
 */
public class LoadingRouteLaborManagerImplTest extends
    BaseServiceManagerTestCase {

    private OperatorManager operatorManager;
    private LoadingRouteManager loadingRouteManager;
    private LaborManager laborManager; 
    private OperatorLaborManager operatorLaborManager;
    private LoadingRouteLaborManager loadingLaborManager;
    
    private Operator operator;
    private OperatorLabor operLaborRecord;
    private LoadingRoute route;
    private LoadingRouteLabor loadingLabor;
    
    /*
     * (non-Javadoc)
     * 
     * @see
     * com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#
     * initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        // populate db with base data.
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_LoadingRegions.xml", DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_LoadingOperators.xml", DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Core_Locations_data.xml", DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Loading_Route_Stop_data.xml", DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Loading_Container_data.xml", DatabaseOperation.INSERT);
        
        this.operator = operatorManager.get(101L);
        this.route = loadingRouteManager.get(1L);
    }

    /**
     * test
     */
    @Test()
    public void testLoadingLaborWorksGood() {

        Calendar now = Calendar.getInstance();
        try {

            this.laborManager.openOperatorLaborRecord(
                now.getTime(), operator, OperatorLaborActionType.SignOn);
            
            this.laborManager.openOperatorLaborRecord(
                now.getTime(), operator, OperatorLaborActionType.Loading);
            
            this.operLaborRecord = this.operatorLaborManager
                .findOpenRecordByOperatorId(operator.getId());

            this.loadingLaborManager.openLaborRecord(
                now.getTime(), operator, route, operLaborRecord);

            this.loadingLabor = this.loadingLaborManager
                .findOpenRecordByRouteId(route.getId(), operator);
            assertNotNull(loadingLabor);
            
            this.loadingLaborManager.updateLaborRecord(operator, route);
            this.loadingLaborManager.updateLaborRecord(operator, route);
            
            now.add(Calendar.HOUR, 1);
            this.laborManager.closeLoadingLaborRecord(now.getTime(), route, operator);
            this.operatorLaborManager.closeLaborRecord(now.getTime(), operator);
        } catch (Exception e) {
            fail("Caught an exception", e);
        }
    }

    
    /**
     * @return the operatorManager
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    
    /**
     * @param operatorManager the operatorManager to set
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    
    /**
     * @return the operatorLaborManager
     */
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }

    
    /**
     * @param operatorLaborManager the operatorLaborManager to set
     */
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    
    /**
     * @return the loadingLaborManager
     */
    public LoadingRouteLaborManager getLoadingLaborManager() {
        return loadingLaborManager;
    }

    
    /**
     * @param loadingLaborManager the loadingLaborManager to set
     */
    public void setLoadingLaborManager(LoadingRouteLaborManager loadingLaborManager) {
        this.loadingLaborManager = loadingLaborManager;
    }

    
    /**
     * @return the loadingRouteManager
     */
    public LoadingRouteManager getLoadingRouteManager() {
        return loadingRouteManager;
    }

    
    /**
     * @param loadingRouteManager the loadingRouteManager to set
     */
    public void setLoadingRouteManager(LoadingRouteManager loadingRouteManager) {
        this.loadingRouteManager = loadingRouteManager;
    }

    
    /**
     * @return the laborManager
     */
    public LaborManager getLaborManager() {
        return laborManager;
    }

    
    /**
     * @param laborManager the laborManager to set
     */
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    
    
}
