/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.core.service.WorkgroupManager;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRouteIssuance;
import com.vocollect.voicelink.loading.service.LoadingRegionManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.LOADING;

import java.util.Date;
import java.util.List;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Unit test class for LoadingRegionManager.
 * 
 * @author kudupi
 */
@Test(groups = { FAST, LOADING })
public class LoadingRegionManagerImplTest extends
        BaseServiceManagerTestCase {

    private LoadingRegionManager loadingRegionManager;
    private WorkgroupManager workGroupManager;

    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {

    }

    /**
     * @param impl
     *            - implementation of Loading region manager.
     */
    @Test(enabled = false)
    public void setLoadingRegionManager(LoadingRegionManager impl) {
        this.loadingRegionManager = impl;
    }

    /**
     * @param impl
     *            - implementation of selection region manager.
     */
    @Test(enabled = false)
    public void setWorkgroupManager(WorkgroupManager impl) {
        this.workGroupManager = impl;
    }

    /**
     * 
     * @throws Exception
     */
    @AfterClass()
    protected void cleanUp() throws Exception {
        classSetUp();
    }
    
    /**
     * Class-wide setup.
     * @throws Exception any
     */
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }    

    /**
     * @throws Exception
     */
    @Test(enabled = false)
    public void testSave() throws Exception {
        LoadingRegion loadingRegion = new LoadingRegion();
        loadingRegion.setName("Test Loading Region Create Test");
        loadingRegion.setNumber(1);
        loadingRegion.setType(RegionType.Loading);
        loadingRegion.setGoalRate(100);
        loadingRegion.setLoadingRouteIssuance(LoadingRouteIssuance.Manual);
        loadingRegion.setContainerIDDigitsOperSpeaks(5);
        loadingRegion.setTrailerIDDigitsOperSpeaks(5);
        loadingRegion.setMaxConsolidationOfContainers(1);

        loadingRegionManager.save(loadingRegion);
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testRegionAddedToWorkgroup() throws Exception {
        LoadingRegion loadingRegion = new LoadingRegion();
        loadingRegion.setName("Test Loading Region Workgrouped");
        loadingRegion.setNumber(2);
        loadingRegion.setType(RegionType.Loading);
        loadingRegion.setGoalRate(100);
        loadingRegion.setLoadingRouteIssuance(LoadingRouteIssuance.Manual);
        loadingRegion.setContainerIDDigitsOperSpeaks(5);
        loadingRegion.setTrailerIDDigitsOperSpeaks(5);
        loadingRegion.setMaxConsolidationOfContainers(1);

        loadingRegionManager.save(loadingRegion);

        List<Workgroup> workGroups = workGroupManager.listAutoAddWorkgroups();
        for (Workgroup workgroup : workGroups) {
            for (WorkgroupFunction wgf : workgroup.getWorkgroupFunctions()) {
                if (wgf.getTaskFunction().getRegionType() == RegionType.Loading) {
                    assertTrue(wgf.getRegions().size() >= 1,
                            "Regions are not getting created in workgroup");
                }
            }
        }
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testFindRegionByNumber() throws Exception {
        LoadingRegion loadingRegion = new LoadingRegion();
        loadingRegion.setName("Test Loading Region Find");
        loadingRegion.setNumber(3);
        loadingRegion.setType(RegionType.Loading);
        loadingRegion.setGoalRate(100);
        loadingRegion.setLoadingRouteIssuance(LoadingRouteIssuance.Manual);
        loadingRegion.setContainerIDDigitsOperSpeaks(5);
        loadingRegion.setTrailerIDDigitsOperSpeaks(5);
        loadingRegion.setMaxConsolidationOfContainers(1);

        loadingRegionManager.save(loadingRegion);

        LoadingRegion targetRegion = loadingRegionManager.findRegionByNumber(3);

        assertTrue(targetRegion.getName().equals("Test Loading Region Find"),
                "Expected region \"Test Loading Region Workgrouped\" but found "
                        + targetRegion.getName());
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testUniquenessByName() throws Exception {
        LoadingRegion loadingRegion = new LoadingRegion();
        loadingRegion.setName("Test Loading Region Unique Name");
        loadingRegion.setNumber(4);
        loadingRegion.setType(RegionType.Loading);
        loadingRegion.setGoalRate(100);
        loadingRegion.setLoadingRouteIssuance(LoadingRouteIssuance.Manual);
        loadingRegion.setContainerIDDigitsOperSpeaks(5);
        loadingRegion.setTrailerIDDigitsOperSpeaks(5);
        loadingRegion.setMaxConsolidationOfContainers(1);

        loadingRegionManager.save(loadingRegion);

        Long id = loadingRegionManager.uniquenessByName("Test Loading Region Unique Name");

        assertTrue(id != null,
                "Expected region should not be unique, but found otherwise");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testUniquenessByNumber() throws Exception {
        
        LoadingRegion loadingRegion = new LoadingRegion();
        loadingRegion.setName("Test Loading Region Unique Number");
        loadingRegion.setNumber(5);
        loadingRegion.setType(RegionType.Loading);
        loadingRegion.setGoalRate(100);
        loadingRegion.setLoadingRouteIssuance(LoadingRouteIssuance.Manual);
        loadingRegion.setContainerIDDigitsOperSpeaks(5);
        loadingRegion.setTrailerIDDigitsOperSpeaks(5);
        loadingRegion.setMaxConsolidationOfContainers(1);
        
        loadingRegionManager.save(loadingRegion);
        
        Long id = loadingRegionManager.uniquenessByNumber(5);

        assertTrue(id != null,
                "Expected region should not be unique, but found otherwise");
    }
    
    /**
     * @throws Exception
     */
    @Test()
    public void testSummaryQueries() throws Exception {
        LoadingRegion loadingRegion = new LoadingRegion();
        loadingRegion.setName("Test Summary Screen Queries");
        loadingRegion.setNumber(6);
        loadingRegion.setType(RegionType.Loading);
        loadingRegion.setGoalRate(100);
        loadingRegion.setLoadingRouteIssuance(LoadingRouteIssuance.Manual);
        loadingRegion.setContainerIDDigitsOperSpeaks(5);
        loadingRegion.setTrailerIDDigitsOperSpeaks(5);
        loadingRegion.setMaxConsolidationOfContainers(1);
        
        loadingRegionManager.save(loadingRegion);
        int numberOfRegions = loadingRegionManager.getAll().size();
        List<DataObject> objects = null;
        ResultDataInfo rdi = new ResultDataInfo();
        rdi.setQueryArgs(new Object[] { new Date() });
        objects = loadingRegionManager.listLoadingRouteSummary(rdi);
        assertEquals(objects.size(), numberOfRegions, "Loading Route Summary");

        objects = loadingRegionManager.listCurrentWorkSummary(rdi);
        assertEquals(objects.size(), numberOfRegions, "Loading Current Work Summary");
    }    

}
