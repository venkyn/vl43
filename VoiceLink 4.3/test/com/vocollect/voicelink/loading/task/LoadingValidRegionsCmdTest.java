/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Unit test class for Loading Valid Regions Command.
 * 
 * @author kudupi
 */
@Test(groups = { FAST, TASK, CORE })
public class LoadingValidRegionsCmdTest extends TaskCommandTestCase {

    private LoadingValidRegionsCmd cmd;

    /**
     * @return a bean for testing.
     */
    private LoadingValidRegionsCmd getCmdBean() {
        return (LoadingValidRegionsCmd) getBean("cmdPrTaskLUTLoadingValidRegions");
    }

    @Override
    @BeforeClass
    protected void classSetUp() throws Exception {
        super.classSetUp();
    }

    /**
     * Each test setup.
     * 
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void testOperatorNotAuthorized() throws Exception {
        super.classSetUp();
        final String operId = "LoadingCmdOpr";
        String sn = "LoadingCmdSerial";

        Response response;
        signOn(sn, operId);
        // Test Get No break types

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        // Verify the result is empty
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get(ResponseRecord.ERROR_CODE_FIELD),
                Long.toString(TaskErrorCode.NO_REGIONS_DEFINED_FOR_REQUESTED_FUNCTION.getErrorCode()),
                "ErrorCode");
        assertFalse(((String) record.get(ResponseRecord.ERROR_MESSAGE_FIELD)).contains("{"),
                   "error message should not contain curly braces");
        assertEquals(record.get(ResponseRecord.ERROR_MESSAGE_FIELD),
                    "No regions exist for function Loading.  Please notify your supervisor.",
                    "incorrect error message");

        signOff(sn, operId);
    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void testOperatorAuthorized() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_LoadingRegions.xml");
        classSetupInsert("VoiceLinkDataUnitTest_LoadingOperators.xml");

        final String operId = "operRequestLoadingRegionCmd";
        String sn = "LoadingCmdSerial1";

        Response response;
        signOn(sn, operId);
        // Test Get No break types

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        // Verify the result is empty
        List<ResponseRecord> records = response.getRecords();
        assert (records.size() > 0);

    }

    /**
     * @param sn
     *            Serial Number
     * @param operId
     *            Operator ID
     * @throws Exception
     */
    private void signOn(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd(
                "cmdPrTaskLUTCoreConfiguration",
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, locale.toString(), "Default",
                        this.goodCombinedTaskVersion() });

        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId });

        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
    }

    /**
     * @param sn
     *            Serial Number
     * @param operId
     *            Operator ID
     * @throws Exception
     */
    private void signOff(String sn, String operId) throws Exception {

        executeLutCmd("cmdPrTaskLUTCoreSignOff", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId });

    }

}
