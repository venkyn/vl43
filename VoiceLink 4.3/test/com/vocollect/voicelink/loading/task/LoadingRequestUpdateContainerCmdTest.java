/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;


import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.loading.model.LoadingContainer;
import com.vocollect.voicelink.loading.model.LoadingRouteLabor;
import com.vocollect.voicelink.loading.service.LoadingContainerManager;
import com.vocollect.voicelink.loading.service.LoadingRouteLaborManager;
import com.vocollect.voicelink.loading.service.LoadingRouteManager;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.LOADING;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Loading Update Container Command Unit test class.
 * 
 * @author kudupi
 */
@Test(groups = { FAST, TASK, LOADING })
public class LoadingRequestUpdateContainerCmdTest extends TaskCommandTestCase {
    
    private LoadingUpdateContainerCmd cmd;
    
    private LoadingRouteLaborManager loadingLaborManager;
    private OperatorLaborManager operatorLaborManager;
    private OperatorManager operatorManager;
    private LoadingRouteManager loadingRouteManager;
    private LoadingContainerManager loadingContainerManager;
    
    private OperatorLabor operLaborRecord;
    
    private String operId = "operLoadingRequestRoute";
    private String sn = "serialLoadingRequestRoute1";
    
    /**
     * @return a bean for testing.
     */
    private LoadingUpdateContainerCmd getCmdBean() {
        return (LoadingUpdateContainerCmd) getBean("cmdPrTaskLUTLoadingUpdateContainer");
    }
    
    /**
     * Inserts all Loading regions and work authorization data.
     *  
     * @throws Exception - Any Exception
     */
    protected void classSetupLoadingData() throws Exception {
        classSetupInsert("VoiceLinkDataUnitTest_LoadingRegions.xml");
        classSetupInsert("VoiceLinkDataUnitTest_LoadingOperators.xml");
        classSetupInsert("VoiceLinkDataUnitTest_Core_Locations_data.xml");
        classSetupInsert("VoiceLinkDataUnitTest_Loading_Route_Stop_data.xml");
        classSetupInsert("VoiceLinkDataUnitTest_Loading_Container_data.xml");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }
    
    /**
     * Test case for Loading Request Container Task Command.
     * Test case to test the positive scenario.
     * @throws Exception if any.*/
     
    @Test()
    public void testLoadingRequestContainerExecute() throws Exception {
        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        requestLoadingRoute("12345");
        String containerID = "C00000000000001";
        String partial = "0";
        requestLoadingContainer("00001", partial);
        // Calling LoadingRequestContainer Task Command.
        Response response = executeCommand(this.getCmdDateSec(), "L", containerID, "", "12", "12345");
        
        //Second container
        containerID = "C00000000000002";
        requestLoadingContainer("00002", partial);
        response = executeCommand(this.getCmdDateSec(), "L", containerID, "", "13", "12345");
        
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("routeComplete"), Integer.parseInt("0"), "routeComplete");
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
    }
    
    /**
     * Test case for Loading Request Container Task Command verifying if the
     * containers count is updated with every container loaded. Test case to
     * test the positive scenario.
     * @throws Exception if any.
     */
     
    @Test()
    public void testLoadingDynamicContainerCountUpdate() throws Exception {
        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        requestLoadingRoute("12345");
        String containerID = "C00000000000001";
        String partial = "0";
        requestLoadingContainer("00001", partial);
        // Calling LoadingRequestContainer Task Command.
        Response response = executeCommand(this.getCmdDateSec(), "L", containerID, "", "12", "12345");
        LoadingRouteLabor loadingRouteLabor;
        
        //Assert the loaded container count is dynamically updated as it is loaded
        Operator operator = operatorManager.findByIdentifier(operId);
        this.operLaborRecord = this.operatorLaborManager
            .findOpenRecordByOperatorId(operator.getId());
        assertEquals(operLaborRecord.getCount().intValue(), 1);
        loadingRouteLabor = this.loadingLaborManager
            .findOpenRecordByRouteId(1, operator);
        assertEquals(loadingRouteLabor.getContainersLoaded().intValue(), 1);
        
        //Second container
        containerID = "C00000000000002";
        requestLoadingContainer("00002", partial);
        response = executeCommand(this.getCmdDateSec(), "L", containerID, "", "13", "12345");
        
        containerID = "C00000000000003";
        requestLoadingContainer("00003", partial);
        response = executeCommand(this.getCmdDateSec(), "L", containerID, "", "13", "12345");
        
        containerID = "C00000000000004";
        requestLoadingContainer("00004", partial);
        response = executeCommand(this.getCmdDateSec(), "L", containerID, "", "13", "12345");
        
        containerID = "C00000000000005";
        requestLoadingContainer("00005", partial);
        response = executeCommand(this.getCmdDateSec(), "L", containerID, "", "14", "12345");
        
        containerID = "C00000000000006";
        requestLoadingContainer("00006", partial);
        response = executeCommand(this.getCmdDateSec(), "L", containerID, "", "15", "12345");
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("routeComplete"), Integer.parseInt("1"), "routeComplete");
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
        
        //Assert the loaded container count is dynamically updated as it is loaded
        operator = operatorManager.findByIdentifier(operId);
        this.operLaborRecord = this.operatorLaborManager
            .findOpenRecordByOperatorId(operator.getId());
        assertEquals(operLaborRecord.getCount().intValue(), 6);
        loadingRouteLabor = this.loadingLaborManager
            .findOpenRecordByRouteId(1, operator);
        assertEquals(loadingRouteLabor.getContainersLoaded().intValue(), 6);
    }
    
    /**
     * @throws Exception
     */
    @Test()
    public void testLoadingRequestContainerCaptureLoadPosition() throws Exception {
        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        requestLoadingRoute("12345");
        
        
        // Calling LoadingRequestContainer Task Command with position left.
        String containerNumber = "C00000000000001";
        long containerId = 1L; 
        String partial = "0";
        requestLoadingContainer("00001", partial);
        Response response = null;
        response = executeCommand(this.getCmdDateSec(), "L", containerNumber, "", "left", "12345");
        Integer expected = -1;
        LoadingContainer container = this.loadingContainerManager.get(containerId);
        assertEquals(container.getLoadPosition(), expected);
        assertEquals(response.getRecords().size(), 1);
        
        
        // Calling LoadingRequestContainer Task Command with position right.
        containerNumber = "C00000000000002";
        containerId = 2L; 
        partial = "0";
        requestLoadingContainer("00002", partial);
        response = executeCommand(this.getCmdDateSec(), "L", containerNumber, "", "right", "12345");
        expected = -2;
        container = this.loadingContainerManager.get(containerId);
        assertEquals(container.getLoadPosition(), expected);
        
        
        // Calling LoadingRequestContainer Task Command with two digit position.
        containerNumber = "C00000000000003";
        containerId = 3L; 
        partial = "0";
        requestLoadingContainer("00003", partial);
        response = executeCommand(this.getCmdDateSec(), "L", containerNumber, "", "00", "12345");
        expected = 0;
        container = this.loadingContainerManager.get(containerId);
        assertEquals(container.getLoadPosition(), expected);
        
        // Calling LoadingRequestContainer Task Command with no load position.
        containerNumber = "C00000000000004";
        containerId = 4L; 
        partial = "0";
        requestLoadingContainer("00004", partial);
        response = executeCommand(this.getCmdDateSec(), "L", containerNumber, "", "", "12345");
        expected = 0;
        container = this.loadingContainerManager.get(containerId);
        assertNull(container.getLoadPosition());
    }
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupLoadingData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }
    
    /**
     * 
     * @throws Exception
     */
    private void validLoadingRegions() throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingValidRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
    }
    
    /**
     * 
     * @param region - the region
     * @throws Exception
     */
    private void requestSingleRegion(String region) throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRequestRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, region});
    }
    
    /**
     * 
     * @throws Exception
     */
    private void requestRegionConfigurations() throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRegionConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
    }
    
    /**
     * @param routeNumber String.
     * @throws Exception
     */
    private void requestLoadingRoute(String routeNumber) throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRequestRoute", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, routeNumber});
    }
    
    /**
     * @param containerID String.
     * @param partial String.
     * @throws Exception
     */
    private void requestLoadingContainer(String containerID, String partial) throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRequestContainer", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, containerID, partial});
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param status - String.
     * @param containerID - containerID the operator requests. 
     * @param masterContainerID - String.
     * @param capturePosition - String.
     * @param trailerID - String.
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String status, String containerID, 
            String masterContainerID, String capturePosition, String trailerID) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId, status, containerID, masterContainerID, 
                    capturePosition, trailerID}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }

    
    /**
     * @return the loadingLaborManager
     */
    @Test(enabled = false)
    public LoadingRouteLaborManager getLoadingLaborManager() {
        return loadingLaborManager;
    }

    /**
     * @param loadingLaborManager the loadingLaborManager to set
     */
    @Test(enabled = false)
    public void setLoadingLaborManager(LoadingRouteLaborManager loadingLaborManager) {
        this.loadingLaborManager = loadingLaborManager;
    }

    /**
     * Getter for the operatorLaborManager property.
     * 
     * @return OperatorLaborManager value of the property
     */
    @Test(enabled = false)
    public OperatorLaborManager getOperatorLaborManager() {
        return this.operatorLaborManager;
    }

    /**
     * Setter for the operatorLaborManager property.
     * 
     * @param operatorLaborManager
     *            the new operatorLaborManager value
     */
    @Test(enabled = false)
    public void setOperatorLaborManager(
            OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    
    /**
     * @return the operatorManager
     */
    @Test(enabled = false)
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    
    /**
     * @param operatorManager the operatorManager to set
     */
    @Test(enabled = false)
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    
    /**
     * @return the loadingRouteManager
     */
    @Test(enabled = false)
    public LoadingRouteManager getLoadingRouteManager() {
        return loadingRouteManager;
    }

    
    /**
     * @param loadingRouteManager the loadingRouteManager to set
     */
    @Test(enabled = false)
    public void setLoadingRouteManager(LoadingRouteManager loadingRouteManager) {
        this.loadingRouteManager = loadingRouteManager;
    }

    /**
     * @return the loadingContainerManager
     */
    @Test(enabled = false)
    public LoadingContainerManager getLoadingContainerManager() {
        return loadingContainerManager;
    }

    /**
     * @param loadingContainerManager the loadingContainerManager to set
     */
    @Test(enabled = false)
    public void setLoadingContainerManager(
            LoadingContainerManager loadingContainerManager) {
        this.loadingContainerManager = loadingContainerManager;
    }

    
}
