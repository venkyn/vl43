/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;


import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.LOADING;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Loading Request Container Command Unit test class.
 * 
 * @author kudupi
 */
@Test(groups = { FAST, TASK, LOADING })
public class LoadingRequestContainerCmdTest extends TaskCommandTestCase {
    
    private LoadingRequestContainerCmd cmd;
    
    private String operId = "operLoadingRequestContainer";
    private String sn = "serialLoadingRequestContainer";
    /**
     * @return a bean for testing.
     */
    private LoadingRequestContainerCmd getCmdBean() {
        return (LoadingRequestContainerCmd) getBean("cmdPrTaskLUTLoadingRequestContainer");
    }
    
    /**
     * Inserts all Loading regions and work authorization data.
     *  
     * @throws Exception - Any Exception
     */
    protected void classSetupLoadingData() throws Exception {
        classSetupInsert("VoiceLinkDataUnitTest_LoadingRegions.xml");
        classSetupInsert("VoiceLinkDataUnitTest_LoadingOperators.xml");
        classSetupInsert("VoiceLinkDataUnitTest_Core_Locations_data.xml");
        classSetupInsert("VoiceLinkDataUnitTest_Loading_Route_Stop_data.xml");
        classSetupInsert("VoiceLinkDataUnitTest_Loading_Container_data.xml");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }
    
    /**
     * Test case for Loading Request Container Task Command.
     * Test case to test the positive scenario.
     * @throws Exception if any.*/
     
    @Test()
    public void testLoadingRequestContainerExecute() throws Exception {
        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        requestLoadingRoute("12345");
        
        String containerID = "00001";
        String partial = "0";
        
        // Calling LoadingRequestContainer Task Command.
        Response response = executeCommand(this.getCmdDateSec(), containerID, partial);
        
        //second container
        String spokenContainerID = "00002";
        String containerNumber = "C00000000000002";
        response = executeCommand(this.getCmdDateSec(), spokenContainerID, partial);
        
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 6, "Containers returned");
        ResponseRecord record = records.get(1);
        assertEquals(record.get("routeID"), Long.parseLong("1"), "routeID");
        assertEquals(record.get("containerNumber"), containerNumber, "containerNumber");
        assertEquals(record.get("partialContainerNumber"), spokenContainerID, "partialContainerNumber");
        assertEquals(record.get("status"), "N", "status");
        assertEquals(record.get("captureLoadPosition"), Long.parseLong("1"), "captureLoadPosition");
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
    }
    
    /**
     * Test case for Loading Request Container Task Command.
     * Test case to test the error scenario.
     * @throws Exception if any.
     */
    @Test()
    public void testLoadingRequestContainerError() throws Exception {
        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        requestLoadingRoute("12345");
        
        String containerID = "10005";
        String partial = "0";

        // Calling LoadingRequestContainer task command.
        Response response = executeCommand(this.getCmdDateSec(), containerID, partial);
        
        //Verify Error result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "Container returned");
        
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "1403", "errorCode");
        assertEquals(record.get("errorMessage"), "No container <SPELL>10005</SPELL> found in route 12345", 
                "errorMessage");
    }
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupLoadingData();
        
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }
    
    /**
     * 
     * @throws Exception
     */
    private void validLoadingRegions() throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingValidRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
    }
    
    /**
     * 
     * @param region - the region
     * @throws Exception
     */
    private void requestSingleRegion(String region) throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRequestRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, region});
    }
    
    /**
     * 
     * @throws Exception
     */
    private void requestRegionConfigurations() throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRegionConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
    }
    
    /**
     * @param routeNumber String.
     * @throws Exception
     */
    private void requestLoadingRoute(String routeNumber) throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRequestRoute", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, routeNumber});
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param containerID - containerID the operator requests. 
     * @param partial - partial String.         
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String containerID, String partial) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId, containerID, partial}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }

}
