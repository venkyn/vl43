/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.LOADING;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Loading Request Route Command Unit test class.
 * 
 * @author kudupi
 */
@Test(groups = { FAST, TASK, LOADING })
public class LoadingRequestRouteCmdTest extends TaskCommandTestCase {
    
    private LoadingRequestRouteCmd cmd;
    
    private String operId = "operLoadingRequestRoute";
    private String sn = "serialLoadingRequestRoute2";
    
    private String operId1 = "operLoadingRequestRoute1";
    private String sn1 = "serialLoadingRequestRoute21";
    
    /**
     * @return a bean for testing.
     */
    private LoadingRequestRouteCmd getCmdBean() {
        return (LoadingRequestRouteCmd) getBean("cmdPrTaskLUTLoadingRequestRoute");
    }

    
    /**
     * Inserts all Loading regions and work authorization data.
     *  
     * @throws Exception - Any Exception
     */
    protected void classSetupLoadingData() throws Exception {
        classSetupInsert("VoiceLinkDataUnitTest_LoadingRegions.xml");
        classSetupInsert("VoiceLinkDataUnitTest_LoadingOperators.xml");
        classSetupInsert("VoiceLinkDataUnitTest_Core_Locations_data.xml");
        classSetupInsert("VoiceLinkDataUnitTest_Loading_Route_Stop_data.xml");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }
    
    /**
     * Test case for Loading Request Route Task Command.
     * @throws Exception if any.*/
     
    @Test()
    public void testLoadingRequestRouteMultipleExecute() throws Exception {
        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        
        // Testing Correct Response.
        // Testing using Automatic Issuance.
        String routeNumber = "12346";

        // Calling LoadingRequestRoute task command.
        Response response = executeCommand(this.getCmdDateSec(), routeNumber, sn, operId);
        
        response = executeCommand(this.getCmdDateSec(), routeNumber, sn1, operId1);
        
        routeNumber = "12345";
        response = executeCommand(this.getCmdDateSec(), routeNumber, sn, operId);
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "routes returned");
        
        ResponseRecord record = records.get(0);
        assertEquals(record.get("routeID"), Long.parseLong("1"), "routeID");
        assertEquals(record.get("routeNumber"), "12345", "routeNumber");
        assertEquals(record.get("captureTrailerID"), "1", "captureTrailerID");
        assertEquals(record.get("doorNumber"), "11", "doorNumber");
        assertEquals(record.get("doorCheckDigits"), "", "doorCheckDigits");
        assertEquals(record.get("summaryPrompt"), "Route 12345 has 0 stops 0 remaining", "summaryPrompt");
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
    }
    
    /**
     * Test case for Loading Request Route Task Command.
     * @throws Exception if any.*/
     
    @Test()
    public void testLoadingRequestRouteExecute() throws Exception {
        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        
        // Testing Correct Response.
        // Testing using Automatic Issuance.
        String routeNumber = "12346";

        // Calling LoadingRequestRoute task command.
        Response response = executeCommand(this.getCmdDateSec(), routeNumber, sn, operId);
        
        routeNumber = "12345";
        response = executeCommand(this.getCmdDateSec(), routeNumber, sn, operId);
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "routes returned");
        
        ResponseRecord record = records.get(0);
        assertEquals(record.get("routeID"), Long.parseLong("1"), "routeID");
        assertEquals(record.get("routeNumber"), "12345", "routeNumber");
        assertEquals(record.get("captureTrailerID"), "1", "captureTrailerID");
        assertEquals(record.get("doorNumber"), "11", "doorNumber");
        assertEquals(record.get("doorCheckDigits"), "", "doorCheckDigits");
        assertEquals(record.get("summaryPrompt"), "Route 12345 has 0 stops 0 remaining", "summaryPrompt");
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
    }
    
    /**
     * Test case for Loading Request Route Task Command.
     * @throws Exception if any.
     */
    @Test()
    public void testLoadingRequestRouteError() throws Exception {
        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        
        // Testing Manual Issuance with wrong Route Number.
        // Error Code 1401 and Error Message "No loading route found." must be returned.
        String routeNumber = "1234";

        // Calling LoadingRequestRoute task command.
        Response response = executeCommand(this.getCmdDateSec(), routeNumber, sn, operId);
        
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "routes returned");
        
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "1401", "errorCode");
        assertEquals(record.get("errorMessage"), "Route 1234 not found or already complete", "errorMessage");
    }
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupLoadingData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(new Date(
                this.getCmdDateSec().getTime())), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(new Date(
                this.getCmdDateSec().getTime())), sn, operId});
        executeLutCmd(
            "cmdPrTaskLUTCoreSignOn", new String[] {
                makeStringFromDate(new Date(
                    this.getCmdDateSec().getTime())), sn, operId,
                "1234" });
        
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
                new String[] {makeStringFromDate(new Date(
                    this.getCmdDateSec().getTime())), sn1, operId1, 
                locale.toString(), "Default", this.goodCombinedTaskVersion()});
            executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
                new String[] {makeStringFromDate(new Date(
                    this.getCmdDateSec().getTime())), sn1, operId1});
            executeLutCmd(
                "cmdPrTaskLUTCoreSignOn", new String[] {
                    makeStringFromDate(new Date(
                        this.getCmdDateSec().getTime())), sn1, operId1,
                    "1234" });
    }
    
    /**
     * 
     * @throws Exception
     */
    private void validLoadingRegions() throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingValidRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        
        executeLutCmd("cmdPrTaskLUTLoadingValidRegions", 
                new String[] {makeStringFromDate(this.getCmdDateSec()), sn1, operId1});
    }
    
    /**
     * 
     * @param region - the region
     * @throws Exception
     */
    private void requestSingleRegion(String region) throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRequestRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, region});
        
        executeLutCmd("cmdPrTaskLUTLoadingRequestRegion", 
                new String[] {makeStringFromDate(this.getCmdDateSec()), sn1, operId1, region});
    }
    
    /**
     * 
     * @throws Exception
     */
    private void requestRegionConfigurations() throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRegionConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        
        executeLutCmd("cmdPrTaskLUTLoadingRegionConfiguration", 
                new String[] {makeStringFromDate(this.getCmdDateSec()), sn1, operId1});
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param routeNumber - routeNumber the operator requests. 
     *          Its "" (blank) in case of automatic issuance.
     * @param serialNumber - serialNumber of the device.
     * @param operatorId - operatorId String
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate, String routeNumber
            , String serialNumber, String operatorId) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), serialNumber, operatorId, routeNumber}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
    
}
