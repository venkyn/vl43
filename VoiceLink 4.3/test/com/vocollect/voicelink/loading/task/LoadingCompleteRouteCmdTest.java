/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;

import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.loading.model.LoadingRouteLabor;
import com.vocollect.voicelink.loading.service.LoadingRouteLaborManager;
import com.vocollect.voicelink.loading.service.LoadingRouteManager;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author mraj
 * 
 */
public class LoadingCompleteRouteCmdTest extends TaskCommandTestCase {
    private LoadingCompleteRouteCmd cmd;

    private LoadingRouteLaborManager loadingLaborManager;
    private OperatorLaborManager operatorLaborManager;
    private OperatorManager operatorManager;
    private LoadingRouteManager loadingRouteManager;
    private OperatorLabor operLaborRecord;

    private String operId = "operLoadingRequestRoute";

    private String sn = "serialLoadingRequestRoute";

    private int timeBumpFactor = 1;

    /**
     * @return a bean for testing.
     */
    private LoadingCompleteRouteCmd getCmdBean() {
        return (LoadingCompleteRouteCmd) getBean("cmdPrTaskLUTLoadingCompleteRoute");
    }

    /**
     * Inserts all Loading regions and work authorization data.
     * 
     * @throws Exception
     *             - Any Exception
     */
    protected void classSetupLoadingData() throws Exception {
        classSetupInsert("VoiceLinkDataUnitTest_LoadingRegions.xml");
        classSetupInsert("VoiceLinkDataUnitTest_LoadingOperators.xml");
        classSetupInsert("VoiceLinkDataUnitTest_Core_Locations_data.xml");
        classSetupInsert("VoiceLinkDataUnitTest_Loading_Route_Stop_data.xml");
        classSetupInsert("VoiceLinkDataUnitTest_Loading_Container_data.xml");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }

    /**
     * Test case for Loading Request Route Task Command.
     * 
     * @throws Exception
     *             if any.
     */

    @Test()
    public void testLoadingRequestRouteExecute() throws Exception {
        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        requestLoadingRoute("12345");
        requestLoadingContainer("00001", "0");
        updateLoadingContainer("C00000000000001");
        takeABreak("1", "0", "0");
        Thread.sleep(1000);
        takeABreak("1", "1", "0");
        updateLoadingContainer("C00000000000002");
        // Testing Correct Response.
        // Testing using Automatic Issuance.
        String routeNumber = "1";

        // Calling LoadingCompleteRoute task command.
        Response response = executeCommand(this.getBumpedDateTime(),
                routeNumber);

        // Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "routes returned");

        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "1404", "errorCode");
        assertEquals(
                record.get("errorMessage"),
                "Not all containers loaded for route 12345. Route 12345 cannot be completed",
                "errorMessage");
    }

    /**
     * Test case for Loading Request Route Task Command.
     * 
     * @throws Exception
     *             if any.
     */

    @Test()
    public void testLoadingRequestRoutePowerOffScenario() throws Exception {
        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        requestLoadingRoute("12345");
        requestLoadingContainer("00001", "0");
        updateLoadingContainer("C00000000000001");

        // Power off OR Pull the battery and replace
        executeLutCmd(
                "cmdPrTaskLUTCoreConfiguration",
                new String[] { makeStringFromDate(this.getBumpedDateTime()),
                        sn, operId, Locale.US.toString(), "Default",
                        this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", new String[] {
                makeStringFromDate(this.getBumpedDateTime()), sn, operId });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
                makeStringFromDate(this.getBumpedDateTime()), sn, operId,
                "1234" });
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        requestLoadingRoute("12345");
        requestLoadingContainer("00002", "0");
        updateLoadingContainer("C00000000000002");

        // Testing Correct Response.
        // Testing using Automatic Issuance.
        String routeNumber = "1";

        // Calling LoadingCompleteRoute task command.
        Response response = executeCommand(this.getBumpedDateTime(),
                routeNumber);

        // Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "routes returned");

        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "1404", "errorCode");
        assertEquals(
                record.get("errorMessage"),
                "Not all containers loaded for route 12345. Route 12345 cannot be completed",
                "errorMessage");
    }

    /**
     * Test case for Loading Request Route Task Command when route is changed.
     * 
     * @throws Exception
     *             if any.
     */

    @Test()
    public void testLoadingLaborRecordCloseOnChangeRoute() throws Exception {
        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        requestLoadingRoute("12345");
        requestLoadingContainer("00001", "0");
        updateLoadingContainer("C00000000000001");

        // Obtain operator and loading labor record id
        String routeId = "1"; // Should be routeNumber
        Operator operator = operatorManager.findByIdentifier(
            operId);
        this.operLaborRecord = this.operatorLaborManager
                .findOpenRecordByOperatorId(operator.getId());
        LoadingRouteLabor loadingRouteLabor = this.loadingLaborManager
                .findOpenRecordByRouteId(Long.valueOf(routeId), operator);

        // Change loading route
        requestLoadingRoute("12346");

        // Now again get the records by id already fetched
        // Testing Correct Response.
        assertNull(this.operatorLaborManager.get(operLaborRecord.getId())
                .getEndTime());
        assertNotNull(loadingLaborManager.get(loadingRouteLabor.getId())
                .getEndTime());

        requestLoadingRoute("12345");
        requestLoadingContainer("00002", "0");

        // Load the remaining container to complete route
        updateLoadingContainer("C00000000000002");
        updateLoadingContainer("C00000000000003");
        updateLoadingContainer("C00000000000004");
        updateLoadingContainer("C00000000000005");
        updateLoadingContainer("C00000000000006");
        // Calling LoadingCompleteRoute task command.
        Response response = executeCommand(this.getBumpedDateTime(), routeId);

        // Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "routes returned");

        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
    }

    /**
     * Test case for Loading Request Route Task Command when region is changed
     * in between.
     * 
     * @throws Exception
     *             if any.
     */
    @Test()
    public void testLoadingLaborRecordCloseOnChangeRegion() throws Exception {
        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        requestLoadingRoute("12345");
        requestLoadingContainer("00001", "0");
        updateLoadingContainer("C00000000000001");

        // Obtain operator and loading labor record id
        String routeId = "1"; // Should be routeNumber
        Operator operator = operatorManager.findByIdentifier(
            operId);
        this.operLaborRecord = this.operatorLaborManager
                .findOpenRecordByOperatorId(operator.getId());
        LoadingRouteLabor loadingRouteLabor = this.loadingLaborManager
                .findOpenRecordByRouteId(Long.valueOf(routeId), operator);

        // Change region to a different one
        validLoadingRegions();
        requestSingleRegion("102");
        requestRegionConfigurations();

        // Now again get the records by id already fetched
        // Testing Correct Response.
        assertNotNull(this.operatorLaborManager.get(operLaborRecord.getId())
                .getEndTime());
        assertNotNull(loadingLaborManager.get(loadingRouteLabor.getId())
                .getEndTime());

        // Change region to original
        validLoadingRegions();
        requestSingleRegion("101");
        requestRegionConfigurations();
        requestLoadingRoute("12345");
        requestLoadingContainer("00002", "0");
        // Load the remaining container to complete route
        updateLoadingContainer("C00000000000002");
        // Calling LoadingCompleteRoute task command.
        Response response = executeCommand(this.getBumpedDateTime(), routeId);

        // Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "routes returned");

        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "1404", "errorCode");
        assertEquals(
                record.get("errorMessage"),
                "Not all containers loaded for route 12345. Route 12345 cannot be completed",
                "errorMessage");
    }

    /**
     * Initializes the database by calling task commands that need to be called
     * prior to task command in unit test.
     * 
     * @throws Exception
     *             - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;

        // Clear Database
        classSetUp();
        classSetupCore();
        classSetupLoadingData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", new String[] {
                makeStringFromDate(this.getBumpedDateTime()), sn, operId,
                locale.toString(), "Default", this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", new String[] {
                makeStringFromDate(this.getBumpedDateTime()), sn, operId });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
                makeStringFromDate(this.getBumpedDateTime()), sn, operId,
                "1234" });
    }

    /**
     * 
     * @throws Exception
     */
    private void validLoadingRegions() throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingValidRegions", new String[] {
                makeStringFromDate(this.getBumpedDateTime()), sn, operId });
    }

    /**
     * 
     * @param region
     *            - the region
     * @throws Exception
     */
    private void requestSingleRegion(String region) throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRequestRegion", new String[] {
                makeStringFromDate(this.getBumpedDateTime()), sn, operId,
                region });
    }

    /**
     * 
     * @throws Exception
     */
    private void requestRegionConfigurations() throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRegionConfiguration", new String[] {
                makeStringFromDate(this.getBumpedDateTime()), sn, operId });
    }

    /**
     * @param routeNumber
     *            String.
     * @throws Exception
     */
    private void requestLoadingRoute(String routeNumber) throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRequestRoute", new String[] {
                makeStringFromDate(this.getBumpedDateTime()), sn, operId,
                routeNumber });
    }

    /**
     * @param containerID
     *            String.
     * @param partial
     *            String.
     * @throws Exception
     */
    private void requestLoadingContainer(String containerID, String partial)
            throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRequestContainer", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                containerID, partial });
    }

    /**
     * @param containerNumber
     *            String.
     * @throws Exception
     */
    private void updateLoadingContainer(String containerNumber)
            throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingUpdateContainer", new String[] {
                makeStringFromDate(this.getBumpedDateTime()), sn, operId, "L",
                containerNumber, "", "22", "123456789" });
    }

    /**
     * method
     * 
     * @return changed time
     */
    private Date getBumpedDateTime() {
        return new Date(this.getCmdDateSec().getTime()
                + (timeBumpFactor++ * 1000));
    }

    /**
     * 
     * @param breakType
     *            - type of break, battery change, lunch, etc
     * @param startEndFlag
     *            - 0 to start break. 1 to end break
     * @param description
     *            - not used
     * @throws Exception
     */
    private void takeABreak(String breakType, String startEndFlag,
            String description) throws Exception {

        executeLutCmd("cmdPrTaskODRCoreSendBreakInfo", new String[] {
                makeStringFromDate(this.getBumpedDateTime()), sn, operId,
                breakType, startEndFlag, description });
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate
     *            - date for command
     * @param routeNumber
     *            - routeNumber the operator requests. Its "" (blank) in case of
     *            automatic issuance.
     * @return - return a response from command
     * @throws Exception
     *             - all exceptions
     */
    private Response executeCommand(Date cmdDate, String routeNumber)
            throws Exception {
        this.cmd = getCmdBean();
        // Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(cmdDate), sn, operId,
                        routeNumber }, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }

    /**
     * @return the loadingLaborManager
     */
    @Test(enabled = false)
    public LoadingRouteLaborManager getLoadingLaborManager() {
        return loadingLaborManager;
    }

    /**
     * @param loadingLaborManager
     *            the loadingLaborManager to set
     */
    @Test(enabled = false)
    public void setLoadingLaborManager(
            LoadingRouteLaborManager loadingLaborManager) {
        this.loadingLaborManager = loadingLaborManager;
    }

    /**
     * Getter for the operatorLaborManager property.
     * 
     * @return OperatorLaborManager value of the property
     */
    @Test(enabled = false)
    public OperatorLaborManager getOperatorLaborManager() {
        return this.operatorLaborManager;
    }

    /**
     * Setter for the operatorLaborManager property.
     * 
     * @param operatorLaborManager
     *            the new operatorLaborManager value
     */
    @Test(enabled = false)
    public void setOperatorLaborManager(
            OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    /**
     * @return the operatorManager
     */
    @Test(enabled = false)
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * @param operatorManager
     *            the operatorManager to set
     */
    @Test(enabled = false)
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * @return the loadingRouteManager
     */
    @Test(enabled = false)
    public LoadingRouteManager getLoadingRouteManager() {
        return loadingRouteManager;
    }

    /**
     * @param loadingRouteManager
     *            the loadingRouteManager to set
     */
    @Test(enabled = false)
    public void setLoadingRouteManager(LoadingRouteManager loadingRouteManager) {
        this.loadingRouteManager = loadingRouteManager;
    }
}
