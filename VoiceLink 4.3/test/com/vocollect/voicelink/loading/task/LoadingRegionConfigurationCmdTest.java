/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;

import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.LOADING;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Loading Region Configuration Command Unit test class.
 * 
 * @author kudupi
 */
@Test(groups = { FAST, TASK, LOADING })
public class LoadingRegionConfigurationCmdTest extends TaskCommandTestCase {
    
    static final Comparator<OperatorLabor> OPERATOR_LABOR_ID = 
        new Comparator<OperatorLabor>() {
        public int compare(OperatorLabor o1, OperatorLabor o2) {
            return o1.getId().compareTo(o2.getId());
        }
    };
    
    private LoadingRegionConfigurationCmd cmd;
    private String operId = "operLoadingRegionConfiguration";
    private String sn = "serialLoadingRegionConfiguration";

    /**
     * @return a bean for testing.
     */
    private LoadingRegionConfigurationCmd getCmdBean() {
        return (LoadingRegionConfigurationCmd) 
            getBean("cmdPrTaskLUTLoadingRegionConfiguration");
    }
    
    /**
     * Inserts all Loading regions and work authorization data.
     *  
     * @throws Exception - Any Exception
     */
    protected void classSetupLoadingRegions() throws Exception {
        classSetupInsert("VoiceLinkDataUnitTest_LoadingRegions.xml");
        classSetupInsert("VoiceLinkDataUnitTest_LoadingOperators.xml");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }
     
    /**
     * request a single region and verify result.
     * @throws Exception any
     */
    @Test()
    public void testRequestSingleRegionExecute() throws Exception {

        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        //Test standard response for a single region sign-on
        Response response = executeCommand(this.getCmdDateSec());

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "regions returned");
        ResponseRecord record = records.get(0);
        
        assertEquals(record.get("regionNumber"), 101, "regionNumber");
        assertEquals(record.get("regionName"), "Loading region 101", "regionName");
        assertEquals(record.get("loadingRouteIssuance"), 1, "loadingRouteIssuance");
        assertEquals(record.get("containerIDDigitsOperSpeaks"), 5, "containerIDDigitsOperSpeaks");
        assertEquals(record.get("trailerIDDigitsOperSpeaks"), 5, "trailerIDDigitsOperSpeaks");
        assertEquals(record.get("maxConsolidationOfContainers"), 1, "maxConsolidationOfContainers");
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
        
    }
    
    /**
     * Test case to show that we cannot request 2 regions.
     * @throws Exception any
     */
    @Test()
    public void testRequestTwoRegionsExecute() throws Exception {

        initialize();
        validLoadingRegions();
        requestSingleRegion("101");
        requestSingleRegion("102");

        //Test standard response for two region sign-on
        Response response = executeCommand(this.getCmdDateSec());

        //Verify that we got all 2 regions.
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "regions returned"); 
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "errorCode");
        assertEquals(record.get("errorMessage"), "", "errorMessage");
    }    
    
    
    /**
     * No regions are signed into but a request for 
     * region configuration has been made.
     * @throws Exception any
     */
    public void testRequestNoRegions() throws Exception {

        initialize();
        validLoadingRegions();
        //Test error is thrown for regions no longer available
        Response response = executeCommand(this.getCmdDateSec());
        
        //Verify Results
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "records returned");
        ResponseRecord record = records.get(0);
        
        assertEquals(Long.parseLong(record.getErrorCode()), 
                     TaskErrorCode.REGIONS_NO_LONGER_VALID.getErrorCode(), 
                     "errorCode");
    }
    

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupLoadingRegions();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }

    /**
     * 
     * @throws Exception
     */
    private void validLoadingRegions() throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingValidRegions", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
    }
    
    /**
     * 
     * @param region - the region
     * @throws Exception
     */
    private void requestSingleRegion(String region) throws Exception {
        executeLutCmd("cmdPrTaskLUTLoadingRequestRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, region});
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }

}
