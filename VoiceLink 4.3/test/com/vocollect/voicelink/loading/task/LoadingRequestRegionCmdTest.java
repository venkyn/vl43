/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.LOADING;

import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Loading Request Region Command Unit test class.
 * 
 * @author kudupi
 */
@Test(groups = { FAST, TASK, LOADING })
public class LoadingRequestRegionCmdTest extends TaskCommandTestCase {
    
    private LoadingRequestRegionCmd cmd;

    /**
     * @return a bean for testing.
     */
    private LoadingRequestRegionCmd getCmdBean() {
        return (LoadingRequestRegionCmd) getBean("cmdPrTaskLUTLoadingRequestRegion");
    }

    /**
     * 
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_LoadingRegions.xml");
        classSetupInsert("VoiceLinkDataUnitTest_LoadingOperators.xml");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.task.BreakTypeCmd#testExecute()}.
     * 
     * @throws Exception
     *             any
     */
    @Test()
    public void testExecute() throws Exception {
        final String operId = "LoadingCmdOpr";

        // ------------------------------------------------------------------
        // Verify operator can select a region
        // ------------------------------------------------------------------
        String sn = "LoadingCmdSerial";
        String regionNumber = "101";

        Response response;
        initialize(sn, operId);

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, regionNumber }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        // Verify the result is 0
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 0);
        
        // ------------------------------------------------------------------
        // Verify operator cannot select a region that is not present in the
        // system
        // ------------------------------------------------------------------
        regionNumber = "104";

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, regionNumber }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()),
                TaskErrorCode.INVALID_REGION_NUMBER.getErrorCode());  
        
        // ----------------------------------------------------------------------
        // We saw the Task Command task is sending zero in the region field and
        // we didn't detect that, this is to test we are detecting that.
        // ----------------------------------------------------------------------
        regionNumber = "0";

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, regionNumber }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        // Verify the result is error 1304
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()),
                TaskErrorCode.INVALID_REGION_NUMBER.getErrorCode());
    }
  
    /**
     * Initializes the database by calling task commands that need to be called
     * prior to task command in unit test.
     * 
     * @param sn
     *            - Serial Number
     * @param operId
     *            - Operator
     * @throws Exception
     *             - Exception
     */
    private void initialize(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd(
                "cmdPrTaskLUTCoreConfiguration",
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, locale.toString(), "Default",
                        this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
    }    

}
