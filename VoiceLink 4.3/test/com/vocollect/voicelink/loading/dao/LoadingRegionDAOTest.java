/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.dao;

import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRouteIssuance;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.List;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

/**
 * Unit Test class for LoadingRegionDAO methods.
 * 
 * @author kudupi
 */
@Test(groups = { FAST, DAO })
public class LoadingRegionDAOTest extends VoiceLinkDAOTestCase {

    private LoadingRegionDAO dao = null;

    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();

        for (int i = 1; i < 5; i++) {
            LoadingRegion loadingRegion = new LoadingRegion();
            loadingRegion.setName("Test Loading Region " + i);
            loadingRegion.setNumber(i);
            loadingRegion.setType(RegionType.Loading);
            loadingRegion.setGoalRate(100);
            loadingRegion.setDescription("Test Loading Region " + i);
            loadingRegion.setLoadingRouteIssuance(LoadingRouteIssuance.Manual);
            loadingRegion.setContainerIDDigitsOperSpeaks(5);
            loadingRegion.setTrailerIDDigitsOperSpeaks(5);
            loadingRegion.setMaxConsolidationOfContainers(1);
            dao.save(loadingRegion);
        }
    }

    /**
     * 
     * @throws Exception
     */
    @AfterMethod()
    protected void cleanUp() throws Exception {
        classSetUp();
    }
    
    /**
     * Class-wide setup.
     * @throws Exception any
     */
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    } 

    /**
     * @param loadingRegionDAO
     *            the user data access object
     */
    @Test(enabled = false)
    public void setLoadingRegionDAO(LoadingRegionDAO loadingRegionDAO) {
        this.dao = loadingRegionDAO;
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testCreate() throws Exception {
        LoadingRegion loadingRegion = new LoadingRegion();
        loadingRegion.setName("Test Loading Region Create Test");
        loadingRegion.setNumber(100);
        loadingRegion.setType(RegionType.Loading);
        loadingRegion.setGoalRate(100);
        loadingRegion.setLoadingRouteIssuance(LoadingRouteIssuance.Manual);
        loadingRegion.setContainerIDDigitsOperSpeaks(5);
        loadingRegion.setTrailerIDDigitsOperSpeaks(5);
        loadingRegion.setMaxConsolidationOfContainers(1);
        dao.save(loadingRegion);
        List<LoadingRegion> fetchedRegions = dao.getAll();
        boolean regionFound = false;
        for (LoadingRegion fetchedRegion : fetchedRegions) {
            if (fetchedRegion.getName().equals("Test Loading Region Create Test")) {
                regionFound = true;
                break;
            }
        }
        assertTrue(regionFound,
                "Region \"Test Loading Region Create Test\" not created");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testRead() throws Exception {
        List<LoadingRegion> fetchedRegions = dao.getAll();

        LoadingRegion targetRegion = null;
        for (LoadingRegion fetchedRegion : fetchedRegions) {
            if (fetchedRegion.getName().equals("Test Loading Region 1")) {
                targetRegion = fetchedRegion;
                break;
            }
        }

        assertTrue(
                targetRegion != null,
                "Region \"Test Loading Region 1\" not found, possibly \"onSetUp\" method didn't create");

        LoadingRegion fetchedRegionById = dao.get(targetRegion.getId());

        assertTrue(fetchedRegionById.getLoadingRouteIssuance().equals(LoadingRouteIssuance.Manual),
                "Region \"Test Loading Loading 1\" didn't save properly");

    }

    /**
     * @throws Exception
     */
    @Test()
    public void testUpdate() throws Exception {
        List<LoadingRegion> fetchedRegions = dao.getAll();

        LoadingRegion targetRegion = null;
        for (LoadingRegion fetchedRegion : fetchedRegions) {
            if (fetchedRegion.getName().equals("Test Loading Region 2")) {
                targetRegion = fetchedRegion;
                break;
            }
        }

        assertTrue(
                targetRegion != null,
                "Region \"Test Loading Region 2\" not found, possibly \"onSetUp\" method didn't create");

        targetRegion.setDescription("Setting some description");
        dao.save(targetRegion);

        LoadingRegion fetchedRegionById = dao.get(targetRegion.getId());

        assertTrue(
                fetchedRegionById.getDescription().equals(
                        "Setting some description"),
                "Region \"Test Loading Region 2\" didn't update properly");

    }

    /**
     * @throws Exception
     */
    @Test()
    public void testDelete() throws Exception {
        List<LoadingRegion> fetchedRegions = dao.getAll();

        LoadingRegion targetRegion = null;
        for (LoadingRegion fetchedRegion : fetchedRegions) {
            if (fetchedRegion.getName().equals("Test Loading Region 3")) {
                targetRegion = fetchedRegion;
                break;
            }
        }
        assertTrue(
                targetRegion != null,
                "Region \"Test Loading Region 3\" not found, possibly \"onSetUp\" method didn't create");

        dao.delete(targetRegion);

        boolean regionDeleted = false;
        try {
            dao.get(targetRegion.getId());
        } catch (EntityNotFoundException ex) {
            regionDeleted = true;
        }
        assertTrue(regionDeleted,
                "Region \"Test Loading Region 3\" didn't delete properly");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testFindRegionByNumber() throws Exception {
        LoadingRegion fetchedRegion = dao.findRegionByNumber(1);
        assertTrue(fetchedRegion.getName().equals("Test Loading Region 1"),
                "Region \"Test Loading Region 1\" not found");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testUniquenessByNumber() throws Exception {
        Long regionID = dao.uniquenessByNumber(1);
        assertTrue(regionID != 0,
                "Method didn't return any region when it is expected");

        regionID = dao.uniquenessByNumber(6);
        assertTrue(regionID == null,
                "Method did return a region when it is not expected");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testUniquenessByName() throws Exception {
        Long regionID = dao.uniquenessByName("Test Loading Region 1");
        assertTrue(regionID != 0,
                "Method didn't return any region when it is expected");

        regionID = dao.uniquenessByName("Test Loading Region 6");
        assertTrue(regionID == null,
                "Method did return a region when it is not expected");
    }
}
