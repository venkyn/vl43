/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.dao;



import com.vocollect.epp.dao.SiteDAO;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.Date;

import org.hibernate.SessionFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * @author mraj
 * 
 */
@Test(groups = { FAST, DAO })
public class ArchiveLoadingRouteDAOTest extends VoiceLinkDAOTestCase {

    protected static final String DATA_DIR = "data/dbunit/voicelink/";

    private ArchiveLoadingRouteDAO archiveLoadingRouteDAO;

    private SiteDAO siteDAO;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return getVLArchiveDAOTestConfigLocations();
    }

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
//        adapter.handleFlatXmlResource(DATA_DIR
//            + "VoiceLinkDataUnitTest_Core.xml", DatabaseOperation.INSERT);
//        adapter.handleFlatXmlResource(
//            DATA_DIR + "VoiceLinkDataUnitTest_LoadingRegions.xml",
//            DatabaseOperation.INSERT);
//        adapter.handleFlatXmlResource(
//            DATA_DIR + "VoiceLinkDataUnitTest_Loading_Route_Stop_data.xml",
//            DatabaseOperation.INSERT);
//        adapter.handleFlatXmlResource(
//            DATA_DIR + "VoiceLinkDataUnitTest_Loading_Container_data.xml",
//            DatabaseOperation.INSERT);
    }

    /**
     * @return the archiveLoadingRouteDAO
     */
    @Test(enabled = false)
    public ArchiveLoadingRouteDAO getArchiveLoadingRouteDAO() {
        return archiveLoadingRouteDAO;
    }

    /**
     * @param archiveLoadingRouteDAO the archiveLoadingRouteDAO to set
     */
    @Test(enabled = false)
    public void setArchiveLoadingRouteDAO(ArchiveLoadingRouteDAO archiveLoadingRouteDAO) {
        this.archiveLoadingRouteDAO = archiveLoadingRouteDAO;
    }

    /**
     * @return the siteDAO
     */
    @Test(enabled = false)
    public SiteDAO getSiteDAO() {
        return siteDAO;
    }

    /**
     * @param siteDAO the siteDAO to set
     */
    @Test(enabled = false)
    public void setSiteDAO(SiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     * @param sf - the session factory
     */
    @Test(enabled = false)
    public void setArchiveSessionFactory(SessionFactory sf) {
        masterSetSessionFactory(sf);
    }

    /**
     * Does nothing so that the sessionFactory Bean doesn't override the
     * archiveSessionFactoryBean.
     * @param sf - the session factory
     */
    @Override
    @Test(enabled = false)
    public void setSessionFactory(SessionFactory sf) {
        // Do nothing
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.dao.ArchiveLoadingRouteDAO#listOlderThan(java.util.Date)}
     * .
     * @throws Exception on unexpected failure.
     */
    @Test(enabled = false)
    public void testListOlderThan() throws Exception {
        Integer resultSetSize = 2;
        LoadingRouteStatus status = LoadingRouteStatus.Available;
        Integer routeList = this.archiveLoadingRouteDAO.updateOlderThan(
            new Date(), status);
        assertEquals(
            routeList, resultSetSize,
            "Wrong number of routes returned.  Could be a problem with data.");

    }
}
