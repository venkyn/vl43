/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.dao;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.core.model.RegionType;

import com.vocollect.voicelink.loading.model.LoadingRegion;
import com.vocollect.voicelink.loading.model.LoadingRoute;
import com.vocollect.voicelink.loading.model.LoadingRouteIssuance;
import com.vocollect.voicelink.loading.model.LoadingRouteStatus;


import static com.vocollect.epp.test.TestGroups.*;

import java.util.Date;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
/**
 * Test class for testing update and retrieval of Loading route objects.  
 * @author mraj
 * 
 */
@Test(groups = { FAST, DAO })
public class LoadingRouteDAOTest extends VoiceLinkDAOTestCase {

    private LoadingRouteDAO loadingRouteDAO = null;

    private LoadingRegionDAO loadingRegionDAO = null;
    
//    private static final String ROUTE = "18";
    
    private static final Long TRAILER_NUMBER = 1234L;

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_Core.xml", DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_SelectionRegions.xml",
            DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_SelectionData.xml",
            DatabaseOperation.INSERT);
        
    }

    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        createRoute(createRegion());
    }

    /**
     * Test retrieval of Loadig Route object. This test ensures that the the
     * object retrieved does NOT have the hibernate formula fields = NULL.
     * @throws DataAccessException
     */
    @Test(enabled = false)
    public void testLoadingRouteView() throws DataAccessException {
        List<LoadingRoute> loadingRoutes = loadingRouteDAO.getAll();
        LoadingRoute loadingRoute = loadingRoutes.get(0);
        loadingRoute = loadingRouteDAO.get(loadingRoute.getId());
        assertNotNull(loadingRoute.getExpectedCube()); 
        assertNotNull(loadingRoute.getExpectedWeight()); 
//        assertEquals(ROUTE, loadingRoute.getNumber());
        assertEquals(TRAILER_NUMBER, loadingRoute.getTrailer());
    }

    /**
     * This test verifies the value of updated Loading route objects. Modified
     * objects are saved and retrieved again to verify the updated value was
     * actually persisted in DB
     * @throws DataAccessException
     */
    @Test(enabled = false)
    public void testLoadingRouteEdit() throws DataAccessException {
        //Fetch
        List<LoadingRoute> loadingRoutes = loadingRouteDAO.getAll();
        LoadingRoute loadingRoute = loadingRoutes.get(0);

        //Edit
//        loadingRoute.setNumber("19");
        loadingRoute.setTrailer("4321");
        loadingRouteDAO.save(loadingRoute);
        loadingRouteDAO.clearSession();
        
        //Fetch again to verify
        loadingRoutes = loadingRouteDAO.getAll();
        loadingRoute = loadingRoutes.get(0);
//        assertEquals("19", loadingRoute.getNumber());
//        assertFalse("20".equals(loadingRoute.getNumber()));
        assertEquals(4321, loadingRoute.getTrailer());
        assertEquals(LoadingRouteStatus.Available, loadingRoute.getStatus());
        assertEquals("1", loadingRoute.getDockDoor());
//        assertEquals(LoadingRouteType.Unknown, loadingRoute.getType());
        
    }

    @Override
    protected void onTearDown() throws Exception {
        List<LoadingRoute> loadingRoutes = loadingRouteDAO.getAll();
        for (LoadingRoute loadingRoute : loadingRoutes) {
            loadingRouteDAO.delete(loadingRoute);
        }
        
        List<LoadingRegion> loadingRegions = loadingRegionDAO.getAll();
        for (LoadingRegion loadingRegion : loadingRegions) {
            loadingRegionDAO.delete(loadingRegion);
        }
        super.onTearDown();
    }

    /**
     * @param loadingRegion object
     * @throws DataAccessException
     */
    @Test(enabled = false)
    private void createRoute(LoadingRegion loadingRegion)
        throws DataAccessException {
        LoadingRoute loadingRoute = new LoadingRoute();
//        loadingRoute.setNumber("1");
        loadingRoute.setRegion(loadingRegion);
        loadingRoute.setDepartureDateTime(new Date());
        loadingRoute.setTrailer(TRAILER_NUMBER + "");
        loadingRoute.setStatus(LoadingRouteStatus.Available);
//        loadingRoute.setDockDoor("1");
        loadingRoute.setVersion(1);
        loadingRouteDAO.save(loadingRoute);
        loadingRouteDAO.clearSession();

    }

    /**
     * 
     * @return loading region created using this method
     * @throws DataAccessException
     */
    @Test(enabled = false)
    private LoadingRegion createRegion() throws DataAccessException {
        LoadingRegion loadingRegion = new LoadingRegion();
        loadingRegion.setName("Test Loading Region 1");
        loadingRegion.setNumber(1);
        loadingRegion.setType(RegionType.Loading);
        loadingRegion.setGoalRate(100);
        loadingRegion.setDescription("Test Loading Region 1");
        loadingRegion.setLoadingRouteIssuance(LoadingRouteIssuance.Manual);
        loadingRegion.setContainerIDDigitsOperSpeaks(5);
        loadingRegion.setTrailerIDDigitsOperSpeaks(5);
        loadingRegion.setMaxConsolidationOfContainers(1);
        loadingRegionDAO.save(loadingRegion);
        loadingRegionDAO.clearSession();
        return loadingRegion;
    }


    /**
     * @return the loadingRouteDAO
     */
    @Test(enabled = false)
    public LoadingRouteDAO getLoadingRouteDAO() {
        return loadingRouteDAO;
    }

    /**
     * @param loadingRouteDAO the loadingRouteDAO to set
     */
    @Test(enabled = false)
    public void setLoadingRouteDAO(LoadingRouteDAO loadingRouteDAO) {
        this.loadingRouteDAO = loadingRouteDAO;
    }

   
    /**
     * @return the loadingRegionDAO
     */
    @Test(enabled = false)
    public LoadingRegionDAO getLoadingRegionDAO() {
        return loadingRegionDAO;
    }

    /**
     * @param loadingRegionDAO the loadingRegionDAO to set
     */
    @Test(enabled = false)
    public void setLoadingRegionDAO(LoadingRegionDAO loadingRegionDAO) {
        this.loadingRegionDAO = loadingRegionDAO;
    }

}
