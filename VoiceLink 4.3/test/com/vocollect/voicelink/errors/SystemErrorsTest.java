/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.errors;

import com.vocollect.voicelink.core.importer.DataSourceAdapterError;
import com.vocollect.voicelink.core.importer.DataSourceParserError;
import com.vocollect.voicelink.core.importer.ImportManagerError;
import com.vocollect.voicelink.core.importer.ImporterError;
import com.vocollect.voicelink.core.importer.PersistenceManagerError;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

/**
 * This test case is used to instantiate systems errors for the importer.
 * @author dgold
 *
 */

public class SystemErrorsTest {

    /**
     * Test method for 'com.vocollect.voicelink.core.importer.Importer.Importer()'
     */
    @Test()
    public void testSystemErrors() {

        try {
            ImporterError.NO_ERROR.toString();
            DataSourceParserError.NO_ERROR.toString();
            DataSourceAdapterError.NO_ERROR.toString();
            PersistenceManagerError.NO_ERROR.toString();
            ImportManagerError.NO_ERROR.toString();
        } catch (Throwable e) {
            e.printStackTrace();
            assertTrue(e.getMessage(), false);
        }
    }


}
