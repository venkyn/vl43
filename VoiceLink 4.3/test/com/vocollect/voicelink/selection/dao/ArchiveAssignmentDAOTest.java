/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.SiteDAO;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.selection.model.ArchiveAssignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.AssignmentSummary;
import com.vocollect.voicelink.selection.model.SelectionRegion;
import com.vocollect.voicelink.selection.model.WorkIdentifier;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.Date;

import org.dbunit.operation.DatabaseOperation;
import org.hibernate.SessionFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * 
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, DAO })
public class ArchiveAssignmentDAOTest extends VoiceLinkDAOTestCase {

    protected static final String DATA_DIR = "data/dbunit/voicelink/";

    private ArchiveAssignmentDAO archiveAssignmentDAO;
    private SiteDAO siteDAO;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return getVLArchiveDAOTestConfigLocations();
    }

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();                
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_Core.xml",
            DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_SelectionRegions.xml",
            DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_SelectionData.xml",
            DatabaseOperation.INSERT);
        
    }

    /** 
     * @param dao the assignment data access object
     */
    @Test(enabled = false)
    public void setArchiveAssignmentDAO(ArchiveAssignmentDAO dao) {
        this.archiveAssignmentDAO = dao;
    }
    
    /** 
     * @param dao the Site data access object
     */
    @Test(enabled = false)
    public void setSiteDAO(SiteDAO dao) {
        this.siteDAO = dao;
    }
    
 
    /** 
     * @param sf - the session factory
     */
    @Test(enabled = false)
    public void setArchiveSessionFactory(SessionFactory sf) {
        masterSetSessionFactory(sf);
    }
    
    /** 
     * Does nothing so that the sessionFactory Bean doesn't override the 
     * archiveSessionFactoryBean.
     * @param sf - the session factory
     */
    @Override
    @Test(enabled = false)
    public void setSessionFactory(SessionFactory sf) {
        //Do nothing
    }

    /**
     * Test method for {@link com.vocollect.epp.dao.GenericDAO#save(java.lang.Object)}.
     * This test is disable because we don't currently support both an
     * Archive SessionFactory and the regular SessionFactor bound to the thread.
     * @throws Exception on unexpected failure.
     */
    @Test(enabled = false)
    public void testSave() throws Exception {
        ArchiveAssignment assign1 = new ArchiveAssignment();
        ArchiveAssignment assign2 = new ArchiveAssignment();
        
        Operator oper = new Operator();
        SelectionRegion region = new SelectionRegion();
        WorkIdentifier workId = new WorkIdentifier();
        AssignmentSummary assignSum = new AssignmentSummary();
        Site site = this.siteDAO.findByName(Site.DEFAULT_SITE_NAME);
        
        oper.setOperatorIdentifier("TestOper");
        region.setName("TestRegion");
        region.setNumber(-1);
        workId.setWorkIdentifierValue("TestValue");
        assignSum.setTotalItemQuantity(-1);
        assignSum.setGoalTime(-1.0);
        
        assign1.setCreatedDate(new Date(0));
        assign1.setNumber(-1L);
//        assign1.setOperator(oper);
        assign1.setRegion(region);
        assign1.setWorkIdentifier(workId);
        assign1.setSummaryInfo(assignSum);
        assign1.setContainerCount(-1);
        assign1.setPickDetailCount(-1);
        assign1.setSite(site);
        assign1.setVersion(98);
        
        assign2.setCreatedDate(new Date(0));
        assign2.setNumber(-1L);
//        assign2.setOperator(oper);
        assign2.setRegion(region);
        assign2.setWorkIdentifier(workId);
        assign2.setSummaryInfo(assignSum);
        assign2.setContainerCount(-1);
        assign2.setPickDetailCount(-1);
        assign2.setSite(site);
        assign2.setVersion(99);

        this.archiveAssignmentDAO.save(assign1);
        this.archiveAssignmentDAO.save(assign2);
        
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.dao.ArchiveAssignmentDAO#listOlderThan(java.util.Date)}.
     * @throws Exception on unexpected failure.
     */
    @Test()
    public void testListOlderThan() throws Exception {
        Integer resultSetSize = 5;
        AssignmentStatus status = AssignmentStatus.Available;
        Integer assignList = this.archiveAssignmentDAO.updateOlderThan(new Date(), status);
        assertEquals(assignList, resultSetSize, 
            "Wrong number of assignments returned.  Could be a problem with data.");
        
    }

}
