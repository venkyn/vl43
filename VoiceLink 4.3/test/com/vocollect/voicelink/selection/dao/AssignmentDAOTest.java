/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.dao.ItemDAO;
import com.vocollect.voicelink.core.dao.LocationDAO;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.model.WorkIdentifier;

import static com.vocollect.epp.test.TestGroups.*;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * 
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, DAO })
public class AssignmentDAOTest extends VoiceLinkDAOTestCase {

    private AssignmentDAO assignmentDAO;
    private SelectionRegionDAO regionDAO; 
    private LocationDAO locationDAO; 
    private ItemDAO itemDAO; 
    
    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();                
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_Core.xml",
            DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_SelectionRegions.xml",
            DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_SelectionData.xml",
            DatabaseOperation.INSERT);
        
    }

    /** 
     * @param dao the assignment data access object
     */
    @Test(enabled = false)
    public void setAssignmentDAO(AssignmentDAO dao) {
        this.assignmentDAO = dao;
    }

    /** 
     * @param dao the selection region data access object
     */
    @Test(enabled = false)
    public void setSelectionRegionDAO(SelectionRegionDAO dao) {
        this.regionDAO = dao;
    }

    /** 
     * @param dao the item data access object
     */
    @Test(enabled = false)
    public void setItemDAO(ItemDAO dao) {
        this.itemDAO = dao;
    }

    /** 
     * @param dao the location data access object
     */
    @Test(enabled = false)
    public void setLocationDAO(LocationDAO dao) {
        this.locationDAO = dao;
    }

    /**
     * Test method for {@link com.vocollect.epp.dao.GenericDAO#save(java.lang.Object)}.
     * @throws Exception on unexpected failure.
     */
    @Test()
    public void testSave() throws Exception {
        Assignment assign = new Assignment();
        Pick pick = new Pick();
        pick.setLocation(this.locationDAO.get(-1L));
        pick.setItem(this.itemDAO.get(-28L));
        pick.setSequenceNumber(1);
        assign.addPick(pick);
        assign.setRegion(this.regionDAO.get(-1L));
        WorkIdentifier workId = new WorkIdentifier();
        workId.setWorkIdentifierValue("12345566");
        workId.setPartialWorkIdentifier("12345566");
        assign.setWorkIdentifier(workId);
        assign.setPriority(5);
        assign.setNumber(-1L);
        this.assignmentDAO.save(assign);
        
    }
}
