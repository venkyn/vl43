/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * Tests for PickDAO methods.
 *
 * @author ddoubleday
 */
public class PickDAOTest extends VoiceLinkDAOTestCase {

    private PickDAO dao = null;
    
    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_ResetAll.xml",
            DatabaseOperation.DELETE_ALL);
        adapter.resetInstallationData();                
    }

    /** 
     * @param pickDAO the user data access object
     */
    @Test(enabled = false)
    public void setPickDAO(PickDAO pickDAO) {
        this.dao = pickDAO;
    }
    
}
