/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.SelectionRegion;
import com.vocollect.voicelink.selection.service.SelectionRegionManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * Unit test for the SelectionRegionManagerImpl.
 * 
 * @author bnorthrop
 */
@Test(groups = { FAST, SELECTION })
public class SelectionRegionManagerImplTest extends BaseServiceManagerTestCase {

    private SelectionRegionManager manager = null;

    /**
     * Getter for the manager property.
     * @return SelectionRegionManager value of the property
     */
    @Test(enabled = false)
    public SelectionRegionManager getSelectionRegionManager() {
        return this.manager;
    }

    /**
     * Setter for the manager property.
     * @param mgr - the new manager value
     */
    @Test(enabled = false)
    public void setSelectionRegionManager(SelectionRegionManager mgr) {
        this.manager = mgr;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkSampleOperators.xml",
            DatabaseOperation.CLEAN_INSERT);
    }


    /**
     * Test updating the partial work identifiers to partial work id.
     * 
     * @throws Exception - any exception
     */
    public void testUpdatePartialWorkIdPartial() throws Exception {
        SelectionRegion selRegion = manager.get(new Long(-1));
        
        //Check size 2
        selRegion.getUserSettingsNormal().setWorkIdentifierRequestLength(2);
        manager.save(selRegion);
        
        selRegion = manager.get(new Long(-1));
        
        for (Assignment a : selRegion.getAssignments()) {
            assertEquals(
                a.getWorkIdentifier().getPartialWorkIdentifier().length(),
                2, 
                "Expect length of 2");
        }
    }
    
    /**
     * Test updating the partial work identifiers to full work id.
     * 
     * @throws Exception - any exception
     */
    public void testUpdatePartialWorkIdFull() throws Exception {
        SelectionRegion selRegion = manager.get(new Long(-1));
        
        //Check size all
        selRegion.getUserSettingsNormal().setWorkIdentifierRequestLength(0);
        manager.save(selRegion);
        
        selRegion = manager.get(new Long(-1));
        
        for (Assignment a : selRegion.getAssignments()) {

            assertEquals(a.getWorkIdentifier().getPartialWorkIdentifier(),
                a.getWorkIdentifier().getWorkIdentifierValue(),
                "Expect same values");
        }
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.selection.service.impl.validateNoOperatorsSignedIn()'.
     * 
     * @throws Exception -Any Exception
     */
    public void testValidateNoOperatorsSignedIn() throws Exception {
        // ============================================================
        // no operator(s) signed into the region
        try {
            SelectionRegion selRegion = manager.get(new Long(-8L));
            manager.executeValidateEditRegion(selRegion);
        } catch (BusinessRuleException e) {
            fail("One or more operator(s) signed into this region.");
            // log.debug("expected exception: " + e.getMessage());
        }

        // ============================================================
        // 3 operator(s) signed into the region
        try {

            SelectionRegion selRegion = manager.get(new Long(-2L));
            manager.executeValidateEditRegion(selRegion);
        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
        }

        // ============================================================
        // 1 operator(s) signed into the region
        try {
            SelectionRegion selRegion = manager.get(new Long(-3L));
            manager.executeValidateEditRegion(selRegion);

        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
        }

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.selection.service.impl.validateAssignmentStatus()'.
     * 
     * @throws Exception -Any Exception
     */
    public void testValidateAssignmentStatus() throws Exception {

        /**
         * ============================================================ There
         * are assignments in the region with a status of In-Progress=true ,
         * Suspended=false, and Passed=true
         */
        try {

            SelectionRegion selRegion = manager.get(new Long(-2L));
            manager.executeValidateEditRegion(selRegion);

        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
        }

        /**
         * ============================================================ There
         * are assignments in the region with a status of In-Progress=true ,
         * Suspended=false, and Passed=false
         */
        try {

            SelectionRegion selRegion = manager.get(new Long(-1L));
            manager.executeValidateEditRegion(selRegion);

        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
        }

        /**
         * ============================================================ There
         * are assignments in the region with a status of In-Progress=false ,
         * Suspended=false, and Passed=false
         */
        try {

            SelectionRegion selRegion = manager.get(new Long(-8L));
            manager.executeValidateEditRegion(selRegion);

        } catch (BusinessRuleException e) {
            fail("There are assignments in the region with a status of "
                + " either 'In-Progress','Suspended' or 'Passed' ");
        }

        /**
         * ============================================================ There
         * are assignments in the region with a status of In-Progress=true ,
         * Suspended=true, and Passed=true
         */
        try {

            SelectionRegion selRegion = manager.get(new Long(-4L));
            manager.executeValidateEditRegion(selRegion);

        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
        }

    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.selection.service.impl.validateDigitsSpoken()'.
     * 
     * @throws Exception -Any Exception
     */
    public void testValidateDigitsSpoken() throws Exception {

        try {

            SelectionRegion selRegion = manager.get(new Long(-7L));
            manager.executeValidateEditRegion(selRegion);

        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
        }
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.selection.service.impl.validateWorkIdRequestLength()'.
     * 
     * @throws Exception -Any Exception
     */
    public void validateWorkIdRequestLength() throws Exception {

        try {

            SelectionRegion selRegion = manager.get(new Long(-13L));
            manager.executeValidateEditRegion(selRegion);

        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
        }
    }
    
    /**
     * Test method for
     * 'com.vocollect.voicelink.selection.service.impl.validateNumberOfAssignments()'.
     * 
     * @throws Exception -Any Exception
     */
    public void testValidateNumberOfAssignments() throws Exception {

        try {
            SelectionRegion selRegion = manager.get(new Long(-4L));
            manager.executeValidateEditRegion(selRegion);

        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
        }
    }

    /**
     * Test method for
     * 'com.vocollect.voicelink.selection.service.impl.validateIssuance()'.
     * 
     * @throws Exception -Any Exception
     */
    public void testValidateIssuance() throws Exception {

        /**
         * ============================================================ If a
         * user switches from a region profile that had automatic issuance to
         * one that has manual issuances automatic issuance - false ,
         * available-true and unavailable-false
         */

        try {
            SelectionRegion selRegion = manager.get(new Long(-4L));

            manager.executeValidateEditRegion(selRegion);

        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
        }

        /**
         * ============================================================ If a
         * user switches from a region profile that had automatic issuance to
         * one that has manual issuances automatic issuance - false ,
         * available-true and unavailable-true
         */

        try {
            SelectionRegion selRegion = manager.get(new Long(-5L));
            manager.executeValidateEditRegion(selRegion);

        } catch (BusinessRuleException e) {
            log.debug("expected exception: " + e.getMessage());
        }

        /**
         * ============================================================
         * Switching from a region profile that had automatic issuance to one
         * that has manual issuances automatic issuance - false ,
         * available-false and unavailable-false
         */

        try {
            SelectionRegion selRegion = manager.get(new Long(-7L));
            manager.executeValidateEditRegion(selRegion);

        } catch (BusinessRuleException e) {
            fail();
        }

    }

    /**
     * Test return correct values.
     * 
     * @throws Exception - any exception
     */
    public void testProfileNumberList() throws Exception {
        Map<String, Long> profiles = null;

        profiles = this.manager.getRegionProfileNumbers(
            AssignmentType.Normal, null);
        assertEquals(profiles.size(), 15, "Expected all 15 profiles");

        profiles = this.manager.getRegionProfileNumbers(
            AssignmentType.Chase, null);
        assertEquals(profiles.size(), 2, "Expected 2 profiles");
        
        //Target Container
        profiles = this.manager.getRegionProfileNumbers(
            AssignmentType.Normal, -9L);
        assertEquals(profiles.size(), 1, "Expected 1 profile");
        
        //Case Label Check Digits
        profiles = this.manager.getRegionProfileNumbers(
            AssignmentType.Normal, -12L);
        assertEquals(profiles.size(), 1, "Expected 1 profile");
        
        //ordinary
        profiles = this.manager.getRegionProfileNumbers(
            AssignmentType.Normal, -1L);
        assertEquals(profiles.size(), 13, "Expected 1 profile");
    }

//    /**
//     * Check for duplicate region number or name.
//     * 
//     * @throws Exception - any exception
//     */
//    public void validateDuplicateRegionNumberAndName() throws Exception {
//
//        SelectionRegion selRegion = manager.get(new Long(-13L));
//        SelectionRegion selRegionName = manager.get(new Long(-12L));
//    
//        //save with no changes, all validations should pass
//        try {
//            manager.executeValidateEditRegion(selRegion);
//        } catch (BusinessRuleException e) {
//            fail("No error expected");
//        }
//
//        //save region with duplicate name
//        try {
//            selRegion.setName(selRegionName.getName());
//            manager.executeValidateEditRegion(selRegion);
//            fail("Expected error to occur");
//        } catch (BusinessRuleException e) {
//            assertEquals(SelectionErrorCode.REGION_DUPLICATE_NAME.toString(), 
//                e.getErrorCode().toString(),
//                "ErrorCode");
//        }
//        
//        //save region with duplicate number
//        try {
//            SelectionRegion newRegion = new SelectionRegion();
//            newRegion.setNumber(selRegion.getNumber());
//            manager.executeValidateEditRegion(newRegion);
//            fail("Expected error to occur");
//        } catch (BusinessRuleException e) {
//            assertEquals(SelectionErrorCode.REGION_DUPLICATE_NUMBER.toString(), 
//                e.getErrorCode().toString(),
//                "ErrorCode");
//        }
//        
//    }
    
    /**
     * @throws Exception
     */
    public void testSummaryQueries() throws Exception {
        List<DataObject> objects = null;
        ResultDataInfo rdi = new ResultDataInfo();
        rdi.setQueryArgs(new Object[] { new Date() });
        objects = manager.listAssignmentSummary(rdi);
        assertEquals(objects.size(), 15, "Assignment Summary");

        objects = manager.listCurrentWorkSummary(rdi);
        assertEquals(objects.size(), 15, "Current Summary");
        
        objects = manager.listShortsSummary(rdi);
        assertEquals(objects.size(), 15, "Shorts Summary");
        
    }

}
