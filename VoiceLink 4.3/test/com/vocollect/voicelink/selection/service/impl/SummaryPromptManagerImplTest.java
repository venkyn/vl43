/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.service.SystemTranslationManager;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.selection.model.SummaryPrompt;
import com.vocollect.voicelink.selection.model.SummaryPromptCharactersToSpeak;
import com.vocollect.voicelink.selection.model.SummaryPromptDefinition;
import com.vocollect.voicelink.selection.model.SummaryPromptDefinitionLocale;
import com.vocollect.voicelink.selection.service.SummaryPromptManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.List;
import java.util.Locale;

import org.testng.annotations.Test;

/**
 * Unit test for the PickDetailManagerImpl.
 * 
 * @author pfunyak
 */
@Test(groups = { FAST, SELECTION })
public class SummaryPromptManagerImplTest extends  BaseServiceManagerTestCase {

    private SummaryPromptManager summaryPromptManager;
    private SystemTranslationManager systemTranslationManager;
    
    private static final int SUPPORTED_LOCALE_COUNT = 13;
    
    /**
     * @param impl - implementation of selection region manager.
     */
    @Test(enabled = false)
    public void setSummaryPromptManager(SummaryPromptManager impl) {
        this.summaryPromptManager = impl;
    }

    /**
     * @param impl - implementation of system translation manager.
     */
    @Test(enabled = false)
    public void setSystemTranslationManager(SystemTranslationManager impl) {
        this.systemTranslationManager = impl;
    }

    /**
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     * @param adapter - to load the dbunit data
     * @throws Exception - any exception
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.resetInstallationData();
    }

    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testSumsForReturnValues() throws Exception {
        
        SummaryPrompt sp = summaryPromptManager.getDefaultSummaryPrompt();

        assertEquals(4, sp.getDefinitions().size(), "4 parts");
        assertEquals(1, sp.getDefinitions().get(0).getPosition(), "Position 1");
        assertEquals(2, sp.getDefinitions().get(1).getPosition(), "Position 2");
        assertEquals(3, sp.getDefinitions().get(2).getPosition(), "Position 3");
        assertEquals(4, sp.getDefinitions().get(3).getPosition(), "Position 4");
        sp.setName("test prompt");
        summaryPromptManager.save(sp);
    }
    
    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testSamplePrompts() throws Exception {
        
        SummaryPrompt sp = summaryPromptManager.getDefaultSummaryPrompt();
        
        List<String> prompts = summaryPromptManager.buildSamplePrompts(sp, Locale.US);
        assertEquals(3, prompts.size(), "3 prompts");

        //Check left 
        sp.getDefinitions().get(1)
            .setSpeakCharactersIndicator(SummaryPromptCharactersToSpeak.Left);
        sp.getDefinitions().get(1).setNumberOfCharacters(4);
        prompts = summaryPromptManager.buildSamplePrompts(sp, Locale.US);
        
        assertEquals(3, prompts.size(), "3 prompts");
        
        //Check Right 
        sp.getDefinitions().get(1)
            .setSpeakCharactersIndicator(SummaryPromptCharactersToSpeak.Right);
        prompts = summaryPromptManager.buildSamplePrompts(sp, Locale.US);
        
        assertEquals(3, prompts.size(), "3 prompts");
        
        //Check More Characters Right
        sp.getDefinitions().get(1).setNumberOfCharacters(15);
        prompts = summaryPromptManager.buildSamplePrompts(sp, Locale.US);
        
        assertEquals(3, prompts.size(), "3 prompts");
        
        //Check More Left
        sp.getDefinitions().get(1)
            .setSpeakCharactersIndicator(SummaryPromptCharactersToSpeak.Left);
        prompts = summaryPromptManager.buildSamplePrompts(sp, Locale.US);
    
        assertEquals(3, prompts.size(), "3 prompts");
        
        //Check Phonetic 
        sp.getDefinitions().get(1)
            .setSpeakCharactersIndicator(SummaryPromptCharactersToSpeak.Left);
        sp.getDefinitions().get(1).setNumberOfCharacters(4);
        sp.getDefinitions().get(1).setAllowSpeakingPhonetically(true);
        sp.getDefinitions().get(2).setAllowSpeakingPhonetically(true);
        prompts = summaryPromptManager.buildSamplePrompts(sp, Locale.US);
        
        assertEquals(3, prompts.size(), "3 prompts");
    }
    
    /**
     * @throws  Exception - any
     * test getting locale methods 
     */
    @Test()
    public void testListLocales() throws Exception {
        
        // verify that we get the correct number of supported languages/locales.
        List<Locale> locales = systemTranslationManager.getAllSupportedLocales();
        assertEquals(locales.size(), SUPPORTED_LOCALE_COUNT, "All locales supported");

        // create a new summary prompt.
        SummaryPrompt sp = summaryPromptManager.getDefaultSummaryPrompt();
        sp.setName("testPrompt");
        SummaryPromptDefinitionLocale spdl = null;
        SummaryPromptDefinition def = null;

        // add a language to the prompt.
        def = sp.getDefinitions().get(0);
        spdl = new SummaryPromptDefinitionLocale();
        spdl.setLocale(locales.get(0));
        spdl.setSummaryPromptDefinition(def);
        spdl.setPromptValue1(def.getPromptValue1());
        spdl.setPromptValue2(def.getPromptValue2());
        spdl.setPromptValue3(def.getPromptValue3());
        sp.getDefinitions().get(0).getLocaleDefintions().add(spdl);
        
        spdl = new SummaryPromptDefinitionLocale();
        def = sp.getDefinitions().get(1);
        spdl.setLocale(locales.get(0));
        spdl.setSummaryPromptDefinition(def);
        spdl.setPromptValue1(def.getPromptValue1());
        spdl.setPromptValue2(def.getPromptValue2());
        spdl.setPromptValue3(def.getPromptValue3());
        sp.getDefinitions().get(1).getLocaleDefintions().add(spdl);
        
        spdl = new SummaryPromptDefinitionLocale();
        def = sp.getDefinitions().get(2);
        spdl.setLocale(locales.get(0));
        spdl.setSummaryPromptDefinition(def);
        spdl.setPromptValue1(def.getPromptValue1());
        spdl.setPromptValue2(def.getPromptValue2());
        spdl.setPromptValue3(def.getPromptValue3());
        sp.getDefinitions().get(2).getLocaleDefintions().add(spdl);

        spdl = new SummaryPromptDefinitionLocale();
        def = sp.getDefinitions().get(3);
        spdl.setLocale(locales.get(0));
        spdl.setSummaryPromptDefinition(def);
        spdl.setPromptValue1(def.getPromptValue1());
        spdl.setPromptValue2(def.getPromptValue2());
        spdl.setPromptValue3(def.getPromptValue3());
        sp.getDefinitions().get(3).getLocaleDefintions().add(spdl);
        
        this.summaryPromptManager.save(sp);

        // add another language to the prompt
        def = sp.getDefinitions().get(0);
        spdl = new SummaryPromptDefinitionLocale();
        spdl.setLocale(locales.get(5));
        spdl.setSummaryPromptDefinition(def);
        spdl.setPromptValue1(def.getPromptValue1());
        spdl.setPromptValue2(def.getPromptValue2());
        spdl.setPromptValue3(def.getPromptValue3());
        sp.getDefinitions().get(0).getLocaleDefintions().add(spdl);
        
        spdl = new SummaryPromptDefinitionLocale();
        def = sp.getDefinitions().get(1);
        spdl.setLocale(locales.get(5));
        spdl.setSummaryPromptDefinition(def);
        spdl.setPromptValue1(def.getPromptValue1());
        spdl.setPromptValue2(def.getPromptValue2());
        spdl.setPromptValue3(def.getPromptValue3());
        sp.getDefinitions().get(1).getLocaleDefintions().add(spdl);
        
        spdl = new SummaryPromptDefinitionLocale();
        def = sp.getDefinitions().get(2);
        spdl.setLocale(locales.get(5));
        spdl.setSummaryPromptDefinition(def);
        spdl.setPromptValue1(def.getPromptValue1());
        spdl.setPromptValue2(def.getPromptValue2());
        spdl.setPromptValue3(def.getPromptValue3());
        sp.getDefinitions().get(2).getLocaleDefintions().add(spdl);

        spdl = new SummaryPromptDefinitionLocale();
        def = sp.getDefinitions().get(3);
        spdl.setLocale(locales.get(5));
        spdl.setSummaryPromptDefinition(def);
        spdl.setPromptValue1(def.getPromptValue1());
        spdl.setPromptValue2(def.getPromptValue2());
        spdl.setPromptValue3(def.getPromptValue3());
        sp.getDefinitions().get(3).getLocaleDefintions().add(spdl);       
        
        this.summaryPromptManager.save(sp);
       
        // verify the list of languages/locales that are not defined yet.
        // this should return 2 less that the total (23) 
        List<Locale> unDefined = summaryPromptManager.getNotYetDefinedLocales(sp);
        assertEquals(unDefined.size(), SUPPORTED_LOCALE_COUNT - 2, "All locales supported");

        //Test Deleteting a language/locale from a prompt
        summaryPromptManager.executeDeleteLocale(sp, locales.get(0));
        unDefined = summaryPromptManager.getNotYetDefinedLocales(sp);
        assertEquals(unDefined.size(), SUPPORTED_LOCALE_COUNT - 1, "All locales supported");
    }
}
