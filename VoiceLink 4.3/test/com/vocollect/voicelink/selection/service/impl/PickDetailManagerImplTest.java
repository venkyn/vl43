/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.selection.model.PickDetail;
import com.vocollect.voicelink.selection.model.PickDetailType;
import com.vocollect.voicelink.selection.service.PickDetailManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * Unit test for the PickDetailManagerImpl.
 * 
 * @author pfunyak
 */
@Test(groups = { FAST, SELECTION })
public class PickDetailManagerImplTest extends  BaseServiceManagerTestCase {

    private PickDetailManager pickDetailManager;

    /**
     * @param impl - implementation of selection region manager.
     */
    @Test(enabled = false)
    public void setPickDetailManager(PickDetailManager impl) {
        this.pickDetailManager = impl;
    }

    /**
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     * @param adapter - to load the dbunit data
     * @throws Exception - any exception
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        
        // populate db with base data.
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
        // add data specific to this test.
        adapter.handleFlatXmlResource(
            DATA_DIR + "VoiceLinkDataUnitTest_PickDetail.xml",
            DatabaseOperation.REFRESH);

    }

    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testSumsForReturnValues() throws Exception {
        
        // try to  sum quantities on an empty pick detail table and verify 
        // that the call returns zero and not a null value
        Integer sum1 = this.pickDetailManager.sumQuantityPicked(-1, new Date(), new Date());
        assertEquals(0, sum1.intValue(), "quantity picked");
        
        Integer sum2 = this.pickDetailManager.sumAssignmentProrateCount(-1, new Date(), new Date());
        assertEquals(0, sum2.intValue(), "Assignment prorate count");
        
        Integer sum3 = this.pickDetailManager.sumGroupProrateCount(-1, -1, new Date(), new Date());
        assertEquals(new Integer(0), sum3, "Group prorate count");
        
    }

    /**
     * @throws  Exception - any
     * 
     */
    @Test()
    public void testSumsForNonZeroReturnValue() throws Exception {
        

        // sum existing quantities and verify results.
        // verify that the summation calls return valid values an do not
        // return null
        
        // get the start and end time from the pick detail table.
        List <PickDetail> pickList = this.pickDetailManager.getAll();
        Date endTime = pickList.get(0).getPickTime();
        Date startTime = pickList.get(pickList.size() - 1).getPickTime();
        for (PickDetail pd : pickList) {
            //get greatest time
            if (pd.getPickTime().after(endTime)) {
                endTime = pd.getPickTime();
            }
            
            //get lowest time
            if (pd.getPickTime().before(startTime)) {
                startTime = pd.getPickTime();
            }
        }

        // verify that call returns the correct value and does not return a null
        Integer sum1 = this.pickDetailManager.sumQuantityPicked(-46, startTime, endTime);
        assertEquals(10, sum1.intValue(), "quantity picked");
        
        Integer sum2 = this.pickDetailManager.sumAssignmentProrateCount(-46, startTime, endTime);
        assertEquals(10, sum2.intValue(), "assignment prorate count");
      
        
        Integer sum3 = this.pickDetailManager.sumGroupProrateCount(1, -90, startTime, endTime);
        assertEquals(10, sum3.intValue(), "Group prorate count");
    }

    /**
     * Test case to verify VLINK-3129 - Only actually picked quantities should
     * be summed when doing labor calculations
     * @throws Exception - any
     */
    @Test()
    public void testVlink3129() throws Exception {
        // get the start and end time from the pick detail table.
        List<PickDetail> pickList = this.pickDetailManager.getAll();
        Date endTime = pickList.get(0).getPickTime();
        Date startTime = pickList.get(pickList.size() - 1).getPickTime();
        int totalAssignmentQuantityPicked = 0;
        for (PickDetail pd : pickList) {

            if (!pd.getPick().getAssignment().getId().equals(-46L)) {
                continue;
            }

            // get greatest time
            if (pd.getPickTime().after(endTime)) {
                endTime = pd.getPickTime();
            }

            // get lowest time
            if (pd.getPickTime().before(startTime)) {
                startTime = pd.getPickTime();
            }

            totalAssignmentQuantityPicked += pd.getQuantityPicked().intValue();
        }

        PickDetail testPickDetail = pickList.get(0);
        PickDetailType originalType = testPickDetail.getType();

        // Test quantity picked for pick details with type TaskPicked
        Integer sum1 = this.pickDetailManager.sumQuantityPicked(
            -46, startTime, endTime);
        assertEquals(
            totalAssignmentQuantityPicked, sum1.intValue(), "quantity picked");

        // Test quantity picked after changing pick detail to type MarkOut
        testPickDetail.setType(PickDetailType.MarkOut);
        this.pickDetailManager.save(testPickDetail);
        int testQuantity = totalAssignmentQuantityPicked
            - testPickDetail.getQuantityPicked().intValue();
        sum1 = this.pickDetailManager
            .sumQuantityPicked(-46, startTime, endTime);
       
        try {
            assertEquals(testQuantity, sum1.intValue(), "quantity picked");
        } finally {
            // Revert, irrespective of assertion result pick detail type
            testPickDetail.setType(originalType);
            this.pickDetailManager.save(testPickDetail);
        }

    }
}
