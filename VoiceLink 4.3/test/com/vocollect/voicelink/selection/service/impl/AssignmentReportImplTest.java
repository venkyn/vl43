/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.test.DbUnitAdapter;

import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.selection.model.ArchiveAssignment;
import com.vocollect.voicelink.selection.model.ArchiveContainer;
import com.vocollect.voicelink.selection.model.ArchivePick;
import com.vocollect.voicelink.selection.model.ArchivePickDetail;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentReport;
import com.vocollect.voicelink.selection.model.AssignmentReportRoot;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.model.PickDetail;

import com.vocollect.voicelink.selection.service.ArchiveAssignmentManager;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.testng.annotations.Test;

/**
 * Unit test case class for Assignment Report
 * @author krishna.udupi
 *
 */
@Test(groups = { FAST, CORE, SECURITY })
public class AssignmentReportImplTest extends BaseServiceManagerTestCase {

    // Parameter values
    String workID = null;

    String customerNumber = null;

    String route = null;

    Long assignmentNumber = null;

    String operatorId = null;

    Date startTime = null;

    Date endTime = null;

    String siteId = "-1"; // Site ID for Default Site.

    private AssignmentManager assignmentManager;

    private ArchiveAssignmentManager archiveAssignmentManager;


    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return getVLArchiveDAOTestConfigLocations();
    }    
    
    /**
     * Getter for AssignmentManager
     * @return assignmentManager
     */
    @Test(enabled = false)
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * Setter for AssignmentManager
     * @param assignmentManager AssignmentManager
     */
    @Test(enabled = false)
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * Getter for ArchiveAssignmentManager
     * @return archiveAssignmentManager
     */
    @Test(enabled = false)
    public ArchiveAssignmentManager getArchiveAssignmentManager() {
        return archiveAssignmentManager;
    }

    /**
     * Setter for ArchiveAssignmentManager
     * @param archiveAssignmentManager ArchiveAssignmentManager
     */
    @Test(enabled = false)
    public void setArchiveAssignmentManager(ArchiveAssignmentManager archiveAssignmentManager) {
        this.archiveAssignmentManager = archiveAssignmentManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        // We are not using this method to load data.
        // We are using a loadData method so we can seperate the tests into
        // multiple
        // test methods and not have to load the data before each test.
    }

    /**
     * Test Assignment values for null.
     * @throws DataAccessException
     */
    @Test()
    public void testAssignmentValuesForNull() throws DataAccessException {
        // Fetching Assignments matching the criteria.
/*        List<AssignmentReport> assignmentList = getAssignmentManager()
            .listAssignmentsforAssignmentReport(new QueryDecorator(),
                workID, operatorId, assignmentNumber, customerNumber, route,
                startTime, endTime);

        for (AssignmentReport assignment : assignmentList) {
            assertNotNull(assignment.getNumber(), "Assignment Number is Null");
            assertNotNull(assignment.getStatus(), "Assignment Status is Null");
            assertNotNull(
                assignment.getCustomerInfo().getCustomerNumber(),
                "Customer Number is Null");
            assertNotNull(assignment.getRoute(), "Route is Null");
            assertNotNull(assignment.getWorkIdentifier()
                .getWorkIdentifierValue(), "Work ID is Null");
            Set<Container> containerSet = assignment.getContainers();
            for (Container container : containerSet) {
                assertNotNull(
                    container.getContainerNumber(), "Container Number is Null");
                assertNotNull(container.getStatus(), "Container Status is Null");
                assertNotNull(
                    container.getContainerType().getId(),
                    "ContainerType is Null");
            }
            List<Pick> pickList = assignment.getPicks();
            for (Pick pick : pickList) {
                assertNotNull(pick.getId(), "Pick ID is Null");
                assertNotNull(pick.getStatus(), "Pick Status is Null");
                assertNotNull(pick.getPickTime(), "Pick Time is Null");
                assertNotNull(
                    pick.getLocation().getPostAisle(), "Post Aisle is Null");
                assertNotNull(
                    pick.getLocation().getPreAisle(), "Pre Aisle is Null");
                assertNotNull(pick.getLocation().getAisle(), "Aisle is Null");
                assertNotNull(
                    pick.getItem().getDescription(), "Pick Description is Null");
                assertNotNull(pick.getItem().getNumber(), "Item Number is Null");
                assertNotNull(
                    pick.getOperator().getOperatorIdentifier(),
                    "Operator ID is Null");
                assertNotNull(
                    pick.getOperator().getName(), "Operator Name is Null");
                assertNotNull(
                    pick.getQuantityPicked(), "Quantity Picked is Null");
                assertNotNull(
                    pick.getQuantityToPick(), "Quantity to Pick is Null");
                assertNotNull(pick.getLocation().getSlot(), "Slot is Null");
                assertNotNull(
                    pick.getItem().getWeight(), "Variable Weight is Null");

                List<PickDetail> pickDetail = pick.getPickDetails();
                for (PickDetail pickDetails : pickDetail) {
                    assertNotNull(
                        pickDetails.getLotNumber(), "Lot Number is Null");
                    assertNotNull(
                        pickDetails.getSerialNumber(), "Serial Number is Null");
                }
            }
        }*/

    }

    /**
     * Test Archive Assignment values for null.
     */
    @Test()
    public void testArchiveAssignmentValuesForNull() throws DataAccessException {

/*        // Fetching Archive Assignments matching the criteria.
        List<AssignmentReportRoot> archiveAssignmentList = getArchiveAssignmentManager()
            .listArchiveAssignmentsforAssignmentReport(new QueryDecorator(), 
                workID, operatorId, null, customerNumber, route, startTime,
                endTime, Long.parseLong(siteId));

        for (ArchiveAssignment archiveAssignment : archiveAssignmentList) {
            assertNotNull(
                archiveAssignment.getNumber(),
                "Archive Assignment Number is Null");
            assertNotNull(
                archiveAssignment.getStatus(),
                "Archive Assignment Status is Null");
            assertNotNull(archiveAssignment.getCustomerInfo()
                .getCustomerNumber(), "Archive Customer Number is Null");
            assertNotNull(archiveAssignment.getRoute(), "Archive Route is Null");
            assertNotNull(archiveAssignment.getWorkIdentifier()
                .getWorkIdentifierValue(), "Archive Work ID is Null");
            Set<ArchiveContainer> archiveContainerSet = archiveAssignment
                .getContainers();
            for (ArchiveContainer archiveContainer : archiveContainerSet) {
                assertNotNull(
                    archiveContainer.getContainerNumber(),
                    "Archive Container Number is Null");
                assertNotNull(
                    archiveContainer.getStatus(),
                    "Archive Container Status is Null");
                assertNotNull(
                    archiveContainer.getContainerType().getId(),
                    "Archive ContainerType is Null");
            }
            List<ArchivePick> archivePickList = archiveAssignment.getPicks();
            for (ArchivePick archivePick : archivePickList) {
                assertNotNull(archivePick.getId(), "Archive Pick ID is Null");
                assertNotNull(
                    archivePick.getStatus(), "Archive Pick Status is Null");
                assertNotNull(
                    archivePick.getPickTime(), "Archive Pick Time is Null");
                assertNotNull(
                    archivePick.getDescription().getPostAisle(),
                    "Archive Post Aisle is Null");
                assertNotNull(
                    archivePick.getDescription().getPreAisle(),
                    "Archive Pre Aisle is Null");
                assertNotNull(
                    archivePick.getDescription().getAisle(),
                    "Archive Aisle is Null");
                assertNotNull(
                    archivePick.getItem().getDescription(),
                    "Archive Pick Description is Null");
                assertNotNull(
                    archivePick.getItem().getNumber(),
                    "Archive Item Number is Null");
                assertNotNull(
                    archivePick.getOperator().getOperatorIdentifier(),
                    "Archive Operator ID is Null");
                assertNotNull(
                    archivePick.getOperator().getName(),
                    "Archive Operator Name is Null");
                assertNotNull(
                    archivePick.getQuantityPicked(),
                    "Archive Quantity Picked is Null");
                assertNotNull(
                    archivePick.getQuantityToPick(),
                    "Archive Quantity to Pick is Null");
                assertNotNull(
                    archivePick.getDescription().getSlot(),
                    "Archive Slot is Null");
                assertNotNull(
                    archivePick.getItem().getWeight(),
                    "Archive Variable Weight is Null");

                List<ArchivePickDetail> archivePickDetail = archivePick
                    .getPickDetails();
                for (ArchivePickDetail archivePickDetails : archivePickDetail) {
                    assertNotNull(
                        archivePickDetails.getLotNumber(),
                        "Archive Lot Number is Null");
                    assertNotNull(
                        archivePickDetails.getSerialNumber(),
                        "Archive Serial Number is Null");
                }
            }
        }*/

    }

}
