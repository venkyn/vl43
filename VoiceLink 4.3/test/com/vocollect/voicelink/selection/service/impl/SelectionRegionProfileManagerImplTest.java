/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.selection.model.SelectionRegionProfile;
import com.vocollect.voicelink.selection.service.SelectionRegionProfileManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * Unit test for the SelectionRegionManagerImpl.
 * 
 * @author bnorthrop
 */
@Test(groups = { FAST, SELECTION })
public class SelectionRegionProfileManagerImplTest extends
    BaseServiceManagerTestCase {

    private SelectionRegionProfileManager manager;

    /**
     * @param impl - implementation of selection region manager.
     */
    @Test(enabled = false)
    public void setSelectionRegionProfileManager(SelectionRegionProfileManager impl) {
        this.manager = impl;
    }

    /**
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     * @param adapter - to load the dbunit data
     * @throws Exception - any exception
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
    }

    /**
     * Test some stuff.
     * @throws Exception - any error
     */
    public void textGetSelectionRegionProfile() throws Exception {
        final Long profileId = new Long(-1L);
        SelectionRegionProfile profile = this.manager.get(profileId);
        log.debug("profile: " + profile.getDescriptiveText());
        assertTrue(profileId.equals(profile.getId()), "IDs don't match!");
    }
}
