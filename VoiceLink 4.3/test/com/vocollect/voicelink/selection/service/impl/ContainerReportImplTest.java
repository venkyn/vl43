/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.selection.model.ContainerReport;
import com.vocollect.voicelink.selection.service.ArchiveAssignmentManager;
import com.vocollect.voicelink.selection.service.ArchiveContainerManager;
import com.vocollect.voicelink.selection.service.ContainerManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.annotations.Test;

/**
 * Unit test case class for Container Modified Report
 * @author Yash Arora
 *
 */
@Test(groups = { FAST, CORE, SECURITY })
public class ContainerReportImplTest extends BaseServiceManagerTestCase {

    // Parameter values
    Set<String> operatorId = new HashSet<String>();

    Set<Long> operatorTeam = new HashSet<Long>();

    String containerNumber;

    Date startTime;

    Date endTime;

    String siteId = "-1"; // Site ID for Default Site.

    private ContainerManager containerManager;

    private ArchiveContainerManager archiveContainerManager;

    /**
     * Getter for ContainerManager
     * @return containerManager
     */
    public ContainerManager getContainerManager() {
        return containerManager;
    }

    /**
     * Setter for ContainerManager
     * @param containerMngr for setting the assignment manager
     */
    @Test(enabled = false)
    public void setContainerManager(ContainerManager containerMngr) {
        this.containerManager = containerMngr;
    }



    /**
     * Getter for the archiveContainerManager property.
     * @return ArchiveContainerManager value of the property
     */
    @Test(enabled = false)
    public ArchiveContainerManager getArchiveContainerManager() {
        return archiveContainerManager;
    }


    /**
     * Setter for the archiveContainerManager property.
     * @param archiveContainerManager the new archiveContainerManager value
     */
    @Test(enabled = false)
    public void setArchiveContainerManager(ArchiveContainerManager archiveContainerManager) {
        this.archiveContainerManager = archiveContainerManager;
    }

    /**
     * Getter for ArchiveAssignmentManager
     * @return archiveAssignmentManager
     */
    public ArchiveAssignmentManager getArchiveAssignmentManager() {
        return archiveAssignmentManager;
    }

    /**
     * Setter for ArchiveAssignmentManager
     * @param archiveAssignmentManager for setting the archive assignment
     *            manager
     */
    @Test(enabled = false)
    public void setArchiveAssignmentManager(ArchiveAssignmentManager archiveAssignmentManager) {
        this.archiveAssignmentManager = archiveAssignmentManager;
    }

    private ArchiveAssignmentManager archiveAssignmentManager = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        // We are not using this method to load data.
        // We are using a loadData method so we can seperate the tests into
        // multiple
        // test methods and not have to load the data before each test.
    }

    /**
     * Test Container values for null.
     * @throws DataAccessException
     */
    @Test()
    public void testContainerValuesforNull() throws DataAccessException {
        operatorId.add(" ");
        List<ContainerReport> containerList = getContainerManager()
            .listContainersForContainerReport(new QueryDecorator(),
                containerNumber, operatorId, startTime, endTime);

        for (ContainerReport containerObj : containerList) {
            assertNotNull(
                containerObj.getContainerNumber(),
                "Container Number is Null");
            assertNotNull(
                containerObj.getContainerStatus(), "Container Status is Null");

            //Pick asserts
            assertNotNull(
                containerObj.getItemDescription(),
                "Item Description is Null");
            assertNotNull(
                containerObj.getItemNumber(), "Item Number is Null");
            assertNotNull(containerObj.getAisle(), "Aisle is Null");
            assertNotNull(
                containerObj.getOperatorId(), "Operator Id is Null");
            assertNotNull(
                containerObj.getOperatorName(), "Operator Name is Null");
            assertNotNull(containerObj.getPickId(), "Pick Id is Null");
            assertNotNull(containerObj.getPickStatus(), "Pick Status is Null");
            assertNotNull(containerObj.getPickTime(), "Pick Time is Null");
            assertNotNull(
                containerObj.getPostAisle(), "Post Aisle is Null");
            assertNotNull(
                containerObj.getPreAisle(), "Pre Aisle is Null");
            assertNotNull(
                containerObj.getQuantityPicked(), "Quantity picked is Null");
            assertNotNull(containerObj.getSlot(), "Slot is Null");

            // pickDtlObj.
            assertNotNull(
                containerObj.getSerialNumber(), "Serial Number is Null");
            assertNotNull(
                containerObj.getVariableWeight(),
                "Variable Weight is Null");
            assertNotNull(containerObj.getPickDetailId(), "Pick Detail ID is Null");
            assertNotNull(
                containerObj.getLotNumber(), "Lot Number is Null");
        }
    }

    /**
     * Test ArchiveContainer values for null.
     * @throws DataAccessException
     */
    @Test()
    public void testArchiveContainerValuesforNull() throws DataAccessException {
        operatorId.add(" ");
        List<ContainerReport> archiveContainerList = getArchiveContainerManager()
            .listArchiveContainersForContainerReport(new QueryDecorator(),
                containerNumber, operatorId, startTime, endTime,
                Long.valueOf(siteId));
        for (ContainerReport containerObj : archiveContainerList) {
            assertNotNull(
                containerObj.getContainerNumber(),
                "Archive Container Number is Null");
            assertNotNull(
                containerObj.getContainerStatus(),
                "Archive Container Status is Null");

            assertNotNull(
                containerObj.getItemDescription(),
                "Archive Assignment Description is Null");
            assertNotNull(containerObj.getItemNumber(), "Item Number is Null");
            assertNotNull(containerObj.getOperatorId(), "Operator Id is Null");
            assertNotNull(
                containerObj.getOperatorName(), "Operator Name is Null");
            assertNotNull(containerObj.getPickId(), "Pick Id is Null");
            assertNotNull(containerObj.getPickStatus(), "Pick Status is Null");
            assertNotNull(containerObj.getPickTime(), "Pick Time is Null");
            assertNotNull(
                containerObj.getQuantityPicked(), "Quantity Picked is Null");

            assertNotNull(
                containerObj.getSerialNumber(), "Serial Number is Null");
            assertNotNull(
                containerObj.getVariableWeight(), "Variable Weight is Null");
            assertNotNull(
                containerObj.getPickDetailId(), "Pick Detail Id is Null");
            assertNotNull(containerObj.getLotNumber(), "Lot Number is Null");

        }


    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return getVLArchiveDAOTestConfigLocations();
    }



}
