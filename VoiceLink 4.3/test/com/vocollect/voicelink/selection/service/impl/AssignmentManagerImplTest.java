/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Customer;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.selection.SelectionErrorCode;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentGroup;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.AssignmentSummary;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.model.SelectionRegion;
import com.vocollect.voicelink.selection.model.WorkIdentifier;
import com.vocollect.voicelink.selection.service.AssignmentManager;
import com.vocollect.voicelink.selection.service.PickManager;
import com.vocollect.voicelink.util.TestUtils;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.PERFORMANCE;
import static com.vocollect.epp.test.TestGroups.SLOW;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dbunit.operation.DatabaseOperation;
import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * 
 * 
 * @author snalan
 */
@Test(groups = { FAST, SELECTION })
public class AssignmentManagerImplTest extends BaseServiceManagerTestCase {

    private AssignmentManager manager = null;

    private PickManager pickManager = null;

    private OperatorManager operatorManager = null;

    private int recordCount = 5;

    private SelectionRegion selectionRegion;

    private WorkIdentifier workIdentifier;

    private Operator operator;

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        this.recordCount = getRecordCount();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
    }

    /**
     * @return - pick manager
     */
    @Test(enabled = false)
    public PickManager getPickManager() {
        return pickManager;
    }

    /**
     * @param pickManager - set pick manager
     */
    @Test(enabled = false)
    public void setPickManager(PickManager pickManager) {
        this.pickManager = pickManager;
    }

    /**
     * @param impl - implementation of assignment manager.
     */
    @Test(enabled = false)
    public void setAssignmentManager(AssignmentManager impl) {
        this.manager = impl;
    }

    /**
     * @return the operatorManager
     */
    @Test(enabled = false)
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * @param operatorManager the operatorManager to set
     */
    @Test(enabled = false)
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * @throws Exception - any exception
     */
    @Test(enabled = false)
    public void getObjectsFromAssignment() throws Exception {
        List<Assignment> assignments = manager.getAll();
        Assignment assignment = assignments.get(0);
        selectionRegion = assignment.getRegion();
        workIdentifier = assignment.getWorkIdentifier();
        operator = assignment.getOperator();
    }

    /**
     * @throws Exception - any exception
     */
    @Test(groups = { PERFORMANCE, SLOW })
    public void testSaveAssignments() throws Exception {

        getObjectsFromAssignment();
        for (int i = 1; i <= recordCount; i++) {
            Assignment assignment = getAssignmentObj(i);
            manager.save(assignment);
        }
        logTimeTaken("Time taken to save " + recordCount
            + " Assignment records : ");

    }

    /**
     * @throws Exception - any exception
     */
    @Test(groups = { PERFORMANCE, SLOW }, dependsOnMethods = { "testSaveAssignments" })
    public void testGetAssignments() throws Exception {

        manager.getAll();
        logTimeTaken("Time taken to retreive " + recordCount
            + " Assignment records : ");
    }

    /**
     * @throws Exception - any exception
     */
    @Test(groups = { PERFORMANCE, SLOW }, dependsOnMethods = { "testGetAssignments" })
    public void testDeleteAssignments() throws Exception {

        List<Assignment> assignments = manager.getAll();
        for (Iterator<Assignment> itr = assignments.iterator(); itr.hasNext();) {
            Assignment assignment = itr.next();

            // A temporary fix for eliminating the lazy loading issue, while
            // running the test case from the Ant task
            // System.err.println(assignment.getPicks()+""+assignment.getContainers());

            manager.delete(assignment.getId());
        }
        logTimeTaken("Time taken to delete " + recordCount
            + " Assignment records : ");
    }

    /**
     * @param number - number
     * @return the assignment object
     */
    @Test(enabled = false)
    private Assignment getAssignmentObj(int number) {

        Assignment assignment = new Assignment();
        assignment.setCalculatedBaseItems(false);
        assignment.setCreatedDate(new Date());
        assignment.setCustomerInfo(new Customer());
        assignment.setDeliveryDate(new Date());
        assignment.setDeliveryLocation("Location");
        assignment.setEndTime(new Date());
        assignment.setGroupInfo(new AssignmentGroup());
        assignment.setNumber(new Long(1));
        assignment.setOperator(operator);
        assignment.setRegion(selectionRegion);
        assignment.setReservedBy("Reserved By");
        assignment.setRoute("Route");
        assignment.setSplitNumber(new Long(1));
        assignment.setStartTime(new Date());
        assignment.setSummaryInfo(new AssignmentSummary());
        assignment.setVersion(100);
        assignment.setWorkIdentifier(workIdentifier);

        return assignment;

    }

    /**
     * @return - return record count
     */
    private int getRecordCount() {
        String recordNumber = System.getProperty("test.parameter.records");
        int recCount = 0;
        try {
            recCount = Integer.parseInt(recordNumber);
        } catch (NumberFormatException ex) {
            recCount = recordCount;
        }

        return recCount;
    }

    /**
     * Test for splitting assignments.
     * 
     * @throws Exception - Any exception
     */
    public void testExecuteCreateSplitAssignment() throws Exception {

        initData(new DbUnitAdapter());

        // ==============================================================
        // Ensure the original assignment isn't split already
        List<Assignment> assignments = manager
            .listAssignmentsByNumber(101107371L);

        assertEquals(1, assignments.size(), "AssignmentNotYetSplit");

        // ============================================================
        // Split once
        ArrayList<Pick> picks = new ArrayList<Pick>();

        picks.add(getPickManager().get(-389L));
        picks.add(getPickManager().get(-391L));
        picks.add(getPickManager().get(-393L));

        manager.executeCreateSplitAssignment(picks, null, null);

        assignments = manager.listAssignmentsByNumber(101107371L);

        assertEquals(2, assignments.size(), "AssignmentSplit2");

        // ============================================================
        // Split a second time
        picks.clear();
        picks.add(getPickManager().get(-394L));

        manager.executeCreateSplitAssignment(picks, null, null);

        // Validate 3 assignments returned
        assignments = manager.listAssignmentsByNumber(101107371L);
        assertEquals(3, assignments.size(), "AssignmentSplit3");

        // ============================================================
        // Test validation for all picks being split
        picks.clear();
        picks.add(getPickManager().get(-392L));
        picks.add(getPickManager().get(-390L));

        try {
            manager.executeCreateSplitAssignment(picks, null, null);
        } catch (BusinessRuleException e) {
            assertEquals(SelectionErrorCode.SPLIT_NOT_ALL_PICKS_ALLOWED,
                e.getErrorCode(), "CannotSplitAllPicks");
        }

        // ============================================================
        // Test validation not in group
        assignments = manager.listAssignmentsByNumber(101107354L);
        assignments.get(0).getGroupInfo().setGroupNumber(1L);
        manager.save(assignments.get(0));

        picks.clear();
        picks.add(getPickManager().get(-370L));
        try {
            manager.executeCreateSplitAssignment(picks, null, null);
        } catch (BusinessRuleException e) {
            assertEquals(SelectionErrorCode.SPLIT_IN_GROUP, e.getErrorCode(),
                "CannotSplitGroup");
        }

        // ============================================================
        // Test validation assignment status
        assignments = manager.listAssignmentsByNumber(101107354L);
        assignments.get(0).changeStatus(AssignmentStatus.Canceled);
        assignments.get(0).getGroupInfo().setGroupNumber(null);
        manager.save(assignments.get(0));

        try {
            manager.executeCreateSplitAssignment(picks, null, null);
        } catch (BusinessRuleException e) {
            assertEquals(SelectionErrorCode.SPLIT_INVALID_ASSIGNMENT_STATUS,
                e.getErrorCode(), "InvalidAssignmentStatus");
        }

        // ============================================================
        // Test validation case label check digit
        picks.clear();
        picks.add(getPickManager().get(-1508L));
        try {
            manager.executeCreateSplitAssignment(picks, null, null);
        } catch (BusinessRuleException e) {
            assertEquals(SelectionErrorCode.SPLIT_CASE_LABEL_CD,
                e.getErrorCode(), "CannotSplitCaseLabelCD");
        }

        // ============================================================
        // Test validation target containers
        picks.clear();
        picks.add(getPickManager().get(-2828L));
        try {
            manager.executeCreateSplitAssignment(picks, null, null);
        } catch (BusinessRuleException e) {
            assertEquals(SelectionErrorCode.SPLIT_TARGET_CONTAINERS,
                e.getErrorCode(), "CannotSplitTargetContainers");
        }

        // ============================================================
        // Test validation Invalid pick status
        picks.clear();
        picks.add(getPickManager().get(-372L));
        picks.get(0).cancelPick();
        getPickManager().save(picks.get(0));

        try {
            manager.executeCreateSplitAssignment(picks, null, null);
        } catch (BusinessRuleException e) {
            assertEquals(SelectionErrorCode.SPLIT_INVALID_PICK_STATUS,
                e.getErrorCode(), "InvalidPickStatus");
        }

    }

    /**
     * Test for grouping assignments.
     * 
     * @throws Exception - Any exception
     */
    public void testExecuteGroupAssignments() throws Exception {
        Long groupNumber = 0L;
        Long groupNumber2 = 0L;

        initData(new DbUnitAdapter());
        manager.initializeState();

        // ============================================================
        // Group 2 Assignments
        ArrayList<Assignment> assignments = new ArrayList<Assignment>();

        assignments.add(manager.get(-689L));
        assignments.add(manager.get(-690L));

        manager.executeGroupAssignments(assignments, true);
        groupNumber = assignments.get(0).getGroupInfo().getGroupNumber();

        List<Assignment> results = manager.listAssignmentsInGroup(groupNumber);

        assertEquals(results.size(), 2, "AssignmentGroup2");

        // ============================================================
        // Add assignment to group
        assignments.clear();
        assignments.add(manager.get(-689L));
        assignments.add(manager.get(-690L));
        assignments.add(manager.get(-691L));

        manager.executeGroupAssignments(assignments, true);

        groupNumber = assignments.get(0).getGroupInfo().getGroupNumber();
        results = manager.listAssignmentsInGroup(groupNumber);

        assertEquals(results.size(), 3, "AssignmentGroupAdd");

        // ============================================================
        // Group grouped assignment to another group
        assignments.clear();
        assignments.add(manager.get(-691L));
        assignments.add(manager.get(-692L));

        manager.executeGroupAssignments(assignments, true);

        groupNumber2 = groupNumber;
        results = manager.listAssignmentsInGroup(groupNumber2);
        assertEquals(results.size(), 2, "AssignmentGroupMoveRemaining");
        groupNumber = assignments.get(0).getGroupInfo().getGroupNumber();
        results = manager.listAssignmentsInGroup(groupNumber);
        assertEquals(results.size(), 2, "AssignmentGroupMoveNew");

        // ============================================================
        // Move leaving 1 out of group
        assignments.clear();
        assignments.add(manager.get(-689L));
        assignments.add(manager.get(-692L));

        manager.executeGroupAssignments(assignments, true);

        results = manager.listAssignmentsInGroup(groupNumber2);
        assertEquals(results.size(), 0, "AssignmentGroupMoveRemaining");
        results = manager.listAssignmentsInGroup(groupNumber);
        assertEquals(results.size(), 0, "AssignmentGroupMoveNew");
        groupNumber = assignments.get(0).getGroupInfo().getGroupNumber();
        results = manager.listAssignmentsInGroup(groupNumber);
        assertEquals(results.size(), 2, "AssignmentGroupMoveNew");

        // ============================================================
        // Validate Same Region
        assignments.clear();
        assignments.add(manager.get(-276L));
        assignments.add(manager.get(-50L));
        validateGroup(assignments, "NotSameRegion",
            SelectionErrorCode.GROUP_NOT_SAME_REGION, false);

        // ============================================================
        // Validate Same Type
        assignments.clear();
        assignments.add(manager.get(-50L));
        assignments.get(0).setType(AssignmentType.Chase);
        manager.save(assignments.get(0));
        assignments.add(manager.get(-49L));
        validateGroup(assignments, "NotSameType",
            SelectionErrorCode.GROUP_NOT_SAME_TYPE, false);

        // ============================================================
        // Validate Status
        assignments.clear();
        assignments.add(manager.get(-50L));
        assignments.get(0).setType(AssignmentType.Normal);
        assignments.get(0).changeStatus(AssignmentStatus.Canceled);
        manager.save(assignments.get(0));
        assignments.add(manager.get(-49L));
        validateGroup(assignments, "InvalidStatus",
            SelectionErrorCode.GROUP_INVALID_ASSIGNMENT_STATUS, false);

        // ============================================================
        // Validate Not allowed to group case label check digits
        assignments.clear();
        assignments.add(manager.get(-190L));
        assignments.add(manager.get(-191L));
        validateGroup(assignments, "NotAllowedCaseLabelCD",
            SelectionErrorCode.GROUP_NO_CASE_LABEL_CD, false);

        // ============================================================
        // Validate Region Not Allowed Multiples
        assignments.clear();
        assignments.add(manager.get(-98L));
        assignments.add(manager.get(-99L));
        validateGroup(assignments, "NotAllowedMultiples",
            SelectionErrorCode.GROUP_REGION_NOT_ALLOW_MULT, false);

        // ============================================================
        // Validate More than allowed
        assignments.clear();
        assignments.add(manager.get(-322L));
        assignments.add(manager.get(-323L));
        assignments.add(manager.get(-324L));
        assignments.add(manager.get(-325L));
        validateGroup(assignments, "TooManyAssignments",
            SelectionErrorCode.GROUP_TOO_MANY_ASSIGNMENTS, false);

        // ============================================================
        // Validate 2Assignments Required
        assignments.clear();
        assignments.add(manager.get(-322L));
        validateGroup(assignments, "2AssignmentsRequired",
            SelectionErrorCode.GROUP_2_ASSIGNMENTS_REQUIRED, false);

        // ============================================================
        // Validate Auto region only from GUI
        assignments.clear();
        assignments.add(manager.get(-46L));
        assignments.add(manager.get(-47L));
        validateGroup(assignments, "AutoIssuanceFromGui",
            SelectionErrorCode.GROUP_NOT_AUTO_ISSUANCE, true);

    }

    /**
     * Run a group command to validate grouping error.
     * 
     * @param assignments - list of assignments to group
     * @param message - message for assert
     * @param errorCode - error code expected
     * @param fromGUI - grouping from GUI or not
     * @throws DataAccessException - database exceptions
     */
    protected void validateGroup(ArrayList<Assignment> assignments,
                                 String message,
                                 SelectionErrorCode errorCode,
                                 boolean fromGUI) throws DataAccessException {

        try {
            manager.executeGroupAssignments(assignments, fromGUI);
        } catch (BusinessRuleException e) {
            assertEquals(e.getErrorCode(), errorCode, message);
        }

    }

    /**
     * Unit tests for ungrouping.
     * 
     * @throws Exception - any
     */
    public void testExecuteUngroupAssignments() throws Exception {
        Long groupNumber = 0L;

        initData(new DbUnitAdapter());
        manager.initializeState();

        // ============================================================
        // Group 2 Assignments
        ArrayList<Assignment> assignments = new ArrayList<Assignment>();

        assignments.add(manager.get(-689L));
        assignments.add(manager.get(-690L));
        assignments.add(manager.get(-691L));

        manager.executeGroupAssignments(assignments, true);
        groupNumber = assignments.get(0).getGroupInfo().getGroupNumber();
        List<Assignment> results = manager.listAssignmentsInGroup(groupNumber);
        assertEquals(results.size(), 3, "AssignmentUngroupGroup");

        // Remove 1 from group, ensure 2 remain
        assignments.clear();
        assignments.add(manager.get(-689L));
        manager.executeUngroupAssignments(assignments, true);
        results = manager.listAssignmentsInGroup(groupNumber);
        assertEquals(results.size(), 2, "AssignmentUngroup1");

        // Remove 1 from group, ensure none remain
        assignments.clear();
        assignments.add(manager.get(-690L));
        manager.executeUngroupAssignments(assignments, true);
        results = manager.listAssignmentsInGroup(groupNumber);
        assertEquals(results.size(), 0, "AssignmentUngroupAll");

        // Create Groups
        manager.initializeState();
        assignments.clear();
        assignments.add(manager.get(-689L));
        assignments.add(manager.get(-690L));

        manager.executeGroupAssignments(assignments, true);
        results = manager.listAssignmentsInGroup(assignments.get(0)
            .getGroupInfo().getGroupNumber());
        assertEquals(results.size(), 2, "AssignmentUngroupGroup1");

        assignments.clear();
        assignments.add(manager.get(-691L));
        assignments.add(manager.get(-692L));

        manager.executeGroupAssignments(assignments, true);
        results = manager.listAssignmentsInGroup(assignments.get(0)
            .getGroupInfo().getGroupNumber());
        assertEquals(results.size(), 2, "AssignmentUngroupGroup2");

        // Test Validations not in group
        assignments.clear();
        assignments.add(manager.get(-693L));
        validateUngroup(assignments, "NotInGroup",
            SelectionErrorCode.UNGROUP_NOT_ALL_IN_GROUP, true);

        // Test Validations not same group
        assignments.clear();
        assignments.add(manager.get(-689L));
        assignments.add(manager.get(-691L));
        validateUngroup(assignments, "NotInSameGroup",
            SelectionErrorCode.UNGROUP_NOT_ALL_SAME_GROUP, true);

        // Test Validations from task must be manual issuance region
        assignments.clear();
        assignments.add(manager.get(-689L));
        assignments.get(0).changeStatus(AssignmentStatus.Canceled);
        validateUngroup(assignments, "UngroupInvalidStatus",
            SelectionErrorCode.UNGROUP_INVALID_STATUS, true);

    }

    /**
     * Method to check for ungrouping errors.
     * 
     * @param assignments - assignments to ungroup
     * @param message - message to display
     * @param errorCode - error code expected
     * @param fromGUI - called from gui flag
     * @throws DataAccessException - database Exceptions
     */
    protected void validateUngroup(ArrayList<Assignment> assignments,
                                   String message,
                                   SelectionErrorCode errorCode,
                                   boolean fromGUI) throws DataAccessException {

        try {
            manager.executeUngroupAssignments(assignments, fromGUI);
        } catch (BusinessRuleException e) {
            assertEquals(e.getErrorCode(), errorCode, message);
        }

    }

    /**
     * Method to test getting assignments for a given region.
     * 
     * @throws Exception - any exception
     */
    public void testListAssignmentsInRegion() throws Exception {

        // Region -13 should have multiple assignments in it.
        final Long hasAssignmentsRegionId = new Long(-13L);
        List<AssignmentType> assignmentList = new ArrayList<AssignmentType>();
        assignmentList.add(AssignmentType.Normal);
        List<Assignment> assignments = manager.listAssignmentsInRegion(
            hasAssignmentsRegionId, assignmentList);
        assertNotNull(assignments,
            "assignments for region should not be null: "
                + hasAssignmentsRegionId);
        assertEquals(assignments.size(), 5,
            "should be 5 assignments for region: " + hasAssignmentsRegionId);
        assignmentList.clear();
        assignmentList.add(AssignmentType.Chase);
        assignments = manager.listAssignmentsInRegion(hasAssignmentsRegionId,
            assignmentList);
        assertEquals(assignments.size(), 0,
            "should have 0 chase assignments for region: "
                + hasAssignmentsRegionId);

        // Invalid region - no assignments
        Long invalidRegionId = new Long(-6969);
        assignmentList.clear();
        assignmentList.add(AssignmentType.Normal);
        assignments = manager.listAssignmentsInRegion(invalidRegionId,
            assignmentList);
        assertEquals(assignments.size(), 0, "assignments should be 0 size for "
            + invalidRegionId);
    }

    /**
     * @throws Exception
     */
    public void testPriorities() throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm:ss");
        Date deliveryDate = sdf.parse("08/01/02 00:00:00");

        Integer priority = 1;

        List<String> customers = manager.listCustomerNumbers();
        List<String> routes = manager.listRoutes();

        assertTrue(customers.size() > 0,
            "Customer list should be greater than 0");
        assertTrue(routes.size() > 0, "Routes list should be greater than 0");

        // Check by customer number
        int results = manager.executeUpdatePriority(1, customers.get(0),
            priority, deliveryDate, deliveryDate);
        assertEquals(0, results, "Expected 0 records updated");

        results = manager.executeUpdatePriority(0, customers.get(0), priority,
            deliveryDate, deliveryDate);
        assertTrue(results > 0, "Expected at least 1 record updated");

        List<Assignment> assignments = manager.getAll();

        int count = 0;
        for (Assignment a : assignments) {
            if (customers.get(0)
                .equals(a.getCustomerInfo().getCustomerNumber())) {
                assertEquals(priority, a.getPriority(),
                    "Expected priority to be 1");
                count++;
            }
        }
        assertEquals(results, count, "Wrong number of results found");

        // Check by route number
        priority = 2;
        results = manager.executeUpdatePriority(0, routes.get(0), priority,
            deliveryDate, deliveryDate);
        assertEquals(0, results, "Expected 0 records updated");

        results = manager.executeUpdatePriority(1, routes.get(0), priority,
            deliveryDate, deliveryDate);
        assertTrue(results > 0, "Expected at least 1 record updated");

        assignments = manager.getAll();

        count = 0;
        for (Assignment a : assignments) {
            if (routes.get(0).equals(a.getRoute())) {
                assertEquals(priority, a.getPriority(),
                    "Expected priority to be 1");
                count++;
            }
        }
        assertEquals(results, count, "Wrong number of results found");

    }

    /**
     * Tests to see if any new JavaBean properties for Assignment haven't been
     * set in executeCreateSplitAssignment().
     * 
     * If this test fails, ensure that you've overridden the
     * AssignmentManagerImpl.executeCreateSplitAssignment() method and have
     * added any new assignment members that you created to it.
     * @throws Exception - any
     */
    @Test
    public void testExecuteCreateSplitAssignmentOverridden() throws Exception {
        // Create new TestUtils instance to use in this test
        TestUtils tu = new TestUtils();

        // find any custom assignment fields that have both a setter and getter
        // get the getter and setter methods to invoke for these fields
        List<Method> setters = new ArrayList<Method>();
        List<Method> getters = new ArrayList<Method>();
        tu.getSettersAndGettersToExecute(Assignment.class, setters, getters);

        // The purpose of this test is to insure new javabeans are included in
        // the
        // executeCreateSplitAssignment() method...
        if (setters.isEmpty() && getters.isEmpty()) {
            // If we don't have any new javabeans, this test isn't needed so
            // exit.
            return;
        }

        // Load data as needed
        initData(new DbUnitAdapter());

        // Lookup all assignments with this number (there should be only 1)
        List<Assignment> assignments = manager
            .listAssignmentsByNumber(101107354L);

        // Ensure that there is only 1.
        assertEquals(1, assignments.size(),
            "Expecting 1 Assignment and found something else.");

        // Build a list containing a subset of the assignments picks to be split
        ArrayList<Pick> picks = new ArrayList<Pick>();
        picks.add(getPickManager().get(-370L));
        picks.add(getPickManager().get(-371L));

        // execute the setters on the original Assignment
        try {
            tu.invokeSetters(assignments.get(0), setters);
        } catch (RuntimeException e) {
            Assert
                .fail("Your Assignment.java customization includes a member whose type"
                    + " (shown at end of this message) is not supported by this test."
                    + " Please verify that you have overridden the"
                    + " AssignmentManagerImpl.executeCreateSplitAssignment() method"
                    + " to include any additional members that you have added to Assignment.java. "
                    + e.getMessage());
        }

        // tell the manager to split the listed picks from the assignment
        manager.executeCreateSplitAssignment(picks, null, null);

        // Lookup all assignments with this number (there should now be 2)
        assignments.clear();
        assignments = manager.listAssignmentsByNumber(101107354L);

        // Ensure that there are 2.
        assertEquals(2, assignments.size(), "AssignmentSplitIn2ForOverride");

        // check the 2 assignments and make sure that both have the same values
        // for the custom fields.
        // NOTE: If this failed, ensure you have overridden the
        // executeCreateSplitAssignment and added any custom fields to it.
        tu.invokeGettersAndCompare(assignments.get(0), assignments.get(1),
            getters);
    }

    /**
     * Tests to see suspended assignment with no operator name can be retrieved
     * as per details and scenario mentioned in VLINK-3020.
     * 
     * @throws Exception - any
     */
    @Test
    public void testVlink3020() throws Exception {
        final Long SUSPENDED_ASSIGNMENT_NUMBER = 112107364L;
        List<Assignment> assignments = manager
            .listAssignmentsByNumber(SUSPENDED_ASSIGNMENT_NUMBER); // suspended
                                                                   // assignment
        Assignment suspendedAssignment = assignments.get(0);

        Operator currentOperator = suspendedAssignment.getOperator();
        WorkIdentifier workId = suspendedAssignment.getWorkIdentifier();

        // edit assignment
        suspendedAssignment.setOperator(null);
        manager.executeAssignmentChangeSave(suspendedAssignment);

        // fetch assignment by work id with the same operator
        List<Assignment> retreivedAssignments = manager
            .listAssignmentsByWorkId(new QueryDecorator(),
                workId.getWorkIdentifierValue(), currentOperator,
                suspendedAssignment.getType(), suspendedAssignment.getRegion());
        Assignment retreivedAssignment = retreivedAssignments.get(0);
        assertEquals(1, retreivedAssignments.size());
        assertEquals(SUSPENDED_ASSIGNMENT_NUMBER,
            retreivedAssignment.getNumber());
        assertNull(retreivedAssignment.getOperator());

        // reset
        retreivedAssignments = null;
        retreivedAssignment = null;

        // fetch assignment by partial work id with the same operator
        retreivedAssignments = manager.listAssignmentsByPartialWorkId(
            new QueryDecorator(), workId.getPartialWorkIdentifier(),
            currentOperator, suspendedAssignment.getType(),
            suspendedAssignment.getRegion());
        retreivedAssignment = retreivedAssignments.get(0);
        assertEquals(1, retreivedAssignments.size());
        assertEquals(SUSPENDED_ASSIGNMENT_NUMBER,
            retreivedAssignment.getNumber());
        assertNull(retreivedAssignment.getOperator());

        validateUngroup((ArrayList<Assignment>) retreivedAssignments,
            "UngroupNotAllInGroup",
            SelectionErrorCode.UNGROUP_NOT_ALL_IN_GROUP, true);
    }

    @Test
    public void testAssignmentCountByRouteDDTAndStatus()
        throws DataAccessException {
        List<Object[]> objectList = manager
            .listAssignmentCountByRouteDDTAndStatus(new Date(1028100000000L),
                new Date(1028180200000L),
                Arrays.asList(AssignmentStatus.values()));

        assertNotNull(objectList);
        assertTrue(objectList.size() > 0);

        Long r18InProgressCount = 0L;
        Long r18AvailableCount = 0L;

        for (Object[] obj : objectList) {
            if (((String) obj[0]).equals("18")) {
                if (((AssignmentStatus) obj[3]) == AssignmentStatus.Available) {
                    r18AvailableCount = (Long) obj[2];
                }

                if (((AssignmentStatus) obj[3]) == AssignmentStatus.InProgress) {
                    r18InProgressCount = (Long) obj[2];
                }
            }
        }

        assertEquals(r18InProgressCount, new Long(1));
        assertEquals(r18AvailableCount, new Long(11));
    }

    @Test
    public void testTotalQuantityAvailablePerRouteDDTByRegion()
        throws DataAccessException {
        List<Object[]> objectList = manager
            .listTotalQuantityAvailablePerRouteDDTByRegion(new Date(
                1028100000000L), new Date(1028180200000L));

        assertNotNull(objectList);
        assertTrue(objectList.size() > 0);

        Long qtyCount = 0L;

        for (Object[] obj : objectList) {
            if (((String) obj[0]).equals("32")) {
                qtyCount += (Long) obj[3];
            }
        }

        assertEquals(qtyCount, new Long(95));
    }

    @Test
    public void testQuantityPickedPerRouteDDT() throws DataAccessException {
        List<Object[]> objectList = manager.listQuantityPickedPerRouteDDT(
            new Date(1028100000000L), new Date(1028180200000L));

        assertNotNull(objectList);
        assertTrue(objectList.size() > 0);

        Long r18QtyPicked = 0L;
        for (Object[] obj : objectList) {
            if (((String) obj[0]).equals("18")) {
                r18QtyPicked = (Long) obj[2];
            }
        }

        assertEquals(r18QtyPicked, new Long(10));
    }

    @Test
    public void testTotalQuantityPerRouteDDT() throws DataAccessException {
        List<Object[]> objectList = manager.listTotalQuantityPerRouteDDT(
            new Date(1028100000000L), new Date(1028180200000L));

        assertNotNull(objectList);
        assertTrue(objectList.size() > 0);

        Long r32TotalQty = 0L;
        for (Object[] obj : objectList) {
            if (((String) obj[0]).equals("32")) {
                r32TotalQty = (Long) obj[2];
            }
        }

        assertEquals(r32TotalQty, new Long(95));
    }

    @Test
    public void testTotalItemsRemainingForAllRegions()
        throws DataAccessException {
        List<Object[]> objectList = manager
            .listTotalItemsRemainingForAllRegions();

        assertNotNull(objectList);
        assertTrue(objectList.size() > 0);

        Long re1Count = 0L;
        Long re2Count = 0L;

        for (Object[] obj : objectList) {
            if (((Long) obj[0]) == -1) {
                re1Count = (Long) obj[1];
            }
            if (((Long) obj[0]) == -2) {
                re2Count = (Long) obj[1];
            }
        }

        assertEquals(re1Count, new Long(175));
        assertEquals(re2Count, new Long(46));
    }

    @Test
    public void testQtyPickedCasesChartData() throws DataAccessException {
        List<Object[]> objectList = manager
            .listQtyPickedCasesChartData(new Date(1020000000000L));

        assertNotNull(objectList);
        assertTrue(objectList.size() > 0);

        Long re1Count = 0L;

        for (Object[] obj : objectList) {
            if (((Long) obj[0]) == -1) {
                re1Count = (Long) obj[1];
            }
        }

        assertEquals(re1Count, new Long(7));
    }

    @Test
    public void testQtyRemainingCasesChartData() throws DataAccessException {
        List<Object[]> objectList = manager.listQtyRemainingCasesChartData();

        assertNotNull(objectList);
        assertTrue(objectList.size() > 0);

        Long re1Count = 0L;
        Long re2Count = 0L;

        for (Object[] obj : objectList) {
            if (((Long) obj[0]) == -1) {
                re1Count = (Long) obj[1];
            }
            if (((Long) obj[0]) == -2) {
                re2Count = (Long) obj[1];
            }
        }

        assertEquals(re1Count, new Long(80));
        assertEquals(re2Count, new Long(46));
    }

    @Test
    public void testAllQuantitiesByRegionDepartureDate()
        throws DataAccessException, BusinessRuleException, ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm:ss");
        Date deliveryDate = sdf.parse("08/01/02 00:00:00");

        Date startTime = (new DateTime(deliveryDate)).minusHours(1).toDate();
        Date endTime = (new DateTime(deliveryDate)).plusHours(1).toDate();

        List<Map<String, Object>> objMapList = manager
            .listAllQuantitiesByRegionDepartureDate(startTime, endTime);

        Long re1QtyPickedCount = 0L;
        Long re16QtyPickedCount = 0L;

        Long re1TotalQtyCount = 0L;
        Long re16TotalQtyCount = 0L;

        for (Map<String, Object> mapObj : objMapList) {
            Region regionObj = (Region) mapObj.get("Region");
            Long qtyPicked = (Long) mapObj.get("QuantityPicked");
            Long totalQty = (Long) mapObj.get("TotalQuantity");

            if (regionObj.getId() == -1) {
                qtyPicked = qtyPicked == null ? 0L : qtyPicked;
                totalQty = totalQty == null ? 0L : totalQty;

                re1QtyPickedCount += qtyPicked;
                re1TotalQtyCount += totalQty;
            }

            if (regionObj.getId() == -16) {
                qtyPicked = qtyPicked == null ? 0L : qtyPicked;
                totalQty = totalQty == null ? 0L : totalQty;

                re16QtyPickedCount += qtyPicked;
                re16TotalQtyCount += totalQty;
            }

        }

        assertEquals(re1QtyPickedCount, new Long(17));
        assertEquals(re1TotalQtyCount, new Long(97));
        assertEquals(re16QtyPickedCount, new Long(0));
        assertEquals(re16TotalQtyCount, new Long(0));
    }

    @Test
    public void testAssignmentsByDateAndStatusInAllRegions()
        throws DataAccessException {
        List<Map<String, Object>> objMapList = manager
            .listAssignmentsByDateAndStatusInAllRegions(
                new Date(1028100000000L), new Date(1028180200000L),
                Arrays.asList(AssignmentStatus.values()));

        assertNotNull(objMapList);
        assertTrue(objMapList.size() > 0);

        Long re1InProgressCount = 0L;
        Long re1AvailableCount = 0L;

        for (Map<String, Object> mapObj : objMapList) {
            Long regionId = (Long) mapObj.get("RegionId");
            Long assignmentCount = (Long) mapObj.get("AssignmentCount");

            if (regionId == -1) {
                assignmentCount = assignmentCount == null
                    ? 0L : assignmentCount;

                if (((AssignmentStatus) mapObj.get("AssignmentStatus")) == AssignmentStatus.Available) {
                    re1AvailableCount += assignmentCount;
                }

                if (((AssignmentStatus) mapObj.get("AssignmentStatus")) == AssignmentStatus.InProgress) {
                    re1InProgressCount += assignmentCount;
                }
            }
        }

        assertEquals(re1InProgressCount, new Long(1));
        assertEquals(re1AvailableCount, new Long(9));
    }

    @Test
    public void testQuantityPickedTodayForAllRoutes()
        throws DataAccessException, BusinessRuleException {

        for (Long i = -326L; i < -321; i++) {
            Assignment assignment = manager.get(i);
            assignment.setStartTime(new Date(1028130000000L));
            assignment.setEndTime(new Date(1028140000000L));
            manager.save(assignment);
        }

        List<Map<String, Object>> objMapList = manager.getPrimaryDAO()
            .listQuantityPickedTodayForAllRoutes(new Date(1020000000000L));

        assertNotNull(objMapList);
        assertTrue(objMapList.size() > 0);

        Long r30Val = 0L;
        Long r32Val = 0L;
        Long r33Val = 0L;

        for (Map<String, Object> mapObj : objMapList) {
            String route = (String) mapObj.get("Route");
            Long qtyRemaining = (Long) mapObj.get("TotalQuantityPicked");

            if (route.equals("30")) {
                r30Val = qtyRemaining;
            }
            if (route.equals("32")) {
                r32Val = qtyRemaining;
            }
            if (route.equals("33")) {
                r33Val = qtyRemaining;
            }
        }

        assertEquals(r30Val, new Long(0));
        assertEquals(r32Val, new Long(37));
        assertEquals(r33Val, new Long(101));

        for (Long i = -326L; i < -321; i++) {
            Assignment assignment = manager.get(i);
            assignment.setStartTime(null);
            assignment.setEndTime(null);
            manager.save(assignment);
        }
    }

    @Test
    public void testQuantityRemainingforAllRoutes() throws DataAccessException {
        List<Map<String, Object>> objMapList = manager.getPrimaryDAO()
            .listQuantityRemainingforAllRoutes();

        assertNotNull(objMapList);
        assertTrue(objMapList.size() > 0);

        Long r17Val = 0L;
        Long r18Val = 0L;
        Long r20Val = 0L;

        for (Map<String, Object> mapObj : objMapList) {
            String route = (String) mapObj.get("Route");
            Long qtyRemaining = (Long) mapObj.get("TotalQuantityRemaining");

            if (route.equals("17")) {
                r17Val = qtyRemaining;
            }
            if (route.equals("18")) {
                r18Val = qtyRemaining;
            }
            if (route.equals("20")) {
                r20Val = qtyRemaining;
            }
        }

        assertEquals(r17Val, new Long(0));
        assertEquals(r18Val, new Long(154));
        assertEquals(r20Val, new Long(100));
    }

    @Test
    public void testFindEarliestDDTByRouteDeliveryDate() throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm:ss");
        Date deliveryDate = sdf.parse("08/01/02 00:00:00");

        // finding earliest ddt of route with multiple departure dates
        Date ddt = manager.findEarliestDDTByRouteDeliveryDate("R3",
            deliveryDate);

        Date deptDate = sdf.parse("08/02/02 00:00:00");
        assertNotNull(ddt);
        assertEquals(ddt, deptDate);

        // finding ddt of route with no departure date
        Date ddt1 = manager.findEarliestDDTByRouteDeliveryDate("R4", new Date(
            1028140200000L));
        assertNotNull(ddt1.getTime());
    }

    @Test
    public void testListAssignmentsByRouteDeliveryDate() throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy hh:mm:ss");
        Date deliveryDate = sdf.parse("08/01/02 00:00:00");

        // updating ddt of route with multiple departure dates
        List<Assignment> assignmentList = manager
            .listAssignmentsByRouteDeliveryDate("R5", deliveryDate);

        assertEquals(assignmentList.size(), 2);

        List<Assignment> assignmentList1 = manager
            .listAssignmentsByRouteDeliveryDate("R8", deliveryDate);

        assertEquals(assignmentList1.size(), 0);

    }

    @Test
    public void testListAllOperatorsInRoutes() {
        List<Map<String, Object>> assignmentList = null;
        try {
            Assignment assignment = manager.get(-326L);
            assignment.setStatus(AssignmentStatus.InProgress);
            manager.save(assignment);
            assignmentList = manager.listAllOperatorsInRoutes();
        } catch (DataAccessException e) {
            e.printStackTrace();
            fail(e.getMessage());
        } catch (BusinessRuleException e) {
            e.printStackTrace();
        }

        assertEquals(assignmentList.size(), 0);
    }

    @Test
    public void testAllRouteByRegion() {
        List<Map<String, Object>> assignmentList = null;
        try {
            assignmentList = manager.listAllRouteByRegion();
        } catch (DataAccessException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        assertEquals(assignmentList.size(), 28);
    }

    @Test
    public void testRouteByLatestEndTime() {
        List<Map<String, Object>> assignmentList = null;
        try {
            assignmentList = manager.listRouteByLatestEndTime();
        } catch (DataAccessException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }

        assertEquals(assignmentList.size(), 0);
    }

}
