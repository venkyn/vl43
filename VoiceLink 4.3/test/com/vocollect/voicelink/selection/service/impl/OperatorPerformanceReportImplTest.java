/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.core.service.impl.OperatorPerformanceReportImplRoot.OperatorPerformace;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;


/**
 * Unit test case class for Operator Top/Bottom Performance Report
 * @author Yash Arora
 *
 */
@Test(groups = { FAST, CORE, SECURITY })
public class OperatorPerformanceReportImplTest  extends BaseServiceManagerTestCase {
    
    // Parameter values
    String operatorId;

    String containerNumber;

    Date startTime;

    Date endTime;

    String siteId = "-1"; // Site ID for Default Site.
    
    Long count;
    String sortOrder;
    Integer function;
    Long region = null;
    boolean reverseSort = false;
    OperatorLaborActionType action = null;
    
    
    private static final int OPERATOR_ID = 0;
    private static final int OPERATOR_NAME = 1;
    private static final int OPERATOR_ACTION = 2;
    private static final int TOTAL_DURATION = 3;
    private static final int TOTAL_COUNT = 4;
    private static final int GOAL_RATE = 5;
    
    
    private OperatorLaborManager operatorLaborManager;

    /**
     * Getter for OperatorLaborManager
     * @return operatorLaborManager
     */
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }

    /**
     * Setter for OperatorLaborManager
     * @param operatorLaborManager for setting the operatorlabor manager
     */
    @Test(enabled = false)
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase#initData(com.vocollect.epp.test.DbUnitAdapter)
     */
    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {
        // We are not using this method to load data.
        // We are using a loadData method so we can seperate the tests into
        // multiple
        // test methods and not have to load the data before each test.
        
    }
    
    /**
     * Test Container values for null.
     * @throws DataAccessException
     */
    @Test()
     public void testContainerValuesforNull() throws DataAccessException {
        
        List<OperatorPerformace> fullList = queryData(startTime, endTime, 
            action, reverseSort, region);
        for (OperatorPerformace op : fullList) {
            op.setPercentOfGoal();
        }
        
    }
    
    
    /**
     * Query performance data from database.
     * 
     * @param startDate - start time 
     * @param endDate - end time
     * @param fnctn - function to query
     * @param rvrseSort - should be sorted lowest to highest
     * @param rgion - region number
     * @return - full ist of objects.
     */
    protected List<OperatorPerformace> queryData(Date startDate, Date endDate,
                                               OperatorLaborActionType function,
                                               boolean reverseSort, Long region) {
        Map<String, OperatorPerformace> fullList = 
            new HashMap<String, OperatorPerformace>();
        List<Object[]> results = getOperatorLaborManager()
            .listPerformanceCounts(startDate, endDate, function, region);

        for (Object[] objs : results) {
            assertNotNull(((String) objs[OPERATOR_ID]), "Operator ID is Null");
            assertNotNull((String) objs[OPERATOR_NAME], "Operator Number is Null");
            assertNotNull(objs[OPERATOR_ACTION], "Operator Action is Null");
            assertNotNull(objs[GOAL_RATE], "Goal Rate is Null");
            assertNotNull(objs[TOTAL_DURATION], "Total duration is Null");
            assertNotNull(objs[TOTAL_COUNT], "Total Count is Null");   
            
        }

        return new ArrayList<OperatorPerformace>(fullList.values());
    }

    
    /**
     * Calculate the performane for each object in list.
     * @param fullList - list of operators and functions
     */
    protected void calculateOverallPerformance(List<OperatorPerformace> fullList) {
        for (OperatorPerformace op : fullList) {
            op.setPercentOfGoal();
        }
    }
}
