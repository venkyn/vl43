/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.logging.Logger;
import com.vocollect.voicelink.util.TestUtils;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;



/**
 * Test the Pick model class.
 * 
 * @author bnorthrop
 */
public class PickTest {

    private static final Logger log = new Logger(PickTest.class);

    /**
     * Test updating a pick to manual pick.
     */
    @Test
    public void testManualPick() {
        SelectionRegion region = new SelectionRegion();

        Assignment assignment = new Assignment();
        assignment.setType(AssignmentType.Normal);
        assignment.setRegion(region);

        // test happy path for manual pick
        Pick pick1 = new Pick();
        pick1.setQuantityToPick(5);
        assignment.addPick(pick1);
        try {
            pick1.updateManualPick(5);
        } catch (BusinessRuleException bre) {
            log.error(bre.getMessage(), bre.getErrorCode());
            Assert.fail("BusinessRuleException error!", bre);
        }

        // test where the quantity to pick is smaller than manual pick
        boolean gotError = false;
        Pick pick2 = new Pick();
        pick2.setQuantityToPick(5);
        assignment.addPick(pick2);
        try {
            pick2.updateManualPick(6);
        } catch (BusinessRuleException bre) {
            gotError = true;
        } finally {
            Assert
                    .assertTrue(gotError,
                            "Expected erro when quantity to pick is greater than manual pick.");
        }

        // test where the to pick is greater than manual pick
        Pick pick3 = new Pick();
        pick3.setQuantityToPick(5);
        assignment.addPick(pick3);
        try {
            pick3.updateManualPick(3);
        } catch (BusinessRuleException bre) {
            log.error(bre.getMessage(), bre.getErrorCode());
            Assert.fail("BusinessRuleException error!", bre);
        }

    }

    /**
     * Tests to see if any new JavaBean properties for Pick 
     * haven't been set in assignPickToChase().
     *
     * If this test fails, ensure that you've overridden the pick.assignPickToChase()
     * method and have added any new pick members that you created to it.
     * @throws Exception - any
     */
    @Test
    public void testAssignPickToChase() throws Exception {
        //Create new TestUtils instance to use in this test
        TestUtils tu = new TestUtils();

        // the original assignment and chase assignment must have the same
        // region
        SelectionRegion region = new SelectionRegion();
        region.setNumber(1);

        // original assignment for region validation
        Assignment originalAssignment = new Assignment();
        originalAssignment.setRegion(region);

        // create a chase assignment with the same region as the original
        Assignment chaseAssignment = new Assignment();
        chaseAssignment.setStatus(AssignmentStatus.Available);
        chaseAssignment.setType(AssignmentType.Chase);
        chaseAssignment.setRegion(region);

        // create a new short for the original assignment
        Pick pick = new Pick();
        pick.setType(PickType.Normal);
        pick.setStatus(PickStatus.Shorted);
        pick.setAssignment(originalAssignment);
        originalAssignment.addPick(pick);

        // find any custom Pick fields that have both a setter and getter
        // get the getter and setter methods to invoke for these fields
        List<Method> setters = new ArrayList<Method>();
        List<Method> getters = new ArrayList<Method>();
        tu.getSettersAndGettersToExecute(Pick.class, setters, getters);
        
        // execute the setters on the original pick
        // execute the setters on the original pick
        try {
            tu.invokeSetters(pick, setters);
        } catch (RuntimeException e) {
            Assert.fail("Your pick.java customization includes a member whose type"
                + " (shown at end of this message) is not supported by this test."
                + " Please verify that you have overridden the Pick.AssignPickToChase() method"
                + " to include any additional members that you have added to Pick.java. "
                + e.getMessage());
        }

        
        // assign original pick to chase
        pick.assignPickToChase(chaseAssignment);
        
        // check the pick on the chase assignment to make
        // sure that any custom fields have been set on the chase pick
        List<Pick> chasePicks = chaseAssignment.getPicks();
        Pick chasePick = chasePicks.get(0);
        
        // execute the getters and compare results
        tu.invokeGettersAndCompare(pick, chasePick, getters);
        
    }

    /**
     * Tests to see if any new JavaBean properties for Pick
     * haven't been set in Pick.getCopy() method.
     * *
     * If this test fails, ensure that you've overridden the pick.getCopy()
     * method and have added any new pick members that you created to it.
     * @throws Exception - any
     */
    @Test
    public void testGetCopy() throws Exception {
        //Create new TestUtils instance to use in this test
        TestUtils tu = new TestUtils();

        // create a new pick to be copied
        Pick pick = new Pick();
        
        // find any custom Pick fields that have both a setter and getter
        // get the getter and setter methods to invoke for these fields
        List<Method> setters = new ArrayList<Method>();
        List<Method> getters = new ArrayList<Method>();
        tu.getSettersAndGettersToExecute(Pick.class, setters, getters);

        try {
            // execute the setters on the original pick
            tu.invokeSetters(pick, setters);
        } catch (RuntimeException e) {
            Assert.fail("Your pick.java customization includes a member whose type"
                + " (shown at end of this message) is not supported by this test."
                + " Please verify that you have overridden the Pick.GetCopy() method"
                + " to include any additional members that you have added to Pick.java. "
                + e.getMessage());
        }
        
        // assign original pick to chase
        Pick copy = pick.getCopy();
        
        // execute the getters and compare results to ensure all new members have been
        // set within the Pick.GetCopy() method.
        tu.invokeGettersAndCompare(pick, copy, getters);

    }
}
