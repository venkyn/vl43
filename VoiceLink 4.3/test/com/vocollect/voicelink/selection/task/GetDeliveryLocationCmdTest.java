/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test for GetDeliveryLocationCmd. The logic to be tested is described in the
 * GetDeliveryLocationCmd javadoc.
 * 
 * @author sfahnestock, snayeem, bnorthrop
 */
@Test(groups = { FAST, TASK, SELECTION })
public class GetDeliveryLocationCmdTest extends TaskCommandTestCase {

    private GetDeliveryLocationCmd cmd;

    private static final String SAMPLE_OPERATOR_ID = "GetDeliveryLocationOper";

    private static final String SAMPLE_SERIAL_NUMBER = "GetDeliveryLocationSerial";

    private static final Long SAMPLE_ASSIGNMENT_PK = -46L;
    
    private static final Long SAMPLE_ASSIGNMENT_PK_2 = -191L;

    /**
     * @return a bean for testing.
     */
    private GetDeliveryLocationCmd getCmdBean() {
        return (GetDeliveryLocationCmd) getBean("cmdPrTaskLUTGetDeliveryLocation");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }

    /**
     * Test method for getting the group number.
     * {@link com.vocollect.voicelink.core.task.GetDeliveryLocationCmd#getGroupNumber()}.
     */
    @Test()
    public void testGetGroupNumber() {
        this.cmd.setGroupNumber(1L);
        assertEquals(this.cmd.getGroupNumber().toString(), "1", "GetGroupNumber");
    }

    /**
     * Test method for getting the assignment primary key.
     * {@link com.vocollect.voicelink.core.task.GetDeliveryLocationCmd#getAssignmentPk()}.
     */
    @Test()
    public void testGetAssignmentPk() {
        this.cmd.setAssignmentPk(1L);
        assertEquals(this.cmd.getAssignmentPk().toString(), "1", "GetAssignmentPk");
    }

    /**
     * Test method for getting the delivery location.
     * {@link com.vocollect.voicelink.core.task.GetDeliveryLocationCmd#execute}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        Long assignmentPK = (Long) groupResponse.getRecords().get(0).get("assignmentPK");
        
        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), 
                assignmentPK.toString());

        // Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "errorCode");
        assertEquals(record.get("deliveryLocation"), "2", "deliveryLocation");
        assertEquals(record.get("deliveryLocationCD"), "2", "deliveryLocationCD");
        assertEquals(record.get("loadingDockDoor"), "", "loadingDockDoor");
        assertEquals(record.get("loadingDockDoorCD"), "", "loadingDockDoorCD");
        assertEquals(record.get("directLoad"), "0", "directLoad");
        assertEquals(record.get("allowOverride"), "0", "allowOverride");
        assertEquals(record.get("container"), "", "container");
    }

    /**
     * Test with an assignment that has no delivery location defined, and must
     * use a customer number mapping.
     * 
     * {@link com.vocollect.voicelink.core.task.GetDeliveryLocationCmd#execute}.
     * @throws Exception any exception.
     */
    @Test()
    public void testExecuteCustomerNumberMapping() throws Exception {

        initialize();

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), 
                SAMPLE_ASSIGNMENT_PK.toString());

        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "errorCode");
        assertEquals(record.get("deliveryLocation"), "-60", "deliveryLocation");
        assertEquals(record.get("deliveryLocationCD"), "-60", "deliveryLocationCD");
    }
    
    /**
     * Test with an assignment that has no delivery location defined, and has no 
     * mapping defined either.
     * 
     * {@link com.vocollect.voicelink.core.task.GetDeliveryLocationCmd#execute}.
     * @throws Exception any exception.
     */
    @Test()
    public void testExecuteNoMapping() throws Exception {

        initialize();

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), 
                SAMPLE_ASSIGNMENT_PK_2.toString());

        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "errorCode");
        assertEquals(record.get("deliveryLocation"), null, "deliveryLocation");
        assertEquals(record.get("deliveryLocationCD"), null, "deliveryLocationCD");
    }    

    /**
     * Test with an assignment that has no delivery location defined, and must
     * use a route mapping.
     * 
     * {@link com.vocollect.voicelink.core.task.GetDeliveryLocationCmd#execute}.
     * @throws Exception any exception.
     */
    @Test()
    public void testExecuteRouteMapping() throws Exception {

        initialize();
        classSetupInsert("VoiceLinkDataUnitTest_DeliveryLocationMapping_RouteSettingTest.xml");

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), 
                                           SAMPLE_ASSIGNMENT_PK.toString());

        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "errorCode");
        assertEquals(record.get("deliveryLocation"), "-60", "deliveryLocation");
        assertEquals(record.get("deliveryLocationCD"), "-60", "deliveryLocationCD");
    }

    /**
     * Initializes the database by calling task commands that need to be called prior
     * to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;

        // Clear Database
        this.cmd.getAssignmentManager().initializeState();

        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();
        removeDeliveryLocationData();
        insertDeliveryLocationData();

        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", new String[] {
            makeStringFromDate(this.getCmdDateSec()), SAMPLE_SERIAL_NUMBER,
            SAMPLE_OPERATOR_ID, locale.toString(), "Default", this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", new String[] {
            makeStringFromDate(this.getCmdDateSec()), SAMPLE_SERIAL_NUMBER, SAMPLE_OPERATOR_ID });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
            makeStringFromDate(this.getCmdDateSec()), SAMPLE_SERIAL_NUMBER, SAMPLE_OPERATOR_ID, "1234" });
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), SAMPLE_SERIAL_NUMBER, SAMPLE_OPERATOR_ID, "2", "3" });
        executeLutCmd("cmdPrTaskLUTGetAssignment", new String[] {
            makeStringFromDate(this.getCmdDateSec()), SAMPLE_SERIAL_NUMBER, SAMPLE_OPERATOR_ID, "1", "1" });
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param groupNumber - group of assignments that were issued
     * @param assignmentPk - assignment pick belongs with
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String groupNumber,
                                    String assignmentPk) throws Exception {
        this.cmd = getCmdBean();
        // Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), SAMPLE_SERIAL_NUMBER,
                SAMPLE_OPERATOR_ID, groupNumber, assignmentPk }, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }

    /**
     * Clears all Delivery Location Mapping data.
     * 
     * @throws Exception - any exception.
     */
    private void insertDeliveryLocationData() throws Exception {
        classSetupInsert("VoiceLinkDataUnitTest_DeliveryLocationMapping.xml");
    }

    /**
     * Remove all the delivery location data after each test, to clean up.
     * 
     * @throws Exception - any exception.
     */
    @AfterMethod
    public void removeDeliveryLocationData() throws Exception {
        classSetupDeleteAll("VoiceLinkDataUnitTest_DeliveryLocationMappingReset.xml");
    }

}
