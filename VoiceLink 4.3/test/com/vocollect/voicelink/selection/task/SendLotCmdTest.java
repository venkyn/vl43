/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for SendLotCmd.
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, SELECTION })
public class SendLotCmdTest extends TaskCommandTestCase {

    private SendLotCmd cmd;
    private String operId = "SendLotOper";
    private String sn = "SendLotSerial";
    
    /**
     * @return a bean for testing.
     */
    private SendLotCmd getCmdBean() {
        return (SendLotCmd) getBean("cmdPrTaskLUTSendLot");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendLotCmd#getLotNumber()}.
    */
    @Test()
    public void testGetLotNumber() {
        this.cmd.setSpeakableLotNumber("1");
        assertEquals(this.cmd.getSpeakableLotNumber(), "1", "GetLotNumber");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendLotCmd#getLotQuantity()}.
    */
    @Test()
    public void testGetLotQuantity() {
        this.cmd.setLotQuantity(1);
        assertEquals(this.cmd.getLotQuantity(), 1, "GetLotQuantity");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendLotCmd#getAssignmentPk()}.
    */
    @Test()
    public void testGetAssignmentPk() {
        this.cmd.setAssignmentPk(1L);
        assertEquals(this.cmd.getAssignmentPk().toString(), "1", "GetAssignmentPk");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendLotCmd#getSequenceNumber()}.
    */
    @Test()
    public void testGetSequenceNumber() {
        this.cmd.setPickID(1L);
        assertEquals(this.cmd.getPickID().toString(), "1", "GetSequenceNumber");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.SendLotCmd#execute}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();
        //Test send lot command
        Response response = executeCommand(this.getCmdDateSec(), "111", "1", "-429", "-2830");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "errorCode"); 
    } 
    

    /**
     * Initialize the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        this.cmd.getAssignmentManager().initializeState();        
        classSetUp();

        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();
        
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param lotNumber - lot number entered by operator
     * @param lotQuantity - quantity for this lot
     * @param assignmentPk - assignment pick belongs with
     * @param sequenceNumber - pick id for a pick record
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String lotNumber, 
                                    String lotQuantity,
                                    String assignmentPk,
                                    String sequenceNumber) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(cmdDate), sn,
                operId, lotNumber, lotQuantity, assignmentPk, sequenceNumber}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
