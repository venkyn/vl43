/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.printserver.AbstractPrintServer;
import com.vocollect.voicelink.printserver.PrintServerFactory;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for PrintCmd.
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, SELECTION })
public class PrintCmdTest extends TaskCommandTestCase {

    private PrintCmd cmd;
    private String operId = "PrintOper";
    private String sn = "PrintSerial";
    
    /**
     * @return a bean for testing.
     */
    private PrintCmd getCmdBean() {
        return (PrintCmd) getBean("cmdPrTaskLUTPrint");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PrintCmd#getGroupNumber()}.
    */
    @Test()
    public void testGetGroupNumber() {
        this.cmd.setGroupNumber(1L);
        assertEquals(this.cmd.getGroupNumber().toString(), "1", "GetGroupNumber");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PrintCmd#getAssignmentPk()}.
    */
    @Test()
    public void testGetAssignmentPk() {
        this.cmd.setAssignmentPk(1L);
        assertEquals(this.cmd.getAssignmentPk().toString(), "1", "GetAssignmentPk");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PrintCmd#getPickContainerPk()}.
    */
    @Test()
    public void testGetPickContainerPk() {
        this.cmd.setPickContainerPk("1");
        assertEquals(this.cmd.getPickContainerPk().toString(), "1", "getPickContainerPk");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PrintCmd#getPrinter()}.
    */
    @Test()
    public void testGetPrinter() {
        this.cmd.setPrinterNumber(1L);
        assertEquals(this.cmd.getPrinterNumber().toString(), "1", "getPrinter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PrintCmd#getOperation()}.
    */
    @Test()
    public void testGetOperation() {
        this.cmd.setOperation(1);
        assertEquals(this.cmd.getOperation(), 1, "getOperation");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PrintCmd#getReprintLabels()}.
    */
    @Test()
    public void testGetReprintLabels() {
        this.cmd.setReprintLabels(1);
        assertEquals(this.cmd.getReprintLabels(), 1, "getReprintLabels");
        
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PrintCmd#execute}.
     * @throws Exception any
     */
    @Test(enabled = false)
    public void testExecutePrint() throws Exception {
        
        initialize();
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        //Test Request Work Stub
        Response response = null;
        Long assignmentPK = null;
        Integer counter = 1;
        
        for (ResponseRecord trr : groupResponse.getRecords()) {
            assignmentPK = (Long) trr.get("assignmentPK");
            response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), 
                assignmentPK.toString(), "1", "", "1", "0");
            //Verify Successful result
            List<ResponseRecord> records = response.getRecords();
            assertEquals(records.size(), 1, "PrintCmd recordSize # " + counter);
            ResponseRecord record = records.get(0);
            assertEquals(record.getErrorCode(), "0", "PrintCmd errorCode # " + counter);
            ++counter;
        }
       // For testing purposes, wait for print job to end.
        AbstractPrintServer server = PrintServerFactory.getPrintServer();
        while (server.getJobQueue().size() > 0) {
            // Wait for job to process
            // this is only needed for testing purposes.
        }
    }
    
    
    /**
     * Test that an invalid printer number is caught and returned to the 
     * terminal operator.
     * @throws Exception
     */
    @Test()
    public void testInvalidPrinterNumber() throws Exception {
        
        initialize();
        // Printer number 4 should be invalid
        Response response = executeCommand(this.getCmdDateSec(), "1", "12345", "1", "", "4", "0");
        
        //Verify invalid printer result
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), 
                Long.toString(TaskErrorCode.PRINTER_NOT_FOUND.getErrorCode()),
                "InvalidPrinterNumber errorCode # ");
    }

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        // Setup Core data
        classSetupCore();
        // Setup Selection Regions
        classSetupSelectionRegions();
        // Setup Selection Data
        classSetupSelectionData();

        //Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "3"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "2", "3"});
        executeLutCmd("cmdPrTaskLUTGetAssignment",
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1"});
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param groupNumber - group of assignments that were issued
     * @param assignmentPk - assignment pick belongs with
     * @param pickContainerPk - internal id of container previously sent to task
     * @param operation - operation to perform
     * @param printer - printer number to print to
     * @param reprintLabels - 0=no reprint, 1=yes reprint
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String groupNumber, 
                                    String assignmentPk,
                                    String operation,
                                    String pickContainerPk,
                                    String printer,
                                    String reprintLabels) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(cmdDate), sn, operId, 
                  groupNumber, assignmentPk, operation, pickContainerPk, printer, reprintLabels}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
