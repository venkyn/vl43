/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for SendLotCmd.
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, SELECTION })
public class VerifyReplenishmentCmdTest extends TaskCommandTestCase {

    private VerifyReplenishmentCmd cmd;
    private String operId = "VerifyReplenishmentOper";
    private String sn = "VerifyReplenishmentSerial";
    
    /**
     * @return a bean for testing.
     */
    private VerifyReplenishmentCmd getCmdBean() {
        return (VerifyReplenishmentCmd) 
            getBean("cmdPrTaskLUTVerifyReplenishment");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.VerifyReplenishmentCmd#getLocationPk()}.
    */
    @Test()
    public void testGetLocationPk() {
        this.cmd.setLocationPk(1L);
        assertEquals(this.cmd.getLocationPk().toString(), "1", "GetLocationPk");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.VerifyReplenishmentCmd#getItemNumber()}.
    */
    @Test()
    public void testGetItemNumber() {
        this.cmd.setItemNumber("1");
        assertEquals(this.cmd.getItemNumber().toString(), "1", "GetItemNumber");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.VerifyReplenishmentCmd#execute}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();
        //Test verify replenishment with replenished
        Response response = executeCommand(this.getCmdDateSec(), "-1779", "916478");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "errorCode"); 
        assertEquals(record.get("replenished"), "1", "replenished"); 
        
        //Set location status to empty
        Location l = this.cmd.getLocationManager().get(-1779L);
        l.setStatus(LocationStatus.Empty);
        this.cmd.getLocationManager().save(l);

        //Test verify replenishment with replenished
        response = executeCommand(this.getCmdDateSec(), "-1779", "916478");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "errorCode"); 
        assertEquals(record.get("replenished"), "0", "replenished empty"); 
    }
    
    

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "3" });
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "15", "3" });
        executeLutCmd("cmdPrTaskLUTGetAssignment", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1" });
        
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param locationPk - location operator is going to be directed to
     * @param itemNumber - new for 3.0 in anticipation for mult items at location
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String locationPk, 
                                    String itemNumber) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(cmdDate), sn, 
                operId, locationPk, itemNumber}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
