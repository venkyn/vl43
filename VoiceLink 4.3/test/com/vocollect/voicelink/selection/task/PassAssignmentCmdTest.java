/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorFunctionLabor;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentLabor;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for PassAssignmentCmd.
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, SELECTION })
public class PassAssignmentCmdTest extends TaskCommandTestCase {

    private PassAssignmentCmd cmd;
    private String operId = "PassAssignmentOper";
    private String sn = "PassAssignmentSerial";
    
    private List <OperatorLabor> olList;
    private OperatorFunctionLabor flRec;
    private List <AssignmentLabor> alList;
    private AssignmentLabor alRec;
    private boolean verifyIsClosed;
    
    /**
     * @return a bean for testing.
     */
    private PassAssignmentCmd getCmdBean() {
        return (PassAssignmentCmd) getBean("cmdPrTaskLUTPassAssignment");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetDeliveryLocationCmd#getGroupNumber()}.
    */
    @Test()
    public void testGetGroupNumber() {
        this.cmd.setGroupNumber(1L);
        assertEquals(this.cmd.getGroupNumber().toString(), "1", "GetGroupNumber");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetDeliveryLocationCmd#execute}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        //===============================================================
        //Test passing an assignment
        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());

        //Get Assignment 
        Assignment a = this.cmd.getAssignmentManager().get(-643L);
        assertNotNull(a, "GotAssignment");
        
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "errorCode"); 
        assertEquals(a.getStatus().toString(), "Passed", "assignmentStatus");
        
        // verify labor records are correct
        verifyOperatorLaborRecords();
        Integer zero = new Integer(0);
        Double dblValue = new Double(0.0);
        assertEquals(flRec.getCount(), zero, "operator labor count ");
        assertEquals(flRec.getActualRate(), dblValue, "operator labor actual rate ");
        assertEquals(flRec.getPercentOfGoal(), dblValue, "operator labor percent of goal ");
        
        verifyIsClosed = true;
        verifyAssignmentLaborRecord(verifyIsClosed);

        assertEquals(alRec.getQuantityPicked(), zero, "asgn labor quantity picked ");
        assertEquals(alRec.getAssignmentProrateCount(), zero, "asgn labor asgn prorate count ");
        assertEquals(alRec.getGroupProrateCount(), zero, "asgn labor group prorate count ");
        assertEquals(alRec.getActualRate(), dblValue, "asgn labor actual rate ");
        assertEquals(alRec.getPercentOfGoal(), dblValue, "asgn labor percent of goal ");
        
        initialize();
        groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        //===============================================================
        // Try to pass a chase assignment. This should result in an error.
        // You can not pass a chase assignment.
        a = this.cmd.getAssignmentManager().get(-643L);
        assertNotNull(a, "GotAssignment");
        a.setType(AssignmentType.Chase);
        this.cmd.getAssignmentManager().save(a);
        
        response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), 
                Long.toString(TaskErrorCode.CANNOT_PASS_CHASE.getErrorCode()), "errorCode"); 
        assertEquals(a.getStatus().toString(), "InProgress", "assignmentStatus");
        
        // verify labor records are correct
        verifyOperatorLaborRecords();
        // assignment labor record should remain open.
        verifyIsClosed = false;
        verifyAssignmentLaborRecord(verifyIsClosed); 

        initialize();
        groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        //===============================================================
        //Test Assigned to another region
        Operator o = this.cmd.getOperatorManager().findByIdentifier(operId);
        a = this.cmd.getAssignmentManager().get(-46L);
        o.setAssignedRegion(a.getRegion());
        this.cmd.getOperatorManager().save(o);
        
        response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), "2", "errorCode"); 
        verifyOperatorLaborRecords();
        // assignment labor should be closed
        verifyIsClosed = true;
        verifyAssignmentLaborRecord(verifyIsClosed); 
        
    }
 

    /** verify operator labor records are correct.
     * 
     * @throws DataAccessException on failure
     */
    private void verifyOperatorLaborRecords() throws DataAccessException {
        
        this.olList =  this.cmd.getLaborManager().getOperatorLaborManager()
                           .listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        assertEquals(olList.size(), 2, "Bad operator labor record count");
        flRec = (OperatorFunctionLabor) olList.get(1);
        assertNull(flRec.getEndTime(), "selection labor record is not open ");
    }
    
    
    /** verify assignment labor records are correct.
     * 
     * @param isClosed - set to true to verify that assignment labor record is closed
     *                   otherwise set to true to verify that labor record is open.
     * @throws DataAccessException on failure
     */
    private void verifyAssignmentLaborRecord(boolean isClosed) throws DataAccessException {
        
         alList = this.cmd.getLaborManager().getAssignmentLaborManager()
                      .listAllRecordsByOperatorLaborId(flRec.getId());
         assertEquals(alList.size(), 1, "Bad Assignment labor record count, ");
         alRec = alList.get(0);
         // if record should be closed, then end time should not be null
         if (isClosed) {
             assertNotNull(alRec.getEndTime(), "assignment labor not closed ");
         } else {
             assertNull(alRec.getEndTime(), "assignment labor not open ");
         }    
    }
    

    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        this.cmd.getAssignmentManager().initializeState();

        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();
        
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "3"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "14", "3"});
        executeLutCmd("cmdPrTaskLUTRequestWork", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "6391", "1", "1"});
        executeLutCmd("cmdPrTaskLUTGetAssignment", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1"});
    }

    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param groupNumber - group of assignments that were issued
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String groupNumber) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(cmdDate),
                sn, operId, groupNumber},  this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
