/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentLabor;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.selection.model.SelectionRegionSummaryPrompt;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for UpdateStatusCmd.
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, SELECTION })
public class GetAssignmentCmdTest extends TaskCommandTestCase {

    private String operId = "GetAssignmentOper";
    private String sn = "GetAssignmentSerial";
    
    private GetAssignmentCmd cmd;
    
    /**
     * @return a bean for testing.
     */
    private GetAssignmentCmd getCmdBean() {
        return (GetAssignmentCmd) getBean("cmdPrTaskLUTGetAssignment");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetAssignmentCmd#getNumberOfAssignments()}.
     */
    public void testGetSetNumberOfAssignments() {
        this.cmd.setNumberOfAssignments("1");
        assertEquals(this.cmd.getNumberOfAssignments(), "1", "NumberOfAssignments Getter/Setter");
        assertEquals(this.cmd.getNumberOfAssignmentsInt(), 1, "NumberOfAssignments Getter/Setter Int");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetAssignmentCmd#getAssignmentType()}.
     */
    public void testGetSetAssignmentType() {
        this.cmd.setAssignmentType(AssignmentType.Normal.toValue());
        assertEquals(AssignmentType.Normal.toValue(), this.cmd.getAssignmentType(), "AssignmentType Getter/Setter");
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteBaseItemsPrompt() throws Exception {
        BigDecimal goalTime = new BigDecimal(0);
        goalTime.setScale(2, BigDecimal.ROUND_HALF_UP);
        
        final Locale locale = Locale.US;
        
        this.cmd.getAssignmentManager().initializeState();

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/voicelink/VoiceLinkDataUnitTest_BaseItemsAssignmentPrompt.xml",
                DatabaseOperation.REFRESH);

        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "3"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "15", "3"});
        
        //===============================================================
        //Test Get Assignment LUT
        Response response = executeCommand(this.getCmdDateSec(), "1", "1");
        
        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        Map<String, Object> record = response.getRecords().get(0);
        
        Long assignmentId = (Long) record.get("assignmentPK");
        //Get Assignment 
        Assignment a = this.cmd.getAssignmentManager().get(assignmentId);
        assertNotNull(a, "GotAssignment");
                
        //Verify Successful result
        String overridePrompt = (String) record.get("overridePrompt");
        assertNotNull(overridePrompt, "Null summary prompt");
        assertTrue(overridePrompt.contains("base item"), "No base item summary");

    }
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {
        
        // Verify that we can get one assignment correctly.
        BigDecimal goalTime = new BigDecimal(0);
        goalTime.setScale(2, BigDecimal.ROUND_HALF_UP);

        // Initialize Data
        initialize();

        //Test Get Assignment LUT
        Response response = executeCommand(this.getCmdDateSec(), "1", "1");
        //Get record back from first run
        assertEquals(1, response.getRecords().size(), "recordSize");
        Map<String, Object> record = response.getRecords().get(0);
        
        Long assignmentId = (Long) record.get("assignmentPK");
        //Get Assignment 
        Assignment a = this.cmd.getAssignmentManager().get(assignmentId);
        assertNotNull(a, "GotAssignment");
                
        //Verify Successful result
        assertEquals(record.get("groupID"), a.getGroupInfo().getGroupNumber(), "groupID");
        assertEquals(record.get("isChase"), false, "isChase");
        assertEquals(record.get("assignmentPK"), a.getId(), "assignmentPK");
        assertEquals(record.get("idDescription").toString(), "115107317", "idDescription");
        assertEquals(record.get("position"), a.getGroupInfo().getGroupPosition(), "position");
        assertEquals(record.get("goalTime"), new BigDecimal(a.getSummaryInfo().getGoalTime())
                    .setScale(2, BigDecimal.ROUND_HALF_UP).toPlainString(), "goalTime");
        assertEquals(record.get("route"), a.getRoute(), "route");
        assertEquals(record.get("activeContainer"), "00", "activeContainer");
        assertEquals(record.get("passAssignment"), 0, "passAssignment");
        assertEquals(record.get("summaryPromptType"),
                     SelectionRegionSummaryPrompt.TaskDefaultPrompt.toValue(), "summaryPromptType"); 
        assertEquals(record.get("overridePrompt"), "", "overridePrompt");
        
        //Verify that an assignment labor record was created correctly
        List <AssignmentLabor> alRecords = 
             this.cmd.getLaborManager().getAssignmentLaborManager()
                     .listOpenRecordsByOperatorId(this.cmd.getOperator().getId());
        assertEquals(alRecords.size(), 1, "Did not create assignment labor record ");
        AssignmentLabor laborRecord = alRecords.get(0);
        assertNotNull(laborRecord.getAssignment().getId(), "Assignment labor - assignment id is null "); 
        assertNotNull(laborRecord.getOperator().getId(), "Assignment labor - operator id is null "); 
        assertNotNull(laborRecord.getStartTime(), "Assignment labor - start time should not be null ");
        assertNull(laborRecord.getEndTime(), "Assignment labor - end time is not null");
        Integer zero = new Integer(0);
        assertEquals(laborRecord.getQuantityPicked(), zero, 
                     "Assignment labor - quantity picked not equal zero");
        assertEquals(laborRecord.getQuantityPicked(), zero, 
                     "Assignment labor - assignment prorate count not equal zero");
        assertEquals(laborRecord.getQuantityPicked(), zero, 
                     "Assignment labor - group prorate count not equal zero ");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteInProgress() throws Exception {
        
        // Try to get an in progress assignment
        initialize();
        
        //===============================================================
        //Test Get Assignment LUT
        Date cmdDateA = this.getCmdDateSec();
        Response responseA = executeCommand(cmdDateA, "1", "1");
        
        //Get record back from first run
        List<ResponseRecord> recordsA = responseA.getRecords();
        assertEquals(recordsA.size(), 1, "recordSizeA");
        Map<String, Object> recordA = recordsA.get(0);
        
        Date cmdDateB = this.getCmdDateSec();
        Response responseB = executeCommand(cmdDateB, "1", "1");
        
        //Get record back from second run
        List<ResponseRecord> recordsB = responseB.getRecords();
        assertEquals(recordsB.size(), 1, "recordSizeB");
        Map<String, Object> recordB = recordsB.get(0);
        
        //Verify Successful result
        assertEquals(recordA.get("groupID"), recordB.get("groupID"), "groupID");
        assertEquals(recordA.get("isChase"), recordB.get("isChase"), "isChase");
        assertEquals(recordA.get("assignmentPK"), recordB.get("assignmentPK"), "assignmentPK");
        assertEquals(recordA.get("idDescription"), recordB.get("idDescription"), "idDescription");
        assertEquals(recordA.get("position"), recordB.get("position"), "position");
        assertEquals(recordA.get("goalTime"), recordB.get("goalTime"), "goalTime");
        assertEquals(recordA.get("route"), recordB.get("route"), "route");
        assertEquals(recordA.get("activeContainer"), recordB.get("activeContainer"), "activeContainer");
        assertEquals(recordA.get("passAssignment"), recordB.get("passAssignment"), "passAssignment");
        assertEquals(recordA.get("summaryPromptType"), recordB.get("summaryPromptType"), "summaryPromptType");
        assertEquals(recordA.get("overridePrompt"), recordB.get("overridePrompt"), "overridePrompt");
        
        //Verify that we get only one open assignment labor record.
        List<AssignmentLabor> laborRecords = 
        this.cmd.getLaborManager().getAssignmentLaborManager()
                .listOpenRecordsByOperatorId(this.cmd.getOperator().getId());
        if (laborRecords.size() == 0) {
            fail("Did not create assignment labor record");
        }
        assertEquals(laborRecords.size(), 1, "Unexpected number of assignments labor records ");
        AssignmentLabor aLabor = laborRecords.get(0);
        assertNull(aLabor.getEndTime(), "Assignment labor - end time is not null");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteNoAssignment() throws Exception {

        initialize();
        //Test Get Assignment LUT
        //Request a chase assignment.
        //Since there are none we should get an error
        Response response = executeCommand(this.getCmdDateSec(), "1", "2");
  
        //Get record back from first run
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSizeA");
        Map<String, Object> record = records.get(0);
        
        //Verify Successful result
        assertEquals(record.get("errorCode"), "2", "ErrorCode");
        
        //Verify that no assignment labor record was created
        List<AssignmentLabor> laborRecords = 
            this.cmd.getLaborManager().getAssignmentLaborManager()
                    .listOpenRecordsByOperatorId(this.cmd.getOperator().getId());
        assertEquals(laborRecords.size(), 0, "Assignment Labor record should not exist ");
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteChaseAssignment() throws Exception {

         // Verify that we can get one chase assignment correctly.
        initialize();
        
        //Get Assignment 
        Assignment a = this.cmd.getAssignmentManager().get(-693L);
        assertNotNull(a, "GotAssignment");
        a.setType(AssignmentType.Chase);
        this.cmd.getAssignmentManager().save(a);

        //===============================================================
        //Test Get Assignment LUT
        Response response = executeCommand(this.getCmdDateSec(), "1", "2");
  
        //Get record back from first run
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSizeA");
        Map<String, Object> record = records.get(0);
        
        //Verify Successful result
        assertEquals(record.get("errorCode"), "0", "ErrorCode");
        assertEquals(record.get("assignmentPK"), a.getId(), "assignmentPK");
        assertEquals(record.get("isChase"), true, "isChase");
        
        //Verify that an assignment labor record was created correctly
        AssignmentLabor laborRecord = this.cmd.getLaborManager().getAssignmentLaborManager()
                                      .findOpenRecordByAssignmentId(a.getId());
        
        assertNotNull(laborRecord, "Did not create assignment labor record ");
        if (laborRecord == null) {
            fail("Did not create assignment labor record");
        } else {
            assertNull(laborRecord.getEndTime(), "Assignment labor - end time is not null");
        }
    }
  
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteMultipleAutomatic() throws Exception {

        // Verify that we request two assignments and get two assignments.
        initialize();

        //Test Get Assignment LUT
        Date cmdDate = this.getCmdDateSec();
        Response response = executeCommand(cmdDate, "2", "1");
        
        //Get record back from first run
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 2, "recordSize");
        Map<String, Object> record = records.get(0);
        assertEquals(record.get("assignmentPK").toString(), "-689", "assignmentPK");
        record = records.get(1);
        assertEquals(record.get("assignmentPK").toString(), "-690", "assignmentPK");
        
        //Verify that we get two open assignment labor records
        List<AssignmentLabor> laborRecords = 
        this.cmd.getLaborManager().getAssignmentLaborManager()
            .listOpenRecordsByOperatorId(this.cmd.getOperator().getId());
        
        if (laborRecords.size() == 0) {
            fail("Did not create assignment labor record");
        }
        assertEquals(laborRecords.size(), 2, "Did not get two assignment labor records");
        AssignmentLabor aLabor = laborRecords.get(0);
        assertNull(aLabor.getEndTime(), "Assignment labor - end time is not null");
        aLabor = laborRecords.get(1);
        assertNull(aLabor.getEndTime(), "Assignment labor - end time is not null");
    }
   
   
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteManual() throws Exception {

        initializeManual();
        //Request an assignment
        executeLutCmd("cmdPrTaskLUTRequestWork", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "7285", "1", "1"});
        
        //===============================================================
        //Test Get Assignment LUT
        Date cmdDate = this.getCmdDateSec();
        Response response = executeCommand(cmdDate, "1", "1");
        
        //Get record back from first run
        List<ResponseRecord> records = response.getRecords();
        assertEquals(1, records.size(), "recordSize");
        Map<String, Object> record = records.get(0);
        
        //Verify Successful result
        assertEquals(record.get("isChase"), false, "isChase");
        assertEquals(record.get("assignmentPK").toString(), "-589", "assignmentPK");
        assertEquals(record.get("idDescription").toString(), "113107285", "idDescription");
        assertEquals(record.get("position").toString(), "1", "position");
        assertEquals(record.get("goalTime"), "5000000000.00", "goalTime");
        assertEquals(record.get("route").toString(), "20", "route");
        assertEquals(record.get("activeContainer").toString(), "00", "activeContainer");
        assertEquals(record.get("passAssignment").toString(), "0", "passAssignment");
        assertEquals(record.get("summaryPromptType").toString(), "0", "summaryPromptType");
        assertEquals(record.get("overridePrompt").toString(), "", "overridePrompt");
       
        //Verify that an assignment labor record was created correctly
        List<AssignmentLabor> laborRecords = 
            this.cmd.getLaborManager().getAssignmentLaborManager()
                .listOpenRecordsByOperatorId(this.cmd.getOperator().getId());
        if (laborRecords.size() == 0) {
            fail("Did not create assignment labor record");
        }
        AssignmentLabor aLabor = laborRecords.get(0);
        assertNull(aLabor.getEndTime(), "Assignment labor - end time is not null");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteManualMultiple() throws Exception {

        initializeManual();

        //Request two assignments
        executeLutCmd("cmdPrTaskLUTRequestWork", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "7285", "1", "1"});
        executeLutCmd("cmdPrTaskLUTRequestWork", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "7289", "1", "1"});
        
        //Test Get Assignment LUT
        Response response = executeCommand(this.getCmdDateSec(), "2", "1");
        
        //Get record back from first run
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 2, "recordSize");
        
        //Verify Successful result of first record
        Map<String, Object> record = records.get(0);
        assertEquals(record.get("isChase"), false, "isChaseA");
        assertEquals(record.get("assignmentPK").toString(), "-589", "assignmentPKA");
        assertEquals(record.get("idDescription").toString(), "113107285", "idDescriptionA");
        assertEquals(record.get("position").toString(), "1", "positionA");
        assertEquals(record.get("goalTime"), "5000000000.00", "goalTimeA");
        assertEquals(record.get("route").toString(), "20", "routeA");
        assertEquals(record.get("activeContainer").toString(), "00", "activeContainerA");
        assertEquals(record.get("passAssignment").toString(), "0", "passAssignmentA");
        assertEquals(record.get("summaryPromptType").toString(), "0", "summaryPromptTypeA");
        assertEquals(record.get("overridePrompt").toString(), "", "overridePromptA");
        
        //Verify Successful result of second record
        record = records.get(1);
        assertEquals(record.get("isChase"), false, "isChaseB");
        assertEquals(record.get("assignmentPK").toString(), "-590", "assignmentPKB");
        assertEquals(record.get("idDescription").toString(), "113107289", "idDescriptionB");
        assertEquals(record.get("position").toString(), "2", "positionB");
        assertEquals(record.get("goalTime"), new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP)
                     .toPlainString(), "goalTimeB");
        assertEquals(record.get("route").toString(), "20", "routeB");
        assertEquals(record.get("activeContainer").toString(), "00", "activeContainerB");
        assertEquals(record.get("passAssignment").toString(), "0", "passAssignmentB");
        assertEquals(record.get("summaryPromptType").toString(), "0", "summaryPromptTypeB");
        assertEquals(record.get("overridePrompt").toString(), "", "overridePromptB");
        
        //Verify that we get only one open assignment labor record
        List<AssignmentLabor> laborRecords = 
        this.cmd.getLaborManager().getAssignmentLaborManager()
            .listOpenRecordsByOperatorId(this.cmd.getOperator().getId());
        if (laborRecords.size() == 0) {
            fail("Did not create assignment labor record");
        }
        assertEquals(laborRecords.size(), 2, "Unexpected number of assignments labor records ");
        AssignmentLabor aLabor = laborRecords.get(0);
        assertNull(aLabor.getEndTime(), "Assignment labor - record 1 end time is not null ");
        aLabor = laborRecords.get(1);
        assertNull(aLabor.getEndTime(), "Assignment labor - record 2 end time is not null ");
    }
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteManualChaseAssignment() throws Exception {

        initializeManual();
        
        //Get Assignment 
        Assignment a = this.cmd.getAssignmentManager().get(-591L);
        assertNotNull(a, "GotAssignment");
        a.setType(AssignmentType.Chase);
        this.cmd.getAssignmentManager().save(a);

        //Request an assignment
        executeLutCmd("cmdPrTaskLUTRequestWork", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "7295", "1", "2"});

        //Test Get Assignment LUT
        Response response = executeCommand(this.getCmdDateSec(), "1", "2");
  
        //Get record back from first run
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSizeA");
        Map<String, Object> record = records.get(0);
        
        //Verify Successful result
        assertEquals(record.get("errorCode"), "0", "ErrorCode");
        assertEquals(record.get("assignmentPK"), a.getId(), "assignmentPK");
        assertEquals(record.get("isChase"), true, "isChase");
        
        //Verify that an assignment labor record was created correctly
        AssignmentLabor laborRecord = 
            this.cmd.getLaborManager().getAssignmentLaborManager()
                .findOpenRecordByAssignmentId(a.getId());
        assertNotNull(laborRecord, "Did not create Assignment Labor record ");
        assertNull(laborRecord.getEndTime(), "Assignment labor - end time is not null");
    }

    
    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        this.cmd.getAssignmentManager().initializeState();

        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();
        
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "3"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "15", "3"});
    }
    
    /**
     * Initialize the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initializeManual() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        this.cmd.getAssignmentManager().initializeState();
        
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();
        
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "3"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "13", "3"});
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param numberOfAssignments - Number of assignments to get
     * @param assignmentType - type of assignment to get
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String numberOfAssignments,
                                    String assignmentType) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(cmdDate), 
                              sn, operId, numberOfAssignments, assignmentType}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
