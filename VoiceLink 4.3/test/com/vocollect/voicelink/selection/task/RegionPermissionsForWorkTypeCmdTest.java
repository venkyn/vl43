/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for RegionPermissionsForWorkTypeCmd.
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, TASK, SELECTION })
public class RegionPermissionsForWorkTypeCmdTest extends TaskCommandTestCase {

    private RegionPermissionsForWorkTypeCmd cmd;
    private String operId = "RegionPermissionsOper";
    private String sn = "RegionPermissionsSerial";
    
    /**
     * @return a bean for testing.
     */
    private RegionPermissionsForWorkTypeCmd getCmdBean() {
        return (RegionPermissionsForWorkTypeCmd) 
            getBean("cmdPrTaskLUTRegionPermissionsForWorkType");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.RegionPermissionsForWorkTypeCmd#getSelectedWorkType()}.
    */
    @Test()
    public void testGetSelectedWorkType() {
        this.cmd.setSelectedWorkType("1");
        assertEquals(this.cmd.getSelectedWorkType(), "1", "GetSelectedWorkType");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.RegionPermissionsForWorkTypeCmd#execute}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();
        // Test Region Permission with no in progress
        Response response = executeCommand(this.getCmdDateSec(), "3");

        // Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 15, "recordSize");
        //Region 15
        boolean found = false;
        for (ResponseRecord record : records) {
            if ((Integer) record.get("regionNumber") == 15) {
                found = true;
                assertEquals(record.getErrorCode(), "0", "errorCode"); 
                assertEquals(record.get("regionNumber"), 15, "regionNumber");
                assertEquals(record.get("regionName"), "Selection Region 15", "regionName");
            }
        }
        assertEquals(true, found, "Region 15 not found");
        
        //===============================================================
        // Test region permission with in progress work
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "15", "3" });
        executeLutCmd("cmdPrTaskLUTGetAssignment", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1" });
        
        response = executeCommand(this.getCmdDateSec(), "3");
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize inProgress");
        
        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode").toString(), "2", "errorCode inProgress");
        // Test region permission with in progress work in other function
        response = executeCommand(this.getCmdDateSec(), "4");
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize inProgress");
        record = records.get(0);
        assertEquals(record.get("errorCode"), "3", "errorCode inProgressOtherType");

        initialize();
        Operator o = this.cmd.getOperatorManager().findByIdentifier(operId);
        Assignment a = this.cmd.getAssignmentManager().get(-46L);
        o.setAssignedRegion(a.getRegion());
        this.cmd.getOperatorManager().save(o);
 
        // Test Assigned to another region on sign on
        response = executeCommand(this.getCmdDateSec(), "3");

        // Verify Successful result
        records = response.getRecords();
        record = records.get(0);

        assertEquals(records.size(), 1, "recordSize");
        assertEquals(record.get("errorCode"), "99", "errorCode Assigned to another region");
    
        initialize();

        // Sign into a region
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "15", "3" });
        
        o = this.cmd.getOperatorManager().findByIdentifier(operId);
        a = this.cmd.getAssignmentManager().get(-46L);
        o.setAssignedRegion(a.getRegion());
        this.cmd.getOperatorManager().save(o);
        
        //===============================================================
        // Test Assigned to another region when already signed into a region
        response = executeCommand(this.getCmdDateSec(), "3");

        // Verify Successful result
        records = response.getRecords();
        record = records.get(0);

        assertEquals(records.size(), 1, "recordSize");
        assertEquals(record.get("errorCode"), "0", "errorCode Assigned to another region");
    
    }
    
    

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param workType - work type
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String workType) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(cmdDate), 
                sn, operId, workType}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
