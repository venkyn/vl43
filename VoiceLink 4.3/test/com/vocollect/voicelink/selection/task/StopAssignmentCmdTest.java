/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorFunctionLabor;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentLabor;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for UpdateStatusCmd.
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, SELECTION })
public class StopAssignmentCmdTest extends TaskCommandTestCase {
    
    private String operId = "StopAssignmentOper";
    private String sn = "StopAssignmentSerial";
    private Long testAssignment = -96L;
    private StopAssignmentCmd cmd;
    
    private List <OperatorLabor> olList;
    private OperatorFunctionLabor flRec;
    private List <AssignmentLabor> alList;
    private AssignmentLabor alRec;
    private boolean verifyIsClosed;
    
    /**
     * @return a bean for testing.
     */
    private StopAssignmentCmd getCmdBean() {
        return (StopAssignmentCmd) getBean("cmdPrTaskLUTStopAssignment");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.GetAssignmentCmd#getNumberOfAssignments()}.
     */
    @Test()
    public void testGetSetGroupNumberNumber() {
        Long groupNumber = testAssignment;
        this.cmd.setGroupNumber(groupNumber);
        assertEquals(this.cmd.getGroupNumber(), groupNumber, "GroupNumber Getter/Setter");
    }

    /**
     * Tests for stop assignment a single normal assignment.

     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        // Get an assignment in region 2 and pick all picks then stop the assignment.
        initialize();
        pickAllPicks(0);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        //Test Stop Assignment LUT
        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        Map<String, Object> record = response.getRecords().get(0);
        
        //Verify Successful result
        assertEquals(record.get("errorCode"), "0", "ErrorCode");

        //Verify Assignment set to complete
        Assignment a = this.cmd.getAssignmentManager().get(testAssignment);
        assertEquals(a.getStatus(), AssignmentStatus.Complete, "AssignmentComplete");
        // verify that labor records are correct
        verifyOperatorLaborRecords();
        // verify that we updated the labor stats on the operator labor record
        Double dblZero = new Double(0.0);
        Integer quantityPicked = new Integer(14);
        assertEquals(flRec.getCount(), quantityPicked, "operator labor count ");
        if (flRec.getActualRate() == dblZero || flRec.getPercentOfGoal() == dblZero) {
            fail("Aggregate value(s) should not be zero");
        }
            
        // verify that record was closed
        verifyIsClosed = true;
        verifyAssignmentLaborRecord(verifyIsClosed);
        // verify final totals on assignment labor record
        assertEquals(alRec.getQuantityPicked(), quantityPicked, "asgn labor quantity picked ");
        assertEquals(alRec.getAssignmentProrateCount(), quantityPicked, "asgn labor asgn prorate count ");
        assertEquals(alRec.getGroupProrateCount(), quantityPicked, "asgn labor group prorate count ");
        
        // Get an assignment in region 2 and pick all picks.
        initialize();
        pickAllPicks(0);
        groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        // change the operators assigned region to region 1.
        Operator o = this.cmd.getOperatorManager().findByIdentifier(operId);
        a = this.cmd.getAssignmentManager().get(-46L);
        o.setAssignedRegion(a.getRegion());
        this.cmd.getOperatorManager().save(o);
        
        // Test Assigned to another region
        // Try to stop the assignment in region 2 while the operator is assigned
        // to region 1
        response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        
        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        record = response.getRecords().get(0);
        
        //Verify that we got error code 2
        assertEquals(record.get("errorCode"), "2", "ErrorCode");
        // verify that labor records are correct
        verifyOperatorLaborRecords();
        verifyIsClosed = true;
        verifyAssignmentLaborRecord(verifyIsClosed);
      }
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteShorts() throws Exception {
    
        initialize();
        pickAllPicks(1); //Short 1 item
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        //Test Stop Assignment LUT
        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString());
        
        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        Map<String, Object> record = response.getRecords().get(0);
        
        //Verify Successful result
        assertEquals(record.get("errorCode"), "0", "ErrorCode");

        //Verify Assignment set to short
        Assignment a = this.cmd.getAssignmentManager().get(testAssignment);
        assertEquals(a.getStatus(), AssignmentStatus.Short, "AssignmentComplete");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteChaseShorts() throws Exception {

        initialize();
        pickAllPicks(1); //Short 1 item
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        //Set assignment to be chase assignment
        Assignment a = this.cmd.getAssignmentManager().get(testAssignment);
        a.setType(AssignmentType.Chase);
        this.cmd.getAssignmentManager().save(a);
        
        //===============================================================
        //Test Get Assignment LUT
        Date cmdDate = this.getCmdDateSec();
        Response response = executeCommand(cmdDate, groupNumber.toString());
        
        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        Map<String, Object> record = response.getRecords().get(0);
        
        //Verify Successful result
        assertEquals(record.get("errorCode"), "0", "ErrorCode");

        //Verify Assignment set to short
        a = this.cmd.getAssignmentManager().get(testAssignment);
        assertEquals(a.getStatus(), AssignmentStatus.Complete, "AssignmentComplete");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteNoPicked() throws Exception {

        initialize();
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        //Test Get Assignment LUT
        Date cmdDate = this.getCmdDateSec();
        Response response = executeCommand(cmdDate, groupNumber.toString());
        
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();

        //Get record back from first run
        assertEquals(response.getRecords().size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        
        //Verify Successful result
        assertEquals(record.getErrorCode(), 
                Long.toString(TaskErrorCode.NOT_ALL_PICKS_PICKED.getErrorCode()),
                "ErrorCode");
        assertFalse(record.getErrorMessage().contains("{"), "No curlies");
        
        // verify that labor records are correct after attempting to close
        // an incomplete assignment.
        verifyOperatorLaborRecords();
        // Verify that the assignment labor record shows that nothing was picked.
        Double dblZero = new Double(0.0);
        Integer quantityPicked = new Integer(0);
        assertEquals(flRec.getCount(), quantityPicked, "operator labor count should be ");
        assertEquals(flRec.getActualRate(), dblZero, "actual rate ");
        assertEquals(flRec.getPercentOfGoal(), dblZero, "percent of goal ");
        
        // verify that assignment labor record is still opened
        verifyIsClosed = false;
        verifyAssignmentLaborRecord(verifyIsClosed);
    }

    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        this.cmd.getAssignmentManager().initializeState();

        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();
        
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDateMS(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDateMS(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", 
            new String[] {makeStringFromDateMS(this.getCmdDateSec()), sn, operId, "3"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", 
            new String[] {makeStringFromDateMS(this.getCmdDateSec()), sn, operId, "2", "3"});
        executeLutCmd("cmdPrTaskLUTGetAssignment",
            new String[] {makeStringFromDateMS(this.getCmdDateSec()), sn, operId, "1", "1"}); 
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        executeLutCmd("cmdPrTaskLUTGetPicks",
            new String[] {makeStringFromDateMS(this.getCmdDateSec()), sn, operId, 
            groupNumber.toString(), "0", "1", "0"}); 
    }
    
    /**
     * Picks all picks in specified assignment.
     * 
     * @param shortCount - number of picks to short
     * @throws Exception - Any Exception
     */
    private void pickAllPicks(int shortCount) throws Exception {
        int pickCount = 0;
        String quantityToPick = "0";
        
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        Response pickResponse = getResponseForCommand("cmdPrTaskLUTGetPicks");

        for (ResponseRecord r : pickResponse.getRecords()) {
            pickCount++;
            if (pickCount++ <= shortCount) {
                quantityToPick = "0";
            } else {
                quantityToPick = r.get("quantityToPick").toString();
            }
            
            executeLutCmd("cmdPrTaskLUTPicked",
                new String[] {makeStringFromDateMS(this.getCmdDateSec()), sn, operId, 
                              groupNumber.toString(), //group number
                              r.get("assignmentPK").toString(), //Assignment ID 
                              r.get("locationID").toString(), //Location ID 
                              quantityToPick, //Quantity Picked 
                              "1", //End of Pick
                              "", //Container ID
                              r.get("pickID").toString(), //Pick ID 
                              "", //Lot Number
                              "", //variable Weight
                              "", //Serial Number
                              }); 

        }
    }

    /** verify operator labor records are correct.
     * 
     * @throws DataAccessException on failure
     */
    private void verifyOperatorLaborRecords() throws DataAccessException {
        
        this.olList =  this.cmd.getLaborManager().getOperatorLaborManager()
                           .listAllRecordsByOperatorId(this.cmd.getOperator().getId());
        assertEquals(olList.size(), 2, "Bad operator labor record count");
        flRec = (OperatorFunctionLabor) olList.get(1);
        assertNull(flRec.getEndTime(), "selection labor record is not open ");
    }
    
    
    /** verify assignment labor records are correct.
     * 
     * @param isClosed - set to true to verify that assignment labor record is closed
     *                   otherwise set to true to verify that labor record is open.
     * @throws DataAccessException on failure
     */
    private void verifyAssignmentLaborRecord(boolean isClosed) throws DataAccessException {
        
         alList = this.cmd.getLaborManager().getAssignmentLaborManager()
                      .listAllRecordsByOperatorLaborId(flRec.getId());
         assertEquals(alList.size(), 1, "Bad Assignment labor record count, ");
         alRec = alList.get(0);
         // if record should be closed, then end time should not be null
         if (isClosed) {
             assertNotNull(alRec.getEndTime(), "assignment labor not closed ");
         } else {
             assertNull(alRec.getEndTime(), "assignment labor not open ");
         }    
    }
    
    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param groupNumber - group number to stop
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String groupNumber) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDateMS(cmdDate), 
                sn, operId, groupNumber}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
