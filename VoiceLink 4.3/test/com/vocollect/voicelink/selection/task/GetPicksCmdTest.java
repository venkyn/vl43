/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.selection.model.SelectionRegionGoBackForShorts.Always;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for GetPicksCmd.
 *
 * @author pfunyak
 */
@Test(groups = { FAST, TASK, SELECTION })
public class GetPicksCmdTest extends TaskCommandTestCase {

    
    private String operId = "GetPicksOper";
    private String sn = "GetPicksSerial";
    
    private GetPicksCmd cmd;
    
    //used to skip picks
    private UpdateStatusCmd cmdUpdateStatus;
    
    
    /**
     * @return a bean for testing.
     */
    private GetPicksCmd getCmdBean() {
        return (GetPicksCmd) getBean("cmdPrTaskLUTGetPicks");
    }
    
    
    /**
     * @return the UpdateStatus command bean for testing.
     */
    private UpdateStatusCmd getUpdateStatusCmdBean() {
        return (UpdateStatusCmd) getBean("cmdPrTaskLUTUpdateStatus");
    }
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @param assignmentID - get this assignment id. Passing an empty string
     *                       means get the first available assignment.
     *                       
     * @throws Exception - Exception
     */
    private void initialize(String assignmentID) throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        this.cmd.getAssignmentManager().initializeState();
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();
        
        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId,
                          locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "3"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "2", "3"});
        
        if (assignmentID != "") {
            Assignment a = 
                this.cmd.getAssignmentManager().get(Long.parseLong(assignmentID));
            a.setReservedBy(operId);
            this.cmd.getAssignmentManager().save(a);
        }
        executeLutCmd("cmdPrTaskLUTGetAssignment",
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1"});
    }
    
    /**
     * Execute a Get Picks command.
     * 
     * @param cmdDate - date for command
     * @param serialNumber - serial number 
     * @param operator - Operator id used for command
     * @param groupNum - groupNumber to use to select picks
     * @param shortsAndSkipsFlag - Determines if pick list contains all picks
     *        or just shorts and skips.
     * @param goBackForShortsIndicator - Determines when to return shorts and 
     *                                   skips.
     * @param pickOrderFlag - Determines of picks are returned in forward or
     *                        reverse order
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String serialNumber,
                                    String operator,
                                    String groupNum,
                                    String shortsAndSkipsFlag,
                                    String goBackForShortsIndicator,
                                    String pickOrderFlag) throws Exception {
        
        this.cmd = getCmdBean();
      
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId, groupNum, shortsAndSkipsFlag, 
                         goBackForShortsIndicator, pickOrderFlag }, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
    
    
    /**
     * Execute an Update Status command.
     * 
     * @param cmdDate - date timestamp.
     * @param serialNumber - serial number 
     * @param operator - Operator id used for command
     * @param groupNum - groupNumber to use to select picks
     * @param locationId - Location to skip.
     * @param skipIndicator - 0 = slot, 1 = alsle.
     * @param status - "s" = skip
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    
    private Response executeUpdateStatusCommand(Date cmdDate,
                                                String serialNumber,
                                                String operator,
                                                String groupNum,
                                                String locationId,
                                                String skipIndicator,
                                                String status) throws Exception {
        
        this.cmdUpdateStatus = getUpdateStatusCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(cmdDate), 
                              serialNumber, operator, groupNum, locationId, skipIndicator, status}, 
                              this.cmdUpdateStatus);
        return getTaskCommandService().executeCommand(this.cmdUpdateStatus);
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }

        
    /**
     * Test method for 
     * {@link com.vocollect.voicelink.core.task.GetPicksCmd#getShortsAndSkipFlag()}.
     */
    @Test()
    public void testGetSetShortsAndSkipsFlag() {
        boolean shortsAndSkipFlag = true;
        this.cmd.setShortsAndSkipsFlag(shortsAndSkipFlag);
        assertEquals(this.cmd.isShortsAndSkipsFlag(), shortsAndSkipFlag, "ShortsAndSkipFlag Getter/Setter");
}

    /**
     * Test method for 
     * {@link com.vocollect.voicelink.core.task.GetPicksCmd#getGoBackForShortsIndicator()}.
     */
    @Test()
    public void testGoBackForShortsIndicator() {
        int goBackForShortIndicator = 1;
        this.cmd.setGoBackForShortsIndicator(goBackForShortIndicator);
        assertEquals(this.cmd.getGoBackForShortsIndicator(), 
                     goBackForShortIndicator, "GoBackForShortsIndicator Getter/Setter");
    }
    
    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.task.GetPicksCmd#getGoBackForShortsIndicatorEnum()}.
     */
    @Test()
    public void testGoBackForShortsIndicatorEnum() {
        int goBackForShortIndicator = 1;
        this.cmd.setGoBackForShortsIndicator(goBackForShortIndicator);
        assertEquals(this.cmd.getGoBackForShortsIndicatorEnum(), Always,
                     "GoBackForShortsIndicator Getter/Setter");
    }
    
    /**
     * Test getter and setter for the pick order flag.
     */
    @Test()
    public void testGetSetPickOrderFlag() {
        short pickOrderFlag = 1;
        this.cmd.setPickOrderFlag(pickOrderFlag);
        assertEquals(this.cmd.getPickOrderFlag(), pickOrderFlag,
                    "SetPickOrderFlag Getter/Setter");
    }

    /**
     * Verify that we allow an empty pick list and the correct "error" message
     * is returned.
     * @throws Exception any
     */
    @Test(enabled = false)
    public void testForEmptyPickList() throws Exception {

        initialize("");
        // execute the getPicks command for groupNumber zero which does not exist
        Response response = 
                 executeCommand(this.getCmdDateSec(), sn, operId, "0",  "0", "0", "0");
        
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord aRecord = records.get(0);

        // verify that we got error message 
        // No picks available for current assignment
        assertEquals(aRecord.getErrorCode(), "99", (String) aRecord.get("errorMessage"));
    }
    
        /**
     * Verify that required fields are populated ie. non-null.
     * @throws Exception any
     */
    @Test(enabled = false)
    public void testForRequiredFields() throws Exception {

        initialize("");
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        Response response = executeCommand(this.getCmdDateSec(), sn, operId,       
                                           groupNumber.toString(),  "0", "0", "0");
        
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord aRecord = records.get(0);
              
        // verify that we did not get an error
        assertEquals(aRecord.get("errorCode"), "0", (String) aRecord.get("errorMessage"));

        // check first pick record returned to verify that 
        //required fields are there.
        assertNotNull(aRecord.get("status"), "status is null");
        assertNotNull(aRecord.get("pickID"), "pickID is null ");
        assertNotNull(aRecord.get("locationID"), "locationID is null ");
        assertNotNull(aRecord.get("regionNumber"), "regionNumber is null");
        assertNotNull(aRecord.get("aisle"), "aisle is null");
        assertNotNull(aRecord.get("slot"), "slot is null");
        assertNotNull(aRecord.get("assignmentPK"), "assignmentID is null ");
        assertNotNull(aRecord.get("itemNumber"), "itemNumber is null ");
    }
    
    /**
     * Verify that we only receive status codes of "B", "N", "G" and "S".
     * @throws Exception any
     */
    @Test(enabled = false)
    public void testForValidStatusCodes() throws Exception {

        initialize("");
        // execute the getPicks command
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        Response response = executeCommand(this.getCmdDateSec(), sn, operId, 
                                           groupNumber.toString(),  "0", "0", "0");
        
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord aRecord = records.get(0);

        // verify that we did not get an error
        assertEquals(aRecord.get("errorCode").toString(), "0",
                     (String) aRecord.get("errorMessage"));
        
        //  Should only get picks with status of 
        // "N" - Not Picked, "B" - BaseItem, "S" - Skipped, "G" - ShortsGoBack
        for (int index = 0; index < records.size(); index++) {
            aRecord = records.get(index);
            String status = aRecord.get("status").toString();
            if ((status != "N") && (status != "B") && (status != "S")
                                && (status != "G")) { 
                fail("Pick Record " + index + " has an invalid status.");
            }    
        }
    }

    /**
     * Verify that setting the pickOrderFlag = 0 causes the picks to be 
     * returned in forward order and setting the pickOrderFlag = 1 causes
     * the picks to be returned in reverse order.
     * @throws Exception any
     */
    @Test(enabled = false)
    public void testForFowardReverseSortOrder() throws Exception {

        initialize("");
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        // execute the getPicks command with "Normal" PickOrder flag
        // result set will be picks list sorted in forward order.
        Response aResponse = executeCommand(this.getCmdDateSec(), sn, operId, 
                                            groupNumber.toString(), "0", "0", "0");
        
        List<ResponseRecord> aRecords = aResponse.getRecords();
        ResponseRecord aRecord = aRecords.get(0);

        // verify that we did not get an error message
        assertEquals(aRecord.get("errorCode"), "0", (String) aRecord.get("errorMessage"));
        

        initialize("");
        groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        // execute the getPicks command with "Reverse" PickOrder flag
        // result set will contain picks sorted in reverse order.
        Response bResponse = executeCommand(this.getCmdDateSec(), sn, operId, 
                                            groupNumber.toString(), "0", "0", "1");
        
        List<ResponseRecord> bRecords = bResponse.getRecords();
        ResponseRecord bRecord = bRecords.get(0);

        // verify that we did not get an error message
        assertEquals(bRecord.get("errorCode"), "0", (String) bRecord.get("errorMessage"));

        // both list should be the same size.
        assertEquals(aRecords.size(), bRecords.size(), "Pick lists differ in size");
        
        // loop through the lists comparing the first pickID in list "A" to the
        // last pickID in list "B". If the lists are sorted correctly then
        // pickID in list "A" should always equal pickID in list "B"
        // Increment list index for list "A" and decrement index for list "B"
        // and compare again.
        for (int index = 0; index < aRecords.size(); index++) {
            aRecord = aRecords.get(index);
            bRecord = bRecords.get(bRecords.size() - index - 1);
            assertEquals(aRecord.get("pickID"), bRecord.get("pickID"), "List is not reversed");
        }
    }
 
    /**
     * Verify that shorts and skips flag works correctly.
     * @throws Exception any
     */
    public void testShortsAndSkipsFlag()  throws Exception {
      
        initialize("");
        // TODO - Phase II: need to fix this test to make sure we also get back 
        //        picks marked as ShortsGoBack
 
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        // Execute UpdateStatus command to skip a pick
        Response aResponse = executeUpdateStatusCommand(this.getCmdDateSec(), sn, operId, 
                                                        groupNumber.toString(), "-945", "0", "S");

        // Get the response record(s)
        List<ResponseRecord> records = aResponse.getRecords();
        ResponseRecord aRecord = records.get(0);
        // verify that we did not get an error message
        assertEquals(aRecord.get("errorCode"), "0", (String) aRecord.get("errorMessage")); 
        
        // Execute UpdateStatus command to skip another pick
        aResponse = executeUpdateStatusCommand(this.getCmdDateSec(), sn, operId, 
                                               groupNumber.toString(), "-940", "0", "G");
        aRecord = records.get(0);
        // verify that we did not get an error message
        assertEquals(aRecord.get("errorCode"), "0", (String) aRecord.get("errorMessage")); 
        // execute the getPicks command with shortsAndSkipsFlag = false
        // we should get all picks back including shorts and skips.
        aResponse =  executeCommand(this.getCmdDateSec(), sn, operId, 
                                    groupNumber.toString(), "0", "0", "0");
        aRecord = records.get(0);
        // verify that we did not get an error message
        assertEquals(aRecord.get("errorCode"), "0", (String) aRecord.get("errorMessage"));    
        // verify that we did not get an error message
        assertEquals(aResponse.getRecords().size(), 5, "Did not get all picks back");
        // execute the getPicks command with shortsAndSkipsFlag = true
        // we should only get back picks that have a status of ShortGoBack or 
        // Skipped.
        aResponse = executeCommand(this.getCmdDateSec(), sn, operId, 
                                   groupNumber.toString(), "1", "0", "0");
        aRecord = records.get(0);
        // verify that we did not get an error message
        assertEquals(aRecord.get("errorCode"), "0", (String) aRecord.get("errorMessage"));    
        // verify that we did not get an error message
        assertEquals(aResponse.getRecords().size(), 1, "Did not get all picks back");
    }
}
