/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.core.model.OperatorFunctionLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for GetConfigurationCmd.
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, TASK, SELECTION })
public class PickingRegionCmdTest extends TaskCommandTestCase {

    private PickingRegionCmd cmd;
    private String operId = "PickingRegionSerial";
    private String sn = "PickingRegionSerial";
    
    /**
     * @return a bean for testing.
     */
    private PickingRegionCmd getCmdBean() {
        return (PickingRegionCmd) getBean("cmdPrTaskLUTPickingRegion");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickingRegionCmd#getSelectedWorkType()}.
     */
    @Test()
    public void testGetSelectedWorkType() {
        this.cmd.setSelectedWorkType("1");
        assertEquals(this.cmd.getSelectedWorkType(), "1", "GetSelectedWorkType");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickingRegionCmd#getSelectedWorkType()}.
     */
    @Test()
    public void testGetSetRegionNumber() {
        this.cmd.setRegionNumber(1);
        assertEquals(this.cmd.getRegionNumber(), 1, "RegionNumber");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickingRegionCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteNormalWorkType() throws Exception {
        
        initialize();
        //Test picking a valid region
        Response response = executeCommand(this.getCmdDateSec(), "1", "3");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        
        //Get Region
        Region r = this.cmd.getRegionManager().findRegionByNumber((Integer) record.get("regionNumber"));

        assertEquals(record.get("regionNumber"), r.getNumber(), "regionNumber");
        assertEquals(record.get("regionName"), r.getName(), "regionName");
        assertEquals(record.get("assignmentType"), AssignmentType.Normal.getValue(),
                     "assignmentType");
        //Verify Operator's current region is set
        assertEquals(this.cmd.getOperator().getCurrentRegion().getNumber(), (Integer) 1,
                     "OperatorCurrentRegion");

        //Test picking an invalid region
        response = executeCommand(this.getCmdDateSec(), "99", "3");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "invalidRegion recordSize");
        record = records.get(0);
        assertEquals(record.get("errorCode").toString(), 
                Long.toString(TaskErrorCode.INVALID_REGION_NUMBER.getErrorCode()), "invalidRegion errorCode");

        //Test picking a region with in progress in other region
        response = executeCommand(this.getCmdDateSec(), "2", "3");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "inProgress recordSizeA");
        record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "inProgress errorCodeA");
        
        //Start assignment in region 1
        response = executeLutCmd("cmdPrTaskLUTGetAssignment", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1" });

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "inProgress recordSizeB");
        record = records.get(0);
        assertEquals(record.get("errorCode").toString(), "0", "inProgress errorCodeB");
        
        //Attempt to pick region 2
        response = executeCommand(this.getCmdDateSec(), "1", "3");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "inProgress recordSizeC");
        record = records.get(0);
        assertEquals(record.get("errorCode"), 
                Long.toString(TaskErrorCode.IN_PROGRESS_IN_OTHER_REGION.getErrorCode()), 
                "inProgress errorCodeC");
        
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickingRegionCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteChaseWorkType() throws Exception {

        initialize();
        //Test picking a valid region
        Response response = executeCommand(this.getCmdDateSec(), "2", "4");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        
        // Get Region
        Region r = this.cmd.getRegionManager().findRegionByNumber((Integer) record.get("regionNumber"));
        
        assertEquals(record.get("regionNumber"), r.getNumber(), "regionNumber");
        assertEquals(record.get("regionName"), r.getName(), "regionName");
        assertEquals(record.get("assignmentType"), AssignmentType.Chase.getValue(), "assignmentType");

        //Test picking an invalid region
        response = executeCommand(this.getCmdDateSec(), "99", "4");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "invalidRegion recordSize");
        record = records.get(0);
        assertEquals(record.get("errorCode"), 
                Long.toString(TaskErrorCode.INVALID_REGION_NUMBER.getErrorCode()),
                "invalidRegion errorCode");
        
        //===============================================================
        //Test picking a region with in progress in other region
        response = executeCommand(this.getCmdDateSec(), "3", "4");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "inProgress recordSizeA");
        record = records.get(0);
        assertEquals(record.get("errorCode").toString(), "0", "inProgress errorCodeA");
        
        //Add assignments
        classSetupSelectionData();

        //Start assignment in region 1
        response = executeLutCmd("cmdPrTaskLUTGetAssignment", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1" });

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "inProgress recordSizeB");
        record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "inProgress errorCodeB");
        
        //Attempt to pick region 2
        response = executeCommand(this.getCmdDateSec(), "1", "4");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "inProgress recordSizeC");
        record = records.get(0);
        assertEquals(record.get("errorCode").toString(), 
                Long.toString(TaskErrorCode.IN_PROGRESS_IN_OTHER_REGION.getErrorCode()),
                "inProgress errorCodeC");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickingRegionCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteBothWorkType() throws Exception {

        initialize();
        //Test picking a valid region
        Response response = executeCommand(this.getCmdDateSec(), "1", "6");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 2, "recordSize");
        ResponseRecord record = records.get(0);
        
        //Get Region
        Region r = this.cmd.getRegionManager().findRegionByNumber((Integer) record.get("regionNumber"));
        assertEquals(record.get("regionNumber"), r.getNumber(), "regionNumber");
        assertEquals(record.get("regionName"), r.getName(), "regionName");
        assertEquals(record.get("assignmentType"), AssignmentType.Chase.getValue(), "assignmentType");
        record = records.get(1);
        
        //Get Region
        r = this.cmd.getRegionManager().findRegionByNumber((Integer) record.get("regionNumber"));
        assertEquals(record.get("regionNumber"), r.getNumber(), "regionNumber");
        assertEquals(record.get("regionName"), r.getName(), "regionName");
        assertEquals(record.get("assignmentType"), AssignmentType.Normal.getValue(), "assignmentType");

        //Test that a selection labor record was opened for the correct region
        OperatorFunctionLabor operLabor = (OperatorFunctionLabor)
            this.cmd.getLaborManager().getOperatorLaborManager()
                .findOpenRecordByOperatorId(this.cmd.getOperator().getId());
        if (operLabor == null) {
            fail("Selection Labor Record was not created");
        } else {
            assertEquals(operLabor.getActionType(), OperatorLaborActionType.Selection, 
                         "Invalid labor record type ");
            assertEquals(operLabor.getRegion().getId(), r.getId(), 
                         "Invalid region id in labor record");
            assertEquals(operLabor.getCount(), new Integer(0), "count is not zero ");
        }
        
        //===============================================================
        //Test picking an invalid region
        response = executeCommand(this.getCmdDateSec(), "99", "6");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "invalidRegion recordSize");
        record = records.get(0);
        assertEquals(record.get("errorCode"),
                Long.toString(TaskErrorCode.INVALID_REGION_NUMBER.getErrorCode()), 
                "invalidRegion errorCode");

        //Test picking a region with in progress in other region
        response = executeCommand(this.getCmdDateSec(), "2", "6");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 2, "inProgress recordSizeA");
        record = records.get(0);
        assertEquals(record.get("errorCode").toString(), "0", "inProgress errorCodeA1");
        record = records.get(1);
        assertEquals(record.get("errorCode").toString(), "0", "inProgress errorCodeA2");
        
        //===============================================================
        //Test that a selection labor record was opened for the correct region
        r = this.cmd.getRegionManager().findRegionByNumber((Integer) record.get("regionNumber"));
        operLabor = (OperatorFunctionLabor)
            this.cmd.getLaborManager().getOperatorLaborManager()
                .findOpenRecordByOperatorId(this.cmd.getOperator().getId());
        if (operLabor == null) {
            fail("Selection Labor Record was not created");
        } else {
            assertEquals(operLabor.getActionType(), OperatorLaborActionType.Selection, 
                         "Invalid labor record type ");
            assertEquals(operLabor.getRegion().getId(), r.getId(), 
                         "Invalid region id in labor record");
        }
        
        //Add assignments
        classSetupSelectionData();

        // Start assignment in region 1
        response = executeLutCmd("cmdPrTaskLUTGetAssignment", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1" });

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "inProgress recordSizeB");
        record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "inProgress errorCodeB");
        
        // Attempt to pick region 2
        response = executeCommand(this.getCmdDateSec(), "1", "6");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "inProgress recordSizeC");
        record = records.get(0);
        assertEquals(record.get("errorCode"), 
                Long.toString(TaskErrorCode.IN_PROGRESS_IN_OTHER_REGION.getErrorCode()),
                "inProgress errorCodeC");
    }
    
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "3"});
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param regionNumber - region number requested
     * @param workType - work type
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String regionNumber,
                                    String workType) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(cmdDate), 
                sn, operId, regionNumber, workType},   this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
