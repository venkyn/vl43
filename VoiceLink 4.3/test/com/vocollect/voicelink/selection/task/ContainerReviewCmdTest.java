/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for ContainerReviewCmd.
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, SELECTION })
public class ContainerReviewCmdTest extends TaskCommandTestCase {

    private ContainerReviewCmd cmd;
    private String operId = "ContainerReviewOper";
    private String sn = "ContainerReviewSerial";
    
    /**
     * @return a bean for testing.
     */
    private ContainerReviewCmd getCmdBean() {
        return (ContainerReviewCmd) getBean("cmdPrTaskLUTContainerReview");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ContainerReviewCmd#getGroupNumber()}.
    */
    @Test()
    public void testGetGroupNumber() {
        this.cmd.setGroupNumber(1L);
        assertEquals(this.cmd.getGroupNumber().toString(), "1", "GetGroupNumber");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ContainerReviewCmd#getAssignmentPk()}.
    */
    @Test()
    public void testGetAssignmentPk() {
        this.cmd.setAssignmentPk(1L);
        assertEquals(this.cmd.getAssignmentPk().toString(), "1", "GetAssignmentPk");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ContainerReviewCmd#getPickContainerPk()}.
    */
    @Test()
    public void testGetPickContainerPk() {
        this.cmd.setPickContainerPk(1L);
        assertEquals(this.cmd.getPickContainerPk().toString(), "1", "getPickContainerPk");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ContainerCmd#execute}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize();
        //Get a list of all containers in the system
        List<Container> allContainers = this.cmd.getContainerManager().listAllContainers();
        
        Assignment a = cmd.getAssignmentManager().get(-689L);
        Long groupNumber = a.getGroupInfo().getGroupNumber(); 
        
        //Test reviewing a non existent container
        Response response = executeCommand(this.getCmdDateSec(), "-1", groupNumber.toString(), "-689");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(1, records.size(), "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), 
                Long.toString(TaskErrorCode.CONTAINER_NOT_FOUND.getErrorCode()), "errorCode"); 
        
        //Test reviewing an empty container
        response = executeCommand(this.getCmdDateSec(), allContainers.get(0).getId().toString(), 
            groupNumber.toString(), "-689");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(1, records.size(), "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), 
                Long.toString(TaskErrorCode.CONTAINER_EMPTY.getErrorCode()), "errorCode"); 
        
        //Test reviewing a valid container with a pick
        executeLutCmd("cmdPrTaskLUTPicked", new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
                      groupNumber.toString(), "-689", "-1", "1", "1", allContainers.get(0).getId().toString(), 
                      "-5706", "", "", "" });  
        response = executeCommand(this.getCmdDateSec(), allContainers.get(0).getId().toString(), 
                                  groupNumber.toString(), "-689");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "errorCode"); 
        assertEquals(record.get("itemDescription").toString(), "BASIC ULTRA LIGHTS", "itemDescription");
        assertEquals(record.get("quantity"), 1, "quantity");
        assertEquals(record.get("location"), "0161121", "location");
    }
    
    

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        this.cmd.getAssignmentManager().initializeState();
        
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "15", "3" });
        executeLutCmd("cmdPrTaskLUTGetAssignment", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1" });

        Assignment a = cmd.getAssignmentManager().get(-689L);
        Long groupNumber = a.getGroupInfo().getGroupNumber(); 
        executeLutCmd("cmdPrTaskLUTContainer", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            groupNumber.toString(), "-689", "", "", "", "3", "3" });   
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param groupNumber - group of assignments that were issued
     * @param assignmentPk - assignment pick belongs with
     * @param pickContainerPk - internal id of container previously sent to task
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String pickContainerPk, 
                                    String groupNumber,
                                    String assignmentPk) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(cmdDate), sn, 
                operId, pickContainerPk, groupNumber, assignmentPk},  this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
