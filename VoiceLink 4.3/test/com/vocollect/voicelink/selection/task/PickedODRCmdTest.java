/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.*;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test for PickODRCmd.
 *
 * @author pfunyak
 */
@Test(groups = { FAST, TASK, SELECTION })
public class PickedODRCmdTest extends TaskCommandTestCase {

    // ***********************************************************************
    // Test picked ODR
    // This tests that no response object is returned and that duplicate
    // ODR's are rejected. All other Picked tests are performed in the PickedLUTCmdTest
    // ***********************************************************************    
    
    private PickedODRCmd odr1;
    private PickedODRCmd odr2;
    private PickedODRCmd odr3;
    
    /**
     * @return a bean for testing.
     */
    private PickedODRCmd getCmdBean() {
        return (PickedODRCmd) getBean("cmdPrTaskODRPicked");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.odr1 = getCmdBean();
        this.odr2 = getCmdBean(); 
        this.odr3 = getCmdBean(); 
    }
    
    /**
     * @throws Exception
     */
    @Test()
    public void testRollOverODRCommand() throws Exception {

        //    TASK_COMMAND_MILLI_DATE_FORMAT = "MM-dd-yy HH:mm:ss.SS";
        final String operId = "PickedODRRolloverOper";
        String sn = "PickedODRRolloverSerial";

        //Initialize data
        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yy HH:mm:ss.SSS");
        Date testDate = formatter.parse("01-09-09 07:28:00.930");
        this.setCmdDate(testDate);
     
        initialize(sn, operId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        testDate = formatter.parse("01-09-09 07:27:59.990");
        this.setCmdDate(testDate);
        
        // The date on this command should be  01-09-09 07:28:00.00
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDateMS(this.getCmdDateMS()), 
                               sn, operId, groupNumber.toString(),
                               "-1", "-1", "1", "1", "", "-874", "", "2.3", ""}, this.odr1);

        Pick thePickAfterFirstUpdate = null;
        Pick thePickAfterSecondUpdate = null;
        
        // Call the Picked ODR command for the initial update.
        // This should be successful and the pick should be updated.
        Response response = getTaskCommandService().executeCommand(this.odr1);
        assertNull(response, "response was not null");
        thePickAfterFirstUpdate = this.odr1.getPickManager().get(-874L);
        if (thePickAfterFirstUpdate.getQuantityPicked() != 1) {
            fail("Pick was not updated");
        }

        testDate = formatter.parse("01-09-09 07:28:01.050");
        this.setCmdDate(testDate);
        // The date on this command should be  01-09-09 07:28:01.50
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDateMS(this.getCmdDateMS()), 
                              sn, operId, groupNumber.toString(), "-1",
                              "-1", "1", "1", "", "-875", "", "2.3", ""}, this.odr2);        
        response = getTaskCommandService().executeCommand(this.odr2);
        assertNull(response, "response was not null");
        // Since there is not response returned for an ODR we can not check for an error 
        // core or message.  To confirm the ODR failure, make sure the pick did not change.
        thePickAfterSecondUpdate = this.odr2.getPickManager().get(-875L);
        if (thePickAfterSecondUpdate.getQuantityPicked() != 1) {
            fail("Pick was updated");
        }

        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDateMS(this.getCmdDateMS()), 
                sn, operId, groupNumber.toString(), "-1",
                "-1", "1", "1", "", "-876", "", "2.3", ""}, this.odr3);         
        response = getTaskCommandService().executeCommand(this.odr3);
        assertNull(response, "response was not null");
        // Since there is not response returned for an ODR we can not check for an error 
        // core or message.  To confirm the ODR failure, make sure the pick did not change.
        thePickAfterSecondUpdate = this.odr3.getPickManager().get(-876L);
        if (thePickAfterSecondUpdate.getQuantityPicked() != 1) {
            fail("Pick was updated");
        }
    }

    
    
    /**
     * @throws Exception
     */
    @Test()
    public void testDuplicateODRCommand() throws Exception {

        final String operId = "PickedODROper";
        String sn = "PickedODRSerial";

        //Initialize data
        initialize(sn, operId);
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        Date cmdDate = this.getCmdDateMS();
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDateMS(cmdDate), 
                               sn, operId, groupNumber.toString(),
                               "-1", "-1", "1", "0", "", "-874", "", "2.3", ""}, this.odr1);

        Pick thePickAfterFirstUpdate = null;
        Pick thePickAfterSecondUpdate = null;
        
        // Call the Picked ODR command for the initial update.
        // This should be successful and the pick should be updated.
        Response response = getTaskCommandService().executeCommand(this.odr1);
        assertNull(response, "response was not null");
        thePickAfterFirstUpdate = this.odr1.getPickManager().get(-874L);
        if (thePickAfterFirstUpdate.getQuantityPicked() != 1) {
            fail("Pick was not updated");
        }
        // Call the Picked ODR command with a different odr command object
        // for the second update with the same timestamp.
        // The second call should fail.
        
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDateMS(cmdDate), 
                              sn, operId, groupNumber.toString(), "-1",
                              "-1", "1", "0", "", "-874", "", "2.3", ""}, this.odr2);        
        response = getTaskCommandService().executeCommand(this.odr2);
        assertNull(response, "response was not null");
        // Since there is not response returned for an ODR we can not check for an error 
        // core or message.  To confirm the ODR failure, make sure the pick did not change.
        thePickAfterSecondUpdate = this.odr2.getPickManager().get(-874L);
        if (thePickAfterSecondUpdate.getQuantityPicked() != 1) {
            fail("Pick was updated");
        }
    }
    
    

    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @param sn - Serial Number
     * @param operId - Operator
     * @throws Exception - Exception
     */
    private void initialize(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;
        
        this.odr1.getAssignmentManager().initializeState();
        this.odr2.getAssignmentManager().initializeState();
        this.odr3.getAssignmentManager().initializeState();
        
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        // Initialize
        // Sleep for 1 millisecond between commands
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", 
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, "2", "3"});
        executeLutCmd("cmdPrTaskLUTGetAssignment",
            new String[] {makeStringFromDateMS(this.getCmdDateMS()), sn, operId, "1", "1"}); 

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        executeLutCmd("cmdPrTaskLUTGetPicks", new String[] {makeStringFromDateMS(this.getCmdDateMS()), 
                       sn, operId, groupNumber.toString(), "0", "1", "0"}); 
    }
}
