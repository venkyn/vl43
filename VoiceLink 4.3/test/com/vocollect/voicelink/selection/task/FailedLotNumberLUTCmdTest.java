/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test for FailedLotNumberLUTCmd.
 *
 * @author mnichols
 */
@Test(groups = { FAST, TASK, SELECTION })
public class FailedLotNumberLUTCmdTest extends TaskCommandTestCase {

    private String operId = "FailedLotNumberLUTOper";
    private String sn = "FailedLotNumberLUTSerial";
    
    private FailedLotNumberLUTCmd cmd;

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }
    
    /**
     * @return a bean for testing.
     */
    private FailedLotNumberLUTCmd getCmdBean() {
        return (FailedLotNumberLUTCmd) getBean("cmdPrTaskLUTFailedLotNumber");
    }
    
    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @param assignmentID - get this assignment id. Passing an empty string
     *                       means get the first available assignment.
     *                       
     * @throws Exception - Exception
     */
    private void initialize(String assignmentID) throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        this.cmd.getAssignmentManager().initializeState();
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();
        
        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId,
                          locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "3"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "2", "3"});
        
        if (assignmentID != "") {
            Assignment a = 
                this.cmd.getAssignmentManager().get(Long.parseLong(assignmentID));
            a.setReservedBy(operId);
            this.cmd.getAssignmentManager().save(a);
        }
        executeLutCmd("cmdPrTaskLUTGetAssignment",
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1"});
    }
    
    /**
     * @param cmdDate - command date
     * @param serialNumber - serial number
     * @param operator - operator
     * @param groupNum - group number
     * @param assignmentID - assignment ID
     * @param pickID - pick ID
     * @param lotNumber - lot number
     * @return Response
     * @throws Exception
     */
    private Response executeFailedLotNumberLUTCommand(Date cmdDate,
                                                      String serialNumber,
                                                      String operator,
                                                      String groupNum,
                                                      String assignmentID,
                                                      String pickID,
                                                      String lotNumber) throws Exception {
              
          cmd = (FailedLotNumberLUTCmd) getBean("cmdPrTaskLUTFailedLotNumber");
          // Run the call the first time
          getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(cmdDate), 
                                serialNumber, operator, groupNum, assignmentID, pickID, lotNumber}, cmd);
          return getTaskCommandService().executeCommand(cmd);
      }
    
    /**
     * Verify that shorts and skips flag works correctly.
     * @throws Exception any
     */
    public void testFailedLotNumber()  throws Exception {
        
        // Initialize  Data
        initialize("");
 
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        // Execute Failed Lot command to mark a lot invalid
        Response aResponse = 
            this.executeFailedLotNumberLUTCommand(this.getCmdDateSec(), sn, operId, groupNumber.toString(),
                                                   "-46", "-361", "-1");

        // Get the response record(s)
        List<ResponseRecord> records = aResponse.getRecords();
        ResponseRecord aRecord = records.get(0);

        // verify that we did not get an error message
        assertEquals(aRecord.get("errorCode"), "0", (String) aRecord.get("errorMessage")); 
    }
}

