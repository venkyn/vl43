/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.Container;
import com.vocollect.voicelink.selection.model.ContainerStatus;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for ContainerCmd.
 *
 * @author sfahnestock
 */

@Test(groups = { FAST, TASK, SELECTION })

public class ContainerCmdTest extends TaskCommandTestCase {

    private ContainerCmd cmd;
    private String operId = "SelectionContainerOper";
    private String sn = "SelectionContainerSerial";
    private Container c;
    
    private Response response;
    private Map<String, Object> record;
    
    /**
     * @return a bean for testing.
     */
    private ContainerCmd getCmdBean() {
        return (ContainerCmd) getBean("cmdPrTaskLUTContainer");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ContainerCmd#getGroupNumber()}.
    */
    public void testGetGroupNumber() {
        this.cmd.setGroupNumber(1L);
        assertEquals(this.cmd.getGroupNumber().toString(), "1", "GetGroupNumber");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ContainerCmd#getAssignmentPk()}.
    */
    public void testGetAssignmentPk() {
        this.cmd.setAssignmentPk("1");
        assertEquals(this.cmd.getAssignmentPk().toString(), "1", "GetAssignmentPk");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ContainerCmd#getPickContainerPk()}.
    */
    public void testGetPickContainerPk() {
        this.cmd.setPickContainerPk("1");
        assertEquals(this.cmd.getPickContainerPk(), "1", "getPickContainerPk");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ContainerCmd#getTargetContainer()}.
    */
    public void testGetTargetContainer() {
        this.cmd.setTargetContainer("1");
        assertEquals(this.cmd.getTargetContainer(), "1", "getTargetContainer");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ContainerCmd#getContainerNumber()}.
    */
    public void testGetContainerNumber() {
        this.cmd.setContainerNumber("1");
        assertEquals(this.cmd.getContainerNumber(), "1", "getContainerNumber");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ContainerCmd#getOperation()}.
    */
    public void testGetOperation() {
        this.cmd.setOperation(1);
        assertEquals(this.cmd.getOperation(), 1, "getOperation");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ContainerCmd#getLabels()}.
    */
    public void testGetLabels() {
        this.cmd.setLabels("1");
        assertEquals(this.cmd.getLabels().toString(), "1", "getLabels");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.ContainerCmd#execute}.
     * @throws Exception any
     */
    public void testExecute() throws Exception {

        // Initialize Data
        initialize();

        Assignment a = cmd.getAssignmentManager().get(-689L);
        String strGroupNumber = a.getGroupInfo().getGroupNumber().toString();
        
        verifyTooFewDigitsError(strGroupNumber);
        verifyPreCreateContainers(strGroupNumber);
        verifyOpenAvaliableContainer(strGroupNumber);
        verifyOpenWithError(strGroupNumber);
        verifyCloseContainer(strGroupNumber);
        verifyOpenAlreadyClosedError(strGroupNumber);
        verifyDuplicateContainerNumberError(strGroupNumber);
    }
    
    
    /**
     * @param strGroupNumber - group number string
     * @throws Exception
     */
    private void verifyTooFewDigitsError(String strGroupNumber) throws Exception {

        //Test Opening a container with too few digits error
        this.response = executeCommand(this.getCmdDateMS(), strGroupNumber, "-689", "", "", "1", "2", ""); 
        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize Open tooFew");
        this.record = response.getRecords().get(0);
        assertEquals(record.get("errorCode"), 
                     Long.toString(TaskErrorCode.INVALID_CONTAINER_NUMBER.getErrorCode()), "errorCode open tooFew");
    }
    
    
    /**
     * @param strGroupNumber - group number string
     * @throws Exception
     */
    private void verifyPreCreateContainers(String strGroupNumber) throws Exception {
        
        //Test pre-creating some containers
        response = executeCommand(this.getCmdDateMS(), strGroupNumber, "-689", "", "", "", "3", "3");
        //Verify Successful result
        assertEquals(response.getRecords().size(), 3, "recordSize precreate");
        
        for (int i = 0; i < 3; i++) {
            record = response.getRecords().get(i);
            // Get Container 
            Long containerIdB = (Long) record.get("systemContainerPk");
            Container cB = this.cmd.getContainerManager().get(containerIdB);
            assertNotNull(cB, "GotContainer precreate");
            assertEquals(record.get("errorCode"), "0", "errorCode precreate");
            assertEquals(record.get("systemContainerPk"), cB.getId(), "systemContainerPk precreate");
            assertEquals(record.get("scannedContainerValidation"), cB.getContainerNumber(),  
                         "scannedContainerValidation precreate");
            assertEquals(record.get("assignmentPk").toString(), "-689", "assignmentPk precreate");
            assertEquals(record.get("idDescription").toString(), "115107317", "idDescription precreate");
            if (cB.getTargetContainerIndicator() == null) {
                assertEquals(record.get("targetContainer").toString(), "", "targetContainer precreate");
            } else {
                assertEquals(record.get("targetContainer"), cB.getTargetContainerIndicator(), 
                             "targetContainer precreate");
            }
            if (cB.getStatus().equals(ContainerStatus.Available)) {
                assertEquals(record.get("containerStatus").toString(), "A", "containerStatus precreate");
            } else if (cB.getStatus().equals(ContainerStatus.Closed)) {
                assertEquals(record.get("containerStatus").toString(), "C",  "containerStatus precreate");
            } else {
                assertEquals(record.get("containerStatus").toString(), "O", "containerStatus precreate");
            }
            assertEquals(record.get("printed"), cB.getPrinted(), "printed precreate");
        }
    }
    
        
    /**
     * @param strGroupNumber - group number string
     * @throws Exception
     */
    private void verifyOpenAvaliableContainer(String strGroupNumber) throws Exception {

        //Test Opening an available container
        response = executeCommand(this.getCmdDateMS(), strGroupNumber, "-689", "", "", "", "2", "");
        //Verify Successful result
        assertEquals(response.getRecords().size(), 3, "recordSize Open");

        for (int i = 0; i < 3; i++) {
            record = response.getRecords().get(i);
            // Get Container 
            Long containerId = (Long) record.get("systemContainerPk");
            c = this.cmd.getContainerManager().get(containerId);
            assertNotNull(c, "GotContainer Open");
            assertEquals(record.get("errorCode"), "0", "errorCode Open");
            assertEquals(record.get("systemContainerPk"), c.getId(), "systemContainerPk Open");
            assertEquals(record.get("scannedContainerValidation"), c.getContainerNumber(),
                         "scannedContainerValidation Open");
            assertEquals(record.get("assignmentPk").toString(), "-689", "assignmentPk Open");
            assertEquals(record.get("idDescription").toString(), "115107317", "idDescription Open");
            if (c.getTargetContainerIndicator() == null) {
                assertEquals(record.get("targetContainer").toString(), "", "targetContainer Open");
            } else {
                assertEquals(record.get("targetContainer"), c.getTargetContainerIndicator(),
                             "targetContainer Open");
            }
            if (c.getStatus().equals(ContainerStatus.Available)) {
                assertEquals(record.get("containerStatus").toString(), "A", "containerStatus Open");
            } else if (c.getStatus().equals(ContainerStatus.Closed)) {
                assertEquals(record.get("containerStatus").toString(), "C", "containerStatus Open");
            } else {
                assertEquals(record.get("containerStatus").toString(), "O", "containerStatus Open");
            }
            assertEquals(record.get("printed"), c.getPrinted(), "printed Open");
        }
    }
    
    
    /**
     * @param strGroupNumber - group number string
     * @throws Exception
     */
    private void verifyOpenWithError(String strGroupNumber) throws Exception {
        
        //Test Opening a container with already existing error
        response = executeCommand(this.getCmdDateMS(), strGroupNumber, "-689", "", "",
                                  c.getContainerNumber(), "2", "");
        
        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize Open alreadyExists");
        record = response.getRecords().get(0);
        assertEquals(record.get("errorCode"), "1021", "errorCode open alreadyExists");
    }
    
    
    /**
     * @param strGroupNumber - group number string
     * @throws Exception
     */
    private void verifyCloseContainer(String strGroupNumber) throws Exception {
        
        //Test Closing a container
        response = executeCommand(this.getCmdDateMS(), strGroupNumber, "-689", "", 
                                  c.getId().toString(), "", "1", "");
        
        //Verify Successful result
        assertEquals(3, response.getRecords().size(), "recordSize Close");
        for (int i = 0; i < 3; i++) {
            record = response.getRecords().get(i);
            // Get Container 
            Long containerIdA = (Long) record.get("systemContainerPk");
            Container cA = this.cmd.getContainerManager().get(containerIdA);
            assertNotNull(cA, "GotContainer Close");
            assertEquals(record.get("errorCode"), "0", "errorCode Close");
            assertEquals(record.get("systemContainerPk"), cA.getId(), "systemContainerPk Close");
            assertEquals(record.get("scannedContainerValidation"), cA.getContainerNumber(), 
                         "scannedContainerValidation Close");
            assertEquals(record.get("assignmentPk").toString(), "-689", "assignmentPk Close");
            assertEquals(record.get("idDescription").toString(), "115107317", "idDescription Close");
            if (cA.getTargetContainerIndicator() == null) {
                assertEquals(record.get("targetContainer").toString(), "", "targetContainer Close");
            } else {
                assertEquals(record.get("targetContainer"), cA.getTargetContainerIndicator(),
                             "targetContainer Close");
            }
            if (cA.getStatus().equals(ContainerStatus.Available)) {
                assertEquals(record.get("containerStatus").toString(), "A", "containerStatus Close");
            } else if (cA.getStatus().equals(ContainerStatus.Closed)) {
                assertEquals(record.get("containerStatus").toString(), "C", "containerStatus Close");
            } else {
                assertEquals(record.get("containerStatus").toString(), "O", "containerStatus Close");
            }
            assertEquals(record.get("printed"), cA.getPrinted(), "printed Close");
        }
    }
    
    
    /**
     * @param strGroupNumber - group number string
     * @throws Exception
     */
    private void verifyOpenAlreadyClosedError(String strGroupNumber) throws Exception {
        
        //Test Opening a container with already closed error
        response = executeCommand(this.getCmdDateMS(), strGroupNumber, "-689", "", "", 
                                  c.getContainerNumber(), "2", "");
        
        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize Open alreadyClosed");
        record = response.getRecords().get(0);
        assertEquals(record.get("errorCode"), 
                Long.toString(TaskErrorCode.CONTAINER_ALREADY_CLOSED.getErrorCode()),  "errorCode open alreadyClosed");
    }
    
    
    /**
     * @param strGroupNumber - group number string
     * @throws Exception
     */
    private void verifyDuplicateContainerNumberError(String strGroupNumber) throws Exception {
  
        //Test Opening a container with duplicate container number error
        // Get container record
        response = executeCommand(this.getCmdDateMS(), strGroupNumber, "-689", "", "", "", "0", "");
        // Close all open containers
        for (ResponseRecord rr : response.getRecords()) {
            record = rr;
            // Get Container 
            Long containerIdC = (Long) record.get("systemContainerPk");
            Container cC = this.cmd.getContainerManager().get(containerIdC);
            if (cC.getStatus().equals(ContainerStatus.Available) 
                || cC.getStatus().equals(ContainerStatus.Open)) {
                response = executeCommand(this.getCmdDateMS(),  strGroupNumber, 
                                           "-96", "", cC.getId().toString(), "", "1", "");
            }
        }
        // Open a new container with number 1234
        response = executeCommand(this.getCmdDateMS(), strGroupNumber, "-689", "", "", "1234", "2", "");
        // Open a new container with number 34 and get error
        response = executeCommand(this.getCmdDateMS(), strGroupNumber, "-689", "", "", "5634", "2", "");
        //Verify Successful result
        assertEquals(response.getRecords().size(), 1, "recordSize Open duplicateNumber");
        record = response.getRecords().get(0);
        assertEquals(record.get("errorCode"),
                Long.toString(TaskErrorCode.CONTAINER_NUMBER_DUPLICATE.getErrorCode()), 
                "errorCode open duplicateNumber");
    }
    
    
    
    
    /**
     * Initializes database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        this.cmd.getAssignmentManager().initializeState();
        
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        // Initialize
        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "3" });
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "15", "3" });
        executeLutCmd("cmdPrTaskLUTGetAssignment", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1" });
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param groupNumber - group of assignments that were issued
     * @param assignmentPk - assignment pick belongs with
     * @param pickContainerPk - internal id of container previously sent to task
     * @param targetContainer - target container if picking to target containers
     * @param containerNumber - if operator specified container number
     * @param operation - operation to perform
     * @param labels - determines the number of labels to pre-create
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String groupNumber, 
                                    String assignmentPk,
                                    String pickContainerPk,
                                    String targetContainer,
                                    String containerNumber,
                                    String operation,
                                    String labels) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDateMS(cmdDate), sn, operId, 
                groupNumber, assignmentPk, pickContainerPk, targetContainer,
                containerNumber, operation, labels}, 
            this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
    
    /**
     * test to verify jira isue VLINK-3236. Covers scenario of duplicate command
     * when initial request timesout
     * @throws Exception - all exceptions
     */
    public void testVlink3236() throws Exception{
        Date cmdDate = new Date(System.currentTimeMillis() + 100000000L);
        
        // Initialize Data
        initialize();

        Assignment a = cmd.getAssignmentManager().get(-689L);
        String strGroupNumber = a.getGroupInfo().getGroupNumber().toString();
        int containerCount = a.getContainers().size();
        assertEquals(0, containerCount);
        
        // Open a new and only container 
        response = executeCommand(cmdDate, strGroupNumber, "-689", "", "", "", "2", "");
        assertEquals(containerCount + 1, a.getContainers().size());
        assertEquals(ContainerStatus.Open, a.getContainers().iterator().next().getStatus());
        
        //send same command to simulate time out scenario, and assert for duplicate command
        response = executeCommand(cmdDate, strGroupNumber, "-689", "", "", "", "2", "");
        assertEquals(
            "VoiceLink-" + response.getRecords().get(0).getErrorCode(),
            TaskErrorCode.DUPLICATE_COMMAND_CALL.toString());
        assertEquals(containerCount + 1, a.getContainers().size());
        assertEquals(ContainerStatus.Open, a.getContainers().iterator().next().getStatus());
        
        //Scenario after user says ready to Duplicate command prompt
        cmdDate.setSeconds(cmdDate.getSeconds() + 10);
        response = executeCommand(cmdDate, strGroupNumber, "-689", "", "", "", "2", "");
        assertNotSame(
            "VoiceLink-" + response.getRecords().get(0).getErrorCode(),
            TaskErrorCode.DUPLICATE_COMMAND_CALL.toString());
        assertEquals(containerCount + 1, a.getContainers().size());
        assertEquals(ContainerStatus.Open, a.getContainers().iterator().next().getStatus());
    }

}
