/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.selection.model.PickStatus;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test for PickODRCmd.
 *
 * @author pfunyak
 */
@Test(groups = { FAST, TASK, SELECTION })
public class PickedLUTCmdTest extends TaskCommandTestCase {

    private static final String SN = "PickedLUTSerial";
    private static final String OPER_ID = "PickedLUTOper";

    private PickedODRCmd cmd;
    
    /**
     * @return a bean for testing.
     */
    private PickedODRCmd getCmdBean() {
        return (PickedLUTCmd) getBean("cmdPrTaskLUTPicked");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickedLUTCmd#getAssignmentID()}.
     */
    @Test()
    public void testGetSetAssignmentID() {
        long assignmentID = 1;
        this.cmd.setAssignmentID(assignmentID);
        assertEquals(this.cmd.getAssignmentID(), assignmentID, "assignmentID Getter/Setter");
    }
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickedLUTCmd#getLocationID()}.
     */
    @Test()
    public void testGetSetLocationID() {
        int locationID = 1;
        this.cmd.setLocationID(locationID);
        assertEquals(this.cmd.getLocationID(), locationID,
                "locationID Getter/Setter");
    }
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickedLUTCmd#getQuantityPicked()}.
     */
    @Test()
    public void testGetSetQuantityPicked() {
        int quantityPicked = 5;
        this.cmd.setQuantityPicked(quantityPicked);
        assertEquals(this.cmd.getQuantityPicked(), quantityPicked,
                     "quantityPicked Getter/Setter");
    }
    
        
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickedLUTCmd#isEndOfPartialPickFlag()}.
     */
    @Test()
    public void testGetSetEndOfPartialPickFlag() {
        boolean endOfPartialPickFlag = true;
        this.cmd.setEndOfPartialPickFlag(endOfPartialPickFlag);
        assertEquals(this.cmd.isEndOfPartialPickFlag(), endOfPartialPickFlag, "endOfPartialPickFlag Getter/Setter");
    }
    
   
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickedLUTCmd#getContainerID()}.
     */
    @Test()
    public void testGetSetContainerID() {
        String containerId = "1";
        this.cmd.setContainerID(containerId);
        assertEquals(this.cmd.getContainerID(), containerId, "containerId Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickedLUTCmd#getPickID()}.
     */
    @Test()
    public void testGetSetPickID() {
        long pickID = 1;
        this.cmd.setPickID(pickID);
        assertEquals(this.cmd.getPickID(), pickID, "pickID Getter/Setter");
    }
    
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickedLUTCmd#getLotNumber()}.
     */
    @Test()
    public void testGetSetLotNumber() {
        String lotNumber = "12345";
        this.cmd.setLotNumber(lotNumber);
        assertEquals(this.cmd.getLotNumber(), lotNumber, "lotNumber Getter/Setter");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickedLUTCmd#getItemSerialNumber()}.
     */
    @Test()
    public void testGetSetItemSerialNumber() {
        String itemSerialNumber = "12345";
        this.cmd.setLotNumber(itemSerialNumber);
        assertEquals(this.cmd.getLotNumber(), itemSerialNumber, 
                "lotNumber Getter/Setter");
    }


    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickedODRCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        initialize(SN, OPER_ID, "2");
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        //===============================================================
        //Test Get Picks Lut
        Response response = executecommand(this.getCmdDateSec(),
            groupNumber.toString(), //Groupid 
            "-96", //AssignmentId
            "-946", //LocationId
            "1",  //Quantity Picked
            "1",  //End of Partial
            "",   //Container Id
            "-878", //Pick Id
            "",   //Lot Number
            "5.5", //Variable Weight 
            "123456ABC"); //SerialNumber
        
        //check a response was returned
        assertNotNull(response, "response Null");
        
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "Pick LUT Single Record");

        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "ErrorCode");
        
        //Verify Pick Detail record
        assertEquals(this.cmd.getPickManager().get(-878L).getPickDetails().size(), 1, "pickDetailCount");
        assertEquals(this.cmd.getPickManager().get(-878L).getStatus(), PickStatus.ShortsGoBack, "pickStatus");
        assertEquals(this.cmd.getPickManager().get(-878L).getPickDetails().get(0).getVariableWeight(), 5.5, 
                    "pickVariableWeight");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickedODRCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteValidations() throws Exception {

        initialize(SN, OPER_ID, "2");
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        //===============================================================
        //Test Invalid pick Id
        Response response = executecommand(this.getCmdDateSec(),
            groupNumber.toString(), //Groupid 
            "-96", //AssignmentId
            "-946", //LocationId
            "-1",  //Quantity Picked
            "1",  //End of Partial
            "",   //Container Id
            "-878", //Pick Id
            "",   //Lot Number
            "", //Variable Weight 
            ""); //SerialNumber
        
        //check a response was returned
        assertNotNull(response, "response Null");
        
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "Pick LUT Single Record");

        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), 
                Long.toString(TaskErrorCode.PICKED_ODR_NEGATIVE_QUANTITY_PICKED_VALUE.getErrorCode()),
                "ErrorCode");

        // Initialize Data for invalid pick status error
        initialize(SN, OPER_ID, "2");
        groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        //===============================================================
        //Test Invalid pick Id
        response = executecommand(this.getCmdDateSec(),
            groupNumber.toString(), //Groupid 
            "-96", //AssignmentId
            "-946", //LocationId
            "6",  //Quantity Picked
            "1",  //End of Partial
            "",   //Container Id
            "-878", //Pick Id
            "",   //Lot Number
            "", //Variable Weight 
            ""); //SerialNumber

        response = executecommand(this.getCmdDateSec(),
            groupNumber.toString(), //Groupid 
            "-96", //AssignmentId
            "-946", //LocationId
            "6",  //Quantity Picked
            "1",  //End of Partial
            "",   //Container Id
            "-878", //Pick Id
            "",   //Lot Number
            "", //Variable Weight 
            ""); //SerialNumber
        
        //check a response was returned
        assertNotNull(response, "response Null");
        
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "Pick LUT Single Record");

        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), TaskErrorCode.PICKED_ODR_INVALID_PICK_STATUS.getErrorCode(),
                     "ErrorCode");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickedODRCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteShort() throws Exception {

        initialize(SN, OPER_ID, "3");
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        //===============================================================
        //Test Get Picks Lut
        Response response = executecommand(this.getCmdDateSec(),
            groupNumber.toString(), //Groupid 
            "-146", //AssignmentId
            "-98", //LocationId
            "0",  //Quantity Picked
            "1",  //End of Partial
            "",   //Container Id
            "-1278", //Pick Id
            "",   //Lot Number
            "", //Variable Weight 
            ""); //SerialNumber
        
        //check a response was returned
        assertNotNull(response, "response Null");
        
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "Pick LUT Single Record");

        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "ErrorCode");
        
        //Verify Pick Detail record
        assertEquals(this.cmd.getPickManager().get(-1278L).getPickDetails().size(), 1, "pickDetailCount");
        assertEquals(this.cmd.getPickManager().get(-1278L).getStatus(), PickStatus.Shorted, "pickStatus");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.PickedODRCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecutePartial() throws Exception {

        initialize(SN, OPER_ID, "2");
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        //===============================================================
        //Test PickedLUT
        Response response = executecommand(this.getCmdDateSec(),
            groupNumber.toString(), //Groupid 
            "-96", //AssignmentId
            "-946", //LocationId
            "2",  //Quantity Picked
            "0",  //End of Partial
            "",   //Container Id
            "-878", //Pick Id
            "",   //Lot Number
            "", //Variable Weight 
            ""); //SerialNumber
        
        //check a response was returned
        assertNotNull(response, "response Null");
        
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "Pick LUT Single Record");

        ResponseRecord record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "ErrorCode");
        
        //Verify Pick Detail record
        assertEquals(this.cmd.getPickManager().get(-878L).getPickDetails().size(), 1, "pickDetailCount");
        assertEquals(this.cmd.getPickManager().get(-878L).getStatus(), PickStatus.Partial, "pickStatus");

        //===============================================================
        //Test Get Picks Lut
        response = executecommand(this.getCmdDateSec(),
            groupNumber.toString(), //Groupid 
            "-96", //AssignmentId
            "-946", //LocationId
            "4",  //Quantity Picked
            "1",  //End of Partial
            "",   //Container Id
            "-878", //Pick Id
            "",   //Lot Number
            "", //Variable Weight 
            ""); //SerialNumber
        
        //check a response was returned
        assertNotNull(response, "response Null");
        
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "Pick LUT Single Record");

        record = records.get(0);
        assertEquals(record.get("errorCode"), "0", "ErrorCode");
        
        //Verify Pick Detail record
        assertEquals(this.cmd.getPickManager().get(-878L).getPickDetails().size(), 2, "pickDetailCount");
        assertEquals(this.cmd.getPickManager().get(-878L).getStatus(), PickStatus.Picked, "pickStatus");
    }
    
    /**
     * Initialize the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @param sn - Serial Number
     * @param operId - Operator
     * @param regionNumber - the number of the region
     * @throws Exception - Exception
     */
    private void initialize(String sn, String operId, String regionNumber) throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        this.cmd.getAssignmentManager().initializeState();

        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTPickingRegion", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, regionNumber, "3"});
        executeLutCmd("cmdPrTaskLUTGetAssignment",
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1"}); 

        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        executeLutCmd("cmdPrTaskLUTGetPicks",
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            groupNumber.toString(), "0", "1", "0"}); 
    }
    
    /**
     * Executes a picked ODR command.
     * 
     * @param cmdDate - Date
     * @param groupNumber - Group Number
     * @param assignmentId = Assignment
     * @param locationId - Location
     * @param quantityPicked - Quantity
     * @param endOfPartialPick - End of pick
     * @param containerId - Container ID
     * @param pickId - Pick ID
     * @param lotNumber - Lot Number
     * @param variableWeight - Variable Weight
     * @param serialNumber - Serial Number
     * @return - response record
     * @throws Exception - any
     */
    private Response executecommand(Date cmdDate,
                                    String groupNumber,
                                    String assignmentId,
                                    String locationId,
                                    String quantityPicked,
                                    String endOfPartialPick,
                                    String containerId,
                                    String pickId,
                                    String lotNumber,
                                    String variableWeight,
                                    String serialNumber) throws Exception {
        
        this.cmd = getCmdBean();
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), SN, OPER_ID, 
                groupNumber,
                assignmentId,
                locationId,
                quantityPicked,
                endOfPartialPick,
                containerId,
                pickId,
                lotNumber,
                variableWeight,
                serialNumber}, 
            this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
