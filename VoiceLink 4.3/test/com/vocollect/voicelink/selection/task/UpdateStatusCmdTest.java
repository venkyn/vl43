/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.selection.model.Pick;
import com.vocollect.voicelink.selection.model.PickStatus;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for UpdateStatusCmd.
 *
 * @author sfahnestock
 */
@Test(groups = { FAST, TASK, SELECTION })
public class UpdateStatusCmdTest extends TaskCommandTestCase {

    private UpdateStatusCmd cmd;
    private String operId = "UpdateStatusOper";
    private String sn = "UpdateStatusSerial";
    
    /**
     * @return a bean for testing.
     */
    private UpdateStatusCmd getCmdBean() {
        return (UpdateStatusCmd) getBean("cmdPrTaskLUTUpdateStatus");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#getGroupNumber()}.
     */
    @Test()
    public void testGetSetGroupNumber() {
        this.cmd.setGroupNumber(1L);
        assertEquals(this.cmd.getGroupNumber().toString(), "1", "GroupNumber Getter/Setter");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#getLocationID()}.
     */
    @Test()
    public void testGetSetLocationID() {
        this.cmd.setLocationID("12345");
        assertEquals(this.cmd.getLocationID(), "12345", "LocationID Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#getSlotAisle()}.
     */
    @Test()
    public void testGetSetSlotAisle() {
        this.cmd.setSlotAisle("1");
        assertEquals(this.cmd.getSlotAisle(), "1", "SlotAisle Getter/Setter");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#getSlotAisle()}.
     */
    @Test()
    public void testGetSetSetStatusTo() {
        this.cmd.setSetStatusTo("S");
        assertEquals(this.cmd.getSetStatusTo(), "S", "SetStatusTo Getter/Setter");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteSkipSlot() throws Exception {

        initialize();
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        //Test Update Status Skip Slot
        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString(),
                           "-946", "0", "S");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        
        //Verify picks status
        assertEquals(countPicksWithStatus(PickStatus.Skipped, groupNumber), 1, "picksUpdate");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteSkipAisle() throws Exception {

        initialize();
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        //Test Update Status Skip Aisle
        Response response = executeCommand(this.getCmdDateSec(), 
            groupNumber.toString(), "-946", "1", "S");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        
        //Verify picks status
        assertEquals(countPicksWithStatus(PickStatus.Skipped, groupNumber), 5, "picksUpdate");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteNotPicked() throws Exception {

        initialize();
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");
        
        //Test Update Status Not Picked
        //Skip Aisle - assumes this work (in previous test)
        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), "-946", "1", "S");
        //Un skip items setting back to not picked
        response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), "", "2", "N");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "ErrorCodeResponse");
        
        //Verify picks status
        assertEquals(countPicksWithStatus(PickStatus.NotPicked, groupNumber), 
                     countPicksWithStatus(null, groupNumber), "picksUpdate");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.UpdateStatusCmd#execute()}.
     * @throws Exception any
     */
    @Test()
    public void testExecuteValidations() throws Exception {

        initialize();
        Response groupResponse = getResponseForCommand("cmdPrTaskLUTGetAssignment");
        Long groupNumber = (Long) groupResponse.getRecords().get(0).get("groupID");

        //Test Update Status Invalid Status
        //Skip Aisle - assumes this work (in previous test)
        Response response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), "-946", "1", "X");
        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), 
                Long.toString(TaskErrorCode.INVALID_STATUS.getErrorCode()), "InvalidStatus");

        //Test Update Status Invalid indicator
        //Skip Aisle - assumes this work (in previous test)
         response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), "-946", "3", "S");
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), 
                Long.toString(TaskErrorCode.INVALID_INDICATOR.getErrorCode()), "InvalidStatus");

        //Test Update Status cannot skip entire assignment
        //Skip Aisle - assumes this work (in previous test)
         response = executeCommand(this.getCmdDateSec(), groupNumber.toString(), "-946", "2", "S");
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize");
        record = records.get(0);
        assertEquals(record.getErrorCode(), 
                Long.toString(TaskErrorCode.CANNOT_SKIP_ENTIRE_ASSIGNMENT.getErrorCode()), 
                "InvalidStatus");
    }
   
    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;

        //Clear Database
        this.cmd.getAssignmentManager().initializeState();

        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "2", "3" });
        executeLutCmd("cmdPrTaskLUTGetAssignment", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "1" });
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param groupNumber - group number
     * @param locationId - Location skipping
     * @param skipIndicator - skip slot, aisle, or all
     * @param status - status to set to
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String groupNumber,
                                    String locationId,
                                    String skipIndicator,
                                    String status) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(new String[] {makeStringFromDate(cmdDate), sn, 
                operId, groupNumber, locationId, skipIndicator, status}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
    
    /**
     * count the number of picks in a group with a specified status.
     * 
     * @param status - status to count
     * @param groupNumber = the number of the group
     * @return - count of picks with specified status
     * @throws DataAccessException - database exception
     */
    private int countPicksWithStatus(PickStatus status, Long groupNumber) 
    throws DataAccessException {
        int count = 0;
        
        for (Pick p : this.cmd.getPickManager().listGroupsPicks(groupNumber)) {
            if (status == null) {
                count++;
            } else if (p.getStatus().equals(status)) {
                count++;
            }
        }
        return count;
    }
}
