/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentType;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.SELECTION;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for GetConfigurationCmd.
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, TASK, SELECTION })
public class RequestWorkCmdTest extends TaskCommandTestCase {

    private RequestWorkCmd cmd;
    private String operId = "RequestWorkOper";
    private String sn = "RequestWorkSerial";
    
    /**
     * @return a bean for testing.
     */
    private RequestWorkCmd getCmdBean() {
        return (RequestWorkCmd) getBean("cmdPrTaskLUTRequestWork");
    }
    
    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean(); 
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.RequestWorkCmd#getworkIdValue()}.
    */
    @Test()
    public void testGetWorkIdValue() {
        this.cmd.setWorkIdValue("1");
        assertEquals(this.cmd.getWorkIdValue().toString(), "1", "GetWorkIdValue");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.RequestWorkCmd#getIsPartialWorkId()}.
    */
    @Test()
    public void testGetIsPartialWorkId() {
        this.cmd.setIsPartialWorkId(true);
        assertEquals(this.cmd.getIsPartialWorkId(), true, "GetIsPartialWorkId");
    }
    
    /**
     * Test method for {@link com.vocollect.voicelink.core.task.RequestWorkCmd#getAssignmentType()}.
    */
    @Test()
    public void testGetAssignmentType() {
        this.cmd.setAssignmentType(1);
        assertEquals(this.cmd.getAssignmentType(), 1, "GetAssignmentType");
    }

    /**
     * Test method for {@link com.vocollect.voicelink.core.task.RegionPermissionsForWorkTypeCmd#execute}.
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {
        
        initialize();
        //Test Request Work with chase assignment
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "13", "3" });
        
        //Get Assignment 
        Assignment a = this.cmd.getAssignmentManager().get(-591L);
        assertNotNull(a, "GotAssignment");
        a.setType(AssignmentType.Chase);
        this.cmd.getAssignmentManager().save(a);
        
        Response response = executeCommand(this.getCmdDateSec(), "7295", "1", "2");

        //Verify Successful result
        List<ResponseRecord> records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize chase");
        ResponseRecord record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "errorCode chase"); 
        assertEquals(record.get("workIdValue"), "107295", "workIdValue chase");
        
        initialize();
        //Test Request Work with full work id
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "15", "3" });
        
        response = executeCommand(this.getCmdDateSec(), "107370", "0", "1");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize fullWorkId");
        record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "errorCode fullWorkId"); 
        assertEquals(record.get("workIdValue"), "107370", "workIdValue fullWorkId");
        
        //Test Request Work with partial work id, multiple records
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1", "3" });
        
        response = executeCommand(this.getCmdDateSec(), "7370", "1", "1");
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 3, "recordSize partialWorkId");
        record = records.get(0);
        assertEquals(record.getErrorCode(), "4", "errorCode partialWorkId"); 
        assertEquals(record.get("workIdValue"), "127370", "workIdValue partialWorkId");
        record = records.get(1);
        assertEquals(record.getErrorCode(), "4", "errorCode partialWorkId"); 
        assertEquals(record.get("workIdValue"), "117370", "workIdValue partialWorkId");
        record = records.get(2);
        assertEquals(record.getErrorCode(), "4", "errorCode partialWorkId"); 
        assertEquals(record.get("workIdValue"), "107370", "workIdValue partialWorkId");
        
        //Test Request Work with non-existent work id
        response = executeCommand(this.getCmdDateSec(), "9999", "0", "1");
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize invalidWorkId");
        record = records.get(0);
        assertEquals(record.getErrorCode(), 
                Long.toString(TaskErrorCode.WORK_REQUEST_FAILED.getErrorCode()), 
                "errorCode invalidWorkId"); 
        
        //Test Request Work with already reserved assignments, fail
        response = executeCommand(this.getCmdDateSec(), "7317", "1", "1");
        //Call a second time to get error
        response = executeCommand(this.getCmdDateSec(), "7317", "1", "1");
        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize reservedFail");
        record = records.get(0);
        assertEquals(record.getErrorCode(),
                Long.toString(TaskErrorCode.WORK_REQUEST_FAILED.getErrorCode()), 
                "errorCode reservedFail"); 

        //Test Request Work with already reserved assignments, pass
        executeLutCmd("cmdPrTaskLUTPickingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "15", "3" });
        
        response = executeCommand(this.getCmdDateSec(), "107364", "1", "1");

        //Verify Successful result
        records = response.getRecords();
        assertEquals(records.size(), 1, "recordSize reservedPass");
        record = records.get(0);
        assertEquals(record.getErrorCode(), "0", "errorCode reservedPass"); 
        assertEquals(record.get("workIdValue"), "107364", "workIdValue reservedPass");
        
    }
    
    

    /**
     * Initializes the database by calling task commands that need to be
     * called prior to task command in unit test.
     * 
     * @throws Exception - Exception
     */
    private void initialize() throws Exception {
        final Locale locale = Locale.US;
        
        //Clear Database
        classSetUp();
        classSetupCore();
        classSetupSelectionRegions();
        classSetupSelectionData();

        executeLutCmd("cmdPrTaskLUTCoreConfiguration", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, 
            locale.toString(), "Default", this.goodCombinedTaskVersion()});
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId});
        executeLutCmd("cmdPrTaskLUTCoreSignOn", 
            new String[] {makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234"});
        executeLutCmd("cmdPrTaskLUTRegionPermissionsForWorkType", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "3" });
        
    }

    /**
     * Execute a command.
     * 
     * @param cmdDate - date for command
     * @param workIdValue - assignment work id requested by operator
     * @param isPartialWorkId - is the value spoken partial work id
     * @param assignmentType - type of assignment to get
     * @return - return a response from command
     * @throws Exception - all exceptions
     */
    private Response executeCommand(Date cmdDate,
                                    String workIdValue, 
                                    String isPartialWorkId, 
                                    String assignmentType) throws Exception {
        this.cmd = getCmdBean();
        //Run the call the first time
        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {makeStringFromDate(cmdDate), sn, operId, 
                workIdValue, isPartialWorkId, assignmentType}, this.cmd);
        return getTaskCommandService().executeCommand(this.cmd);
    }
}
