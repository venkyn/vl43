/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test for CycleCountingRegionsCmd.
 * 
 * @author khazra
 */
@Test(groups = { FAST, TASK, CORE })
public class CycleCountingRegionsCmdTest extends TaskCommandTestCase {

    private CycleCountingRegionsCmd cmd;

    /**
     * @return a bean for testing.
     */
    private CycleCountingRegionsCmd getCmdBean() {
        return (CycleCountingRegionsCmd) getBean("cmdPrTaskLUTCycleCountingRegions");
    }

    @Override
    @BeforeClass
    protected void classSetUp() throws Exception {
        super.classSetUp();
    }

    /**
     * Each test setup.
     * 
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void testOperatorNotAuthorized() throws Exception {
        super.classSetUp();
        final String operId = "CycleCountCmdOpr";
        String sn = "CycleCountCmdSerial";

        Response response;
        signOn(sn, operId);
        // Test Get No break types

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        // Verify the result is empty
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("regionNumber"), "", "Region Number not empty");
        assertEquals(record.get("regionName"), "", "Region Name not empty");

        signOff(sn, operId);
    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void testOperatorAuthorized() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_CycleCountingRegions.xml");
        classSetupInsert("VoiceLinkDataUnitTest_CycleCountingOperators.xml");

        final String operId = "operRequestCCRegionCmd";
        String sn = "CycleCountCmdSerial1";

        Response response;
        signOn(sn, operId);
        // Test Get No break types

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        // Verify the result is empty
        List<ResponseRecord> records = response.getRecords();
        assert (records.size() > 0);

    }

    /**
     * @param sn
     *            Serial Number
     * @param operId
     *            Operator ID
     * @throws Exception
     */
    private void signOn(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd(
                "cmdPrTaskLUTCoreConfiguration",
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, locale.toString(), "Default",
                        this.goodCombinedTaskVersion() });

        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId });

        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
    }

    /**
     * @param sn
     *            Serial Number
     * @param operId
     *            Operator ID
     * @throws Exception
     */
    private void signOff(String sn, String operId) throws Exception {

        executeLutCmd("cmdPrTaskLUTCoreSignOff", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId });

    }

}
