/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingLabor;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManager;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test for BreakTypeCmd.
 * 
 * @author khazra
 */
@Test(groups = { FAST, TASK, CORE })
public class CycleCountingAssignmentCmdTest extends TaskCommandTestCase {

    private CycleCountingAssignmentCmd cmd;

    private CycleCountingLaborManager cycleCountingLaborManager;

    private OperatorManager operatorManager;

    /**
     * @return the cycleCountingLaborManager
     */
    @Test(enabled = false)
    public CycleCountingLaborManager getCycleCountingLaborManager() {
        return cycleCountingLaborManager;
    }

    /**
     * @param cycleCountingLaborManager the cycleCountingLaborManager to set
     */
    @Test(enabled = false)
    public void setCycleCountingLaborManager(CycleCountingLaborManager cycleCountingLaborManager) {
        this.cycleCountingLaborManager = cycleCountingLaborManager;
    }

    /**
     * @return the operatorManager
     */
    @Test(enabled = false)
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * @param operatorManager the operatorManager to set
     */
    @Test(enabled = false)
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * @return a bean for testing.
     */
    private CycleCountingAssignmentCmd getCmdBean() {
        return (CycleCountingAssignmentCmd) getBean("cmdPrTaskLUTCycleCountingAssignment");
    }

    /**
     * 
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_CycleCountingAssignments.xml");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.task.BreakTypeCmd#testExecute()}.
     * 
     * @throws Exception any
     */
    @Test()
    public void testExecute() throws Exception {

        // ------------------------------------------------------------------
        // Verify record when location supplied
        // ------------------------------------------------------------------
        String operId = "CycleCountAssnCmdOpr";
        String sn = "CycleCountAssnCmdOpr";
        String indicator = "0";
        String location = "1";
        String locationCheckDigit = "1";

        Response response;
        initialize(sn, operId);

        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                indicator, location, locationCheckDigit }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("locationID").toString(), "1");
        assertEquals(record.get("preAisle").toString(), "Bldg 1");
        assertEquals(record.get("aisle").toString(), "Aisle 1");
        assertEquals(record.get("postAisle"), null);
        assertEquals(record.get("slot").toString(), "Slot 1");
        assertEquals(record.get("locationCheckDigit").toString(), "1");
        assertEquals(record.get("pvid").toString(), "1");
        assertEquals(record.get("itemNumber").toString(), "1");
        assertEquals(record.get("itemDescription").toString(), "Item 1");
        assertEquals(record.get("itemUPC").toString(), "1");
        assertEquals(record.get("expectedQuantity").toString(), "2");
        assertEquals(record.get("uom").toString(), "case");
        assertEquals(record.get("countMode").toString(), "K");
        assertEquals(record.get("tie").toString(), "");
        assertEquals(record.get("high").toString(), "");
        assertEquals(record.get("image").toString(), "");

        // ------------------------------------------------------------------
        // Verify record when location is not supplied
        // ------------------------------------------------------------------
        operId = "CycleCountAssnCmdOpr";
        sn = "CycleCountAssnCmdOpr";
        indicator = "0";
        location = "";
        locationCheckDigit = "";

        initialize(sn, operId);

        executeLutCmd("cmdPrTaskLUTCycleCountingLocation", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId });

        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                indicator, location, locationCheckDigit }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        records = response.getRecords();
        record = records.get(0);
        assertEquals(record.get("locationID").toString(), "1");
        assertEquals(record.get("preAisle").toString(), "Bldg 1");
        assertEquals(record.get("aisle").toString(), "Aisle 1");
        assertEquals(record.get("postAisle"), null);
        assertEquals(record.get("slot").toString(), "Slot 1");
        assertEquals(record.get("locationCheckDigit").toString(), "1");
        assertEquals(record.get("pvid").toString(), "1");
        assertEquals(record.get("itemNumber").toString(), "1");
        assertEquals(record.get("itemDescription").toString(), "Item 1");
        assertEquals(record.get("itemUPC").toString(), "1");
        assertEquals(record.get("expectedQuantity").toString(), "2");
        assertEquals(record.get("uom").toString(), "case");
        assertEquals(record.get("countMode").toString(), "K");
        assertEquals(record.get("tie").toString(), "");
        assertEquals(record.get("high").toString(), "");
        assertEquals(record.get("image").toString(), "");

        // ------------------------------------------------------------------
        // Invalid location - no location "0" in the system.
        // ------------------------------------------------------------------
        operId = "CycleCountAssnCmdOpr";
        sn = "CycleCountAssnCmdOpr";
        indicator = "0";
        location = "0";
        locationCheckDigit = "0";

        initialize(sn, operId);

        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                indicator, location, locationCheckDigit }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        records = response.getRecords();
        assertEquals(records.size(), 1, "Number of records");

        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()),
            TaskErrorCode.CC_INVALID_LOCATION.getErrorCode());

        // ------------------------------------------------------------------
        // No location item mapping
        // Should create an assignment with a single detail but no item info
        // ------------------------------------------------------------------
        operId = "CycleCountAssnCmdOpr";
        sn = "CycleCountAssnCmdOpr";
        indicator = "0";
        location = "5";
        locationCheckDigit = "5";

        initialize(sn, operId);

        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                indicator, location, locationCheckDigit }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        records = response.getRecords();
        assertEquals(records.size(), 1, "Number of records");

        record = records.get(0);
        assertEquals(record.get("locationID").toString(), "5");
        assertEquals(record.get("preAisle").toString(), "Bldg 5");
        assertEquals(record.get("aisle").toString(), "Aisle 5");
        assertEquals(record.get("postAisle"), null);
        assertEquals(record.get("slot").toString(), "Slot 5");
        assertEquals(record.get("locationCheckDigit").toString(), "5");
        assertNull(record.get("pvid"));
        assertNull(record.get("itemNumber"));
        assertNull(record.get("itemDescription"));
        assertNull(record.get("itemUPC"));
        assertEquals(record.get("expectedQuantity").toString(), "0");
        assertNull(record.get("uom"));
        assertEquals(record.get("countMode").toString(), "K");
        assertEquals(record.get("tie").toString(), "");
        assertEquals(record.get("high").toString(), "");
        assertEquals(record.get("image").toString(), "");

        // ------------------------------------------------------------------
        // Operator labor record created in operator directed
        // ------------------------------------------------------------------
        operId = "CycleCountAssnCmdOpr";
        sn = "CycleCountAssnCmdOpr";
        indicator = "0";
        location = "3";
        locationCheckDigit = "3";

        initialize(sn, operId);

        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                indicator, location, locationCheckDigit }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        CycleCountingLabor cycleCountingLabor = this.cycleCountingLaborManager
            .findOpenRecordByAssignmentId(3);

        assertNull(cycleCountingLabor);
    }

    /**
     * Initializes the database by calling task commands that need to be called
     * prior to task command in unit test.
     * 
     * @param sn - Serial Number
     * @param operId - Operator
     * @throws Exception - Exception
     */
    private void initialize(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;

        String regionNumber = "1";
        String password = "1234";
        String allRegions = "0";

        executeLutCmd(
            "cmdPrTaskLUTCoreConfiguration",
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                locale.toString(), "Default", this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, password });
        executeLutCmd("cmdPrTaskLUTCycleCountingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, regionNumber,
            allRegions });

    }

}
