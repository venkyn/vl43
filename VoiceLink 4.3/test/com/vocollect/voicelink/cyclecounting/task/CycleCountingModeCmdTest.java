/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test for CycleCountingModeCmd.
 * 
 * @author pkolonay
 */
@Test(groups = { FAST, TASK, CORE })
public class CycleCountingModeCmdTest extends TaskCommandTestCase {

    private CycleCountingModeCmd cmd;

    /**
     * @return a bean for testing.
     */
    private CycleCountingModeCmd getCmdBean() {
        return (CycleCountingModeCmd) getBean("cmdPrTaskLUTCycleCountingMode");
    }

    /**
     * 
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_CycleCountingRegions.xml");
        classSetupInsert("VoiceLinkDataUnitTest_CycleCountingOperators.xml");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }

    /**
     * 
     * @throws Exception
     *             any
     */
    @Test()
    public void testCountModeSystemDirected() throws Exception {

        final String operId = "ccOperator1";
        String regionNumber = "101";
        String regionNumber2 = "102";
        String allRegion = "0";
        String countMode = "H";
        String sn = "ccDevice" + "_" + countMode;

        Response response;
        initialize(sn, operId);
        // Test Get No break types

        // Check that correct mode is returned for a single region
        executeLutCmd("cmdPrTaskLUTCycleCountingRegion", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                regionNumber, allRegion });

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        ResponseRecord record = response.getRecords().get(0);

        assertEquals(record.get("modeIndicator").toString(), countMode,
                "modeIndicator");

        // Now test mode when operator selects more than one region 
        executeLutCmd("cmdPrTaskLUTCycleCountingRegion", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                regionNumber2, allRegion });

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        record = response.getRecords().get(0);

        assertEquals(record.get("modeIndicator").toString(), countMode,
                "modeIndicator");
    }

    /**
     * 
     * @throws Exception
     *             any
     */
    @Test()
    public void testCountModeOperDirected() throws Exception {

        final String operId = "ccOperator2";
        String regionNumber = "201";
        String regionNumber2 = "202";
        String allRegion = "0";
        String countMode = "P";
        String sn = "ccDevice" + "_" + countMode;

        Response response;
        initialize(sn, operId);
        // Test Get No break types

        // Check that correct mode is returned for a single region
        executeLutCmd("cmdPrTaskLUTCycleCountingRegion", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                regionNumber, allRegion });

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        ResponseRecord record = response.getRecords().get(0);

        assertEquals(record.get("modeIndicator").toString(), countMode,
                "modeIndicator");

        // Now test mode when operator selects more than one region 
        executeLutCmd("cmdPrTaskLUTCycleCountingRegion", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                regionNumber2, allRegion });

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        record = response.getRecords().get(0);

        assertEquals(record.get("modeIndicator").toString(), countMode,
                "modeIndicator");
    }

    /**
     * Initializes the database by calling task commands that need to be called
     * prior to task command in unit test.
     * 
     * @param sn
     *            - Serial Number
     * @param operId
     *            - Operator
     * @throws Exception
     *             - Exception
     */
    private void initialize(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd(
                "cmdPrTaskLUTCoreConfiguration",
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, locale.toString(), "Default",
                        this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
        executeLutCmd("cmdPrTaskLUTCycleCountingRegions", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId });
    }

}
