/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test for BreakTypeCmd.
 * 
 * @author khazra
 */
@Test(groups = { FAST, TASK, CORE })
public class CycleCountingReserveChainCmdTest extends TaskCommandTestCase {

    private CycleCountingReserveChainCmd cmd;

    /**
     * @return a bean for testing.
     */
    private CycleCountingReserveChainCmd getCmdBean() {
        return (CycleCountingReserveChainCmd) getBean("cmdPrTaskLUTCycleCountingReserveChain");
    }

    /**
     * 
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.task.BreakTypeCmd#testExecute()}.
     * 
     * @throws Exception
     *             any
     */
    @Test()
    public void testExecute() throws Exception {

        final String operId = "CycleCountCmdOpr";
        String sn = "CycleCountCmdReserveChain";
        String locationId = "1";
        String itemNumber = "1";
        
        Response response;
        initialize(sn, operId);

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, locationId, itemNumber }, this.cmd);
        
        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);
        
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(record.get("locationID").toString(), "");
        assertEquals(record.get("palletID").toString(), "");
        assertEquals(record.get("item").toString(), "");
        assertEquals(record.get("quantity"), "");

    }

    /**
     * Initializes the database by calling task commands that need to be called
     * prior to task command in unit test.
     * 
     * @param sn
     *            - Serial Number
     * @param operId
     *            - Operator
     * @throws Exception
     *             - Exception
     */
    private void initialize(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd(
                "cmdPrTaskLUTCoreConfiguration",
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, locale.toString(), "Default",
                        this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
    }

}
