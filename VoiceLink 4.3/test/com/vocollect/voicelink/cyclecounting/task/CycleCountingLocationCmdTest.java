/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManager;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author pkolonay
 * 
 */
public class CycleCountingLocationCmdTest extends TaskCommandTestCase {

    private CycleCountingLocationCmd cmd;

    private CycleCountingAssignmentManager cycleCountingAssignmentManager;

    private CycleCountingLaborManager cycleCountingLaborManager;

    /**
     * @return a bean for testing.
     */
    private CycleCountingLocationCmd getCmdBean() {
        return (CycleCountingLocationCmd) getBean("cmdPrTaskLUTCycleCountingLocation");
    }

    /**
     * @return the cycleCountingAssignmentManager
     */
    public CycleCountingAssignmentManager getCycleCountingAssignmentManager() {
        return cycleCountingAssignmentManager;
    }

    /**
     * @param cycleCountingAssignmentManager
     *            the cycleCountingAssignmentManager to set
     */
    public void setCycleCountingAssignmentManager(
            CycleCountingAssignmentManager cycleCountingAssignmentManager) {
        this.cycleCountingAssignmentManager = cycleCountingAssignmentManager;
    }

    /**
     * @return the cycleCountingLaborManager
     */
    public CycleCountingLaborManager getCycleCountingLaborManager() {
        return cycleCountingLaborManager;
    }

    /**
     * @param cycleCountingLaborManager
     *            the cycleCountingLaborManager to set
     */
    public void setCycleCountingLaborManager(
            CycleCountingLaborManager cycleCountingLaborManager) {
        this.cycleCountingLaborManager = cycleCountingLaborManager;
    }

    /**
     * 
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_CycleCountingRegions.xml");
        classSetupInsert("VoiceLinkDataUnitTest_CycleCountingOperators.xml");
        classSetupInsert("VoiceLinkDataUnitTest_CycleCounting.xml");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }

    /**
     * 
     * @throws Exception
     *             any
     */
    @Test()
    public void testCycleCountLocation() throws Exception {

        final String operId = "ccOperator1";
        String sn = "ccDevice1";
        String allRegion = "0";
        String regionNumber = "201";
        long ccAid = 1;

        Response response;
        initialize(sn, operId);

        executeLutCmd("cmdPrTaskLUTCycleCountingRegion", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                regionNumber, allRegion });

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        ResponseRecord record = response.getRecords().get(0);

        CycleCountingAssignment ccA = this.getCycleCountingAssignmentManager()
                .get(ccAid);

        assertEquals(ccA.getStatus(), CycleCountStatus.InProgress, "status");

        assertEquals(ccA.getOperator().getOperatorIdentifier(), operId, "oper");

        assertEquals(record.get("pvid"), null, "pvid");

    }

    /**
     * 
     * @throws Exception
     *             any
     */
    @Test()
    public void testCycleCountLocationNoOperatorAssigned() throws Exception {

        final String operId = "ccOperator2";
        String sn = "ccDevice2";
        String allRegion = "0";
        String regionNumber = "201";
        long ccAid = 2;

        Response response;
        initialize(sn, operId);

        executeLutCmd("cmdPrTaskLUTCycleCountingRegion", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                regionNumber, allRegion });

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        ResponseRecord record = response.getRecords().get(0);

        assertEquals(record.get("pvid"), null, "pvid");

        CycleCountingAssignment ccA = this.getCycleCountingAssignmentManager()
                .get(ccAid);

        assertEquals(ccA.getStatus(), CycleCountStatus.InProgress, "status");

        assertEquals(ccA.getOperator().getOperatorIdentifier(), operId, "oper");

    }

    /**
     * 
     * @throws Exception
     *             any
     */
    @Test()
    public void testCycleCountLocationNoAssignments() throws Exception {

        final String operId = "ccOperator2";
        String sn = "ccDevice2";
        String allRegion = "0";
        String regionNumber = "202";
        String pvid = "";

        Response response;
        initialize(sn, operId);

        executeLutCmd("cmdPrTaskLUTCycleCountingRegion", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                regionNumber, allRegion });

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        ResponseRecord record = response.getRecords().get(0);
        assertEquals(record.getErrorCode(), "2","did not get expected error code");

        
    }

    /**
     * Initializes the database by calling task commands that need to be called
     * prior to task command in unit test.
     * 
     * @param sn
     *            - Serial Number
     * @param operId
     *            - Operator
     * @throws Exception
     *             - Exception
     */
    private void initialize(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd(
                "cmdPrTaskLUTCoreConfiguration",
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, locale.toString(), "Default",
                        this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
        executeLutCmd("cmdPrTaskLUTCycleCountingRegions", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId });
    }

}
