/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.voicelink.cyclecounting.model.CycleCountDetailStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingLabor;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManager;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;

import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author pkolonay
 * 
 */
public class CycleCountingUpdateCmdTest extends TaskCommandTestCase {

    private CycleCountingItemCountUpdateCmd cmd;

    private CycleCountingAssignmentManager cycleCountingAssignmentManager;

    private CycleCountingLaborManager cycleCountingLaborManager;

    /**
     * @return the cycleCountingLaborManager
     */
    @Test(enabled = false)
    public CycleCountingLaborManager getCycleCountingLaborManager() {
        return cycleCountingLaborManager;
    }

    /**
     * @param cycleCountingLaborManager the cycleCountingLaborManager to set
     */
    @Test(enabled = false)
    public void setCycleCountingLaborManager(CycleCountingLaborManager cycleCountingLaborManager) {
        this.cycleCountingLaborManager = cycleCountingLaborManager;
    }

    /**
     * @return a bean for testing.
     */
    private CycleCountingItemCountUpdateCmd getCmdBean() {
        return (CycleCountingItemCountUpdateCmd) getBean("cmdPrTaskODRItemCountUpdate");
    }

    /**
     * @return the cycleCountingAssignmentManager
     */
    public CycleCountingAssignmentManager getCycleCountingAssignmentManager() {
        return cycleCountingAssignmentManager;
    }

    /**
     * @param cycleCountingAssignmentManager the cycleCountingAssignmentManager
     *            to set
     */
    public void setCycleCountingAssignmentManager(CycleCountingAssignmentManager cycleCountingAssignmentManager) {
        this.cycleCountingAssignmentManager = cycleCountingAssignmentManager;
    }

    /**
     * 
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_CycleCountingRegions.xml");
        classSetupInsert("VoiceLinkDataUnitTest_CycleCountingOperators.xml");
        classSetupInsert("VoiceLinkDataUnitTest_CycleCounting.xml");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
    }

    /**
     * 
     * @throws Exception any
     */
    @Test()
    public void testCycleCountItemUpdateCount() throws Exception {

        final String operId = "ccOperator1";
        String sn = "ccDevice1UpdateCmd";
        String allRegion = "0";
        String regionNumber = "201";
        String actualQuantity = "10";
        long ccAid = 1;

        Response response;
        initialize(sn, operId);

        executeLutCmd("cmdPrTaskLUTCycleCountingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, regionNumber,
            allRegion });

        response = executeLutCmd(
            "cmdPrTaskLUTCycleCountingLocation",
            new String[] { makeStringFromDate(this.getCmdDateSec()), sn, operId });

        response = executeLutCmd("cmdPrTaskLUTCycleCountingAssignment",
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId, "", "",
                "" });

        ResponseRecord record = response.getRecords().get(0);

        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                record.get("locationID").toString(),
                record.get("itemNumber").toString(), actualQuantity,
                record.get("uom").toString(), "", "" }, this.cmd);

        Response odrResponse = getTaskCommandService().executeCommand(this.cmd);
        assertEquals(odrResponse, null, "ODR returned response.");

        CycleCountingAssignment ccA = this.getCycleCountingAssignmentManager()
            .get(ccAid);

        assertEquals(ccA.getStatus(), CycleCountStatus.InProgress,
            "cycleCountStatus");
        CycleCountingDetail ccD = ccA.getCycleCountingDetails().get(0);

        assertEquals(Integer.toString(ccD.getActualQuantity()), actualQuantity,
            "actualQuantity");
        assertEquals(ccD.getStatus(), CycleCountDetailStatus.Counted,
            "detailStatus");

        record = response.getRecords().get(1);

        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                record.get("locationID").toString(),
                record.get("itemNumber").toString(), actualQuantity,
                record.get("uom").toString(), "", "" }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        assertEquals(odrResponse, null, "ODR returned response.");

        ccA = this.getCycleCountingAssignmentManager().get(ccAid);

        assertEquals(ccA.getStatus(), CycleCountStatus.Complete,
            "cycleCountStatus");
        ccD = ccA.getCycleCountingDetails().get(0);

        CycleCountingLabor cycleCountingLabor = this.cycleCountingLaborManager
            .findOpenRecordByAssignmentId(ccAid);

        assertNull(cycleCountingLabor);

        assertEquals(Integer.toString(ccD.getActualQuantity()), actualQuantity,
            "actualQuantity");
        assertEquals(ccD.getStatus(), CycleCountDetailStatus.Counted,
            "detailStatus");

    }

    /**
     * Test that the item update works correctly for the case where an on the
     * fly assignment has to be created.
     * @throws Exception any
     */
    @Test()
    public void testCycleCountItemUpdateCountNewAssignment() throws Exception {

        final String operId = "ccOperator1";
        String sn = "ccDevice1UpdateCmd";
        String allRegion = "0";
        String regionNumber = "203";
        String actualQuantity = "0";
        String spokenVerification = "13097";
        String checkDigit = "10";
        long oid = 201;

        Response response;
        initialize(sn, operId);

        executeLutCmd("cmdPrTaskLUTCycleCountingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, regionNumber,
            allRegion });

        response = executeLutCmd("cmdPrTaskLUTCycleCountingAssignment",
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId, "0",
                spokenVerification, checkDigit });

        ResponseRecord record = response.getRecords().get(0);

        getCommandDispatcher()
            .mapArgumentsToCommand(
                new String[] {
                    makeStringFromDate(this.getCmdDateSec()), sn, operId,
                    record.get("locationID").toString(),
                    record.get("itemNumber").toString(), actualQuantity, "",
                    "", "" }, this.cmd);

        Response odrResponse = getTaskCommandService().executeCommand(this.cmd);

        assertEquals(odrResponse, null, "ODR returned response.");

        CycleCountingAssignment ccA = this.getCycleCountingAssignmentManager()
            .findInProgressAssignment(oid);
        Long ccAid = ccA.getId();

        assertEquals(ccA.getStatus(), CycleCountStatus.InProgress,
            "cycleCountStatus");
        CycleCountingDetail ccD = ccA.getCycleCountingDetails().get(0);

        assertEquals(Integer.toString(ccD.getActualQuantity()), actualQuantity,
            "actualQuantity");
        assertEquals(ccD.getStatus(), CycleCountDetailStatus.Counted,
            "detailStatus");

        record = response.getRecords().get(1);

        getCommandDispatcher()
            .mapArgumentsToCommand(
                new String[] {
                    makeStringFromDate(this.getCmdDateSec()), sn, operId,
                    record.get("locationID").toString(),
                    record.get("itemNumber").toString(), actualQuantity, "",
                    "", "" }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);

        assertEquals(odrResponse, null, "ODR returned response.");

        ccA = this.getCycleCountingAssignmentManager().get(ccAid);

        assertEquals(ccA.getStatus(), CycleCountStatus.Complete,
            "cycleCountStatus");
        ccD = ccA.getCycleCountingDetails().get(1);

        assertEquals(Integer.toString(ccD.getActualQuantity()), actualQuantity,
            "actualQuantity");
        assertEquals(ccD.getStatus(), CycleCountDetailStatus.Counted,
            "detailStatus");

    }

    /**
     * Test that the item update works correctly for the case where a skip slot
     * is done.
     * @throws Exception any
     */
    @Test()
    public void testCycleCountItemUpdateCountSkipSlot() throws Exception {

        final String operId = "ccOperator1";
        String sn = "ccDevice1UpdateCmd";
        String allRegion = "0";
        String regionNumber = "201";
        String actualQuantity = "0";
        long ccAid = 2;

        Response response;
        initialize(sn, operId);

        executeLutCmd("cmdPrTaskLUTCycleCountingRegion", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, regionNumber,
            allRegion });

        response = executeLutCmd(
            "cmdPrTaskLUTCycleCountingLocation",
            new String[] { makeStringFromDate(this.getCmdDateSec()), sn, operId });

        response = executeLutCmd("cmdPrTaskLUTCycleCountingAssignment",
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId, "", "",
                "" });

        ResponseRecord record = response.getRecords().get(0);

        getCommandDispatcher().mapArgumentsToCommand(
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                record.get("locationID").toString(),
                record.get("itemNumber").toString(), actualQuantity,
                record.get("uom").toString(), "Y", "" }, this.cmd);

        Response odrResponse = getTaskCommandService().executeCommand(this.cmd);

        assertEquals(odrResponse, null, "ODR returned response.");

        CycleCountingAssignment ccA = this.getCycleCountingAssignmentManager()
            .get(ccAid);

        assertEquals(ccA.getStatus(), CycleCountStatus.Skipped,
            "cycleCountStatus");
        CycleCountingDetail ccD = ccA.getCycleCountingDetails().get(0);

        assertEquals(Integer.toString(ccD.getActualQuantity()), actualQuantity,
            "actualQuantity");
        assertEquals(ccD.getStatus(), CycleCountDetailStatus.NotCounted,
            "detailStatus");

        ccD = ccA.getCycleCountingDetails().get(1);

        assertEquals(Integer.toString(ccD.getActualQuantity()), actualQuantity,
            "actualQuantity");
        assertEquals(ccD.getStatus(), CycleCountDetailStatus.NotCounted,
            "detailStatus");
    }

    /**
     * Initializes the database by calling task commands that need to be called
     * prior to task command in unit test.
     * 
     * @param sn - Serial Number
     * @param operId - Operator
     * @throws Exception - Exception
     */
    private void initialize(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd(
            "cmdPrTaskLUTCoreConfiguration",
            new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId,
                locale.toString(), "Default", this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
        executeLutCmd("cmdPrTaskLUTCycleCountingRegions", new String[] {
            makeStringFromDate(this.getCmdDateSec()), sn, operId });
    }

}
