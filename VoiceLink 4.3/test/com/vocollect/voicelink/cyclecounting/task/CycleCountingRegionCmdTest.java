/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.task;

import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.task.command.Response;
import com.vocollect.voicelink.task.command.ResponseRecord;
import com.vocollect.voicelink.task.command.TaskCommandTestCase;
import com.vocollect.voicelink.task.command.TaskErrorCode;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.TASK;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CORE;

import java.util.List;
import java.util.Locale;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test for BreakTypeCmd.
 * 
 * @author khazra
 */
@Test(groups = { FAST, TASK, CORE })
public class CycleCountingRegionCmdTest extends TaskCommandTestCase {

    private CycleCountingRegionCmd cmd;
    private OperatorManager operatorManager;
    private OperatorLaborManager operatorLaborManager;

    /**
     * @return a bean for testing.
     */
    private CycleCountingRegionCmd getCmdBean() {
        return (CycleCountingRegionCmd) getBean("cmdPrTaskLUTCycleCountingRegion");
    }

    /**
     * @return Operator labor manager
     */
    private OperatorLaborManager getOperatorLaborManagerBean() {
        return (OperatorLaborManager) getBean("operatorLaborManager");
    }

    /**
     * @return Operator manager
     */
    private OperatorManager getOperatorManagerBean() {
        return (OperatorManager) getBean("operatorManager");
    }

    /**
     * 
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        super.classSetUp();
        classSetupInsert("VoiceLinkDataUnitTest_CycleCountingRegions.xml");
        classSetupInsert("VoiceLinkDataUnitTest_CycleCountingOperators.xml");
    }

    /**
     * Each test setup.
     */
    @BeforeMethod
    protected void testSetUp() {
        this.cmd = getCmdBean();
        this.operatorLaborManager = getOperatorLaborManagerBean();
        this.operatorManager = getOperatorManagerBean();
    }

    /**
     * Test method for
     * {@link com.vocollect.voicelink.core.task.BreakTypeCmd#testExecute()}.
     * 
     * @throws Exception
     *             any
     */
    @Test()
    public void testExecute() throws Exception {
        final String operId = "CycleCountCmdOpr";

        // ------------------------------------------------------------------
        // Verify operator can select a region
        // ------------------------------------------------------------------
        String sn = "CycleCountCmdSerial";
        String regionNumber = "101";
        String allRegion = "0";

        Response response;
        initialize(sn, operId);

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, regionNumber, allRegion }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        // Verify the result is 0
        List<ResponseRecord> records = response.getRecords();
        ResponseRecord record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()), 0);

        //Check operator labor record created
        Operator operator = this.operatorManager.findByIdentifier(operId);
        OperatorLabor operatorLabor = this.operatorLaborManager
                .findOpenRecordByOperatorId(operator.getId());
        assertNotNull(operatorLabor);

        // ------------------------------------------------------------------
        // Verify operator cannot select a region that is not present in the
        // system
        // ------------------------------------------------------------------
        regionNumber = "104";

        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, regionNumber, allRegion }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        // Verify the result is error 1301
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()),
                TaskErrorCode.CC_REGION_NOT_FOUND.getErrorCode());

        // ----------------------------------------------------------------------
        // Verify operator cannot select region of two different count type and
        // count mode
        // ----------------------------------------------------------------------
        regionNumber = "101";
        allRegion = "0";
        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, regionNumber, allRegion }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        regionNumber = "103";
        allRegion = "0";
        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, regionNumber, allRegion }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        // Verify the result is error 1302
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()),
                TaskErrorCode.CC_NOT_COMPAT_WITH_PREV_REGION.getErrorCode());

        // ----------------------------------------------------------------------
        // Verify operator cannot select all when regions has mixed count mode
        // and count type
        // ----------------------------------------------------------------------
        regionNumber = "0";
        allRegion = "1";
        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, regionNumber, allRegion }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        // Verify the result is error 1304
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()),
                TaskErrorCode.CC_ALL_REGION_NOT_ALLOWED.getErrorCode());

        // ----------------------------------------------------------------------
        // We saw the Task Command task is sending zero in the region field and
        // we didn't detect that, this is to test we are detecting that.
        // ----------------------------------------------------------------------
        regionNumber = "0";
        allRegion = "0";
        getCommandDispatcher().mapArgumentsToCommand(
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, regionNumber, allRegion }, this.cmd);

        response = getTaskCommandService().executeCommand(this.cmd);
        assert (response != null);

        // Verify the result is error 1304
        records = response.getRecords();
        record = records.get(0);
        assertEquals(Long.parseLong(record.getErrorCode()),
                TaskErrorCode.CC_REGION_NOT_FOUND.getErrorCode());
    }

    /**
     * Initializes the database by calling task commands that need to be called
     * prior to task command in unit test.
     * 
     * @param sn
     *            - Serial Number
     * @param operId
     *            - Operator
     * @throws Exception
     *             - Exception
     */
    private void initialize(String sn, String operId) throws Exception {
        final Locale locale = Locale.US;

        executeLutCmd(
                "cmdPrTaskLUTCoreConfiguration",
                new String[] { makeStringFromDate(this.getCmdDateSec()), sn,
                        operId, locale.toString(), "Default",
                        this.goodCombinedTaskVersion() });
        executeLutCmd("cmdPrTaskLUTCoreBreakTypes", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId });
        executeLutCmd("cmdPrTaskLUTCoreSignOn", new String[] {
                makeStringFromDate(this.getCmdDateSec()), sn, operId, "1234" });
    }

}
