/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.service.ItemManager;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.cyclecounting.model.CountMode;
import com.vocollect.voicelink.cyclecounting.model.CountType;
import com.vocollect.voicelink.cyclecounting.model.CycleCountDetailStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingRegionManager;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * @author pkolonay
 * 
 */
public class CycleCountingAssignmentManagerImplTest extends
        BaseServiceManagerTestCase {

    private CycleCountingAssignmentManager  cycleCountingAssignmentManager;
    private LocationManager                 locationManager;
    private ItemManager                     itemManager;
    private OperatorManager                 operatorManager;
    private CycleCountingRegionManager      cycleCountingRegionManager;

    private Location                        location;
    private Item                            item;
    private CycleCountingRegion             ccRegion;
    private Operator                        operator;
    
    /**
     * @return the cycleCountingRegionManager
     */
    public CycleCountingRegionManager getCycleCountingRegionManager() {
        return cycleCountingRegionManager;
    }

    /**
     * @param regionManager - 
     *            the cycleCountingRegionManager to set
     */

    public void setCycleCountingRegionManager(CycleCountingRegionManager regionManager) {
        this.cycleCountingRegionManager = regionManager;
    }

    /**
     * @return the cycleCountingAssignmentManager
     */
    public CycleCountingAssignmentManager getCycleCountingAssignmentManager() {
        return cycleCountingAssignmentManager;
    }

    /**
     * @param cycleCountingAssignmentManager
     *            the cycleCountingAssignmentManager to set
     */
    public void setCycleCountingAssignmentManager(
            CycleCountingAssignmentManager cycleCountingAssignmentManager) {
        this.cycleCountingAssignmentManager = cycleCountingAssignmentManager;
    }

    /**
     * @return the locationManager
     */
    public LocationManager getLocationManager() {
        return locationManager;
    }

    /**
     * @param locationManager
     *            the locationManager to set
     */
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * @return the itemManager
     */
    public ItemManager getItemManager() {
        return itemManager;
    }

    /**
     * @param itemManager
     *            the itemManager to set
     */
    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
    }

    /**
     * @return the operatorManager
     */
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * @param operatorManager
     *            the operatorManager to set
     */
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    @AfterClass()
    protected void cleanUp() throws Exception {
        List<CycleCountingAssignment> assignments = this.cycleCountingAssignmentManager
                .getAll();
        List<CycleCountingRegion> regions = this.getCycleCountingRegionManager().getAll();

        // Cleanup the object created in the database.
        for (CycleCountingAssignment ccAss : assignments) {
            this.getCycleCountingAssignmentManager().delete(ccAss);
        }

        for (CycleCountingRegion region : regions) {
            this.getCycleCountingRegionManager().delete(region);
        }

        this.getLocationManager().delete(location);
        this.getItemManager().delete(item);
    }

    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {

//        adapter.resetInstallationData();
//        // adapter.handleFlatXmlResource(DATA_DIR
//        // + "VoiceLinkDataUnitTest_CycleCounting.xml",
//        // DatabaseOperation.INSERT);
//        // adapter.handleFlatXmlResource(DATA_DIR
//        // + "VoiceLinkDataUnitTest_CycleCountingOperators.xml",
//        // DatabaseOperation.INSERT);
//        adapter.handleFlatXmlResource(DATA_DIR + "VoiceLinkDataSmall.xml",
//                DatabaseOperation.CLEAN_INSERT);
        
        this.location = new Location();
        this.location.setScannedVerification("12345");
        this.location.setStatus(LocationStatus.Replenished);
        this.getLocationManager().save(this.location);
        
        this.item = new Item();
        this.item.setNumber("12345");
        this.item.setWeight(10.0);
        this.item.setCube(3.0);
        this.item.setIsVariableWeightItem(false);
        this.item.setIsSerialNumberItem(false);
        
        this.getItemManager().save(this.item);
        
        this.ccRegion = new CycleCountingRegion();

        this.ccRegion.setName("ccRegion");
        this.ccRegion.setNumber(888);
        this.ccRegion.setType(RegionType.CycleCounting);
        this.ccRegion.setCountMode(CountMode.SystemDirected);
        this.ccRegion.setCountType(CountType.Known);
        this.ccRegion.setGoalRate(200);

        this.getCycleCountingRegionManager().save(this.ccRegion);
        
    }

    @Test()
    public void testSave() throws Exception {

        CycleCountingAssignment ccAss = new CycleCountingAssignment();
        CycleCountingDetail detail = new CycleCountingDetail();
        List<CycleCountingDetail> details = new ArrayList<CycleCountingDetail>();

        ccAss.setLocation(this.location);
        ccAss.setStatus(CycleCountStatus.Available);
        ccAss.setSequence(1001L);
        ccAss.setRegion(this.ccRegion);

        detail.setItem(this.item);
        detail.setExpectedQuantity(10);
        detail.setCycleCountingAssignment(ccAss);
        detail.setStatus(CycleCountDetailStatus.NotCounted);
        detail.setUnitOfMeasure("case");

        details.add(detail);

        ccAss.setCycleCountingDetails(details);

        this.getCycleCountingAssignmentManager().save(ccAss);

        CycleCountingAssignment savedAssignment = this
                .getCycleCountingAssignmentManager().getAll().get(0);

        assertEquals(savedAssignment.getSequence().toString(), "1001",
                "Cycle  Counting assignment not found");

    }

}
