/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.service.ItemManager;
import com.vocollect.voicelink.core.service.LocationManager;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.cyclecounting.model.CountMode;
import com.vocollect.voicelink.cyclecounting.model.CountType;
import com.vocollect.voicelink.cyclecounting.model.CycleCountDetailStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingLabor;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingAssignmentManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingLaborManager;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingRegionManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * @author khazra
 * 
 */
public class CycleCountingLaborManagerImplTest extends
    BaseServiceManagerTestCase {

    private CycleCountingAssignmentManager cycleCountingAssignmentManager;

    private CycleCountingLaborManager cycleCountingLaborManager;

    private LocationManager locationManager;

    private ItemManager itemManager;

    private OperatorManager operatorManager;

    private OperatorLaborManager operatorLaborManager;

    private CycleCountingRegionManager cycleCountingRegionManager;

    private Location location;

    private Item item;

    private CycleCountingRegion ccRegion;

    private Operator operator;

    private CycleCountingAssignment ccAss;

    private OperatorLabor operLaborRecord;

    private CycleCountingLabor cycleCountingLabor;

    /**
     * @return the cycleCountingRegionManager
     */
    @Test(enabled = false)
    public CycleCountingRegionManager getCycleCountingRegionManager() {
        return cycleCountingRegionManager;
    }

    /**
     * @param regionManager - the cycleCountingRegionManager to set
     */
    @Test(enabled = false)
    public void setCycleCountingRegionManager(CycleCountingRegionManager regionManager) {
        this.cycleCountingRegionManager = regionManager;
    }

    /**
     * @return the cycleCountingAssignmentManager
     */
    @Test(enabled = false)
    public CycleCountingAssignmentManager getCycleCountingAssignmentManager() {
        return cycleCountingAssignmentManager;
    }

    /**
     * @param cycleCountingAssignmentManager the cycleCountingAssignmentManager
     *            to set
     */
    @Test(enabled = false)
    public void setCycleCountingAssignmentManager(CycleCountingAssignmentManager cycleCountingAssignmentManager) {
        this.cycleCountingAssignmentManager = cycleCountingAssignmentManager;
    }

    /**
     * @return the locationManager
     */
    @Test(enabled = false)
    public LocationManager getLocationManager() {
        return locationManager;
    }

    /**
     * @param locationManager the locationManager to set
     */
    @Test(enabled = false)
    public void setLocationManager(LocationManager locationManager) {
        this.locationManager = locationManager;
    }

    /**
     * @return the itemManager
     */
    @Test(enabled = false)
    public ItemManager getItemManager() {
        return itemManager;
    }

    /**
     * @param itemManager the itemManager to set
     */
    @Test(enabled = false)
    public void setItemManager(ItemManager itemManager) {
        this.itemManager = itemManager;
    }

    /**
     * @return the operatorManager
     */
    @Test(enabled = false)
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * @param operatorManager the operatorManager to set
     */
    @Test(enabled = false)
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * @return the cycleCountingLaborManager
     */
    @Test(enabled = false)
    public CycleCountingLaborManager getCycleCountingLaborManager() {
        return cycleCountingLaborManager;
    }

    /**
     * @param cycleCountingLaborManager the cycleCountingLaborManager to set
     */
    @Test(enabled = false)
    public void setCycleCountingLaborManager(CycleCountingLaborManager cycleCountingLaborManager) {
        this.cycleCountingLaborManager = cycleCountingLaborManager;
    }

    /**
     * @return the operatorLaborManager
     */
    @Test(enabled = false)
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }

    /**
     * @param operatorLaborManager the operatorLaborManager to set
     */
    @Test(enabled = false)
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    /**
     * @throws Exception
     */
    @AfterClass()
    protected void cleanUp() throws Exception {
        this.cycleCountingLaborManager.delete(cycleCountingLabor);
        this.operatorLaborManager.delete(operLaborRecord);
        this.cycleCountingAssignmentManager.delete(ccAss);
        this.locationManager.delete(location);
        this.itemManager.delete(item);
        this.operatorManager.delete(operator);
        this.cycleCountingRegionManager.delete(ccRegion);
    }

    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {

        this.location = new Location();
        this.location.setScannedVerification("12345");
        this.location.setStatus(LocationStatus.Replenished);
        this.getLocationManager().save(this.location);

        this.item = new Item();
        this.item.setNumber("12345");
        this.item.setWeight(10.0);
        this.item.setCube(3.0);
        this.item.setIsVariableWeightItem(false);
        this.item.setIsSerialNumberItem(false);

        this.getItemManager().save(this.item);

        this.ccRegion = new CycleCountingRegion();

        this.ccRegion.setName("ccRegion");
        this.ccRegion.setNumber(888);
        this.ccRegion.setType(RegionType.CycleCounting);
        this.ccRegion.setCountMode(CountMode.SystemDirected);
        this.ccRegion.setCountType(CountType.Known);
        this.ccRegion.setGoalRate(200);

        this.getCycleCountingRegionManager().save(this.ccRegion);

        ccAss = new CycleCountingAssignment();
        CycleCountingDetail detail = new CycleCountingDetail();
        List<CycleCountingDetail> details = new ArrayList<CycleCountingDetail>();

        ccAss.setLocation(this.location);
        ccAss.setStatus(CycleCountStatus.Available);
        ccAss.setSequence(1001L);
        ccAss.setRegion(this.ccRegion);

        detail.setItem(this.item);
        detail.setExpectedQuantity(10);
        detail.setCycleCountingAssignment(ccAss);
        detail.setStatus(CycleCountDetailStatus.NotCounted);
        detail.setUnitOfMeasure("case");

        details.add(detail);

        ccAss.setCycleCountingDetails(details);

        this.getCycleCountingAssignmentManager().save(ccAss);

        this.operator = new Operator();

        this.operator.setName("TestCCLaborOperator");
        this.operator.setOperatorIdentifier("TestCCLaborOperator");
        this.operator.setPassword("nopass");
        Set<Region> regions = new HashSet<Region>();
        regions.add(ccRegion);
        this.operator.setRegions(regions);
        this.operatorManager.save(operator);

    }

    /**
     * 
     */
    @Test()
    public void testCreateCycleCountingLabor() throws Exception {
        Calendar now = Calendar.getInstance();
        this.operatorLaborManager.openLaborRecord(now.getTime(), operator,
            OperatorLaborActionType.CycleCounting);

        operLaborRecord = this.operatorLaborManager
            .findOpenRecordByOperatorId(operator.getId());

        this.cycleCountingLaborManager.openLaborRecord(now.getTime(), operator,
            ccAss, operLaborRecord);

        cycleCountingLabor = this.cycleCountingLaborManager
            .findOpenRecordByAssignmentId(ccAss.getId());

        now.add(Calendar.HOUR, 1);
        this.cycleCountingLaborManager.closeLaborRecord(now.getTime(), ccAss);
        this.operatorLaborManager.closeLaborRecord(now.getTime(), operator);
    }

}
