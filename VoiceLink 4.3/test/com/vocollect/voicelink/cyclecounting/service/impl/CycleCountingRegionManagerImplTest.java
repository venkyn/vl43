/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.model.Workgroup;
import com.vocollect.voicelink.core.model.WorkgroupFunction;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.WorkgroupManager;
import com.vocollect.voicelink.core.service.impl.BaseServiceManagerTestCase;
import com.vocollect.voicelink.cyclecounting.model.CountMode;
import com.vocollect.voicelink.cyclecounting.model.CountType;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingRegionManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.voicelink.test.VoicelinkTestGroups.CYCLECOUNTING;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * @author khazra
 * 
 */
@Test(groups = { FAST, CYCLECOUNTING })
public class CycleCountingRegionManagerImplTest extends
        BaseServiceManagerTestCase {

    private CycleCountingRegionManager ccManager;
    private WorkgroupManager wgManager;

    private OperatorManager operatorManager;
    private Operator operator;

    @Override
    protected void initData(DbUnitAdapter adapter) throws Exception {

    }

    /**
     * @param impl
     *            - implementation of selection region manager.
     */
    @Test(enabled = false)
    public void setCycleCountingRegionManager(CycleCountingRegionManager impl) {
        this.ccManager = impl;
    }

    /**
     * @param impl
     *            - implementation of selection region manager.
     */
    @Test(enabled = false)
    public void setWorkgroupManager(WorkgroupManager impl) {
        this.wgManager = impl;
    }

    /**
     * @return the operatorManager
     */
    @Test(enabled = false)
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * @param operatorManager
     *            the operatorManager to set
     */
    @Test(enabled = false)
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }


    /**
     * 
     * @throws Exception
     */
    @AfterClass()
    protected void cleanUp() throws Exception {
        this.operatorManager.delete(this.operator);
        
        List<CycleCountingRegion> ccRegions = ccManager.getAll();

        for (CycleCountingRegion ccRegion : ccRegions) {
            ccManager.delete(ccRegion);
        }

    }

    /**
     * @throws Exception
     */
    @Test(enabled = false)
    public void testSave() throws Exception {
        CycleCountingRegion ccRegion = new CycleCountingRegion();
        ccRegion.setName("Test CC Region Create Test");
        ccRegion.setNumber(100);
        ccRegion.setType(RegionType.CycleCounting);
        ccRegion.setGoalRate(10);
        ccRegion.setCountType(CountType.Blind);
        ccRegion.setCountMode(CountMode.OperatorDirected);

        ccManager.save(ccRegion);

    }

    /**
     * @throws Exception
     */
    @Test()
    public void testRegionAddedToWorkgroup() throws Exception {
        CycleCountingRegion ccRegion = new CycleCountingRegion();
        ccRegion.setName("Test CC Region Workgrouped");
        ccRegion.setNumber(101);
        ccRegion.setType(RegionType.CycleCounting);
        ccRegion.setGoalRate(10);
        ccRegion.setCountType(CountType.Blind);
        ccRegion.setCountMode(CountMode.OperatorDirected);

        ccManager.save(ccRegion);

        List<Workgroup> workGroups = wgManager.listAutoAddWorkgroups();
        for (Workgroup workgroup : workGroups) {
            for (WorkgroupFunction wgf : workgroup.getWorkgroupFunctions()) {
                if (wgf.getTaskFunction().getRegionType() == RegionType.CycleCounting) {

                    assertTrue(wgf.getRegions().size() >= 1,
                            "Regions are not getting created in workgroup");
                }
            }
        }

    }

    /**
     * @throws Exception
     */
    @Test()
    public void testFindRegionByNumber() throws Exception {
        CycleCountingRegion ccRegion = new CycleCountingRegion();
        ccRegion.setName("Test CC Region Find");
        ccRegion.setNumber(102);
        ccRegion.setType(RegionType.CycleCounting);
        ccRegion.setGoalRate(10);
        ccRegion.setCountType(CountType.Blind);
        ccRegion.setCountMode(CountMode.OperatorDirected);

        ccManager.save(ccRegion);

        CycleCountingRegion targetRegion = ccManager.findRegionByNumber(102);

        assertTrue(targetRegion.getName().equals("Test CC Region Find"),
                "Expected region \"Test CC Region Workgrouped\" but found "
                        + targetRegion.getName());
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testUniquenessByName() throws Exception {
        CycleCountingRegion ccRegion = new CycleCountingRegion();
        ccRegion.setName("Test CC Region Unique Name");
        ccRegion.setNumber(103);
        ccRegion.setType(RegionType.CycleCounting);
        ccRegion.setGoalRate(10);
        ccRegion.setCountType(CountType.Blind);
        ccRegion.setCountMode(CountMode.OperatorDirected);

        ccManager.save(ccRegion);

        Long id = ccManager.uniquenessByName("Test CC Region Unique Name");

        assertTrue(id != null,
                "Expected region should not be unique, but found otherwise");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testUniquenessByNumber() throws Exception {
        CycleCountingRegion ccRegion = new CycleCountingRegion();
        ccRegion.setName("Test CC Region Unique Number");
        ccRegion.setNumber(104);
        ccRegion.setType(RegionType.CycleCounting);
        ccRegion.setGoalRate(10);
        ccRegion.setCountType(CountType.Blind);
        ccRegion.setCountMode(CountMode.OperatorDirected);

        ccManager.save(ccRegion);

        Long id = ccManager.uniquenessByNumber(104);

        assertTrue(id != null,
                "Expected region should not be unique, but found otherwise");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testAverageGoalRate() throws Exception {
        CycleCountingRegion ccRegion1 = new CycleCountingRegion();
        ccRegion1.setName("Test CC Region Goal Rate 1");
        ccRegion1.setNumber(105);
        ccRegion1.setType(RegionType.CycleCounting);
        ccRegion1.setGoalRate(10);
        ccRegion1.setCountType(CountType.Blind);
        ccRegion1.setCountMode(CountMode.OperatorDirected);

        ccManager.save(ccRegion1);

        CycleCountingRegion ccRegion2 = new CycleCountingRegion();
        ccRegion2.setName("Test CC Region Goal Rate 2");
        ccRegion2.setNumber(106);
        ccRegion2.setType(RegionType.CycleCounting);
        ccRegion2.setGoalRate(10);
        ccRegion2.setCountType(CountType.Blind);
        ccRegion2.setCountMode(CountMode.OperatorDirected);

        ccManager.save(ccRegion2);

        this.operator = new Operator();
        this.operator.setName("TestCCLaborOperator");
        this.operator.setOperatorIdentifier("TestCCLaborOperator");
        this.operator.setPassword("nopass");
        Set<Region> regions = new HashSet<Region>();
        regions.add(ccRegion1);
        regions.add(ccRegion2);
        this.operator.setRegions(regions);
        this.operatorManager.save(operator);

        Double avgGoalRate = ccManager.avgGoalRate();

        assertTrue(avgGoalRate == 10, "Expected goal rate is 10 but found "
                + avgGoalRate);
    }

}
