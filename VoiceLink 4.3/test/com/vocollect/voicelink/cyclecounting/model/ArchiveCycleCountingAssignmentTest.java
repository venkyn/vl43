/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.epp.model.Site;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.LocationDescription;

import java.util.Date;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

/**
 * Tests the Archive Cycle Counting Assigment Object.
 * 
 * @author mlashinsky
 */
public class ArchiveCycleCountingAssignmentTest {

    ArchiveCycleCountingAssignment archiveCCA = new ArchiveCycleCountingAssignment();

    List<ArchiveCycleCountingDetail> archCycleCountingDetails = null;

    Date createdDate = new Date();

    Date endTime = new Date();

    LocationDescription description = new LocationDescription();

    ExportStatus exportStatus = ExportStatus.Exported;

    String operatorIdentifier = "Sam.I.am";

    String operatorName = "Simon Says";

    CycleCountingRegion region = new CycleCountingRegion();

    Long sequence = -1L;

    Site site = new Site();

    Date startTime = new Date();

    CycleCountStatus status = CycleCountStatus.Complete;

    /**
     * Sets up the data for the test execution.
     */
    @BeforeClass()
    protected void setUp() {
    }

    /**
     * Tests the SequenceNumber
     */
    @Test(enabled = false)
    public void testSequence() {
        assertEquals(this.archiveCCA.getSequence(), this.sequence,
            "CC Assignment sequence did not return the proper value.");
        assertNotNull(this.archiveCCA.getSequence(),
            "CC Assignment sequence was null");
    }

    /**
     * Tests the Region
     */
    @Test(enabled = false)
    public void testRegion() {
        assertEquals(this.archiveCCA.getRegion(), this.region,
            "CC Assignment region did not return the proper value.");
        assertNotNull(this.archiveCCA.getRegion(),
            "CC Assignment region was null");

    }

    /**
     * Tests the Status
     */
    @Test(enabled = false)
    public void testStatus() {
        assertEquals(this.archiveCCA.getStatus(), this.status,
            "CC Assignment status did not return the proper value.");
        assertNotNull(this.archiveCCA.getStatus(),
            "CC Assignment status was null");

    }

    /**
     * Tests the OperatorID
     */
    @Test(enabled = false)
    public void testOperatorIdentifier() {
        assertEquals(this.archiveCCA.getOperatorIdentifier(),
            this.operatorIdentifier,
            "CC Assignment operatorID did not return the proper value.");
        assertNotNull(this.archiveCCA.getOperatorIdentifier(),
            "CC Assignment operatorID was null");
    }

    /**
     * Tests the Operator Name
     */
    @Test(enabled = false)
    public void testOperatorName() {
        assertEquals(this.archiveCCA.getOperatorName(), this.operatorName,
            "CC Assignment operatorName did not return the proper value.");
        assertNotNull(this.archiveCCA.getOperatorName(),
            "CC Assignment operatorName was null");
    }

    /**
     * Tests the StartTime
     */
    @Test(enabled = false)
    public void testStartTime() {
        assertEquals(this.archiveCCA.getStartTime(), this.startTime,
            "CC Assignment startTime did not return the proper value.");
        assertNotNull(this.archiveCCA.getStartTime(),
            "CC Assignment startTime was null");
    }

    /**
     * Tests the EndTime
     */
    @Test(enabled = false)
    public void testEndTime() {
        assertEquals(this.archiveCCA.getEndTime(), this.endTime,
            "CC Assignment endTime did not return the proper value.");
        assertNotNull(this.archiveCCA.getEndTime(),
            "CC Assignment endTime was null");
    }

    /**
     * Tests the ExportStatus
     */
    @Test(enabled = false)
    public void testExportStatus() {
        assertEquals(this.archiveCCA.getExportStatus(), this.exportStatus,
            "CC Assignment exportStatus did not return the proper value.");
        assertNotNull(this.archiveCCA.getExportStatus(),
            "CC Assignment exportStatus was null");
    }

    /**
     * Tests the Description
     */
    @Test(enabled = false)
    public void testDescription() {
        assertEquals(this.getArchiveCCA().getDescription(), this.description,
            "CC Assignment description did not return the proper value.");
        assertNotNull(this.archiveCCA.getDescription(),
            "CC Assignment description was null");
    }

    /**
     * Tests the list of ArchiveCCDetails
     */
    @Test(enabled = false)
    public void testArchiveCCDetail() {
        assertEquals(this.archCycleCountingDetails,
            archiveCCA.getArchiveCCDetails(),
            "CC Assignment details did not return the proper value.");
        assertNotNull(archiveCCA.getArchiveCCDetails(),
            "CC Assignment details was null");
    }

    /**
     * Tests the Site
     */
    @Test(enabled = false)
    public void testSite() {
        assertEquals(this.archiveCCA.getSite(), site,
            "CC Assignment site did not return the proper value.");
        assertNotNull(this.archiveCCA.getSite(), "CC site region was null");
    }

    /**
     * Tests the CreatedDate
     */
    @Test(enabled = false)
    public void testCreatedDate() {
        assertEquals(this.archiveCCA.getCreatedDate(), this.createdDate,
            "CC Assignment createdDate did not return the proper value.");
        assertNotNull(this.archiveCCA.getCreatedDate(),
            "CC Assignment createdDate was null");
    }

    /**
     * Getter for the archiveCCA property.
     * @return ArchiveCycleCountingAssignment value of the property
     */
    public ArchiveCycleCountingAssignment getArchiveCCA() {
        this.archiveCCA.setArchiveCCDetails(this.archCycleCountingDetails);
        this.archiveCCA.setCreatedDate(this.createdDate);
        this.archiveCCA.setDescription(this.description);
        this.archiveCCA.setEndTime(this.endTime);
        this.archiveCCA.setExportStatus(this.exportStatus);
        this.archiveCCA.setOperatorIdentifier(this.operatorIdentifier);
        this.archiveCCA.setOperatorName(this.operatorName);
        this.archiveCCA.setRegion(this.region);
        this.archiveCCA.setSequence(this.sequence);
        this.archiveCCA.setSite(this.site);
        this.archiveCCA.setStartTime(this.startTime);
        this.archiveCCA.setStatus(this.status);

        return archiveCCA;
    }

    /**
     * Setter for the archiveCCA property.
     * @param archiveCCA the new archiveCCA value
     */
    public void setArchiveCCA(ArchiveCycleCountingAssignment archiveCCA) {
        this.archiveCCA = archiveCCA;
    }

}
