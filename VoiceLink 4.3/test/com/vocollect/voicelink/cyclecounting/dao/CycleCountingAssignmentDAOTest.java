/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.dao;

import com.vocollect.voicelink.core.dao.ItemDAO;
import com.vocollect.voicelink.core.dao.LocationDAO;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.cyclecounting.model.CountMode;
import com.vocollect.voicelink.cyclecounting.model.CountType;
import com.vocollect.voicelink.cyclecounting.model.CycleCountDetailStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit test for the Line Loading Carton mananger.
 * 
 */
@Test(groups = { FAST, DAO })
public class CycleCountingAssignmentDAOTest extends VoiceLinkDAOTestCase {

    private LocationDAO locationDAO;

    private ItemDAO itemDAO;

    private CycleCountingAssignmentDAO cycleCountingAssignmentDAO;

    private CycleCountingRegionDAO cycleCountingRegionDAO;

    private Location location;

    private Item item1, item2;

    private CycleCountingRegion ccRegion;

    /**
     * @return the cycleCountingRegionDAO
     */
    public CycleCountingRegionDAO getCycleCountingRegionDAO() {
        return cycleCountingRegionDAO;
    }

    /**
     * @param cycleCountingRegionDAO the cycleCountingRegionDAO to set
     */
    @Test(enabled = false)
    public void setCycleCountingRegionDAO(CycleCountingRegionDAO cycleCountingRegionDAO) {
        this.cycleCountingRegionDAO = cycleCountingRegionDAO;
    }

    /**
     * @return the cycleCountingAssignmentDAO
     */
    public CycleCountingAssignmentDAO getCycleCountingAssignmentDAO() {
        return cycleCountingAssignmentDAO;
    }

    /**
     * @param cycleCountingAssignmentDAO the cycleCountingAssignmentDAO to set
     */
    @Test(enabled = false)
    public void setCycleCountingAssignmentDAO(CycleCountingAssignmentDAO cycleCountingAssignmentDAO) {
        this.cycleCountingAssignmentDAO = cycleCountingAssignmentDAO;
    }

    /**
     * classSetUp is used to repopulate the database before each test.
     * 
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

    }

    /**
     * @param dao the item data access object
     */
    @Test(enabled = false)
    public void setItemDAO(ItemDAO dao) {
        this.itemDAO = dao;
    }

    /**
     * @param dao the location data access object
     */
    @Test(enabled = false)
    public void setLocationDAO(LocationDAO dao) {
        this.locationDAO = dao;
    }

    /**
     * Test method for
     * {@link com.vocollect.epp.dao.GenericDAO#save(java.lang.Object)}.
     * 
     * @throws Exception on unexpected failure.
     */
    @Test()
    public void testCreateCycleCountingAssignment() throws Exception {
        CycleCountingAssignment assign = new CycleCountingAssignment();
        List<CycleCountingDetail> details = new ArrayList<CycleCountingDetail>();

        CycleCountingDetail detail = new CycleCountingDetail();

        // create some data to use for testing.
        this.location = new Location();
        this.location.setScannedVerification("12345");
        this.location.setStatus(LocationStatus.Replenished);
        this.locationDAO.save(this.location);

        this.item1 = new Item();
        this.item1.setNumber("12345");
        this.item1.setWeight(10.0);
        this.item1.setCube(3.0);
        this.item1.setIsVariableWeightItem(false);
        this.item1.setIsSerialNumberItem(false);

        this.itemDAO.save(this.item1);

        this.item2 = new Item();
        this.item2.setNumber("12346");
        this.item2.setWeight(10.0);
        this.item2.setCube(3.0);
        this.item2.setIsVariableWeightItem(false);
        this.item2.setIsSerialNumberItem(false);

        this.itemDAO.save(this.item2);

        this.ccRegion = new CycleCountingRegion();

        this.ccRegion.setName("ccRegion");
        this.ccRegion.setNumber(888);
        this.ccRegion.setType(RegionType.CycleCounting);
        this.ccRegion.setCountMode(CountMode.SystemDirected);
        this.ccRegion.setCountType(CountType.Known);
        this.ccRegion.setGoalRate(200);

        this.getCycleCountingRegionDAO().save(this.ccRegion);

        detail.setExpectedQuantity(10);
        detail.setItem(item1);
        detail.setStatus(CycleCountDetailStatus.NotCounted);
        detail.setUnitOfMeasure("ea");
        detail.setCycleCountingAssignment(assign);
        details.add(detail);

        detail = new CycleCountingDetail();
        detail.setCycleCountingAssignment(assign);
        detail.setExpectedQuantity(20);
        detail.setItem(this.item2);
        detail.setStatus(CycleCountDetailStatus.NotCounted);
        detail.setUnitOfMeasure("case");
        details.add(detail);

        assign.setLocation(this.location);
        assign.setCycleCountingDetails(details);
        assign.setSequence(100L);
        assign.setStatus(CycleCountStatus.Available);
        assign.setRegion(ccRegion);

        this.cycleCountingAssignmentDAO.save(assign);

        List<CycleCountingAssignment> ccAssignments = this.cycleCountingAssignmentDAO
            .getAll();

        assertTrue(ccAssignments.get(0).getSequence() == 100,
            "CycleCountingAssignment Assignment not created");

        assertEquals(ccAssignments.get(0).getCycleCountingDetails().get(0)
            .getExpectedQuantity(), 10, "CycleCountingDetail not created");

        // clean up objects after test
        this.cycleCountingAssignmentDAO.delete(ccAssignments.get(0));

        this.getCycleCountingRegionDAO().delete(this.ccRegion);

        this.locationDAO.delete(location);
        this.itemDAO.delete(item1);
        this.itemDAO.delete(item2);

    }

    /**
     * @throws Exception - throws exception
     */
    @Test(enabled = false)
    public void testDetails() throws Exception {

        List<CycleCountingAssignment> ccAssignments = this.cycleCountingAssignmentDAO
            .getAll();

        assertEquals(ccAssignments.get(0).getCycleCountingDetails().get(0)
            .getExpectedQuantity(), 10, "CycleCountingDetail not created");

    }

    /**
     * @throws Exception - throws exception
     */
    @Test(enabled = false)
    public void testFindAssignmentByOperator() throws Exception {

        List<CycleCountingAssignment> ccAssignments = this.cycleCountingAssignmentDAO
            .listAssignmentsByOperator(null);

        assertTrue(ccAssignments.get(0).getSequence() == 100,
            "CycleCountingAssignment Assignment not created");

    }

}
