/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.dao;

import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.cyclecounting.model.CountMode;
import com.vocollect.voicelink.cyclecounting.model.CountType;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.List;

import org.testng.annotations.Test;

/**
 * Tests for CycleCountingRegionDAO methods.
 * 
 * @author khazra
 */
@Test(groups = { FAST, DAO })
public class CycleCountingRegionDAOTest extends VoiceLinkDAOTestCase {

    private CycleCountingRegionDAO dao = null;

    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();

        for (int i = 101; i < 105; i++) {
            CycleCountingRegion ccRegion = new CycleCountingRegion();
            ccRegion.setName("Test CC Region " + i);
            ccRegion.setNumber(i);
            ccRegion.setType(RegionType.CycleCounting);
            ccRegion.setGoalRate(10);
            ccRegion.setCountType(CountType.Blind);
            ccRegion.setCountMode(CountMode.OperatorDirected);

            dao.save(ccRegion);
        }
    }

    @Override
    protected void onTearDown() throws Exception {
        List<CycleCountingRegion> ccRegions = dao.getAll();

        for (CycleCountingRegion ccRegion : ccRegions) {
            dao.delete(ccRegion);
        }

        super.onTearDown();
    }

    /**
     * @param ccDAO
     *            the user data access object
     */
    @Test(enabled = false)
    public void setCycleCountingRegionDAO(CycleCountingRegionDAO ccDAO) {
        this.dao = ccDAO;
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testCreate() throws Exception {
        CycleCountingRegion ccRegion = new CycleCountingRegion();
        ccRegion.setName("Test CC Region Create Test");
        ccRegion.setNumber(100);
        ccRegion.setType(RegionType.CycleCounting);
        ccRegion.setGoalRate(10);
        ccRegion.setCountType(CountType.Blind);
        ccRegion.setCountMode(CountMode.OperatorDirected);

        dao.save(ccRegion);

        List<CycleCountingRegion> fetchedRegions = dao.getAll();

        boolean regionFound = false;
        for (CycleCountingRegion fetchedRegion : fetchedRegions) {
            if (fetchedRegion.getName().equals("Test CC Region Create Test")) {
                regionFound = true;
                break;
            }
        }

        assertTrue(regionFound,
                "Region \"Test CC Region Create Test\" not created");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testRead() throws Exception {
        List<CycleCountingRegion> fetchedRegions = dao.getAll();

        CycleCountingRegion targetRegion = null;
        for (CycleCountingRegion fetchedRegion : fetchedRegions) {
            if (fetchedRegion.getName().equals("Test CC Region 101")) {
                targetRegion = fetchedRegion;
                break;
            }
        }

        assertTrue(
                targetRegion != null,
                "Region \"Test CC Region 101\" not found, possibly \"onSetUp\" method didn't create");

        CycleCountingRegion fetchedRegionById = dao.get(targetRegion.getId());

        assertTrue(fetchedRegionById.getCountType().equals(CountType.Blind),
                "Region \"Test CC Region 101\" didn't save properly");

    }

    /**
     * @throws Exception
     */
    @Test()
    public void testUpdate() throws Exception {
        List<CycleCountingRegion> fetchedRegions = dao.getAll();

        CycleCountingRegion targetRegion = null;
        for (CycleCountingRegion fetchedRegion : fetchedRegions) {
            if (fetchedRegion.getName().equals("Test CC Region 102")) {
                targetRegion = fetchedRegion;
                break;
            }
        }

        assertTrue(
                targetRegion != null,
                "Region \"Test CC Region 102\" not found, possibly \"onSetUp\" method didn't create");

        targetRegion.setDescription("Setting some description");
        dao.save(targetRegion);

        CycleCountingRegion fetchedRegionById = dao.get(targetRegion.getId());

        assertTrue(
                fetchedRegionById.getDescription().equals(
                        "Setting some description"),
                "Region \"Test CC Region 102\" didn't update properly");

    }

    /**
     * @throws Exception
     */
    @Test()
    public void testDelete() throws Exception {
        List<CycleCountingRegion> fetchedRegions = dao.getAll();

        CycleCountingRegion targetRegion = null;
        for (CycleCountingRegion fetchedRegion : fetchedRegions) {
            if (fetchedRegion.getName().equals("Test CC Region 103")) {
                targetRegion = fetchedRegion;
                break;
            }
        }

        assertTrue(
                targetRegion != null,
                "Region \"Test CC Region 103\" not found, possibly \"onSetUp\" method didn't create");

        dao.delete(targetRegion);

        boolean regionDeleted = false;
        try {
            dao.get(targetRegion.getId());
        } catch (EntityNotFoundException ex) {
            regionDeleted = true;
        }

        assertTrue(regionDeleted,
                "Region \"Test CC Region 103\" didn't delete properly");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testFindRegionByNumber() throws Exception {
        CycleCountingRegion fetchedRegion = dao.findRegionByNumber(101);
        assertTrue(fetchedRegion.getName().equals("Test CC Region 101"),
                "Region \"Test CC Region 101\" not found");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testUniquenessByNumber() throws Exception {
        Long regionID = dao.uniquenessByNumber(101);
        assertTrue(regionID != 0,
                "Method didn't return any region when it is expected");

        regionID = dao.uniquenessByNumber(201);
        assertTrue(regionID == null,
                "Method did return a region when it is not expected");
    }

    /**
     * @throws Exception
     */
    @Test()
    public void testUniquenessByName() throws Exception {
        Long regionID = dao.uniquenessByName("Test CC Region 101");
        assertTrue(regionID != 0,
                "Method didn't return any region when it is expected");

        regionID = dao.uniquenessByName("Test CC Region 201");
        assertTrue(regionID == null,
                "Method did return a region when it is not expected");
    }
}
