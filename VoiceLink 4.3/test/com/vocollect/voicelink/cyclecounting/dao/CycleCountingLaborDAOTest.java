/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.dao.ItemDAO;
import com.vocollect.voicelink.core.dao.LocationDAO;
import com.vocollect.voicelink.core.dao.LocationItemDAO;
import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.dao.OperatorLaborDAO;
import com.vocollect.voicelink.core.dao.VoiceLinkDAOTestCase;
import com.vocollect.voicelink.core.model.ExportStatus;
import com.vocollect.voicelink.core.model.Item;
import com.vocollect.voicelink.core.model.Location;
import com.vocollect.voicelink.core.model.LocationStatus;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLabor;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.cyclecounting.model.CountMode;
import com.vocollect.voicelink.cyclecounting.model.CountType;
import com.vocollect.voicelink.cyclecounting.model.CycleCountStatus;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingLabor;
import com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit test for the Line Loading Carton mananger.
 * 
 */
@Test(groups = { FAST, DAO })
public class CycleCountingLaborDAOTest extends VoiceLinkDAOTestCase {

    private LocationDAO locationDAO;

    private ItemDAO itemDAO;

    private OperatorDAO operatorDAO;

    private OperatorLaborDAO operatorLaborDAO;

    private LocationItemDAO locationItemDAO;

    private CycleCountingAssignmentDAO cycleCountingAssignmentDAO;

    private CycleCountingDetailDAO cycleCountingDetailDAO;

    private CycleCountingRegionDAO cycleCountingRegionDAO;

    private CycleCountingLaborDAO cycleCountingLaborDAO;

    private OperatorLabor operatorLabor;

    private Operator operator;

    private CycleCountingRegion ccRegion;

    private Location location;

    private Item item;

    private CycleCountingAssignment ccAssignment;

    private CycleCountingLabor cycleCountingLabor1;

    private CycleCountingLabor cycleCountingLabor2;

    /**
     * @return the cycleCountingRegionDAO
     */
    public CycleCountingRegionDAO getCycleCountingRegionDAO() {
        return cycleCountingRegionDAO;
    }

    /**
     * @param cycleCountingRegionDAO the cycleCountingRegionDAO to set
     */
    @Test(enabled = false)
    public void setCycleCountingRegionDAO(CycleCountingRegionDAO cycleCountingRegionDAO) {
        this.cycleCountingRegionDAO = cycleCountingRegionDAO;
    }

    /**
     * @return the cycleCountingAssignmentDAO
     */
    public CycleCountingAssignmentDAO getCycleCountingAssignmentDAO() {
        return cycleCountingAssignmentDAO;
    }

    /**
     * @param cycleCountingAssignmentDAO the cycleCountingAssignmentDAO to set
     */
    @Test(enabled = false)
    public void setCycleCountingAssignmentDAO(CycleCountingAssignmentDAO cycleCountingAssignmentDAO) {
        this.cycleCountingAssignmentDAO = cycleCountingAssignmentDAO;
    }

    /**
     * @return the cycleCountingLaborDAO
     */
    @Test(enabled = false)
    public CycleCountingLaborDAO getCycleCountingLaborDAO() {
        return cycleCountingLaborDAO;
    }

    /**
     * @param cycleCountingLaborDAO the cycleCountingLaborDAO to set
     */
    @Test(enabled = false)
    public void setCycleCountingLaborDAO(CycleCountingLaborDAO cycleCountingLaborDAO) {
        this.cycleCountingLaborDAO = cycleCountingLaborDAO;
    }

    /**
     * classSetUp is used to repopulate the database before each test.
     * 
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

    }

    /**
     * @return the operatorDAO
     */
    @Test(enabled = false)
    public OperatorDAO getOperatorDAO() {
        return operatorDAO;
    }

    /**
     * @param operatorDAO the operatorDAO to set
     */
    @Test(enabled = false)
    public void setOperatorDAO(OperatorDAO operatorDAO) {
        this.operatorDAO = operatorDAO;
    }

    /**
     * @return the operatorLaborDAO
     */
    @Test(enabled = false)
    public OperatorLaborDAO getOperatorLaborDAO() {
        return operatorLaborDAO;
    }

    /**
     * @param operatorLaborDAO the operatorLaborDAO to set
     */
    @Test(enabled = false)
    public void setOperatorLaborDAO(OperatorLaborDAO operatorLaborDAO) {
        this.operatorLaborDAO = operatorLaborDAO;
    }

    /**
     * @param dao the item data access object
     */
    @Test(enabled = false)
    public void setItemDAO(ItemDAO dao) {
        this.itemDAO = dao;
    }

    /**
     * @return the locationDAO
     */
    public LocationDAO getLocationDAO() {
        return locationDAO;
    }

    /**
     * @return the itemDAO
     */
    public ItemDAO getItemDAO() {
        return itemDAO;
    }

    /**
     * @param dao the location data access object
     */
    @Test(enabled = false)
    public void setLocationDAO(LocationDAO dao) {
        this.locationDAO = dao;
    }

    /**
     * @return the locationItemDAO
     */
    @Test(enabled = false)
    public LocationItemDAO getLocationItemDAO() {
        return locationItemDAO;
    }

    /**
     * @param locationItemDAO the locationItemDAO to set
     */
    @Test(enabled = false)
    public void setLocationItemDAO(LocationItemDAO locationItemDAO) {
        this.locationItemDAO = locationItemDAO;
    }

    /**
     * @return the cycleCountingDetailDAO
     */
    @Test(enabled = false)
    public CycleCountingDetailDAO getCycleCountingDetailDAO() {
        return cycleCountingDetailDAO;
    }

    /**
     * @param cycleCountingDetailDAO the cycleCountingDetailDAO to set
     */
    @Test(enabled = false)
    public void setCycleCountingDetailDAO(CycleCountingDetailDAO cycleCountingDetailDAO) {
        this.cycleCountingDetailDAO = cycleCountingDetailDAO;
    }

    /**
     * @throws DataAccessException
     * 
     */
    @Test(enabled = true)
    public void testCycleCountingLabor() throws DataAccessException {
        createItems();
        createLocations();
        createCCRegions();
        createAssignments();
        createOperator();
        createOperatorLabor();

        createCycleCountingLaborOpened();
        createCloseCycleCountingLabor();

        getOpenRecordsByOperatorId();
        getRecordsByOperatorId();
        getAllRecordsByOperatorLaborId();

        deleteAllData();
    }

    /**
     * @throws DataAccessException
     */
    @Test(enabled = false)
    private void deleteAllData() throws DataAccessException {
        this.getCycleCountingLaborDAO().delete(cycleCountingLabor1);
        this.getCycleCountingLaborDAO().delete(cycleCountingLabor2);
        this.getOperatorLaborDAO().delete(operatorLabor);
        this.getOperatorDAO().delete(operator);
        this.getCycleCountingAssignmentDAO().delete(ccAssignment);
        this.getCycleCountingRegionDAO().delete(ccRegion);
        this.getLocationDAO().delete(location);
        this.getItemDAO().delete(item);
    }

    /**
     * 
     */
    @Test(enabled = false)
    private void createOperatorLabor() throws DataAccessException {
        operatorLabor = new OperatorLabor();
        operatorLabor.setOperator(operator);
        operatorLabor.setActionType(OperatorLaborActionType.CycleCounting);
        operatorLabor.setStartTime(new Date());
        operatorLabor.setExportStatus(ExportStatus.NotExported);
        operatorLabor.setFilterType(OperatorLaborFilterType.CycleCounting);

        this.operatorLaborDAO.save(operatorLabor);
    }

    /**
     * 
     */
    @Test(enabled = false)
    private void createOperator() throws DataAccessException {
        operator = new Operator();
        operator.setName("CCLaborTestOpr");
        operator.setOperatorIdentifier("CCLaborTestOpr");

        this.operatorDAO.save(operator);
    }

    /**
     * @throws DataAccessException
     * 
     */
    @Test(enabled = false)
    private void createItems() throws DataAccessException {
        item = new Item();
        item.setNumber("1");
        item.setDescription("Item 1");
        item.setWeight(1);
        item.setCube(1);
        item.setIsVariableWeightItem(false);
        item.setIsSerialNumberItem(false);

        this.itemDAO.save(item);
    }

    /**
     * @throws DataAccessException
     * 
     */
    @Test(enabled = false)
    private void createLocations() throws DataAccessException {
        location = new Location();
        location.setSpokenVerification("1");
        location.setScannedVerification("1");
        location.setCheckDigits("1");
        location.setPreAisle("Bldg 1");
        location.setAisle("Aisle 1");
        location.setSlot("Slot 1");
        location.setStatus(LocationStatus.Replenished);

        this.locationDAO.save(location);
    }

    /**
     * @throws DataAccessException
     * 
     */
    @Test(enabled = false)
    private void createCCRegions() throws DataAccessException {
        ccRegion = new CycleCountingRegion();

        ccRegion.setName("System Directed Known");
        ccRegion.setNumber(1);
        ccRegion.setType(RegionType.CycleCounting);
        ccRegion.setCountMode(CountMode.SystemDirected);
        ccRegion.setCountType(CountType.Known);
        ccRegion.setGoalRate(2);

        this.getCycleCountingRegionDAO().save(ccRegion);

    }

    /**
     * @throws DataAccessException
     * 
     */
    @Test(enabled = false)
    private void createAssignments() throws DataAccessException {
        ccAssignment = new CycleCountingAssignment();

        ccAssignment.setLocation(location);
        ccAssignment.setSequence(1L);
        ccAssignment.setStatus(CycleCountStatus.Available);
        ccAssignment.setRegion(ccRegion);

        this.cycleCountingAssignmentDAO.save(ccAssignment);

    }

    /**
     * 
     */
    @Test(enabled = false)
    private void createCycleCountingLaborOpened() throws DataAccessException {
        cycleCountingLabor1 = new CycleCountingLabor();
        cycleCountingLabor1.setActualRate(1.0);
        cycleCountingLabor1.setAssignment(ccAssignment);
        cycleCountingLabor1.setBreakLaborId(null);
        cycleCountingLabor1.setCreatedDate(new Date());
        cycleCountingLabor1.setDuration(0L);
        cycleCountingLabor1.setEndTime(null);
        cycleCountingLabor1.setExportStatus(ExportStatus.NotExported);
        cycleCountingLabor1.setLocationsCounted(0);
        cycleCountingLabor1.setOperator(operator);
        cycleCountingLabor1.setOperatorLabor(operatorLabor);
        cycleCountingLabor1.setPercentOfGoal(0.0);
        cycleCountingLabor1.setStartTime(new Date());

        this.cycleCountingLaborDAO.save(cycleCountingLabor1);

    }

    /**
     * 
     */
    @Test(enabled = false)
    private void createCloseCycleCountingLabor() throws DataAccessException {
        cycleCountingLabor2 = new CycleCountingLabor();
        Calendar now = Calendar.getInstance();
        cycleCountingLabor2.setActualRate(1.0);
        cycleCountingLabor2.setAssignment(ccAssignment);
        cycleCountingLabor2.setBreakLaborId(null);
        cycleCountingLabor2.setCreatedDate(new Date());
        cycleCountingLabor2.setDuration(0L);
        cycleCountingLabor2.setExportStatus(ExportStatus.NotExported);
        cycleCountingLabor2.setLocationsCounted(0);
        cycleCountingLabor2.setOperator(operator);
        cycleCountingLabor2.setOperatorLabor(operatorLabor);
        cycleCountingLabor2.setPercentOfGoal(0.0);
        cycleCountingLabor2.setStartTime(now.getTime());
        now.add(Calendar.HOUR, 1);
        cycleCountingLabor2.setEndTime(now.getTime());

        this.cycleCountingLaborDAO.save(cycleCountingLabor2);
    }

    /**
     * 
     */
    @Test(enabled = false)
    private void getAllRecordsByOperatorLaborId() throws DataAccessException {
        List<CycleCountingLabor> cycleCountingLabor = this.cycleCountingLaborDAO
            .listAllRecordsByOperatorLaborId(operatorLabor.getId());

        assertEquals(cycleCountingLabor.size(), 2);

    }

    /**
     * 
     */
    @Test(enabled = false)
    private void getRecordsByOperatorId() throws DataAccessException {
        List<CycleCountingLabor> cycleCountingLabor = this.cycleCountingLaborDAO
            .listRecordsByOperatorId(operator.getId());

        assertEquals(cycleCountingLabor.size(), 2);
    }

    /**
     * 
     */
    @Test(enabled = false)
    private void getOpenRecordsByOperatorId() throws DataAccessException {
        List<CycleCountingLabor> cycleCountingLabor = this.cycleCountingLaborDAO
            .listOpenRecordsByOperatorId(operator.getId());

        assertEquals(cycleCountingLabor.size(), 1);
    }
}
