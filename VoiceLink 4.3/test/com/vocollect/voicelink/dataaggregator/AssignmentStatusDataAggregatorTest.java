/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.FAST;
import java.util.Date;

import org.dbunit.operation.DatabaseOperation;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

/**
 * @author smittal
 * 
 */
@Test(groups = { DA, FAST })
public class AssignmentStatusDataAggregatorTest extends
    VoiceLinkDataAggregatorTestCase {

    private DataAggregatorHandler dataAggregatorHandler = null;

    private AssignmentManager assignmentManager = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_SelectionRegions.xml",
            DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
        setUpSiteContext(false);
    }

    /**
     * @return the dataAggregatorHandler
     */
    @Test(enabled = false)
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * @param dataAggregatorHandler the dataAggregatorHandler to set
     */
    @Test(enabled = false)
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }

    /**
     * @return the assignmentManager
     */
    @Test(enabled = false)
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * @param assignmentManager the assignmentManager to set
     */
    @Test(enabled = false)
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    @Test(enabled = true)
    public void testAssignmentCountPerStatus() throws BusinessRuleException,
        DataAccessException, JSONException {

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("AssignmentStatus");

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        assertNotNull(da);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        for (int i = 0; i < output.length(); i++) {
            // 0 because no assignment will fall in the 24 hour window.
            assertEquals(output.getJSONObject(i).getInt("numberofassignments"),
                0);
        }

        // Try again with updated assignment
        Assignment assignment1 = getAssignmentManager().get(-693L);
        Assignment assignment2 = getAssignmentManager().get(-692L);
        Date ddt = new Date();
        ddt.setTime(ddt.getTime() + 600000);

        String availableAssignmentRouteToCompare = getAssignmentManager()
            .getRouteDepartureDateField(assignment1.getRoute(), ddt);
        String completeAssignmentRouteToCompare = getAssignmentManager()
            .getRouteDepartureDateField(assignment2.getRoute(), ddt);

        assignment1.setDepartureDateTime(ddt);
        assignment2.setDepartureDateTime(ddt);
        assignment2.setStatus(AssignmentStatus.Complete);
        getAssignmentManager().save(assignment1);
        getAssignmentManager().save(assignment2);

        // Execute aggregator and assert
        output = da.execute();
        for (int i = 0; i < output.length(); i++) {
            JSONObject obj = output.getJSONObject(i);

            if ((obj.getString("routeatdeparturedate")
                .equals(availableAssignmentRouteToCompare))
                && (obj.getString("status").equals("Available"))) {
                assertEquals(obj.getInt("numberofassignments"), 1);
            } else if ((obj.getString("routeatdeparturedate")
                .equals(completeAssignmentRouteToCompare))
                && (obj.getString("status").equals("Complete"))) {
                assertEquals(obj.getInt("numberofassignments"), 1);
            } else {
                // 0 because no assignment will fall in the 24 hour window.
                assertEquals(obj.getInt("numberofassignments"), 0);
            }
        }

    }

    @Test(enabled = true)
    public void testAssignmentStatusAggregatorColumnSequence()
        throws JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("AssignmentStatus");

        assertNotNull(da);

        String[] fields = {
            "routeatdeparturedate", "status", "numberofassignments" };

        JSONArray assignmentStatusColumns = da.getAllOutputColumns();

        for (int i = 0; i < assignmentStatusColumns.length(); i++) {
            JSONObject columnObject = (JSONObject) assignmentStatusColumns
                .get(i);
            assertEquals(
                columnObject.getString(GenericDataAggregator.FIELD_ID),
                fields[i]);
            assertEquals(
                columnObject.getInt(GenericDataAggregator.COLUMN_SEQUENCE),
                i + 1);
        }
    }

}
