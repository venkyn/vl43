/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.TimeWindowFilterType;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

/**
 * 
 * 
 * @author khazra
 */
@Test(groups = { DA, FAST })
public class RegionDataAggregatorTest extends VoiceLinkDataAggregatorTestCase {

    private DataAggregatorHandler dataAggregatorHandler = null;

    private OperatorManager operatorManager;

    private RegionManager regionManager;

    private AssignmentManager assignmentManager;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_OperatorDataAggregator.xml",
            DatabaseOperation.CLEAN_INSERT);
        setUpSiteContext(false);
    }

    /**
     * @return the dataAggregatorHandler
     */
    @Test(enabled = false)
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * @param dataAggregatorHandler the dataAggregatorHandler to set
     */
    @Test(enabled = false)
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }

    /**
     * Getter for the operatorManager property.
     * @return OperatorManager value of the property
     */
    @Test(enabled = false)
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * Setter for the operatorManager property.
     * @param operatorManager the new operatorManager value
     */
    @Test(enabled = false)
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * Getter for the RegionManager property.
     * @return regionManager value of the property
     */
    @Test(enabled = false)
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * Setter for the RegionManager property.
     * @param regionManager the new regionManager value
     */
    @Test(enabled = false)
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Getter for the assignmentManager property.
     * @return AssignmentManager value of the property
     */
    @Test(enabled = false)
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * Setter for the assignmentManager property.
     * @param assignmentmanager the new assignmentManager value
     */
    @Test(enabled = false)
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    @Test(enabled = false)
    public void updateAssignmentDepartureDateTimesToTest()
        throws DataAccessException, BusinessRuleException {
        Date now = DateUtil.convertTimeToSiteTime(new Date());
        Date nowPlusInterval = (new DateTime(now)).plusHours(7).toDate();

        List<Assignment> assignments = getAssignmentManager().getAll();

        for (int i = 0; i < assignments.size(); i++) {
            Assignment assignment = assignments.get(i);
            assignment.setDepartureDateTime(nowPlusInterval);
            getAssignmentManager().save(assignment);
        }

    }

    @Test(enabled = false)
    public void rollBackAssignmentDepartureDateTimesToTest()
        throws DataAccessException, BusinessRuleException {

        List<Assignment> assignments = getAssignmentManager().getAll();

        for (int i = 0; i < assignments.size(); i++) {
            Assignment assignment = assignments.get(i);
            assignment.setDepartureDateTime(null);
            getAssignmentManager().save(assignment);
        }

    }

    @Test(enabled = true)
    public void testOperatorsWorking() throws BusinessRuleException,
        DataAccessException, JSONException {

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Region");

        assertNotNull(da);

        int numberOfIntervals = getRegionManager().getUserConfiguredIntervals(
            RegionDataAggregator.INTERVALS).length;    

        Operator operator1 = getOperatorManager().get(7L);
        Operator operator2 = getOperatorManager().get(8L);
        Operator operator3 = getOperatorManager().get(9L);

        operator1.setCurrentRegion(getRegionManager().get(-1L));
        operator2.setCurrentRegion(getRegionManager().get(-1L));
        operator3.setCurrentRegion(getRegionManager().get(-1L));

        Assignment assignment1 = getAssignmentManager().get(-97L);
        Assignment assignment2 = getAssignmentManager().get(-51L);
        Assignment assignment3 = getAssignmentManager().get(-50L);

        assignment1.setOperator(operator1);
        assignment2.setOperator(operator2);
        assignment3.setOperator(operator3);

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();

        assertEquals(output.length(),
            getRegionManager().listByTypeOrderByName(RegionType.Selection)
                .size() * numberOfIntervals);

        int numberOfOperators = 0;
        for (int i = 0; i < output.length(); i++) {
            if (output.getJSONObject(i).get("region")
                .equals("Selection Region 1")) {
                numberOfOperators = output.getJSONObject(i).getInt(
                    "operatorsworking");
                break;
            }
        }
        assertEquals(numberOfOperators, 3);
    }

    @Test(enabled = true)
    public void testOperatorsRequired() throws BusinessRuleException,
        DataAccessException, JSONException {

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Region");

        assertNotNull(da);

        int numberOfIntervals = getRegionManager().getUserConfiguredIntervals(
            RegionDataAggregator.INTERVALS).length;        

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();

        assertEquals(output.length(),
            getRegionManager().listByTypeOrderByName(RegionType.Selection)
                .size() * numberOfIntervals);

        for (int i = 0; i < output.length(); i++) {
            assertEquals(output.getJSONObject(i).getInt("operatorrequired"), 0);
        }
    }

    @Test(enabled = true)
    public void testTotalQuantity() throws BusinessRuleException,
        DataAccessException, JSONException {

        updateAssignmentDepartureDateTimesToTest();

        Long totalQuantityInRegion1 = 97L;

        String region1Name = getRegionManager().get(-1L).getName();

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Region");

        assertNotNull(da);

        int numberOfIntervals = getRegionManager().getUserConfiguredIntervals(
            RegionDataAggregator.INTERVALS).length;        

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();

        assertEquals(output.length(),
            getRegionManager().listByTypeOrderByName(RegionType.Selection)
                .size() * numberOfIntervals);

        Long totalQuantity = 0L;

        for (int i = 0; i < output.length(); i++) {
            if ((output.getJSONObject(i).get("region").equals(region1Name))
                && (output.getJSONObject(i).getInt("interval") == 8)) {
                totalQuantity = output.getJSONObject(i)
                    .getLong("totalquantity");
                break;
            }
        }

        assertEquals(totalQuantityInRegion1, totalQuantity);
        rollBackAssignmentDepartureDateTimesToTest();
    }

    @Test(enabled = true)
    public void testQuantityToPick() throws BusinessRuleException,
        DataAccessException, JSONException {

        updateAssignmentDepartureDateTimesToTest();

        Long totalQuantityInRegion1 = 97L;
        Long totalQuantityPickedInRegion1 = 17L;
        Long totalQuantityToPickInRegion1 = totalQuantityInRegion1
            - totalQuantityPickedInRegion1;

        String region1Name = getRegionManager().get(-1L).getName();

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Region");

        assertNotNull(da);

        int numberOfIntervals = getRegionManager().getUserConfiguredIntervals(
            RegionDataAggregator.INTERVALS).length;

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();

        assertEquals(output.length(),
            getRegionManager().listByTypeOrderByName(RegionType.Selection)
                .size() * numberOfIntervals);

        Long quantityToPick = null;

        for (int i = 0; i < output.length(); i++) {
            if ((output.getJSONObject(i).get("region").equals(region1Name))
                && (output.getJSONObject(i).getInt("interval") == 8)) {
                quantityToPick = output.getJSONObject(i).getLong(
                    "quantitytopick");
                break;
            }
        }

        assertEquals(totalQuantityToPickInRegion1, quantityToPick);
        rollBackAssignmentDepartureDateTimesToTest();
    }

    @Test(enabled = true)
    public void testRegionPercentageComplete() throws BusinessRuleException,
        DataAccessException, JSONException {

        updateAssignmentDepartureDateTimesToTest();

        Double totalQuantityInRegion1 = 97.0;
        Double totalQuantityPickedInRegion1 = 17.0;

        Double totalPercentWorkDoneInRegion1 = (totalQuantityInRegion1 == 0
            ? 0
            : (totalQuantityPickedInRegion1 / totalQuantityInRegion1) * 100.0);
        BigDecimal bd = new BigDecimal(totalPercentWorkDoneInRegion1);
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        totalPercentWorkDoneInRegion1 = bd.doubleValue();

        String region1Name = getRegionManager().get(-1L).getName();

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Region");

        assertNotNull(da);

        int numberOfIntervals = getRegionManager().getUserConfiguredIntervals(
            RegionDataAggregator.INTERVALS).length;     

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();

        assertEquals(output.length(),
            getRegionManager().listByTypeOrderByName(RegionType.Selection)
                .size() * numberOfIntervals);

        Double percentWorkDone = null;

        for (int i = 0; i < output.length(); i++) {
            if ((output.getJSONObject(i).get("region").equals(region1Name))
                && (output.getJSONObject(i).getInt("interval") == 8)) {
                percentWorkDone = output.getJSONObject(i).getDouble(
                    "percentassignmentscomplete");
                break;
            }
        }

        assertEquals(totalPercentWorkDoneInRegion1, percentWorkDone);
        rollBackAssignmentDepartureDateTimesToTest();
    }

    @Test()
    public void testRegionAggregatorColumnSequence() throws JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Region");

        assertNotNull(da);

        String[] fields = {
            "region", "departuredateinterval", "operatorsworking",
            "operatorrequired", "percentassignmentscomplete", "quantitytopick",
            "totalquantity", "interval" };

        JSONArray regionColumns = da.getAllOutputColumns();

        for (int i = 0; i < regionColumns.length(); i++) {
            JSONObject columnObject = (JSONObject) regionColumns.get(i);
            assertEquals(
                columnObject.getString(GenericDataAggregator.FIELD_ID),
                fields[i]);
            assertEquals(
                columnObject.getInt(GenericDataAggregator.COLUMN_SEQUENCE),
                i + 1);
        }
    }

    @Test()
    public void testRegionDADataWithFilter() throws BusinessRuleException, JSONException,
        DataAccessException {

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("RegionAssignmentStatus");

        assertNotNull(da);

        int filterValue = TimeWindowFilterType.NEXT_4_HRS.toValue();

        Date startTime = DateUtil.convertTimeToSiteTime(new Date());
        DateTime endTimeValue = new DateTime(startTime);
        Date endTime = endTimeValue.plusHours(filterValue).toDate();

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, startTime);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, endTime);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        for (int i = 0; i < output.length(); i++) {
            String intervalDesc = output.getJSONObject(i).getString(
                "departuredateinterval");
            Integer interval = Integer.parseInt(intervalDesc.substring(5, 7));
            assertTrue(interval <= filterValue);
        }
    }
}
