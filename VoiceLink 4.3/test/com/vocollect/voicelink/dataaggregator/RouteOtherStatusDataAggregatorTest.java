/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.Arrays;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

/**
 * @author smittal
 * 
 */
@Test(groups = { DA, FAST })
public class RouteOtherStatusDataAggregatorTest extends VoiceLinkDataAggregatorTestCase {

    private DataAggregatorHandler dataAggregatorHandler = null;

    private AssignmentManager assignmentManager = null;
    
    /**
     * @return the dataAggregatorHandler
     */
    @Test(enabled = false)
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * @param dataAggregatorHandler the dataAggregatorHandler to set
     */
    @Test(enabled = false)
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }

    /**
     * @return the assignmentManager
     */
    @Test(enabled = false)
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * @param assignmentManager the assignmentManager to set
     */
    @Test(enabled = false)
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * Set the site context.
     * @param siteId - the site ID
     * @throws Exception
     * 
     */
    protected void setSiteContext(Long siteId) throws Exception {
        this.setUpSiteContextNoDefaultSite(true);
        SiteContextHolder.getSiteContext().setCurrentSite(siteId);
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_SelectionData.xml",
            DatabaseOperation.CLEAN_INSERT);
        setUpSiteContext(false);
    }

    @Test(enabled = true)
    public void testIdentiyColumn() throws Exception, DataAccessException,
        JSONException {
        setSiteContext(-1L);
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("RouteOtherStatus");

        JSONObject parameters = new JSONObject();

        Site site = SiteContextHolder.getSiteContext().getCurrentSite();
        parameters.put("SITE", site);

        da.setParameters(parameters);
        
        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertTrue(output.length() > 0);

        for (int i = 0; i < output.length(); i++) {
            assertNotNull(output.getJSONObject(i).getString("route"));
        }
    }

    @Test()
    public void testProgressPercentage() throws Exception, DataAccessException,
        JSONException {
        setSiteContext(-1L);
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("RouteOtherStatus");

        JSONObject parameters = new JSONObject();

        Site site = SiteContextHolder.getSiteContext().getCurrentSite();
        parameters.put("SITE", site);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertTrue(output.length() > 0);
        for (int i = 0; i < output.length(); i++) {
            Double percentComplete = output.getJSONObject(i).getDouble(
                "percentageroutecomplete");
            assertNotNull(percentComplete);
            assertTrue((percentComplete == 0.00) || (percentComplete == 100.00));
        }
    }

    @Test()
    public void testRouteStatus() throws Exception, DataAccessException,
        JSONException {
        setSiteContext(-1L);
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("RouteOtherStatus");

        JSONObject parameters = new JSONObject();

        Site site = SiteContextHolder.getSiteContext().getCurrentSite();
        parameters.put("SITE", site);

        da.setParameters(parameters);

        String[] statuses = { "Completed", "NotStarted" };
        List<String> routeStatuses = Arrays.asList(statuses);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertTrue(output.length() > 0);
        for (int i = 0; i < output.length(); i++) {
            String routeStatus = output.getJSONObject(i).getString("status");
            assertNotNull(routeStatus);
            assertTrue(routeStatuses.contains(routeStatus));
        }
    }
    
    @Test()
    public void testRouteAggregatorColumnSequence() throws JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("RouteOtherStatus");

        assertNotNull(da);

        String[] fields = {
            "route", "status", "percentageroutecomplete" };
        
        JSONArray routeColumns = da.getAllOutputColumns();

        for (int i = 0; i < routeColumns.length(); i++) {
            JSONObject columnObject = (JSONObject) routeColumns.get(i);
            assertEquals(
                columnObject.getString(GenericDataAggregator.FIELD_ID),
                fields[i]);
            assertEquals(
                columnObject.getInt(GenericDataAggregator.COLUMN_SEQUENCE),
                i + 1);
        }
    }
}
