/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.SiteContextHolder;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.model.RegionType;
import com.vocollect.voicelink.core.service.RegionManager;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.FAST;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

/**
 * @author smittal
 * 
 */
@Test(groups = { DA, FAST })
public class InfiniteRegionAggregatorTest extends
    VoiceLinkDataAggregatorTestCase {

    private DataAggregatorHandler dataAggregatorHandler = null;
    
    private RegionManager regionManager = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_SelectionRegions.xml",
            DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
        setUpSiteContext(false);
    }

    /**
     * @return the dataAggregatorHandler
     */
    @Test(enabled = false)
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * @param dataAggregatorHandler the dataAggregatorHandler to set
     */
    @Test(enabled = false)
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }

    /**
     * Getter for the regionManager property.
     * @return RegionManager value of the property
     */
    @Test(enabled = false)
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * Setter for the regionManager property.
     * @param regionManager the new regionManager value
     */
    @Test(enabled = false)
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Set the site context.
     * @param siteId - the site ID
     * @throws Exception
     * 
     */
    @Test(enabled = false)
    protected void setSiteContext(Long siteId) throws Exception {
        this.setUpSiteContextNoDefaultSite(true);
        SiteContextHolder.getSiteContext().setCurrentSite(siteId);
    }
    
    @Test(enabled = true)
    public void testRegionNumberAndName() throws Exception {
        setSiteContext(-1L);
        
        List<Region> regions = getRegionManager().listByTypeOrderByName(
            RegionType.Selection);

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("InfiniteRegionDataAggregator");

        assertNotNull(da);

        Site site = SiteContextHolder.getSiteContext().getCurrentSite();
        JSONObject parameters = new JSONObject();
        parameters.put("SITE", site);
        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        
        for (int i = 0; i < regions.size(); i++) {
            assertEquals(regions.get(i).getName(), ((JSONObject) output.get(i)).getString("region"));
            assertEquals(regions.get(i).getNumber(), (Integer) ((JSONObject) output.get(i)).getInt("regionnumber"));
        }
    }
    
    @Test(enabled = true)
    public void testTotalQuantityRemaining() throws Exception {
        setSiteContext(-1L);
        
        List<Region> regions = getRegionManager().listByTypeOrderByName(
            RegionType.Selection);

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("InfiniteRegionDataAggregator");

        assertNotNull(da);

        Site site = SiteContextHolder.getSiteContext().getCurrentSite();
        JSONObject parameters = new JSONObject();
        parameters.put("SITE", site);
        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertEquals(output.length(), regions.size());

        for (int i = 0; i < output.length(); i++) {
            JSONObject obj = (JSONObject) output.get(i);
            if ((obj.getString("region").equals("Selection region 16"))
                && (obj.getString("region").equals("Selection region 7"))) {
                assertTrue(obj.getLong("itemsremaining") > 0);
            }
        }        
    }
    
    @Test(enabled = true)
    public void testTotalQuantityPicked() throws Exception {
        setSiteContext(-1L);
        
        List<Region> regions = getRegionManager().listByTypeOrderByName(
            RegionType.Selection);

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("InfiniteRegionDataAggregator");

        assertNotNull(da);

        Site site = SiteContextHolder.getSiteContext().getCurrentSite();
        JSONObject parameters = new JSONObject();
        parameters.put("SITE", site);
        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertEquals(output.length(), regions.size());
        
        for (int i = 0; i < output.length(); i++) {
            JSONObject obj = (JSONObject) output.get(i);
            assertTrue(obj.getLong("itemspicked") == 0);
        }
        
    }
    
    @Test(enabled = true)
    public void testHoursRemaining() throws Exception {
        setSiteContext(-1L);
        
        List<Region> regions = getRegionManager().listByTypeOrderByName(
            RegionType.Selection);

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("InfiniteRegionDataAggregator");

        assertNotNull(da);

        Site site = SiteContextHolder.getSiteContext().getCurrentSite();
        JSONObject parameters = new JSONObject();
        parameters.put("SITE", site);
        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertEquals(output.length(), regions.size());
        
        for (int i = 0; i < output.length(); i++) {
            JSONObject obj = (JSONObject) output.get(i);
            assertTrue(obj.getDouble("hoursremaining") == 0);
        }
        
    }

    @Test(enabled = true)
    public void testInfiniteRegionAggregatorColumnSequence()
        throws JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("InfiniteRegionDataAggregator");

        assertNotNull(da);

        String[] fields = {
            "region", "regionnumber", "hoursremaining", "itemsremaining", 
            "itemspicked" };

        JSONArray assignmentStatusColumns = da.getAllOutputColumns();

        for (int i = 0; i < assignmentStatusColumns.length(); i++) {
            JSONObject columnObject = (JSONObject) assignmentStatusColumns
                .get(i);
            assertEquals(
                columnObject.getString(GenericDataAggregator.FIELD_ID),
                fields[i]);
            assertEquals(
                columnObject.getInt(GenericDataAggregator.COLUMN_SEQUENCE),
                i + 1);
        }
    }

}
