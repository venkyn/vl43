/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.model.AssignmentStatus;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.Date;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

/**
 * @author mraj
 * 
 */
@Test(groups = { DA, FAST })
public class RouteDataAggregatorTest extends VoiceLinkDataAggregatorTestCase {

    private DataAggregatorHandler dataAggregatorHandler = null;

    private AssignmentManager assignmentManager;

    /**
     * @return the dataAggregatorHandler
     */
    @Test(enabled = false)
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * @param dataAggregatorHandler the dataAggregatorHandler to set
     */
    @Test(enabled = false)
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }

    /**
     * @return the assignmentManager
     */
    @Test(enabled = false)
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * @param assignmentManager the assignmentManager to set
     */
    @Test(enabled = false)
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_SelectionData.xml",
            DatabaseOperation.CLEAN_INSERT);
        setUpSiteContext(false);

        List<Assignment> assignments = this.getAssignmentManager().getAll();

        DateTime newDepartureDate = new DateTime();
        newDepartureDate = newDepartureDate.plusHours(2);
        for (int i = 0; i < assignments.size(); i++) {
            Assignment assignment = assignments.get(i);
            assignment.setDepartureDateTime(newDepartureDate.toDate());
            this.getAssignmentManager().save(assignments);
        }
    }

    @Test(enabled = true)
    public void testIdentiyColumn() throws Exception, DataAccessException,
        JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Route");

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertTrue(output.length() > 0);

        for (int i = 0; i < output.length(); i++) {
            assertNotNull(output.getJSONObject(i).getString("route"));
        }
    }

    @Test()
    public void testRouteDepartureDate() throws Exception, DataAccessException,
        JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Route");

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertTrue(output.length() > 0);

        for (int i = 0; i < output.length(); i++) {
            assertNotNull(output.getJSONObject(i).get("departuredate"));
        }
    }

    @Test()
    public void testProgressPercentage() throws Exception, DataAccessException,
        JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Route");

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertTrue(output.length() > 0);
        for (int i = 0; i < output.length(); i++) {
            assertNotNull(output.getJSONObject(i).getDouble(
                "percentageroutecomplete"));
            if (output.getJSONObject(i).getString("route").equals("18")) {
                assertEquals(
                    output.getJSONObject(i)
                        .getString("percentageroutecomplete"), "6.45");
            }

        }
    }

    @Test()
    public void testProjectedDepartureDelay() throws Exception,
        DataAccessException, JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Route");

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertTrue(output.length() > 0);

        for (int i = 0; i < output.length(); i++) {
            assertNotNull(output.getJSONObject(i).getInt(
                "projecteddeparturedelay"));
            if (output.getJSONObject(i).getString("route").equals("18")) {

                Date departureDate = (Date) output.getJSONObject(i).get(
                    "departuredate");
                Date projectedDepartureDate = (Date) output.getJSONObject(i)
                    .get("projecteddeparturedate");
                Minutes differenceInMinutes = Minutes.minutesBetween(
                    new DateTime(departureDate), new DateTime(
                        projectedDepartureDate));
                Integer projectedDepartureDelayInMinute = differenceInMinutes
                    .getMinutes();
                assertEquals(
                    (Integer) output.getJSONObject(i).getInt(
                        "projecteddeparturedelay"),
                    projectedDepartureDelayInMinute);

            }
        }
    }

    @Test()
    public void testAvailableAssignmentsCount() throws Exception,
        DataAccessException, JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Route");

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertTrue(output.length() > 0);
        for (int i = 0; i < output.length(); i++) {
            assertNotNull(output.getJSONObject(i).getDouble(
                "availableassignments"));
            if (output.getJSONObject(i).getString("route").equals("18")) {
                assertEquals(
                    output.getJSONObject(i).getString("availableassignments"),
                    "14");
            }

        }
    }

    @Test()
    public void testCompletedAssignmentsCount() throws Exception,
        DataAccessException, JSONException {

        Assignment newAssignment = getAssignmentManager().getAll().get(0);

        String route = newAssignment.getRoute();

        newAssignment.setStatus(AssignmentStatus.Complete);
        getAssignmentManager().save(newAssignment);

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Route");

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertTrue(output.length() > 0);
        for (int i = 0; i < output.length(); i++) {
            assertNotNull(output.getJSONObject(i).getDouble(
                "completedassignments"));
            if (output.getJSONObject(i).getString("route").equals(route)) {
                assertEquals(
                    output.getJSONObject(i).getString("completedassignments"),
                    "1");
            }
        }
    }

    @Test()
    public void testShortedAssignmentsCount() throws Exception,
        DataAccessException, JSONException {

        Assignment newAssignment = getAssignmentManager().getAll().get(0);

        String route = newAssignment.getRoute();

        newAssignment.setStatus(AssignmentStatus.Short);
        getAssignmentManager().save(newAssignment);

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Route");

        JSONObject parameters = new JSONObject();
        parameters.put(GenericDataAggregator.START_TIME_PARAMETER, null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER, null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        assertTrue(output.length() > 0);
        for (int i = 0; i < output.length(); i++) {
            assertNotNull(output.getJSONObject(i).getDouble(
                "shortedassignments"));
            if (output.getJSONObject(i).getString("route").equals(route)) {
                assertEquals(
                    output.getJSONObject(i).getString("shortedassignments"),
                    "1");
            }
        }
    }
    
    @Test()
    public void testRouteAggregatorColumnSequence() throws JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("Route");

        assertNotNull(da);

        String[] fields = {
            "routeatdeparturedate", "departuredate", "projecteddeparturedate",
            "projecteddeparturedelay", "operatorrequired",
            "percentageroutecomplete", "availableassignments",
            "shortedassignments", "completedassignments", "route" };
        
        JSONArray routeColumns = da.getAllOutputColumns();

        for (int i = 0; i < routeColumns.length(); i++) {
            JSONObject columnObject = (JSONObject) routeColumns.get(i);
            assertEquals(
                columnObject.getString(GenericDataAggregator.FIELD_ID),
                fields[i]);
            assertEquals(
                columnObject.getInt(GenericDataAggregator.COLUMN_SEQUENCE),
                i + 1);
        }
    }
}
