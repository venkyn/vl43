/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.TimeWindowFilterType;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.voicelink.selection.model.Assignment;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.FAST;
import java.util.Date;


import org.dbunit.operation.DatabaseOperation;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;


/**
 * @author mraj
 *
 */
@Test(groups = { DA, FAST })
public class RegionAssignmentStatusDataAggregatorTest extends
    VoiceLinkDataAggregatorTestCase {
    
    private DataAggregatorHandler dataAggregatorHandler = null;
    
    private AssignmentManager assignmentManager = null;
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_SelectionRegions.xml",
            DatabaseOperation.REFRESH);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
        setUpSiteContext(false);
    }

    /**
     * @return the dataAggregatorHandler
     */
    @Test(enabled = false)
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * @param dataAggregatorHandler the dataAggregatorHandler to set
     */
    @Test(enabled = false)
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }

    
    /**
     * @return the assignmentManager
     */
    @Test(enabled = false)
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    
    /**
     * @param assignmentManager the assignmentManager to set
     */
    @Test(enabled = false)
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    @Test(enabled = true)
    public void testAssignmentCountPerStatus() throws BusinessRuleException,
        DataAccessException, JSONException {

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("RegionAssignmentStatus");

        assertNotNull(da);

        JSONObject parameters = new JSONObject();
        parameters.put(
            GenericDataAggregator.START_TIME_PARAMETER,
            null);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER,
            null);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();
        for (int i = 0; i < output.length(); i++) {
            //0 because no assignment will fall in the 24 hour window.
            assertEquals(output.getJSONObject(i).getInt("numberofassignments"), 0);
        }
        
        //Try again with updated assignment
        Assignment assignment = getAssignmentManager().get(-693L);
        Date ddt =  DateUtil.convertTimeToSiteTime(new Date());
        
        assignment.setDepartureDateTime(new Date(ddt.getTime() + 600000));
        getAssignmentManager().save(assignment);
        
        // Execute aggregator and assert
        output = da.execute();
        for (int i = 0; i < output.length(); i++) {
            JSONObject obj = output.getJSONObject(i);
            
            if (obj.getString("region").equals("Selection Region 15")
                &&  obj.getString("status").equals("Available")) {
                assertEquals(obj.getInt("numberofassignments"), 1);
            } else {
                //0 because no assignment will fall in the 24 hour window.
                assertEquals(obj.getInt("numberofassignments"), 0);
            }
        }        
    }
    
    @Test()
    public void testRegionAssignmentStatusAggregatorColumnSequence() throws JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("RegionAssignmentStatus");

        assertNotNull(da);

        String[] fields = {
            "regionininterval", "region", "departuredateinterval", "status",
            "numberofassignments" };
        
        JSONArray regionAssignmentColumns = da.getAllOutputColumns();

        for (int i = 0; i < regionAssignmentColumns.length(); i++) {
            JSONObject columnObject = (JSONObject) regionAssignmentColumns.get(i);
            assertEquals(
                columnObject.getString(GenericDataAggregator.FIELD_ID),
                fields[i]);
            assertEquals(
                columnObject.getInt(GenericDataAggregator.COLUMN_SEQUENCE),
                i + 1);
        }
    }
    
    @Test()
    public void testRegionAssignmentStatusDADataWithFilter()
        throws BusinessRuleException, JSONException, DataAccessException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("RegionAssignmentStatus");

        assertNotNull(da);        

        int filterValue = TimeWindowFilterType.NEXT_4_HRS.toValue();
        
        Date startTime = DateUtil.convertTimeToSiteTime(new Date());
        DateTime endTimeValue = new DateTime(startTime);
        Date endTime = endTimeValue.plusHours(filterValue).toDate();

        JSONObject parameters = new JSONObject();
        parameters.put(
            GenericDataAggregator.START_TIME_PARAMETER,
            startTime);
        parameters.put(GenericDataAggregator.END_TIME_PARAMETER,
            endTime);

        da.setParameters(parameters);
        
        // Execute aggregator and assert
        JSONArray output = da.execute();
        for (int i = 0; i < output.length(); i++) {
            //0 because no assignment will fall in the 24 hour window.
            assertEquals(output.getJSONObject(i).getInt("numberofassignments"), 0);
            String intervalDesc = output.getJSONObject(i).getString("departuredateinterval");
            Integer interval = Integer.parseInt(intervalDesc.substring(5, 7));
            assertTrue(interval <= filterValue);
        }
        
        //Try again with updated assignment
        Assignment assignment = getAssignmentManager().get(-693L);
        Date ddt =  DateUtil.convertTimeToSiteTime(new Date());
        
        assignment.setDepartureDateTime(new Date(ddt.getTime() + 600000));
        getAssignmentManager().save(assignment);
        
        // Execute aggregator and assert
        output = da.execute();
        for (int i = 0; i < output.length(); i++) {
            JSONObject obj = output.getJSONObject(i);
            
            if (obj.getString("region").equals("Selection Region 15")
                && obj.getString("status").equals("Available")) {
                assertEquals(obj.getInt("numberofassignments"), 1);
            } else {
                //0 because no assignment will fall in the 24 hour window.
                assertEquals(obj.getInt("numberofassignments"), 0);
            }
            
            String intervalDesc = output.getJSONObject(i).getString("departuredateinterval");
            Integer intervalVal = Integer.parseInt(intervalDesc.substring(5, 7));
            assertTrue(intervalVal <= filterValue);
        }
        
    }
    
}
