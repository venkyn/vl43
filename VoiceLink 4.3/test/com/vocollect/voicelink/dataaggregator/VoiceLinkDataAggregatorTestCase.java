/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.BaseDAOTestCase;


/**
 * Base class for running DAO tests.
 * 
 * @author Dennis Doubleday
 */
public abstract class VoiceLinkDataAggregatorTestCase extends BaseDAOTestCase {
    
    /**
     * VL-specific Spring config files use in VL DAO unit tests.
     */
    public static final String[] VL_DAO_TEST_CONFIGS =
        new String [] {
        "classpath*:/applicationContext-voicelink*-service.xml",
        "classpath*:/applicationContext-voicelink*-dao.xml",
        "classpath*:/applicationContext-voicelink-dataaggregator.xml",
        "classpath*:/applicationContext-voicelink-resources.xml",
        "classpath*:/applicationContext-voicelink-hibernate-models.xml",            
        "classpath*:/applicationContext-voicelink-reportPrinters.xml",            
        "classpath*:/applicationContext-voicelink-security.xml"
        };
 
    protected static final String DATA_DIR = "data/dbunit/voicelink/";

    /**
     * @return the Spring config locations necessary for VL DAO tests.
     */
    public static String[] getDAOTestConfigLocations() {
        String[] config = new String[EPP_DAO_TEST_CONFIGS.length
                                     + VL_DAO_TEST_CONFIGS.length];
        System.arraycopy(EPP_DAO_TEST_CONFIGS, 0, config, 0, EPP_DAO_TEST_CONFIGS.length);
        System.arraycopy(VL_DAO_TEST_CONFIGS, 0, config, EPP_DAO_TEST_CONFIGS.length, 
            VL_DAO_TEST_CONFIGS.length);
        return config;
        
    }
 
    /**
     * @return the Spring config locations required for VL Archive DAO tests.
     */
    public static final String[] getVLArchiveDAOTestConfigLocations() {
        String[] eppArchiveConfigs = getArchiveDAOTestConfigLocations();
        String[] config = 
            new String[eppArchiveConfigs.length + VL_DAO_TEST_CONFIGS.length + 1];
        System.arraycopy(eppArchiveConfigs, 0, config, 0, eppArchiveConfigs.length);
        System.arraycopy(VL_DAO_TEST_CONFIGS, 0, config, eppArchiveConfigs.length, 
            VL_DAO_TEST_CONFIGS.length);
        config[config.length - 1] = 
            "classpath:/applicationContext-voicelink*-archive.xml";
        return config;
        
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return getDAOTestConfigLocations();
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        setUpSiteContext(true);
    }
    
}
