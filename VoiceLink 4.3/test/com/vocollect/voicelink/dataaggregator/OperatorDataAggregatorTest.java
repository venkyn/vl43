/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.dataaggregator;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.GenericDataAggregator;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.voicelink.core.model.BreakType;
import com.vocollect.voicelink.core.model.Operator;
import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorTeam;
import com.vocollect.voicelink.core.model.Region;
import com.vocollect.voicelink.core.service.BreakTypeManager;
import com.vocollect.voicelink.core.service.LaborManager;
import com.vocollect.voicelink.core.service.OperatorLaborManager;
import com.vocollect.voicelink.core.service.OperatorManager;
import com.vocollect.voicelink.core.service.OperatorTeamManager;
import com.vocollect.voicelink.core.service.RegionManager;
import com.vocollect.voicelink.core.service.SignOffManager;
import com.vocollect.voicelink.selection.service.AssignmentManager;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.dbunit.operation.DatabaseOperation;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.annotations.Test;

/**
 * 
 * 
 * @author khazra
 */
@Test(groups = { DA, FAST })
public class OperatorDataAggregatorTest extends VoiceLinkDataAggregatorTestCase {

    private DataAggregatorHandler dataAggregatorHandler = null;

    private OperatorManager operatorManager;

    private OperatorLaborManager operatorLaborManager;

    private LaborManager laborManager;

    private AssignmentManager assignmentManager;

    private BreakTypeManager breakTypeManager = null;

    private SignOffManager signOffManager = null;

    private RegionManager regionManager = null;

    private OperatorTeamManager operatorTeamManager;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_All.xml", DatabaseOperation.CLEAN_INSERT);
        adapter.handleFlatXmlResource(DATA_DIR
            + "VoiceLinkDataUnitTest_OperatorDataAggregator.xml",
            DatabaseOperation.CLEAN_INSERT);
        setUpSiteContext(false);
    }

    /**
     * @return the dataAggregatorHandler
     */
    @Test(enabled = false)
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * @param dataAggregatorHandler the dataAggregatorHandler to set
     */
    @Test(enabled = false)
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }

    /**
     * Getter for the operatorManager property.
     * @return OperatorManager value of the property
     */
    @Test(enabled = false)
    public OperatorManager getOperatorManager() {
        return operatorManager;
    }

    /**
     * Setter for the operatorManager property.
     * @param operatorManager the new operatorManager value
     */
    @Test(enabled = false)
    public void setOperatorManager(OperatorManager operatorManager) {
        this.operatorManager = operatorManager;
    }

    /**
     * Getter for the operatorLaborManager property.
     * @return OperatorLaborManager value of the property
     */
    @Test(enabled = false)
    public OperatorLaborManager getOperatorLaborManager() {
        return operatorLaborManager;
    }

    /**
     * Setter for the operatorLaborManager property.
     * @param operatorLaborManager the new operatorLaborManager value
     */
    @Test(enabled = false)
    public void setOperatorLaborManager(OperatorLaborManager operatorLaborManager) {
        this.operatorLaborManager = operatorLaborManager;
    }

    /**
     * Getter for the laborManager property.
     * @return LaborManager value of the property
     */
    @Test(enabled = false)
    public LaborManager getLaborManager() {
        return laborManager;
    }

    /**
     * Setter for the laborManager property.
     * @param laborManager the new laborManager value
     */
    @Test(enabled = false)
    public void setLaborManager(LaborManager laborManager) {
        this.laborManager = laborManager;
    }

    /**
     * Getter for the assignmentManager property.
     * @return AssignmentManager value of the property
     */
    @Test(enabled = false)
    public AssignmentManager getAssignmentManager() {
        return assignmentManager;
    }

    /**
     * Setter for the assignmentManager property.
     * @param assignmentManager the new assignmentManager value
     */
    @Test(enabled = false)
    public void setAssignmentManager(AssignmentManager assignmentManager) {
        this.assignmentManager = assignmentManager;
    }

    /**
     * @return the breakTypeManager
     */
    @Test(enabled = false)
    public BreakTypeManager getBreakTypeManager() {
        return breakTypeManager;
    }

    /**
     * @param breakTypeManager the breakTypeManager to set
     */
    @Test(enabled = false)
    public void setBreakTypeManager(BreakTypeManager breakTypeManager) {
        this.breakTypeManager = breakTypeManager;
    }

    /**
     * @return the signOffManager
     */
    @Test(enabled = false)
    public SignOffManager getSignOffManager() {
        return signOffManager;
    }

    /**
     * @param signOffManager the signOffManager to set
     */
    @Test(enabled = false)
    public void setSignOffManager(SignOffManager signOffManager) {
        this.signOffManager = signOffManager;
    }

    /**
     * @return the regionManager
     */
    @Test(enabled = false)
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * @param regionManager the regionManager to set
     */
    @Test(enabled = false)
    public void setRegionManager(RegionManager regionManager) {
        this.regionManager = regionManager;
    }

    /**
     * Getter for the operatorTeamManager property.
     * @return OperatorTeamManager value of the property
     */
    @Test(enabled = false)
    public OperatorTeamManager getOperatorTeamManager() {
        return operatorTeamManager;
    }

    /**
     * Setter for the operatorTeamManager property.
     * @param operatorTeamManager the new operatorTeamManager value
     */
    @Test(enabled = false)
    public void setOperatorTeamManager(OperatorTeamManager operatorTeamManager) {
        this.operatorTeamManager = operatorTeamManager;
    }

    @Test(enabled = true)
    public void testIdleAfterSignOn() throws BusinessRuleException,
        DataAccessException, JSONException {

        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("OperatorInfo");

        assertNotNull(da);
        // Time window for no valid results
        JSONObject parameters = new JSONObject();

        Date startTime = new DateTime("2013-07-31T11:00:00.000").toDate();
        Date endTime = new DateTime("2013-07-31T11:30:00.000").toDate();

        parameters.put("START_TIME", startTime);
        parameters.put("END_TIME", endTime);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();

        assertEquals(output.length(), 10);
        int count = 0;
        int index = 0;
        for (int i = 0; i < output.length(); i++) {
            if (output.getJSONObject(i).getInt(OperatorDataAggregator.FIELDS[5]) == 5) {
                count++;
                index = i;
            }
        }
        assertEquals(count, 1);
        assertEquals(output.getJSONObject(index).getString("operator"), "test3");
    }

    @Test(enabled = true)
    public void testBreakDuration() throws BusinessRuleException,
        DataAccessException, JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("OperatorInfo");

        assertNotNull(da);

        // Creating operator
        List<Operator> operators = this.operatorManager.getAll();

        // Creating labor data for sign on
        DateTime actionTime = new DateTime("2013-07-31T09:00:00.000");

        BreakType breakType = this.breakTypeManager.getAll().get(0);

        for (Operator operator : operators) {
            actionTime = actionTime.plusMinutes(60);
            laborManager.openOperatorLaborRecord(actionTime.minusMinutes(30)
                .toDate(), operator, OperatorLaborActionType.Selection);
            laborManager.openBreakLaborRecord(actionTime.toDate(), operator,
                breakType);
            laborManager.closeBreakLaborRecord(actionTime.plusMinutes(5)
                .toDate(), operator);
        }

        JSONObject parameters = new JSONObject();

        Date startTime = new DateTime("2013-07-31T11:00:00.000").toDate();
        Date endTime = new DateTime("2013-07-31T11:30:00.000").toDate();

        parameters.put("START_TIME", startTime);
        parameters.put("END_TIME", endTime);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();

        assertEquals(output.length(), 10);
        int count = 0;
        for (int i = 0; i < output.length(); i++) {
            if (output.getJSONObject(i).getInt(OperatorDataAggregator.FIELDS[4]) == 5) {
                count++;
            }
        }
        assertEquals(count, 1);
        assertEquals(output.getJSONObject(1).getInt(OperatorDataAggregator.FIELDS[4]), 5);
    }

    @Test(enabled = true)
    public void testOperatorTeams() throws BusinessRuleException,
        DataAccessException, JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("OperatorInfo");

        assertNotNull(da);

        // Creating operator
        Operator operator = this.operatorManager.getAll().get(0);

        // Creating Operator Teams and adding the operator to those teams

        Set<OperatorTeam> operatorTeams = new HashSet<OperatorTeam>();
        for (int i = 0; i < 3; i++) {
            OperatorTeam ot = new OperatorTeam();
            ot.setName("ot" + i);
            this.operatorTeamManager.save(ot);
            operatorTeams.add(ot);
        }

        operator.setOperatorTeams(operatorTeams);

        operatorManager.save(operator);

        //Set time window
        JSONObject parameters = new JSONObject();
        Date startTime = new DateTime("2013-07-31T11:00:00.000").toDate();
        Date endTime = new DateTime("2013-07-31T11:30:00.000").toDate();
        parameters.put("START_TIME", startTime);
        parameters.put("END_TIME", endTime);
        da.setParameters(parameters);
        
        JSONArray output = da.execute();

        assertEquals(output.length(), 10);

        List<OperatorTeam> otList = this.operatorTeamManager.getAll();

        String otArray = output.getJSONObject(0).getString(OperatorDataAggregator.FIELDS[10]);

        for (OperatorTeam ot : otList) {
            assertTrue(otArray.contains(ot.getName()));
        }

    }

    @Test(enabled = true)
    public void testOperatorCurrRegion() throws BusinessRuleException,
        DataAccessException, JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("OperatorInfo");

        assertNotNull(da);

        // Creating another operator having current region
        Operator operator = this.operatorManager.getAll().get(0);
        Region currRegion = getRegionManager().getAll().get(0);
        operator.setCurrentRegion(currRegion);

        //Set time window
        JSONObject parameters = new JSONObject();
        Date startTime = new DateTime("2013-07-31T11:00:00.000").toDate();
        Date endTime = new DateTime("2013-07-31T11:30:00.000").toDate();
        parameters.put("START_TIME", startTime);
        parameters.put("END_TIME", endTime);
        da.setParameters(parameters);
        
        // Execute aggregator and assert
        JSONArray output = da.execute();

        assertEquals(output.length(), 10);
        assertEquals(output.getJSONObject(0).getString(OperatorDataAggregator.FIELDS[9]),
            currRegion.getName());
    }

    @SuppressWarnings("deprecation")
    @Test(enabled = true)
    public void testSignOnTime() throws BusinessRuleException,
        DataAccessException, JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("OperatorInfo");

        assertNotNull(da);

        // Creating operator
        Operator operator = this.operatorManager.listAllOrderByName().get(0);
        Date signOnTime = DateUtil.convertTimeToSiteTime(operator.getSignOnTime());
        
        operator.setSignOffTime(null);
        this.operatorManager.save(operator);

        //Set time window
        JSONObject parameters = new JSONObject();
        Date startTime = new DateTime("2013-07-31T11:00:00.000").toDate();
        Date endTime = new DateTime("2013-07-31T11:30:00.000").toDate();
        parameters.put("START_TIME", startTime);
        parameters.put("END_TIME", endTime);
        da.setParameters(parameters);
        
        JSONArray output = da.execute();

        assertEquals(output.length(), 10);
        assertTrue(output.getJSONObject(0).getInt(OperatorDataAggregator.FIELDS[3]) == Integer
            .parseInt(String.format("%02d%02d", signOnTime.getHours(),
                signOnTime.getMinutes())));
        assertTrue(output.getJSONObject(9).getInt(
            OperatorDataAggregator.FIELDS[3]) == GenericDataAggregator.DEFAULT_TIME_VAL);
    }

    @Test(enabled = true)
    public void testIdleBeforeBreak() throws BusinessRuleException,
        DataAccessException, JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("OperatorInfo");

        assertNotNull(da);

        // Creating operator
        List<Operator> operators = this.operatorManager.getAll();

        // Creating labor data for sign on
        DateTime actionTime = new DateTime("2013-07-31T09:00:00.000");

        BreakType breakType = this.breakTypeManager.getAll().get(0);

        for (Operator operator : operators) {
            actionTime = actionTime.plusMinutes(60);
            laborManager.openOperatorLaborRecord(actionTime.minusMinutes(30)
                .toDate(), operator, OperatorLaborActionType.Selection);
            laborManager.openBreakLaborRecord(actionTime.toDate(), operator,
                breakType);
            laborManager.closeBreakLaborRecord(actionTime.plusMinutes(5)
                .toDate(), operator);
        }

        JSONObject parameters = new JSONObject();

        Date startTime = new DateTime("2013-07-31T11:00:00.000").toDate();
        Date endTime = new DateTime("2013-07-31T11:30:00.000").toDate();

        parameters.put("START_TIME", startTime);
        parameters.put("END_TIME", endTime);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();

        assertEquals(output.length(), 10);
        int count = 0;
        for (int i = 0; i < output.length(); i++) {
            if (output.getJSONObject(i).getInt(OperatorDataAggregator.FIELDS[7]) == 30) {
                count++;
            }
        }
        assertEquals(count, 1);
        assertEquals(output.getJSONObject(1).getInt(OperatorDataAggregator.FIELDS[7]), 30);
    }

    @Test(enabled = true)
    public void testIdleAfterBreak() throws BusinessRuleException,
        DataAccessException, JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("OperatorInfo");

        assertNotNull(da);

        // Creating operator
        List<Operator> operators = this.operatorManager.getAll();

        // Creating labor data for sign on
        DateTime actionTime = new DateTime("2013-07-31T09:00:00.000");

        BreakType breakType = this.breakTypeManager.getAll().get(0);

        int i = 0;
        for (Operator operator : operators) {
            i++;
            actionTime = actionTime.plusMinutes(60);
            laborManager.openOperatorLaborRecord(actionTime.minusMinutes(30)
                .toDate(), operator, OperatorLaborActionType.Selection);
            laborManager.openBreakLaborRecord(actionTime.toDate(), operator,
                breakType);
            laborManager.closeBreakLaborRecord(actionTime.plusMinutes(5)
                .toDate(), operator);
            if (i == 2) {
                laborManager.openOperatorLaborRecord(actionTime.plusMinutes(35)
                    .toDate(), operator, OperatorLaborActionType.SignOff);
            }
        }

        JSONObject parameters = new JSONObject();

        Date startTime = new DateTime("2013-07-31T11:00:00.000").toDate();
        Date endTime = new DateTime("2013-07-31T11:30:00.000").toDate();

        parameters.put("START_TIME", startTime);
        parameters.put("END_TIME", endTime);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();

        assertEquals(output.length(), 10);
        int count = 0;
        for (i = 0; i < output.length(); i++) {
            if (output.getJSONObject(i).getInt(OperatorDataAggregator.FIELDS[8]) == 30) {
                count++;
            }
        }
        assertEquals(count, 1);
        assertEquals(output.getJSONObject(1).getInt("idleafterbreak"), 30);
    }

    @Test(enabled = true)
    public void testIdleBeforeSignOff() throws BusinessRuleException,
        DataAccessException, JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("OperatorInfo");

        assertNotNull(da);

        // Creating operator
        List<Operator> operators = this.operatorManager.getAll();

        // Creating labor data for sign on
        DateTime actionTime = new DateTime("2013-07-31T08:05:00.000");


        for (Operator operator : operators) {
            actionTime = actionTime.plusMinutes(60);
            laborManager.openOperatorLaborRecord(actionTime.toDate(), operator,
                OperatorLaborActionType.SignOff);
        }

        JSONObject parameters = new JSONObject();

        Date startTime = new DateTime("2013-07-31T11:00:00.000").toDate();
        Date endTime = new DateTime("2013-07-31T11:30:00.000").toDate();

        parameters.put("START_TIME", startTime);
        parameters.put("END_TIME", endTime);

        da.setParameters(parameters);

        // Execute aggregator and assert
        JSONArray output = da.execute();

        assertEquals(output.length(), 10);
        int count = 0;
        for (int i = 0; i < output.length(); i++) {
            if (output.getJSONObject(i).getInt(OperatorDataAggregator.FIELDS[6]) == 5) {
                count++;
            }
        }
        assertEquals(count, 1);
    }
    
    @Test()
    public void testOperatorAggregatorColumnSequence() throws JSONException {
        GenericDataAggregator da = this.dataAggregatorHandler
            .getDataAggregatorByName("OperatorInfo");

        assertNotNull(da);

        JSONArray operatorColumns = da.getAllOutputColumns();

        for (int i = 0; i < operatorColumns.length(); i++) {
            JSONObject columnObject = (JSONObject) operatorColumns.get(i);
            assertEquals(
                columnObject.getString(GenericDataAggregator.FIELD_ID),
                OperatorDataAggregator.FIELDS[i]);
            assertEquals(
                columnObject.getInt(GenericDataAggregator.COLUMN_SEQUENCE),
                i + 1);
        }
    }
}
