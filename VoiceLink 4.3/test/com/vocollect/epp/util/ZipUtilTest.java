/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.LOGGING;
import static com.vocollect.epp.test.TestGroups.UNIT;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;


/**
 * Test class for the <code>LogFilesAction</code> class.
 *
 * @author Adam Stein
 */
@Test(groups = { FAST, UNIT, LOGGING }, enabled = true)
public class ZipUtilTest {

    /**
     *
     */
    private static final String FILE11 = "file11\u00FF";
    private static final int BUFFER = 1024;
    /**
     * Tests the static void zipData(Map inputData, OutputStream out)class method
     * from ZipUtil.
     * @throws Exception when there is an issue zipping the file.
     */
    public void testZipData() throws Exception {
        Map<String, InputStream> logMap = new HashMap<String, InputStream>();

        //This map is used to verify the zipping
        Map<String, String> testMap = new HashMap<String, String>();

        String file1Content = " This is a simple zip test ";
        String file2Content = " This kind of test special characters. "
                              + "1. \u00E0 2. \u00F2 3. \u00E8 4. \u00E1 5. \u00C8";
        String file3Content = " One more test \u00E8 4. \u00E1 5. \u00C8";

        InputStream bais1 = new ByteArrayInputStream(file1Content.getBytes());
        InputStream bais2 = new ByteArrayInputStream(file2Content.getBytes());
        InputStream bais3 = new ByteArrayInputStream(file3Content.getBytes());

        logMap.put(FILE11, bais1);
        logMap.put("file22", bais2);
        logMap.put("file33", bais3);

        testMap.put(FILE11, file1Content);
        testMap.put("file22", file2Content);
        testMap.put("file33", file3Content);

        ByteArrayOutputStream zipBaos = new ByteArrayOutputStream();
        ZipUtil.zipData(logMap, zipBaos);

        Boolean testResult = verifyZipByUnzip(zipBaos, testMap);
        assertTrue("DownloadFiles test - Zipping data", testResult.equals(Boolean.TRUE));
    }

    /**
     * Validate the zip data.
     * @param zipBaos zipped ByteArrayInputStream
     * @param testMap - Contains the original data which was zipped
     * @return if verifyZipByUnzip is successful then return TRUE else FALSE
     */
    private Boolean verifyZipByUnzip(OutputStream zipBaos, Map<String, String> testMap) {
        //Logic to Uncompress the data
        ByteArrayOutputStream baos = (ByteArrayOutputStream) zipBaos;
        ZipInputStream zis = new ZipInputStream(new ByteArrayInputStream(baos.toByteArray()));

        ZipEntry entry = null;
        try {
            while ((entry = zis.getNextEntry()) != null) {
                int count;
                byte[] data = new byte[BUFFER];
                ByteArrayOutputStream dest = new ByteArrayOutputStream();
                while ((count = zis.read(data, 0, BUFFER)) != -1) {
                   dest.write(data, 0, count);
                }

                Iterator it = testMap.entrySet().iterator();
                Boolean foundEntry = Boolean.FALSE;
                while (it.hasNext()) {
                    Map.Entry pairs = (Map.Entry) it.next();
                    if (entry.getName().equals(pairs.getKey().toString())) {
                        String origString = new String(dest.toByteArray());
                        foundEntry = Boolean.TRUE;
                        if (!(origString.equals(pairs.getValue().toString()))) {
                            return Boolean.FALSE;
                        }
                    }
                }
                if (foundEntry.equals(Boolean.FALSE)) {
                    return Boolean.FALSE;
                }
                dest.flush();
                dest.close();
            }
            zis.closeEntry();
            zis.close();
        } catch (IOException e) {
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }





}
