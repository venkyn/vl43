/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.dao.BaseDAOTestCase;
import com.vocollect.epp.dao.RoleDAO;
import com.vocollect.epp.dao.RoleDAOTest;
import com.vocollect.epp.dao.UserDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.UserProperty;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.test.DbUnitAdapter;

import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * @author dgold
 *
 */
public class SiteContextTest extends BaseDAOTestCase {


    /**
     *
     */
    private static final Long USER_SITE_ID = 3L;

    /**
     *
     */
    private static final Long DEFAULT_SITE_ID = -1L;

    /**
     *
     */
    private static final String SITE_CONTEXT_TEST_USER = "site_context_test_user";

    private UserDAO userDAO = null;

    private RoleDAO roleDAO = null;

    private DbUnitAdapter adapter = new DbUnitAdapter();

    private SiteManager siteManager;

    private UserManager userManager = null;

    /**
     * {@inheritDoc}
     * @see junit.framework.TestCase#setUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
        setUpSiteContext(true);

    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getAllSites()}.
     * @throws DataAccessException
     */
    @Test(enabled = true)
    public void testGetAllSites() throws DataAccessException {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        assertEquals(siteContext.getAllSites().size(), 5); // 4 in the dataset, plus the default...

    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getSite(java.lang.Long)}.
     * @throws DataAccessException
     */
    @Test(enabled = true)
    public void testGetSiteLong() throws DataAccessException {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Site site = siteContext.getAllSites().get(0);

        Site oughtaBeTheSame = siteContext.getSite(site.getId());
        assertNotNull(oughtaBeTheSame);
        assertEquals(oughtaBeTheSame.getId(), site.getId());
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#refreshSites()}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testRefreshSites() throws Exception {

        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        assertEquals(siteContext.getAllSites().size(), 5);
        adapter.resetInstallationData();
        //handleFlatXmlResource calls refreshSites...
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest2.xml", DatabaseOperation.REFRESH);
        assertEquals(siteContext.getAllSites().size(), 6);
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
        siteContext.refreshSites();
        assertEquals(siteContext.getAllSites().size(), 5);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#updateUser(com.vocollect.epp.model.User, java.lang.Long)}.
     */
    @Test(enabled = false)
    public void testUpdateUser() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getCurrentSite()}.
     * @throws DataAccessException
     */
    @Test(enabled = true)
    public void testGetCurrentSite() throws DataAccessException {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        assertEquals(siteContext.getCurrentSite().getId(), DEFAULT_SITE_ID);

        siteContext.setCurrentSite(USER_SITE_ID);
        assertNotNull(siteContext.getCurrentSite());
        assertEquals(siteContext.getCurrentSite().getId(), USER_SITE_ID);

    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#restorePriorSite()}.
     * @throws DataAccessException
     */
    @Test(enabled = true)
    public void testRestorePriorSite() throws DataAccessException {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        siteContext.setCurrentSite(DEFAULT_SITE_ID);
        siteContext.setCurrentSite(USER_SITE_ID);
        siteContext.restorePriorSite();
        assertEquals(siteContext.getCurrentSite().getId(), DEFAULT_SITE_ID);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getTagBySiteId(java.lang.Long)}.
     */
    @Test(enabled = true)
    public void testGetTagBySiteId() {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Tag tag = siteContext.getTagBySiteId(USER_SITE_ID);

        assertEquals(tag.getId(), Long.valueOf("4"));

    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#isInCurrentSiteMode()}.
     */
    @Test(enabled = true)
    public void testIsInCurrentSiteMode() {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        assertTrue(siteContext.isInCurrentSiteMode());
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#isInAllSiteMode()}.
     */
    @Test(enabled = true)
    public void testIsInAllSiteMode() {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        assertTrue(siteContext.isInCurrentSiteMode());
        assertFalse(siteContext.isInAllSiteMode());
        siteContext.setToAllSiteMode();
        assertFalse(siteContext.isInCurrentSiteMode());
        assertTrue(siteContext.isInAllSiteMode());
        siteContext.setToCurrentSiteMode();
        assertTrue(siteContext.isInCurrentSiteMode());
        assertFalse(siteContext.isInAllSiteMode());
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#isInSystemMode()}.
     */
    @Test(enabled = true)
    public void testIsInSystemMode() {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        assertTrue(siteContext.isInCurrentSiteMode());
        assertFalse(siteContext.isInSystemMode());
        siteContext.setToSystemMode();
        assertFalse(siteContext.isInCurrentSiteMode());
        assertTrue(siteContext.isInSystemMode());
        siteContext.setToCurrentSiteMode();
        assertTrue(siteContext.isInCurrentSiteMode());
        assertFalse(siteContext.isInSystemMode());
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getSites(com.vocollect.epp.model.Taggable)}.
     * @throws Exception
     * @see {@link #testSetSitesTaggableListOfSite()}
     */
    @Test(enabled = false)
    public void testGetSitesTaggable() throws Exception {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getSiteIds(com.vocollect.epp.model.Taggable)}.
     * @throws Exception
     * @see {@link #testSetSitesTaggableListOfSite()}
     */
    @Test(enabled = false)
    public void testGetSiteIdsTaggable() throws Exception {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#isInSite(com.vocollect.epp.model.Taggable, com.vocollect.epp.model.Site)}.
     * @see {@link #testSetSitesTaggableListOfSite()}
     */
    @Test(enabled = false)
    public void testIsInSite() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#setSites(com.vocollect.epp.model.Taggable, java.util.List)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testSetSitesTaggableListOfSite() throws Exception {
        Long userSiteId = USER_SITE_ID;
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Site userSite = siteContext.getSite(userSiteId);
        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);
        assertNull(siteContext.getSites(user));
        LinkedList<Site> sites = new LinkedList<Site>();
        sites.add(userSite);
        siteContext.setSites(user, sites);
        assertEquals(siteContext.getSites(user).size(), 1);
        assertEquals(siteContext.getSites(user).get(0), userSite);
        assertTrue(siteContext.isInSite(user, userSite));
        userDAO.save(user);
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#setContextSites(com.vocollect.epp.model.Taggable)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testSetContextSites() throws Exception {
        Long userSiteId = USER_SITE_ID;
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Site userSite = siteContext.getSite(userSiteId);
        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);

        siteContext.addSite(user, userSite);

        siteContext.clearAllSites();

        siteContext.setContextSites(user);

        assertEquals(siteContext.getSitesInContext(), siteContext.getSites(user));
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#setSite(com.vocollect.epp.model.Taggable, com.vocollect.epp.model.Site)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testSetSiteTaggableSite() throws Exception {
        Long userSiteId = USER_SITE_ID;
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Site userSite = siteContext.getSite(userSiteId);
        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);
        assertNull(siteContext.getSites(user));
        siteContext.setSite(user, userSite);
        assertEquals(siteContext.getSites(user).size(), 1);
        assertEquals(siteContext.getSites(user).get(0), userSite);
        assertTrue(siteContext.isInSite(user, userSite));
        getUserDAO().save(user);
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#setSite(com.vocollect.epp.model.Taggable, java.lang.Long)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testSetSiteTaggableLong() throws Exception {
        Long userSiteId = USER_SITE_ID;
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Site userSite = siteContext.getSite(userSiteId);
        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);
        assertNull(siteContext.getSites(user));
        siteContext.setSite(user, userSiteId);
        assertEquals(siteContext.getSites(user).size(), 1);
        assertEquals(siteContext.getSites(user).get(0), userSite);
        assertTrue(siteContext.isInSite(user, userSite));
        assertNotNull(siteContext.getSiteIds(user));
        assertEquals(siteContext.getSiteIds(user).get(0), userSiteId);
        userSiteId = 5L;
        siteContext.setSite(user, userSiteId);
        userSite = siteContext.getSite(userSiteId);
        assertEquals(siteContext.getSites(user).size(), 1);
        assertEquals(siteContext.getSites(user).get(0), userSite);
        assertTrue(siteContext.isInSite(user, userSite));
        assertNotNull(siteContext.getSiteIds(user));
        assertEquals(siteContext.getSiteIds(user).get(0), userSiteId);
        userDAO.save(user);
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#addSites(com.vocollect.epp.model.Taggable, java.util.List)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testAddSites() throws Exception {
        Long userSiteId = USER_SITE_ID;
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Site userSite = siteContext.getSite(userSiteId);
        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);
        assertNull(siteContext.getSites(user));
        siteContext.setSite(user, userSiteId);

        LinkedList<Site> sites = new LinkedList<Site>();
        userSiteId = 5L;
        sites.add(siteContext.getSite(userSiteId));
        userSiteId = 7L;
        sites.add(siteContext.getSite(userSiteId));

        siteContext.addSites(user, sites);

        List<Site> userSites = siteContext.getSites(user);

        assertEquals(userSites.size(), 3);
        sites.add(userSite);
        assertTrue(userSites.containsAll(sites));
        assertTrue(sites.containsAll(userSites));
        userDAO.save(user);
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#addSitesById(com.vocollect.epp.model.Taggable, java.util.List)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testAddSitesById() throws Exception {
        Long userSiteId = USER_SITE_ID;
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Site userSite = siteContext.getSite(userSiteId);
        List<Site> sites = new LinkedList<Site>();
        List<Long> siteIds = new LinkedList<Long>();
        sites.add(userSite);
        siteIds.add(userSite.getId());
        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);

        siteContext.addSitesById(user, siteIds);

        assertEquals(siteContext.getSites(user), sites);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#addSite(com.vocollect.epp.model.Taggable, com.vocollect.epp.model.Site)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testAddSiteTaggableSite() throws Exception {
        Long userSiteId = USER_SITE_ID;
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Site userSite = siteContext.getSite(userSiteId);
        List<Site> sites = new LinkedList<Site>();
        sites.add(userSite);
        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);
        assertNull(siteContext.getSites(user));
        siteContext.setSite(user, userSite);

        userSiteId = 5L;
        userSite = siteContext.getSite(userSiteId);
        siteContext.addSite(user, userSite);
        sites.add(userSite);

        List<Site> userSites = siteContext.getSites(user);

        assertEquals(userSites.size(), 2);

        sites.add(userSite);
        assertTrue(userSites.containsAll(sites));
        assertTrue(sites.containsAll(userSites));
        userDAO.save(user);
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);

    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#addSite(com.vocollect.epp.model.Taggable, java.lang.Long)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testAddSiteTaggableLong() throws Exception {
        Long userSiteId = USER_SITE_ID;
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Site userSite = siteContext.getSite(userSiteId);
        List<Site> sites = new LinkedList<Site>();
        sites.add(userSite);
        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);
        assertNull(siteContext.getSites(user));
        siteContext.setSite(user, userSiteId);

        userSiteId = 5L;
        userSite = siteContext.getSite(userSiteId);
        siteContext.addSite(user, userSiteId);
        sites.add(userSite);

        List<Site> userSites = siteContext.getSites(user);

        assertEquals(userSites.size(), 2);

        sites.add(userSite);
        assertTrue(userSites.containsAll(sites));
        assertTrue(sites.containsAll(userSites));
        userDAO.save(user);
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#addSiteToContext(com.vocollect.epp.model.Site)}.
     * @throws Exception
     * @throws BusinessRuleException
     */
    @Test(enabled = true)
    public void testAddSiteToContext() throws BusinessRuleException, Exception {
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        int siteCount = siteContext.getSiteCountInContext();
        Site newSite = new Site();
        newSite.setName("new test site");
        getSiteManager().save(newSite);
        siteContext.refreshSites();
        assertEquals(siteContext.getSiteCountInContext(), siteCount);

        siteContext.addSiteToContext(newSite);

        assertEquals(siteContext.getSiteCountInContext(), siteCount + 1);
        assertEquals(siteContext.getSiteInContext(newSite.getId()), newSite);

        getSiteManager().save(newSite);

        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#removeSites(com.vocollect.epp.model.Taggable, java.util.List)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testRemoveSites() throws Exception {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);

        Long userSiteId = USER_SITE_ID;
        Site userSite = siteContext.getSite(userSiteId);
        List<Site> sites = new LinkedList<Site>();
        sites.add(userSite);
        
        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);
        User gottenUser = userManager.get(user.getId());
        
        
        assertNull(siteContext.getSites(user));
        siteContext.setSite(user, userSiteId);
        
        userSiteId = 5L;
        userSite = siteContext.getSite(userSiteId);
        siteContext.addSite(user, userSiteId);
        sites.add(userSite);

        List<Site> userSites = siteContext.getSites(user);

        siteContext.removeSites(user, userSites);

        assertTrue(siteContext.getSites(user).isEmpty());

        siteContext.addSite(user, siteContext.getSite(userSiteId));
        userSiteId = USER_SITE_ID;

        assertEquals(siteContext.getSites(user).size(), 1);

        userDAO.save(user);
		
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);

    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#removeSitesById(com.vocollect.epp.model.Taggable, java.util.List)}.
     * @see #testRemoveSiteTaggableSite
     */
    @Test(enabled = false)
    public void testRemoveSitesById() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#removeSite(com.vocollect.epp.model.Taggable, com.vocollect.epp.model.Site)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testRemoveSiteTaggableSite() throws Exception {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);

        Long userSiteId = USER_SITE_ID;
        Site userSite = siteContext.getSite(userSiteId);
        List<Site> sites = new LinkedList<Site>();
        List<Long> siteIds = new LinkedList<Long>();
        sites.add(userSite);
        siteIds.add(userSiteId);
        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);
        assertNull(siteContext.getSites(user));
        siteContext.setSite(user, userSiteId);

        userSiteId = 5L;
        userSite = siteContext.getSite(userSiteId);
        siteContext.addSite(user, userSiteId);
        sites.add(userSite);

        assertEquals(siteContext.getSites(user).size(), 2);

        siteContext.removeSite(user, userSite);

        assertEquals(siteContext.getSites(user).size(), 1);

        userSiteId = USER_SITE_ID;
        siteContext.addSite(user, userSite);

        assertEquals(siteContext.getSites(user).size(), 2);

        siteContext.removeSitesById(user, siteIds);
        assertEquals(siteContext.getSites(user).size(), 1);

        userDAO.save(user);

        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);

    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#removeSite(com.vocollect.epp.model.Taggable, java.lang.Long)}.
     * @see #testRemoveSiteTaggableSite()
     */
    @Test(enabled = false)
    public void testRemoveSiteTaggableLong() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getSitesInContext()}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testGetSites() throws Exception {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        siteContext.refreshSites();
        assertNotNull(siteContext);
        List<Site> sites = siteContext.getSitesInContext();
        assertEquals(sites.size(), 1);  // only the default site
        assertNotNull(siteContext.getSite(DEFAULT_SITE_ID));

        siteContext.addSiteToContext(siteContext.getSite(5L));

        sites = siteContext.getSitesInContext();
        assertEquals(sites.size(), 2);
        assertNotNull(siteContext.getSite(DEFAULT_SITE_ID));
        assertNotNull(siteContext.getSite(5L));

    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#removeSiteFromContext(com.vocollect.epp.model.Site)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testRemoveSiteFromContextSite() throws Exception {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        List<Site> sites = siteContext.getSitesInContext();
        assertEquals(sites.size(), 1);  // only the default site
        assertNotNull(siteContext.getSite(DEFAULT_SITE_ID));

        siteContext.addSiteToContext(siteContext.getSite(5L));

        sites = siteContext.getSitesInContext();
        assertEquals(sites.size(), 2);
        assertNotNull(siteContext.getSite(DEFAULT_SITE_ID));
        assertNotNull(siteContext.getSite(5L));
        siteContext.removeSiteFromContext(siteContext.getSite(DEFAULT_SITE_ID));
        sites = siteContext.getSitesInContext();
        assertEquals(sites.size(), 1);  // only the default site
        assertNotNull(siteContext.getSite(5L));
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#removeSiteFromContext(java.lang.Long)}.
     * @throws DataAccessException
     * @see #testRemoveSiteFromContextSite
     */
    @Test(enabled = false)
    public void testRemoveSiteFromContextLong() throws DataAccessException {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getSiteIdsInContext()}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testGetSiteIds() throws Exception {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        List<Site> sites = siteContext.getSitesInContext();
        assertEquals(sites.size(), 1);  // only the default site
        assertNotNull(siteContext.getSite(DEFAULT_SITE_ID));

        siteContext.addSiteToContext(siteContext.getSite(5L));

        List<Long> siteIds = siteContext.getSiteIdsInContext();

        assertEquals(siteIds.size(), 2);

        assertTrue(siteIds.contains(DEFAULT_SITE_ID));
        assertTrue(siteIds.contains(5L));
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#changeCurrentSite(com.vocollect.epp.model.Site)}.
     * @throws DataAccessException
     */
    @Test(enabled = true)
    public void testChangeCurrentSiteSite() throws DataAccessException {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Site oldSite = siteContext.getCurrentSite();

        siteContext.changeCurrentSite(siteContext.getSite(5L));

        assertEquals(siteContext.getCurrentSite(), siteContext.getSite(5L));

        siteContext.restorePriorSite();

        assertEquals(siteContext.getCurrentSite(), oldSite);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#changeCurrentSite(java.lang.Long)}.
     * @see #testChangeCurrentSiteSite()
     */
    @Test(enabled = false)
    public void testChangeCurrentSiteLong() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#changeCurrentSite(java.lang.String)}.
     * @throws DataAccessException
     */
    @Test(enabled = true)
    public void testChangeCurrentSiteString() throws DataAccessException {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Site oldSite = siteContext.getCurrentSite();
        Site newSite = siteContext.getSite(5L);

        siteContext.changeCurrentSite(newSite.getName());

        assertEquals(siteContext.getCurrentSite(), siteContext.getSite(5L));

        siteContext.restorePriorSite();

        assertEquals(siteContext.getCurrentSite(), oldSite);

    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getSite(com.vocollect.epp.model.Taggable)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testGetSiteTaggable() throws Exception {
        Long userSiteId = USER_SITE_ID;
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);

        Site userSite = siteContext.getSite(userSiteId);

        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);
        siteContext.setSite(user, USER_SITE_ID);
        // verify that this user has exactly one site

        assertEquals(siteContext.getSites(user).size(), 1L);
        assertEquals(siteContext.getSites(user).get(0).getId(), userSiteId);

        assertEquals(siteContext.getSite(user).getId(), userSiteId);

        userDAO.save(user);

        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#setToSystemMode()}.
     * @see #testIsInSystemMode()
     */
    @Test(enabled = false)
    public void testSetToSystemMode() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#setToAllSiteMode()}.
     * @see #testIsInAllSiteMode()
     */
    @Test(enabled = false)
    public void testSetToAllSiteMode() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#setToCurrentSiteMode()}.
     * @see #testIsInCurrentSiteMode()
     */
    @Test(enabled = false)
    public void testSetToCurrentSiteMode() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getCurrentSiteName()}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testGetCurrentSiteName() throws Exception {
        Long userSiteId = USER_SITE_ID;
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);

        Site currentSite = siteContext.getSite(DEFAULT_SITE_ID);
        siteContext.setCurrentSite(currentSite);

        assertEquals(siteContext.getCurrentSite(), currentSite);
        assertEquals(siteContext.getCurrentSiteName(), currentSite.getName());

        siteContext.changeCurrentSite(userSiteId);
        currentSite = siteContext.getSite(userSiteId);

        assertEquals(siteContext.getCurrentSite(), currentSite);
        assertEquals(siteContext.getCurrentSiteName(), currentSite.getName());
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#isInCurrentSite(com.vocollect.epp.model.Taggable)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testIsInCurrentSite() throws Exception {
        Long userSiteId = USER_SITE_ID;
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        Site userSite = siteContext.getSite(userSiteId);

        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);

        siteContext.setCurrentSite(DEFAULT_SITE_ID);

        siteContext.setSite(user, userSite);

        assertFalse(siteContext.isInCurrentSite(user));

        siteContext.setCurrentSite(userSiteId);

        assertTrue(siteContext.isInCurrentSite(user));

        userDAO.save(user);

        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getSiteCountInContext()}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testGetSiteCount() throws Exception {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        assertEquals(siteContext.getSiteCountInContext(), 1);
        siteContext.removeSiteFromContext(DEFAULT_SITE_ID);
        assertEquals(siteContext.getSiteCountInContext(), 0);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#clearAllSites()}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testClearAllSites() throws Exception {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);
        assertEquals(siteContext.getSiteCountInContext(), 1);
        siteContext.clearAllSites();
        assertEquals(siteContext.getSiteCountInContext(), 0);
        siteContext.addSiteToContext(siteContext.getSite(DEFAULT_SITE_ID));
        assertEquals(siteContext.getSiteCountInContext(), 1);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getDefaultSiteForUser(com.vocollect.epp.model.User)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testGetDefaultSiteForUser() throws Exception {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);

        Long userSiteId = USER_SITE_ID;
        Site userSite = siteContext.getSite(userSiteId);

        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);

        siteContext.setCurrentSite(DEFAULT_SITE_ID);

        siteContext.setSite(user, userSite);
        userDAO.save(user);

        assertNotNull(siteContext.getDefaultSiteForUser(user));
        assertEquals(siteContext.getDefaultSiteForUser(user), userSite);
        userDAO.save(user);

        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#isFilterBySite()}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testIsFilterBySite() throws Exception {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);

        Long userSiteId = USER_SITE_ID;
        Site userSite = siteContext.getSite(userSiteId);

        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);


        siteContext.setSite(user, userSite);
        userDAO.save(user);


        siteContext.clearAllSites();
        siteContext.addSiteToContext(userSite);

        siteContext.setFilterBySite(true);
        siteContext.setHasAllSiteAccess(false);

        assertTrue(siteContext.isFilterBySite());
        siteContext.setCurrentSite(DEFAULT_SITE_ID);

        // Currently, we have tags that say default & user site.

        User retreival = getUserManager().findByName(user.getName());
        assertNull(retreival);

        // The DAO gets the object you've ID'd if you have access to the site.
        retreival = userDAO.get(user.getId());
        assertNotNull(retreival);
        siteContext.clearAllSites();
        siteContext.addSiteToContext(siteContext.getSite(DEFAULT_SITE_ID));
        retreival = null;
        try {
            retreival = userDAO.get(user.getId());
        } catch (DataAccessException dae) {
            // Expected
        }
        assertNull(retreival);

        // Set up so we can retrieve the user
        siteContext.clearAllSites();
        siteContext.addSiteToContext(userSite);
        siteContext.setCurrentSite(userSite);


        siteContext.setFilterBySite(false);
        assertFalse(siteContext.isFilterBySite());
        retreival = getUserManager().findByName(user.getName());
        assertNotNull(retreival);
        retreival = getUserManager().get(user.getId());
        assertNotNull(retreival);

        siteContext.setFilterBySite(true);
        assertTrue(siteContext.isFilterBySite());

        siteContext.clearAllSites();
        siteContext.setCurrentSite(DEFAULT_SITE_ID);

        siteContext.addSiteToContext(userSite);
        siteContext.setCurrentSite(userSite);

        retreival = getUserManager().findByName(user.getName());
        assertNotNull(retreival);
        retreival = getUserManager().get(user.getId());
        assertNotNull(retreival);
        siteContext.setHasAllSiteAccess(true);

        userDAO.save(user);

        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#setFilterBySite(boolean)}.
     * @see #testIsFilterBySite
     */
    @Test(enabled = false)
    public void testSetFilterBySite() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#isHasAllSiteAccess()}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testIsHasAllSiteAccess() throws Exception {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);

        Long userSiteId = USER_SITE_ID;
        Site userSite = siteContext.getSite(userSiteId);

        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);


        siteContext.setSite(user, userSite);
        userDAO.save(user);


        siteContext.setCurrentSite(DEFAULT_SITE_ID);
        siteContext.clearAllSites();
        siteContext.addSiteToContext(userSite);

        siteContext.setFilterBySite(true);
        siteContext.setHasAllSiteAccess(true);
        assertTrue(siteContext.isHasAllSiteAccess());
        userDAO.get(user.getId());
        // in the wrong site
        User retreival = getUserManager().findByName(user.getName());
        assertNull(retreival);

        // Set to the right site
        siteContext.setCurrentSite(userSite);
        retreival = getUserManager().findByName(user.getName());
        assertNotNull(retreival);
        userDAO.get(user.getId());


        siteContext.setHasAllSiteAccess(false);
        siteContext.clearAllSites();
        siteContext.setCurrentSite(DEFAULT_SITE_ID);
        assertFalse(siteContext.isHasAllSiteAccess());
        retreival = getUserManager().findByName(user.getName());
        assertNull(retreival);
        try {
            retreival = userDAO.get(user.getId());
        } catch (DataAccessException ex) {
            // expected
        }
        assertNull(retreival);


        siteContext.setHasAllSiteAccess(true);
        assertTrue(siteContext.isHasAllSiteAccess());

        siteContext.clearAllSites();
        siteContext.addSiteToContext(userSite);
        siteContext.setCurrentSite(userSite);

        retreival = getUserManager().findByName(user.getName());
        assertNotNull(retreival);
        retreival = userDAO.get(user.getId());

        userDAO.save(user);

        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#setHasAllSiteAccess(boolean)}.
     * @see #testIsHasAllSiteAccess()
     */
    @Test(enabled = false)
    public void testSetHasAllSiteAccess() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getDefaultSite(com.vocollect.epp.model.User)}.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testGetDefaultSite() throws Exception {
        SiteContext siteContext  = SiteContextHolder.getSiteContext();
        assertNotNull(siteContext);

        Long userSiteId = USER_SITE_ID;
        Site userSite = siteContext.getSite(userSiteId);
        Site defaultSite = siteContext.getSite(DEFAULT_SITE_ID);

        User user = createUser(SITE_CONTEXT_TEST_USER, userSite, RoleDAOTest.READONLY_ROLE_NAME, false);


        siteContext.setSite(user, userSite);
        Site userDefault = siteContext.getDefaultSiteForUser(user);
        assertEquals(userDefault, userSite);
        siteContext.addSite(user, defaultSite);
        userDAO.save(user);

        userDefault = siteContext.getDefaultSiteForUser(user);
        assertEquals(userDefault, defaultSite);

        siteContext.removeSite(user, defaultSite);

        userDAO.save(user);
        userDefault = siteContext.getDefaultSiteForUser(user);
        assertEquals(userDefault, userSite);

        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/SiteContextTest.xml", DatabaseOperation.REFRESH);
    }

// Deprecated functions, not tested unless we hvae to retain them.

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getSiteByTagId(java.lang.Long)}.
     */
    @Test(enabled = false)
    public void testGetSiteByTagId() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#setCurrentTagId(java.lang.Long)}.
     */
    @Test(enabled = false)
    public void testSetCurrentTagId() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#setSites(java.util.Set)}.
     */
    @Test(enabled = false)
    public void testSetSitesSetOfTag() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getSiteTagId()}.
     */
    @Test(enabled = false)
    public void testGetSiteTagId() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getTagIdsForQuery()}.
     */
    @Test(enabled = false)
    public void testGetSiteTagIds() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getTagById(java.lang.Long)}.
     */
    @Test(enabled = false)
    public void testGetTagById() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#validSiteByTagId(java.lang.Long)}.
     */
    @Test(enabled = false)
    public void testValidSite() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getTagBySiteName(java.lang.String)}.
     */
    @Test(enabled = false)
    public void testGetTagBySiteName() {
        fail("Not yet implemented");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.SiteContext#getAllTagsForUsers()}.
     */
    @Test(enabled = false)
    public void testGetAllTagsForUsers() {
        fail("Not yet implemented");
    }

    /**
     * @param name .
     * @param site .
     * @param role .
     * @param allSiteAccess .
     * @return user
     * @throws DataAccessException
     */
    @Test(enabled = false)
    private User createUser(String name, Site site, String role, boolean allSiteAccess) throws DataAccessException {
        String testPwd = "password";
        User user = new User();
        user.setName(name);
        user.setEnabled(true);
        user.setPassword(testPwd);
        user.setEmailAddress("testuserd@vocollect.com");
        user.addRole(roleDAO.findByName(role));
        user.setLastLoginLocation("location");
        Date loginTime = new Date();
        user.setLastLoginTime(loginTime);
        user.setNotes("notes");
        user.setAllSitesAccess(allSiteAccess);
        // user.setCurrentSite(site.getId());   // VVC-2514
                                                //  Not site ID but tag ID is appropriate here.
                                                //  That, however, would require a SiteContext
                                                //  to determine the tag, and since the value
                                                //  is not being used, better to just comment
                                                //  it out for now.

        UserProperty userProperty = new UserProperty();
        Set<UserProperty> userProperties = new HashSet<UserProperty>();
        userProperty.setName("testproperty");
        userProperty.setValue("testvalue");
        userProperties.add(userProperty);
        user.setUserProperties(userProperties);
        user.addSite(site);

        userDAO.save(user);

        return user;

    }

    /**
     * @return userDAO
     */
    @Test(enabled = false)
    public UserDAO getUserDAO() {
        return userDAO;
    }

    /**
     * @param daoParam .
     */
    @Test(enabled = false)
    public void setUserDAO(UserDAO daoParam) {
        userDAO = daoParam;
    }

    /**
     * @return roleDAO
     */
    @Test(enabled = false)
    public RoleDAO getRoleDAO() {
        return roleDAO;
    }

    /**
     * @param rdaoParam .
     */
    @Test(enabled = false)
    public void setRoleDAO(RoleDAO rdaoParam) {
        roleDAO = rdaoParam;
    }

    /**
     * @param siteManagerParam the siteManager to set
     */
    public void setSiteManager(SiteManager siteManagerParam) {
        siteManager = siteManagerParam;
    }

    /**
     * @return the siteManager
     */
    public SiteManager getSiteManager() {
        return siteManager;
    }

    /**
     * @return userManager
     */
    public UserManager getUserManager() {
        return userManager;
    }

    /**
     * @param userManagerParam .
     */
    public void setUserManager(UserManager userManagerParam) {
        userManager = userManagerParam;
    }
}
