/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

/**
 * Test cases for StringUtil methods.
 *
 * @author ??
 */
public class StringUtilTest {

    /**
     * @throws Exception
     */
    @Test()
    public void testIsNullOrEmpty() throws Exception {
        assertTrue("isNull", StringUtil.isNullOrEmpty(null));
        assertTrue("isEmpty", StringUtil.isNullOrEmpty(""));
        assertFalse("isNotNull", StringUtil.isNullOrEmpty(" "));
    }

    /**
     *
     */
    @Test()
    public void testDustForHTMLString() {
        String source = "<hr>";
        String expected = "&lt;hr&gt;";

        String dest = StringUtil.dustForHTML(source);
        assertTrue("Destination incorrect. Expected " + expected + " got " + dest, dest.equals(expected));

        source = source + source;
        expected = expected + expected;

        dest = StringUtil.dustForHTML(source);
        assertTrue("Destination incorrect. Expected " + expected + " got " + dest, dest.equals(expected));
    }

    /**
     *
     */
    @Test()
    public void testIsNumericString() {
        assertTrue("Numeric string shows as non-numeric", StringUtil.isNumeric("12345"));
        assertTrue("Numeric string with leading spaces shows as non-numeric", StringUtil.isNumeric("  12345"));
        assertTrue("Numeric string with trailing spaces shows as non-numeric", StringUtil.isNumeric("12345  "));
        assertTrue("Numeric string with leading and trailing spaces shows as non-numeric"
            , StringUtil.isNumeric("  12345  "));

        assertFalse("Non-numeric showing as numeric", StringUtil.isNumeric("junk"));
        assertFalse("Non-numeric with embedded numeric chars showing as numeric", StringUtil.isNumeric("j1unk"));
        assertFalse("Non-numeric with leading numeric chars showing as numeric", StringUtil.isNumeric("j1unk"));
        assertFalse("Non-numeric with embedded chars showing as numeric", StringUtil.isNumeric("1junk"));
        assertFalse("Non-numeric with trailing numeric chars showing as numeric", StringUtil.isNumeric("junk1"));
        assertFalse("Non-numeric with leading and trailing numeric chars showing as numeric"
            , StringUtil.isNumeric("1junk1"));

    }
}
