/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.model.NotificationPriority;

import java.util.Locale;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;


/**
 * Test case for ResourceUtil class.
 *
 * @author ddoubleday
 */
public class ResourceUtilTest {

    /**
     * Test method for {@link com.vocollect.epp.util.ResourceUtil#makeEnumResourceKey(com.vocollect.epp.dao.hibernate.ValueBasedEnum)}.
     */
    @Test()
    public final void testMakeEnumResourceKey() {
        String key = ResourceUtil.makeEnumResourceKey(NotificationPriority.CRITICAL);
        assertEquals(key, "com.vocollect.epp.model.NotificationPriority.0");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.ResourceUtil#getLocalizedEnumName(com.vocollect.epp.dao.hibernate.ValueBasedEnum)}.
     */
    @Test()
    public final void testGetLocalizedEnumName() {
        String localName = ResourceUtil.getLocalizedEnumName(NotificationPriority.CRITICAL);
        assertEquals(localName, "Critical");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.ResourceUtil#getLocalizedKeyValue(java.lang.String)}.
     */
    @Test()
    public final void testGetLocalizedKeyValue() {
        String key = ResourceUtil.makeEnumResourceKey(NotificationPriority.CRITICAL);
        String localName = ResourceUtil.getLocalizedKeyValue(key);
        assertEquals(localName, "Critical");
    }

    /**
     * Test method for {@link com.vocollect.epp.util.ResourceUtil#getLocalizedMessage(java.lang.String, java.lang.Object[], java.lang.String)}.
     */
    @Test()
    public final void testGetLocalizedMessageStringObjectArrayString() {
        assertEquals((String) null, ResourceUtil.getLocalizedMessage(
            "noMatch", null, (String) null));
        assertEquals("noMatch", ResourceUtil.getLocalizedMessage(
            "noMatch", null, "noMatch"));
        String key = "nav.search.sizetext.singular";
        assertEquals("{0} - {1} Match.", ResourceUtil.getLocalizedMessage(
            key, null, key));
        assertEquals("0 - 1 Match.", ResourceUtil.getLocalizedMessage(
            key, new Object[] {"0", 1}, "noMatch"));
        assertEquals("0 - User Match.", ResourceUtil.getLocalizedMessage(
            key, new Object[] {"0", UserMessage.KEY_EVAL_PREFIX + "entity.User"}, "noMatch"));
    }

    /**
     * Test method for {@link com.vocollect.epp.util.ResourceUtil#getLocalizedMessage(java.lang.String, java.lang.Object[], java.lang.String, java.util.Locale)}.
     */
    @Test()
    public final void testGetLocalizedMessageStringObjectArrayStringLocale() {
        assertEquals((String) null, ResourceUtil.getLocalizedMessage(
            "noMatch", null, (String) null, Locale.ENGLISH));
        assertEquals("noMatch", ResourceUtil.getLocalizedMessage(
            "noMatch", null, "noMatch", Locale.ENGLISH));
        String key = "nav.search.sizetext.singular";
        assertEquals("{0} - {1} Match.", ResourceUtil.getLocalizedMessage(
            key, null, key, Locale.ENGLISH));
        assertEquals("0 - 1 Match.", ResourceUtil.getLocalizedMessage(
            key, new Object[] {"0", 1}, "noMatch", Locale.ENGLISH));
        assertEquals("0 - User Match.", ResourceUtil.getLocalizedMessage(
            key, new Object[] {"0", UserMessage.KEY_EVAL_PREFIX + "entity.User"}, 
            "noMatch", Locale.ENGLISH));
    }

    /**
     * Test method for {@link com.vocollect.epp.util.ResourceUtil#getLocalizedMessage(java.lang.String, java.lang.Object[], java.util.Locale)}.
     */
    @Test()
    public final void testGetLocalizedMessageStringObjectArrayLocale() {
        String key = "nav.search.sizetext.singular";
        assertEquals("{0} - {1} Match.", ResourceUtil.getLocalizedMessage(
            key, null, Locale.ENGLISH));
        assertEquals("0 - 1 Match.", ResourceUtil.getLocalizedMessage(
            key, new Object[] {"0", 1}, Locale.ENGLISH));
        assertEquals("0 - User Match.", ResourceUtil.getLocalizedMessage(
            key, new Object[] {"0", UserMessage.KEY_EVAL_PREFIX + "entity.User"}, Locale.ENGLISH));
   }

    /**
     * Test method for {@link com.vocollect.epp.util.ResourceUtil#translateArg(java.lang.Object, java.util.Locale)}.
     */
    @Test()
    public final void testTranslateArg() {
        Object o = new Object();
        assertEquals(o, ResourceUtil.translateArg(o, Locale.ENGLISH));
        o = "fakeKey";
        assertEquals(o, ResourceUtil.translateArg(o, Locale.ENGLISH));
        o = "entity.User";
        assertEquals(o, ResourceUtil.translateArg(o, Locale.ENGLISH));
        o = UserMessage.KEY_EVAL_PREFIX + o;
        assertEquals("User", ResourceUtil.translateArg(o, Locale.ENGLISH));
    }

}
