/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;


/**
 * 
 *
 * @author jkercher
 */
@Test()
public class FeatureVoterTest {

    /**
     * @throws Exception
     */
    public void testMapURI() throws Exception {
        FeatureVoter voter = new FeatureVoter();
        
        String uri = "what/is/the/matrix/neo.action";
        assertEquals(voter.mapURI(uri), uri);
        
        uri = "localhost:9090/showprintwindow.action";
        assertEquals(voter.mapURI(uri), uri);
        
        uri = "/admin/user/getdataforprint.action";
        assertEquals(voter.mapURI(uri), "/admin/user/getdata.action");
        
    }
}
