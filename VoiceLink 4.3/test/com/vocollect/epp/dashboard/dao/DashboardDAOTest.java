/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.dao;

import com.vocollect.epp.dao.BaseDAOTestCase;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.DASHBOARD;

import java.util.List;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Unit Tests for DashboardDAO methods.
 * 
 * @author smittal
 */
@Test(groups = { DASHBOARD })
public class DashboardDAOTest extends BaseDAOTestCase {

    private DashboardDAO dao = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/chart-data.xml");
        adapter.handleFlatXmlResource("data/dbunit/dashboard-data.xml");
    }

    /**
     * Method to perform data clean up after all unit test cases are executed.
     * @throws Exception
     */
    @AfterClass()
    protected void cleanUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * @param dashboardDAO the user data access object
     */
    @Test(enabled = false)
    public void setDashboardDAO(DashboardDAO dashboardDAO) {
        this.dao = dashboardDAO;
    }

    /**
     * . Test the get all method
     */
    @Test()
    public void testGetAll() {
        try {
            List<Dashboard> dashboards = this.dao.getAll();
            assert (dashboards.size() > 0);
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }

    @Test(enabled = false)
    public void testUniquenessByName() {
        try {
            List<Dashboard> dashboards = this.dao.getAll();

            Long l1 = this.dao.uniquenessByName(dashboards.get(0).getName());

            Long l2 = this.dao.uniquenessByName(dashboards.get(1).getName());

            assert (!l1.equals(l2));
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }
}
