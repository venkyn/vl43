/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.service.impl;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.service.ChartManager;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dashboard.service.DashboardDetailManager;
import com.vocollect.epp.dashboard.service.DashboardManager;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.service.DAColumnManager;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.BaseServiceTestCase;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.SiteContextHolder;

import static com.vocollect.epp.test.TestGroups.DASHBOARD;

import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit test class for testing dashboard detail manager.
 * @author smittal
 * 
 */
@Test(groups = { DASHBOARD })
public class DashboardDetailManagerImplTest extends BaseServiceTestCase {

    private DashboardManager dashboardManager = null;

    private ChartManager chartManager = null;

    private DashboardDetailManager dashboardDetailManager = null;
    
    private DAColumnManager daColumnManager = null;

    /**
     * @throws Exception
     */
    @BeforeClass
    public void classSetup() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/chart-data.xml");
        adapter.handleFlatXmlResource("data/dbunit/dashboard-data.xml");
    }

    /**
     * @return the dashboardManager
     */
    @Test(enabled = false)
    public DashboardManager getDashboardManager() {
        return dashboardManager;
    }

    /**
     * @param dashboardManager - the dashboard manager
     */
    @Test(enabled = false)
    public void setDashboardManager(DashboardManager dashboardManager) {
        this.dashboardManager = dashboardManager;
    }

    /**
     * @return the chartManager
     */
    @Test(enabled = false)
    public ChartManager getChartManager() {
        return chartManager;
    }

    /**
     * @param chartManager the chartManager to set
     */
    @Test(enabled = false)
    public void setChartManager(ChartManager chartManager) {
        this.chartManager = chartManager;
    }

    /**
     * Getter for the dashboardDetailManager property.
     * @return DashboardDetailManager value of the property
     */
    @Test(enabled = false)
    public DashboardDetailManager getDashboardDetailManager() {
        return dashboardDetailManager;
    }

    /**
     * Setter for the dashboardDetailManager property.
     * @param dashboardDetailManager the new dashboardDetailManager value
     */
    @Test(enabled = false)
    public void setDashboardDetailManager(DashboardDetailManager dashboardDetailManager) {
        this.dashboardDetailManager = dashboardDetailManager;
    }


    /**
     * Getter for the daColumnManager property.
     * @return DAColumnManager value of the property
     */
    @Test(enabled = false)
    public DAColumnManager getDaColumnManager() {
        return daColumnManager;
    }

    /**
     * Setter for the daColumnManager property.
     * @param daColumnManager the new daColumnManager value
     */
    @Test(enabled = false)
    public void setDaColumnManager(DAColumnManager daColumnManager) {
        this.daColumnManager = daColumnManager;
    }

    /**
     * Set the site context.
     * @param siteId - the site ID
     * @throws Exception
     * 
     */
    protected void setSiteContext(Long siteId) throws Exception {
        this.setUpSiteContextNoDefaultSite(true);
        SiteContextHolder.getSiteContext().setCurrentSite(siteId);
    }

    /**
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    @Test()
    public void testFindLinkedDrillDownField() throws DataAccessException,
        BusinessRuleException {
        Dashboard dashboard = getDashboardManager().get(-1L);
        Chart parentChart = getChartManager().get(-1L);
        Chart drillDownChart = getChartManager().get(-2L);
        DAColumn linkedDrillDownField = getDaColumnManager().get(-1L);

        getDashboardManager().executeAddChartToDashboard(dashboard,
            parentChart, drillDownChart, linkedDrillDownField);

        DAColumn linkedField = getDashboardDetailManager()
            .findLinkedDrillDownField(dashboard, parentChart, drillDownChart);

        assertEquals(linkedDrillDownField.getFieldId(),
            linkedField.getFieldId());

    }

    /**
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    @Test()
    public void testListDetailsByLinkedField() throws DataAccessException,
        BusinessRuleException {

        Dashboard dashboard = getDashboardManager().get(-2L);

        Chart parentChart = getChartManager().get(-1L);
        Chart drillDownChart = getChartManager().get(-2L);

        DAColumn linkedDrillDownField = getDaColumnManager().get(-2L);

        getDashboardManager().executeAddChartToDashboard(dashboard,
            parentChart, null, null);
        getDashboardManager().executeAddChartToDashboard(dashboard,
            parentChart, drillDownChart, linkedDrillDownField);

        assertEquals(
            getDashboardDetailManager().listDetailIdsByLinkedField(
                linkedDrillDownField).size(), 1);

    }

    /**
     * @throws Exception e
     */
    @Test()
    public void testForceDelete() throws Exception {
        setSiteContext(-1L);

        DAColumn linkedDrillDownField = getDaColumnManager().get(-3L);

        List<Long> affectedDetailIds = this.dashboardDetailManager
            .listDetailIdsByLinkedField(linkedDrillDownField);
        assertEquals(affectedDetailIds.size(), 2);
        
        for (Long detailId : affectedDetailIds) {
            getDashboardDetailManager().forceDelete(detailId);
        }

        assertEquals(
            getDashboardDetailManager().listDetailIdsByLinkedField(
                linkedDrillDownField).size(), 0);
    }
}
