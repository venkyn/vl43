/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.service.impl;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.service.ChartManager;
import com.vocollect.epp.dao.RoleDAO;
import com.vocollect.epp.dao.RoleDAOTest;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dashboard.service.DashboardManager;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.service.DAColumnManager;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.UserProperty;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.service.impl.BaseServiceTestCase;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.SiteContextHolder;

import static com.vocollect.epp.test.TestGroups.DASHBOARD;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit test class for testing dashboard manager.
 * @author smittal
 * 
 */
@Test(groups = { DASHBOARD })
public class DashboardManagerImplTest extends BaseServiceTestCase {

    private DashboardManager dashboardManager = null;

    private ChartManager chartManager = null;
    
    private DAColumnManager daColumnManager = null;

    private SiteManager siteManager = null;
    
    private UserManager userManager = null;
    
    private RoleDAO rdao = null;

    /**
     * @throws Exception
     */
    @BeforeClass
    public void classSetup() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/chart-data.xml");
        adapter.handleFlatXmlResource("data/dbunit/dashboard-data.xml");
    }

    /**
     * Set the site context.
     * @param siteId - the site ID
     * @throws Exception
     * 
     */
    @Test(enabled = false)
    protected void setSiteContext(Long siteId) throws Exception {
        this.setUpSiteContextNoDefaultSite(true);
        SiteContextHolder.getSiteContext().setCurrentSite(siteId);
    }

    /**
     * @return the dashboardManager
     */
    @Test(enabled = false)
    public DashboardManager getDashboardManager() {
        return dashboardManager;
    }

    /**
     * @param dashboardManager - the dashboard manager
     */
    @Test(enabled = false)
    public void setDashboardManager(DashboardManager dashboardManager) {
        this.dashboardManager = dashboardManager;
    }

    /**
     * @return the chartManager
     */
    @Test(enabled = false)
    public ChartManager getChartManager() {
        return chartManager;
    }

    /**
     * @param chartManager the chartManager to set
     */
    @Test(enabled = false)
    public void setChartManager(ChartManager chartManager) {
        this.chartManager = chartManager;
    }

    /**
     * Getter for the daColumnManager property.
     * @return DAColumnManager value of the property
     */
    @Test(enabled = false)
    public DAColumnManager getDaColumnManager() {
        return daColumnManager;
    }

    /**
     * Setter for the daColumnManager property.
     * @param daColumnManager the new daColumnManager value
     */
    @Test(enabled = false)
    public void setDaColumnManager(DAColumnManager daColumnManager) {
        this.daColumnManager = daColumnManager;
    }

    /**
     * @return the siteManager
     */
    @Test(enabled = false)
    public SiteManager getSiteManager() {
        return siteManager;
    }

    /**
     * @param siteManager the siteManager to set
     */
    @Test(enabled = false)
    public void setSiteManager(SiteManager siteManager) {
        this.siteManager = siteManager;
    }

    /**
     * @return the userManager
     */
    @Test(enabled = false)
    public UserManager getUserManager() {
        return userManager;
    }

    
    /**
     * @param userManager the userManager to set
     */
    @Test(enabled = false)
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }
    
    /**
     * Test method for 'com.vocollect.epp.dao.UserDAO.setRoleDAO(RoleDAO rdao)'.
     * @param roleDAO role data access object
     */
    @Test(enabled = false)
    public void setRoleDAO(RoleDAO roleDAO) {
        this.rdao = roleDAO;
    }

    /**
     * . Tests the getAll method
     */
    @Test()
    public void testGetAllDashboards() {
        try {
            List<Dashboard> dashboards = this.dashboardManager.getAll();

            // We are not testing how many dashboards have been returned just
            // because
            // the data XML file will keep growing and we have to maintain this
            // test case very time we update the data XML
            assert (dashboards.size() > 0);
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }

    @Test()
    public void testUniquenessByName() {

        Dashboard dashboard1 = new Dashboard();
        dashboard1.setName("dashboard");
        dashboard1.setDescription("abc");

        try {
            this.dashboardManager.verifyUniquenessByName(dashboard1);
        } catch (Exception e) {
            assert (e instanceof FieldValidationException);
            fail(e.getLocalizedMessage(), e);
        }
    }

    @Test()
    public void testSave() throws Exception {
        setSiteContext(-1L);

        Dashboard dashboard = null;

        try {
            dashboard = new Dashboard();

            dashboard.setName("abcDashboard");
            dashboard.setDescription("dashboard");

            this.dashboardManager.save(dashboard);

            getDashboardManager().getPrimaryDAO().flushSession();
            getDashboardManager().getPrimaryDAO().clearSession();

            List<Dashboard> dashboards = getDashboardManager().getAll();

            // Check 1 of the dashboards in the system should match the above
            // persisted
            for (Dashboard dashboard1 : dashboards) {
                if (dashboard1.getName().equals("dashboard")) {
                    assertEquals(dashboard1.getName(), dashboard.getName());
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        } finally {
            // Clean
            try {
                getDashboardManager().delete(dashboard);
                getDashboardManager().getPrimaryDAO().flushSession();
                getDashboardManager().getPrimaryDAO().clearSession();
            } catch (Exception e) {

            }
        }

    }

    @Test()
    public void testAddChartToDashboard() throws Exception {
        setSiteContext(-1L);

        Dashboard dashboard = this.dashboardManager.get(-1L);
        Chart parentChart = this.getChartManager().get(-1L);

        this.dashboardManager.executeAddChartToDashboard(dashboard, parentChart,
            null, null);
        
        this.dashboardManager.getPrimaryDAO().clearSession();

        Dashboard newDb = this.dashboardManager.get(-1L);
        assertEquals(newDb.getDetails().size(), 1);
    }

    @Test()
    public void testListDashboards() throws Exception {
        setSiteContext(-1L);
        List<Dashboard> dashboards = this.dashboardManager.listDashboards();

        assertEquals(dashboards.size(), 3);
    }

   

    @Test()
    public void testFindDefaultDashboard() throws Exception {
        setSiteContext(-1L);
        // test user with all fields
        User user = new User();
        user.setName("testuserd1");
        user.setEnabled(true);
        user.setPassword("testPwd");
        user.setEmailAddress("testuserd1@vocollect.com");
        user.addRole(rdao.findByName(RoleDAOTest.ADMIN_ROLE_NAME));
        user.setLastLoginLocation("location");
        Date loginTime = new Date();
        user.setLastLoginTime(loginTime);
        user.setNotes("notes");
        user.setAllSitesAccess(true);
        user.setCurrentSite(-1L);
        
        getUserManager().save(user);
        
        Dashboard dashboard = this.dashboardManager.getAll().get(0);
        
        UserProperty property = new UserProperty();
        property.setName("default.dashboard.for.site.-1");
        property.setValue(dashboard.getId() + "");
        Set<UserProperty> props = new HashSet<UserProperty>();
        props.add(property);
        user.setUserProperties(props);
        getUserManager().save(user);

        assertEquals(this.dashboardManager.findDefaultDashboard(user).getName(),
            dashboard.getName());
        
        getUserManager().delete(user.getId());
    }

    @Test()
    public void testSaveDrillDown() throws DataAccessException,
        BusinessRuleException {
        Dashboard dashboard = this.dashboardManager.getAll().get(1);
        Chart parentChart = this.getChartManager().get(-1L);
        Chart childChart = this.getChartManager().get(-2L);
        DAColumn linkedDrillDownField = this.getDaColumnManager().get(-1L);
        
        Long dbId = dashboard.getId();

        this.dashboardManager.executeAddChartToDashboard(dashboard, parentChart,
            childChart, linkedDrillDownField);

        this.dashboardManager.getPrimaryDAO().clearSession();
        Dashboard newDb = this.dashboardManager.get(dbId);
        assertEquals(newDb.getDetails().size(), 1);
        assertEquals(newDb.getDetails().iterator().next().getDrillDownChart()
            .getName(), childChart.getName());
    }
}
