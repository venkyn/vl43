/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.model;


import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.model.ChartDataField;
import com.vocollect.epp.chart.model.ChartType;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAInformation;

import static com.vocollect.epp.test.TestGroups.DASHBOARD;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;

import java.util.Set;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * 
 * 
 * @author smittal
 */
@Test(groups = { FAST, MODEL, DASHBOARD })
public class DashboardDetailTest {

    private DAInformation daInformation = null;

    private DAColumn daColumn = new DAColumn();

    private Set<ChartDataField> chartDataFields = null;

    /**
     * . Test the hash code generates equal number
     */
    @Test()
    public void testHashCodeEqual() {

        Dashboard dashboard1 = new Dashboard();
        dashboard1.setName("dashboard");
        dashboard1.setDescription("abc");
        
        Dashboard dashboard2 = new Dashboard();
        dashboard2.setName("dashboard");
        dashboard2.setDescription("abc");

        Chart parentChart1 = new Chart();
        Chart childChart1 = new Chart();

        parentChart1.setName("parentChart");
        parentChart1.setDescription("abc");
        parentChart1.setDaInformation(daInformation);
        parentChart1.setCategory(daColumn);
        parentChart1.setChartDataFields(chartDataFields);
        parentChart1.setType(ChartType.COLUMN);

        childChart1.setName("childChart");
        childChart1.setDescription("abc");
        childChart1.setDaInformation(daInformation);
        childChart1.setCategory(daColumn);
        childChart1.setChartDataFields(chartDataFields);
        childChart1.setType(ChartType.COLUMN);

        Chart parentChart2 = new Chart();
        Chart childChart2 = new Chart();

        parentChart2.setName("parentChart");
        parentChart2.setDescription("abc");
        parentChart2.setDaInformation(daInformation);
        parentChart2.setCategory(daColumn);
        parentChart2.setChartDataFields(chartDataFields);
        parentChart2.setType(ChartType.COLUMN);

        childChart2.setName("childChart");
        childChart2.setDescription("abc");
        childChart2.setDaInformation(daInformation);
        childChart2.setCategory(daColumn);
        childChart2.setChartDataFields(chartDataFields);
        childChart2.setType(ChartType.COLUMN);

        DashboardDetail dashboardDetail1 = new DashboardDetail();
        DashboardDetail dashboardDetail2 = new DashboardDetail();

        dashboardDetail1.setDashboard(dashboard1);
        dashboardDetail1.setParentChart(parentChart1);
        dashboardDetail1.setDrillDownChart(childChart1);
        dashboardDetail1.setLinkedDrillDownField(daColumn);

        dashboardDetail2.setDashboard(dashboard2);
        dashboardDetail2.setParentChart(parentChart2);
        dashboardDetail2.setDrillDownChart(childChart2);
        dashboardDetail2.setLinkedDrillDownField(daColumn);

        assertEquals("Hash codes are not same", dashboardDetail2.hashCode(),
            dashboardDetail1.hashCode());
    }

    /**
     * . Test the hash code generates zero for null dashboard
     */
    @Test()
    public void testHashCodeZero() {

        Dashboard dashboard = null;

        Chart parentChart = new Chart();
        Chart childChart = new Chart();

        parentChart.setName("parentChart");
        parentChart.setDescription("abc");
        parentChart.setDaInformation(daInformation);
        parentChart.setCategory(daColumn);
        parentChart.setChartDataFields(chartDataFields);
        parentChart.setType(ChartType.COLUMN);

        childChart.setName("childChart");
        childChart.setDescription("abc");
        childChart.setDaInformation(daInformation);
        childChart.setCategory(daColumn);
        childChart.setChartDataFields(chartDataFields);
        childChart.setType(ChartType.COLUMN);

        DashboardDetail dashboardDetail1 = new DashboardDetail();

        dashboardDetail1.setDashboard(dashboard);
        dashboardDetail1.setParentChart(parentChart);
        dashboardDetail1.setDrillDownChart(childChart);
        dashboardDetail1.setLinkedDrillDownField(daColumn);

        assertEquals("Hash codes are not same", 0, dashboardDetail1.hashCode());
    }

    /**
     * . Test the hash code generates different number
     */
    @Test()
    public void testHashCodeDifferent() {
        Dashboard dashboard1 = new Dashboard();
        dashboard1.setName("dashboard");
        dashboard1.setDescription("abc");
        
        Dashboard dashboard2 = new Dashboard();
        dashboard2.setName("dashboard");
        dashboard2.setDescription("abc");

        Chart parentChart1 = new Chart();
        Chart childChart1 = new Chart();

        parentChart1.setName("parentChart");
        parentChart1.setDescription("abc");
        parentChart1.setDaInformation(daInformation);
        parentChart1.setCategory(daColumn);
        parentChart1.setChartDataFields(chartDataFields);
        parentChart1.setType(ChartType.COLUMN);

        childChart1.setName("childChart");
        childChart1.setDescription("abc");
        childChart1.setDaInformation(daInformation);
        childChart1.setCategory(daColumn);
        childChart1.setChartDataFields(chartDataFields);
        childChart1.setType(ChartType.COLUMN);

        Chart parentChart2 = new Chart();
        Chart childChart2 = new Chart();

        parentChart2.setName("parentChart");
        parentChart2.setDescription("abc");
        parentChart2.setDaInformation(daInformation);
        parentChart2.setCategory(daColumn);
        parentChart2.setChartDataFields(chartDataFields);
        parentChart2.setType(ChartType.COLUMN);

        childChart2.setName("childChart");
        childChart2.setDescription("abc");
        childChart2.setDaInformation(daInformation);
        childChart2.setCategory(daColumn);
        childChart2.setChartDataFields(chartDataFields);
        childChart2.setType(ChartType.COLUMN);
        
        DashboardDetail dashboardDetail1 = new DashboardDetail();
        DashboardDetail dashboardDetail2 = new DashboardDetail();

        dashboardDetail1.setDashboard(dashboard1);
        dashboardDetail1.setParentChart(parentChart1);
        dashboardDetail1.setDrillDownChart(childChart1);
        dashboardDetail1.setLinkedDrillDownField(daColumn);

        dashboardDetail2.setDashboard(dashboard2);
        dashboardDetail2.setParentChart(parentChart2);
        dashboardDetail2.setDrillDownChart(null);
        dashboardDetail2.setLinkedDrillDownField(daColumn);
        
        // Everything is same, but child chart of 1 detail is null
        assertEquals("Hash code should be different",
            dashboardDetail1.hashCode() != dashboardDetail2.hashCode(), true);
        
        //child chart of 1 detail is null and dashboard of the detail is different
        dashboard2.setName("dashboard2");
        dashboardDetail2.setDashboard(dashboard2);
        
        assertEquals("Hash code should be different",
            dashboardDetail1.hashCode() != dashboardDetail2.hashCode(), true);
        
        //for one detail dashboard and parent chart is different and child chart is same.
        parentChart2.setName("parentChart2");
        dashboardDetail2.setParentChart(parentChart2);
        dashboardDetail2.setDrillDownChart(childChart2);
        
        assertEquals("Hash code should be different",
            dashboardDetail1.hashCode() != dashboardDetail2.hashCode(), true);
        
        //for one detail dashboard and child chart are different but parent chart is same
        parentChart2.setName("parentChart");
        childChart2.setName("childChart2");
        dashboardDetail2.setParentChart(parentChart2);
        dashboardDetail2.setDrillDownChart(childChart2);
        
        assertEquals("Hash code should be different",
            dashboardDetail1.hashCode() != dashboardDetail2.hashCode(), true);
        
        //for one detail dashboard is different but parent and child chart are same
        parentChart2.setName("parentChart");
        childChart2.setName("childChart");
        dashboardDetail2.setParentChart(parentChart2);
        dashboardDetail2.setDrillDownChart(childChart2);
        
        assertEquals("Hash code should be different",
            dashboardDetail1.hashCode() != dashboardDetail2.hashCode(), true);
        
    }

    /**
     * . Tests two types are equal
     */
    @Test()
    public void testEqual() {

        Dashboard dashboard1 = new Dashboard();
        dashboard1.setName("dashboard");
        dashboard1.setDescription("abc");
        
        Dashboard dashboard2 = new Dashboard();
        dashboard2.setName("dashboard");
        dashboard2.setDescription("abc");

        Chart parentChart1 = new Chart();
        Chart childChart1 = new Chart();

        parentChart1.setName("parentChart");
        parentChart1.setDescription("abc");
        parentChart1.setDaInformation(daInformation);
        parentChart1.setCategory(daColumn);
        parentChart1.setChartDataFields(chartDataFields);
        parentChart1.setType(ChartType.COLUMN);

        childChart1.setName("childChart");
        childChart1.setDescription("abc");
        childChart1.setDaInformation(daInformation);
        childChart1.setCategory(daColumn);
        childChart1.setChartDataFields(chartDataFields);
        childChart1.setType(ChartType.COLUMN);

        Chart parentChart2 = new Chart();
        Chart childChart2 = new Chart();

        parentChart2.setName("parentChart");
        parentChart2.setDescription("abc");
        parentChart2.setDaInformation(daInformation);
        parentChart2.setCategory(daColumn);
        parentChart2.setChartDataFields(chartDataFields);
        parentChart2.setType(ChartType.COLUMN);

        childChart2.setName("childChart");
        childChart2.setDescription("abc");
        childChart2.setDaInformation(daInformation);
        childChart2.setCategory(daColumn);
        childChart2.setChartDataFields(chartDataFields);
        childChart2.setType(ChartType.COLUMN);

        DashboardDetail dashboardDetail1 = new DashboardDetail();
        DashboardDetail dashboardDetail2 = new DashboardDetail();

        dashboardDetail1.setDashboard(dashboard1);
        dashboardDetail1.setParentChart(parentChart1);
        dashboardDetail1.setDrillDownChart(childChart1);
        dashboardDetail1.setLinkedDrillDownField(daColumn);

        dashboardDetail2.setDashboard(dashboard2);
        dashboardDetail2.setParentChart(parentChart2);
        dashboardDetail2.setDrillDownChart(childChart2);
        dashboardDetail2.setLinkedDrillDownField(daColumn);

        assertEquals("Types should be equal", dashboardDetail1.equals(dashboardDetail2),
            true);
    }

    /**
     * . Tests two types are not equal
     */
    @Test()
    public void testNotEqual() {
        Dashboard dashboard1 = new Dashboard();
        dashboard1.setName("dashboard");
        dashboard1.setDescription("abc");
        
        Dashboard dashboard2 = new Dashboard();
        dashboard2.setName("dashboard");
        dashboard2.setDescription("abc");

        Chart parentChart1 = new Chart();
        Chart childChart1 = new Chart();

        parentChart1.setName("parentChart");
        parentChart1.setDescription("abc");
        parentChart1.setDaInformation(daInformation);
        parentChart1.setCategory(daColumn);
        parentChart1.setChartDataFields(chartDataFields);
        parentChart1.setType(ChartType.COLUMN);

        childChart1.setName("childChart");
        childChart1.setDescription("abc");
        childChart1.setDaInformation(daInformation);
        childChart1.setCategory(daColumn);
        childChart1.setChartDataFields(chartDataFields);
        childChart1.setType(ChartType.COLUMN);

        Chart parentChart2 = new Chart();
        Chart childChart2 = new Chart();

        parentChart2.setName("parentChart");
        parentChart2.setDescription("abc");
        parentChart2.setDaInformation(daInformation);
        parentChart2.setCategory(daColumn);
        parentChart2.setChartDataFields(chartDataFields);
        parentChart2.setType(ChartType.COLUMN);

        childChart2.setName("childChart");
        childChart2.setDescription("abc");
        childChart2.setDaInformation(daInformation);
        childChart2.setCategory(daColumn);
        childChart2.setChartDataFields(chartDataFields);
        childChart2.setType(ChartType.COLUMN);
        
        DashboardDetail dashboardDetail1 = new DashboardDetail();
        DashboardDetail dashboardDetail2 = new DashboardDetail();

        dashboardDetail1.setDashboard(dashboard1);
        dashboardDetail1.setParentChart(parentChart1);
        dashboardDetail1.setDrillDownChart(childChart1);
        dashboardDetail1.setLinkedDrillDownField(daColumn);

        dashboardDetail2.setDashboard(dashboard2);
        dashboardDetail2.setParentChart(parentChart2);
        dashboardDetail2.setDrillDownChart(null);
        dashboardDetail2.setLinkedDrillDownField(daColumn);
        
        // Everything is same, but child chart of 1 detail is null
        assertEquals("Types should not be equal",
            dashboardDetail1.equals(dashboardDetail2), false);
        
        //child chart of 1 detail is null and dashboard of the detail is different
        dashboard2.setName("dashboard2");
        dashboardDetail2.setDashboard(dashboard2);
        
        assertEquals("Types should not be equal",
            dashboardDetail1.equals(dashboardDetail2), false);
        
        //for one detail dashboard and parent chart is different and child chart is same.
        parentChart2.setName("parentChart2");
        dashboardDetail2.setParentChart(parentChart2);
        dashboardDetail2.setDrillDownChart(childChart2);
        
        assertEquals("Types should not be equal",
            dashboardDetail1.equals(dashboardDetail2), false);
        
        //for one detail dashboard and child chart are different but parent chart is same
        parentChart2.setName("parentChart");
        childChart2.setName("childChart2");
        dashboardDetail2.setParentChart(parentChart2);
        dashboardDetail2.setDrillDownChart(childChart2);
        
        assertEquals("Types should not be equal",
            dashboardDetail1.equals(dashboardDetail2), false);
        
        //for one detail dashboard is different but parent and child chart are same
        parentChart2.setName("parentChart");
        childChart2.setName("childChart");
        dashboardDetail2.setParentChart(parentChart2);
        dashboardDetail2.setDrillDownChart(childChart2);
        
        assertEquals("Types should not be equal",
            dashboardDetail1.equals(dashboardDetail2), false);
        
        //dashboard of one detail is null
        dashboardDetail2.setDashboard(null);
        
        assertEquals("Types should not be equal",
            dashboardDetail1.equals(dashboardDetail2), false);
    }
}
