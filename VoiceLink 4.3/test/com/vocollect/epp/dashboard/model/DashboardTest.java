/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.model;


import static com.vocollect.epp.test.TestGroups.DASHBOARD;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;

import java.util.Set;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * 
 * 
 * @author smittal
 */
@Test(groups = { FAST, MODEL, DASHBOARD })
public class DashboardTest {

    private Set<DashboardDetail> details = null;

    /**
     * . Test the hash code generates equal number
     */
    @Test()
    public void testHashCodeEqual() {
        Dashboard dashboard1 = new Dashboard();
        Dashboard dashboard2 = new Dashboard();

        dashboard1.setName("dashboard1");
        dashboard1.setDescription("abc");
        dashboard1.setDetails(details);

        dashboard2.setName("dashboard1");
        dashboard2.setDescription("abc");
        dashboard2.setDetails(details);

        assertEquals("Hash codes are not same", dashboard2.hashCode(),
            dashboard1.hashCode());
    }

    /**
     * . Test the hash code generates different number
     */
    @Test()
    public void testHashCodeDifferent() {
        Dashboard dashboard1 = new Dashboard();
        Dashboard dashboard2 = new Dashboard();

        dashboard1.setName("dashboard1");
        dashboard1.setDescription("abc");
        dashboard1.setDetails(details);

        dashboard2.setName("dashboard2");
        dashboard2.setDescription("abc");
        dashboard2.setDetails(details);

        assertEquals("Hash code should be different",
            dashboard1.hashCode() != dashboard2.hashCode(), true);
    }

    /**
     * . Tests two types are equal
     */
    @Test()
    public void testEqual() {
        Dashboard dashboard1 = new Dashboard();
        Dashboard dashboard2 = new Dashboard();

        dashboard1.setName("dashboard1");
        dashboard1.setDescription("abc");
        dashboard1.setDetails(details);

        dashboard2.setName("dashboard1");
        dashboard2.setDescription("abc");
        dashboard2.setDetails(details);

        assertEquals("Types should be equal", dashboard1.equals(dashboard2), true);
    }

    /**
     * . Tests two types are not equal
     */
    @Test()
    public void testNotEqual() {
        Dashboard dashboard1 = new Dashboard();
        Dashboard dashboard2 = new Dashboard();

        dashboard1.setName("dashboard1");
        dashboard1.setDescription("abc");
        dashboard1.setDetails(details);

        // When compared with other object
        Object obj = new Object();

        assertEquals("Objects should not be equal", dashboard1.equals(obj), false);

        // Name of one dashboard is null
        dashboard2.setName(null);
        dashboard2.setDescription("abc");
        dashboard2.setDetails(details);

        assertEquals("Types should not be equal", dashboard1.equals(dashboard2), false);

        // When compared with different dashboards
        dashboard2.setName("dashboard2");
        dashboard2.setDescription("abc");
        dashboard2.setDetails(details);

        assertEquals("Types should not be equal", dashboard1.equals(dashboard2), false);
    }
}
