/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.test;

import java.sql.Types;

import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.datatype.DataTypeException;
import org.dbunit.dataset.datatype.DefaultDataTypeFactory;


/**
 * DataTypeFactory for HSQL. This is part of dbunit 2.2.1, but we haven't
 * upgraded yet. Can be removed when we do.
 *
 * @author ddoubleday
 */
public class HsqlDataTypeFactory extends DefaultDataTypeFactory {

    /**
     * {@inheritDoc}
     * @see org.dbunit.dataset.datatype.DefaultDataTypeFactory#createDataType(int, java.lang.String)
     */
    @Override
    public DataType createDataType(int sqlType, String sqlTypeName)
        throws DataTypeException {
        if (sqlType == Types.BOOLEAN) {
            return DataType.BOOLEAN;
        }

        return super.createDataType(sqlType, sqlTypeName);
    }
}
