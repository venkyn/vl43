/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.test;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.SiteContextHolder;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;

import org.dbunit.DatabaseTestCase;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.excel.XlsDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.ext.mssql.InsertIdentityOperation;
import org.dbunit.ext.oracle.OracleDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;

import com.vocollect.epp.ant.DbUnitUtil;
import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.util.SiteContextHolder;

/**
 * This class implements a simple wrapper around DbUnit functionality.
 *
 * @author jkercher
 * @author ddoubleday
 */
public class DbUnitAdapter extends DatabaseTestCase {

    private static final Logger log = new Logger(DbUnitAdapter.class);

    // Stores the results of the last operation.
    private IDataSet dataSet = null;
    // Default database operation; used by methods that do not take a
    // DatabaseOperation parameter.
    private DatabaseOperation defaultOperation;
    // Stores database connection properties (read from database.properties).
    private static Properties dbProps = null;


    /**
     * Constructor.
     */
    public DbUnitAdapter() {
        // Read db connection properties if necessary.
        if (dbProps == null) {
            dbProps = getDbProperties();
        }

        setDefaultOperation(DatabaseOperation.CLEAN_INSERT);
    }

    /**
     * {@inheritDoc}
     * @see org.dbunit.DatabaseTestCase#getDataSet()
     */
    @Override
    protected IDataSet getDataSet() throws Exception {
        return this.dataSet;
    }

    /**
     * Set the default database operation.  This operation will be used
     * by methods that do not accept a DatabaseOperation parameter.
     * @param operation specifies the default DbUnit operation. This may be
     * adjusted if the current database is SQLServer, because SQLServer
     * requires override behavior in some cases.
     */
    public void setDefaultOperation(DatabaseOperation operation) {
        // If using SQLServer, translate operation to SQLServer equivalent
        // if there is one. Otherwise, just assign.
        this.defaultOperation = translateOperation(operation);
    }

    /**
     * @return the default DbUnit operation.
     */
    public DatabaseOperation getDefaultOperation() {
        return this.defaultOperation;
    }

    /**
     * Creates and returns a DatabaseConnection object initialized with
     * connection properties (url, user, pwd) retrieved from the
     * database.properties file.
     * @see org.dbunit.DatabaseTestCase#getConnection()
     * @return the connection.
     * @throws Exception on any error
     */
    @Override
    public IDatabaseConnection getConnection() throws Exception {
        DatabaseConfig dbConfig;

        // Read the database properties if necessary.
        if (dbProps == null) {
            throw new RuntimeException("Missing database connection properties."
                + "  DbUnitAdapter cannot continue.");
        }
        
        // Get the default schema
        String schema = dbProps.getProperty("hibernate.default_schema");
        if (isOracleDatabase()) {
            schema = schema.toUpperCase();
        }

        // Ensure the database driver is loaded.
        Class.forName(dbProps.getProperty("hibernate.connection.driver_class"));

        // Create and return a new database connection.
        Connection jdbcConnection = DriverManager.getConnection(
            dbProps.getProperty("hibernate.connection.url"),
            dbProps.getProperty("hibernate.connection.username"),
            dbProps.getProperty("hibernate.connection.password"));

        // If a default schema was specified, use it when creating the connection
        DatabaseConnection dbConnection;
        
        if (schema != null && !schema.equals("")) {
            dbConnection = new DatabaseConnection(jdbcConnection, schema);
        } else {
            dbConnection = new DatabaseConnection(jdbcConnection);
        }
        dbConfig = dbConnection.getConfig();
        if (isOracleDatabase()) {
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                                 new OracleDataTypeFactory());
            dbConfig.setFeature(DatabaseConfig.FEATURE_SKIP_ORACLE_RECYCLEBIN_TABLES, true);
        } else if (isHSQLDatabase() || isH2Database()) {
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                new HsqlDataTypeFactory());
        } else if (isSQLServerDatabase()) {
            dbConfig.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY,
                new MsSqlDataTypeFactoryJDBC2());
        }
        return dbConnection;

    }

    /**
     * Clear data for all apps that are part of the build.
     * @param connection - the db connection
     * @throws Exception - if dataload problem happens
     */
    // SHOULD BE PRIVATE (Remove clearSiteCache if changed)
    public void clearClientInstallationData(DatabaseConnection connection) throws Exception {

        clearClientInstallationData(connection,
            "data/dbunit/voicelink/VoiceLinkDataUnitTest_ResetAll.xml");
        clearClientInstallationData(connection,
            "data/dbunit/voiceconsole/VoiceConsoleDataUnitTest_ResetAll.xml");
        clearClientInstallationData(connection,
            "data/dbunit/lm/LicenseManagerDataUnitTest_ResetAll.xml");
        clearSiteCache();
    }

    /**
     * Clear data for all apps that are part of the build.
     * @param connection - the db connection
     * @param resetSource - the DbUnit source resource that tells which tables
     * to delete.
     * @throws Exception - if dataload problem happens
     */
    private void clearClientInstallationData(DatabaseConnection connection,
                                             String             resetSource)
    throws Exception {
        InputStream resetData = this.getClass().getClassLoader()
        .getResourceAsStream(resetSource);

        if (resetData != null) {
            // Because app data references to EPP Tag data, EPP
            // data cannot be cleared if app data is still there. This
            // is safe to check in to EPP, because it won't affect the EPP
            // build.
            IDataSet appTableSet = new FlatXmlDataSet(resetData);
            translateOperation(DatabaseOperation.DELETE_ALL).execute(
                connection, appTableSet);
        }
    }

/**
     * Load the default EPP dataset, returning the database to its original
     * state.
     * @param connection - the db connection
     * @throws Exception on any failure.
     */

    // SHOULD BE PRIVATE (Remove clearSiteCache if changed)
    public void resetEPPInstallationData(DatabaseConnection connection) throws Exception {
        InputStream clearData = this.getClass().getClassLoader()
        .getResourceAsStream("epp-reset-data.xml");
        IDataSet clearTableSet = new FlatXmlDataSet(clearData);
        translateOperation(DatabaseOperation.DELETE_ALL).execute(
            connection, clearTableSet);

        InputStream installData = this.getClass().getClassLoader()
        .getResourceAsStream("epp-install-data.xml");
        this.dataSet = createNullReplacementDataSet(installData);
        translateOperation(DatabaseOperation.CLEAN_INSERT).execute(
            connection, this.dataSet);

        InputStream systemTranslationData = this.getClass().getClassLoader()
                .getResourceAsStream("epp-install-systemdata.xml");
        this.dataSet = createNullReplacementDataSet(systemTranslationData);
        translateOperation(DatabaseOperation.CLEAN_INSERT).execute(connection,
                this.dataSet);

        InputStream translationData = this.getClass().getClassLoader()
                .getResourceAsStream("epp-install-translationdata.xml");
        this.dataSet = createNullReplacementDataSet(translationData);
        translateOperation(DatabaseOperation.CLEAN_INSERT).execute(connection,
                this.dataSet);
        
        InputStream modifiedData = this.getClass().getClassLoader()
            .getResourceAsStream("epp-install-copied-modified-data.xml");
        this.dataSet = createNullReplacementDataSet(modifiedData);
        translateOperation(DatabaseOperation.CLEAN_INSERT).
        execute(connection, this.dataSet);
        clearSiteCache();
    }

    /**
     * @param connection the database connection
     * @throws Exception on any failure.
     */
    public void loadClientInstallationData(DatabaseConnection connection) throws Exception {
        loadClientInstallationData(connection, "install-data-voicelink.xml");
        loadClientInstallationData(connection, "install-data-voiceconsole.xml");
        loadClientInstallationData(connection, "install-copied-properties-voiceconsole.xml", DatabaseOperation.REFRESH);
        loadClientInstallationData(connection, "install-data-licensemanager.xml");
        
    }

    /**
     * @param connection the database connection
     * @param fileName name of the file to load from classpath
     * @param dbOperation type of database operation
     * @throws Exception on any failure.
     */
    protected void loadClientInstallationData(DatabaseConnection connection,
                                              String fileName, DatabaseOperation dbOperation) throws Exception {
        InputStream installData = this.getClass().getClassLoader()
        .getResourceAsStream(fileName);
        if (installData != null) {
            // If app installation data is visible, add it, too. This
            // is safe to check in to EPP, because it won't affect the EPP
            // build.
            IDataSet appTableSet = createNullReplacementDataSet(installData);
            // Can't CLEAN_INSERT here or EPP data will be wiped out.
            translateOperation(dbOperation).execute(
                connection, appTableSet);
        }
        clearSiteCache();
    }
    
    /**
     * @param connection the database connection
     * @param fileName name of the file to load from classpath
     * @throws Exception on any failure.
     */
    protected void loadClientInstallationData(DatabaseConnection connection, String fileName) throws Exception {
        loadClientInstallationData(connection, fileName, DatabaseOperation.INSERT);
    }

    /**
     * Load the default EPP dataset, returning the database to its original
     * state.
     * @throws Exception on any failure.
     */
    public void resetInstallationData() throws Exception {
        DatabaseConnection connection = (DatabaseConnection) getConnection();
        Statement stmt = null;
        try {
            if (isHSQLDatabase()) {
                // Must turn ref integrity off when clearing data in HSQL.
                Connection conn = connection.getConnection();
                stmt = conn.createStatement();
                stmt.executeUpdate("SET DATABASE REFERENTIAL INTEGRITY FALSE");
            }
            clearClientInstallationData(connection);
            resetEPPInstallationData(connection);

            if (isHSQLDatabase()) {
                // Turn ref integrity back on.
                stmt.executeUpdate("SET DATABASE REFERENTIAL INTEGRITY TRUE");
            }

            loadClientInstallationData(connection);

        } catch (Exception e) {
            log.error("Installation reset failed", SystemErrorCode.ENTITY_NOT_FOUND, e);
            if (isHSQLDatabase()) {
                // Fail-safe to turn ref integrity back on in error situation.
                if (stmt != null) {
                    stmt.executeUpdate("SET DATABASE REFERENTIAL INTEGRITY TRUE");
                }
            }
        } finally {
            connection.close();
            clearSiteCache();
        }
    }

    /**
     * This method uses the default operation to process the
     * specified test resource.  The resource must conform to the
     * 'flat xml' type.
     * @param resourceName name of the file to be processed.
     * @throws Exception if an error occurs during processing
     */
    public void handleFlatXmlResource(String resourceName) throws Exception {
        handleFlatXmlResource(resourceName, this.defaultOperation);

    }

    /**
     * This method uses the given db operation to process the specified
     * test resource. The resource must conform to the 'flat xml' type.
     * @param resourceName specifies a flat xml dataset file
     * @param dbOperation specifies the database operation to invoke
     * @throws Exception if an error occurs during processing
     */
    public void handleFlatXmlResource(String resourceName, DatabaseOperation dbOperation) throws Exception {
        DatabaseConnection connection = (DatabaseConnection) getConnection();
        InputStream resourceStream =
            this.getClass().getClassLoader().getResourceAsStream(resourceName);
        this.dataSet = createNullReplacementDataSet(resourceStream);
        try {
            translateOperation(dbOperation).execute(connection, this.dataSet);
        } finally {
            connection.close();
            clearSiteCache();
        }
    }

    /**
     * This method uses the default operation to process the
     * specified Excel spreadsheet.
     * @param resourceName name of the Excel spreadsheet to be processed.
     * @throws Exception if an error occurs during processing
     */
    public void handleXlsResource(String resourceName) throws Exception {
        handleXlsResource(resourceName, this.defaultOperation);
    }

    /**
     * This method uses the given db operation to process the specified
     * Excel spreadsheet.
     * @param resourceName specifies an Excel spreadsheet containing
     * data to be processed
     * @param dbOperation specifies the database operation to invoke
     * @throws Exception if an error occurs during processing
     */
    public void handleXlsResource(String resourceName, DatabaseOperation dbOperation) throws Exception {
        DatabaseConnection connection = (DatabaseConnection) getConnection();
        InputStream resourceStream =
            this.getClass().getClassLoader().getResourceAsStream(resourceName);
        this.dataSet = new XlsDataSet(resourceStream);

        try {
            translateOperation(dbOperation).execute(connection, this.dataSet);
        } finally {
            connection.close();
            clearSiteCache();
        }
    }

    /**
     * This method opens database.properties and parses its contents into a
     * Properties object.  These properties are used to create database
     * connections to enable DbUnit functionality.
     * @return database connection properties.
     */
    private static Properties getDbProperties() {

        Properties props = null;

        try {
            // Get the database.properties file as an input stream.
            // (expect this file in <project_root>/src).
            InputStream dbPropsFile =
                DbUnitAdapter.class.getClassLoader().getResourceAsStream("database.properties");
            // Load and return the database properties.
            props = new Properties();
            props.load(dbPropsFile);
            dbPropsFile.close();
        } catch (Exception e) {
            log.warn("*********************************************");
            log.warn("Error occured while trying to read database.properties.");
            e.printStackTrace();
            log.warn("*********************************************");
        }
        return props;
    }

    /**
     * @return true if this is an Oracle database, false otherwise.
     */
    public boolean isOracleDatabase() {
        return dbProps.getProperty("hibernate.connection.driver_class").
            contains("OracleDriver");
    }

    /**
     * @return true if this is a SQLServer database, false otherwise.
     */
    public boolean isSQLServerDatabase() {
        return dbProps.getProperty("hibernate.connection.driver_class").
            contains("SQLServerDriver");
    }

    /**
     * @return .
     */
    public boolean isHSQLDatabase() {
        return dbProps.getProperty("hibernate.connection.driver_class").
            contains("hsqldb");
    }

    /**
     * @return .
     */
    public boolean isH2Database() {
        return dbProps.getProperty("hibernate.connection.driver_class").
            contains("org.h2");
    }


    /**
     * Translate the operation to SQLServer terms if SQLServer is being used.
     * @param operation the desired operation
     * @return the translated operation, or the same if no translation needed.
     */
    protected DatabaseOperation translateOperation(DatabaseOperation operation) {
        // If using SQLServer, translate operation to SQLServer equivalent
        // if there is one. Otherwise, just return same.
        if (isSQLServerDatabase()) {
            if (operation == DatabaseOperation.CLEAN_INSERT) {
                return InsertIdentityOperation.CLEAN_INSERT;
            } else if (operation == DatabaseOperation.REFRESH) {
                return InsertIdentityOperation.REFRESH;
            } else if (operation == DatabaseOperation.INSERT) {
                return InsertIdentityOperation.INSERT;
            } else {
                return operation;
            }
        }
        return operation;

    }

    /**
     * Create the DataSet for the operation.
     * <p>
     * Important! Any replacement token changes made here must also be
     * made to <code>EppDbUnitOperation</code>, and, if the data is
     * installation data, to <code>DbUnitInstallerAdapter.</code>
     * @param stream the data input stream
     * @return a ReplacementDataSet that converts certain tokens in the input
     * stream.
     * @throws DataSetException on failure to create data set
     * @throws IOException on failure to read from stream
     */
    protected IDataSet createNullReplacementDataSet(InputStream stream)
        throws DataSetException, IOException {

        ReplacementDataSet newDataSet = new ReplacementDataSet(new FlatXmlDataSet(stream));
        return DbUnitUtil.createReplacementDataSet(newDataSet);
    }

    /**
     * The SiteContext list of sites will only refresh if it is null.  Every
     * time the database is updated with new data, the sites need to be
     * refreshed in case sites were removed or added.
     * @throws DataAccessException error accessing site context
     */
    private void clearSiteCache() throws DataAccessException {
        if (SiteContextHolder.getSiteContext() != null) {
            SiteContextHolder.getSiteContext().clearSiteCache();
        }
    }

}
