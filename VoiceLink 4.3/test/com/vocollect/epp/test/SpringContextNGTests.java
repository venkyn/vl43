/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.test;

import com.vocollect.epp.logging.Logger;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.StringUtils;
import org.testng.Assert;

/**
 * Superclass for TestNG test cases using a Spring context.
 *
 * <p>Maintains a static cache of contexts by key. This has significant performance
 * benefit if initializing the context would take time. While initializing a
 * Spring context itself is very quick, some beans in a context, such as
 * a LocalSessionFactoryBean for working with Hibernate, may take time to
 * initialize. Hence it often makes sense to do that initializing once.
 *
 * <p>Normally you won't extend this class directly, but extend one
 * of its subclasses.
 *
 * <p>Original code taken from Open Source CVS repository.
 *    (org.springframework.test.AbstractSpringContextTests v1.9)
 *
 * @author Sreenivas Cheekala
 * @see DepInjectionSpringContextNGTests
 */

public abstract class SpringContextNGTests extends Assert {

    /**
     * Map of context keys returned by subclasses of this class, to Spring
     * Contexts. This needs to be static, as TestNG tests are destroyed and
     * recreated between running individual test methods.
     */
    private static Map contextKeyToContextMap = new HashMap();

    /**
     * Logger available to subclasses.
     */
    protected static final Logger logger = new Logger(SpringContextNGTests.class);

    /**
     * Default constructor for AbstractSpringContextNGTests.
     */
    public SpringContextNGTests() {
    }

    /**
     * Set custom locations dirty. This will cause them to be reloaded from the
     * cache before the next test case is executed.
     * <p>
     * Call this method only if you change the state of a singleton bean,
     * potentially affecting future tests.
     * @param locations the locations to set.
     */
    protected void setDirty(String[] locations) {
        String keyString = contextKeyString(locations);
        ConfigurableApplicationContext ctx =
            (ConfigurableApplicationContext) contextKeyToContextMap
            .remove(keyString);
        if (ctx != null) {
            ctx.close();
        }
    }

    /**
     * @param contextKey the key to look for
     * @return true if the context key is cached, false otherwise.
     */
    protected boolean hasCachedContext(Object contextKey) {
        return contextKeyToContextMap.containsKey(contextKey);
    }

    /**
     * Subclasses can override this to return a String representation of their
     * contextKey for use in logging.
     * @param contextKey the key
     * @return the String rep of the key
     */
    protected String contextKeyString(Object contextKey) {
        if (contextKey instanceof String[]) {
            return StringUtils
                .arrayToCommaDelimitedString((String[]) contextKey);
        } else {
            return contextKey.toString();
        }
    }

    /**
     * Get the application context associated with the specified key.
     * @param key the key
     * @return the context.
     */
    @SuppressWarnings("unchecked")
    protected ConfigurableApplicationContext getContext(Object key) {
        String keyString = contextKeyString(key);
        ConfigurableApplicationContext ctx = (ConfigurableApplicationContext) contextKeyToContextMap
            .get(keyString);
        if (ctx == null) {
            if (key instanceof String[]) {
                ctx = loadContextLocations((String[]) key);
            } else {
                ctx = loadContext(key);
            }
            contextKeyToContextMap.put(keyString, ctx);
        }
        return ctx;
    }

    /**
     * Subclasses can invoke this to get a context key for the given location.
     * This doesn't affect the applicationContext instance variable in this
     * class. Dependency Injection cannot be applied from such contexts.
     * @param locations the locations to load from.
     * @return the context created by reading all the locations.
     */
    protected ConfigurableApplicationContext
        loadContextLocations(String[] locations) {
        if (logger.isInfoEnabled()) {
            logger.info("Loading config for: "
                + StringUtils.arrayToCommaDelimitedString(locations));
        }
        return new ClassPathXmlApplicationContext(locations);
    }

    /**
     * A placeholder for subclasses to override.
     * @param key the key to load context for.
     * @return the associated context
     */
    protected ConfigurableApplicationContext loadContext(Object key) {
        throw new UnsupportedOperationException("Subclasses may override this");
    }

}
