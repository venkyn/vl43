/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.test;


/**
 * This is the enumeration of TestNG groups. It uses String constants instead
 * of an actual enum because the enum is less usable with the @Test annotation.
 *
 * @author ddoubleday
 */
public class TestGroups {

    // SPEED CATEGORIES
    public static final String FAST = "fast";
    public static final String SLOW = "slow";

    // FEATURE CATEGORIES
    public static final String DAO = "dao";
    public static final String DA = "dataaggregator";
    public static final String GUI = "gui";
    public static final String EXCEPTIONS = "exceptions";
    public static final String LOGGING = "logging";
    public static final String SECURITY = "security";
    public static final String SCHEDULER = "scheduler";
    public static final String FILTERING = "filtering";
    public static final String SHIFTCLICK = "shiftclick";

    // ARCHITECTUAL CATEGORIES
    public static final String ACTION = "action";
    public static final String MODEL = "model";
    public static final String TASK = "task";

    // TEST TYPE CATEGORIES
    public static final String UNIT = "unit";
    public static final String SYSTEM = "system";
    public static final String WEBSERVICE = "webservice";
    public static final String VCWEBSERVICE = "VCwebservice";
    public static final String CXFWEBSERVICE = "cxfwebservice";
    public static final String INSTALLER = "installer";
    public static final String ACCEPTANCE = "acceptance";
    public static final String VLACCEPTANCE = "VLacceptance";
    public static final String PHASE3VLACCEPTANCE = "VLPhase3Acceptance";
    public static final String REGRESSION = "regression";
    public static final String ENGLISH_ONLY = "Englishonly";
    public static final String HIGH = "High Priority";
    public static final String MEDIUM = "Medium Priority";
    public static final String LOW = "Low Priority";
    public static final String DBREFRESHNEW = "DBRefresh";
    public static final String ACCEPTANCE_GROUP1 = "AcceptanceGroup1";
    public static final String ACCEPTANCE_GROUP2 = "AcceptanceGroup2";
    public static final String ACCEPTANCE_GROUP3 = "AcceptanceGroup3";

    // SYSTEM TEST CATEGORIES
    public static final String SYSTEM_CONFIG = "system_config";
    public static final String SYSTEM_CONFIG2 = "system_config2";

    // PERFORMANCE CATEGORIES
    public static final String PERFORMANCE = "performance";

    // CONCURRENCY CATAGORIES
    public static final String CONCURRENCY = "concurrency";

    // SITE AREAS
    public static final String DASHBOARD = "dashboards";
    public static final String CHART = "charts";
    public static final String ALERT = "alerts";
    public static final String ALERTCRITERIA = "alertCriterias";
    public static final String SHIFT = "shifts";
    public static final String REGIONS = "regions";
    public static final String VSC = "VehicleSafetyCheck";
    public static final String EJS = "ExternalJobScheduler";
    public static final String LOCATIONS = "locations";
    public static final String ITEMS = "items";
    public static final String CONTAINERS = "containers";
    public static final String OPERATORS = "operators";
    public static final String BREAKTYPES = "breaktypes"; 
    public static final String ASSIGNMENTS = "assignments";
    public static final String SHORTS = "shorts";
    public static final String LABOR = "labor";
    public static final String PRINTERS = "printers";
    public static final String WORKGROUPS = "workgroups";
    public static final String USERS = "users";
    public static final String ROLES = "roles";
    public static final String LOGS = "logs";
    public static final String SCHEDULES = "schedules";
    public static final String NOTIFICATIONS = "notifications";
    public static final String SYSTEMCONFIG = "systemconfig";
    public static final String SEARCH = "search";
    public static final String SITES = "sites";
    public static final String HOMEPAGE = "homepage";
    public static final String LICENSING = "licensing";
    public static final String REPORTS = "reports";

    public static final String SYSTEM_CONFIG_MULTITHREADED = "multithreaded";

    // MESSAGE KEYS CONFIRMATION GROUPS
    public static final String MESSAGEKEYS = "messagekeys";
    public static final String MESSAGEKEYSCONFIG = "messagekeysconfig";
    public static final String MKEPPHOME = "mkepphome";
    public static final String MKEPPUSERS = "mkeppusers";
    public static final String MKEPPROLES = "mkepproles";
    public static final String MKEPPLOGS = "mkepplogs";
    public static final String MKEPPSCHEDULES = "mkeppschedules";
    public static final String MKEPPSITES = "mkeppsites";
    public static final String MKEPPNOTIFICATIONS = "mkeppnotifications";
    public static final String MKEPPLICENSING = "mkepplicensing";
    public static final String MKEPPSYSTEMCONFIG = "mkeppsystemconfig";
    public static final String MKEPPABOUT = "mkeppabout";
    public static final String MKEPPPROFILE = "mkeppprofile";
    public static final String MKEPPSEARCH = "mkeppsearch";
    public static final String MKEPPERRORS = "mkepperrors";
    public static final String MKEPPLOGIN = "mkepplogin";
    public static final String MKVLSELHOME = "mkvlselhome";
    public static final String MKVLSELASSIGNMENTS = "mkvlselassignments";
    public static final String MKVLSELLABOR = "mkvlsellabor";
    public static final String MKVLSELSHORTS = "mkvlselshorts";
    public static final String MKVLSELBREAKTYPES = "mkvlselbreaktypes";
    public static final String MKVLSELCONTAINERS = "mkvlselcontainers";
    public static final String MKVLSELDELMAPPINGS = "mkvlseldelmappings";
    public static final String MKVLSELITEMS = "mkvlselitems";
    public static final String MKVLSELITEMLOCMAPPINGS = "mkvlselitemlocmappings";
    public static final String MKVLSELLOCATIONS = "mkvlsellocations";
    public static final String MKVLSELOPERATORS = "mkvlseloperators";
    public static final String MKVLSELPRINTERS = "mkvlselprinters";
    public static final String MKVLSELREGIONS = "mkvlselregions";
    public static final String MKVLSELREPORTS = "mkvlselreports";
    public static final String MKVLSELSUMPROMPTS = "mkvlselsumprompts";
    public static final String MKVLSELWORKGROUPS = "mkvlselworkgroups";
    public static final String MKVLPAHOME = "mkvlpahome";
    public static final String MKVLPALICENSES = "mkvlpalicenses";
    public static final String MKVLPALICENSEDETAILS = "mkvlpalicensedetails";
    public static final String MKVLPALABOR = "mkvlpalabor";
    public static final String MKVLPABREAKTYPES = "mkvlpabreaktypes";
    public static final String MKVLPAITEMS = "mkvlpaitems";
    public static final String MKVLPALOCATIONS = "mkvlpalocations";
    public static final String MKVLPAOPERATORS = "mkvlpaoperators";
    public static final String MKVLPAREASONCODES = "mkvlpareasoncodes";
    public static final String MKVLPAREGIONS = "mkvlparegions";
    public static final String MKVLPAWORKGROUPS = "mkvlpaworkgroups";
    public static final String MKVLREPHOME = "mkvlrephome";
    public static final String MKVLREPASSIGNMENTS = "mkvlrepassignments";
    public static final String MKVLREPASSIGNMENTDETAILS = "mkvlrepassignmentdetails";
    public static final String MKVLREPLABOR = "mkvlreplabor";
    public static final String MKVLREPBREAKTYPES = "mkvlrepbreaktypes";
    public static final String MKVLREPITEMS = "mkvlrepitems";
    public static final String MKVLREPLOCATIONS = "mkvlreplocations";
    public static final String MKVLREPOPERATORS = "mkvlrepoperators";
    public static final String MKVLREPREASONCODES = "mkvlrepreasoncodes";
    public static final String MKVLREPREGIONS = "mkvlrepregions";
    public static final String MKVLREPWORKGROUPS = "mkvlrepworkgroups";
    public static final String MKVLLLHOME = "mkvlllhome";
    public static final String MKVLLLROUTESTOPS = "mkvlllroutestops";
    public static final String MKVLLLPALLETS = "mkvlllpallets";
    public static final String MKVLLLCARTONS = "mkvlllcartons";
    public static final String MKVLLLLABOR = "mkvllllabor";
    public static final String MKVLLLOPERATORS = "mkvllloperators";
    public static final String MKVLLLREGIONS = "mkvlllregions";
    public static final String MKVLLLWORKGROUPS = "mkvlllworkgroups";

}
