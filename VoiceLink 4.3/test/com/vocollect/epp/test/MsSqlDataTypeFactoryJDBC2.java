/*
 * Copyright (c) 2008 Vocollect, Inc. Pittsburgh, PA 15235 All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.test;

import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.datatype.DataTypeException;
import org.dbunit.ext.mssql.MsSqlDataTypeFactory;

/**
 * DataTypeFactory for MsSqlServer is implemented in later versions of DbUnit,
 * but we're still on 2.1. The most recent version (2.4.7) still has an issue
 * with NCHAR so we'd still need to define it as -15.
 *
 * @author treed
 */
public class MsSqlDataTypeFactoryJDBC2 extends MsSqlDataTypeFactory {

    public static final int NCHAR = -8;

    public static final int NVARCHAR = -9;

    public static final int NTEXT = -10;

    public static final int NTEXT_MSSQL_2005 = -16;

    public static final int NCHAR_JDBC2 = -15;

    /**
     * {@inheritDoc}
     * @see org.dbunit.ext.mssql.MsSqlDataTypeFactory#createDataType(int,
     *      java.lang.String)
     */
    @Override
    public DataType createDataType(int sqlType, String sqlTypeName)
        throws DataTypeException {

        // TODO : Process MS SQL Server custom datatype here
        switch (sqlType) {
        case NCHAR:
            return DataType.CHAR; // nchar
        case NVARCHAR:
            return DataType.VARCHAR; // nvarchar
        case NTEXT:
            return DataType.LONGVARCHAR; // ntext
        case NTEXT_MSSQL_2005:
            return DataType.LONGVARCHAR; // ntext
        case NCHAR_JDBC2:
            return DataType.CHAR; // ntext
        default:
            return super.createDataType(sqlType, sqlTypeName);
        }
    }
}
