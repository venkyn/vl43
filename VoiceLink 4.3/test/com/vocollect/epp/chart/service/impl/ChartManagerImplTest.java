/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.chart.service.impl;

import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.model.ChartDataField;
import com.vocollect.epp.chart.model.ChartType;
import com.vocollect.epp.chart.service.ChartManager;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.dashboard.model.DashboardDetail;
import com.vocollect.epp.dashboard.service.DashboardDetailManager;
import com.vocollect.epp.dashboard.service.DashboardManager;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.dataaggregator.service.DAColumnManager;
import com.vocollect.epp.dataaggregator.service.DAInformationManager;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.service.impl.BaseServiceTestCase;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.SiteContextHolder;

import static com.vocollect.epp.test.TestGroups.CHART;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit test class for testing chart manager.
 * @author smittal
 * 
 */
@Test(groups = { CHART })
public class ChartManagerImplTest extends BaseServiceTestCase {

    private ChartManager chartManager = null;

    private DAInformation daInformation = null;

    private DAColumn daColumn = null;

    private SiteManager siteManager = null;

    private DAInformationManager daInformationManager = null;

    private DashboardManager dashboardManager = null;

    private DAColumnManager daColumnManager = null;
    
    private DashboardDetailManager dashboardDetailManager = null;
    
    /**
     * @throws Exception
     */
    @BeforeClass
    public void classSetup() throws Exception {
        
        DbUnitAdapter adapter = new DbUnitAdapter();
        // Reset the database to the default state.
        adapter.resetInstallationData();
        // Replace default users with the 100 user dataset.
        adapter.handleFlatXmlResource("data/dbunit/chart-data.xml");
    }

    /**
     * @return the dashboardDetailManager
     */
    @Test(enabled = false)
    public DashboardDetailManager getDashboardDetailManager() {
        return dashboardDetailManager;
    }

    
    /**
     * @param dashboardDetailManager the dashboardDetailManager to set
     */
    @Test(enabled = false)
    public void setDashboardDetailManager(DashboardDetailManager dashboardDetailManager) {
        this.dashboardDetailManager = dashboardDetailManager;
    }


    /**
     * Set the site context.
     * @param siteId - the site ID
     * @throws Exception
     * 
     */
    @Test(enabled = false)
    protected void setSiteContext(Long siteId) throws Exception {
        this.setUpSiteContextNoDefaultSite(true);
        SiteContextHolder.getSiteContext().setCurrentSite(siteId);
    }

    /**
     * @return the chartManager
     */
    @Test(enabled = false)
    public ChartManager getChartManager() {
        return chartManager;
    }

    /**
     * @param chartManager - the chart manager
     */
    @Test(enabled = false)
    public void setChartManager(ChartManager chartManager) {
        this.chartManager = chartManager;
    }

    /**
     * @return the siteManager
     */
    @Test(enabled = false)
    public SiteManager getSiteManager() {
        return siteManager;
    }

    /**
     * @param siteManager the siteManager to set
     */
    @Test(enabled = false)
    public void setSiteManager(SiteManager siteManager) {
        this.siteManager = siteManager;
    }

    /**
     * @return the daInformationManager
     */
    @Test(enabled = false)
    public DAInformationManager getDaInformationManager() {
        return daInformationManager;
    }

    /**
     * @param daInformationManager the daInformationManager to set
     */
    @Test(enabled = false)
    public void setDaInformationManager(DAInformationManager daInformationManager) {
        this.daInformationManager = daInformationManager;
    }

    /**
     * Getter for the dashboardManager property.
     * @return DashboardManager value of the property
     */
    @Test(enabled = false)
    public DashboardManager getDashboardManager() {
        return dashboardManager;
    }

    /**
     * Setter for the dashboardManager property.
     * @param dashboardManager the new dashboardManager value
     */
    @Test(enabled = false)
    public void setDashboardManager(DashboardManager dashboardManager) {
        this.dashboardManager = dashboardManager;
    }

    /**
     * Getter for the daColumnManager property.
     * @return DAColumnManager value of the property
     */
    @Test(enabled = false)
    public DAColumnManager getDaColumnManager() {
        return daColumnManager;
    }

    /**
     * Setter for the daColumnManager property.
     * @param daColumnManager the new daColumnManager value
     */
    @Test(enabled = false)
    public void setDaColumnManager(DAColumnManager daColumnManager) {
        this.daColumnManager = daColumnManager;
    }

    @Test()
    public void testUniquenessByName() {

        Chart chart1 = new Chart();
        chart1.setName("chart");
        chart1.setDescription("abc");
        chart1.setDaInformation(daInformation);
        chart1.setCategory(daColumn);
        chart1.setType(ChartType.COLUMN);

        try {
            this.chartManager.verifyUniquenessByName(chart1);
        } catch (Exception e) {
            assert (e instanceof FieldValidationException);
            fail(e.getLocalizedMessage(), e);
        }
    }

    /**
     * . Tests the getAll method
     */
    @Test()
    public void testGetAllCharts() {
        try {
            List<Chart> charts = this.chartManager.getAll();

            assertEquals(charts.size(), 15);
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }

    @Test()
    public void testListCharts() throws Exception {
        setSiteContext(-1L);
        List<Chart> charts = this.chartManager.listCharts();

        assertEquals(charts.size(), 15);
    }

    @Test()
    public void testSave() throws Exception {
        setSiteContext(-1L);

        Chart chart = null;
        DAInformation da = null;

        try {
            chart = new Chart();

            chart.setName("saveChart");
            chart.setDescription("chart");
            da = getDaInformationManager().getAll().get(0);
            chart.setDaInformation(da);
            chart.setCategory(da.getColumns().iterator().next());
            chart.setType(ChartType.COLUMN);
            chart.setPivotOn(da.getColumns().iterator().next());
            chart.setPivotFor(da.getColumns().iterator().next());

            this.chartManager.save(chart);

            getChartManager().getPrimaryDAO().flushSession();
            getChartManager().getPrimaryDAO().clearSession();

            List<Chart> charts = getChartManager().getAll();

            // Check 1 of the charts in the system should match the above
            // persisted
            for (Chart chart1 : charts) {
                if (chart1.getName().equals("saveChart")) {
                    assertEquals(chart1.getName(), chart.getName());
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        } finally {
            // Clean
            try {
                getChartManager().delete(chart);
                getChartManager().getPrimaryDAO().flushSession();
                getChartManager().getPrimaryDAO().clearSession();
            } catch (Exception e) {

            }
        }

    }

    @Test()
    public void testSaveWithDataField() throws Exception {
        setSiteContext(-1L);

        Chart chart = null;
        DAInformation da = null;

        try {
            chart = new Chart();

            chart.setName("saveChartWithDataField");
            chart.setDescription("chart");
            da = getDaInformationManager().getAll().get(0);
            DAColumn column = da.getColumns().iterator().next();
            chart.setDaInformation(da);
            chart.setCategory(column);
            chart.setType(ChartType.COLUMN);

            ChartDataField field = new ChartDataField();
            field.setDaColumn(column);
            field.setChart(chart);
            Set<ChartDataField> fields = new HashSet<ChartDataField>();
            fields.add(field);
            chart.setChartDataFields(fields);
            this.chartManager.save(chart);

            getChartManager().getPrimaryDAO().flushSession();
            getChartManager().getPrimaryDAO().clearSession();

            List<Chart> charts = getChartManager().getAll();

            // Check 1 of the charts in the system should match the above
            // persisted
            for (Chart chart1 : charts) {
                if (chart1.getName().equals("saveChartWithDataField")) {
                    assertEquals(chart1.getName(), chart.getName());
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        } finally {
            // Clean
            try {
                getChartManager().delete(chart);
                getChartManager().getPrimaryDAO().flushSession();
                getChartManager().getPrimaryDAO().clearSession();
            } catch (Exception e) {

            }
        }
    }

    @Test()
    public void testChartDelete() throws Exception {
        setSiteContext(-1L);

        Chart chart = null;
        DAInformation da = null;

        chart = new Chart();

        da = getDaInformationManager().getAll().get(0);
        DAColumn column = da.getColumns().iterator().next();

        chart.setName("deleteChart");
        chart.setDescription("chart");
        chart.setDaInformation(da);
        chart.setCategory(da.getColumns().iterator().next());
        chart.setType(ChartType.COLUMN);

        ChartDataField field1 = new ChartDataField();
        field1.setDaColumn(column);
        field1.setChart(chart);

        ChartDataField field2 = new ChartDataField();
        column = da.getColumns().iterator().next();
        field2.setDaColumn(column);
        field2.setChart(chart);
        Set<ChartDataField> fields = new HashSet<ChartDataField>();
        fields.add(field1);
        fields.add(field2);
        chart.setChartDataFields(fields);

        this.chartManager.save(chart);
        assertNotNull(this.chartManager.get(chart.getId()));
        assertEquals(this.chartManager.getAll().size(), 16);

        this.chartManager.delete(chart.getId());
        assertEquals(this.chartManager.getAll().size(), 15);

    }

    @Test()
    public void testAssociatedChartDelete() throws Exception {
        setSiteContext(-1L);

        Chart chart = this.chartManager.get(-2L);

        assertNotNull(chart);
        assertEquals(this.chartManager.getAll().size(), 15);

        Dashboard dashboard = null;
        Set<DashboardDetail> details = new HashSet<DashboardDetail>();
        try {
            dashboard = new Dashboard();
            dashboard.setName("dashboard");
            dashboard.setDescription("abc");

            this.dashboardManager.save(dashboard);

            DashboardDetail detail = new DashboardDetail();
            detail.setDashboard(dashboard);
            detail.setParentChart(chart);
            detail.setDrillDownChart(null);
            details.add(detail);

            chart.setDashboardDetails(details);

            this.chartManager.save(chart);

            assertNotNull(this.chartManager.get(chart.getId()));
            assertEquals(this.chartManager.get(chart.getId())
                .getDashboardDetails().size(), 1);
            assertEquals(this.chartManager.getAll().size(), 15);

            this.chartManager.delete(chart.getId());
        } catch (DataAccessException e) {
            UserMessage message = e.getUserMessage();
            String str = message.getKey();
            assertTrue(str == "chart.delete.error.dashboard.inUse");
        }
    }

    @Test()
    public void testListFirstLevelParentCharts() throws Exception {
        setSiteContext(-1L);

        Chart parentChart = this.chartManager.get(-3L);

        assertNotNull(parentChart);
        assertEquals(this.chartManager.getAll().size(), 15);

        Dashboard dashboard = new Dashboard();
        dashboard.setName("dashboard1");
        dashboard.setDescription("abc");

        this.dashboardManager.save(dashboard);

        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, null, null);

        assertEquals(this.chartManager.listParentCharts(dashboard).size(), 1);

        Chart childChart = this.chartManager.get(-1L);
        DAColumn linkedDrillDownField = this.getDaColumnManager().get(-1L);
        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, childChart, linkedDrillDownField);

        assertEquals(this.chartManager.listParentCharts(dashboard).size(), 1);
    }

    @Test()
    public void testListDrillDownCharts() throws Exception {
        setSiteContext(-1L);

        Chart parentChart = this.chartManager.get(-4L);

        assertNotNull(parentChart);
        assertEquals(this.chartManager.getAll().size(), 15);

        Dashboard dashboard = new Dashboard();
        dashboard.setName("dashboard2");
        dashboard.setDescription("abc");

        this.dashboardManager.save(dashboard);

        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, null, null);

        assertEquals(
            this.chartManager.listDrillDownChartsForDropdown(dashboard,
                parentChart).size(), 14);

        Chart childChart = this.chartManager.get(-1L);
        DAColumn linkedDrillDownField = this.getDaColumnManager().get(-1L);

        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, childChart, linkedDrillDownField);

        assertEquals(
            this.chartManager.listDrillDownChartsForDropdown(dashboard,
                parentChart).size(), 13);
    }

    @Test()
    public void testAssociatedDashboardCount() throws Exception {
        setSiteContext(-1L);

        Chart chart = this.chartManager.get(-5L);

        assertNotNull(chart);
        assertEquals(this.chartManager.getAll().size(), 15);

        Dashboard dashboard1 = new Dashboard();
        dashboard1.setName("dashboard3");
        dashboard1.setDescription("abc");

        this.dashboardManager.save(dashboard1);

        this.dashboardManager.executeAddChartToDashboard(dashboard1, chart,
            null, null);

        assertEquals(getChartManager().getPrimaryDAO()
            .countAssociatedDashboards(chart).toString(), "1");

        Dashboard dashboard2 = new Dashboard();
        dashboard2.setName("dashboard3");
        dashboard2.setDescription("abc");

        this.dashboardManager.save(dashboard2);

        this.dashboardManager.executeAddChartToDashboard(dashboard2, chart,
            null, null);

        assertEquals(getChartManager().getPrimaryDAO()
            .countAssociatedDashboards(chart).toString(), "2");
    }

    @Test()
    public void testListChildChartsOfParentChart() throws Exception {
        setSiteContext(-1L);

        Chart parentChart = this.chartManager.get(-6L);

        assertNotNull(parentChart);
        assertEquals(this.chartManager.getAll().size(), 15);

        Dashboard dashboard = new Dashboard();
        dashboard.setName("dashboard4");
        dashboard.setDescription("abc");

        this.dashboardManager.save(dashboard);

        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, null, null);

        assertEquals(this.chartManager.listChildCharts(dashboard, parentChart)
            .size(), 0);

        Chart childChart = this.chartManager.get(-7L);
        DAColumn linkedDrillDownField = this.getDaColumnManager().get(-1L);

        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, childChart, linkedDrillDownField);

        assertEquals(this.chartManager.listChildCharts(dashboard, parentChart)
            .size(), 1);
    }

    @Test()
    public void testListAllParentCharts() throws Exception {
        setSiteContext(-1L);

        Chart parentChart = this.chartManager.get(-8L);

        assertNotNull(parentChart);
        assertEquals(this.chartManager.getAll().size(), 15);

        Dashboard dashboard = new Dashboard();
        dashboard.setName("dashboard1");
        dashboard.setDescription("abc");

        this.dashboardManager.save(dashboard);

        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, null, null);

        assertEquals(
            this.chartManager.listParentChartsForParentDropdown(dashboard)
                .size(), 1);

        Chart childChart = this.chartManager.get(-9L);
        DAColumn linkedDrillDownField = this.getDaColumnManager().get(-1L);

        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, childChart, linkedDrillDownField);

        assertEquals(
            this.chartManager.listParentChartsForParentDropdown(dashboard)
                .size(), 1);

        Chart childOfChildChart = this.chartManager.get(-10L);
        this.dashboardManager.executeAddChartToDashboard(dashboard, childChart,
            childOfChildChart, linkedDrillDownField);

        assertEquals(
            this.chartManager.listParentChartsForParentDropdown(dashboard)
                .size(), 2);
    }

    @Test()
    public void testListAllChildCharts() throws Exception {
        setSiteContext(-1L);

        Chart parentChart = this.chartManager.get(-11L);

        assertNotNull(parentChart);
        assertEquals(this.chartManager.getAll().size(), 15);

        Dashboard dashboard = new Dashboard();
        dashboard.setName("dashboard1");
        dashboard.setDescription("abc");

        this.dashboardManager.save(dashboard);

        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, null, null);

        assertEquals(
            this.chartManager.listChildChartsForParentDropdown(dashboard)
                .size(), 0);

        Chart childChart = this.chartManager.get(-12L);
        DAColumn linkedDrillDownField = this.getDaColumnManager().get(-1L);

        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, childChart, linkedDrillDownField);

        assertEquals(
            this.chartManager.listChildChartsForParentDropdown(dashboard)
                .size(), 1);

        Chart childOfChildChart = this.chartManager.get(-13L);
        this.dashboardManager.executeAddChartToDashboard(dashboard, childChart,
            childOfChildChart, linkedDrillDownField);

        assertEquals(
            this.chartManager.listChildChartsForParentDropdown(dashboard)
                .size(), 2);
    }

    @Test()
    public void testGetChartJSON() throws Exception {
        setSiteContext(-1L);

        Chart chart = this.chartManager.get(-14L);

        assertNotNull(chart);
        assertEquals(this.chartManager.getAll().size(), 15);

        Dashboard dashboard = new Dashboard();
        dashboard.setName("dashboard1");
        dashboard.setDescription("abc");

        this.dashboardManager.save(dashboard);

        JSONObject chartJSON = this.chartManager.getChartJSON(dashboard, chart, new JSONArray());

        assertEquals((Long) chartJSON.get("id"), chart.getId());
        assertEquals(chartJSON.getString("name"), chart.getName());
        assertEquals(chartJSON.getString("description"), chart.getDescription());
        assertEquals(ChartType.valueOf(chartJSON.getString("type")),
            chart.getType());
        assertEquals(
            ((JSONObject) chartJSON.get("category")).getString("fieldId"),
            chart.getCategory().getFieldId());
        assertEquals(((JSONArray) chartJSON.get("fields")).length(), 0);
        assertEquals(((JSONArray) chartJSON.get("childChartIds")).length(), 0);
    }

    @Test()
    public void testGetChartData() throws Exception {
        setSiteContext(-1L);

        Chart chart = this.chartManager.get(-14L);

        assertNotNull(chart);
        assertEquals(this.chartManager.getAll().size(), 15);

        JSONArray aggregatorData = this.chartManager.executeChartDataAggregator(chart, null);
        String chartData = this.chartManager.getChartData(chart, aggregatorData).getJSONObject(0).toString();
        assertNotNull(chartData);
        String expectedData = "{\"column8\":22.24,\"column7\":\"14/5/74 1:41:15 PM EDT\",\"column1\":1,\"column0\":0,"
            + "\"column2\":2,\"column5\":\"07:00\",\"column4\":\"Test column 5\"}";
        assertEquals(chartData, expectedData);
    }
    
    @Test()
    public void testDrillDownData() throws Exception {
        setSiteContext(-1L);

        Chart parentChart = this.chartManager.get(-14L);

        assertNotNull(parentChart);
        assertEquals(this.chartManager.getAll().size(), 15);

        Dashboard dashboard = new Dashboard();
        dashboard.setName("dashboard1");
        dashboard.setDescription("abc");

        this.dashboardManager.save(dashboard);

        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, null, null);

        Chart childChart = this.chartManager.get(-15L);
        DAColumn linkedDrillDownField = this.getDaColumnManager().get(-2L);

        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, childChart, linkedDrillDownField);

        JSONArray aggregatorData = this.chartManager.executeChartDataAggregator(parentChart, dashboard.getTimeFilter());
        JSONArray parentChartData = this.chartManager.getChartData(parentChart, aggregatorData);
        assertNotNull(parentChartData);
        String parentSelectedFieldValue = parentChartData.getJSONObject(0)
            .getString(parentChart.getCategory().getFieldId());

        JSONArray drillDownFilterParameters = new JSONArray();
        JSONObject filterParam = new JSONObject();
        filterParam.put("chartid", parentChart.getId());
        filterParam.put("fieldid", parentChart.getCategory().getFieldId());
        filterParam.put("fieldvalue", parentSelectedFieldValue);
        drillDownFilterParameters.put(filterParam);

        JSONArray drillDownChartAggregatorData = this.chartManager
            .executeChartDataAggregator(childChart, dashboard.getTimeFilter());
        JSONArray drillDownChartData = this.chartManager
            .fetchDrillDownChartData(dashboard, childChart,
                drillDownFilterParameters, drillDownChartAggregatorData);
        assertNotNull(drillDownChartData);
        assertEquals(parentChartData.toString(), drillDownChartData.toString());
    }
    

    @Test()
    public void testForceDelete() throws Exception {
        setSiteContext(-1L);

        Dashboard dashboard = new Dashboard();
        dashboard.setName("dashboard1");
        dashboard.setDescription("abc");
        this.dashboardManager.save(dashboard);

        Chart parentChart =  new Chart();

        parentChart.setName("saveChartWithDataField");
        parentChart.setDescription("chart");
        DAInformation da = getDaInformationManager().getAll().get(0);
        DAColumn column = da.getColumns().iterator().next();
        parentChart.setDaInformation(da);
        parentChart.setCategory(column);
        parentChart.setType(ChartType.COLUMN);
        
        this.chartManager.save(parentChart);
        
        Long pId = parentChart.getId();
        assertNotNull(parentChart);
        
        Chart childChart = this.chartManager.get(-12L);
        DAColumn linkedDrillDownField = this.getDaColumnManager().get(-1L);

        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, null, null);
        
        this.dashboardManager.executeAddChartToDashboard(dashboard,
            parentChart, childChart, linkedDrillDownField);
        
        Chart childChart2 = this.chartManager.get(-13L);
        this.dashboardManager.executeAddChartToDashboard(dashboard,
            childChart, childChart2, linkedDrillDownField);
        
        //Setup another dashboard with different drill-down configurations
        Dashboard dashboard2 = new Dashboard();
        dashboard2.setName("dashboard2");
        dashboard2.setDescription("xyz");
        this.dashboardManager.save(dashboard2);

        Chart pChart = this.chartManager.get(-13L);
        assertNotNull(pChart);
        Chart cChart1 = this.chartManager.get(-11L);

        this.dashboardManager.executeAddChartToDashboard(dashboard2,
            pChart, null, null);
        
        this.dashboardManager.executeAddChartToDashboard(dashboard2,
            pChart, cChart1, linkedDrillDownField);
        
        Chart cChart2 = this.chartManager.get(-12L);
        this.dashboardManager.executeAddChartToDashboard(dashboard2,
            cChart1, cChart2, linkedDrillDownField);


        //Start to delete the charts now
        this.chartManager.forceDelete(parentChart);
        
        //Dashboard xyz should now have only Chart 13 and no drilldowns
        assertTrue(dashboardDetailManager.checkDashboardAndChartExist(dashboard2, pChart));
        
        dashboard = dashboardManager.get(dashboard.getId());
        assertEquals(dashboard2.getDetails().size(), 0);

        try {
            parentChart = null;
            parentChart = this.chartManager.get(pId);
            
        } catch (EntityNotFoundException en) {
            assertNull(parentChart);
            return;
        }

        fail();
    }
}
