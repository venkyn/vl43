/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.chart.model;

import com.vocollect.epp.dashboard.model.Dashboard;

import static com.vocollect.epp.test.TestGroups.CHART;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * 
 * 
 * @author smittal
 */
@Test(groups = { FAST, MODEL, CHART })
public class UserChartPreferenceTest {

    /**
     * . Test the hash code generates equal number
     */
    @Test()
    public void testHashCodeEqual() {

        UserChartPreference preference1 = new UserChartPreference();
        UserChartPreference preference2 = preference1;

        assertEquals(preference1.hashCode(), preference2.hashCode());
    }

    
    /**
     * . Tests two types are equal
     */
    @Test()
    public void testEqual() {
        Dashboard dashboard = new Dashboard();
        Chart chart = new Chart();

        UserChartPreference preference1 = new UserChartPreference();
        preference1.setDashboard(dashboard);
        preference1.setChart(chart);

        UserChartPreference preference2 = new UserChartPreference();
        preference2.setDashboard(dashboard);
        preference2.setChart(chart);

        assertEquals("Types should be equal", preference1.equals(preference2),
            true);
    }

    
}
