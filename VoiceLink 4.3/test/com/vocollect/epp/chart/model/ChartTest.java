/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.chart.model;

import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAInformation;

import static com.vocollect.epp.test.TestGroups.CHART;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;

import java.util.Set;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * 
 * 
 * @author smittal
 */
@Test(groups = { FAST, MODEL, CHART })
public class ChartTest {

    private DAInformation daInformation = null;

    private DAColumn category = null;
    
    private DAColumn pivotOn = null;
    
    private DAColumn pivotFor = null;
    
    private DAColumn sortBy = null;

    private Set<ChartDataField> chartDataFields = null;

    /**
     * . Test the hash code generates equal number
     */
    @Test()
    public void testHashCodeEqual() {
        
        Chart chart1 = new Chart();
        Chart chart2 = new Chart();

        chart1.setName("chart1");
        chart1.setDescription("abc");
        chart1.setDaInformation(daInformation);
        chart1.setCategory(category);
        chart1.setChartDataFields(chartDataFields);
        chart1.setType(ChartType.COLUMN);
        chart1.setPivotOn(pivotOn);
        chart1.setPivotFor(pivotFor);
        chart1.setSortBy(sortBy);
        chart1.setSortOrder(null);

        chart2.setName("chart1");
        chart2.setDescription("abc");
        chart2.setDaInformation(daInformation);
        chart2.setCategory(category);
        chart2.setChartDataFields(chartDataFields);
        chart2.setType(ChartType.COLUMN);
        chart2.setPivotOn(pivotOn);
        chart2.setPivotFor(pivotFor);
        chart2.setSortBy(sortBy);
        chart2.setSortOrder(null);

        assertEquals("Hash codes are not same", chart2.hashCode(),
            chart1.hashCode());
    }

    /**
     * . Test the hash code generates different number
     */
    @Test()
    public void testHashCodeDifferent() {
        Chart chart1 = new Chart();
        Chart chart2 = new Chart();

        chart1.setName("chart1");
        chart1.setDescription("abc");
        chart1.setDaInformation(daInformation);
        chart1.setCategory(category);
        chart1.setChartDataFields(chartDataFields);
        chart1.setType(ChartType.COLUMN);
        chart1.setPivotOn(pivotOn);
        chart1.setPivotFor(pivotFor);
        chart1.setSortBy(sortBy);
        chart1.setSortOrder(null);

        chart2.setName("chart2");
        chart2.setDescription("abc");
        chart2.setDaInformation(daInformation);
        chart2.setCategory(category);
        chart2.setChartDataFields(chartDataFields);
        chart2.setType(ChartType.COLUMN);
        chart2.setPivotOn(pivotOn);
        chart2.setPivotFor(pivotFor);
        chart2.setSortBy(sortBy);
        chart2.setSortOrder(null);

        assertEquals("Hash code should be different",
            chart1.hashCode() != chart2.hashCode(), true);
    }

    /**
     * . Tests two types are equal
     */
    @Test()
    public void testEqual() {
        Chart chart1 = new Chart();
        Chart chart2 = new Chart();

        chart1.setName("chart1");
        chart1.setDescription("abc");
        chart1.setDaInformation(daInformation);
        chart1.setCategory(category);
        chart1.setChartDataFields(chartDataFields);
        chart1.setType(ChartType.COLUMN);
        chart1.setPivotOn(pivotOn);
        chart1.setPivotFor(pivotFor);
        chart1.setSortBy(sortBy);
        chart1.setSortOrder(null);

        chart2.setName("chart1");
        chart2.setDescription("abc");
        chart2.setDaInformation(daInformation);
        chart2.setCategory(category);
        chart2.setChartDataFields(chartDataFields);
        chart2.setType(ChartType.COLUMN);
        chart2.setPivotOn(pivotOn);
        chart2.setPivotFor(pivotFor);
        chart2.setSortBy(sortBy);
        chart2.setSortOrder(null);

        assertEquals("Types should be equal", chart1.equals(chart2), true);
    }

    /**
     * . Tests two types are not equal
     */
    @Test()
    public void testNotEqual() {
        Chart chart1 = new Chart();
        Chart chart2 = new Chart();

        chart1.setName("chart1");
        chart1.setDescription("abc");
        chart1.setDaInformation(daInformation);
        chart1.setCategory(category);
        chart1.setChartDataFields(chartDataFields);
        chart1.setType(ChartType.COLUMN);
        chart1.setPivotOn(pivotOn);
        chart1.setPivotFor(pivotFor);
        chart1.setSortBy(sortBy);
        chart1.setSortOrder(null);

        // When compared with other object
        Object obj = new Object();

        assertEquals("Objects should not be equal", chart1.equals(obj), false);

        // Name of one chart is null
        chart2.setName(null);
        chart2.setDescription("abc");
        chart2.setDaInformation(daInformation);
        chart2.setCategory(category);
        chart2.setChartDataFields(chartDataFields);
        chart2.setType(ChartType.COLUMN);
        chart2.setPivotOn(pivotOn);
        chart2.setPivotFor(pivotFor);

        assertEquals("Types should not be equal", chart1.equals(chart2), false);

        // When compared with different charts
        chart2.setName("chart2");
        chart2.setDescription("abc");
        chart2.setDaInformation(daInformation);
        chart2.setCategory(category);
        chart2.setChartDataFields(chartDataFields);
        chart2.setType(ChartType.COLUMN);
        chart2.setPivotOn(pivotOn);
        chart2.setPivotFor(pivotFor);
        chart2.setSortBy(sortBy);
        chart2.setSortOrder(null);

        assertEquals("Types should not be equal", chart1.equals(chart2), false);
    }
}
