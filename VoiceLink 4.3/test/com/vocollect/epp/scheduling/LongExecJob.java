/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling;

import com.vocollect.epp.logging.Logger;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * This class implements a long running job for test purposes.
 *
 * @see org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean
 */
public class LongExecJob extends EPPSpringQuartzJob {

    private static final Logger log = new Logger(LongExecJob.class);

    private static final int SLEEP = 1000;

    private static final int LIMIT = 20;

    private boolean run;

    /**
     * {@inheritDoc}
     * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
     */
    protected void internalExecute(JobExecutionContext context)
        throws JobExecutionException {
        int count = 0;
        run = true;

        try {
            log.debug("Start - " + System.currentTimeMillis());
            while ((run) && (count++ < LIMIT)) {
                log.debug("COUNT ==> " + count);
                Thread.sleep(SLEEP);
            }
        } catch (InterruptedException ie) {
            throw new JobExecutionException(ie);
        } finally {
            log.debug("Running to false...........");
            run = false;
            log.debug("Finish - " + System.currentTimeMillis());
        }
    }

    /**
     * {@inheritDoc}
     * @see org.quartz.InterruptableJob#interrupt()
     */
    protected void internalInterrupt() {
        log.debug("Interrupting job................");
        run = false;
    }
}
