/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.logging.LoggingError;
import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationDetail;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.model.Tag;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.service.TagManager;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * Base class for testing notifications.
 *
 * @author hulrich
 */
public class NotificationJob extends EPPSpringQuartzJob {

    private static final Logger log = new Logger(NotificationJob.class);

    private static final String SITE_LIST = "siteList";

    private static final String PRIORITY = "priority";

    /**
     * NotificationManager is user to create a Notification when an entry is
     * written in Vocollect.err.
     */
    private NotificationManager notificationManager;

    /**
     * Used for getting Tags.
     */
    private TagManager tagManager;

    /**
     * Used for getting hostname.
     */
    private Properties serverProperties;

    private boolean running;

    /**
     * {@inheritDoc}
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    protected void internalExecute(JobExecutionContext ctx)
        throws JobExecutionException {
        try {
            if (!running) {
                running = true;

                List list = (List) ctx.getJobDetail().getJobDataMap().get(
                    SITE_LIST);
                List<Tag> tagList = new ArrayList<Tag>();

                Tag systemTag = getTagManager().get(Tag.SYSTEM);

                if (list == null) {
                    log.warn("Unable to get site ids, using system");
                    tagList.add(systemTag);
                } else {
                    for (int i = 0; i < list.size(); i++) {
                        String s = (String) list.get(i);
                        try {
                            Long id = Long.parseLong(s);
                            log
                                .debug("++++++++++++++ Setting tag with id of --- "
                                    + id);
                            log.debug("tag manager -- " + getTagManager());
                            Tag t = getTagManager().findByTaggedObjectId(
                                Tag.SITE, id);
                            if (t != null) {
                                log.debug("Got the tag -- " + t.getId()
                                    + " -- " + t.getTaggableId());
                            } else {
                                log.debug("The tag is null");
                            }
                            tagList.add(t);
                        } catch (NumberFormatException nfe) {
                            // Badly entered data - just ignore
                            // Go ahead and add the Tag.SYSTEM
                            if (!tagList.contains(systemTag)) {
                                tagList.add(systemTag);
                            }
                        }
                    }
                }

                for (Tag tag : tagList) {

                    Notification notification = new Notification();

                    // Main Notification

                    SiteContext siteContext = SiteContextHolder
                        .getSiteContext();
//                    siteContext.setCurrentTagId(tag.getId());
                    siteContext.setCurrentSite(tag.getTaggableId());
                    SiteContextHolder.setSiteContext(siteContext);

                    Set<Tag> tagSet = new HashSet<Tag>();
                    tagSet.add(tag);
                    notification.setTags(tagSet);

                    notification.setErrorNumber("E"
                        + LoggingError.FILE_IO_ERROR.getErrorCode());

                    notification.setHost(serverProperties
                        .getProperty("server.name"));

                    notification
                        .setApplication("notification.column.keyname.Application.0");

                    notification
                        .setProcess("notification.column.keyname.Process.0");

                    notification
                        .setMessage("notification.column.keyname.Message.0");

                    // Set the default priority for this notification.
                    NotificationPriority np = NotificationPriority.CRITICAL;
                    // Let the job XML override the default priority.
                    String priority = ctx.getJobDetail().getJobDataMap()
                        .getString(PRIORITY);
                    if (priority != null) {
                        np = NotificationPriority.valueOf(priority);
                    }
                    log.debug("Creating notification with priority " + np);
                    notification.setPriority(np);

                    notification.setCreationDateTime(new Date());

                    notification.setDetails(buildDetails(ctx));
                    log.debug("****************** Saving notification....");
                    getNotificationManager().createNotificationAndEmail(
                        notification);
                    log.debug("Saved....");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new JobExecutionException("DataAccess - " + e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    protected void internalInterrupt() {
        running = false;
    }

    /**
     * @param ctx the job context
     * @return the set of notification details associated with the context.
     */
    private Set<NotificationDetail> buildDetails(JobExecutionContext ctx) {
        Set<NotificationDetail> dtls = new HashSet<NotificationDetail>();
        String det = "det";
        int i = 1;
        boolean cont = true;
        while (cont) {
            String not = ctx.getJobDetail().getJobDataMap().getString(det + i);
            if ((not != null) && (!not.equals(""))) {
                NotificationDetail detail = new NotificationDetail();
                String[] st = not.split(",");
                if (st.length == 2) {
                    log.debug(st[0] + " == " + st[1]);
                    detail.setKey(st[0].trim());
                    detail.setValue(st[1].trim());
                    detail.setRenderType("1");
                    detail.setOrdering("" + (i - 1));
                    dtls.add(detail);
                }
            } else {
                cont = false;
            }
            i++;
        }
        log.debug("Returning " + dtls.size() + " details");
        return dtls;
    }

    /**
     * Getter for the notificationManager property.
     * @return NotificationManager value of the property
     */
    public NotificationManager getNotificationManager() {
        return this.notificationManager;
    }

    /**
     * Setter for the notificationManager property.
     * @param notificationManager the new notificationManager value
     */
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * Getter for the serverProperties property.
     * @return Properties value of the property
     */
    public Properties getServerProperties() {
        return this.serverProperties;
    }

    /**
     * Setter for the serverProperties property.
     * @param serverProperties the new serverProperties value
     */
    public void setServerProperties(Properties serverProperties) {
        this.serverProperties = serverProperties;
    }

    /**
     * Getter for the tagManager property.
     * @return TagManager value of the property
     */
    public TagManager getTagManager() {
        return this.tagManager;
    }

    /**
     * Setter for the tagManager property.
     * @param tagManager the new tagManager value
     */
    public void setTagManager(TagManager tagManager) {
        this.tagManager = tagManager;
    }

}
