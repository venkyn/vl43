/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.scheduling;

import com.vocollect.epp.logging.Logger;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;

/**
 * This class implements a schedulable job that always fails; it exists for test
 * purposes only.
 *
 * @see org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean
 */
public class FailingJob implements EPPQuartzJob {

    private static final Logger log = new Logger(FailingJob.class);

    /**
     * {@inheritDoc}
     * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
     */
    public void execute(JobExecutionContext context)
        throws JobExecutionException {

        log.debug("Failing on startup ...........");
        throw new JobExecutionException(""
            + SchedulingErrorCode.UNABLE_TO_SCHED_JOB);
    }

    /**
     * {@inheritDoc}
     * @see org.quartz.InterruptableJob#interrupt()
     */
    public void interrupt() throws UnableToInterruptJobException {

        log.debug("Failing on interrupt ...........");
        throw new UnableToInterruptJobException(""
            + SchedulingErrorCode.UNABLE_TO_STOP_JOB);
    }
}
