/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.exceptions;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.InvalidQueryException;

import static com.vocollect.epp.test.TestGroups.EXCEPTIONS;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.ArrayList;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;


/**
 * Test class for the <code>BulkOperationException</code> class.
 *
 * @author Dennis Doubleday
 */
@Test(groups = { FAST, EXCEPTIONS })
public class BulkOperationExceptionTest {

    /**
     * Test method for 'BulkOperationException.BulkOperationException()'.
     */
    public void testBulkOperationException() {
        BulkOperationException e = new BulkOperationException();
        assert e.getExceptions() != null;
        assert e.getExceptions().isEmpty();
        assert e.getCause() == null;
    }

    /**
     * Test method for
     * 'BulkOperationException.addException(VocollectException)'.
     * Test method for 'BulkOperationException.getExceptions()'.
     */
    public void testAddAndGetException() {
        BulkOperationException e = new BulkOperationException();
        e.addException(new VocollectException());
        assert !e.getExceptions().isEmpty();
        assert e.getCause() == null;
        assertEquals("exception count", e.getExceptions().size(), 1);
        e.addException(new VocollectException());
        assertEquals("exception count", e.getExceptions().size(), 2);
    }

    /**
     * Test method for 'BulkOperationException.setExceptions(List)'.
     */
    public void testSetExceptions() {

        BulkOperationException e = new BulkOperationException();
        e.addException(new VocollectException());
        assert !e.getExceptions().isEmpty();
        e.setExceptions(null);
        assert e.getExceptions().isEmpty() : "set null clears the list";
        ArrayList<VocollectException> list =
            new ArrayList<VocollectException>();
        list.add(new DataAccessException());
        list.add(new InvalidQueryException());
        e.setExceptions(list);
        assertEquals("exception count", e.getExceptions().size(), 2);
        assert e.getExceptions().get(1)
            instanceof InvalidQueryException : "order is preserved";
    }

    /**
     * Test method for 'BulkOperationException.toString()'.
     */
    public void testToString() {
        BulkOperationException e = new BulkOperationException();
        e.addException(new VocollectException());
        assert e.toString().contains("VocollectException");
    }

}
