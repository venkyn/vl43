/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.exceptions;

import com.vocollect.epp.errors.MessageMap;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.LoggingError;

import static com.vocollect.epp.test.TestGroups.EXCEPTIONS;
import static com.vocollect.epp.test.TestGroups.FAST;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;



/**
 * Test class for the VocollectException class.
 *
 * @author Dennis Doubleday
 */
@Test(groups = { FAST, EXCEPTIONS })
public class VocollectExceptionTest {

    private final String msgBadErrCode = "Bad error code";
    private final String testKey = "test.key";
    private final String testString = "test";

    /**
     * Test method for 'VocollectException()'.
     */
    public void testVocollectException() {
        VocollectException e = new VocollectException();
        assert e.getMessages() == null : "not-null messages";
        assert e.getUserMessage() == null : "not-null message";
        assert e.getErrorCode() == null : "not-null error code";
        assert e.getCause() == null : "not-null cause";
    }


    /**
     * Test method for
     * 'VocollectException(ErrorCode, UserMessage, Throwable)'.
     */
    public void testVocollectExceptionErrorCodeUserMessage() {
        VocollectException e = new VocollectException(
            LoggingError.ADD_ZIP_ENTRY_FAILED,
            new UserMessage(testKey, testString));
        assert e.getMessages() != null : "non-null messages";
        assertEquals("One message", e.getMessages().getSize(), 1);
        assertEquals(msgBadErrCode, e.getErrorCode(),
                      LoggingError.ADD_ZIP_ENTRY_FAILED);

    }

    /**
     * Test method for
     * 'VocollectException(ErrorCode, UserMessage, Throwable)'.
     */
    public void testVocollectExceptionErrorCodeUserMessageThrowable() {
        VocollectException e = new VocollectException(
            LoggingError.ADD_ZIP_ENTRY_FAILED,
            new UserMessage(testKey, testString),
            new RuntimeException(testString));
        assert e.getMessages() != null : "non-null messages";
        assertEquals("One message", e.getMessages().getSize(), 1);
        assertEquals("Exception message", e.getCause().getMessage(), testString);
        assertEquals(msgBadErrCode, e.getErrorCode(),
                      LoggingError.ADD_ZIP_ENTRY_FAILED);

    }

    /**
     * Test method for 'VocollectException.get{set}ErrorCode()'.
     */
    public void testErrorCode() {
        VocollectException e = new VocollectException();
        e.setErrorCode(LoggingError.ADD_ZIP_ENTRY_FAILED);
        assertEquals(msgBadErrCode, e.getErrorCode(),
            LoggingError.ADD_ZIP_ENTRY_FAILED);

    }

    /**
     * Test method for 'VocollectException.getUserMessage()'.
     */
    public void testGetUserMessage() {
        VocollectException e = new VocollectException(
            LoggingError.ADD_ZIP_ENTRY_FAILED,
            new UserMessage(testKey, testString),
            new RuntimeException(testString));
        assert e.getUserMessage() != null : "null message";
        assertEquals("bad message", e.getUserMessage().getKey(), testKey);
        e.addMessage(new UserMessage("test2.key"));
        assertEquals("bad 2 message", e.getUserMessage().getKey(), testKey);

    }

    /**
     * Test method for 'VocollectException.getMessages()'.
     */
    public void testGetMessages() {
        VocollectException e = new VocollectException(
            LoggingError.ADD_ZIP_ENTRY_FAILED,
            new UserMessage(testKey, testString),
            new RuntimeException(testString));
        assert e.getMessages() != null : "null message map";
        e.addMessage(new UserMessage("test2.key"));
        assertEquals("message count", e.getMessages().getSize(), 2);

    }

    /**
     * Test method for 'VocollectException.addMessage(UserMessage)'.
     */
    public void testAddMessage() {
        VocollectException e = new VocollectException();
        assert e.getMessages() == null : "non-null message map";
        e.addMessage(new UserMessage(testKey));
        assertEquals("message count", e.getMessages().getSize(), 1);
        assertEquals("bad message", e.getUserMessage().getKey(), testKey);

    }

    /**
     * Test method for 'VocollectException.setMessages(MessageMap)'.
     */
    public void testSetMessages() {
        VocollectException e = new VocollectException();
        assert e.getMessages() == null : "non-null message map";
        MessageMap map = new MessageMap().
            add(new UserMessage(testKey)).
            add(new UserMessage("test2.key"));
        e.setMessages(map);
        assertEquals("message count", e.getMessages().getSize(), 2);
    }

    /**
     * Test method for 'VocollectException.toString()'.
     */
    public void testToString() {
        VocollectException e = new VocollectException();
        assert e.toString() != null : "null toString";
        e.addMessage(new UserMessage(testKey));
        assert e.toString().contains(testKey);
    }

    /**
     * Test method for 'VocollectException.getMessage()'.
     */
    public void testGetMessage() {
        VocollectException e = new VocollectException();
        assert e.getMessage() != null;
    }
}
