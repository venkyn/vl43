/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.User;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.acegisecurity.userdetails.UserDetails;
import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 *
 * @author sshaulis
 */
@Test(groups = { FAST, SECURITY })
public class UserManagerImplTest extends BaseServiceTestCase {

    private SiteManager siteManager = null;

    private UserManager manager = null;

    private final long adminUserID = -1;

    private final String adminUserName = "admin";

    private final String user1UserName = "User01";

    private final String user2UserName = "User02";

    private final String adminEmail = "some@where.com";

    /**
     * @param impl .
     */
    @Test(enabled = false)
    public void setUserManager(UserManager impl) {
        this.manager = impl;
    }

    /**
     * @param impl .
     */
    @Test(enabled = false)
    public void setSiteManager(SiteManager impl) {
        this.siteManager = impl;
    }

    /**
     * @throws Exception on any failure
     */
    @BeforeMethod
    protected void methodSetup() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/users-simple.xml", DatabaseOperation.INSERT);
        setUpSiteContext(false);
    }

    /**
     * Test method for
     * 'com.vocollect.epp.service.impl.UserManagerImpl.getUser(Long)'.
     */
    public void testGetUserLong() throws Exception {
        User user = manager.get(adminUserID);
        org.testng.AssertJUnit
            .assertNotNull("userManagerImplGetUserLong", user);
        org.testng.AssertJUnit.assertTrue("userManagerImplGetUserLong", user
            .getId() == adminUserID);
    }

    /**
     * Test method for
     * 'com.vocollect.epp.service.impl.UserManagerImpl.getUser(String)'.
     */
    public void testGetUserString() throws DataAccessException {
        User user = manager.findByName(adminUserName);
        org.testng.AssertJUnit.assertNotNull(
            "userManagerImplGetUserString", user);
        org.testng.AssertJUnit.assertTrue("userManagerImplGetUserString", user
            .getName().equals(adminUserName));
    }

    /**
     * Test method for
     * 'com.vocollect.epp.service.impl.UserManagerImpl.getUsers()'.
     */
    public void testGetUsers() throws DataAccessException {
        List<User> users = manager.getAll();
        org.testng.AssertJUnit.assertNotNull("userManagerImplGetUsers", users);
        org.testng.AssertJUnit.assertTrue("userManagerImplGetUsers", users
            .size() > 0);
    }

    /**
     * Test method for
     * 'com.vocollect.epp.service.impl.UserManagerImpl.loadUserByUsername(String)'.
     */
    public void testLoadUserByUsername() {
        UserDetails user = manager.loadUserByUsername(adminUserName);
        org.testng.AssertJUnit.assertNotNull(
            "userManagerImplGetUserString", user);
        org.testng.AssertJUnit.assertTrue("userManagerImplGetUserString", user
            .getUsername().equals(adminUserName));
    }

    /**
     * Test method for
     * 'com.vocollect.epp.service.impl.UserManagerImpl.removeUser(Long)'.
     */
    public void testRemoveUserLong() throws Exception {
        User user = null;
        manager.delete(manager.findByName(user1UserName).getId());
        // this next getUser() should throw an exception
        user = manager.findByName(user1UserName);
        assert user == null : "Found user " + user1UserName + " after delete";
    }

    /**
     * Test method for
     * 'com.vocollect.epp.service.impl.UserManagerImpl.removeUser(User)'.
     */
    public void testRemoveUserUser() throws Exception {
        User user2 = null;
        manager.delete(manager.findByName(user2UserName));
        // this next getUser() should throw an exception
        user2 = manager.findByName(user2UserName);
        assert user2 == null : "Found user " + user2UserName + " after delete";
    }

    /**
     * Test method for
     * 'com.vocollect.epp.service.impl.UserManagerImpl.saveUser(User)'.
     */
    public void testSaveUser() throws Exception {
        User user = manager.findByName(adminUserName);
        user.setEmailAddress(adminEmail);
        manager.save(user);
        user = manager.findByName(adminUserName);
        org.testng.AssertJUnit.assertNotNull("userManagerImplSaveUser", user);
        org.testng.AssertJUnit.assertTrue("userManagerImplSaveUser", user
            .getEmailAddress().equals(adminEmail));
    }

    /**
     * Test method for
     * 'com.vocollect.epp.service.impl.UserManagerImpl.setUserDAO(UserDAO)' and
     * 'com.vocollect.epp.service.impl.UserManagerImpl.getProviderDAO()'.
     */
    public void testGetSetUserDAO() {
        String testMsg = "Testing UserDAO setter/getter";
        org.testng.AssertJUnit.assertNotNull(testMsg, manager.getProviderDAO());
    }

    /**
     * @throws Exception
     */
    public void testUpdateUser() throws Exception {
        User user = manager.findByName(user1UserName);
        user.setEmailAddress(adminEmail);
        manager.save(user);

        // Set<Site> sites = user.getSites();
        Set<Site> sites = new HashSet<Site>();
        Site s = siteManager.get(-1L);
        assertEquals(s.getName(), "Default");
        sites.add(s);
        s = siteManager.get(-2L);
        assertEquals(s.getName(), "Site 2");
        sites.add(s);

        user.setSites(sites);
        try {
            manager.save(user);
        } catch (Exception e) {
            System.out.println("Stop");
        }

        user = manager.findByName(user1UserName);
        assertEquals(user.getSites().size(), 2);
        org.testng.AssertJUnit.assertNotNull("userManagerImplSaveUser", user);
        org.testng.AssertJUnit.assertTrue("userManagerImplSaveUser", user
            .getEmailAddress().equals(adminEmail));
    }
    
    
    /**
     * @throws Exception
     */
    public void testUpdateUserChartPreferences() throws Exception {
        User user = manager.get(-1L);
        assertNotNull(user);
        
        manager.updateUserChartPreferencesByDashboardAndChart(-1L, -1L);
    }
}
