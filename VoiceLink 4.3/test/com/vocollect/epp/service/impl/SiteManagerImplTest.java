/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 *
 * @author KalpnaT
 */
@Test(groups = { FAST, SECURITY })
public class SiteManagerImplTest extends BaseServiceTestCase {

    private SiteManager manager = null;
    private final long siteID = -1;
    private final long siteTestID = -2;
    private final String defaultSiteDesc = "Testing Site Manager Impl";

    /**
     * @param impl .
     */
    @Test(enabled = false)
    public void setSiteManager(SiteManager impl) {
        this.manager = impl;
    }

    /**
     * @throws Exception
     */
    @BeforeClass()
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.handleFlatXmlResource("data/dbunit/sites-simple.xml", DatabaseOperation.REFRESH);
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.SiteManagerImpl.get(Long)'.
     */
    public void testGetSiteLong() throws DataAccessException {
        Site site = manager.get(siteID);
        org.testng.AssertJUnit.assertNotNull("siteManagerImplGetSiteLong", site);
        org.testng.AssertJUnit.assertTrue("siteManagerImplGetSiteLong",
            site.getName().equals(Site.DEFAULT_SITE_NAME));
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.SiteManagerImpl.getAll()'.
     */
    public void testGetSites() throws DataAccessException {
        List<Site> sites = manager.getAll();
        org.testng.AssertJUnit.assertNotNull("siteManagerImplGetSites", sites);
        org.testng.AssertJUnit.assertTrue("siteManagerImplGetSites", sites.size() > 0);
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.SiteManagerImpl.removeSite(Long)'.
     */
    public void testRemoveSiteLong() throws Exception {
        try {
           manager.delete(siteTestID);
           manager.get(siteTestID);
           fail("testRemoveSiteLong should have thrown an exception");
        } catch (Exception e) {
            log.info("expected exception in siteManagerImplTest.testRemoveSiteLong");
        }


    }


    /**
     * Test method for
     * 'com.vocollect.epp.service.impl.UserManagerImpl.saveSite(Site)'.
     */
    public void testSaveSite() throws Exception {
        Site site = manager.get(siteID);
        site.setDescription(defaultSiteDesc);
        manager.save(site);
        site = manager.get(siteID);
        org.testng.AssertJUnit.assertNotNull("siteManagerImplSaveSite", site);
        org.testng.AssertJUnit.assertTrue("siteManagerImplSaveSite", site.getDescription().equals(defaultSiteDesc));
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.SiteManagerImpl.setSiteDAO(SiteDAO)'
     * and 'com.vocollect.epp.service.impl.SiteManagerImpl.getProviderDAO()'.
     */
    public void testGetSetSiteDAO() {
        String testMsg = "Testing SiteDAO setter/getter";
        org.testng.AssertJUnit.assertNotNull(testMsg, manager.getProviderDAO());
    }
}
