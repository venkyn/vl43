/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;


import com.vocollect.epp.dao.UserDAO;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.ColumnSortType;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.ResultData;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.web.util.DataProviderTableUtil;
import com.vocollect.epp.web.util.DisplayUtilities;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test DataProviderImpl logic via various child classes.
 *
 * @author jkercher
 */
@Test(enabled = true, groups = { FAST, SECURITY })
public class DataProviderImplTest30 extends BaseDataProviderImplTestCase {

    // Objects required for data access.
    private UserDAO userdao = null;
    private DisplayUtilities displayUtilities = null;



    /**
     * Pre-method setup code to instantiate a default ResultDataInfo object.
     */
    @BeforeMethod
    public void methodSetup() throws Exception {
        System.out.println("Running Test 30");

        setUpSiteContext(true);
    }

    /**
     * @return .
     */
    public ResultDataInfo makeResultInfo() {
        ResultDataInfo rd = new ResultDataInfo(
            "name", // String tooltipColumn
            "name", // String   sortColumn
            true,   // boolean  sortAscending
            0L,     // long     rowId
            0L,     // long     offset
            0,      // int      rowCount
            0,      // int      startIndex
            20,     // int      sliderHeight
            20,     // int      rowsPerPage
            false,       // int      refreshRequest
            null,
            this.displayUtilities,
            false
        );
        return rd;
    }

    /**
     * Pre-class setup code handles database initialization.  This should
     * yield 100 users with IDs ranging from -100 to -199.
     * @throws Exception if an error occurs during database setup.
     */
    @BeforeClass
    public void classSetup() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        // Reset the database to the default state.
        adapter.resetInstallationData();
        // Replace default users with the 100 user dataset.
        adapter.handleFlatXmlResource("data/dbunit/users-30.xml");

    }

    /**
     * @return .
     */
    private Column getColumn() {
        Column c = new Column();
        c.setSortType(ColumnSortType.Normal);
        c.setDefaultSorted(false);
        c.setDefaultSortAsc(false);
        c.setField("name");
        return c;

    }

    /**
     * @param rdi .
     * @return .
     */
    private ResultData getResults(ResultDataInfo rdi) {
        List<DataObject> dataObjects = DataProviderTableUtil.callGetDataMethod(rdi, userdao, "getAll");
        ResultData rd = DataProviderTableUtil.windowData(rdi, dataObjects, this.displayUtilities);
        return rd;
    }

    /**
     * Get by offset is called when the slider values
     * changes without a tooltip.
     * @throws Exception if an error occurs during test execution
     */
    @Test(enabled = true)
    public void testGetByOffset() throws Exception {
        System.out.println("Testing moving slider with 30 rows");
        ResultDataInfo rdi;
        Column c = getColumn();


        System.out.println("Starting test 1");
        rdi = makeResultInfo();
        rdi.setFirstRowId(null);
        rdi.setOffset(0);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        ResultData rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 30);
        assertEquals(rd.getNumPreviousRows(), 0);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 2");
        rdi = makeResultInfo();
        rdi.setFirstRowId(null);
        rdi.setOffset(7);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 30);
        assertEquals(rd.getNumPreviousRows(), 7);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 3");
        rdi = makeResultInfo();
        rdi.setFirstRowId(null);
        rdi.setOffset(19);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 30);
        assertEquals(rd.getNumPreviousRows(), 19);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 4");
        rdi = makeResultInfo();
        rdi.setFirstRowId(null);
        rdi.setOffset(20);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 30);
        assertEquals(rd.getNumPreviousRows(), 20);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());



        System.out.println("Starting test 5");
        rdi = makeResultInfo();
        rdi.setFirstRowId(null);
        rdi.setOffset(23);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 27);
        assertEquals(rd.getNumPreviousRows(), 20);
        assertEquals(-103L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 6");
        rdi = makeResultInfo();
        rdi.setFirstRowId(null);
        rdi.setOffset(29);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 21);
        assertEquals(rd.getNumPreviousRows(), 20);
        assertEquals(-109L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 7");
        rdi = makeResultInfo();
        rdi.setFirstRowId(null);
        rdi.setOffset(100);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        printContents(rd);
        assertEquals(rd.getData().size(), 30);
        assertEquals(rd.getNumPreviousRows(), 10);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());


    }
    /**
     * Get by offset is called when the slider values
     * changes without a tooltip.
     * @throws Exception if an error occurs during test execution
     */
    @Test(enabled = true)
    public void testGetById() throws Exception {
        System.out.println("Testing moving tooltip or auto-refresh 30");
        ResultDataInfo rdi;
        Column c = getColumn();

        System.out.println("Starting test 1");
        rdi = makeResultInfo();
        rdi.setFirstRowId(new Long(-100));
        rdi.setOffset(50);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        ResultData rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 30);
        assertEquals(rd.getNumPreviousRows(), 0);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 2");
        rdi = makeResultInfo();
        rdi.setFirstRowId(new Long(-107));
        rdi.setOffset(0);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        System.out.println("**************************");
        System.out.println(rd.getNumPreviousRows());
        printContents(rd);
        assertEquals(rd.getData().size(), 30);
        assertEquals(rd.getNumPreviousRows(), 7);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 3");
        rdi = makeResultInfo();
        rdi.setFirstRowId(new Long(-119));
        rdi.setOffset(0);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 30);
        assertEquals(rd.getNumPreviousRows(), 19);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 4");
        rdi = makeResultInfo();
        rdi.setFirstRowId(new Long(-120));
        rdi.setOffset(0);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 30);
        assertEquals(rd.getNumPreviousRows(), 20);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());



        System.out.println("Starting test 5");
        rdi = makeResultInfo();
        rdi.setFirstRowId(new Long(-123));
        rdi.setOffset(0);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 27);
        assertEquals(rd.getNumPreviousRows(), 20);
        assertEquals(-103L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 6");
        rdi = makeResultInfo();
        rdi.setFirstRowId(new Long(-129));
        rdi.setOffset(0);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 21);
        assertEquals(rd.getNumPreviousRows(), 20);
        assertEquals(-109L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 7");
        rdi = makeResultInfo();
        rdi.setFirstRowId(new Long(100));
        rdi.setOffset(100);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 30);
        assertEquals(rd.getNumPreviousRows(), 10);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-129L, (long) rd.getData().get(rd.getData().size() - 1).getId());
    }

    /**
     * Test a simple upward scroll on the users data set.
     * @throws Exception if an error occurs during test execution
     */
    @Test(enabled = false)
    public void testUserScrollUp() throws Exception {
        System.out.println("Testing scrolling up");
        ResultDataInfo rdi;
        Column c = getColumn();

        System.out.println("Starting test 1");
        // Get the first row; no previous rows should be returned.
        rdi = makeResultInfo();
        rdi.setFirstRowId(-100L);
        rdi.setStartIndex(-20);
        rdi.setRowCount(20);
        rdi.setSortColumnObject(c);

        ResultData rd = getResults(rdi);
        ////printContents(rd);
        assertEquals(rd.getData().size(), 0);
        assertEquals(rd.getNumPreviousRows(), 0);
        // Get the second row: 1 previous row should be returned.

        System.out.println("Starting test 2");
        rdi = makeResultInfo();
        rdi.setStartIndex(-20);
        rdi.setRowCount(20);
        rdi.setFirstRowId(-101L);
        rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(1, rd.getData().size());
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(1, rd.getNumPreviousRows());

        System.out.println("Starting test 3");
        rdi = makeResultInfo();
        rdi.setStartIndex(-20);
        rdi.setRowCount(20);
        rdi.setFirstRowId(-107L);
        rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        ////printContents(rd);
        assertEquals(7, rd.getData().size());
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-106L, (long) rd.getData().get(rd.getData().size() - 1).getId());
        assertEquals(7, rd.getNumPreviousRows());

        System.out.println("Starting test 4");
        rdi = makeResultInfo();
        rdi.setStartIndex(-20);
        rdi.setRowCount(20);
        rdi.setFirstRowId(-170L);
        rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(20, rd.getData().size());
        assertEquals(-150L, (long) rd.getData().get(0).getId());
        assertEquals(-169L, (long) rd.getData().get(rd.getData().size() - 1).getId());
        assertEquals(20, rd.getNumPreviousRows());

        System.out.println("Starting test 5");
        // Get the first row; no previous rows should be returned.
        rdi = makeResultInfo();
        rdi.setFirstRowId(-201L);
        rdi.setStartIndex(-20);
        rdi.setRowCount(20);
        rdi.setOffset(50);
        rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        ////printContents(rd);
        assertEquals(rd.getData().size(), 0);

    }

    /**
     * Test a simple upward scroll on the users data set.
     * @throws Exception if an error occurs during test execution
     */
    @Test(enabled = false)
    public void testUserScrollDown() throws Exception {
        System.out.println("Testing scrolling down");
        ResultDataInfo rdi;
        Column c = getColumn();

        System.out.println("Starting test 1");
        // Get the first row; no previous rows should be returned.
        rdi = makeResultInfo();
        rdi.setFirstRowId(-199L);
        rdi.setStartIndex(1);
        rdi.setRowCount(20);

        ResultData rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 0);
        assertEquals(rd.getNumPreviousRows(), 0);
        // Get the second row: 1 previous row should be returned.
        System.out.println("Starting test 2");
        rdi = makeResultInfo();
        rdi.setStartIndex(1);
        rdi.setRowCount(20);
        rdi.setFirstRowId(-198L);
        rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(1, rd.getData().size());
        assertEquals(-199L, (long) rd.getData().get(0).getId());
        assertEquals(0, rd.getNumPreviousRows());

        System.out.println("Starting test 3");
        rdi = makeResultInfo();
        rdi.setStartIndex(1);
        rdi.setRowCount(20);
        rdi.setFirstRowId(-192L);
        rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(7, rd.getData().size());
        assertEquals(-193L, (long) rd.getData().get(0).getId());
        assertEquals(-199, (long) rd.getData().get(rd.getData().size() - 1).getId());
        assertEquals(0, rd.getNumPreviousRows());

        System.out.println("Starting test 4");
        rdi = makeResultInfo();
        rdi.setStartIndex(1);
        rdi.setRowCount(20);
        rdi.setFirstRowId(-130L);
        rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(20, rd.getData().size());
        assertEquals(-131L, (long) rd.getData().get(0).getId());
        assertEquals(-150L, (long) rd.getData().get(rd.getData().size() - 1).getId());
        assertEquals(0, rd.getNumPreviousRows());

        System.out.println("Starting test 5");
        // Get the first row; no previous rows should be returned.
        rdi = makeResultInfo();
        rdi.setFirstRowId(-201L);
        rdi.setStartIndex(1);
        rdi.setRowCount(20);
        rdi.setOffset(50);
        rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        ////printContents(rd);
        assertEquals(rd.getData().size(), 0);

    }


    /**
     * Allow object injection.  This is not a test method; set enabled=false
     * to prevent execution by the TestNG framework.
     * @param userDAO provides access to user data.
     */
    @Test(enabled = false)
    public void setUserDAO(UserDAO userDAO) {
        this.userdao = userDAO;
    }

    /**
     * Print out the contents of the given ResultData object to standard
     * output.
     *
     * @param rd .
     */
    private void printContents(ResultData rd) {

        System.out.println("Result Data: total rows: " + rd.getTotalRows());
        System.out.println("Result Data: contents:");
        List<DataObject> dataList = rd.getData();
        for (int i = 0; i < dataList.size(); i++) {
            DataObject data = dataList.get(i);
            System.out.println(data.toString());
        }

    }


    /**
     * Setter for the displayUtilities property.
     * @param displayUtilities the new displayUtilities value
     */
    @Test(enabled = false)
    public void setDisplayUtilities(DisplayUtilities displayUtilities) {
        this.displayUtilities = displayUtilities;
    }

}
