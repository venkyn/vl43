/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.exceptions.VocollectException;
import com.vocollect.epp.service.LDAPConfigurationManager;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.UNIT;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Performs various tests for the ldap configuration manager.
 *
 * @author kganesan
 */
@Test(groups = { UNIT })
public class LDAPConfigurationManagerImplTest extends BaseServiceTestCase {
    private LDAPConfigurationManager ldapConfigurationManager = null;
    protected static final String DATA_DIR = "data/dbunit/";

    /**
     * @param ldapConfigurationManager - the ldap
     */
    @Test(enabled = false)
    public void setLdapConfigurationManager(LDAPConfigurationManager ldapConfigurationManager) {
        this.ldapConfigurationManager = ldapConfigurationManager;
    }

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        String sDataURLFF = DATA_DIR + "LDAPConfigurationDAOTest.xml";
        adapter.handleFlatXmlResource(sDataURLFF, DatabaseOperation.INSERT);
    }

    /**
     * @throws VocollectException
     */
    @Test(enabled = true)
    public void testDeleteLDAPConfigurations() throws VocollectException {
        assertTrue(null != this.ldapConfigurationManager, "Ldap configuration manager not initialized.");

        Long[] ldapConfigurationIds = new Long[2];
        ldapConfigurationIds[0] = -200L;
        ldapConfigurationIds[1] = -203L;

        this.ldapConfigurationManager
            .deleteLDAPConfigurations(ldapConfigurationIds);
    }

}
