/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;


/**
 * 
 *
 * @author ddoubleday
 */
public class BaseDataProviderImplTestCase extends BaseServiceTestCase {

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        String[] superConfig = super.getConfigLocations();
        String[] config = new String[superConfig.length + 1];
        System.arraycopy(superConfig, 0, config, 0, superConfig.length);
        // Needed for displayUtilties bean.
        config[superConfig.length] = "applicationContext-epp-actions.xml";
        return config;
    }

}
