/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.model.Filter;
import com.vocollect.epp.service.FilterManager;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.FILTERING;

import java.util.List;

import org.testng.annotations.Test;


/**
 * 
 *
 * @author jstonebrook
 */
@Test(groups = { FAST, FILTERING })
public class FilterManagerImplTest extends BaseServiceTestCase {

    private FilterManager filterManager = null;

    /**
     * Setter for the filterManager property.
     * @param filterManager the new filterManager value
     */
    @Test(enabled = false) 
    public void setFilterManager(FilterManager filterManager) {
        this.filterManager = filterManager;
    }
 
    /**
     * Going to test various serialized JSON objects and make sure
     * the appropriate object graph is built.
     */
    public void testJSONDeserialization() throws Exception {
        // I am going to cheat and use ' for the field separators - and
        // then do a global replace with the " which is the value the JSON
        // serialization actually looks
        String testString = 
            "{'viewId':'-1','columnId':'-1','operandId':'-1','value1':'test','value2':'','locked':'false'}";
        List<Filter> filterList = filterManager.constructFilterFromSerializedJSON(testString);

        assertEquals(filterList.size(), 1);
        Filter filter = filterList.get(0);
        assertEquals(filter.getFilterCriteria().size(), 1);
        assertEquals(filter.getFilterCriteria().get(0).getColumn().getId().longValue(), -1L);
        assertEquals(filter.getFilterCriteria().get(0).getOperand().getId().longValue(), -1L);
        
        assertTrue(filter.toHQL().contains(" ( upper(obj.name) like 'TEST%' ) "));
    }

    /**
     * Going to test various serialized JSON objects and make sure
     * the appropriate object graph is built.
     */
    public void testJSONDeserializationWithTwoStringsSameColumn() throws Exception {
        // I am going to cheat and use ' for the field separators - and
        // then do a global replace with the " which is the value the JSON
        // serialization actually looks
        String testString1 = 
            "{'viewId':'-1','columnId':'-1','operandId':'-1','value1':'test','value2':'','locked':'false'}";
        String testString2 = 
            "{'viewId':'-1','columnId':'-1','operandId':'-1','value1':'test2','value2':'','locked':'false'}";
        List<Filter> filterList = filterManager.constructFilterFromSerializedJSON(
            testString1 + "," + testString2);

        assertEquals(filterList.size(), 1);
        Filter filter = filterList.get(0);

        assertEquals(filter.getFilterCriteria().size(), 2);
        assertEquals(filter.getFilterCriteria().get(0).getColumn().getId().longValue(), -1L);
        assertEquals(filter.getFilterCriteria().get(0).getOperand().getId().longValue(), -1L);
        assertEquals(filter.getFilterCriteria().get(1).getColumn().getId().longValue(), -1L);
        assertEquals(filter.getFilterCriteria().get(1).getOperand().getId().longValue(), -1L);
        
        assertTrue(filter.toHQL().contains(" upper(obj.name) like 'TEST%' "));
        assertTrue(filter.toHQL().contains(" upper(obj.name) like 'TEST2%' "));
        assertTrue(filter.toHQL().contains(" or "));
    }


    /**
     * Going to test various serialized JSON objects and make sure
     * the appropriate object graph is built.
     */
    public void testJSONDeserializationWithThreeStringsTwoColumns() throws Exception {
        // I am going to cheat and use ' for the field separators - and
        // then do a global replace with the " which is the value the JSON
        // serialization actually looks
        String testString1 = 
            "{'viewId':'-1','columnId':'-1','operandId':'-1','value1':'test','value2':'','locked':'false'}";
        String testString2 = 
            "{'viewId':'-1','columnId':'-1','operandId':'-1','value1':'test2','value2':'','locked':'false'}";
        String testString3 = 
            "{'viewId':'-1','columnId':'-2','operandId':'-1','value1':'test3','value2':'','locked':'false'}";
        
        List<Filter> filterList = filterManager.constructFilterFromSerializedJSON(
            testString1 + "," + testString2 + "," + testString3);

        assertEquals(filterList.size(), 1);
        Filter filter = filterList.get(0);


        assertEquals(filter.getFilterCriteria().size(), 3);
        assertEquals(filter.getFilterCriteria().get(0).getColumn().getId().longValue(), -1L);
        assertEquals(filter.getFilterCriteria().get(0).getOperand().getId().longValue(), -1L);
        assertEquals(filter.getFilterCriteria().get(1).getColumn().getId().longValue(), -1L);
        assertEquals(filter.getFilterCriteria().get(1).getOperand().getId().longValue(), -1L);
        assertEquals(filter.getFilterCriteria().get(2).getColumn().getId().longValue(), -2L);
        assertEquals(filter.getFilterCriteria().get(2).getOperand().getId().longValue(), -1L);
       
        assertTrue(filter.toHQL().contains(" upper(obj.name) like 'TEST%' "));
        assertTrue(filter.toHQL().contains(" or "));
        assertTrue(filter.toHQL().contains(" upper(obj.name) like 'TEST2%' "));
        assertTrue(filter.toHQL().contains(" upper(obj.emailAddress) like 'TEST3%' "));
        assertTrue(filter.toHQL().contains(" ) and ( "));
    }
    
    /**
     * Going to test various serialized JSON objects and make sure
     * the appropriate object graph is built.
     * @throws Exception
     */
    public void testJSONDeserializationWithThreeViews() throws Exception {
        // I am going to cheat and use ' for the field separators - and
        // then do a global replace with the " which is the value the JSON
        // serialization actually looks
        String testString1 = 
            "{'viewId':'-1','columnId':'-1','operandId':'-1','value1':'test','value2':'','locked':'false'}";
        String testString2 = 
            "{'viewId':'-2','columnId':'-101','operandId':'-1','value1':'test2','value2':'','locked':'false'}";
        String testString3 = 
            "{'viewId':'-3','columnId':'-201','operandId':'-1','value1':'test3','value2':'','locked':'false'}";
        
        List<Filter> filterList = filterManager.constructFilterFromSerializedJSON(
            testString1 + "," + testString2 + "," + testString3);

        assertEquals(filterList.size(), 3);
    }
}
