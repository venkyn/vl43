/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.ExternalJob;
import com.vocollect.epp.service.ExternalJobManager;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.EJS;

import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit test class for testing Vehicle manager.
 * @author mraj
 * 
 */
@Test(groups = { EJS })
public class ExternalJobManagerImplTest extends BaseDataProviderImplTestCase {

    private ExternalJobManager externalJobManager = null;


    /**
     * @throws Exception
     */
    @BeforeClass
    public void classSetup() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        // Reset the database to the default state.
        adapter.resetInstallationData();
        // Replace default users with the 100 user dataset.
        adapter.handleFlatXmlResource("data/dbunit/external-job-data.xml");

    }

    /**
     * @return the externalJobManager
     */
    @Test(enabled = false)
    public ExternalJobManager getExternalJobManager() {
        return externalJobManager;
    }

    /**
     * @param extrnalJobManager - the vehicle manager
     */
    @Test(enabled = false)
    public void setExternalJobManager(ExternalJobManager extrnalJobManager) {
        this.externalJobManager = extrnalJobManager;
    }

    /**
     * Tests the getAll method
     */
    @Test()
    public void testGetAllJobs() {
        try {
            List<ExternalJob> externalJobs = this.externalJobManager.getAll();

            // We are not testing how many jobs has been returned just because
            // the data XML file will keep growing and we have to maintain this
            // test case very time we update the data XML
            assert (externalJobs.size() > 0);
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }

}
