/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.User;
import com.vocollect.epp.search.SearchResult;
import com.vocollect.epp.search.SearchResultDisplayCollection;
import com.vocollect.epp.search.SearchResultList;
import com.vocollect.epp.service.SearchManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.SiteContextHolder;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.providers.UsernamePasswordAuthenticationToken;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.context.i18n.LocaleContextHolder;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Provides unit tests on the search and collection functionality.
 * 
 * @author hulrich
 */
@Test(groups = { FAST, SECURITY })
public class SearchManagerImplTest extends BaseServiceTestCase {

    private SearchManager searchManager;

    private UserManager userManager;
    private static final String DATA_DIR = "data/dbunit/";

    private static final String USER2 = "User2";

    private static final String USER3 = "User3";

    private static final String USER4 = "User4";

    private static final String USER5 = "User5";

    private static final String USERMB = "憫戨扗";

    private static final String USERS = "Users";

    private static final long SITE4 = 3L;

    private static final long SITE5 = 5L;

    private static final long SITE6 = 7L;

    private static final long SITE7 = 9L;
    
    private static final int CAT_LIMIT = 25;

    /**
     * Sets the search manager.
     * @param searchManager the manager
     */
    @Test(enabled = false)
    public void setSearchManager(SearchManager searchManager) {
        this.searchManager = searchManager;
    }

    /**
     * @param userManager .
     */
    @Test(enabled = false)
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    /**
     * Sets the data for the class.
     * @throws Exception on exception
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        log("SETTING UP.");
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
            DATA_DIR + "search.xml", DatabaseOperation.INSERT);
    }

    /**
     * Searches data as a multi site user (admin privileges). This will test
     * whether the user is able to view data associated with their sites and
     * unable to view data not associated with their sites. Data not associated
     * with any site (Not Taggable) is viewable under SearchManager.ALL_SITES.
     * The current site must be set to a valid site, along with all site access
     * set to false. The result collection should be organized correctly, with
     * the current site first and the "All Sites" as last, and the sites in
     * between organized alphabetically. The business areas must be organized
     * alphabetically as well. The find method will return all expected results,
     * which should be checked to ensure that Compass is functioning as
     * expected. However, the collection will limit the results to only those
     * that are displayable.
     * 
     * @throws Exception on exception
     */
    @Test(enabled = false)
    public void searchAsMultiSiteUser() throws Exception {
        log("Searching multi sites data");

        User user = userManager.findByName(USER3);
        setCurrentUser(user);
        setSite(SITE4, false);

        List<ExpectedResult> erAll = buildAll();
        SearchResultList<SearchResult> results = searchManager.searchAll("user");

        assertNotNull(results, "Could not find result list");

        assertTrue(isValidResultList(erAll, results));

        List<ExpectedResult> erCollection = buildAll();
        int i = 0;
        for (ExpectedResult er : erAll) {
            log(i + " Site :: " + er.site + " -- " + erAll.size());
            if (er.site.equals(this.getSite(SITE7).getName())) {
                erCollection.remove(i);
            } else if (er.site.equals("Default")) {
                erCollection.remove(i);
            } else {
                i++;
            }
        }

        for (ExpectedResult er : erAll) {
            log(er.site + " __ " + er.displayName);
        }

        SearchResultDisplayCollection resultCollection = 
            SearchResultDisplayCollection.buildDisplayCollection(results, user, CAT_LIMIT);

        assertNotNull(resultCollection, "Collection is null.");

        System.out.println("Got the collection -- "
            + resultCollection.getCount());

        assertTrue(isValidCollection(erCollection, resultCollection));
        log("all sites passed.");
    }

    /**
     * Searches data as an all site user (admin privileges). This will test
     * whether the user is able to view data associated with their sites and
     * unable to view data not associated with their sites. Data not associated
     * with any site (Not Taggable) is viewable under SearchManager.ALL_SITES.
     * The current site must be set to a valid site, along with all site access
     * set to true. The result collection should be organized correctly, with
     * the current site first and the "All Sites" as last, and the sites in
     * between organized alphabetically. The business areas must be organized
     * alphabetically as well. The find method will return all expected results,
     * which should be checked to ensure that Compass is functioning as
     * expected. However, the collection will limit the results to only those
     * that are displayable.
     * 
     * @throws Exception on exception
     */
    @Test(enabled = false)
    public void searchAsAllSiteUser() throws Exception {
        log("Searching all sites data");

        User user = userManager.findByName("admin");
        setCurrentUser(user);
        setSite(SITE4, false);

        List<ExpectedResult> erAll = buildAll();
        SearchResultList<SearchResult> results = searchManager.searchAll("user");

        assertNotNull(results, "Could not find result list");

        assertTrue(isValidResultList(erAll, results));

        List<ExpectedResult> erCollection = buildAll();

        SearchResultDisplayCollection resultCollection = 
            SearchResultDisplayCollection.buildDisplayCollection(results, user, CAT_LIMIT);

        assertNotNull(resultCollection, "Collection is null.");

        System.out.println("Got the collection -- "
            + resultCollection.getCount());

        assertTrue(isValidCollection(erCollection, resultCollection));
        log("all sites passed.");
    }

    /**
     * Searches data as single site user (admin privileges). This will test
     * whether the user is able to view data associated with their site and
     * unable to view data not associated with their site. The current site must
     * be set to a valid site, along with all site access set to false. The
     * result collection should be organized with all results under the
     * SearchManager.RESULTS tag. The find method will return all expected
     * results, which should be checked to ensure that Compass is functioning as
     * expected. However, the collection will limit the results to only those
     * that are displayable.
     * 
     * @throws Exception on exception
     */
    @Test(enabled = false)
    public void searchAsSingleSiteUser() throws Exception {
        System.out.println("searching as single site user... " + USER5);

        User user = userManager.findByName(USER5);
        setCurrentUser(user);
        setSite(SITE7, false);

        List<ExpectedResult> erAll = buildAll();

        List<ExpectedResult> erCollection = new LinkedList<ExpectedResult>();

        erCollection.add(this.buildResult(SearchManager.RESULTS, USERS, USER5));
        erCollection.add(this.buildResult(
            SearchManager.RESULTS, "Roles", "User"));

        SearchResultList<SearchResult> resultsForUser5 = searchManager
            .searchAll("user");
        assertNotNull(resultsForUser5, "Single site user is null.");

        assertTrue(isValidResultList(erAll, resultsForUser5));

        SearchResultDisplayCollection resultCollection = 
            SearchResultDisplayCollection
                .buildDisplayCollection(resultsForUser5, user, CAT_LIMIT);

        assertNotNull(resultCollection, "Collection is null.");

        System.out.println("Got the collection -- "
            + resultCollection.getCount());

        assertTrue(isValidCollection(erCollection, resultCollection));
    }

    /**
     * Searches as a user that is not able to view a particular aspect of the
     * data, for instance Roles and Notifications. This method will conduct a
     * search that will ONLY return Roles and Notifications within the users
     * site. The result list will display all the valid results, however, the
     * collection will display nothing.
     * 
     * Data:User2 has permission "View Users" User2 is attached to Role5(View
     * Users) and Site4
     * 
     * @throws Exception on exception
     */
    @Test(enabled = false)
    public void searchNonViewableData() throws Exception {
        log("Searching non-viewable data");

        User user = userManager.findByName(USER2);
        setCurrentUser(user);
        setSite(SITE4, false);

        List<ExpectedResult> erAll = buildAll();
        SearchResultList<SearchResult> results = searchManager.searchAll("user");

        assertNotNull(results, "Could not find result list");

        assertTrue(isValidResultList(erAll, results));

        List<ExpectedResult> erCollection = new LinkedList<ExpectedResult>();

        for (SearchResult sr : results.getData()) {
            log("SR: " + sr);
        }

        SearchResultDisplayCollection resultCollection =
            SearchResultDisplayCollection.buildDisplayCollection(results, user, CAT_LIMIT);

        assertNotNull(resultCollection, "Collection is null.");

        System.out.println("Got the collection -- "
            + resultCollection.getCount());

        assertTrue(isValidCollection(erCollection, resultCollection));
        log("Non-viewable data passed.");
    }

    /**
     * Tests that the analyzers are hooked up and multibyte characters are
     * indexed.  This test works when run through TestNG but not when run through
     * ANT.
     * @throws Exception
     */
    @Test(enabled = false)
    public void testMultiByteChars() throws Exception {
        log("Searching for multibyte characters...");

        User user = userManager.findByName("admin");
        setCurrentUser(user);
        setSite(SITE7, false);
        List<ExpectedResult> erAll = new LinkedList<ExpectedResult>();
        erAll.add(this
            .buildResult(this.getSite(SITE7).getName(), USERS, USERMB));

        log("Beginnning to search ---- ");
        LocaleContextHolder.setLocale(new Locale("zh_CN"));
        SearchResultList<SearchResult> results = this.searchManager.searchAll(
            "憫", "admin");

        log("Done searching == " + results.getData().size());
        for (SearchResult sr : results.getData()) {
            log("RESULT: " + sr.getDisplayName());
        }

        assertTrue(results.getData().size() == 1);

        assertTrue(isValidResultList(erAll, results));

        SearchResultDisplayCollection coll = 
            SearchResultDisplayCollection.buildDisplayCollection(results, user, CAT_LIMIT);

        assertTrue(isValidCollection(erAll, coll));
    }

    /**
     * Tests whether the search can handle translated data if the searcher is
     * typing in the translated value.
     * @throws Exception
     */
    @Test(enabled = false)
    public void testDataTranslationNonLocal() throws Exception {
        log("Searching for translated data (nonlocal) ...");

        User user = userManager.findByName("admin");
        setCurrentUser(user);
        setSite(SITE7, false);

        List<ExpectedResult> erAll = new LinkedList<ExpectedResult>();
        erAll
            .add(this.buildResult(this.getSite(SITE7).getName(), USERS, USER5));
        erAll
            .add(this.buildResult(this.getSite(SITE7).getName(), USERS, USER4));

        LocaleContextHolder.setLocale(new Locale("fr_FR"));
        SearchResultList<SearchResult> results = this.searchManager.searchAll(
            "chapeau", "admin");

        assertTrue(isValidResultList(erAll, results));

        SearchResultDisplayCollection coll = 
            SearchResultDisplayCollection.buildDisplayCollection(results, user, CAT_LIMIT);

        assertTrue(isValidCollection(erAll, coll));
    }

    /**
     * Tests whether the search can handle translated data if the searcher is
     * typing in the key value.
     * @throws Exception
     */
    @Test(enabled = false)
    public void testDataTranslationLocal() throws Exception {
        log("Searching for translated data (local) ...");

        User user = userManager.findByName("admin");
        setCurrentUser(user);
        setSite(SITE7, false);

        List<ExpectedResult> erAll = new LinkedList<ExpectedResult>();
        erAll
            .add(this.buildResult(this.getSite(SITE7).getName(), USERS, USER5));
        erAll
            .add(this.buildResult(this.getSite(SITE7).getName(), USERS, USER4));

        SearchResultList<SearchResult> results = this.searchManager.searchAll(
            "hat", "admin");

        assertTrue(isValidResultList(erAll, results));

        SearchResultDisplayCollection coll = 
            SearchResultDisplayCollection.buildDisplayCollection(results, user, CAT_LIMIT);

        assertTrue(isValidCollection(erAll, coll));
    }

    /**
     * Tests whether the initial result list is valid.
     * @param expectedResults the expected results
     * @param results the search results
     * @return whether it is valid
     */
    @Test(enabled = false)
    private boolean isValidResultList(List<ExpectedResult> expectedResults,
                                      SearchResultList<SearchResult> results) {

        log("result list count -- " + results.getData().size());
        for (SearchResult result : results.getData()) {
            for (ExpectedResult er : expectedResults) {
                if (er.isEqual(result)) {
                    assertFalse(er.found, "Duplicate results - " + result);
                    log("Found -- " + result);
                    er.found = true;
                }
            }
        }

        for (ExpectedResult er : expectedResults) {
            assertTrue(er.found, "Result is not found -- " + er.areaName
                + " : " + er.displayName);
        }
        return true;
    }

    /**
     * Tests whether the collection holds the expected results.
     * @param expectedResults the expected results
     * @param collection the search results
     * @return whether it is valid
     */
    @Test(enabled = false)
    private boolean isValidCollection(List<ExpectedResult> expectedResults,
                                      SearchResultDisplayCollection collection) {

        for (ExpectedResult result : expectedResults) {
            log("Result -- " + result.site + " : " + result.displayName);

            for (SearchResultList<SearchResultList<SearchResult>> si : collection
                .getSiteList()) {
                log(si.getName() + " : " + si.getSize());
            }

            SearchResultList<SearchResultList<SearchResult>> site = collection
                .getSiteCollection(result.site);
            assertNotNull(site, "There is no site found for -- " + result.site);

            SearchResultList<SearchResult> areaList = site.get(result.areaName);
            assertNotNull(areaList, "There is no area found for -- "
                + result.areaName);
            log("AREA : " + areaList.getName() + " -- "
                + areaList.getData().size());
            for (SearchResult sr : areaList.getData()) {
                log(site.getName() + "  SR: " + sr);
                if (result.isEqual(sr)) {
                    log("Found - " + result.displayName);
                    result.found = true;
                }
            }
        }

        for (ExpectedResult result : expectedResults) {
            assertTrue(result.found, "Result was not found -- "
                + result.displayName + " : " + result.site);
        }

        return true;
    }

    /**
     * Sets the current user.
     * @param user the current user
     */
    @Test(enabled = false)
    private void setCurrentUser(User user) {
        Authentication auth = new UsernamePasswordAuthenticationToken(
            user, user.getPassword(), user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    /**
     * Sets the site.
     * @param siteId the site id
     * @param allSites whether it is all sites
     */
    @Test(enabled = false)
    private void setSite(Long siteId, boolean allSites) throws Exception {
        setUpSiteContext(true);
        SiteContextHolder.getSiteContext().setCurrentSite(siteId);
        SiteContextHolder.getSiteContext().setHasAllSiteAccess(allSites);
    }

    /**
     * Gets the site.
     * @param id the id of the site
     * @return the site
     * @throws Exception if the site cannot be retrieved
     */
    private Site getSite(Long id) throws Exception {
        return SiteContextHolder.getSiteContext().getSiteInContext(id);
    }

    /**
     * Builds an expected result.
     * @param site the site
     * @param area the area
     * @param display the display name
     * @return the expected result
     */
    @Test(enabled = false)
    private ExpectedResult buildResult(String site, String area, String display) {
        ExpectedResult er = new ExpectedResult();
        er.site = site;
        er.areaName = area;
        er.displayName = display;

        return er;
    }

    /**
     * Builds a list of all possible search results on the query "user".
     * @return all expected results
     * @throws Exception if there is a problem retrieving site info
     */
    @Test(enabled = false)
    private List<ExpectedResult> buildAll() throws Exception {
        List<ExpectedResult> erAll = new LinkedList<ExpectedResult>();
        erAll.add(buildResult(SearchManager.ALL_SITES, "Roles", "User"));
        // erAll.add(buildResult("Default", USERS, "admin"));
        // erAll.add(buildResult("Default", USERS, "vocollect"));

        erAll.add(buildResult(getSite(SITE4).getName(), USERS, USER2));
        erAll.add(buildResult(getSite(SITE4).getName(), USERS, USER3));
        erAll.add(buildResult(getSite(SITE5).getName(), USERS, USER3));
        erAll.add(buildResult(getSite(SITE6).getName(), USERS, USER3));
        erAll.add(buildResult(getSite(SITE4).getName(), USERS, USER4));
        erAll.add(buildResult(getSite(SITE5).getName(), USERS, USER4));
        erAll.add(buildResult(getSite(SITE6).getName(), USERS, USER4));
        erAll.add(buildResult(getSite(SITE7).getName(), USERS, USER5));
        return erAll;
    }

    /**
     * @param msg .
     */
    @Test(enabled = false)
    private void log(String msg) {
        System.out.println(this.getClass().getCanonicalName() + " -- " + msg);
    }

    /**
     * Inner class used to store expected results.
     * @author hulrich
     */
    private class ExpectedResult {

        private String displayName;

        private String areaName;

        private String site;

        private boolean found;

        /**
         * @param res .
         * @return .
         */
        boolean isEqual(SearchResult res) {
            if (displayName.equals(res.getDisplayName())
                && (areaName.equals(res.getArea()))) {
                log("Matched displays");
                return true;
            }
            return false;
        }
    }
}
