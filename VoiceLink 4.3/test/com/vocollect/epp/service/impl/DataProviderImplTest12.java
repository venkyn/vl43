/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.UserDAO;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.ColumnSortType;
import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.ResultData;
import com.vocollect.epp.util.ResultDataInfo;
import com.vocollect.epp.web.util.DataProviderTableUtil;
import com.vocollect.epp.web.util.DisplayUtilities;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test DataProviderImpl logic via various child classes.
 *
 * @author jkercher
 */
@Test(enabled = true, groups = { FAST, SECURITY })
public class DataProviderImplTest12 extends BaseDataProviderImplTestCase {

    // Objects required for data access.
    private UserDAO userdao = null;
    private DisplayUtilities displayUtilities = null;


    /**
     * Pre-method setup code to instantiate a default ResultDataInfo object.
     */
    @BeforeMethod
    public void methodSetup() throws Exception {
        System.out.println("Running Test 12");

        setUpSiteContext(true);
    }

    /**
     * @return .
     */
    public ResultDataInfo makeResultInfo() {
        ResultDataInfo rd = new ResultDataInfo(
            "name", // String tooltipColumn
            "name", // String   sortColumn
            true,   // boolean  sortAscending
            0L,     // long     rowId
            0L,     // long     offset
            0,      // int      rowCount
            0,      // int      startIndex
            20,     // int      sliderHeight
            20,     // int      rowsPerPage
            false,       // int      refreshRequest
            null,
            this.displayUtilities,
            false
        );
        return rd;
    }

    /**
     * @param rdi .
     * @return .
     */
    private ResultData getResults(ResultDataInfo rdi) {
        List<DataObject> dataObjects = DataProviderTableUtil.callGetDataMethod(rdi, userdao, "getAll");
        ResultData rd = DataProviderTableUtil.windowData(rdi, dataObjects, this.displayUtilities);
        return rd;
    }

    /**
     * Pre-class setup code handles database initialization.  This should
     * yield 100 users with IDs ranging from -100 to -199.
     * @throws Exception if an error occurs during database setup.
     */
    @BeforeClass
    public void classSetup() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        // Reset the database to the default state.
        adapter.resetInstallationData();
        // Replace default users with the 100 user dataset.
        adapter.handleFlatXmlResource("data/dbunit/users-12.xml");

    }

    /**
     * @return .
     */
    private Column getColumn() {
        Column c = new Column();
        c.setSortType(ColumnSortType.Normal);
        c.setDefaultSorted(false);
        c.setDefaultSortAsc(false);
        c.setField("name");
        return c;

    }

    /**
     * Get by offset is called when the slider values
     * changes without a tooltip.
     * @throws Exception if an error occurs during test execution
     */
    @Test(enabled = true)
    public void testGetByOffset() throws Exception {
        System.out.println("Testing moving slider");
        ResultDataInfo rdi;
        Column c = getColumn();
        System.out.println("Starting test 1");
        rdi = makeResultInfo();
        rdi.setFirstRowId(null);
        rdi.setOffset(0);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
        rdi.setRefreshRequest(true);
        rdi.setSortColumnObject(c);

        ResultData rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 12);
        assertEquals(rd.getNumPreviousRows(), 0);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-111L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        rdi = makeResultInfo();
        rdi.setFirstRowId(null);
        rdi.setOffset(7);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
        rdi.setRefreshRequest(true);
        rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 12);
        assertEquals(rd.getNumPreviousRows(), 7);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-111L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        rdi = makeResultInfo();
        rdi.setFirstRowId(null);
        rdi.setOffset(11);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
        rdi.setRefreshRequest(true);
        rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 12);
        assertEquals(rd.getNumPreviousRows(), 11);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-111L, (long) rd.getData().get(rd.getData().size() - 1).getId());


        System.out.println("Starting test 4");
        rdi = makeResultInfo();
        rdi.setFirstRowId(null);
        rdi.setOffset(50);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
        rdi.setRefreshRequest(true);
        rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 12);
        assertEquals(rd.getNumPreviousRows(), 0);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-111L, (long) rd.getData().get(rd.getData().size() - 1).getId());


    }
    /**
     * Get by offset is called when the slider values.
     * changes without a tooltip
     * @throws Exception if an error occurs during test execution
     */
    @Test(enabled = true)
    public void testGetById() throws Exception {
        System.out.println("Testing moving tooltip or auto-refresh 12");
        ResultDataInfo rdi;
        Column c = getColumn();


        c.setSortType(ColumnSortType.Normal);
        c.setDefaultSorted(false);
        c.setDefaultSortAsc(false);
        c.setField("name");

        System.out.println("Starting test 1");
        rdi = makeResultInfo();
        rdi.setFirstRowId(new Long(-100));
        rdi.setOffset(10);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        ResultData rd = getResults(rdi);
        printContents(rd);
        assertEquals(rd.getData().size(), 12);
        assertEquals(rd.getNumPreviousRows(), 0);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-111L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 2");
        rdi = makeResultInfo();
        rdi.setFirstRowId(new Long(-107));
        rdi.setOffset(300);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);
        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 12);
        assertEquals(rd.getNumPreviousRows(), 7);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-111L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 3");
        rdi = makeResultInfo();
        rdi.setFirstRowId(new Long(-111));
        rdi.setOffset(0);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 12);
        assertEquals(rd.getNumPreviousRows(), 11);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-111L, (long) rd.getData().get(rd.getData().size() - 1).getId());


        System.out.println("Starting test 10");
        rdi = makeResultInfo();
        rdi.setFirstRowId(new Long(200));
        rdi.setOffset(50);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 12);
        assertEquals(rd.getNumPreviousRows(), 0);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-111L, (long) rd.getData().get(rd.getData().size() - 1).getId());

        System.out.println("Starting test 11");
        rdi = makeResultInfo();
        rdi.setFirstRowId(new Long(200));
        rdi.setOffset(2);
        rdi.setRowCount(60);
        rdi.setStartIndex(-20);
         rdi.setRefreshRequest(true);
         rdi.setSortColumnObject(c);

        rd = getResults(rdi);
        //printContents(rd);
        assertEquals(rd.getData().size(), 12);
        assertEquals(rd.getNumPreviousRows(), 2);
        assertEquals(-100L, (long) rd.getData().get(0).getId());
        assertEquals(-111L, (long) rd.getData().get(rd.getData().size() - 1).getId());
    }

    /**
     * Test a simple upward scroll on the users data set.
     * @throws Exception if an error occurs during test execution
     */
    @Test(enabled = true)
    public void testUserScrollUp() throws Exception {
        // there should be no scrollbar so nothing to test
    }

    /**
     * Test a simple upward scroll on the users data set.
     * @throws Exception if an error occurs during test execution
     */
    @Test(enabled = true)
    public void testUserScrollDown() throws Exception {
        // there should be no scrollbar so nothing to test
    }

    /**
     * Allow object injection.  This is not a test method; set enabled=false
     * to prevent execution by the TestNG framework.
     * @param newUserdao provides access to user data.
     */
    @Test(enabled = false)
    public void setUserDAO(UserDAO newUserdao) {
        this.userdao = newUserdao;
    }

    /**
     * Print out the contents of the given ResultData object to standard
     * output.
     * @param rd .
     */
    private void printContents(ResultData rd) {

        System.out.println("Result Data: total rows: " + rd.getTotalRows());
        System.out.println("Result Data: contents:");
        List<DataObject> dataList = rd.getData();
        for (int i = 0; i < dataList.size(); i++) {
            DataObject data = dataList.get(i);
            System.out.println(data.toString());
        }

    }


    /**
     * Setter for the displayUtilities property.
     * @param displayUtilities the new displayUtilities value
     */
    @Test(enabled = false)
    public void setDisplayUtilities(DisplayUtilities displayUtilities) {
        this.displayUtilities = displayUtilities;
    }

}
