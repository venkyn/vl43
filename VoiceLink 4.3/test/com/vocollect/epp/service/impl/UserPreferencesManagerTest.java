/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.UserDAO;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.View;
import com.vocollect.epp.service.UserPreferencesManager;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.ui.ColumnDTO;

import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.ArrayList;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 *
 * @author ddoubleday
 */
@Test(groups = { FAST })
public class UserPreferencesManagerTest extends BaseServiceTestCase {
    private UserPreferencesManager upMan = null;

    private UserDAO userDAO = null;

    /**
     * @param upm .
     */
    @Test(enabled = false)
    public void setUserPreferencesManager(UserPreferencesManager upm) {
        this.upMan = upm;
    }

    /**
     * @param dao .
     */
    @Test(enabled = false)
    public void setUserDAO(UserDAO dao) {
        this.userDAO = dao;
    }

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        // Add extra sample data.
        adapter.handleFlatXmlResource("data/dbunit/sample-data.xml",
                                      DatabaseOperation.INSERT);
        adapter.handleFlatXmlResource("data/dbunit/userPreferencesTest.xml");
    }

    /**
     * @throws Exception
     */
    @AfterClass
    protected void classTearDown() throws Exception {
       DbUnitAdapter adapter = new DbUnitAdapter();
       //adapter.resetEppData();
       adapter.handleFlatXmlResource("data/dbunit/userPreferencesTest.xml", DatabaseOperation.DELETE_ALL);
    }

    /**
     * @throws DataAccessException
     */
    @Test(enabled = true)
    public void testGetandSaveColumnsNoPrefs() throws DataAccessException {
        // get the prefs
        Long viewId = -1L;
        Long userId = -2L;
        View v = upMan.getView(viewId);
        User u = userDAO.get(userId);
        List<Column> columns = upMan.getColumns(v, u);
        assertNotNull(columns);
        assertEquals(8, columns.size());
        Column c = columns.get(0);
        assertEquals(c.getField(), "name");
        assertEquals(c.getWidth(), 210);
        assertTrue(c.isSorted());
        c = columns.get(1);
        assertEquals(c.getField(), "roles");
        assertEquals(c.getWidth(), 110);
        assertFalse(c.isSorted());
        c = columns.get(2);
        assertEquals(c.getField(), "emailAddress");
        assertEquals(c.getWidth(), 120);
        assertFalse(c.isSorted());
        // set the prefs
        // now update the preferences
        List<ColumnDTO> colPrefs = new ArrayList<ColumnDTO>();
        for (Column col : columns) {
            ColumnDTO dto = new ColumnDTO();
            dto.setColumnId(col.getId());
            dto.setOrder(columns.size() - col.getOrder() + 1);
            dto.setSortAsc(true);
            if (col.getField().equals("roles")) {
                dto.setSorted(true);
            } else {
                dto.setSorted(false);
            }
            dto.setVisible(true);
            dto.setWidth(col.getWidth() + 20);
            colPrefs.add(dto);
        }
        // update the preferences
        upMan.savePreferences(colPrefs, u);

        //verify the set
        List<Column> newColumns = upMan.getColumns(v, u);
        assertNotNull(columns);
        assertEquals(8, columns.size());
        c = newColumns.get(1);
        assertEquals(c.getField(), "notes");
        assertEquals(c.getWidth(), 170);
        assertFalse(c.isSorted());
        /*
        c= newColumns.get(1);
        assertEquals(c.getField(),"emailAddress");
        assertEquals(c.getWidth(), 141);
        assertTrue(c.isSorted());
        c= newColumns.get(2);
        assertEquals(c.getField(),"lastLoginTime");
        assertEquals(c.getWidth(), 221);
        assertFalse(c.isSorted());
        */
    }


    /**
     * @throws DataAccessException
     */
    @Test(enabled = true)
    public void testSavePrefs() throws DataAccessException {
        // test getting preferences that already exist
        Long viewId = -1L;
        Long userid = -1L;
        View v = upMan.getView(viewId);
        User u = userDAO.get(userid);

        List<Column> columns = upMan.getColumns(v, u);

        // first verify the columns have been overridden
        assertNotNull(columns);
        assertEquals(8, columns.size());
        Column c = columns.get(0);
        assertEquals(c.getField(), "enabled");
        assertEquals(c.getWidth(), 71);
        assertFalse(c.isSorted());
        c = columns.get(1);
        assertEquals(c.getField(), "roles");
        assertEquals(c.getWidth(), 111);
        assertFalse(c.isSorted());
        c = columns.get(2);
        assertEquals(c.getField(), "notes");
        assertEquals(c.getWidth(), 81);
        assertFalse(c.isSorted());

        c = columns.get(5);
        assertEquals(c.getField(), "emailAddress");
        assertEquals(c.getWidth(), 121);
        assertTrue(c.isSorted());

        // now update the preferences
        List<ColumnDTO> colPrefs = new ArrayList<ColumnDTO>();
        for (Column col : columns) {
            ColumnDTO dto = new ColumnDTO();
            dto.setColumnId(col.getId());
            dto.setOrder(columns.size() - col.getOrder() + 1);
            dto.setSortAsc(true);
            if (col.getField().equals("emailAddress")) {
                dto.setSorted(true);
            } else {
                dto.setSorted(false);
            }
            dto.setVisible(true);
            dto.setWidth(col.getWidth() + 20);
            colPrefs.add(dto);
        }
        // update the preferences
        upMan.savePreferences(colPrefs, u);

        // now re-get the columns for verification
        System.out.println("(********(((************");
        List<Column> newColumns = upMan.getColumns(v, u);
        assertNotNull(columns);
        assertEquals(8, columns.size());
        c = newColumns.get(1);
        System.out.println(c.getField());
        assertEquals(c.getField(), "name");
        assertEquals(c.getWidth(), 1131);
        assertFalse(c.isSorted());
        c = newColumns.get(2);
        assertEquals(c.getField(), "emailAddress");
        assertEquals(c.getWidth(), 141);
        assertTrue(c.isSorted());
        c = newColumns.get(3);
        assertEquals(c.getField(), "lastLoginTime");
        assertEquals(c.getWidth(), 221);
        assertFalse(c.isSorted());
    }
}
