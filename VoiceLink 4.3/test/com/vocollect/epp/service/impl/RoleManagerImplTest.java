/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;


import com.vocollect.epp.dao.RoleDAOTest;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.model.Feature;
import com.vocollect.epp.model.FeatureGroup;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.service.RoleManager;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 *
 *
 * @author sshaulis
 */
@Test(groups = { FAST, SECURITY })
public class RoleManagerImplTest extends BaseServiceTestCase {
    private RoleManager manager = null;

    static final String ADMIN_ROLE_DESCRIPTION = "Admin test description";
    static final long ADMIN_ROLE_ID = -1;
    static final long ADMIN_USER_EDIT = -3;
    static final long TEST_ROLE_ID = -4;

    /**
     * @param impl the manager
     */
    @Test(enabled = false)
    public void setRoleManager(RoleManager impl) {
        this.manager = impl;
    }

    /**
     * @throws Exception on any failure
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/users-simple.xml", DatabaseOperation.INSERT);
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.RoleManagerImpl.getFeature(Long)'.
     * @throws DataAccessException for any DAO error that occurs
     */
    public void testGetFeature() throws DataAccessException {
        Feature feature = manager.getFeature(ADMIN_USER_EDIT);
        org.testng.AssertJUnit.assertNotNull("roleManagerImplGetFeature", feature);
        long id = feature.getId();
        org.testng.AssertJUnit.assertEquals("roleManagerImplGetFeature", id, ADMIN_USER_EDIT);
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.RoleManagerImpl.getFeatureGroups()'.
     * @throws DataAccessException for any DAO error that occurs
     */
    public void testGetFeatureGroups() throws DataAccessException {
        List<FeatureGroup> groups = manager.getFeatureGroups();
        org.testng.AssertJUnit.assertNotNull("roleManagerImplGetFeatureGroups", groups);
        org.testng.AssertJUnit.assertTrue("roleManagerImplGetFeatureGroups", groups.size() > 0);
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.RoleManagerImpl.getRole(Long)'.
     * @throws DataAccessException for any DAO error that occurs
     */
    public void testGetRoleLong() throws DataAccessException {
        Role role = manager.get(ADMIN_ROLE_ID);
        org.testng.AssertJUnit.assertNotNull("roleManagerImplGetRoleLong", role);
        org.testng.AssertJUnit.assertTrue("roleManagerImplGetRoleLong", role.getId() == ADMIN_ROLE_ID);
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.RoleManagerImpl.getRole(String)'.
     * @throws DataAccessException for any DAO error that occurs
     */
    public void testGetRoleString() throws DataAccessException {
        Role role = manager.findByName(RoleDAOTest.ADMIN_ROLE_NAME);
        org.testng.AssertJUnit.assertNotNull("roleManagerImplGetRoleString", role);
        org.testng.AssertJUnit.assertTrue("roleManagerImplGetRoleString",
            role.getName().equalsIgnoreCase(RoleDAOTest.ADMIN_ROLE_NAME));
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.RoleManagerImpl.getRoles()'.
     * @throws DataAccessException for any DAO error that occurs
     */
    public void testGetRoles() throws DataAccessException {
        List<Role> roles = manager.getAll();
        org.testng.AssertJUnit.assertNotNull("roleManagerImplGetRoles", roles);
        org.testng.AssertJUnit.assertTrue("roleManagerImplGetRoles", roles.size() > 0);
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.RoleManagerImpl.removeRole(Long)'.
     * @throws Exception for any error that occurs
     */
    public void testRemoveRoleLong() throws Exception {

        Role role = null;
        manager.delete(manager.get(TEST_ROLE_ID));
        // this next get should throw an exception
        try {
            role = manager.get(TEST_ROLE_ID);
            assert role == null : "Found role with ID " + TEST_ROLE_ID
                + " after delete";
        } catch (EntityNotFoundException e) {
            // expected
        }
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.RoleManagerImpl.removeRole(Role)'.
     * @throws Exception for any error that occurs
     */
    public void testRemoveRoleRole() throws Exception {
        Role role = null;
        manager.delete(manager.findByName(RoleDAOTest.READONLY_ROLE_NAME));
        // this next findByName should return null
        role = manager.findByName(RoleDAOTest.READONLY_ROLE_NAME);
        assert role == null : "Found role "
            + RoleDAOTest.READONLY_ROLE_NAME + " after delete";
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.RoleManagerImpl.saveRole(Role)'.
     * @throws Exception for any error that occurs
     */
    public void testSaveRole() throws Exception {

        // Reloading data before this test works around a bug we have been
        // unable to find with this test when running against SQL Server. It
        // shouldn't be necessary, though. -- ddoubleday
        classSetUp();

        Role role = new Role();
        role.setName("testSaveRole");
        role.setDescription("test description");
        manager.save(role);
        role = manager.findByName("testSaveRole");
        org.testng.AssertJUnit.assertNotNull("roleManagerImplSaveRole", role);
        org.testng.AssertJUnit.assertTrue("roleManagerImplSaveRole", role.getDescription().equals("test description"));
    }

    /**
     * Test method for 'com.vocollect.epp.service.impl.RoleManagerImpl.setRoleDAO(RoleDAO)'
     * and 'com.vocollect.epp.service.impl.RoleManagerImpl.getProviderDAO()'.
     */
    public void testGetSetRoleDAO() {
        org.testng.AssertJUnit.assertNotNull(
            "roleManagerImplGetSetRoleDAO", manager.getProviderDAO());
    }
}
