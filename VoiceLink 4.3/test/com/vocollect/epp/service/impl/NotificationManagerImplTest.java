/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.service.NotificationManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.LOPArrayList;
import com.vocollect.epp.util.SiteContextHolder;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * This class test various operations of NotificationManager.
 * @author ddoubleday
 */
@Test(groups = { NOTIFICATIONS, FAST })
public class NotificationManagerImplTest extends BaseServiceTestCase {

    // injected
    private NotificationManager notificationManager;

    // injected
    private UserManager userManager;

    /**
     * Auto Setup notificationManager by Spring.
     * @param nManager .
     */
    @Test(enabled = false)
    public void setNotificationManager(NotificationManager nManager) {
        this.notificationManager = nManager;
    }

    /**
     * Getter for the notificationManager property.
     * @return the value of the property
     */
    private NotificationManager getNotificationManager() {
        return this.notificationManager;
    }

    /**
     * Getter for the userManager property.
     * @return the value of the property
     */
    private UserManager getUserManager() {
        return this.userManager;
    }

    /**
     * Setter for the userManager property.
     * @param userManager the new userManager value
     */
    @Test(enabled = false)
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    @Test(enabled = false)
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
            "data/dbunit/notification-test.xml", DatabaseOperation.INSERT);
    }



    // TODO: Implement these tests. The email tests will require
    // refactoring so the email engine is injected. -- ddoubleday
//    public void testSendEmailNotification() {
//        fail("Not yet implemented");
//    }
//
//    public void testUpdateAck() {
//        fail("Not yet implemented");
//    }
//
//    public void testCreateNotificationAndEmail() {
//        fail("Not yet implemented");
//    }
//
//    public void testExecutePurge() {
//        fail("Not yet implemented");
//    }

    /**
     * Test method for special case queries from the DAO.
     */
    public void testAllQueries() throws Exception {
        setUpSiteContext(true);
        List<Object[]> notificationInfo =
            getNotificationManager().getGroupedCountBySite(
                    getUserManager().findByName("user 3"),
                    NotificationPriority.CRITICAL);
        assertEquals(notificationInfo.size(), 5, "getGroupedCountBySite");

        List<String> userEmails =
            getNotificationManager().getUserEmailsBySiteId(-1L);
        assertEquals(userEmails.size(), 1);

        Number max = getNotificationManager().getMaxNotificationIdForSiteId(
            NotificationPriority.CRITICAL, -1L);
        assertEquals(max.intValue(), 7, "getMaxNotificationIdForSiteId");

        Calendar cal = Calendar.getInstance();
        cal.set(2006, Calendar.NOVEMBER, 1);
        Date date = cal.getTime();
        List<Notification> notifications =
            getNotificationManager().listOlderThan(new QueryDecorator(), date);
        assertEquals(notifications.size(), 1, "listOlderThan");

        // Get the first Notification
        List<Notification> nList = notificationManager.getAll();
        Notification first = nList.get(0);

        Map<String, Object> nmap =
            getNotificationManager().getNotificationMap(first.getId());
        assertEquals(nmap.get("Message"), first.getMessage(),
            "notification map message");
        assertEquals(nmap.get("Application"), first.getApplication(),
            "notification map application");

        // SystemNotification
        Notification sysNotification =
            getNotificationManager().systemNotification();
        assertEquals("notification.column.keyname.Application.2",
            sysNotification.getApplication(), "systemNotification");
    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void testCreateNotification() throws Exception {

        // Simulate setting up the context as the purge/archive managers do. 
        // No current site, but they are in system mode.
        setUpSiteContextNoDefaultSite(true);
        SiteContextHolder.getSiteContext().setToSystemMode();


        // Create Purge/Archive Notification
        LOPArrayList lop = new LOPArrayList();
        getNotificationManager().createNotification("systemConfiguration.purgearchive.title",
            "errorCode.message.EPP104",
            NotificationPriority.CRITICAL,
            new Date(),
            "100",
            "notification.column.keyname.Application.2",
            lop);

        System.err.println("Test breakpoint");
    }


}
