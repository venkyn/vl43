/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.errors;

import com.vocollect.epp.logging.LoggingError;

import static com.vocollect.epp.test.TestGroups.EXCEPTIONS;
import static com.vocollect.epp.test.TestGroups.FAST;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;


/**
 * Test class for <code>UserMessage</code> class.
 *
 * @author Dennis Doubleday
 */
@Test(groups = { FAST, EXCEPTIONS })
public class UserMessageTest {

    private final String testKey = "test.key";
    private final String testMsg = "Testing key match";

    private final int one = 1;
    private final int two = 2;
    private final int three = 3;

    /**
     * Test method for 'UserMessage.UserMessage(String)'.
     */
    public void testUserMessageString() {
        UserMessage msg = new UserMessage(testKey);
        assertEquals(testMsg, msg.getKey(), testKey);
    }

    /**
     * Test method for 'UserMessage.UserMessage(String, Object...)'.
     */
    public void testUserMessageStringObjectArray() {
        UserMessage msg = new UserMessage(testKey, one, two, three);
        assertEquals(testMsg, msg.getKey(), testKey);
        assertEquals("object count", msg.getValues().size(), three);
    }

    /**
     * Test method for 'UserMessage.UserMessage(ErrorCode)'.
     */
    public void testUserMessageErrorCode() {
        UserMessage msg = new UserMessage(LoggingError.NO_ERROR);
        assertEquals(testMsg, msg.getKey(), LoggingError.NO_ERROR
            .getDefaultMessageKey());
    }

    /**
     * Test method for 'UserMessage.UserMessage(ErrorCode, Object...)'.
     */
    public void testUserMessageErrorCodeObjectArray() {
        UserMessage msg = new UserMessage(LoggingError.NO_ERROR, one, two, three);
        assertEquals(testMsg, msg.getKey(), LoggingError.NO_ERROR
            .getDefaultMessageKey());
        assertEquals("object count", msg.getValues().size(), three);
    }

    /**
     * Test method for 'UserMessage.getKey()'.
     */
    public void testGetKey() {
        UserMessage msg = new UserMessage(testKey);
        assertEquals(testMsg, msg.getKey(), testKey);
    }

    /**
     * Test method for 'UserMessage.getValues()'.
     */
    public void testGetValues() {
        UserMessage msg = new UserMessage(testKey, one, two, three);
        assertEquals("object count", msg.getValues().size(), three);
        assert (Integer) msg.getValues().get(0) == one : "first argument matches";
        assert (Integer) msg.getValues().get(1) == two : "second argument matches";
        assert (Integer) msg.getValues().get(2) == three : "third argument matches";
    }

    /**
     * Test method for 'UserMessage.toString()'.
     */
    public void testToString() {
        UserMessage msg = new UserMessage(testKey, one, two, three);
        assert msg.toString().contains(testKey);
    }

}
