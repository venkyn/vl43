/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.errors;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertTrue;


/**
 * The test cases for this set of classes.
 *
 * Note that many tests simply need to call constructors to test various error
 * code range scenarios; in such cases the constructor is called but is not
 * assigned to any variable.
 *
 * @author dgold
 */
public class ErrorCodeTest {

    private String prefix = "";
    private static final long LOW_END = 1000;
    private static final long HIGH_END = 1999;


    /*
     * These next 8 tests test the interaction between distinct classes, all
     * inheriting from the base ErrorCode, which is abstract. This is a test of
     * the actual use case for the ErrorCode as specified in the EDD.
     *
     */

    /**
     * Test that 2 classes inheriting form ErrorCode with no overlap and
     * malformations throw no exceptions.
     *
     */
    @Test()
    public final void testWellBehaved() {

        // boolean to show if this is in an expected state or not.
        boolean expected = true;
        // Well behaved classes, no dups or overlaps, etc
        ExampleError.NO_ERROR.toString();
        try {
            WellBehaved.NO_ERROR.toString();
        } catch (Throwable e) {
            expected = false;
        }
        assertTrue(expected);
    }

    /**
     * Test to check that an error code range can consist of exactly one error
     * code. This tests that the bounds (high & low) are inclusive.
     */
    @Test()
    public final void testSingleErrorRange() {

        // boolean to show if this is in an expected state or not.
        boolean expected = true;
        // Well behaved classes, no dups or overlaps, etc
        ExampleError.NO_ERROR.toString();
        try {
            SingleErrorRange.NO_ERROR.toString();
        } catch (Throwable e) {
            expected = false;
        }
        assertTrue(expected);
    }

    /**
     * Test that an overlap in the upper end of the range throws an exception.
     *
     */
    @Test()
    public final void testUpperRangeOverlapped() {
        ExampleError.NO_ERROR.toString();

        // boolean to show if this is in an expected state or not.
        boolean expected = false;
        // Range violations - overlap on upper end of range
        try {
            UpperRangeOverlapped.NO_ERROR.toString();
        } catch (Throwable e) {
            expected = true;
        }
        assertTrue(expected);
    }

    /**
     * Test that an overlap on the low end of the range throws an exception.
     *
     */
    @Test()
    public final void testLowerRangeOverlapped() {

        ExampleError.NO_ERROR.toString();
        // boolean to show if this is in an expected state or not.
        boolean expected = false;
        expected = false;
        try {
            LowerRangeOverlapped.NO_ERROR.toString();
        } catch (Throwable e) {
            expected = true;
        }
        assertTrue(expected);
    }

    /**
     * Test that a rnage cannot be created in the middle of another range.
     *
     */
    @Test()
    public final void testMidRangeOverlapped() {

        ExampleError.NO_ERROR.toString();
        // boolean to show if this is in an expected state or not.
        boolean expected = false;
        expected = false;
        try {
            MidRangeOverlapped.NO_ERROR.toString();
        } catch (Throwable e) {
            expected = true;
        }
        assertTrue(expected);
    }

    /**
     * Test that you can't create a range that contains another range - sort of
     * the obverse of the previous test.
     *
     */
    @Test()
    public final void testWholeRangeOverlapped() {
        ExampleError.NO_ERROR.toString();
        boolean expected = false;
        try {
            WholeRangeOverlapped.NO_ERROR.toString();
        } catch (Throwable e) {
            expected = true;
        }
        assertTrue(expected);
    }

    /**
     * Test that you cannot duplicate error codes within a range.
     *
     */
    @Test()
    public final void testDuplicateErrorCode() {
        ExampleError.NO_ERROR.toString();
        boolean expected = false;
        try {
            DupError.NO_ERROR.toString();
        } catch (Throwable e) {
            expected = true;
        }
        assertTrue(expected);
    }

    /**
     * Test that you a misformed error range throws an exception.
     *
     */
    @Test()
    public final void testReversedRange() {
        ExampleError.NO_ERROR.toString();
        boolean expected = false;
        try {
            ReversedRange.NO_ERROR.toString();
        } catch (Throwable e) {
            expected = true;
        }
        assertTrue(expected);
    }

    /**
     * Test that you can't add an error code which is out-of-range.
     *
     */
    @Test()
    public final void testOutOfRange() {
        ExampleError.NO_ERROR.toString();
        boolean expected = false;
        try {
            OutOfRange.NO_ERROR.toString();
        } catch (Throwable e) {
            expected = true;
        }
        assertTrue(expected);
    }


    /**
     * Test that you can't have an error class with no range.
     *
     */
    @Test()
    public final void testNoRange() {
        ExampleError.NO_ERROR.toString();
        boolean expected = false;
        try {
            NoRange.NO_ERROR.toString();
        } catch (Throwable e) {
            expected = true;
        }
        assertTrue(expected);
    }

    /**
     * Test that you can't have an error class with no range.
     *
     */
    @Test()
    public final void testSeparatePrefix() {
        ExampleError.NO_ERROR.toString();
        boolean expected = true;
        try {
            MirrorError.NO_ERROR.toString();
        } catch (Throwable e) {
            expected = false;
        }
        assertTrue(expected);
    }


    /**
     * Test method for 'com.vocollect.epp.logging.ErrorCode.ErrorCode(String,
     * long, long)'.
     */
    @Test()
    public final void testErrorCodeStringLongLong() {
        this.prefix = "testErrorCodeStringLongLong";
        Exerciser ee = new Exerciser(this.prefix, LOW_END, HIGH_END);

        assertNotNull(ee);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorCode.ErrorCode(ErrorCode,
     * long)'.
     */
    @Test()
    public final void testErrorCodeErrorCodeLong() {
        this.prefix = "testErrorCodeErrorCodeLong";
        boolean thrown = false;
        try {
            new Exerciser(WellBehaved.NO_ERROR, LOW_END);
        } catch (RuntimeException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorCode.getRange()'.
     */
    @Test()
    public final void testGetRange() {
        ErrorRange er = ExampleError.SECOND_ERROR.getRange();
        assertNotNull(er);
        assertTrue(er.getLowEndOfRange() == ExampleError.LOW_END);
        assertTrue(er.getHighEndOfRange() == ExampleError.HIGH_END);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorCode.getRangeMax()'.
     */
    @Test()
    public final void testGetRangeMax() {
        ErrorRange er = ExampleError.SECOND_ERROR.getRange();
        long grm = ExampleError.SECOND_ERROR.getRangeMax();
        assertTrue(grm == er.getHighEndOfRange());
        assertTrue(grm == ExampleError.HIGH_END);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorCode.getRangeMin()'.
     */
    @Test()
    public final void testGetRangeMin() {
        ErrorRange er = ExampleError.SECOND_ERROR.getRange();
        long grm = ExampleError.SECOND_ERROR.getRangeMin();
        assertTrue(grm == er.getLowEndOfRange());
        assertTrue(grm == ExampleError.LOW_END);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorCode.getErrorCode()'.
     */
    @Test()
    public final void testGetErrorCode() {
        final int testValue = 102;
        assertTrue(testValue == ExampleError.SECOND_ERROR.getErrorCode());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorCode.isErrorCodeSet()'.
     */
    @Test()
    public final void testIsErrorCodeSet() {
        assertTrue(ExampleError.SECOND_ERROR.isErrorCodeSet());
    }

    /**
     * Test method for
     * 'com.vocollect.epp.logging.ErrorCode.clearErrorCodeIsSet()'.
     */
    @Test()
    public final void testClearErrorCodeIsSet() {
        this.prefix = "testClearErrorCodeIsSet";
        Exerciser ee = new Exerciser(WellBehaved.NO_ERROR, WellBehaved.NO_ERROR.getRangeMax() - 1);
        assertTrue(ee.isErrorCodeSet());
        ee.clearErrorCodeIsSet();
        assertFalse(ee.isErrorCodeSet());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorCode.asString()'.
     */
    @Test()
    public final void testAsString() {
        // Should be getPrefix() + "-" + getErrorCode()
        ExampleError ee = ExampleError.SECOND_ERROR;
        String ex = ee.toString();

        assertTrue(0 == ex.compareTo(ee.getPrefix() + "-" + ee.getErrorCode()));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorCode.getPrefix()'.
     */
    @Test()
    public final void testGetPrefix() {
        assertTrue(0 == "Ex".compareTo(ExampleError.SECOND_ERROR.getPrefix()));
    }


    /**
     * Model error class, that does everything right and against which the tests
     * are run.
     * @author dgold
     *
     */
    static final class ExampleError extends ErrorCode {

        public static final ExampleError
        NO_ERROR = new ExampleError(),
        SECOND_ERROR = new ExampleError(102),
        FOURTH_ERROR = new ExampleError(104),
        FIFTH_ERROR =  new ExampleError(105);

        @Deprecated public static final ExampleError
        THIRD_ERROR = new ExampleError(103),
        SIXTH_ERROR = new ExampleError(106);

        public static final int LOW_END = 100;
        public static final int HIGH_END = 999;

        /**
         * Constructor.
         */
        private ExampleError() {
            super("Ex", LOW_END, HIGH_END);
        }

        /**
         * Constructor.
         * @param err .
         */
        private ExampleError(long err) {
            super(ExampleError.NO_ERROR, err);
        }
    }

    /**
     * Another model class, which coexists with the example above.
     * @author dgold
     *
     */
    static final class WellBehaved extends ErrorCode {
        public static final WellBehaved
        NO_ERROR = new WellBehaved();
        public static final int LOW_END = 10;
        public static final int HIGH_END = 99;

        /**
         * Constructor.
         */
        private WellBehaved() {
            super("Ex", LOW_END, HIGH_END);
        }
    }

    /**
     * Another model class, where the range is exactly one error code.
     *
     * @author dgold
     */
    static final class SingleErrorRange extends ErrorCode {
        public static final SingleErrorRange
        NO_ERROR = new SingleErrorRange(),
        SINGLE_ERROR = new SingleErrorRange(1);

        /**
         * Constructor.
         */
        private SingleErrorRange() {
            super("Ex", 1, 1);
        }
        /**
         * Constructor.
         * @param err .
         */
        private SingleErrorRange(long err) {
            super(SingleErrorRange.NO_ERROR, err);
        }
    }

    /**
     * A poorly behaved class - that overlaps the range of the WellBehaved class.
     * @author dgold
     *
     */
    static final class UpperRangeOverlapped extends ErrorCode {
        public static final int LOW_END = 80;
        public static final int HIGH_END = 100;

        public static final UpperRangeOverlapped
        NO_ERROR = new UpperRangeOverlapped();

        /**
         * Constructor.
         */
        private UpperRangeOverlapped() {
            super("Ex", LOW_END, HIGH_END);
        }
    }

    /**
     * A poorly behaved class - that overlaps the range of the WellBehaved class.
     * @author dgold
     *
     */
    static final class LowerRangeOverlapped extends ErrorCode {
        public static final int LOW_END = 1;
        public static final int HIGH_END = 40;

        public static final LowerRangeOverlapped
        NO_ERROR = new LowerRangeOverlapped();

        /**
         * Constructor.
         */
        private LowerRangeOverlapped() {
            super("Ex", LOW_END, HIGH_END);
        }
    }

    /**
     * A poorly behaved class - that overlaps the range of the WellBehaved class.
     * @author dgold
     *
     */
    static final class MidRangeOverlapped extends ErrorCode {
        public static final int LOW_END = 49;
        public static final int HIGH_END = 60;

        public static final MidRangeOverlapped
        NO_ERROR = new MidRangeOverlapped();

        /**
         * Constructor.
         */
        private MidRangeOverlapped() {
            super("Ex", LOW_END, HIGH_END);
        }
    }
    /**
     * A poorly behaved class - that overlaps the range of the WellBehaved class.
     * @author dgold
     *
     */
    static final class WholeRangeOverlapped extends ErrorCode {
        public static final int LOW_END = 1;
        public static final int HIGH_END = 200;

        public static final WholeRangeOverlapped
        NO_ERROR = new WholeRangeOverlapped();

        /**
         * Constructor.
         */
        private WholeRangeOverlapped() {
            super("Ex", LOW_END, HIGH_END);
        }
    }

    /**
     * A poorly behaved class that has an error code duplicated.
     * @author dgold
     *
     */
    static final class DupError extends ErrorCode {
        public static final int LOW_END = 1001;
        public static final int HIGH_END = 1999;

        public static final DupError
        NO_ERROR = new DupError(),
        FIRST_ERROR = new DupError(1001),
        SECOND_ERROR = new DupError(1002),
        THIRD_ERROR = new DupError(1001);

        /**
         * Constructor.
         */
        private DupError() {
            super("Ex", LOW_END, HIGH_END);
        }
        /**
         * Constructor.
         * @param err .
         */
        private DupError(long err) {
            super(DupError.NO_ERROR, err);
        }
    }

    /**
     * A poorly behaved class that has the range [low > high].
     * @author dgold
     *
     */
    static final class ReversedRange extends ErrorCode {
        public static final int LOW_END = 2100;
        public static final int HIGH_END = 2000;

        public static final ReversedRange
        NO_ERROR = new ReversedRange();

        /**
         * Constructor.
         */
        private ReversedRange() {
            super("Ex", LOW_END, HIGH_END);
        }

    }

    /**
     * A poorly behaved class that tries to accept an error code which is not in
     * its declared range.
     *
     * @author dgold
     *
     */
    static final class OutOfRange extends ErrorCode {
        public static final int LOW_END = 3000;
        public static final int HIGH_END = 4000;

        public static final OutOfRange
        NO_ERROR = new OutOfRange(),
        FIRST_ERROR = new OutOfRange(2000);

        /**
         * Constructor.
         */
        private OutOfRange() {
            super("Ex", LOW_END, HIGH_END);
        }
        /**
         * Constructor.
         * @param err .
         */
        private OutOfRange(long err) {
            super(ReversedRange.NO_ERROR, err);
        }

    }

    /**
     * Simple class with no members.
     *
     * @author dgold
     */
    static class Exerciser extends ErrorCode {

        /**
         * Constructor.
         * @param s .
         * @param low .
         * @param hi .
         */
        public Exerciser(String s, long low, long hi) {
            super(s, low, hi);
        }
        /**
         * Constructor.
         * @param pattern .
         * @param err .
         */
        public Exerciser(ErrorCode pattern, long err) {
            super(pattern, err);
        }

    }

    /**
     * Class to prove that you cannot instantiate an error class with no range.
     *
     *
     * @author dgold
     */
    static final class NoRange extends ErrorCode {
        public static final int LOW_END = 4001;
        public static final int HIGH_END = 4999;

        public static final NoRange
        NO_ERROR = new NoRange(4001);

        /**
         * Constructor.
         */
        private NoRange() {
            super("Ex", LOW_END, HIGH_END);
        }
        /**
         * Constructor.
         * @param err .
         */
        private NoRange(long err) {
            super(NoRange.NO_ERROR, err);
        }
    }

    /**
     * Copy of the model error class, which duplicates the range and codes for
     * the ExampleError class, but uses a different prefix.
     * @author dgold
     *
     */
    static final class MirrorError extends ErrorCode {

        public static final int LOW_END = 100;
        public static final int HIGH_END = 999;

        public static final MirrorError
        NO_ERROR = new MirrorError(),
        SECOND_ERROR = new MirrorError(102),
        FOURTH_ERROR = new MirrorError(104),
        FIFTH_ERROR =  new MirrorError(105);

        @Deprecated public static final MirrorError
        THIRD_ERROR = new MirrorError(103),
        SIXTH_ERROR = new MirrorError(106);

        /**
         * Constructor.
         */
        private MirrorError() {
            super("Mirror", LOW_END, HIGH_END);
        }

        /**
         * Constructor.
         * @param err .
         */
        private MirrorError(long err) {
            super(MirrorError.NO_ERROR, err);
        }
    }
}
