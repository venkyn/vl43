/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.errors;

import com.vocollect.epp.logging.exceptions.BadErrorRangeException;
import com.vocollect.epp.logging.exceptions.DuplicateErrorTypeException;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;


/**
 *
 *
 * @author dgold
 */
public class ErrorRangeTest {

    private String prefix       = "pfx";

    private String otherPrefix  = "ofx";

    private final long   lowestEnd    = 0;

    private final long   smallHighEnd = 9;

    private final long   lowEnd       = 100;

    private final long   highEnd      = 199;

    private final long   higherEnd    = 299;

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.ErrorRange()'
     */
    @Test()
    public void testErrorRange() {
        this.prefix = "testErrorRange";
        new ErrorRange();
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.ErrorRange(String, long, long)'
     */
    @Test()
    public void testErrorRangeStringLongLong() {
        this.prefix = "testErrorRangeStringLongLong";
        new ErrorRange(this.prefix, this.lowEnd, this.highEnd);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.setRange(String, long, long)'
     */
    @Test()
    public void testSetRange() {
        this.prefix = "testSetRange";
        this.otherPrefix = "testSetRange_";
        boolean badErrorRangePassed = true;
        boolean duplicateErrorTypePassed = true;

        ErrorRange er = new ErrorRange();
        ErrorRange er1 = new ErrorRange();
        try {
            badErrorRangePassed = false;
            er.setRange(prefix, this.highEnd, this.lowEnd);
        } catch (BadErrorRangeException e1) {
            badErrorRangePassed = true;
        }
        assertTrue(badErrorRangePassed);

        er.setRange(this.prefix, this.lowestEnd, this.smallHighEnd);
        er.register();
        try {
            duplicateErrorTypePassed = false;
            er1.setRange(this.prefix, this.smallHighEnd, this.higherEnd);
        } catch (DuplicateErrorTypeException e) {
            duplicateErrorTypePassed = true;
        }
        assertTrue(duplicateErrorTypePassed);

        er1.setRange(this.prefix, this.highEnd + 1, this.higherEnd);
        er1.setRange(this.otherPrefix, this.lowEnd, this.highEnd);

        assertTrue(this.smallHighEnd == er.getHighEndOfRange());
        assertTrue(this.lowestEnd == er.getLowEndOfRange());

        assertTrue(this.lowEnd == er1.getLowEndOfRange());
        assertTrue(this.highEnd == er1.getHighEndOfRange());

    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.rangeOverlap(String, long, long)'
     */
    @Test()
    public void testRangeOverlap() {
        this.prefix = "testRangeOverlap";
        this.otherPrefix = "testRangeOverlap_";
        new ErrorRange(this.prefix, this.lowEnd, this.highEnd); // NOPMD

        assertTrue(ErrorRange.rangeOverlap(
            this.prefix, this.lowEnd, this.highEnd));
        assertTrue(!ErrorRange.rangeOverlap(
            this.prefix, this.highEnd + 1, this.higherEnd));
        assertTrue(!ErrorRange.rangeOverlap(
            this.prefix, this.lowEnd - 1, this.lowEnd - 1));
        assertTrue(ErrorRange.rangeOverlap(
            this.prefix, this.lowEnd - 1, this.lowEnd));
        assertTrue(ErrorRange.rangeOverlap(
            this.prefix, this.highEnd, this.highEnd));
        assertTrue(ErrorRange.rangeOverlap(
            this.prefix, this.highEnd - 2, this.highEnd - 1));
        assertTrue(!ErrorRange.rangeOverlap(
            this.otherPrefix, this.lowEnd, this.highEnd));

    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.rangeExists(long, long)'
     */
    @Test()
    public void testRangeExistsLongLong() {
        this.prefix = "testRangeExistsLongLong";
        new ErrorRange(this.prefix, this.lowEnd, this.highEnd); // NOPMD
        assertTrue(ErrorRange.rangeExists(this.lowEnd, this.highEnd));

        assertTrue(!ErrorRange.rangeExists(this.lowEnd + 1, this.highEnd));
        assertTrue(!ErrorRange.rangeExists(this.lowEnd, this.highEnd + 1));
        assertTrue(!ErrorRange.rangeExists(this.lowEnd - 1, this.highEnd));
        assertTrue(!ErrorRange.rangeExists(this.lowEnd, this.highEnd - 1));
        assertTrue(!ErrorRange.rangeExists(this.lowEnd + 1, this.highEnd - 1));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.registerRange(String, long, long)'
     */
    @Test()
    public void testRegisterRange() {
        this.prefix = "testRegisterRange";
        this.otherPrefix = "testRegisterRange_";
        assertTrue(ErrorRange.registerRange(
            this.prefix, this.lowEnd, this.highEnd));
        assertTrue(ErrorRange.registerRange(
            this.prefix, this.highEnd + 1, this.higherEnd));
        assertTrue(!ErrorRange.registerRange(
            this.prefix, this.lowEnd, this.highEnd));
        assertTrue(ErrorRange.registerRange(
            this.otherPrefix, this.lowEnd, this.highEnd));

    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.register()'
     */
    @Test()
    public void testRegister() {
        this.prefix = "testRegister";
        this.otherPrefix = "testRegister_";
        ErrorRange er1 = new ErrorRange();

        er1.setRange(this.prefix, this.lowEnd, this.highEnd);
        assertTrue(er1.register());
        assertFalse(er1.register());
        er1.setRange(this.otherPrefix, this.lowEnd, this.highEnd);
        assertTrue(er1.register());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.contains(long)'
     */
    @Test()
    public void testContains() {
        this.prefix = "testContains";
        ErrorRange er1 = new ErrorRange(this.prefix, this.lowEnd, this.highEnd);
        assertTrue(er1.isInRange(this.lowEnd));
        assertTrue(er1.isInRange(this.highEnd));
        assertTrue(er1.isInRange(this.lowEnd + 1));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.getHighEndOfRange()'
     */
    @Test()
    public void testGetHighEndOfRange() {
        this.prefix = "testGetHighEndOfRange";
        ErrorRange er1 = new ErrorRange(this.prefix, this.lowEnd, this.highEnd);
        assertTrue(this.highEnd == er1.getHighEndOfRange());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.getLowEndOfRange()'
     */
    @Test()
    public void testGetLowEndOfRange() {
        this.prefix = "testGetLowEndOfRange";
        ErrorRange er1 = new ErrorRange(this.prefix, this.lowEnd, this.highEnd);
        assertTrue(this.lowEnd == er1.getLowEndOfRange());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.isRangeSet()'
     */
    @Test()
    public void testIsRangeSet() {

    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.getPrefix()'
     */
    @Test()
    public void testGetPrefix() {
        this.prefix = "testGetPrefix";
        ErrorRange er1 = new ErrorRange(this.prefix, this.lowEnd, this.highEnd);
        assertTrue(0 == this.prefix.compareTo(er1.getPrefix()));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorRange.setPrefix(String)'
     */
    @Test()
    public void testSetPrefix() {
        this.prefix = "testSetPrefix";
        ErrorRange er1 = new ErrorRange();
        er1.setPrefix(this.prefix);
        assertTrue(0 == this.prefix.compareTo(er1.getPrefix()));
    }

}
