/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */


package com.vocollect.epp.errors;

import com.vocollect.epp.logging.LoggingError;
import com.vocollect.epp.logging.exceptions.DuplicateErrorCodeException;
import com.vocollect.epp.scheduling.SchedulingErrorCode;
import com.vocollect.epp.search.SearchErrorCode;
import com.vocollect.epp.security.SecurityErrorCode;
import com.vocollect.epp.ui.UserPreferencesErrorCode;
import com.vocollect.epp.util.ZipUtilErrorCode;
import com.vocollect.epp.web.action.DataProviderErrorCodeRoot;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Tests the Error Code range and make sures that each 
 * error code range is unique.
 *
 * @author revans
 */

public class EPPErrorCodeDuplicateTest {

   /**
     * Test that there are no overlapping error ranges.
     * If duplicate ranges then exception is thrown.
     * 
     */
    @Test()
    public final void testDuplicateErrorCodeException() {
        
        // Boolean to show the expected state.
        boolean duplicateException = false;

        try {
            ZipUtilErrorCode.NO_ERROR.toString();
        } catch (DuplicateErrorCodeException e) {
            duplicateException = true;
        } catch (ExceptionInInitializerError e) {
            duplicateException = true;
        }
        assertTrue(!duplicateException);

        /*
         * TODO: Obfuscate is running before and renaming class giving a Class
         * not found exception. Need to find out how this is running to make
         * this test pass. try { LicenseErrorCode.NO_ERROR.toString(); } catch
         * (DuplicateErrorCodeException e) { duplicateException = true; } catch
         * (ExceptionInInitializerError e) { duplicateException = true; }
         * assertTrue(!duplicateException);
         */

        try {
            DataProviderErrorCodeRoot.NO_ERROR.toString();
        } catch (DuplicateErrorCodeException e) {
            duplicateException = true;
        } catch (ExceptionInInitializerError e) {
            duplicateException = true;
        }
        assertTrue(!duplicateException);

        try {
            LoggingError.NO_ERROR.toString();
        } catch (DuplicateErrorCodeException e) {
            duplicateException = true;
        } catch (ExceptionInInitializerError e) {
            duplicateException = true;
        }
        assertTrue(!duplicateException);

        try {
            ReportErrorRoot.NO_ERROR.toString();
        } catch (DuplicateErrorCodeException e) {
            duplicateException = true;
        } catch (ExceptionInInitializerError e) {
            duplicateException = true;
        }
        assertTrue(!duplicateException);

        try {
            SchedulingErrorCode.NO_ERROR.toString();
        } catch (DuplicateErrorCodeException e) {
            duplicateException = true;
        } catch (ExceptionInInitializerError e) {
            duplicateException = true;
        }
        assertTrue(!duplicateException);

        try {
            SearchErrorCode.NO_ERROR.toString();
        } catch (DuplicateErrorCodeException e) {
            duplicateException = true;
        } catch (ExceptionInInitializerError e) {
            duplicateException = true;
        }
        assertTrue(!duplicateException);

        try {
            SecurityErrorCode.NO_ERROR.toString();
        } catch (DuplicateErrorCodeException e) {
            duplicateException = true;
        } catch (ExceptionInInitializerError e) {
            duplicateException = true;
        }
        assertTrue(!duplicateException);

        try {
            SystemErrorCode.NO_ERROR.toString();
        } catch (DuplicateErrorCodeException e) {
            duplicateException = true;
        } catch (ExceptionInInitializerError e) {
            duplicateException = true;
        }
        assertTrue(!duplicateException);

        try {
            UserPreferencesErrorCode.NO_ERROR.toString();
        } catch (DuplicateErrorCodeException e) {
            duplicateException = true;
        } catch (ExceptionInInitializerError e) {
            duplicateException = true;
        }
        assertTrue(!duplicateException);                    
    }     
}
