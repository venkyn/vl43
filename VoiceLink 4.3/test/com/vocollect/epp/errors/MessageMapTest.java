/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.errors;

import static com.vocollect.epp.test.TestGroups.EXCEPTIONS;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.Iterator;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.fail;


/**
 * Test class for <code>MessageMap</code> class.
 *
 * @author Dennis Doubleday
 */
@Test(groups = { FAST, EXCEPTIONS })
public class MessageMapTest {

    private final String testKey = "test.key";
    private final String testKey2 = "test2.key";
    private final String noMessages = "no messages";
    private final String cat1 = "cat1";
    private final String cat2 = "cat2";

    private MessageMap map = null;

    /**
     * Setup run before tests.
     */
    @BeforeMethod()
    public void setup() {
        this.map = new MessageMap();

    }

    /**
     * Test method for 'MessageMap.MessageMap()'.
     */
    public void testMessageMap() {
        assert !map.iterator().hasNext() : noMessages;
    }

    /**
     * Test method for 'MessageMap.add(UserMessage)'.
     */
    public void testAddUserMessage() {
        try {
            map.add((UserMessage) null);
            fail("Adding null message should throw exception");
        } catch (IllegalArgumentException e) {
            // Expected failure, do nothing.
        }
        assert !map.iterator().hasNext() : noMessages;
        map.add(new UserMessage(testKey));
        assertEquals(
            "check message text", map.iterator().next().getKey(), testKey);

    }

    /**
     * Test method for 'MessageMap.add(String, UserMessage)'.
     */
    public void testAddStringUserMessage() {
        try {
            map.add(null, (UserMessage) null);
            fail("Adding null message should throw exception");
        } catch (IllegalArgumentException e) {
            // Expected failure, do nothing.
        }
        assert !map.iterator().hasNext() : noMessages;
        map.add(MessageMap.DEFAULT_CATEGORY, new UserMessage(testKey));
        // Should add to same default category
        map.add(null, new UserMessage(testKey2));
        Iterator<UserMessage> iterator = map.iterator();
        assertEquals("check message text", iterator.next().getKey(), testKey);
        assertEquals(
            "check message2 text", iterator.next().getKey(), testKey2);

    }

    /**
     * Test method for 'MessageMap.add(MessageMap)'.
     */
    public void testAddMessageMap() {
        map.add((MessageMap) null);
        assert !map.iterator().hasNext() : noMessages;
        MessageMap map2 = new MessageMap().add(
            cat1, new UserMessage("test1.key")).add(
            cat2, new UserMessage(testKey2));
        map.add(map2);
        assertEquals("cat1 message count", map.getSize(cat1), 1);
        assertEquals("cat2 message count", map.getSize(cat2), 1);
        assertEquals("cat1 message", map.getMessages(cat1).iterator().next()
            .getKey(), "test1.key");
        assertEquals("cat2 message", map.getMessages(cat2).iterator().next()
            .getKey(), testKey2);
    }

    /**
     * Test method for 'MessageMap.clear()'.
     */
    public void testClear() {
        map.add(new UserMessage(testKey));
        map.clear();
        assert map.isEmpty();

    }

    /**
     * Test method for 'MessageMap.isEmpty()'.
     */
    public void testIsEmpty() {
        assert map.isEmpty() : "map should be empty";
        map.add(new UserMessage(testKey));
        assert !map.isEmpty() : "map should not be empty";

    }

    /**
     * Test method for 'MessageMap.iterator()'.
     */
    public void testIterator() {
        assert !map.iterator().hasNext() : noMessages;
        map.add(MessageMap.DEFAULT_CATEGORY, new UserMessage(testKey));
        // Add to a different category
        map.add(cat1, new UserMessage(testKey2));
        Iterator<UserMessage> iterator = map.iterator();
        iterator.next();
        // Should be one more
        assert iterator.hasNext() : "iterator should have had another";
        iterator.next();
        assert !iterator.hasNext() : "iterator reports more than expected";
    }

    /**
     * Test method for 'MessageMap.getMessages(String)'.
     */
    public void testGetMessages() {
        assert !map.getMessages(cat1).iterator().hasNext() : noMessages;
        map.add(MessageMap.DEFAULT_CATEGORY, new UserMessage(testKey));
        // Add to a different category
        map.add(cat1, new UserMessage(testKey2));
        Iterator<UserMessage> iterator = map.getMessages(cat1).iterator();
        iterator.next();
        // Should be only one in this category
        assert !iterator.hasNext() : "iterator reports more than expected";

    }

    /**
     * Test method for 'MessageMap.getCategoryNames()'.
     */
    public void testGetCategoryNames() {
        map.add(cat1, new UserMessage(testKey));
        // Add to a different category
        map.add(cat2, new UserMessage(testKey2));
        assertEquals("category count", map.getCategoryNames().size(), 2);
        for (String name : map.getCategoryNames()) {
            assert name.equals(cat1) || name.equals(cat2) : "bad category name";
        }
    }

    /**
     * Test method for 'MessageMap.getSize()'.
     */
    public void testGetSize() {
        assertEquals("empty size", map.getSize(), 0);
        map.add(cat1, new UserMessage(testKey));
        // Add to a different category
        map.add(cat2, new UserMessage(testKey2));
        assertEquals("size", map.getSize(), 2);
    }

    /**
     * Test method for 'MessageMap.getSize(String)'.
     */
    public void testGetSizeString() {
        assertEquals("empty size", map.getSize(cat1), 0);
        map.add(cat1, new UserMessage(testKey));
        // Add to a different category
        map.add(cat2, new UserMessage(testKey2));
        assertEquals("empty size", map.getSize(cat1), 1);

    }

    /**
     * Test method for 'MessageMap.toString()'.
     */
    public void testToString() {
        assert !map.toString().contains(testKey) : "bad empty toString";
        map.add(cat1, new UserMessage(testKey));
        assert map.toString().contains(testKey) : "bad toString with key";
    }

}
