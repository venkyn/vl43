/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.errors;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;


/**
 *
 *
 * @author dgold
 */
public class ErrorListTest {

    private final long errorValue = 10L;

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorList.ErrorList()'.
     * the constructor
     */
    @Test()
    public final void testErrorList() {
        new ErrorList();
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorList.addError(long)'.
     */
    @Test()
    public final void testAddError() {
        ErrorList el = new ErrorList();
        el.addError(this.errorValue);
        assertTrue(el.isRegistered(this.errorValue));
        assertFalse(el.addError(this.errorValue));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ErrorList.registered(long)'.
     */
    @Test()
    public final void testRegistered() {
        ErrorList el = new ErrorList();
        el.register(this.errorValue);
        assertTrue(el.isRegistered(this.errorValue));

    }

}
