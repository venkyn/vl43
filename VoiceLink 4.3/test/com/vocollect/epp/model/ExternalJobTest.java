/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import static com.vocollect.epp.test.TestGroups.EJS;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * 
 * 
 * @author khazra
 */
@Test(groups = { FAST, MODEL, EJS })
public class ExternalJobTest {

    /**
     * Test the hash code generates equal number
     */
    @Test()
    public void testHashCodeEqual() {
        ExternalJob externalJob1 = new ExternalJob();
        ExternalJob externalJob2 = new ExternalJob();
        
        externalJob1.setName("Job1");
        externalJob1.setCommand("test.bat");
        externalJob1.setJobType(ExternalJobType.Executable);
        externalJob1.setWorkingDirectory("c:\test1");
        
        externalJob2.setName("Job1");
        externalJob2.setCommand("test.bat");
        externalJob2.setJobType(ExternalJobType.Executable);
        externalJob2.setWorkingDirectory("c:\test1");

        assertEquals("Hash codes are not same", externalJob2.hashCode(),
            externalJob1.hashCode());
    }

    /**
     * Test the hash code generates different number
     */
    @Test()
    public void testHashCodeDifferent() {
        ExternalJob externalJob1 = new ExternalJob();
        ExternalJob externalJob2 = new ExternalJob();
        
        externalJob1.setName("Job1");
        externalJob1.setCommand("test.bat");
        externalJob1.setJobType(ExternalJobType.Executable);
        externalJob1.setWorkingDirectory("c:\test1");
        
        externalJob2.setName("Job2");
        externalJob2.setCommand("test.bat");
        externalJob2.setJobType(ExternalJobType.Executable);
        externalJob2.setWorkingDirectory("c:\test1");


        assertEquals("Hash code should be different",
            externalJob1.hashCode() != externalJob2.hashCode(), true);
    }

    /**
     * Tests two types are equal
     */
    @Test()
    public void testEqual() {
        ExternalJob externalJob1 = new ExternalJob();
        ExternalJob externalJob2 = new ExternalJob();
        
        externalJob1.setName("Job1");
        externalJob1.setCommand("test.bat");
        externalJob1.setJobType(ExternalJobType.Executable);
        externalJob1.setWorkingDirectory("c:\test1");
        
        externalJob2.setName("Job1");
        externalJob2.setCommand("test.bat");
        externalJob2.setJobType(ExternalJobType.Executable);
        externalJob2.setWorkingDirectory("c:\test1");

        assertEquals("Types should be equal", externalJob1.equals(externalJob2), true);
    }

    /**
     * Tests two types are not equal
     */
    @Test()
    public void testNotEqual() {
        ExternalJob externalJob1 = new ExternalJob();
        ExternalJob externalJob2 = new ExternalJob();
        
        externalJob1.setName("Job1");
        externalJob1.setCommand("test.bat");
        externalJob1.setJobType(ExternalJobType.Executable);
        externalJob1.setWorkingDirectory("c:\test1");
        
        

        // When compared with other object
        Object obj = new Object();

        assertEquals("Objects should not be equal", externalJob1.equals(obj), false);

        // Name of one job is null
        externalJob2.setName(null);
        externalJob2.setCommand("test.bat");
        externalJob2.setJobType(ExternalJobType.Executable);
        externalJob2.setWorkingDirectory("c:\test1");

        assertEquals("Types should not be equal", externalJob1.equals(externalJob2),
            false);

        // When compared with different external job
        externalJob2.setName("Job2");
        externalJob2.setCommand("test.bat");
        externalJob2.setJobType(ExternalJobType.Executable);
        externalJob2.setWorkingDirectory("c:\test1");

        assertEquals("Types should not be equal", externalJob1.equals(externalJob2),
            false);
    }
}
