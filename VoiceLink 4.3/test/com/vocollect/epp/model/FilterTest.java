/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.dao.BaseDAOTestCase;
import com.vocollect.epp.dao.ColumnDAO;
import com.vocollect.epp.ui.Operand;
import com.vocollect.epp.ui.OperandSetup;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.FILTERING;
import static com.vocollect.epp.test.TestGroups.MODEL;

import java.util.LinkedList;

import org.testng.annotations.Test;


/**
 * 
 *
 * @author jstonebrook
 */
@Test(groups = { FAST, FILTERING, MODEL })
public class FilterTest extends BaseDAOTestCase {

    private ColumnDAO columnDAO;
    
    /**
     * @param dao .
     */
    @Test(enabled = false) 
    public void setColumnDAO(ColumnDAO dao) {
        this.columnDAO = dao;
    }

    /**
     * Tests the starts with operand with one value.
     */
    public void testStartsWithOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand startsWithOperand = OperandSetup.getOperandById(-1L);  // Starts with operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, startsWithOperand, "test", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( upper(obj.name) like 'TEST%' ) "));
    }
    
    /** 
     * Tests the starts with operand with two values.
     */
    public void testStartsWithOperandTwoValues() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand startsWithOperand = OperandSetup.getOperandById(-1L);  // Starts with operand
     
        FilterCriterion filterCriterion1 = 
            new FilterCriterion(column, startsWithOperand, "test", "", false);
        
        FilterCriterion filterCriterion2 = 
            new FilterCriterion(column, startsWithOperand, "test2", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion1);
        filterCriterionList.add(filterCriterion2);
        filter.setFilterCriteria(filterCriterionList);
        
        String hql = filter.toHQL();
        assertTrue(hql.contains(" upper(obj.name) like 'TEST%' "));
        assertTrue(hql.contains(" upper(obj.name) like 'TEST2%' "));
        assertTrue(hql.contains(" or "));
    }
   

    /** 
     * Tests the starts with operand with two columns.
     */
    public void testStartsWithOperandTwoColumns() throws Exception {
        Filter filter = new Filter();
        
        Column column1 = columnDAO.get(-1L);  // This is the user name
        Column column2 = columnDAO.get(-2L);  // This is the email address 
        Operand startsWithOperand = OperandSetup.getOperandById(-1L);  // Starts with operand
     
        FilterCriterion filterCriterion1 = 
            new FilterCriterion(column1, startsWithOperand, "test", "", false);
        
        FilterCriterion filterCriterion2 = 
            new FilterCriterion(column2, startsWithOperand, "test2", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion1);
        filterCriterionList.add(filterCriterion2);
        filter.setFilterCriteria(filterCriterionList);
        
        String hql = filter.toHQL();
        assertTrue(hql.contains(" upper(obj.name) like 'TEST%' "));
        assertTrue(hql.contains(" upper(obj.emailAddress) like 'TEST2%' "));
        assertTrue(hql.contains(" ) and ( "));
    }
    

    /** 
     * Tests the starts with operand with two columns.
     */
    public void testStartsWithOperandTwoColumnsAndThreeValues() throws Exception {
        Filter filter = new Filter();
        
        Column column1 = columnDAO.get(-1L);  // This is the user name
        Column column2 = columnDAO.get(-2L);  // This is the email address 
        Operand startsWithOperand = OperandSetup.getOperandById(-1L);  // Starts with operand
     
        FilterCriterion filterCriterion1 = 
            new FilterCriterion(column1, startsWithOperand, "test", "", false);
 
        FilterCriterion filterCriterion1a = 
            new FilterCriterion(column1, startsWithOperand, "testa", "", false);

        FilterCriterion filterCriterion2 = 
            new FilterCriterion(column2, startsWithOperand, "test2", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion1);
        filterCriterionList.add(filterCriterion1a);
        filterCriterionList.add(filterCriterion2);
        filter.setFilterCriteria(filterCriterionList);
        
        String hql = filter.toHQL();
        assertTrue(hql.contains(" upper(obj.name) like 'TEST%' "));
        assertTrue(hql.contains(" or "));
        assertTrue(hql.contains(" upper(obj.name) like 'TESTA%' "));
        assertTrue(hql.contains(" upper(obj.emailAddress) like 'TEST2%' "));
        assertTrue(hql.contains(" ) and ( "));
    }
 
    /**
     * Tests the ends with operand with one value.
     */
    public void testEndsWithOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand operand = OperandSetup.getOperandById(-2L);  // Ends with operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, operand, "test", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( upper(obj.name) like '%TEST' ) "));
    }

    /**
     * Tests the contains operand with one value.
     */
    public void testContainsOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand operand = OperandSetup.getOperandById(-3L);  // Contains operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, operand, "test", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( upper(obj.name) like '%TEST%' ) "));
    }

    /**
     * Tests the equal operand with one value.
     */
    public void testEqualOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand operand = OperandSetup.getOperandById(-11L);  // Equal operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, operand, "5", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( obj.name = 5 ) "));
    }

    /**
     * Tests the greater than operand with one value.
     */
    public void testGreaterThanOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand operand = OperandSetup.getOperandById(-12L);  // Greater than operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, operand, "5", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( obj.name > 5 ) "));
    }

    /**
     * Tests the greater than or equal operand with one value.
     */
    public void testGreaterThanOrEqualOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand operand = OperandSetup.getOperandById(-13L);  // Greater than or equal operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, operand, "5", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( obj.name >= 5 ) "));
    }

    /**
     * Tests the less than operand with one value.
     */
    public void testLessThanOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand operand = OperandSetup.getOperandById(-14L);  // Less than operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, operand, "5", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( obj.name < 5 ) "));
    }

    /**
     * Tests the less than or equal operand with one value.
     */
    public void testLessThanOrEqualOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand operand = OperandSetup.getOperandById(-15L);  // Less than or equal operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, operand, "5", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( obj.name <= 5 ) "));
    }

    /**
     * Tests the between operand with one value.
     */
    public void testBetweenOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand operand = OperandSetup.getOperandById(-16L);  // between operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, operand, "5", "10", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( obj.name > 5 and obj.name < 10 ) "));
    }

    /**
     * Tests the within operand with one value.
     */
    public void testWithinOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand operand = OperandSetup.getOperandById(-23L);  // within operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, operand, "12/31/2006", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( obj.name >= :name0 ) "));
    }

    /**
     * Tests the not within operand with one value.
     */
    public void testNotWithinOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand operand = OperandSetup.getOperandById(-22L);  // not within operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, operand, "12/31/2006", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( obj.name < :name0 ) "));
    }


    /**
     * Tests the Equal To operand with one value.
     */
    public void testEqualToOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand operand = OperandSetup.getOperandById(-31L);  // equal to operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, operand, "1", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( obj.name = 1 ) "));
    }

    /**
     * Tests the Not Equal To operand with one value.
     */
    public void testNotEqualToOperand() throws Exception {
        Filter filter = new Filter();
        
        Column column = columnDAO.get(-1L);  // This is the user name
        Operand operand = OperandSetup.getOperandById(-32L);  // not equal to operand
     
        FilterCriterion filterCriterion = 
            new FilterCriterion(column, operand, "1", "", false);
        
        LinkedList<FilterCriterion> filterCriterionList = new LinkedList<FilterCriterion>();
        filterCriterionList.add(filterCriterion);
        filter.setFilterCriteria(filterCriterionList);
        
        assertTrue(filter.toHQL().contains(" ( obj.name <> 1 ) "));
    }
}
