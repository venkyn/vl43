/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;


import com.vocollect.epp.util.DateUtil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertTrue;


/**
 *
 *
 * @author dgold
 */
public class FileViewTest {

    // File that has no name
    private File   noFile            = null;

    // typical, small, log file
    private File   logFile           = null;

    // logfile with a timestamp in the name
    private File   timestampedFile   = null;

    // error logfile, bigger than logFile but under a meg
    private File   errLogFile        = null;

    // logfile, under a meg, but over a kb
    private File   mediumLogFile     = null;

    // logfile, over a meg
    private File   largeLogFile      = null;

    // logfile with no extension
    private File   noExtLog          = null;

    private String logFileName       = "testFileView.log";

    private String timestampFileName = null;

    private String errLogFileName    = null;

    private String mediumLogFileName    = null;

    private String largeLogFileName  = null;

    private String now               = null;

    private final String fileTypeLog = "log";
    private final String falseString = "false";
    private final String trueString = "true";



    /**
     * @see TestCase#setUp()
     */
    @BeforeClass
    protected void setUp() throws Exception {

        this.noFile = new File("");

        this.logFile = new File(this.logFileName);

        this.now = timestamp();
        this.timestampFileName = "testFileView." + this.now + ".log";

        writeFile(
            this.timestampFileName,
            "Test file contents, just so the file size > 0", 0, 0);

        this.timestampedFile = new File(this.timestampFileName);

        this.errLogFileName = "testErrLog." + this.now + ".err";

        this.mediumLogFileName = "testMediumLog." + this.now + ".log";

        writeFile(
            this.errLogFileName,
            "Test error file contents, just so the file size > 0 and different than the timestamped file.",
            200, 100);
        this.errLogFile = new File(this.errLogFileName);

        this.largeLogFileName = "largeLog." + this.now + ".log";

        writeFile(
            this.largeLogFileName,
            "Test error file contents, with a > 1MB file.", 2000000, 1000);
        this.largeLogFile = new File(this.largeLogFileName);

        writeFile(
            this.mediumLogFileName,
            "Test log file contents, with a > 1KB file.", 20000, 1000);
        this.mediumLogFile = new File(this.mediumLogFileName);

        writeFile(
            "localhost.2006-06-07.log",
            "Test error file contents, just so the file size > 0", 200, 100);

        writeFile(
            "sec_fault.html",
            "Test error file contents, just so the file size > 0", 200, 100);

        writeFile(
            "testfile.zip",
            "Test error file contents, just so the file size > 0", 200, 100);

        this.timestampedFile = new File(this.timestampFileName);
        this.errLogFile = new File(this.errLogFileName);
        this.noExtLog = new File("noExtension");
    }

    /**
     * @see TestCase#tearDown()
     */
    @AfterTest()
    protected void tearDown() throws Exception {
        this.noFile.delete();
        this.logFile.delete();
        this.timestampedFile.delete();
        this.largeLogFile.delete();
        this.mediumLogFile.delete();
        File temp = new File("localhost.2006-06-07.log");
        temp.delete();
        temp = new File("sec_fault.html");
        temp.delete();
        temp = new File("testfile.zip");
        temp.delete();
    }

    /**
     *
     */
    @Test()
    public void testMisc() {
        new FileView(this.noFile);
        new FileView(this.logFile);
        new FileView(this.timestampedFile);
        // This file is here to exercise convertFileSize for files > 1MB
        //new FileView(this.largeLogFile);

        new FileView(this.noExtLog);

        new FileView(new File(timestampFileName + ".1"));

    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.toString()'.
     */
    @Test()
    public void testToString() {
        FileView f1 = new FileView(this.noFile);
        FileView f2 = new FileView(this.logFile);
        FileView f3 = new FileView(this.timestampedFile);
        assertTrue(0 == f1.toString().compareTo(""));
        assertTrue(0 == f2.toString().compareTo(this.logFileName));
        assertTrue(0 == f3.toString().compareTo(this.timestampFileName));
    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.equals(Object)'.
     */
    @Test()
    public void testEqualsObject() {
        FileView f1 = new FileView(this.noFile);
        FileView f2 = new FileView(this.logFile);
        FileView f3 = new FileView(this.timestampedFile);
        FileView f4 = new FileView(this.timestampedFile);
        String otherType = "testString";

        assertFalse(f1.equals(otherType));

        assertTrue(f1.equals(f1));

        assertFalse(f1.equals(f2));
        assertFalse(f2.equals(f1));
        assertFalse(f2.equals(f3));
        assertFalse(f3.equals(f2));
        assertFalse(f2.equals(f4));
        assertFalse(f4.equals(f2));
        assertTrue(f3.equals(f4));
        assertTrue(f4.equals(f3));

        f1.setFileName(null);

        assertTrue(f1.equals(f1));

        assertFalse(f1.equals(f2));
        assertFalse(f2.equals(f1));

    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.FileView(File)'.
     */
    @Test()
    public void testFileViewFile() {
        FileView f1 = new FileView(this.noFile);
        assertNotNull(f1);
        FileView f2 = new FileView(this.logFile);
        assertNotNull(f2);
        FileView f3 = new FileView(this.timestampedFile);
        assertNotNull(f3);
    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.getFileModifiedDate()'.
     */
    @Test()
    public void testGetFileModifiedDate() {
        FileView f1 = new FileView(this.noFile);
        FileView f2 = new FileView(this.logFile);
        FileView f3 = new FileView(this.timestampedFile);

        String modDate = DateUtil.formatTimestampForTableComponent(
            new Date(f1.getActualModificationDate())
        );
        assertTrue(0 == f1.getFileModifiedDate().compareTo(modDate));

        modDate = DateUtil.formatTimestampForTableComponent(
            new Date(f2.getActualModificationDate())
        );
        assertTrue(0 == f2.getFileModifiedDate().compareTo(modDate));

        modDate = DateUtil.formatTimestampForTableComponent(
            new Date(f3.getActualModificationDate())
        );
        assertTrue(0 == f3.getFileModifiedDate().compareTo(modDate));
    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.getFileName()'.
     */
    @Test()
    public void testGetFileName() {
        FileView f1 = new FileView(this.noFile);
        FileView f2 = new FileView(this.logFile);
        FileView f3 = new FileView(this.timestampedFile);

        assertTrue(0 == f1.getFileName().compareTo(""));
        assertTrue(0 == f2.getFileName().compareTo(this.logFileName));
        assertTrue(0 == f3.getFileName().compareTo(this.timestampFileName));
    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.setFileName(String)'.
     */
    @Test()
    public void testSetFileName() {
        FileView f1 = new FileView(this.noFile);
        f1.setFileName("test");
        assertTrue(0 == f1.getFileName().compareTo("test"));

    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.getFileType()'.
     */
    @Test()
    public void testGetFileType() {
        FileView f1 = new FileView(this.noFile);
        FileView f2 = new FileView(this.logFile);
        FileView f3 = new FileView(this.timestampedFile);
        FileView f4 = new FileView(this.errLogFile);

        assertTrue(LogFileType.UNKNOWN == f1.getFileType());
        assertTrue(LogFileType.LOG == f2.getFileType());
        assertTrue(LogFileType.LOG == f3.getFileType());
        assertTrue(LogFileType.ERROR == f4.getFileType());
    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.getDisplayName()'.
     */
    @Test()
    public void testGetDisplayName() {
        FileView f2 = new FileView(this.logFile);

        assertNotNull(f2.getDisplayName());
    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.getFileType(String)'.
     */
    @Test()
    public void testGetFileTypeString() {
        LogFileType fileType = FileView.getFileType(this.errLogFileName);
        assertTrue(0 == fileType.compareTo(LogFileType.ERROR));

        fileType = FileView.getFileType("");
        assertTrue(LogFileType.UNKNOWN == fileType);
        fileType = FileView.getFileType(this.logFileName);
        assertTrue(LogFileType.LOG == fileType);
        fileType = FileView.getFileType(this.timestampFileName);
        assertTrue(LogFileType.LOG == fileType);

    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.getFileCreatedDate()'.
     */
    @Test()
    public void testGetFileCreatedDate() {
        FileView f1 = new FileView(this.noFile);
        FileView f2 = new FileView(this.logFile);

        assertFalse(0 == f1.getFileCreatedDate().compareTo(""));
        assertFalse(0 == f1.getFileCreatedDate().compareTo(""));
        assertFalse(0 == f1.getFileCreatedDate().compareTo(""));
        assertFalse(0 == f2.getFileCreatedDate().compareTo(""));
        assertFalse(0 == f2.getFileCreatedDate().compareTo(""));
        assertFalse(0 == f2.getFileCreatedDate().compareTo(""));
    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.getActualFileSize()'.
     */
    @Test()
    public void testGetActualFileSize() {
        FileView f1 = new FileView(this.noFile);
        FileView f2 = new FileView(this.logFile);
        FileView f3 = new FileView(this.timestampedFile);

        assertTrue(0 == f1.getActualFileSize());
        assertTrue(0 == f2.getActualFileSize());
        assertFalse(0 == f3.getActualFileSize());
    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.getFileSuffix()'
     */
    @Test()
    public void testGetFileSuffix() {
        FileView f1 = new FileView(this.noFile);
        FileView f2 = new FileView(this.largeLogFile);
        FileView f3 = new FileView(this.mediumLogFile);

        f1.setFileSuffix(f1.getActualFileSize());
        assertTrue(("bytes").equals(f1.getFileSuffix()));
        f2.setFileSuffix(f2.getActualFileSize());
        assertTrue(("MB").equals(f2.getFileSuffix()));
        f3.setFileSuffix(f3.getActualFileSize());
        assertTrue(("KB").equals(f3.getFileSuffix()));
    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.setFileSuffix(Long)'
     */
    @Test()
    public void testSetFileSuffix() {
        FileView f1 = new FileView(this.logFile);
        f1.setFileSuffix(f1.getActualFileSize());
        assertTrue(!f1.getFileSuffix().equals(""));
        assertTrue(f1.getFileSuffix().equals("bytes"));

    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.getSelected()'.
     */
    @Test()
    public void testGetSelected() {
        FileView f1 = new FileView(this.noFile);
        FileView f2 = new FileView(this.logFile);
        FileView f3 = new FileView(this.timestampedFile);
        FileView f4 = new FileView(this.errLogFile);

        assertTrue(0 == f1.getSelected().compareTo(falseString));
        assertTrue(0 == f2.getSelected().compareTo(falseString));
        assertTrue(0 == f3.getSelected().compareTo(falseString));
        assertTrue(0 == f4.getSelected().compareTo(falseString));

    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.setSelected(String)'.
     */
    @Test()
    public void testSetSelected() {
        FileView f1 = new FileView(this.noFile);
        FileView f2 = new FileView(this.logFile);
        FileView f3 = new FileView(this.timestampedFile);
        assertTrue(0 == f1.getSelected().compareTo(falseString));
        assertTrue(0 == f2.getSelected().compareTo(falseString));
        assertTrue(0 == f3.getSelected().compareTo(falseString));

        f1.setSelected(trueString);
        assertFalse(0 == f1.getSelected().compareTo(falseString));
        assertTrue(0 == f1.getSelected().compareTo(trueString));

        assertTrue(0 == f2.getSelected().compareTo(falseString));
        assertTrue(0 == f3.getSelected().compareTo(falseString));
        f1.setSelected(falseString);
        assertTrue(0 == f1.getSelected().compareTo(falseString));
        assertFalse(0 == f1.getSelected().compareTo(trueString));
        assertTrue(0 == f2.getSelected().compareTo(falseString));
        assertTrue(0 == f3.getSelected().compareTo(falseString));

    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.getId()'.
     */
    @Test()
    public void testGetId() {
        FileView f1 = new FileView(this.noFile);
        FileView f2 = new FileView(this.logFile);
        FileView f3 = new FileView(this.timestampedFile);
        FileView f4 = new FileView(this.errLogFile);

        assertFalse(f1.getId() == f2.getId());
        assertFalse(f1.getId() == f3.getId());
        assertFalse(f1.getId() == f4.getId());
        assertFalse(f2.getId() == f3.getId());
        assertFalse(f2.getId() == f4.getId());
        assertFalse(f3.getId() == f4.getId());

        assertTrue(f1.getId() == f1.getId());
        assertTrue(f2.getId() == f2.getId());
        assertTrue(f3.getId() == f3.getId());
        assertTrue(f4.getId() == f4.getId());
    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.setId(Long)'.
     */
    @Test()
    public void testSetId() {
        FileView f1 = new FileView(this.noFile);
        long oldID = f1.getId();
        f1.setId(oldID + 1);
        assertTrue(f1.getId() == oldID + 1);

    }

    /**
     * Test method for 'com.vocollect.epp.model.FileView.getActualModificationDate()'.
     */
    @Test()
    public void testGetActualModificationDate() {
        FileView f1 = new FileView(this.noFile);
        FileView f2 = new FileView(this.logFile);
        FileView f3 = new FileView(this.timestampedFile);
        FileView f4 = new FileView(this.errLogFile);

        assertTrue(f1.getActualModificationDate() == this.noFile.lastModified());
        assertTrue(f2.getActualModificationDate() == this.logFile
            .lastModified());
        assertTrue(f3.getActualModificationDate() == this.timestampedFile
            .lastModified());
        assertTrue(f4.getActualModificationDate() == this.errLogFile
            .lastModified());

    }

    /**
     * @return .
     */
    private String timestamp() {
        String retval = null;
        Calendar cal = Calendar.getInstance();

        retval = "" + cal.get(Calendar.YEAR) + "" + cal.get(Calendar.MONTH)
            + "" + cal.get(Calendar.DAY_OF_MONTH) + "."
            + cal.get(Calendar.HOUR_OF_DAY) + "" + cal.get(Calendar.MINUTE)
            + "" + cal.get(Calendar.MILLISECOND);
        return retval;
    }

    /**
     * @param fileName .
     * @param fileHeader .
     * @param fileSize .
     * @param lineLength .
     */
    private void writeFile(String fileName,
                           String fileHeader,
                           long fileSize,
                           int lineLength) {

        try {
            FileWriter out = new FileWriter(fileName);
            if (null != fileHeader) {
                out.write(fileHeader);
            }
            if ((fileSize > 0) && (lineLength > 0)) {
                for (int i = 0; i <= fileSize / lineLength; i++) {
                    for (int j = 0; j <= lineLength; j++) {
                        out.write('x');
                    }
                    out.write('\n');
                }

            }
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
