/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.acegisecurity.GrantedAuthority;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;


/**
 *
 *
 * @author sshaulis
 */
@Test(groups = { FAST, SECURITY, MODEL })
public class UserTest {

    /**
     * Test method for 'com.vocollect.epp.model.User.hashCode()'.
     */
    public void testHashCode() {
        User user1 = new User();
        user1.setName("user");
        User user2 = new User();
        user2.setName("user");
        assertEquals("hashCode", user1.hashCode(), user2.hashCode());
        assertTrue("hashCodeEquals", user1.equals(user2));
        user2.setName("user2");
        assertFalse("hashCodeDiff", user1.hashCode() == user2.hashCode());
    }


    /**
     * Test method for 'com.vocollect.epp.model.User.toString()'.
     */
    public void testToString() {
        User user1 = new User();
        user1.setName("user");
        assertTrue("toString", user1.toString().contains("name=user"));
    }

    /**
     * Test method for 'com.vocollect.epp.model.User.equals(Object)'.
     */
    public void testEqualsObject() {
        User user1 = new User();
        User user2 = new User();
        FeatureGroup fg1 = new FeatureGroup();

        fg1.setName("group");
        fg1.setDescription("description");
        user1.setName("user");
        user2.setName("user");
        assertTrue("equals", user1.equals(user2));
        user2.setName("user2");
        assertFalse("equalDiffNames", user1.equals(user2));
        assertFalse("equalDiffObjects", user1.equals(fg1));
        assertFalse("equalsNullObjects", user1.equals(null));
    }

    /**
     * Test get/set methods for 'com.vocollect.epp.model.User'.
     */
    public void testGetSetMethods() {
        final Integer zero = 0;

        Date now = new Date();
        User user1 = new User();
        Set<Role> roles = new HashSet<Role>();
        Role role = new Role();

        Set<UserProperty> userProperties = new HashSet<UserProperty>();
        UserProperty userProperty = new UserProperty();

        Set<Site> sites = new HashSet<Site>();
        Site site = new Site();

        user1.setName("name");
        user1.setPassword("password");
        user1.setEmailAddress("user@vocollect.com");
        user1.setLastLoginLocation("location");
        user1.setEnabled(true);
        user1.setLastLoginTime(now);
        user1.setNotes("notes");
        assertEquals("userName", user1.getName(), "name");
        assertEquals("userUserName", user1.getUsername(), "name");
        assertEquals("userPassword", user1.getPassword(), "password");
        assertEquals("userEmail", user1.getEmailAddress(), "user@vocollect.com");
        assertEquals("userLastLogonLocation", user1.getLastLoginLocation(), "location");
        assertEquals("userLastLoginTime", user1.getLastLoginTime(), now);
        assertEquals("userNotes", user1.getNotes(), "notes");
        assertTrue("userAccountNonExpired", user1.isAccountNonExpired());
        assertTrue("userAccountNonLocked", user1.isAccountNonLocked());
        assertTrue("userCreditialsNonExpired", user1.isCredentialsNonExpired());
        assertTrue("userIsEnabled", user1.isEnabled());
        user1.setEnabled(false);
        assertFalse(user1.isEnabled());
        GrantedAuthority[] authorities1 = user1.getAuthorities();
        assertNull("userNullAuthorities", authorities1);

        assertNull("userNullRoles", user1.getRoles());
        role.setName("role");
        roles.add(role);
        user1.setRoles(roles);
        assertEquals("userRoles", user1.getRoles(), roles);
        authorities1 = user1.getAuthorities();
        assertNotNull("userNotNullAuthorities", authorities1);
        assertTrue("userAuthorityLength", authorities1.length > 0);

        assertNull("userNullSites", user1.getSites());
        site.setName("Test Site");
        sites.add(site);
        user1.setSites(sites);
        assertEquals("userSites", user1.getSites(), sites);

        assertNull("userNullProperty", user1.getUserProperties());
        userProperty.setName("Test Property");
        userProperty.setValue("Test Value");
        userProperties.add(userProperty);
        user1.setUserProperties(userProperties);
        assertEquals("userProperty", user1.getUserProperties(), userProperties);

        user1.setVersion(Integer.MAX_VALUE);
        assertTrue("userVersion", user1.getVersion() == Integer.MAX_VALUE);
        user1.setVersion(Integer.MIN_VALUE);
        assertTrue("userVersion", user1.getVersion() == Integer.MIN_VALUE);
        user1.setVersion(zero);
        assertEquals("userVersion", user1.getVersion(), zero);
    }
}
