/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;


/**
 *
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, SECURITY, MODEL })
public class FeatureTest {

    /**
     * Test method for 'com.vocollect.epp.model.Feature.hashCode()'.
     */
    public void testHashCode() {
        Feature f1 = new Feature();
        f1.setName("group");
        f1.setDescription("description");
        Feature f2 = new Feature();
        f2.setName("group");
        f2.setDescription("description");
        assertEquals("hashCode", f1.hashCode(), f2.hashCode());
        assertTrue("hashCodeEquals", f1.equals(f2));
        f2.setName("group2");
        assertFalse("hashCodeDiff", f1.hashCode() == f2.hashCode());
    }

    /**
     * Test method for 'com.vocollect.epp.model.Feature.toString()'.
     */
    public void testToString() {
        Feature f1 = new Feature();
        f1.setName("feature");
        assertTrue("toString", f1.toString().contains("name=feature"));
    }

    /**
     * Test method for 'com.vocollect.epp.model.Feature.equals(Object)'.
     */
    public void testEqualsObject() {
        Feature f1 = new Feature();
        Feature f2 = new Feature();
        FeatureGroup fg1 = new FeatureGroup();

        fg1.setName("group");
        fg1.setDescription("description");
        f1.setName("feature");
        f1.setDescription("description");
        f2.setName("feature");
        f2.setDescription("description");
        assertTrue("equals", f1.equals(f2));
        f2.setDescription("description2");
        assertTrue("equalDiffDescription", f1.equals(f2));
        f2.setName("feature2");
        assertFalse("equalDiffNames", f1.equals(f2));
        assertFalse("equalDiffObjects", f1.equals(fg1));
        assertFalse("equalNullObjects", f1.equals(null));
    }

    /**
     * Test for all get/set methods in 'com.vocollect.epp.model.FeatureGroup'.
     */
    public void testGetSetMethods() {
        Feature f1 = new Feature();
        FeatureGroup fg1 = new FeatureGroup();

        f1.setName("name");
        f1.setDescription("description");
        f1.setIsReadOnly(true);
        f1.setFeatureGroup(fg1);
        assertTrue("nameEquals", f1.getName() == "name");
        assertTrue("descriptionEquals", f1.getDescription() == "description");
        assertTrue("isReadOnlyEquals", f1.getIsReadOnly());
        assertFalse("isReadOnlyEquals", !f1.getIsReadOnly());
        f1.setIsReadOnly(false);
        assertTrue("isReadOnlyEquals", !f1.getIsReadOnly());
        assertFalse("isReadOnlyEquals", f1.getIsReadOnly());
        assertTrue("featureGroupEquals", f1.getFeatureGroup() == fg1);
    }
}
