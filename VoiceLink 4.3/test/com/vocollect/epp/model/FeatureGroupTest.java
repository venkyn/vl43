/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;


/**
 * Unit test class for FeatureGroup class.
 *
 * @author Dennis Doubleday
 */
@Test(groups = { FAST, SECURITY, MODEL })
public class FeatureGroupTest {

    /**
     * Test method for 'com.vocollect.epp.model.FeatureGroup.hashCode()'.
     */
    public void testHashCode() {
        FeatureGroup fg1 = new FeatureGroup();
        fg1.setName("group");
        fg1.setDescription("description");
        FeatureGroup fg2 = new FeatureGroup();
        fg2.setName("group");
        fg2.setDescription("description");
        assertEquals("hashCode", fg1.hashCode(), fg2.hashCode());
        assertTrue("hashCodeEquals", fg1.equals(fg2));
        fg2.setName("group2");
        assertFalse("hashCodeDiff", fg1.hashCode() == fg2.hashCode());
    }

    /**
     * Test method for 'com.vocollect.epp.model.FeatureGroup.toString()'.
     */
    public void testToString() {
        FeatureGroup fg1 = new FeatureGroup();
        fg1.setName("group");
        assertTrue("toString", fg1.toString().contains("name=group"));
    }

    /**
     * Test method for 'com.vocollect.epp.model.FeatureGroup.equals(Object)'.
     */
    public void testEqualsObject() {
        FeatureGroup fg1 = new FeatureGroup();
        User user = new User();
        fg1.setName("group");
        fg1.setDescription("description");
        FeatureGroup fg2 = new FeatureGroup();
        fg2.setName("group");
        fg2.setDescription("description");
        assertTrue("equals", fg1.equals(fg2));
        fg2.setDescription("description2");
        assertTrue("equalDiffDescription", fg1.equals(fg2));
        fg2.setName("group2");
        assertFalse("equalDiffNames", fg1.equals(fg2));
        assertFalse("equalDiffObjects", fg1.equals(user));
        assertFalse("equalNullObject", fg1.equals(null));
    }

    /**
     * Test for all get/set methods in 'com.vocollect.epp.model.FeatureGroup'.
     */
    public void testGetSetMethods() {
        FeatureGroup fg1 = new FeatureGroup();
        Set<Feature> features = new HashSet<Feature>();
        Feature feature1 = new Feature();

        feature1.setDescription("description");
        features.add(feature1);

        fg1.setName("name");
        fg1.setDescription("description");
        fg1.setFeatures(features);

        assertTrue("featureEquals", fg1.getFeatures() == features);
        assertTrue("nameEquals", fg1.getName() == "name");
        assertTrue("descriptionEquals", fg1.getDescription() == "description");
        assertFalse("nameNotEquals", fg1.getName() == "name2");
        assertFalse("descriptionNotEquals", fg1.getDescription() == "description2");
    }
}
