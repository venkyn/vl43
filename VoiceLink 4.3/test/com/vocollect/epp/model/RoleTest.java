/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;



/**
 *
 *
 * @author sshaulis
 */
@Test(groups = { FAST, SECURITY, MODEL })
public class RoleTest {

    /**
     * Test method for 'com.vocollect.epp.model.Role.hashCode()'.
     */
    public void testHashCode() {
        Role role1 = new Role();
        role1.setName("role");
        role1.setDescription("description");
        Role role2 = new Role();
        role2.setName("role");
        role2.setDescription("description");
        assertEquals("hashCode", role1.hashCode(), role2.hashCode());
        assertTrue("hashCodeEquals", role1.equals(role2));
        role2.setName("role2");
        assertFalse("hashCodeDiff", role1.hashCode() == role2.hashCode());
    }

    /**
     * Test method for 'com.vocollect.epp.model.Role.toString()'.
     */
    public void testToString() {
        Role role1 = new Role();
        role1.setName("role");
        assertTrue("toString", role1.toString().contains("name=role"));
    }

    /**
     * Test method for 'com.vocollect.epp.model.Role.equals(Object)'.
     */
    public void testEqualsObject() {
        Role role1 = new Role();
        User user = new User();
        role1.setName("role");
        role1.setDescription("description");
        Role role2 = new Role();
        role2.setName("role");
        role2.setDescription("description");
        assertTrue("equals", role1.equals(role2));
        role2.setDescription("description2");
        assertTrue("equalDiffDescription", role1.equals(role2));
        role2.setName("role2");
        assertFalse("equalDiffNames", role1.equals(role2));
        assertFalse("equalDiffObjects", role1.equals(user));
        assertTrue("equalSameObject", role1.equals(role1));
        assertFalse("equalNullObject", role1.equals(null));
    }

    /**
     * Test get/set methods for 'com.vocollect.epp.model.Role'.
     */
    public void testGetSetMethods() {
        final Integer zero = 0;

        Role role = new Role();
        role.setName("role");
        role.setDescription("description");
        role.setIsAdministrative(true);

        assertEquals("roleName", role.getName(), "role");
        assertEquals("roleDescription", role.getDescription(), "description");
        assertEquals("roleAuthority", role.getAuthority(), "ROLE_role");
        assertTrue("roleIsAdministrative", role.getIsAdministrative());
        role.setIsAdministrative(false);
        assertFalse("roleIsAdministrative", role.getIsAdministrative());

        role.setVersion(Integer.MAX_VALUE);
        assertTrue("roleVersionMax", role.getVersion() == Integer.MAX_VALUE);
        role.setVersion(Integer.MIN_VALUE);
        assertTrue("roleVersionMin", role.getVersion() == Integer.MIN_VALUE);
        role.setVersion(zero);
        assertEquals("roleVersionZero", role.getVersion(), zero);

        assertEquals("roleUserCount", role.getUserCount(), 0);

        Set<Feature> features = new HashSet<Feature>();
        Feature feature = new Feature();
        features.add(feature);
        assertNull("roleFeature", role.getFeatures());
        role.addFeature(feature);
        assertNotNull("roleFeature", role.getFeatures());
        role.setFeatures(null);
        assertNull("roleFeature", role.getFeatures());
        role.setFeatures(features);
        assertTrue("roleFeature", role.getFeatures() == features);
    }

}
