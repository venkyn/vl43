/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.email;

import com.vocollect.epp.dao.BaseDAOTestCase;
import com.vocollect.epp.service.NotificationManager;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;

import java.io.File;
import java.io.StringWriter;
import java.util.Map;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.testng.annotations.Test;

/**
 *
 *
 * @author ??
 */
public class EmailNotificationTest extends BaseDAOTestCase {

    private NotificationManager notificationManager;

    private JavaMailSenderImpl javaMailSender;

    /* @BeforeClass
    protected void classSetUp() {
        try {
            DbUnitAdapter adapter = new DbUnitAdapter();
            adapter.handleFlatXmlResource("data/dbunit/notification-test.xml");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    } */

    /**
     * @param notificationManager .
     */
    @Test(enabled = false)
    public void setNotificationManager(NotificationManager notificationManager) {
        this.notificationManager = notificationManager;
    }

    /**
     * @throws Exception
     */
    @Test(groups = { "dev" })
    public void testSendEmail() throws Exception {
        // notificationManager.sendEmailNotification(new Long(-1));
    }

    /* @Test(groups = { "dev" })
    public void sendEmail() {

        String msg = "";
        SimpleMailMessage mailMsg = null;

        // Set up email configuration
        javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setPort(25);
        javaMailSender.setHost("ex01.vocollect.int");
        javaMailSender.setUsername("");
        javaMailSender.setPassword("");

        try {
            Notification n = new Notification();
            n.setPriority("Critical");
            n.setAcknowledgedDateTime(null);
            Iterator notificationListIterator = notificationManager
                .queryByExample(n).iterator();

            Map<String, Object> nMap = new HashMap<String, Object>();

            Properties properties = new Properties();
            ClassLoader loader = ClassLoader.getSystemClassLoader();
            properties.load(loader
                .getResourceAsStream("com/vocollect/epp/package.properties"));

            while (notificationListIterator.hasNext()) {
                Notification notification = (Notification) notificationListIterator
                    .next();

                nMap.put("Application", notification.getApplication());
                nMap.put("Process", notification.getProcess());
                nMap.put("Priority", notification.getPriority());
                nMap.put("Massage", notification.getMessage());
                nMap.put("Date", notification.getCreationDateTime().toString());

                Set<NotificationDetail> nSet = notification.getDetails();
                if (nSet.size() > 0) {
                    nMap.put("recordId", notification.getId().toString());

                    Iterator notificationDetailListIterator = nSet.iterator();

                    JSONObject json = null;
                    JSONArray jsonArray = new JSONArray();

                    while (notificationDetailListIterator.hasNext()) {
                        NotificationDetail notificationDetail = (NotificationDetail) notificationDetailListIterator
                            .next();
                        json = new JSONObject();
                        json.put(properties.getProperty(notificationDetail
                            .getKey()), " " + notificationDetail.getValue());
                        jsonArray.put(Integer.parseInt(notificationDetail
                            .getOrdering()), json);
                    }

                    String str = jsonArray.toString();
                    nMap.put("nDstr", str.substring(1, str.length() - 1));
                } else {
                    nMap.put("nDstr", "");
                }

                msg = processFreeMarker(nMap);
                System.out.println(msg);

                mailMsg = new SimpleMailMessage();
                mailMsg.setTo("daich@vocollect.com");
                mailMsg.setSubject("Critical Notification.");
                mailMsg.setText(msg);
                // javaMailSender.send(mailMsg);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    } */

    /**
     * @param root .
     * @return .
     * @throws Exception
     */
    private String processFreeMarker(Map root) throws Exception {
        Configuration cfg = new Configuration();
        cfg
            .setDirectoryForTemplateLoading(new File(
                "C:/Perforce1666_IT-2066_daich/enterpriseProducts/epp/dev/notifications/main/src/page/notification"));
        cfg.setObjectWrapper(new DefaultObjectWrapper());
        Template temp = cfg.getTemplate("emailNotification.ftl");
        StringWriter swtr = new StringWriter();
        temp.process(root, swtr);
        return swtr.toString();
    }

}
