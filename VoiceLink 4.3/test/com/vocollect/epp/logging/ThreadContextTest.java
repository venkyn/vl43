/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.logging;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Class to allow us to access protected methods of the ThreadContext.
 *
 *
 * @author dgold
 */
class MyThreadContext extends ThreadContext {

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.logging.ThreadContext#finalize()
     */
    public void finalize() throws Throwable {
        super.finalize();
    }

}

/**
 *
 *
 * @author dgold
 */
public class ThreadContextTest {

    private String clientIP         = "123.456.789.000";

    private String deviceID         = "tid: x32";

    private String proxyIP          = "255.192.087.001";

    private String purpose          = "test";

    private String requestedService = "web service";

    private long   threadID         = 1052L;

    /**
     * Test method for 'com.vocollect.epp.logging.ThreadContext.finalize()'.
     */
    @Test()
    public void testFinalize() throws Throwable {
        // I expect this to fail.
        MyThreadContext tc = new MyThreadContext();
        try {
            tc.finalize();
        } catch (Throwable e) {
            // This is expected.
        }
        fillOutFields(tc);
        tc.setContext();
        // An exception here would be bad
        tc.finalize();
        // test the try/catch block
        tc.finalize();
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ThreadContext.getClientIP()'.
     * This will test setClientIP as well.
     */
    @Test()
    public void testGetClientIP() {
        ThreadContext tc = new ThreadContext();
        tc.setClientIP(this.clientIP);
        assertTrue(0 == tc.getClientIP().compareTo(this.clientIP));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ThreadContext.getProxyIP()'
     * This will test setProxyIP as well.
     */
    @Test()
    public void testGetProxyIP() {
        ThreadContext tc = new ThreadContext();
        tc.setProxyIP(this.proxyIP);
        assertTrue(0 == tc.getProxyIP().compareTo(this.proxyIP));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ThreadContext.getPurpose()'
     * This will test getPurpose as well.
     */
    @Test()
    public void testGetPurpose() {
        ThreadContext tc = new ThreadContext();
        tc.setPurpose(this.purpose);
        assertTrue(0 == tc.getPurpose().compareTo(this.purpose));
    }

    /**
     * Test method for
     * 'com.vocollect.epp.logging.ThreadContext.getRequestedService()' this
     * tests setRequestedService as well.
     */
    @Test()
    public void testGetRequestedService() {
        ThreadContext tc = new ThreadContext();
        tc.setRequestedService(this.requestedService);
        assertTrue(0 == tc.getRequestedService().compareTo(
            this.requestedService));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ThreadContext.getDeviceID()'
     * Tests setDeviceID as well.
     */
    @Test()
    public void testGetDeviceID() {
        ThreadContext tc = new ThreadContext();
        tc.setDeviceID(this.deviceID);
        assertTrue(0 == tc.getDeviceID().compareTo(this.deviceID));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.ThreadContext.getThreadID()'
     * tests setThreadID as well.
     */
    @Test()
    public void testGetThreadID() {
        ThreadContext tc = new ThreadContext();
        tc.setThreadID(this.threadID);
        assertTrue(tc.getThreadID() == this.threadID);
    }

    /**
     * @param tc .
     */
    private void fillOutFields(ThreadContext tc) {
        tc.setClientIP(this.clientIP);
        tc.setDeviceID(this.deviceID);
        tc.setProxyIP(this.proxyIP);
        tc.setPurpose(this.purpose);
        tc.setRequestedService(this.requestedService);
        tc.setThreadID(this.threadID);
    }

}
