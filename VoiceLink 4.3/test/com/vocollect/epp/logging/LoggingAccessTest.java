/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.logging;

import static org.testng.AssertJUnit.assertNotNull;

/**
 *
 *
 * @author dgold
 */
public class LoggingAccessTest {

    /**
     * Test method for
     * 'com.vocollect.epp.errors.LoggingAccess.LoggingAccess(long)'
     */
    public void testLoggingAccess() {
        assertNotNull(LoggingError.NO_ERROR);
    }

}
