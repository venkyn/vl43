/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.logging;

import com.vocollect.epp.errors.ErrorCode;

import java.util.Enumeration;
import java.util.ResourceBundle;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.spi.LoggingEvent;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertNotNull;
import static org.testng.AssertJUnit.assertNull;
import static org.testng.AssertJUnit.assertTrue;


/**
 * Tests for the Logger.
 *
 * @author dgold
 */
@SuppressWarnings("deprecation")
public class LoggerTest {

    // Deprecation warnings are suppressed because we are purposely
    // testing deprecated methods here.

    /**
     * The logger to log messages to.
     */
    private com.vocollect.epp.logging.Logger theLog = null;

    /**
     * The lower error range boundary.
     */
    static final long LOWER_ERR_RANGE = 1234567890;

    /**
     * The upper error range boundary.
     */
    static final long UPPER_ERR_RANGE = 1234567899;

    /**
     *
     *
     *
     * @author dgold
     */

    class Pod {

        private String name  = "test name";

        private long   count = 0;

        /**
         * {@inheritDoc}
         * @see java.lang.Object#toString()
         */
        public String toString() {
            return this.name + ": " + this.count;
        }
    }

    private Pod podInst = new Pod();

    /**
     * Fake exception class.
     */
    class FonyException extends Exception {

        private static final long serialVersionUID = 1L;

        /**
         * Constructor.
         */
        FonyException() {
            super();
        }

        /**
         * Constructor.
         * @param reason .
         */
        FonyException(String reason) {
            super(reason);
        }
    }

    private FonyException fake = null;

    /**
     * Model error class, that does everything right and against which
     * the tests are run.
     * @author dgold
     *
     */
    static final class ExampleError extends ErrorCode {

        public static final ExampleError NO_ERROR     = new ExampleError();

        public static final ExampleError SECOND_ERROR = new ExampleError(
                                                         1234567892);

        public static final ExampleError FOURTH_ERROR = new ExampleError(
                                                         1234567894);

        public static final ExampleError FIFTH_ERROR  = new ExampleError(
                                                         1234567895);

        @Deprecated
        public static final ExampleError THIRD_ERROR  = new ExampleError(
                                                         1234567893);

        @Deprecated
        public static final ExampleError SIXTH_ERROR  = new ExampleError(
                                                         1234567896);

        /**
         * Constructor.
         */
        private ExampleError() {
            super("Ex", LOWER_ERR_RANGE, UPPER_ERR_RANGE);
        }

        /**
         * Constructor.
         * @param err - new resulting error code
         */
        private ExampleError(long err) {
            super(ExampleError.NO_ERROR, err);
        }
    }

    //  BufferedWriter out = null;
    //  BufferedReader inBuf = null;

    /**
     * Constant for FATAL Error Level.
     */
    static final String FATAL = "FATAL";

    /**
     * Constant for ERROR Error Level.
     */
    static final String ERROR = "ERROR";

    /**
     * Constant for WARN Error Level.
     */
    static final String WARN  = "WARN";

    /**
     * Constant for INFO Error Level.
     */
    static final String INFO  = "INFO";

    /**
     * Constant for DEBUG Error Level.
     */
    static final String DEBUG = "DEBUG";

    /**
     * Constant for TRACE Error Level.
     */
    static final String TRACE = "TRACE";

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.removeAllAppenders()'.
     * @throws Exception - occurs during unknown exception
     */
    @BeforeMethod
    protected void setUp() throws Exception {

        this.theLog = new com.vocollect.epp.logging.Logger(LoggerTest.class
            .getName());

        //      BasicConfigurator.configure();
        //        PipedOutputStream temp = new PipedOutputStream();
        //      this.out = new BufferedWriter(new OutputStreamWriter(temp));
        //      this.inBuf = new BufferedReader(new InputStreamReader(
        //      new PipedInputStream(temp)));

        //Layout textForm = new SimpleLayout();

        this.theLog.removeAllAppenders();

        // This writes the named file, containing entries that have "level
        //- message"

        //      this.theLog.addAppender(new WriterAppender(textForm, this.out));
        fake = new FonyException("no reason");

    }

    /**
     * Deconfiguration for the end of all tests,.
     * @throws Exception - occurs during unknown exception
     */
    @AfterMethod
    protected void tearDown() throws Exception {
        //      this.in.close();
        //      this.out.close();
        //      File temp = new File(LoggerTest.class.getName() + ".test.log");
        //      temp.delete();
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.hashCode()'.
     */
    @Test()
    public void testHashCode() {
        assertTrue(0 != this.theLog.hashCode());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.Logger(String)'.
     */
    @Test()
    public void testLoggerString() {
        com.vocollect.epp.logging.Logger theLocalLog = new com.vocollect.epp.logging.Logger(
            LoggerTest.class.getName());
        assertNotNull(theLocalLog);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.Logger(Class)'.
     */
    @Test()
    public void testLoggerClass() {
        com.vocollect.epp.logging.Logger theLocalLog = new com.vocollect.epp.logging.Logger(
            LoggerTest.class);
        assertNotNull(theLocalLog);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.error(
     * Object, Throwable)'.
     */
    @Test()
    public void testErrorObjectThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.error(this.podInst, fake);
        this.theLog.setLevel(Level.ERROR);
        this.theLog.error(this.podInst, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.fatal(Object)'.
     */
    @Test()
    public void testErrorObject() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.error(this.podInst);
        this.theLog.setLevel(Level.ERROR);
        this.theLog.error(this.podInst);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.fatal(Object)'.
     */
    @Test()
    public void testErrorString() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.error(this.podInst.toString());
        this.theLog.setLevel(Level.ERROR);
        this.theLog.error(this.toString());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.error(
     * Object, ErrorCode)'.
     */
    @Test()
    public void testErrorObjectErrorCode() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.error(this.podInst, ExampleError.SECOND_ERROR);
        this.theLog.setLevel(Level.ERROR);
        this.theLog.error(this.podInst, ExampleError.SECOND_ERROR);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.error(
     * String, ErrorCode)'.
     */
    @Test()
    public void testErrorStringErrorCode() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.error(this.podInst.toString(), ExampleError.SECOND_ERROR);
        this.theLog.setLevel(Level.ERROR);
        this.theLog.error(this.podInst.toString(), ExampleError.SECOND_ERROR);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.error(
     * Object, ErrorCode, Throwable)'.
     */
    @Test()
    public void testErrorObjectErrorCodeThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.error(this.podInst, ExampleError.SECOND_ERROR, fake);
        this.theLog.setLevel(Level.ERROR);
        this.theLog.error(this.podInst, ExampleError.SECOND_ERROR, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.error(
     * String, ErrorCode, Throwable)'.
     */
    @Test()
    public void testErrorStringErrorCodeThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.error(
            this.podInst.toString(), ExampleError.SECOND_ERROR, fake);
        this.theLog.setLevel(Level.ERROR);
        this.theLog.error(
            this.podInst.toString(), ExampleError.SECOND_ERROR, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.fatal(
     * Object, Throwable)'.
     */
    @Test()
    public void testFatalObjectThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.fatal(this.podInst, fake);
        this.theLog.setLevel(Level.FATAL);
        this.theLog.fatal(this.podInst, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.fatal(Object)'.
     */
    @Test()
    public void testFatalObject() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.fatal(this.podInst);
        this.theLog.setLevel(Level.FATAL);
        this.theLog.fatal(this.podInst);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.fatal(
     * Object, ErrorCode)'.
     */
    @Test()
    public void testFatalObjectErrorCode() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.fatal(this.podInst, ExampleError.SECOND_ERROR);
        this.theLog.setLevel(Level.FATAL);
        this.theLog.fatal(this.podInst, ExampleError.SECOND_ERROR);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.fatal(
     * String, ErrorCode)'.
     */
    @Test()
    public void testFatalStringErrorCode() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.fatal(this.podInst.toString(), ExampleError.SECOND_ERROR);
        this.theLog.setLevel(Level.FATAL);
        this.theLog.fatal(this.podInst.toString(), ExampleError.SECOND_ERROR);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.fatal(
     * Object, ErrorCode, Throwable)'.
     */
    @Test()
    public void testFatalObjectErrorCodeThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.fatal(this.podInst, ExampleError.SECOND_ERROR, fake);
        this.theLog.setLevel(Level.FATAL);
        this.theLog.fatal(this.podInst, ExampleError.SECOND_ERROR, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.fatal(
     * String, ErrorCode, Throwable)'.
     */
    @Test()
    public void testFatalStringErrorCodeThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.fatal(
            this.podInst.toString(), ExampleError.SECOND_ERROR, fake);
        this.theLog.setLevel(Level.FATAL);
        this.theLog.fatal(
            this.podInst.toString(), ExampleError.SECOND_ERROR, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.debug(
     * Object, Throwable)'.
     */
    @Test()
    public void testDebugObjectThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.debug(this.podInst, fake);
        this.theLog.setLevel(Level.DEBUG);
        this.theLog.debug(this.podInst, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.debug(Object)'.
     */
    @Test()
    public void testDebugObject() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.debug(this.podInst);
        this.theLog.setLevel(Level.DEBUG);
        this.theLog.debug(this.podInst);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.debug(
     * Object, ErrorCode)'.
     */
    @Test()
    public void testDebugObjectErrorCode() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.debug(this.podInst, ExampleError.SECOND_ERROR);
        this.theLog.setLevel(Level.DEBUG);
        this.theLog.debug(this.podInst, ExampleError.SECOND_ERROR);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.debug(
     * String, ErrorCode)'.
     */
    @Test()
    public void testDebugStringErrorCode() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.debug(this.podInst.toString(), ExampleError.SECOND_ERROR);
        this.theLog.setLevel(Level.DEBUG);
        this.theLog.debug(this.podInst.toString(), ExampleError.SECOND_ERROR);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.debug(
     * Object, ErrorCode, Throwable)'.
     */
    @Test()
    public void testDebugObjectErrorCodeThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.debug(this.podInst, ExampleError.SECOND_ERROR, fake);
        this.theLog.setLevel(Level.DEBUG);
        this.theLog.debug(this.podInst, ExampleError.SECOND_ERROR, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.debug(
     * String, ErrorCode, Throwable)'.
     */
    @Test()
    public void testDebugStringErrorCodeThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.debug(
            this.podInst.toString(), ExampleError.SECOND_ERROR, fake);
        this.theLog.setLevel(Level.DEBUG);
        this.theLog.debug(
            this.podInst.toString(), ExampleError.SECOND_ERROR, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.info(
     * Object, Throwable)'.
     */
    @Test()
    public void testInfoObjectThrowable() {
        this.theLog.setLevel(Level.OFF);

        this.theLog.info(this.podInst, fake);
        this.theLog.setLevel(Level.INFO);
        this.theLog.info(this.podInst, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.info(Object)'.
     */
    @Test()
    public void testInfoObject() {
        this.theLog.setLevel(Level.OFF);

        this.theLog.info(this.podInst);
        this.theLog.setLevel(Level.INFO);
        this.theLog.info(this.podInst);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.info(
     * Object, ErrorCode)'.
     */
    @Test()
    public void testInfoObjectErrorCode() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.info(this.podInst, ExampleError.SECOND_ERROR);
        this.theLog.setLevel(Level.INFO);
        this.theLog.info(this.podInst, ExampleError.SECOND_ERROR);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.info(
     * String, ErrorCode)'.
     */
    @Test()
    public void testInfoStringErrorCode() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.info(this.podInst.toString(), ExampleError.SECOND_ERROR);
        this.theLog.setLevel(Level.INFO);
        this.theLog.info(this.podInst.toString(), ExampleError.SECOND_ERROR);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.info(
     * Object, ErrorCode, Throwable)'.
     */
    @Test()
    public void testInfoObjectErrorCodeThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.info(this.podInst, ExampleError.SECOND_ERROR, fake);
        this.theLog.setLevel(Level.INFO);
        this.theLog.info(this.podInst, ExampleError.SECOND_ERROR, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.info(
     * String, ErrorCode, Throwable)'.
     */
    @Test()
    public void testInfoStringErrorCodeThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.info(
            this.podInst.toString(), ExampleError.SECOND_ERROR, fake);
        this.theLog.setLevel(Level.INFO);
        this.theLog.info(
            this.podInst.toString(), ExampleError.SECOND_ERROR, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.trace(
     * Object, Throwable)'.
     */
    @Test()
    public void testTraceObjectThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.trace(this.podInst, fake);
        this.theLog.setLevel(Level.INFO);
        this.theLog.trace(this.podInst, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.trace(Object)'.
     */
    @Test()
    public void testTraceObject() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.trace(this.podInst);
        this.theLog.setLevel(Level.INFO);
        this.theLog.trace(this.podInst);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.trace(
     * Object, ErrorCode)'.
     */
    @Test()
    public void testTraceObjectErrorCode() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.trace(this.podInst, ExampleError.SECOND_ERROR);
        this.theLog.setLevel(Level.TRACE);
        this.theLog.trace(this.podInst, ExampleError.SECOND_ERROR);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.trace(
     * String, ErrorCode)'.
     */
    @Test()
    public void testTraceStringErrorCode() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.trace(this.podInst.toString(), ExampleError.SECOND_ERROR);
        this.theLog.setLevel(Level.TRACE);
        this.theLog.trace(this.podInst.toString(), ExampleError.SECOND_ERROR);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.trace(
     * Object, ErrorCode, Throwable)'.
     */
    @Test()
    public void testTraceObjectErrorCodeThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.trace(this.podInst, ExampleError.SECOND_ERROR, fake);
        this.theLog.setLevel(Level.TRACE);
        this.theLog.trace(this.podInst, ExampleError.SECOND_ERROR, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.trace(
     * String, ErrorCode, Throwable)'.
     */
    @Test()
    public void testTraceStringErrorCodeThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.trace(
            this.podInst.toString(), ExampleError.SECOND_ERROR, fake);
        this.theLog.setLevel(Level.TRACE);
        this.theLog.trace(
            this.podInst.toString(), ExampleError.SECOND_ERROR, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.warn(
     * Object, Throwable)'.
     */
    @Test()
    public void testWarnObjectThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.warn(this.podInst, fake);
        this.theLog.setLevel(Level.WARN);
        this.theLog.warn(this.podInst, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.warn(Object)'.
     */
    @Test()
    public void testWarnObject() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.warn(this.podInst);
        this.theLog.setLevel(Level.WARN);
        this.theLog.warn(this.podInst);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.warn(
     * Object, ErrorCode)'.
     */
    @Test()
    public void testWarnObjectErrorCode() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.warn(this.podInst, ExampleError.SECOND_ERROR);
        this.theLog.setLevel(Level.WARN);
        this.theLog.warn(this.podInst, ExampleError.SECOND_ERROR);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.warn(
     * String, ErrorCode)'.
     */
    @Test()
    public void testWarnStringErrorCode() {
        this.theLog.setLevel(Level.OFF);
        this.theLog.warn(this.podInst.toString(), ExampleError.SECOND_ERROR);
        this.theLog.setLevel(Level.WARN);
        this.theLog.warn(this.podInst.toString(), ExampleError.SECOND_ERROR);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.warn(
     * Object, ErrorCode, Throwable)'.
     */
    @Test()
    public void testWarnObjectErrorCodeThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.warn(this.podInst, ExampleError.SECOND_ERROR, fake);
        this.theLog.setLevel(Level.WARN);
        this.theLog.warn(this.podInst, ExampleError.SECOND_ERROR, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.warn(
     * String, ErrorCode, Throwable)'.
     */
    @Test()
    public void testWarnStringErrorCodeThrowable() {

        this.theLog.setLevel(Level.OFF);
        this.theLog.warn(
            this.podInst.toString(), ExampleError.SECOND_ERROR, fake);
        this.theLog.setLevel(Level.WARN);
        this.theLog.warn(
            this.podInst.toString(), ExampleError.SECOND_ERROR, fake);
    }

    /**
     *Test method for 'com.vocollect.epp.logging.Logger.addAppender(Appender)'.
     * Test method for 'com.vocollect.epp.logging.Logger.getAppender(String)'
     * Test method for 'com.vocollect.epp.logging.Logger.isAttached(Appender)'
     * Test method for 'com.vocollect.epp.logging.Logger.getAllAppenders()'
     */
    @Test()
    public void testAddAppender() {
        RollingFileAppender addedAppender = new RollingFileAppender();
        addedAppender.setName("added");

        this.theLog.addAppender(addedAppender);
        Enumeration<?> allAppenders = this.theLog.getAllAppenders();
        assertTrue(allAppenders.hasMoreElements());

        assertNotNull(this.theLog.getAppender(addedAppender.getName()));
        assertTrue(this.theLog.isAttached(addedAppender));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.assertLog(
     * boolean, String)'.
     */
    @Test()
    public void testAssertLog() {
        this.theLog.assertLog(false, "log this");
        this.theLog.assertLog(true, "don't log this");
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.callAppenders(
     * LoggingEvent)'.
     */
    @Test()
    public void testCallAppenders() {

        LoggingEvent l = new LoggingEvent(
            "com.vocollect.epp.logging.Logger", Logger.getLogger(this
                .getClass()), Level.WARN, this.podInst, fake);
        this.theLog.callAppenders(l);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.equals(Object)'.
     */
    @Test()
    public void testEqualsObject() {
        assertTrue(this.theLog.equals(this.theLog));
        com.vocollect.epp.logging.Logger theLocalLog = new com.vocollect.epp.logging.Logger(
            LoggerTest.class.getName());
        assertFalse(this.theLog.equals(theLocalLog));

    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.getAdditivity()'.
     */
    @Test()
    public void testGetAdditivity() {
        assertTrue(this.theLog.getAdditivity());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.getEffectiveLevel()'.
     */
    @Test()
    public void testGetEffectiveLevel() {
        this.theLog.setLevel(Level.DEBUG);
        assertTrue(Level.DEBUG == this.theLog.getEffectiveLevel());
        assertFalse(Level.TRACE == this.theLog.getEffectiveLevel());
    }

    /**
     * Test for 'com.vocollect.epp.logging.Logger.getLoggerRepository()'.
     */
    @Test()
    public void testGetLoggerRepository() {
        assertNotNull(this.theLog.getLoggerRepository());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.getResourceBundle()'.
     */
    @Test()
    public void testGetResourceBundle() {
        // This will need to change if we actually set the resource bundle
        assertNull(this.theLog.getResourceBundle());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.isDebugEnabled()'.
     */
    @Test()
    public void testIsDebugEnabled() {
        this.theLog.setLevel(Level.DEBUG);
        assertTrue(this.theLog.isDebugEnabled());
        this.theLog.setLevel(Level.WARN);
        assertFalse(this.theLog.isDebugEnabled());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.isEnabledFor(
     * Priority)'.
     */
    @Test()
    public void testIsEnabledFor() {
        this.theLog.setLevel(Level.WARN);
        assertTrue(this.theLog.isEnabledFor(Level.WARN));
        assertFalse(this.theLog.isEnabledFor(Level.INFO));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.isInfoEnabled()'.
     */
    @Test()
    public void testIsInfoEnabled() {
        this.theLog.setLevel(Level.INFO);
        assertTrue(this.theLog.isInfoEnabled());
        this.theLog.setLevel(Level.WARN);
        assertFalse(this.theLog.isInfoEnabled());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.isTraceEnabled()'.
     */
    @Test()
    public void testIsTraceEnabled() {
        this.theLog.setLevel(Level.TRACE);
        assertTrue(this.theLog.isTraceEnabled());
        this.theLog.setLevel(Level.DEBUG);
        assertFalse(this.theLog.isTraceEnabled());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.l7dlog(Priority,
     * String, Object[], Throwable)'.
     */
    @Test()
    public void testL7dlogPriorityStringObjectArrayThrowable() {

    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.l7dlog(Priority,
     * String, Throwable)'.
     */
    @Test()
    public void testL7dlogPriorityStringThrowable() {

    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.log(Priority, Object,
     * Throwable)'.
     */
    @Test()
    public void testLogPriorityObjectThrowable() {
        this.theLog.log(Level.ERROR, podInst, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.log(Priority,Object)'.
     */
    @Test()
    public void testLogPriorityObject() {
        this.theLog.log(Level.ERROR, podInst);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.log(String, Priority,
     * Object, Throwable)'.
     */
    @Test()
    public void testLogStringPriorityObjectThrowable() {
        this.theLog.log("A string to pass through", Level.ERROR, podInst, fake);
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.removeAppender(
     * Appender)'.
     */
    @Test()
    public void testRemoveAppenderAppender() {
        RollingFileAppender addedAppender = new RollingFileAppender();
        addedAppender.setName("added");

        this.theLog.addAppender(addedAppender);
        this.theLog.removeAppender(addedAppender);
        assertFalse(this.theLog.isAttached(addedAppender));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.removeAppender(
     * String)'.
     */
    @Test()
    public void testRemoveAppenderString() {
        RollingFileAppender addedAppender = new RollingFileAppender();
        addedAppender.setName("added");

        this.theLog.addAppender(addedAppender);
        this.theLog.removeAppender(addedAppender.getName());
        assertFalse(this.theLog.isAttached(addedAppender));
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.setAdditivity(
     * boolean)'.
     */
    @Test()
    public void testSetAdditivity() {
        this.theLog.setAdditivity(false);
        assertFalse(this.theLog.getAdditivity());
        this.theLog.setAdditivity(true);
        assertTrue(this.theLog.getAdditivity());
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.setResourceBundle(
     * ResourceBundle)'.
     */
    @Test()
    public void testSetResourceBundle() {
        ResourceBundle rb = this.theLog.getResourceBundle();

        this.theLog.setResourceBundle(rb);

        if (null == rb) {
            assertNull(this.theLog.getResourceBundle());
        } else {
            assertNotNull(this.theLog.getResourceBundle());
        }
    }

    /**
     * Test method for loggin a null string.
     */
    @Test()
    public void testNullStringMessages() {
        boolean expectException = false;
        String testString = null;

        try {
            this.theLog.error(testString);
        } catch (Throwable e) {
            expectException = true;
        }
        try {
            this.theLog.error(testString, ExampleError.SECOND_ERROR);
        } catch (Throwable e) {
            assertTrue(expectException);
            return;
        }
        assertFalse(expectException);
        return;
    }

    /**
     * Test method for 'com.vocollect.epp.logging.Logger.toString()'.
     */
    @Test()
    public void testToString() {
        assertNotNull(this.theLog.toString());
    }

    /**
     * Checks the format of the error message.
     *
     * @param level .
     * @param error .
     * @param code .
     * @return .
     */
    public boolean checkFormat(String level, String error, ErrorCode code) {
        assertNotNull("level is null", level);
        assertNotNull("error is null", error);
        assertNotNull("errorCode is null is null", code);
        // Should be in the form ERROR - {message}
        // where {message} is "[ERR:" + errorCode.asString() + "][" + message
        //+ "]"
        String errMsg = level + " - " + "[ERR:" + code.toString() + "]["
            + this.podInst.toString() + "]";
        return (0 == error.compareTo(errMsg));
    }

    /**
     * Checks the format of the error message with a throwable.
     * @param level - the severity of the error
     * @param error - a string reperesentation of the error
     * @param code - an object encapsulating the error code
     * @param e - the exception and stacktrace
     * @return a boolean representing whether there is an error
     */
    public boolean checkFormat(String level,
                               String error,
                               ErrorCode code,
                               Throwable e) {
        assertNotNull("level is null", level);
        assertNotNull("error is null", error);
        assertNotNull("errorCode is null is null", code);
        assertNotNull("Exception is null", e);

        // Should be in the form ERROR - {message}
        // where {message} is "[ERR:" + errorCode.asString() + "]["
        //+ message + "]"
        String errMsg = level + " - " + "[ERR:" + code.toString() + "]["
            + this.podInst.toString() + "]";
        //      try {
        //      do {
        //      temp = this.inBuf.readLine();
        //      }
        //      while (null != temp && (0 < temp.length()));
        //
        //      } catch (IOException e1) {
        //      // TODO Auto-generated catch block
        //      assertTrue("Caught an io exception reaching end of file", false);
        //      }
        return (0 == error.compareTo(errMsg));
    }

    /**
     * @return theLog
     */
    public com.vocollect.epp.logging.Logger getTheLog() {
        return theLog;
    }

    /**
     * @param theLog .
     */
    public void setTheLog(com.vocollect.epp.logging.Logger theLog) {
        this.theLog = theLog;
    }
}
