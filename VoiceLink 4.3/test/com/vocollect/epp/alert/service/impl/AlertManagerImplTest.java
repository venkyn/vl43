/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.service.impl;

import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.alert.model.AlertCriteria;
import com.vocollect.epp.alert.model.AlertCriteriaRelation;
import com.vocollect.epp.alert.model.AlertOperandType;
import com.vocollect.epp.alert.model.AlertReNotificationFrequency;
import com.vocollect.epp.alert.model.AlertStatus;
import com.vocollect.epp.alert.service.AlertManager;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.dataaggregator.service.DAInformationManager;
import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.exceptions.FieldValidationException;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.service.impl.BaseServiceTestCase;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.SiteContextHolder;

import static com.vocollect.epp.test.TestGroups.ALERT;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit test class for testing Alert manager.
 * @author smittal
 * 
 */
@Test(groups = { ALERT })
public class AlertManagerImplTest extends BaseServiceTestCase {

    private AlertManager alertManager = null;

    private DAInformation daInformation = null;

    private DAInformationManager daInformationManager = null;

    private SiteManager siteManager = null;

    private List<AlertCriteria> alertCriterias = null;

    /**
     * @throws Exception
     */
    @BeforeClass
    public void classSetup() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        // Reset the database to the default state.
        adapter.resetInstallationData();
        // Replace default users with the 100 user dataset.
        adapter.handleFlatXmlResource("data/dbunit/alert-data.xml");
    }

    /**
     * Set the site context.
     * @param siteId - the site ID
     * @throws Exception
     * 
     */
    protected void setSiteContext(Long siteId) throws Exception {
        this.setUpSiteContextNoDefaultSite(true);
        SiteContextHolder.getSiteContext().setCurrentSite(siteId);
    }

    /**
     * @return the alertManager
     */
    @Test(enabled = false)
    public AlertManager getAlertManager() {
        return alertManager;
    }

    /**
     * @param alertManager - the alert manager
     */
    @Test(enabled = false)
    public void setAlertManager(AlertManager alertManager) {
        this.alertManager = alertManager;
    }

    /**
     * @return the daInformationManager
     */
    @Test(enabled = false)
    public DAInformationManager getDaInformationManager() {
        return daInformationManager;
    }

    /**
     * @param daInformationManager the daInformationManager to set
     */
    @Test(enabled = false)
    public void setDaInformationManager(DAInformationManager daInformationManager) {
        this.daInformationManager = daInformationManager;
    }

    /**
     * @return the siteManager
     */
    @Test(enabled = false)
    public SiteManager getSiteManager() {
        return siteManager;
    }

    /**
     * @param siteManager the siteManager to set
     */
    @Test(enabled = false)
    public void setSiteManager(SiteManager siteManager) {
        this.siteManager = siteManager;
    }

    /**
     * . Tests the getAll method
     */
    @Test()
    public void testGetAllAlerts() {
        try {
            List<Alert> alerts = this.alertManager.getAll();

            // We are not testing how many alerts have been returned just
            // because
            // the data XML file will keep growing and we have to maintain this
            // test case very time we update the data XML
            assert (alerts.size() > 0);
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }

    @Test()
    public void testAlertDelete() throws Exception {
        setSiteContext(-2L);

        Alert alert = null;
        DAInformation da = null;
        AlertReNotificationFrequency freq = new AlertReNotificationFrequency();
        freq.setHours(0);
        freq.setMinutes(0);

        alert = new Alert();
        alert.setAlertName("deleteAlert");
        alert.setAlertNotificationPriority(NotificationPriority.INFORMATION);
        alert.setReNotificationFrequency(freq);
        alert.setEmailAddress("email");
        alert.setAlertStatus(AlertStatus.ENABLED);
        da = getDaInformationManager().getAll().get(0); // operator
        alert.setDaInformation(da);

        List<AlertCriteria> criterias = new ArrayList<AlertCriteria>();
        AlertCriteria criteria = new AlertCriteria();
        criteria.setAlert(alert);
        criteria.setSequence(0);
        criteria.setOperandType(AlertOperandType.EQUALSTO);
        criteria.setThreshold("10");
        criteria.setRelation(AlertCriteriaRelation.WHEN);
        criteria.setDaColumn(da.getColumns().iterator().next());
        criterias.add(criteria);
        alert.setAlertCriterias(criterias);

        try {
            this.alertManager.save(alert);
            assertNotNull(this.alertManager.get(alert.getId()));
            
            this.alertManager.delete(alert.getId());
            assertNull(this.alertManager.get(alert.getId()));
        } catch (BusinessRuleException e) {
            UserMessage message = e.getUserMessage();
            String str = message.getKey();
            assertTrue(str == "alert.delete.error.enabled");
        }
    }

    @Test()
    public void testUniquenessByName() {

        Date lastAlertEventTime = new Date();
        Date lastEvaluationTime = new Date();
        AlertReNotificationFrequency freq = new AlertReNotificationFrequency();
        freq.setHours(0);
        freq.setMinutes(45);

        Alert alert1 = new Alert();
        alert1.setAlertName("alert");
        alert1.setDaInformation(daInformation);
        alert1.setAlertCriterias(alertCriterias);
        alert1.setAlertNotificationPriority(NotificationPriority.INFORMATION);
        alert1.setEmailAddress("email");
        alert1.setAlertStatus(AlertStatus.ENABLED);
        alert1.setReNotificationFrequency(freq);
        alert1.setLastAlertEventTime(lastAlertEventTime);
        alert1.setLastEvaluationTime(lastEvaluationTime);

        try {
            this.alertManager.verifyUniquenessByAlertName(alert1);
        } catch (Exception e) {
            assert (e instanceof FieldValidationException);
            fail(e.getLocalizedMessage(), e);
        }
    }

    @Test()
    public void testSave() throws Exception {
        setSiteContext(-2L);

        Alert alert = null;
        DAInformation da = null;
        AlertReNotificationFrequency freq = new AlertReNotificationFrequency();
        freq.setHours(0);
        freq.setMinutes(0);

        try {
            alert = new Alert();
            alert.setAlertName("alertTest789456");
            alert
                .setAlertNotificationPriority(NotificationPriority.INFORMATION);
            alert.setReNotificationFrequency(freq);
            alert.setEmailAddress("email");
            alert.setAlertStatus(AlertStatus.ENABLED);
            da = getDaInformationManager().getAll().get(0); // operator
            alert.setDaInformation(da);

            List<AlertCriteria> criterias = new ArrayList<AlertCriteria>();
            AlertCriteria criteria = new AlertCriteria();
            criteria.setAlert(alert);
            criteria.setSequence(0);
            criteria.setOperandType(AlertOperandType.EQUALSTO);
            criteria.setThreshold("10");
            criteria.setRelation(AlertCriteriaRelation.WHEN);
            criteria.setDaColumn(da.getColumns().iterator().next());
            criterias.add(criteria);
            alert.setAlertCriterias(criterias);

            getAlertManager().save(alert);

            getAlertManager().getPrimaryDAO().flushSession();
            getAlertManager().getPrimaryDAO().clearSession();

            List<Alert> alerts = getAlertManager().getAll();

            // Check 1 of the alerts in the system should match the above
            // persisted
            for (Alert alert1 : alerts) {
                if (alert1.getAlertName().equals("alertTest789456")) {
                    assertEquals(alert1.getAlertName(), alert.getAlertName());
                    assertEquals(alert1.getAlertCriterias().size(), alert
                        .getAlertCriterias().size());

                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        } finally {
            // Clean
            try {
                getAlertManager().delete(alert);
                getAlertManager().getPrimaryDAO().flushSession();
                getAlertManager().getPrimaryDAO().clearSession();
            } catch (Exception e) {

            }
        }

    }

    @Test()
    public void testExecuteAlerts() throws Exception {
        setSiteContext(-2L);
        try {
            Alert alertDefaultSite = this.alertManager.get(-1L);
            assertNull(alertDefaultSite.getLastAlertEventTime());
            assertNull(alertDefaultSite.getLastEvaluationTime());
            
            this.alertManager.executeAlerts(alertDefaultSite);            

            Alert alertTestSite = this.alertManager.get(-5L);
            assertNull(alertTestSite.getLastAlertEventTime());
            assertNull(alertTestSite.getLastEvaluationTime());
            
            this.alertManager.executeAlerts(alertTestSite);

            Alert disabledAlert = this.alertManager.get(-4L);
            assertNull(disabledAlert.getLastAlertEventTime());
            assertNull(disabledAlert.getLastEvaluationTime());

            this.alertManager.executeAlerts(disabledAlert);

            assertNotNull(alertDefaultSite.getLastAlertEventTime());
            assertNotNull(alertDefaultSite.getLastEvaluationTime());

            assertNotNull(alertTestSite.getLastAlertEventTime());
            assertNotNull(alertTestSite.getLastEvaluationTime());

            assertNotNull(disabledAlert.getLastAlertEventTime());
            assertNotNull(disabledAlert.getLastEvaluationTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
