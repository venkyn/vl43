/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.service.impl;

import com.vocollect.epp.alert.model.AlertCriteria;
import com.vocollect.epp.alert.service.AlertCriteriaManager;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.service.impl.BaseDataProviderImplTestCase;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.ALERTCRITERIA;

import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit Tests for AlertCriteriaManagerImpl methods.
 * 
 * @author smittal
 */
@Test(groups = { ALERTCRITERIA })
public class AlertCriteriaManagerImplTest extends BaseDataProviderImplTestCase {

    private AlertCriteriaManager alertCriteriaManager = null;

    /**
     * @throws Exception
     */
    @BeforeClass
    public void classSetup() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        // Reset the database to the default state.
        adapter.resetInstallationData();
        // Replace default users with the 100 user dataset.
        adapter.handleFlatXmlResource("data/dbunit/alert-data.xml");

    }

    /**
     * @return the alertCriteriaManager
     */
    @Test(enabled = false)
    public AlertCriteriaManager getAlertCriteriaManager() {
        return alertCriteriaManager;
    }

    /**
     * @param alertCriteriaManager - the alert manager
     */
    @Test(enabled = false)
    public void setAlertCriteriaManager(AlertCriteriaManager alertCriteriaManager) {
        this.alertCriteriaManager = alertCriteriaManager;
    }

    /**
     * . Tests the getAll method
     */
    @Test()
    public void testGetAllAlertCriterias() {
        try {
            List<AlertCriteria> alertCriterias = this.alertCriteriaManager
                .getAll();

            // We are not testing how many alertCriterias have been returned
            // just
            // because
            // the data XML file will keep growing and we have to maintain this
            // test case very time we update the data XML
            assert (alertCriterias.size() > 0);
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }
}
