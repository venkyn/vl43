/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.model;

import com.vocollect.epp.dataaggregator.model.DAColumn;

import static com.vocollect.epp.test.TestGroups.ALERTCRITERIA;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * 
 * 
 * @author smittal
 */
@Test(groups = { FAST, MODEL, ALERTCRITERIA })
public class AlertCriteriaTest {

    private DAColumn daColumn = null;

    private Alert alert = null;

    /**
     * . Test the hash code generates equal number
     */
    @Test()
    public void testHashCodeEqual() {
        AlertCriteria alertCriteria1 = new AlertCriteria();
        AlertCriteria alertCriteria2 = new AlertCriteria();

        alertCriteria1.setAlert(alert);
        alertCriteria1.setOperandType(AlertOperandType.EQUALSTO);       
        alertCriteria1.setThreshold("threshold");
        alertCriteria1.setDaColumn(daColumn);

        alertCriteria2.setAlert(alert);
        alertCriteria2.setOperandType(AlertOperandType.EQUALSTO);       
        alertCriteria2.setThreshold("threshold");
        alertCriteria2.setDaColumn(daColumn);

        assertEquals("Hash codes are not same", alertCriteria2.hashCode(),
            alertCriteria1.hashCode());
    }

    /**
     * . Test the hash code generates different number
     */
    @Test()
    public void testHashCodeDifferent() {
        AlertCriteria alertCriteria1 = new AlertCriteria();
        AlertCriteria alertCriteria2 = new AlertCriteria();

        alertCriteria1.setAlert(alert);
        alertCriteria1.setOperandType(AlertOperandType.EQUALSTO);       
        alertCriteria1.setThreshold("threshold");
        alertCriteria1.setDaColumn(daColumn);

        alertCriteria2.setAlert(alert);
        alertCriteria2.setOperandType(AlertOperandType.NOTEQUALSTO);       
        alertCriteria2.setThreshold("threshold");
        alertCriteria2.setDaColumn(daColumn);

        assertEquals("Hash code should be different",
            alertCriteria1.hashCode() != alertCriteria2.hashCode(), true);
    }

    /**
     * . Tests two types are equal
     */
    @Test()
    public void testEqual() {
        AlertCriteria alertCriteria1 = new AlertCriteria();
        AlertCriteria alertCriteria2 = new AlertCriteria();

        alertCriteria1.setAlert(alert);
        alertCriteria1.setOperandType(AlertOperandType.EQUALSTO);       
        alertCriteria1.setThreshold("threshold");
        alertCriteria1.setDaColumn(daColumn);

        alertCriteria2.setAlert(alert);
        alertCriteria2.setOperandType(AlertOperandType.EQUALSTO);       
        alertCriteria2.setThreshold("threshold");
        alertCriteria2.setDaColumn(daColumn);

        assertEquals("Types should be equal",
            alertCriteria1.equals(alertCriteria1), true);
    }

    /**
     * . Tests two types are not equal
     */
    @Test()
    public void testNotEqual() {
        AlertCriteria alertCriteria1 = new AlertCriteria();
        AlertCriteria alertCriteria2 = new AlertCriteria();

        alertCriteria1.setAlert(alert);
        alertCriteria1.setOperandType(AlertOperandType.EQUALSTO);       
        alertCriteria1.setThreshold("threshold");
        alertCriteria1.setDaColumn(daColumn);

        // When compared with other object
        Object obj = new Object();

        assertEquals("Objects should not be equal", alertCriteria1.equals(obj),
            false);

        // When compared with different criterias
        alertCriteria2.setAlert(alert);
        alertCriteria1.setOperandType(AlertOperandType.GREATERTHAN);       
        alertCriteria2.setThreshold("threshold");
        alertCriteria2.setDaColumn(daColumn);

        assertEquals("Types should not be equal",
            alertCriteria1.equals(alertCriteria2), false);
    }
}
