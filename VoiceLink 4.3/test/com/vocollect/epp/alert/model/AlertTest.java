/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.model;

import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.model.NotificationPriority;

import static com.vocollect.epp.test.TestGroups.ALERT;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;

import java.util.Date;
import java.util.List;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * 
 * 
 * @author smittal
 */
@Test(groups = { FAST, MODEL, ALERT })
public class AlertTest {

    private DAInformation daInformation = null;

    private List<AlertCriteria> alertCriterias = null;

    private Date lastAlertEventTime = new Date();
    
    private Date lastEvaluationTime = new Date();

    /**
     * . Test the hash code generates equal number
     */
    @Test()
    public void testHashCodeEqual() {
        Alert alert1 = new Alert();
        Alert alert2 = new Alert();

        AlertReNotificationFrequency freq = new AlertReNotificationFrequency();
        freq.setHours(0);
        freq.setMinutes(45);

        alert1.setAlertName("alert1");
        alert1.setDaInformation(daInformation);
        alert1.setAlertCriterias(alertCriterias);
        alert1.setAlertNotificationPriority(NotificationPriority.INFORMATION);
        alert1.setEmailAddress("email");
        alert1.setReNotificationFrequency(freq);
        alert1.setAlertStatus(AlertStatus.ENABLED);
        alert1.setLastAlertEventTime(lastAlertEventTime);
        alert1.setLastEvaluationTime(lastEvaluationTime);

        alert2.setAlertName("alert1");
        alert2.setDaInformation(daInformation);
        alert2.setAlertCriterias(alertCriterias);
        alert2.setAlertNotificationPriority(NotificationPriority.INFORMATION);
        alert2.setEmailAddress("email");
        alert2.setAlertStatus(AlertStatus.ENABLED);
        alert2.setReNotificationFrequency(freq);
        alert2.setLastAlertEventTime(lastAlertEventTime);
        alert2.setLastEvaluationTime(lastEvaluationTime);

        assertEquals("Hash codes are not same", alert2.hashCode(),
            alert1.hashCode());
    }

    /**
     * . Test the hash code generates different number
     */
    @Test()
    public void testHashCodeDifferent() {
        Alert alert1 = new Alert();
        Alert alert2 = new Alert();
        AlertReNotificationFrequency freq = new AlertReNotificationFrequency();
        freq.setHours(0);
        freq.setMinutes(45);

        alert1.setAlertName("alert1");
        alert1.setDaInformation(daInformation);
        alert1.setAlertCriterias(alertCriterias);
        alert1.setAlertNotificationPriority(NotificationPriority.INFORMATION);
        alert1.setEmailAddress("email");
        alert1.setAlertStatus(AlertStatus.ENABLED);
        alert1.setReNotificationFrequency(freq);
        alert1.setLastAlertEventTime(lastAlertEventTime);
        alert1.setLastEvaluationTime(lastEvaluationTime);

        alert2.setAlertName("alert2");
        alert2.setDaInformation(daInformation);
        alert2.setAlertCriterias(alertCriterias);
        alert2.setAlertNotificationPriority(NotificationPriority.INFORMATION);
        alert2.setEmailAddress("email");
        alert2.setAlertStatus(AlertStatus.ENABLED);
        alert2.setReNotificationFrequency(freq);
        alert2.setLastAlertEventTime(lastAlertEventTime);
        alert2.setLastEvaluationTime(lastEvaluationTime);

        assertEquals("Hash code should be different",
            alert1.hashCode() != alert2.hashCode(), true);
    }

    /**
     * . Tests two types are equal
     */
    @Test()
    public void testEqual() {
        Alert alert1 = new Alert();
        Alert alert2 = new Alert();
        AlertReNotificationFrequency freq = new AlertReNotificationFrequency();
        freq.setHours(0);
        freq.setMinutes(45);
        
        alert1.setAlertName("alert1");
        alert1.setDaInformation(daInformation);
        alert1.setAlertCriterias(alertCriterias);
        alert1.setAlertNotificationPriority(NotificationPriority.INFORMATION);
        alert1.setEmailAddress("email");
        alert1.setAlertStatus(AlertStatus.ENABLED);
        alert1.setReNotificationFrequency(freq);
        alert1.setLastAlertEventTime(lastAlertEventTime);
        alert1.setLastEvaluationTime(lastEvaluationTime);

        alert2.setAlertName("alert1");
        alert2.setDaInformation(daInformation);
        alert2.setAlertCriterias(alertCriterias);
        alert2.setAlertNotificationPriority(NotificationPriority.INFORMATION);
        alert2.setEmailAddress("email");
        alert2.setAlertStatus(AlertStatus.ENABLED);
        alert2.setReNotificationFrequency(freq);
        alert2.setLastAlertEventTime(lastAlertEventTime);
        alert2.setLastEvaluationTime(lastEvaluationTime);

        assertEquals("Types should be equal", alert1.equals(alert2), true);
    }

    /**
     * . Tests two types are not equal
     */
    @Test()
    public void testNotEqual() {
        Alert alert1 = new Alert();
        Alert alert2 = new Alert();
        AlertReNotificationFrequency freq = new AlertReNotificationFrequency();
        freq.setHours(0);
        freq.setMinutes(45);
        
        alert1.setAlertName("alert1");
        alert1.setDaInformation(daInformation);
        alert1.setAlertCriterias(alertCriterias);
        alert1.setAlertNotificationPriority(NotificationPriority.INFORMATION);
        alert1.setEmailAddress("email");
        alert1.setAlertStatus(AlertStatus.ENABLED);
        alert1.setReNotificationFrequency(freq);
        alert1.setLastAlertEventTime(lastAlertEventTime);
        alert1.setLastEvaluationTime(lastEvaluationTime);

        // When compared with other object
        Object obj = new Object();

        assertEquals("Objects should not be equal", alert1.equals(obj), false);

        // Name of one alert is null
        alert2.setAlertName(null);
        alert2.setDaInformation(daInformation);
        alert2.setAlertCriterias(alertCriterias);
        alert2.setAlertNotificationPriority(NotificationPriority.INFORMATION);
        alert2.setEmailAddress("email");
        alert2.setAlertStatus(AlertStatus.ENABLED);
        alert2.setReNotificationFrequency(freq);
        alert2.setLastAlertEventTime(lastAlertEventTime);
        alert2.setLastEvaluationTime(lastEvaluationTime);

        assertEquals("Types should not be equal", alert1.equals(alert2), false);

        // When compared with different alerts
        alert2.setAlertName("alert2");
        alert2.setDaInformation(daInformation);
        alert2.setAlertCriterias(alertCriterias);
        alert2.setAlertNotificationPriority(NotificationPriority.INFORMATION);
        alert2.setEmailAddress("email");
        alert2.setAlertStatus(AlertStatus.ENABLED);
        alert2.setReNotificationFrequency(freq);
        alert2.setLastAlertEventTime(lastAlertEventTime);
        alert2.setLastEvaluationTime(lastEvaluationTime);

        assertEquals("Types should not be equal", alert1.equals(alert2), false);
    }
}
