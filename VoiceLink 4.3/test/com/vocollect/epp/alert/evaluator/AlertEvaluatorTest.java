/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.evaluator;

import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.alert.model.AlertCriteria;
import com.vocollect.epp.alert.service.AlertManager;
import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.service.impl.BaseDataProviderImplTestCase;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.util.AlertUtil;
import com.vocollect.epp.util.SiteContextHolder;

import static com.vocollect.epp.test.TestGroups.ALERT;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Test;

/**
 * Unit Tests for AlertCriteriaManagerImpl methods.
 * 
 * @author smittal
 */
@Test(groups = { ALERT })
public class AlertEvaluatorTest extends BaseDataProviderImplTestCase {

    private AlertManager alertManager;

    private JexlAlertEvaluator jexlAlertEvaluator;

    private DataAggregatorHandler dataAggregatorHandler = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/alert-evaluator-data.xml");
    }

    /**
     * Set the site context.
     * @param siteId - the site ID
     * @throws Exception
     * 
     */
    @Test(enabled = false)
    protected void setSiteContext(Long siteId) throws Exception {
        this.setUpSiteContextNoDefaultSite(true);
        SiteContextHolder.getSiteContext().setCurrentSite(siteId);
    }

    /**
     * @return the dataAggregatorHandler
     */
    @Test(enabled = false)
    public DataAggregatorHandler getDataAggregatorHandler() {
        return dataAggregatorHandler;
    }

    /**
     * @param dataAggregatorHandler the dataAggregatorHandler to set
     */
    @Test(enabled = false)
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.dataAggregatorHandler = dataAggregatorHandler;
    }

    /**
     * Getter for the alertManager property.
     * @return AlertManager value of the property
     */
    @Test(enabled = false)
    public AlertManager getAlertManager() {
        return alertManager;
    }

    /**
     * Setter for the alertManager property.
     * @param alertManager the new alertManager value
     */
    @Test(enabled = false)
    public void setAlertManager(AlertManager alertManager) {
        this.alertManager = alertManager;
    }

    /**
     * Getter for the jexlAlertEvaluator property.
     * @return JexlAlertEvaluator value of the property
     */
    @Test(enabled = false)
    public JexlAlertEvaluator getJexlAlertEvaluator() {
        return jexlAlertEvaluator;
    }

    /**
     * Setter for the jexlAlertEvaluator property.
     * @param jexlAlertEvaluator the new jexlAlertEvaluator value
     */
    @Test(enabled = false)
    public void setJexlAlertEvaluator(JexlAlertEvaluator jexlAlertEvaluator) {
        this.jexlAlertEvaluator = jexlAlertEvaluator;
    }

    /**
     * . Tests the getAll method
     * @throws Exception 
     */
    @Test()
    public void testIntegerCriterias() throws Exception {
        setSiteContext(-1L);

        Alert alert = this.alertManager.get(-1L);
        JSONArray output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);
        JSONObject record = output.getJSONObject(0);
        assertEquals(
            record.toString(),
            "{\"column8\":22.24,\"column7\":\"Tue May 14 23:11:15 IST 1974\",\"column1\":1,\"column0\":0,\"column2\"" 
            + ":2,\"column5\":700,\"column4\":\"Test column 5\"}");

        alert = this.alertManager.get(-2L);
        output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);
        record = output.getJSONObject(0);
        assertEquals(
            record.toString(),
            "{\"column8\":22.24,\"column7\":\"Tue May 14 23:11:15 IST 1974\",\"column1\":1,\"column0\":0,\"column2\"" 
                + ":2,\"column5\":700,\"column4\":\"Test column 5\"}");

        alert = this.alertManager.get(-3L);
        output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);
        record = output.getJSONObject(0);
        assertEquals(
            record.toString(),
            "{\"column8\":22.24,\"column7\":\"Tue May 14 23:11:15 IST 1974\",\"column1\":1,\"column0\":0,\"column2\"" 
                + ":2,\"column5\":700,\"column4\":\"Test column 5\"}");

        alert = this.alertManager.get(-4L);
        output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);
        record = output.getJSONObject(0);
        assertEquals(
            record.toString(),
            "{\"column8\":22.24,\"column7\":\"Tue May 14 23:11:15 IST 1974\",\"column1\":1,\"column0\":0,\"column2\"" 
                + ":2,\"column5\":700,\"column4\":\"Test column 5\"}");

        alert = this.alertManager.get(-5L);
        output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);
        record = output.getJSONObject(0);
        assertEquals(
            record.toString(),
            "{\"column8\":22.24,\"column7\":\"Tue May 14 23:11:15 IST 1974\",\"column1\":1,\"column0\":0,\"column2\"" 
                + ":2,\"column5\":700,\"column4\":\"Test column 5\"}");

        alert = this.alertManager.get(-6L);
        output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 0);
    }

    /**
     * . Tests the getAll method
     * @throws Exception 
     */
    @Test()
    public void testDateCriterias() throws Exception {
        setSiteContext(-1L);

        Alert alert = this.alertManager.get(-17L);
        JSONArray output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);
        JSONObject record = output.getJSONObject(0);
        assertEquals(
            record.toString(),
            "{\"column8\":22.24,\"column7\":\"Tue May 14 23:11:15 IST 1974\",\"column1\":1,\"column0\":0,\"column2\"" 
                + ":2,\"column5\":700,\"column4\":\"Test column 5\"}");

        
    }

    /**
     * . Tests the float type fields
     * @throws Exception 
     */
    @Test()
    public void testFloatCriterias() throws Exception {
        setSiteContext(-1L);

        Alert alert = this.alertManager.get(-18L);
        JSONArray output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);
        JSONObject record = output.getJSONObject(0);
        assertEquals(
            record.toString(),
            "{\"column8\":22.24,\"column7\":\"Tue May 14 23:11:15 IST 1974\",\"column1\":1,\"column0\":0,\"column2\"" 
                + ":2,\"column5\":700,\"column4\":\"Test column 5\"}");

        
    }
    /**
     * . Tests the getAll method
     * @throws Exception 
     */
    @Test()
    public void testStringCriterias() throws Exception {
        setSiteContext(-1L);

        Alert alert = this.alertManager.get(-7L);
        JSONArray output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);

        alert = this.alertManager.get(-8L);
        output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);

        alert = this.alertManager.get(-9L);
        output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);

        alert = this.alertManager.get(-10L);
        output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);

        alert = this.alertManager.get(-11L);
        output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);

    }

    /**
     * . Tests the getAll method
     * @throws Exception 
     */
    @Test()
    public void testTimeCriterias() throws Exception {
        setSiteContext(-1L);

        Alert alert = this.alertManager.get(-12L);
        JSONArray output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);
        JSONObject record = output.getJSONObject(0);
        assertEquals(
            record.toString(),
            "{\"column8\":22.24,\"column7\":\"Tue May 14 23:11:15 IST 1974\",\"column1\":1,\"column0\":0,\"column2\"" 
                + ":2,\"column5\":700,\"column4\":\"Test column 5\"}");

        alert = this.alertManager.get(-13L);
        output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);
        record = output.getJSONObject(0);
        assertEquals(
            record.toString(),
            "{\"column8\":22.24,\"column7\":\"Tue May 14 23:11:15 IST 1974\",\"column1\":1,\"column0\":0,\"column2\"" 
                + ":2,\"column5\":700,\"column4\":\"Test column 5\"}");

    }

    /**
     * . Tests the getAll method
     * @throws Exception 
     */
    @Test()
    public void testOrCriterias() throws Exception {
        setSiteContext(-1L);

        // Or criteria not satisfying
        Alert alert = this.alertManager.get(-14L);
        JSONArray output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 0);

        // One condition in or satisfies
        List<AlertCriteria> criterias = alert.getAlertCriterias();
        for (AlertCriteria criteria : criterias) {
            if (criteria.getId() == -14) {
                criteria.setThreshold("st col");
            }
        }
        output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);
    }

    /**
     * . Tests the getAll method
     * @throws Exception 
     */
    @Test()
    public void testAndCriterias() throws Exception {
        setSiteContext(-1L);

        Alert alert = this.alertManager.get(-15L);
        JSONArray output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 0);

        // All conditions are satisfying
        List<AlertCriteria> criterias = alert.getAlertCriterias();
        for (AlertCriteria criteria : criterias) {
            if (criteria.getId() == -16) {
                criteria.setThreshold("st col");
            }
        }
        output = this.jexlAlertEvaluator.evaluate(alert);
        assertNotNull(output);
        assertEquals(output.length(), 1);

    }
}
