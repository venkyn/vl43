/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.alert.dao;

import com.vocollect.epp.alert.model.AlertCriteria;
import com.vocollect.epp.dao.BaseDAOTestCase;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.ALERTCRITERIA;

import java.util.List;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
/**
 * Unit Tests for AlertCriteriaDAO methods.
 * 
 * @author smittal
 */
@Test(groups = { ALERTCRITERIA })
public class AlertCriteriaDAOTest extends BaseDAOTestCase {
    private AlertCriteriaDAO dao = null;

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/alert-data.xml");
    }

    /**
     * Method to perform data clean up after all unit test cases are executed.
     * @throws Exception
     */
    @AfterClass()
    protected void cleanUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * @param alertCriteriaDAO the user data access object
     */
    @Test(enabled = false)
    public void setAlertCriteriaDAO(AlertCriteriaDAO alertCriteriaDAO) {
        this.dao = alertCriteriaDAO;
    }

    /**
     * . Test the get all method
     */
    @Test()
    public void testGetAll() {
        try {
            List<AlertCriteria> alertCriterias = this.dao.getAll();
            assert (alertCriterias.size() > 0);
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }
}
