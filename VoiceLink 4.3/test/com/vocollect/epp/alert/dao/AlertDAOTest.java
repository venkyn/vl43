/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.dao;

import com.vocollect.epp.alert.model.Alert;
import com.vocollect.epp.dao.BaseDAOTestCase;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.ALERT;

import java.util.List;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Unit Tests for AlertDAO methods.
 * 
 * @author smittal
 */
@Test(groups = { ALERT })
public class AlertDAOTest extends BaseDAOTestCase {

    private AlertDAO dao = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/alert-data.xml");
    }
    
 
    /**
     * Method to perform data clean up after all unit test cases are executed.
     * @throws Exception
     */
    @AfterClass()
    protected void cleanUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * @param alertDAO the user data access object
     */
    @Test(enabled = false)
    public void setAlertDAO(AlertDAO alertDAO) {
        this.dao = alertDAO;
    }

    /**
     * . Test the get all method
     */
    @Test()
    public void testGetAll() {
        try {
            List<Alert> alerts = this.dao.getAll();
            assert (alerts.size() > 0);
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }

    @Test()
    public void testUniquenessByName() {
        try {
            List<Alert> alerts = this.dao.getAll();

            assert (this.dao
                .uniquenessByAlertName(alerts.get(0).getAlertName()) != this.dao
                .uniquenessByAlertName(alerts.get(1).getAlertName()));
        } catch (DataAccessException e) {
            fail(e.getLocalizedMessage(), e);
        }
    }
}
