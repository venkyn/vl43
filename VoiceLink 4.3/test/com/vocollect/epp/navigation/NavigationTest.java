/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.navigation;

import com.vocollect.epp.ui.Breadcrumb;
import com.vocollect.epp.ui.Navigation;

import java.util.List;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;


/**
 * Test class for the Navigation class.
 *
 *
 * @author jgeisler
 */
@Test()
public class NavigationTest {

    /**
     * Used to make sure the breadcrumbs are being
     * correctly generated from the URI.
     */
    public void testBreadcrumbTrail() {
        String appender = "breadcrumbs.";
        String testAppDiv = "application";
        String testActMenuDiv = "actionsMenu";
        String testActionDiv = "action";
        String testURI = "/epp"
                       + "/" + testAppDiv
                       + "/" + testActMenuDiv
                       + "/" + testActionDiv
                       + ".action";

        Navigation testNavigationObject = new Navigation(testURI);

        List<Breadcrumb> resultTrail = testNavigationObject.getBreadcrumbTrail();
        Breadcrumb crumbApp = resultTrail.get(0);
        Breadcrumb crumbObj = resultTrail.get(1);
        Breadcrumb crumbAct = resultTrail.get(2);
        String val = appender + testAppDiv;
        assertEquals("Testing breadcrumbs", (val), crumbApp.getText());
        val = val + "." + testActMenuDiv;
        assertEquals("Testing breadcrumbs", (val), crumbObj.getText());
        val = val + "." + testActionDiv;
        assertEquals("Testing breadcrumbs", (val), crumbAct.getText());

    }

    /**
     * Used to make sure that the application menu
     * name is being properly parsed from the URI.
     *
     */
    public void testApplicationMenu() {
        String testAppDiv = "application";
        String testURI = "/epp/" + testAppDiv;
        Navigation testNavigationObject = new Navigation(testURI);

        assertEquals("Testing app menu", testAppDiv, testNavigationObject.getApplicationMenu());
    }

    /**
     * Used to make sure that the action menu is
     * being properly parsed from the URI.
     *
     */
    public void testObjectName() {
        String testAppDiv = "application";
        String testActMenuDiv = "actionMenu";
        String testURI = "/epp/" + testAppDiv + "/" + testActMenuDiv;
        Navigation testNavigationObject = new Navigation(testURI);

        assertEquals("Testing action menu", testActMenuDiv, testNavigationObject.getActionsMenu());
    }

    /**
     * Used to test the results of a null URI.
     *
     */
    @Test(expectedExceptions = { IllegalArgumentException.class })
    public void testNullURI() {
        new Navigation(null);
    }

}
