/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.model.Homepage;
import com.vocollect.epp.model.Summary;
import com.vocollect.epp.model.TabularSummary;
import com.vocollect.epp.model.UpdatingSummary;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.UserHomepage;
import com.vocollect.epp.service.HomepageManager;
import com.vocollect.epp.service.SummaryManager;
import com.vocollect.epp.service.UserManager;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

/**
 *
 *
 * @author svoruganti
 */
@Test(groups = { DAO, SECURITY, FAST })
public class HomepageDAOTest extends BaseDAOTestCase {

    private HomepageManager homepageManager;

    private SummaryManager summaryManager;

    private UserManager userManager;

    /**
     * @param homepageManager .
     */
    @Test(enabled = false)
    public void setHomepageManager(HomepageManager homepageManager) {
        this.homepageManager = homepageManager;
    }

    /**
     * @param summaryManager .
     */
    @Test(enabled = false)
    public void setSummaryManager(SummaryManager summaryManager) {
        this.summaryManager = summaryManager;
    }

    /**
     * @param userManager .
     */
    @Test(enabled = false)
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void testAddSumamry() throws Exception {

       TabularSummary summary = new TabularSummary();
       summary.setTitle("Notification");
       summary.setActionUrl("/admin/notificationsummary/getData.action");
       summary.setRefreshRate(5);
       summary.setViewId(new Long("-7"));

       Summary summary1 = new Summary();
       summary1.setTitle("NormalSummary");
       summary1.setSourceFile("welcome.ftl");

       UpdatingSummary summary2 = new UpdatingSummary();
       summary2.setTitle("UpdatingSummary");
       summary2.setSourceFile("");
       summary2.setRefreshRate(5);
       summary2.setActionUrl("getUpdatingSumary.action");

       summaryManager.save(summary);
       summaryManager.save(summary1);
       summaryManager.save(summary2);
    }

    /**
     * @throws Exception
     */
    @Test(enabled = false)
    public void testAddHomepage() throws Exception {
      UserHomepage hm = new UserHomepage();
      Map<String , Long> sumLoc = new HashMap<String , Long>();
      sumLoc.put("11", new Long(4));
      sumLoc.put("12", new Long(2));
      sumLoc.put("21", new Long(3));
      sumLoc.put("22", new Long(3));

      hm.setSummaries(sumLoc);
      hm.setHomepageId(new Long(1));

      User user = new User();
      user = userManager.get(new Long("-1"));
      hm.setUser(user);

      homepageManager.save(hm);

    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void testAddDefaultHomepage() throws Exception {
        Homepage hm = new Homepage();
        Map<String , Long> sumLoc = new HashMap<String , Long>();
        sumLoc.put("11", new Long(3));
        sumLoc.put("12", new Long(2));

        hm.setSummaries(sumLoc);
        hm.setHomepageId(new Long(1));

        homepageManager.save(hm);


    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void getAllSummaries() throws Exception {
        summaryManager.getAll();

    }


}
