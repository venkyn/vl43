/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.hibernate.finder.QueryDecorator;
import com.vocollect.epp.model.Notification;
import com.vocollect.epp.model.NotificationPriority;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.Test;

/**
 * 
 *
 * @author ddoubleday
 */
@Test(groups = { DAO, FAST })
public class NotificationDAOTest extends BaseDAOTestCase {

    private NotificationDAO notificationDAO;
    
    
    /**
     * Getter for the notificationDAO property.
     * @return the value of the property
     */
    @Test(enabled = false)
    public NotificationDAO getNotificationDAO() {
        return this.notificationDAO;
    }

    
    /**
     * Setter for the notificationDAO property.
     * @param notificationDAO the new notificationDAO value
     */
    @Test(enabled = false)
    public void setNotificationDAO(NotificationDAO notificationDAO) {
        this.notificationDAO = notificationDAO;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    @Test(enabled = false)
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
            "data/dbunit/notification-test.xml", DatabaseOperation.INSERT);
        setUpSiteContext(true);      
    }


    /**
     * Test method for special case queries from the DAO.
     * @throws Exception
     */
    public void testAllQueries() throws Exception {
        List<Object[]> notificationInfo = 
            getNotificationDAO().getGroupedCountBySite(
                    "user 3", NotificationPriority.CRITICAL);
        assertEquals(notificationInfo.size(), 5, "getGroupedCountBySite");
        
        notificationInfo = 
            getNotificationDAO().getGroupedCountBySiteForAllSiteAccess(
                NotificationPriority.CRITICAL);
        assertEquals(notificationInfo.size(), 5, "size of critical all site");
        assertEquals((notificationInfo.get(0)[2]).toString(), "2", 
                     "count of critical all site");
        notificationInfo = 
            getNotificationDAO().getGroupedCountBySiteForAllSiteAccess(
                NotificationPriority.INFORMATION);
        assertEquals(notificationInfo.size(), 0, "size of information all site");
        // There should be 1 Site with one WARNING.
        notificationInfo = 
            getNotificationDAO().getGroupedCountBySiteForAllSiteAccess(
                NotificationPriority.WARNING);
        assertEquals(notificationInfo.size(), 1, "size of warning all site");
        assertEquals((notificationInfo.get(0)[2]).toString(), "1", 
            "count of warning all site");

        int count = getNotificationDAO().getUnackedSystemNotificationsCount(
            NotificationPriority.CRITICAL);
        assertEquals(count, 1, "unackedSystemNotificationsCount");

        List<String> userEmails = 
            getNotificationDAO().getUserEmailsBySiteId(-1L);
        assertEquals(userEmails.size(), 1);
        
        List<Notification> notifications = 
            getNotificationDAO().getUnackNotifications(NotificationPriority.CRITICAL);
        assertEquals(notifications.size(), 8);

        Number max = getNotificationDAO().getMaxNotificationIdForSystemTag(
            NotificationPriority.WARNING);
        assertEquals(max.intValue(), 6, "getMaxNotificationIdForSystemTag");

        max = getNotificationDAO().getMaxNotificationIdForSiteId(
            NotificationPriority.CRITICAL, -1L);
        assertEquals(max.intValue(), 7, "getMaxNotificationIdForSiteId");

        Calendar cal = Calendar.getInstance();
        cal.set(2006, Calendar.NOVEMBER, 1);
        Date date = cal.getTime();
        notifications = getNotificationDAO().listOlderThan(new QueryDecorator(), date);
        assertEquals(notifications.size(), 1, "listOlderThan");
    }

}
