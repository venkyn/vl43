/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.util.TimeZone;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Tests for siteDAO.
 *
 * @author KalpnaT
 */
@Test(groups = { DAO, FAST, SECURITY })
public class SiteDAOTest extends BaseDAOTestCase {

    private long testSiteID;
    private SiteDAO dao  = null;

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * Test method for 'com.vocollect.epp.dao.siteDAO.setSiteDAO(siteDAO dao)'.
     * @param siteDAO the site data access object
     */
    @Test(enabled = false)
    public void setSiteDAO(SiteDAO siteDAO) {
        this.dao = siteDAO;
    }


    /**
     * Test method for 'com.vocollect.epp.dao.siteDAO.get)'.
     * @throws Exception for any error
     */
    public void testGetSite() throws Exception {
        final Long siteID = -1L;
        Site site = dao.get(siteID);
        assertNotNull(site);
        assertTrue(site.getName().equals("Default"));
    }

    /**
     * Test method for 'com.vocollect.epp.dao.siteDAO.save(Site site)'.
     * @throws Exception for any error
     */
    @Test(enabled = true)
    public void testUpdateSite() throws Exception {
        final Long siteID = -1L;
        Site site = dao.get(siteID);

        site.setDescription("Update test");
        TimeZone tz = TimeZone.getDefault();
        site.setTimeZone(tz);
        site.setNotes("notes");

        dao.save(site);

        site = dao.get(siteID);

        // make sure all of the fields we updated were saved
        assertEquals("Update test", site.getDescription(), "Description Updated");
        assertEquals("notes", site.getNotes(), "Notes Updated");
        assertTrue(site.getTimeZone().equals(tz), "TimeZone Updated");

        // verify that constraint violation occurs when adding new site
        // with same name
        String siteName = (dao.get(siteID)).getName();
        site = new Site();
        site.setName(siteName);
        try {
            dao.save(site);
            fail("saveSite didn't throw Database constraint Exception");
        } catch (Exception e) {
            assertNotNull(e);
            log.debug("expected exception: " + e.getMessage());
        }

    }


    /**
     * Test method for 'com.vocollect.epp.dao.siteDAO.delete(Site site)'.
     * @throws Exception for any error
     */
    @Test(enabled = true)
    public void testAddAndRemoveSite() throws Exception {
        // test site with all fields
        Site site = new Site();
        site.setDescription("Add and remove test");
        site.setName("testSite");
        site.setNotes("Notes");
        TimeZone tz = TimeZone.getDefault();
        site.setTimeZone(tz);

        dao.save(site);
        final long siteID = site.getId();
        assertNotNull(site.getName());

        dao.delete(site.getId());

        try {
           site = dao.get(siteID);
           fail("found deleted site testSite");
        } catch (EntityNotFoundException e) {
            log.debug("expected exception: " + e.getMessage());
        }

    }

    /**
     * Test method for 'com.vocollect.epp.dao.siteDAO.save(Site site)'.
     * @throws Exception for any error
     */
    @Test(enabled = true)
    public void testAddSiteNoName() throws Exception {
        // test site with no site name. Should throw exception
        try {
            Site site = new Site();
            site.setDescription("Add and remove test");
            //Don't set the name
            //site.setName("testSite");
            site.setNotes("Notes");
            TimeZone tz = TimeZone.getDefault();
            site.setTimeZone(tz);

            dao.save(site);
            fail("testAddSiteNoName should have thrown an exception");
        } catch (Exception e) {
            // this is the expected exception
            log.info("expected exception in siteDAOTest.testAddSiteNoName");
        }
    }

    /**
     * Test method for 'com.vocollect.epp.dao.siteDAO.save(Site site)'.
     * @throws Exception for any error
     */
    @Test(enabled = true)
    public void testAddOneSite() throws Exception {
        // Add the site separately so that it can be deleted
        // within a separate method that calls remove.
        Site site = new Site();
        site.setDescription("Add and remove test");
        site.setName("testSite1");
        site.setNotes("Notes");
        TimeZone tz = TimeZone.getDefault();
        site.setTimeZone(tz);
        dao.save(site);
        testSiteID = site.getId();
    }

    /**
     * Test method for 'com.vocollect.epp.dao.siteDAO.delete(Site site)'.
     * @throws Exception for any error
     */
    @Test(enabled = true, dependsOnMethods = { "testAddOneSite" })
    public void testRemoveOneSite() throws Exception {
        // the AddOneSite method will run and independently save a
        // site to the database. This method will remove that same site
        // But since it's not within the same method, this should require
        // a trip to the database to insure that it reads the site again.

        // This is not actually a reliable test, because the dependsOnMethods
        // attribute only insures that the dependent method will run after
        // the named method. It doesn't guarantee that nothing will run in
        // between them. If another test from another class runs first, data
        // will be cleaned up and this fails. So, the test is changed
        // to only attempt to delete if the site is actually still
        // around. --ddoubleday

        try {
            Site site = dao.get(testSiteID);
            dao.delete(site.getId());
        } catch (EntityNotFoundException e) {
            // This is the workaround, it may not be there if another
            // test has run in between this and the other one.
        }
    }
}

