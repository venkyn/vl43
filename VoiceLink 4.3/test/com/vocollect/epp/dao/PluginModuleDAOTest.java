/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.plugin.PluginModule;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;

import java.util.List;

import org.testng.annotations.Test;

/**
 * Test cases for the PluginModuleDAO.
 *
 * @author ddoubleday
 */
@Test(groups = { DAO, FAST })
public class PluginModuleDAOTest extends BaseDAOTestCase {

    private PluginModuleDAO dao  = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * Called by the framework to set the PluginModuleDAO object.
     * @param pluginModuleDAO the data access object
     */
    @Test(enabled = false)
    public void setPluginModuleDAO(PluginModuleDAO pluginModuleDAO) {
        this.dao = pluginModuleDAO;
    }

    /**
     * Test the listEnabledModules method.
     * @throws Exception
     */
    public void testListEnabledModules() throws Exception {
        List<PluginModule> modules = this.dao.listEnabledModules();
        assertFalse(modules.isEmpty(), "enabled modules");

        int sequenceNumber = 0;
        for (PluginModule module : modules) {
            assertTrue(module.getIsEnabled(), "isEnabled");
            assertTrue(module.getSequenceNumber() >= sequenceNumber, "sequence");
        }
    }
}
