/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.model.User;
import com.vocollect.epp.model.UserColumn;
import com.vocollect.epp.model.View;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test cases for user columns.
 *
 * @author ??
 */
@Test(groups = { DAO, FAST })
public class UserColumnDAOTest extends BaseDAOTestCase {

    private GenericDAO<View> viewDAO = null;

    private UserColumnDAO userColumnDAO = null;

    private UserDAO userDAO = null;

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/userColumn-test.xml");
    }

    /**
     * @throws Exception DBUnit may throw an exception
     */
    @AfterClass
    protected void classTearDown() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        //adapter.resetEppData();
        adapter.handleFlatXmlResource(
            "data/dbunit/userColumn-test.xml", DatabaseOperation.DELETE_ALL);
    }

    /**
     * @param dao .
     */
    @Test(enabled = false)
    public void setUserColumnDAO(UserColumnDAO dao) {
        this.userColumnDAO = dao;
    }

    /**
     * @param dao .
     */
    @Test(enabled = false)
    public void setViewDAO(GenericDAO<View> dao) {
        this.viewDAO = dao;
    }

    /**
     * @param dao .
     */
    @Test(enabled = false)
    public void setUserDAO(UserDAO dao) {
        this.userDAO = dao;
    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void testGetView() throws Exception {
        View v;
        v = viewDAO.get(-1L);
        assertNotNull(v);
        assertEquals("users", v.getName());
    }

    /**
     * Test method for 'com.vocollect.epp.dao.UserColumnDAO.get(User u, Column).
     * c)'
     * @throws Exception
     */
    @Test(enabled = false)
    public void testGetUserColumn() throws Exception {

        User u = null;
        u = userDAO.get(-1L);

        UserColumn uc = userColumnDAO.findByUserAndColumn(u.getId(), -1L);
        assertNotNull(uc);
        assertEquals(new Long(-1), uc.getId());
        assertEquals(100, uc.getWidth());
        assertEquals(5, uc.getOrder());

        // now get one that does not exist
        u = userDAO.get(-2L);
        uc = userColumnDAO.findByUserAndColumn(u.getId(), -1L);
        assertNull(uc);

    }

    /**
     * Test method for 'com.vocollect.epp.dao.UserDAO.saveUser(User user)'.
     * @throws Exception
     */
    @Test(enabled = true)
    public void testSaveUserColumn() throws Exception {
        User u = null;
        u = userDAO.get(-1L);

        UserColumn uc = userColumnDAO.findByUserAndColumn(u.getId(), -1L);
        assertNotNull(uc);
        assertEquals(5, uc.getOrder());

        // set the user and column values and get the column we need
        // change some values
        uc.setOrder(2);

        // write to DB
        userColumnDAO.save(uc);

        // verify the save worked
        uc = userColumnDAO.findByUserAndColumn(u.getId(), -1L);
        assertNotNull(uc);
        assertEquals(2, uc.getOrder());

    }

}
