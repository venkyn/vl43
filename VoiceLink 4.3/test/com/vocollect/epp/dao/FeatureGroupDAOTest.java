/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.model.FeatureGroup;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.test.TestGroups;

import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * FeatureGroupDAO test cases.
 *
 * @author ddoubleday
 */
@Test(groups = { TestGroups.DAO, TestGroups.SECURITY, TestGroups.FAST })
public class FeatureGroupDAOTest extends BaseDAOTestCase {

    private GenericDAO<FeatureGroup> dao  = null;

    /**
     * Method called to reinitialize the database when this class is loaded.
     * @throws Exception
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * Called by the framework to set the RoleDAO object.
     * @param fgdao the DAO.
     */
    @Test(enabled = false)
    public void setFeatureGroupDAO(GenericDAO<FeatureGroup> fgdao) {
        this.dao = fgdao;
    }

    /**
     * Test method for 'DAO.retrieveAll with FeatureGroups'.
     * @throws Exception
     */
    public void testRetrieveAll() throws Exception {
        List<FeatureGroup> groups = dao.getAll();
        assertTrue(!groups.isEmpty(), "Got feature groups");
        for (FeatureGroup group : groups) {
            assertTrue(!group.getFeatures().isEmpty(), "Got features");
        }
    }

}
