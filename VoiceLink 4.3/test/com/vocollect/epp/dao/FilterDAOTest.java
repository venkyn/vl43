/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.model.Filter;
import com.vocollect.epp.model.FilterCriterion;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.service.UserPreferencesManager;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.test.TestGroups;
import com.vocollect.epp.ui.OperandSetup;

import java.util.LinkedList;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


/**
 * Test for ColumnDAO interface.
 *
 * @author ddoubleday
 */
@Test(groups = { TestGroups.DAO, TestGroups.FAST })
public class FilterDAOTest extends BaseDAOTestCase {

    private FilterDAO filterDAO;
    private UserManager userManager;
    private ColumnDAO columnDAO;
    
    private UserPreferencesManager userPrefsManager;

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception
     */
    @BeforeMethod
    protected void methodSetup() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        setUpSiteContext(false);
    }


    /**
     * @param dao .
     */
    @Test(enabled = false)
    public void setFilterDAO(FilterDAO dao) {
        this.filterDAO = dao;
    }

    /**
     * @param dao .
     */
    @Test(enabled = false)
    public void setColumnDAO(ColumnDAO dao) {
        this.columnDAO = dao;
    }

    /**
     * @param umngr .
     */
    @Test(enabled = false)
    public void setUserManager(UserManager umngr) {
        this.userManager = umngr;
    }

    /**
     * @param upm .
     */
    @Test(enabled = false)
    public void setUserPreferencesManager(UserPreferencesManager upm) {
        this.userPrefsManager = upm;
    }

    /**
     *
     * @throws DataAccessException
     * @throws BusinessRuleException
     */
    @Test(enabled = true)
    public void testFindByUserIdAndViewId() throws DataAccessException, BusinessRuleException {

        LinkedList<FilterCriterion> lfc = new LinkedList<FilterCriterion>();
        FilterCriterion fc = new FilterCriterion(columnDAO.get(-3L), OperandSetup.getOperandById(-3L), "what", "what", false);
        lfc.add(fc);
        Filter f = new Filter(userManager.findByName("admin"), userPrefsManager.getView(-1L), lfc);
        filterDAO.save(f);


        Filter newF = filterDAO.findByUserIdAndViewId(userManager.findByName("admin").getId(), -1L);

        assertEquals(newF.getUser().getId(), f.getUser().getId());
        assertEquals(newF.getView().getId(), f.getView().getId());

    }


}
