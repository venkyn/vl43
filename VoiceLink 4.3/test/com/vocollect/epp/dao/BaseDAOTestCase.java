/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.logging.Logger;
import com.vocollect.epp.model.Site;
import com.vocollect.epp.test.DepInjectionSpringContextNGTests;
import com.vocollect.epp.util.SiteContext;
import com.vocollect.epp.util.SiteContextHolder;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import org.springframework.orm.hibernate3.SessionHolder;
import org.springframework.transaction.support.TransactionSynchronizationManager;


/**
 * Base class for running DAO tests.
 *
 * @author Dennis Doubleday
 */
public abstract class BaseDAOTestCase extends DepInjectionSpringContextNGTests {
    // Just declare a single logger for all tests that derive from this, name
    // it with the package name.
    protected static final Logger log =
        new Logger(BaseDAOTestCase.class.getPackage().toString());

    public static final String[] EPP_DAO_TEST_CONFIGS = new String[] {
        "classpath*:/applicationContext-epp-utils.xml",
        "classpath*:/applicationContext-epp-dataaggregator.xml",
        "classpath*:/applicationContext-epp-evaluator.xml",
        "classpath*:/applicationContext-epp-service.xml",
        "classpath*:/applicationContext-epp-dao.xml",
        "classpath*:/applicationContext-epp-resources.xml",
        "classpath*:/applicationContext-epp-hibernate-models.xml",
        "classpath*:/applicationContext-epp-hibernate.xml",
        "classpath*:/applicationContext-epp-users-security.xml",
        "classpath*:/applicationContext-epp-security.xml",
        "classpath*:/applicationContext-epp-events.xml"
        };

    private static final String[] EPP_DAO_ARCHIVE_TEST_CONFIGS = new String[] {
        "classpath*:/applicationContext-epp-hibernate-archive.xml"
        };

    /**
     * @return the Spring config locations required for Archive tests.
     */
    public static final String[] getArchiveDAOTestConfigLocations() {
        String[] config = new String[EPP_DAO_TEST_CONFIGS.length
                                     + EPP_DAO_ARCHIVE_TEST_CONFIGS.length];
        System.arraycopy(EPP_DAO_TEST_CONFIGS, 0, config, 0, EPP_DAO_TEST_CONFIGS.length);
        System.arraycopy(EPP_DAO_ARCHIVE_TEST_CONFIGS, 0, config,
            EPP_DAO_TEST_CONFIGS.length, EPP_DAO_ARCHIVE_TEST_CONFIGS.length);
        return config;


    }

    private SessionFactory sessionFactory;

    private SiteContext siteContext;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.test.DepInjectionSpringContextNGTests#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return EPP_DAO_TEST_CONFIGS;
    }

    /**
     * Constructor.
     */
    public BaseDAOTestCase() {
        setAutowireMode(AUTOWIRE_BY_NAME);
    }


    /**
     * @param filterSites filter on sites.
     * @throws Exception on failure to set up.
     */
    public void setUpSiteContext(boolean filterSites) throws Exception {
        SiteContextHolder.setSiteContext(this.siteContext);
        Site defaultSite = SiteContextHolder.getSiteContext().getSite(-1L);
        SiteContextHolder.getSiteContext().setHasAllSiteAccess(true);
        SiteContextHolder.getSiteContext().setFilterBySite(filterSites);
        SiteContextHolder.getSiteContext().setCurrentSite(defaultSite);

    }

    /**
     * used to simulate a SYSTEM user like the purge.
     * @param filterSites filter on sites.
     * @throws Exception on failure to set up.
     */
    public void setUpSiteContextNoDefaultSite(boolean filterSites) throws Exception {
        SiteContextHolder.setSiteContext(this.siteContext);
        SiteContextHolder.getSiteContext().getSite(-1L);
        SiteContextHolder.getSiteContext().setHasAllSiteAccess(true);
        SiteContextHolder.getSiteContext().setFilterBySite(filterSites);



    }



    /**
     * @see DepInjectionSpringContextNGTests#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        // The following code simulates what happens in the
        // OpenSessionInViewFilter in the real app.
        Session session = SessionFactoryUtils.getSession(this.sessionFactory, true);
        TransactionSynchronizationManager.bindResource(
            sessionFactory, new SessionHolder(session));
    }

    /**
     * @see DepInjectionSpringContextNGTests#onTearDown()
     */
    @Override
    protected void onTearDown() throws Exception {
        // The following code simulates what happens in the
        // OpenSessionInViewFilter in the real app.
        Session session = ((SessionHolder)
            TransactionSynchronizationManager.getResource(sessionFactory)).getSession();
        TransactionSynchronizationManager.unbindResource(sessionFactory);
        SessionFactoryUtils.releaseSession(session, sessionFactory);
        super.onTearDown();
    }

    /**
     * Getter for the sessionFactory property.
     *
     * @return sessionFactory
     */
    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }


    /**
     * Setter for the sessionFactory property.
     *
     * @param sessionFactory .
     */
    public void setSessionFactory(SessionFactory sessionFactory) {
        masterSetSessionFactory(sessionFactory);
    }

    /**
     * Master setter for the sessionFactory property.
     * Now that we have two datasources, we need a way to use archiveSessionFactory.
     * The method I used was override the setSessionFactory() to do nothing and
     * provide a setArchiveSessionFactory() that sets sessionFactory in each DAO tester
     * that requires the archive database.
     *
     * @param masterSessionFactory .
     */
    public void masterSetSessionFactory(SessionFactory masterSessionFactory) {
        this.sessionFactory = masterSessionFactory;
    }

    /**
     * @param siteContext .
     */
    public void setSiteContext(SiteContext siteContext) {
        this.siteContext = siteContext;
    }
}
