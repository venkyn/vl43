/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.model.LDAPConfiguration;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.DAO;

import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * A class for unit testing LDAPConfiguration DAO.
 *
 * @author kganesan
 */
@Test(groups = { DAO })
public class LDAPConfigurationDAOTest extends BaseDAOTestCase {

    private LDAPConfigurationDAO ldapConfigurationDAO = null;
    protected static final String DATA_DIR = "data/dbunit/";


    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        String sDataURLFF = DATA_DIR + "LDAPConfigurationDAOTest.xml";
        adapter.handleFlatXmlResource(sDataURLFF, DatabaseOperation.INSERT);
    }



    /**
     * Sets the ldap configuration DAO via Spring autowiring.
     *
     * @param ldapConfigurationDAO - The LDAPConfigurationDAO object.
     */
    @Test(enabled = false)
    public void setLdapConfigurationDAO(LDAPConfigurationDAO ldapConfigurationDAO) {
        this.ldapConfigurationDAO = ldapConfigurationDAO;
    }

    /**
     * Tests the get method.
     *
     */
    @Test(enabled = true)
    protected void testGetId() throws Exception {
        assertNotNull(this.ldapConfigurationDAO, "LDAP configuration DAO not initialized.");

        Integer findIndex = null;

        try {
            final long sampleSize = this.ldapConfigurationDAO.getCount();
            assertTrue(sampleSize > 0, "No LDAP configuration data could be found for testing.");

            List<LDAPConfiguration> ldap = null;

            for (findIndex = 0; findIndex < sampleSize; findIndex++) {
                ldap = this.ldapConfigurationDAO.getAll();
                List<LDAPConfiguration> daoIDLDAP = this.ldapConfigurationDAO.getAll();

                assertEquals(ldap.get(findIndex).getId(), daoIDLDAP.get(findIndex).getId(),
                    "IDs do not match as expected.");
                assertTrue(ldap.equals(daoIDLDAP), "Incorrect LDAP configuration retrieved by ID.");
            }

        } catch (Throwable t) {

            fail("Exception for ldap configuration; "
                 + "index = " + findIndex.toString(), t);
        }

    }

    /**
     * Tests for Uniqueness.
     *
     */
    @Test(enabled = true)
    public void testLDAPConfigurationUniqueness() throws Exception {
        assertNotNull(this.ldapConfigurationDAO, "LDAP Configuration DAO not initialized.");

        Long ldapTestId = null;
        Long getPkId = null;
        ldapTestId = -204L;

        LDAPConfiguration daoIDLDAP = this.ldapConfigurationDAO.get(ldapTestId);

        getPkId = this.ldapConfigurationDAO.uniqueness(
            daoIDLDAP.getUseSSL(),
            daoIDLDAP.getLdapHost(),
            daoIDLDAP.getLdapPort(),
            daoIDLDAP.getSearchUsername(),
            daoIDLDAP.getSearchPassword(),
            daoIDLDAP.getSearchBase(),
            daoIDLDAP.getSearchableAttribute(),
            daoIDLDAP.getPasswordAttribute());

        assertEquals(ldapTestId, getPkId, "IDs do not match as expected.");
    }

    /**
     * Tests basic CRUD operations for ldap configuration objects.
     */
    @Test(enabled = true, groups = { "check-in", "overnight" })
    public void testCRUD() {

        assertTrue(null != this.ldapConfigurationDAO, "ldap configuration DAO not initialized.");

        Long savedID = null;
        String operation = null;
        LDAPConfiguration daoLDAPConf = null;

        try {
            operation = "data initialization";
            LDAPConfiguration ldapConf = new LDAPConfiguration();
            ldapConf.setUseSSL(true);
            ldapConf.setLdapHost("ldapHost3");
            ldapConf.setLdapPort("ldapPort3");
            ldapConf.setSearchUsername("searchUsername3");
            ldapConf.setSearchPassword("searchPassword3");
            ldapConf.setSearchBase("searchBase3");
            ldapConf.setSearchableAttribute("searchableAttribute3");
            ldapConf.setPasswordAttribute("passwordAttribute3");

            // create
            operation = "create";
            this.ldapConfigurationDAO.save(ldapConf);
            savedID = ldapConf.getId();

            // update
            operation = "update";
            ldapConf.setSearchUsername("searchUsername3-UPDATED");
            this.ldapConfigurationDAO.save(ldapConf);

            // read
            operation = "read";
            daoLDAPConf = this.ldapConfigurationDAO.get(savedID);
            assertTrue(0 == daoLDAPConf.getSearchUsername().compareTo("searchUsername3-UPDATED"),
            "Update did not retain changed value.");
            assertTrue(ldapConf.equals(daoLDAPConf), "Update to ldap configuration didn't take.");

        } catch (Throwable t) {
            fail("Exception during " + operation + " in ldap configuration CRUD test.", t);
        }

        try {
            // delete
            operation = "delete";
            this.ldapConfigurationDAO.delete(daoLDAPConf);
            LDAPConfiguration daoDeleted = this.ldapConfigurationDAO.get(savedID);
            if (null != daoDeleted) {
                fail("Deletion of LDAP configuration did not work.");
            }
        } catch (EntityNotFoundException ex) {
            // OK, it shouldn't be found
        } catch (Throwable t) {
            fail("Exception during " + operation + " in ldap configuration CRUD test.", t);
        }

    }
}
