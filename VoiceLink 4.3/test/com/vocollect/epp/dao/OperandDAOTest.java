/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.ui.Operand;
import com.vocollect.epp.ui.OperandSetup;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.test.TestGroups;

import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * Test for ColumnDAO interface.
 *
 * @author ddoubleday
 */
@Test(groups = { TestGroups.DAO, TestGroups.FAST })
public class OperandDAOTest extends BaseDAOTestCase {

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();

    }

    /**
     *
     * @throws DataAccessException
     */
    @Test(enabled = true)
    public void testFindByType() throws DataAccessException {
        List<Operand> stringOperands = OperandSetup.getOperandByType(Operand.TYPE_STRING);
        assertEquals(stringOperands.size(), 4);
        assertEquals(stringOperands.get(0).getLabel(), "startsWith");

        List<Operand> numberOperands = OperandSetup.getOperandByType(Operand.TYPE_NUMBER);
        assertEquals(numberOperands.size(), 6);
        assertEquals(numberOperands.get(0).getLabel(), "equals");

        List<Operand> dateOperands = OperandSetup.getOperandByType(Operand.TYPE_DATE);
        assertEquals(dateOperands.size(), 3);
        assertEquals(dateOperands.get(0).getLabel(), "emptyDate");

        List<Operand> enumerationOperands = OperandSetup.getOperandByType(Operand.TYPE_ENUMERATION);
        assertEquals(enumerationOperands.size(), 2);
        assertEquals(enumerationOperands.get(0).getLabel(), "equalTo");
    }



}
