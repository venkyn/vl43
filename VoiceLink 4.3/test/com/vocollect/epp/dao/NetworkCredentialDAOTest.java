/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.EntityNotFoundException;
import com.vocollect.epp.model.NetworkCredential;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.DAO;

import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Performs various tests of the network credential DAO.
 *
 * @author sthomas
 */
@Test(groups = { DAO })
public class NetworkCredentialDAOTest extends BaseDAOTestCase {

    protected static final String DATA_DIR = "data/dbunit/";

    // the DAO object
    private NetworkCredentialDAO dao = null;

    /**
     * Sets the DAO object via Spring configuration.
     * @param theDAO - The network credential DAO.
     */
    @Test(enabled = false)
    public void setNetworkCredentialDAO(NetworkCredentialDAO theDAO) {
        this.dao = theDAO;
    }

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception for any error
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        String sDataURLFF = DATA_DIR + "NetworkCredentialDAOTest.xml";
        adapter.handleFlatXmlResource(sDataURLFF, DatabaseOperation.INSERT);
    }

    /**
     * Tests the findByUserName() and get() methods.
     * 
     */
    @Test(enabled = true)
    public void testRetrievalOperations() {

        assertNotNull(this.dao, "Network credential DAO not initialized.");

        Integer findIndex = null;

        Long sampleSize = null;

        try {
            sampleSize = this.dao.getCount();
        } catch (DataAccessException t) {
            fail("DataAccessException has occurred.", t);
        }
        assertTrue(
            sampleSize > 0,
            "No network credential data could be found for testing.");

        for (findIndex = 1; findIndex <= sampleSize; findIndex++) {

            // test the retrieval by username
            String name = "USERNAME" + findIndex.toString();
            List<NetworkCredential> daoCreds = this.dao.listByUserName(name);
            NetworkCredential listedCred = daoCreds.get(0);
            assertNotNull(daoCreds, "Network credential(s) for '" + name
                + "' not retrieved.");
            assertEquals(
                listedCred.getUserName(), name,
                "Incorrect network credential retrieved by username.");

            // test the retrieval by id
            Long credID = listedCred.getId();
            NetworkCredential daoIDCred = null;
            try {
                daoIDCred = this.dao.get(credID);
            } catch (DataAccessException e) {
                fail("DataAccessException has occurred.", e);
            }
            assertEquals(
                listedCred.getId(), daoIDCred.getId(),
                "IDs do not match as expected.");
            assertTrue(
                listedCred.equals(daoIDCred),
                "Incorrect network credential retrieved by ID.");
        }

    }

    /**
     * Tests basic CRUD operations for network credential objects.
     */
    @Test(enabled = true, groups = { "check-in", "overnight" })
    public void testCRUD() {

        assertNotNull(this.dao, "Network credential DAO not initialized.");

        Long savedID = null;
        String operation = null;
        NetworkCredential daoCred = null;
        try {

            operation = "data initialization";
            NetworkCredential updateCred = new NetworkCredential();
            updateCred.setPlainUserName("PLAINNAME10");
            updateCred.setUserName("USERNAME10");
            updateCred.setPassword("PASSWORD10");
            updateCred.setCertificateFileName("CERTFILENAME10");
            updateCred.setCertificateKeyFileName("KEYFILENAME10");
            updateCred.setCertificatePassword("CERTPASSWORD10");

            // create
            operation = "create";
            this.dao.save(updateCred);
            savedID = updateCred.getId();

            // update
            operation = "update";
            String newName = "USERNAME10-UPDATED";
            updateCred.setUserName(newName);
            this.dao.save(updateCred);

            // read
            operation = "read";
            daoCred = this.dao.get(savedID);
            assertEquals(
                updateCred.getUserName(), newName,
                "Update did not retain changed value.");
            assertTrue(
                daoCred.equals(updateCred),
                "Update to network credential didn't take.");

        } catch (Throwable t) {
            fail("Exception during " + operation
                + " in network credential CRUD test.", t);
        }

        try {
            // delete
            operation = "delete";
            this.dao.delete(daoCred);
            NetworkCredential daoDeleted = this.dao.get(savedID);
            assertNull(daoDeleted, "Delete of network credential did not work.");
        } catch (EntityNotFoundException ex) {

            // Good! It shouldn't be found...

        } catch (Throwable t) {
            fail("Exception during " + operation
                + " in network credential CRUD test.", t);
        }

    }
}
