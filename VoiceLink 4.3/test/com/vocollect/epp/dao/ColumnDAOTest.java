/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.model.Column;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.View;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.test.TestGroups;

import java.util.List;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * Test for ColumnDAO interface.
 *
 * @author ddoubleday
 */
@Test(groups = { TestGroups.DAO, TestGroups.FAST })
public class ColumnDAOTest extends BaseDAOTestCase {

    private UserDAO userDAO = null;

    private GenericDAO<View> viewDAO = null;

    private ColumnDAO columnDAO = null;

    /**
     * classSetUp is used to repopulate the database before each test.
     * @throws Exception
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource(
            "data/dbunit/userColumn-test.xml", DatabaseOperation.INSERT);

    }

    /**
     * @param dao .
     */
    @Test(enabled = false)
    public void setUserDAO(UserDAO dao) {
        this.userDAO = dao;
    }


    /**
     * Setter for the columnDAO property.
     * @param columnDAO the new columnDAO value
     */
    @Test(enabled = false)
    public void setColumnDAO(ColumnDAO columnDAO) {
        this.columnDAO = columnDAO;
    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void testGetColumn() throws Exception {
        Column c = this.columnDAO.get(-1L);
        assertNotNull(c);
        assertEquals("name", c.getField());
    }

    /**
     * @throws Exception
     */
    @Test(enabled = true)
    public void testGetColumns() throws Exception {
        setUpSiteContext(true);

        View v = this.viewDAO.get(-1L);
        User u = userDAO.get(-1L);
        List<Column> columns = this.columnDAO.getColumns(v, u);
        assertEquals(columns.size(), 8);
    }


    /**
     * Setter for the viewDAO property.
     * @param viewDAO the new viewDAO value
     */
    @Test(enabled = false)
    public void setViewDAO(GenericDAO<View> viewDAO) {
        this.viewDAO = viewDAO;
    }

}
