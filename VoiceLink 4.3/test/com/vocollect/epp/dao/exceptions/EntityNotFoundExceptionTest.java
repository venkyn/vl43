/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.exceptions;

import com.vocollect.epp.errors.SystemErrorCode;
import com.vocollect.epp.model.User;

import static com.vocollect.epp.test.TestGroups.EXCEPTIONS;
import static com.vocollect.epp.test.TestGroups.FAST;

import org.testng.annotations.Test;

/**
 * Test class for the EntityNotFoundException class.
 *
 * @author Dennis Doubleday
 */
@Test(groups = { FAST, EXCEPTIONS })
public class EntityNotFoundExceptionTest {

    /**
     * Test method for 'EntityNotFoundException(Class, Long)'.
     */
    public void testEntityNotFoundException() {
        EntityNotFoundException e = new EntityNotFoundException(User.class, 1L);
        assert e.getMessages() != null;
        assert e.getErrorCode().equals(SystemErrorCode.ENTITY_NOT_FOUND);
        assert e.getCause() == null;
    }

    /**
     * Test method for 'EntityNotFoundException(Throwable t)'.
     */
    public void testEntityNotFoundExceptionThrowable() {
        Throwable t = new Throwable();
        EntityNotFoundException e = new EntityNotFoundException(t);
        assert e.getMessages() != null;
        assert e.getErrorCode().equals(SystemErrorCode.ENTITY_NOT_FOUND);
        assert e.getCause() == t;
    }

}
