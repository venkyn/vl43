/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.exceptions;

import com.vocollect.epp.errors.UserMessage;
import com.vocollect.epp.logging.LoggingError;

import static com.vocollect.epp.test.TestGroups.EXCEPTIONS;
import static com.vocollect.epp.test.TestGroups.FAST;


import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Test class for the DatabaseLockException class.
 *
 * @author Dennis Doubleday
 */
@Test(groups = { FAST, EXCEPTIONS })
public class DatabaseLockExceptionTest {

    /**
     * Test method for 'DatabaseLockException()'.
     */
    public void testDatabaseLockException() {
        DatabaseLockException e = new DatabaseLockException();
        assert e.getMessages() == null;
        assert e.getErrorCode() == null;
        assert e.getCause() == null;
    }

    /**
     * Test method for
     * 'DatabaseLockException(ErrorCode, UserMessage, Throwable)'.
     */
    public void testDatabaseLockExceptionErrorCodeUserMessageThrowable() {
        DatabaseLockException e = new DatabaseLockException(
            LoggingError.ADD_ZIP_ENTRY_FAILED,
            new UserMessage("test.key", "test"),
            new RuntimeException("test"));
        assert e.getMessages() != null : "non-null messages";
        assertEquals("One message", e.getMessages().getSize(), 1);
        assertEquals("Exception message", e.getCause().getMessage(), "test");
        assertEquals("Bad error code", e.getErrorCode(),
                      LoggingError.ADD_ZIP_ENTRY_FAILED);

    }
}
