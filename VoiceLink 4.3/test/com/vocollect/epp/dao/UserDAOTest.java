/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.chart.dao.ChartDAO;
import com.vocollect.epp.chart.model.Chart;
import com.vocollect.epp.chart.model.UserChartPreference;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.DatabaseConstraintException;
import com.vocollect.epp.dashboard.dao.DashboardDAO;
import com.vocollect.epp.dashboard.model.Dashboard;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.model.User;
import com.vocollect.epp.model.UserProperty;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.testng.annotations.Test;

/**
 * Tests for UserDAO.
 *
 * @author ddoubleday
 */
@Test(groups = { DAO, FAST, SECURITY })
public class UserDAOTest extends BaseDAOTestCase {

    private UserDAO dao = null;

    private RoleDAO rdao = null;
    
    private ChartDAO chartDao = null;
    
    private DashboardDAO dashboardDao = null;

    private final String adminAcct = "admin";

    private final String testPwd = "testpass";

    private final String testUser = "scott4";

    private final String testProp = "testproperty";

    private final String testVal = "testvalue";

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/chart-data.xml");
        adapter.handleFlatXmlResource("data/dbunit/dashboard-data.xml");
        setUpSiteContext(false);
    }

    /**
     * Test method for 'com.vocollect.epp.dao.UserDAO.setUserDAO(UserDAO dao)'.
     * @param userDAO the user data access object
     */
    @Test(enabled = false)
    public void setUserDAO(UserDAO userDAO) {
        this.dao = userDAO;
    }

    /**
     * Test method for 'com.vocollect.epp.dao.UserDAO.setRoleDAO(RoleDAO rdao)'.
     * @param roleDAO role data access object
     */
    @Test(enabled = false)
    public void setRoleDAO(RoleDAO roleDAO) {
        this.rdao = roleDAO;
    }
    
    /**
     * Setter method for chart dao.
     * @param chartDAO chart data access object
     */
    @Test(enabled = false)
    public void setChartDAO(ChartDAO chartDAO) {
        this.chartDao = chartDAO;
    }
    
    /**
     * Setter method for dashboard dao.
     * @param dashboardDAO dashboard data access object
     */
    @Test(enabled = false)
    public void setDashboardDAO(DashboardDAO dashboardDAO) {
        this.dashboardDao = dashboardDAO;
    }

    /**
     * Test method for 'com.vocollect.epp.dao.UserDAO.getUser(String user)'.
     * @throws Exception for any error
     */
    public void testGetUserInvalid() throws Exception {
        User user = dao.findByName("badusername");
        if (user != null) {
            fail("'badusername' found in database, failing test...");
        }
    }

    /**
     * Test method for 'com.vocollect.epp.dao.UserDAO.getUser(String user)'.
     * @throws Exception for any error
     */
    @Test()
    public void testGetUser() throws Exception {
        final long adminUserID = -1;
        User user = dao.findByName(adminAcct);
        assertNotNull(user);
        assertEquals(1, user.getRoles().size());
        assertTrue(user.getName().equals(adminAcct));
        user = null;
        user = dao.get(adminUserID);
        assertNotNull(user);
        assertEquals(1, user.getRoles().size());
        assertTrue(user.getName().equals(adminAcct));
    }

    /**
     * Test method for 'com.vocollect.epp.dao.UserDAO.saveUser(User user)'.
     * @throws Exception for any error
     */
    // @Test(dependsOnMethods = { "testAddAndRemoveOneUser" })
    @Test()
    public void testUpdateUser() throws Exception {
        User user = dao.findByName(adminAcct);

        user.setEmailAddress("tomcat@new.com");
        user.setEnabled(true);
        user.setPassword("newpass");
        user.setLastLoginLocation("newlocation");
        Date loginTime = new Date();
        user.setLastLoginTime(loginTime);
        user.setNotes("newnotes");

        UserProperty userProperty = new UserProperty();
        Set<UserProperty> userProperties = new HashSet<UserProperty>();
        userProperty.setName("Report Format");
        userProperty.setValue("PDF");
        userProperties.add(userProperty);
        user.setUserProperties(userProperties);

        dao.save(user);

        user = dao.findByName(adminAcct);

        // make sure all of the fields we updated were saved
        assertEquals("tomcat@new.com", user.getEmailAddress(), "new email");
        assertTrue(user.isEnabled(), "is enabled");
        assertTrue(user.getPassword().equals("newpass"), "new password");
        assertEquals("newlocation", user.getLastLoginLocation(), "new location");
        assertEquals("newnotes", user.getNotes(), "new notes");
        assertTrue(user.getLastLoginTime().equals(loginTime), "new login time");
        assertTrue(
            user.getUserProperties().equals(userProperties), "new user property");

        // verify that constraint violation occurs when adding new user
        // with same name
        String name = user.getName();
        user = new User();
        user.setName(name);
        user.setPassword(name);
        user.addRole(rdao.findByName("User"));

        try {
            dao.save(user);
            fail("saveUser didn't throw DatabaseConstraintException");
        } catch (DatabaseConstraintException e) {
            assertNotNull(e);
            log.debug("expected exception: " + e.getMessage());
        }

    }

    /**
     * Test method for 'com.vocollect.epp.dao.UserDAO.addRole(Role role)'.
     * @throws Exception for any error
     */
    @Test()
    public void testAddUserRole() throws Exception {
        User user = dao.findByName(adminAcct);

        assertEquals(1, user.getRoles().size());

        Role role = rdao.findByName(RoleDAOTest.READONLY_ROLE_NAME);
        user.addRole(role);
        dao.save(user);
        assertEquals(2, user.getRoles().size());

        // add the same role twice - should result in no additional role
        user.addRole(role);
        assertEquals(2, user.getRoles().size());

        dao.save(user);

        assertEquals(user.getRoles().size(), 2, "more than 2 roles");

        user.getRoles().remove(role);
        dao.save(user);

        assertEquals(1, user.getRoles().size());
    }

    /**
     * Test method for 'com.vocollect.epp.dao.UserDAO.removeUser(User user)'.
     * @throws Exception for any error
     */
    // @Test(dependsOnMethods = { "testAddAndRemoveOneUser" })
    @Test()
    public void testAddAndRemoveUser() throws Exception {
        // test user with all fields
        User user = new User();
        user.setName("testuserd");
        user.setEnabled(true);
        user.setPassword(testPwd);
        user.setEmailAddress("testuserd@vocollect.com");
        user.addRole(rdao.findByName(RoleDAOTest.ADMIN_ROLE_NAME));
        user.setLastLoginLocation("location");
        Date loginTime = new Date();
        user.setLastLoginTime(loginTime);
        user.setNotes("notes");
        user.setAllSitesAccess(true);
        user.setCurrentSite(-1L);

        UserProperty userProperty = new UserProperty();
        Set<UserProperty> userProperties = new HashSet<UserProperty>();
        userProperty.setName("testproperty");
        userProperty.setValue("testvalue");
        userProperties.add(userProperty);
        user.setUserProperties(userProperties);

        dao.save(user);

        assertNotNull(user.getName());
        assertEquals(testPwd, user.getPassword());

        dao.delete(user.getId());

        user = dao.findByName("testuserd");
        if (user != null) {
            fail("found deleted user testuser");
        }
    }

    /**
     * Test method for 'com.vocollect.epp.dao.UserDAO.saveUser(User user)'.
     * @throws Exception for any error
     */
    @Test()
    public void testAddUserNoName() throws Exception {
        // test user with no user name. Should throw exception
        try {
            User user = new User();
            // no name -- user.setName("scott2");
            user.setEnabled(true);
            user.setPassword(testPwd);
            user.setEmailAddress("testuserd@vocollect.com");
            user.addRole(rdao.findByName(RoleDAOTest.ADMIN_ROLE_NAME));
            user.setLastLoginLocation("location");
            Date loginTime = new Date();
            user.setLastLoginTime(loginTime);
            user.setNotes("notes");
            user.setAllSitesAccess(false);
            user.setCurrentSite(-1L);

            UserProperty userProperty = new UserProperty();
            Set<UserProperty> userProperties = new HashSet<UserProperty>();
            userProperty.setName("testproperty");
            userProperty.setValue("testvalue");
            userProperties.add(userProperty);
            user.setUserProperties(userProperties);

            dao.save(user);
            fail("testAddUserNoName should have thrown an exception");
        } catch (DataAccessException e) {
            // this is the expected exception
            log.info("expected exception in userDAOTest.testAddUserNoName");
        }
    }

    /**
     * Test method for adding and deleting a user. Tests
     * 'com.vocollect.epp.dao.UserDAO.saveUser(User user)' and
     * 'com.vocollect.epp.dao.UserDAO.removeUser(User user)'
     *
     * @throws Exception
     */
    // @Test
    @Test()
    public void testAddAndRemoveOneUser() throws Exception {
        testAddOneUser();
        testRemoveOneUser();
    }

    /**
     * Test method for 'com.vocollect.epp.dao.UserDAO.saveUser(User user)'.
     * @throws Exception for any error
     */
    @Test()
    public void testAddOneUser() throws Exception {
        // Add the user separately so that it can be deleted
        // within a separate method that calls remove.
        User user = new User();
        user.setName(testUser);
        user.setEnabled(true);
        user.setPassword(testPwd);
        // user.setEmailAddress("testuserd@vocollect.com");
        // user.addRole(rdao.findByName(RoleDAOTest.ADMIN_ROLE_NAME));
        // user.setLastLoginLocation("location");
        // Date loginTime = new Date();
        // user.setLastLoginTime(loginTime);
        user.setNotes("notes");
        user.setAllSitesAccess(false);
        user.setCurrentSite(-1L);

        UserProperty userProperty = new UserProperty();
        Set<UserProperty> userProperties = new HashSet<UserProperty>();
        userProperty.setName("testproperty");
        userProperty.setValue("testvalue");
        userProperties.add(userProperty);
        user.setUserProperties(userProperties);

        // Thread.sleep(30000);
        dao.save(user);
    }

    /**
     * Test method for 'com.vocollect.epp.dao.UserDAO.removeUser(User user)'.
     * @throws Exception for any error
     */
    @Test()
   public void testRemoveOneUser() throws Exception {
        // the AddOneUser method will run and independently save a
        // user to the database. This method will remove that same user
        // But since it's not within the same method, this should require
        // a trip to the database to insure that it reads the user again.

        User user = dao.findByName(testUser);
        if (user == null) {
            testAddOneUser();
            user = dao.findByName(testUser);
        }
        dao.delete(user.getId());
    }

    /**
     * @throws Exception on any error
     */
    @Test()
    public void testCountAdministrators() throws Exception {
        Number adminCount = dao.countDatabaseAuthenticatedAdministrators();
        assert adminCount.intValue() != 0 : "No administrators returned";
    }

    /**
     * Tests the getPropertyByName method in the UserDAO.
     * @throws Exception
     */
    public void testGetPropertyByName() throws Exception {

        User user = new User();
        user.setName(testUser);
        user.setEnabled(true);
        user.setPassword(testPwd);

        user.setNotes("notes");
        user.setAllSitesAccess(false);
        user.setCurrentSite(-1L);

        UserProperty userProperty = new UserProperty();
        Set<UserProperty> userProperties = new HashSet<UserProperty>();
        userProperty.setName(testProp);
        userProperty.setValue(testVal);
        userProperties.add(userProperty);
        user.setUserProperties(userProperties);

        dao.save(user);

        UserProperty prop = dao.getPropertyByName(testUser, testProp);
        assertEquals(testVal, prop.getValue());

        dao.delete(user.getId());
    }
    
    /**
     * Test saving of chart preference.
     * @throws Exception on error
     */
    public void testSaveChartPreference() throws Exception {
        User user = new User();
        user.setName("chartUser");
        user.setEnabled(true);
        user.setPassword(testPwd);
        
        user.setNotes("notes");
        user.setAllSitesAccess(false);
        user.setCurrentSite(-1L);
        
        Dashboard dashboard = dashboardDao.get(-1L);
        Chart chart = chartDao.get(-1L);
        
        
        UserChartPreference chartPreference = new UserChartPreference();
        chartPreference.setChart(chart);
        chartPreference.setDashboard(dashboard);
        chartPreference.setChartWidth(10.0F);
        chartPreference.setChartHeight(10.0F);
        
        
        Set<UserChartPreference> chartPreferences = new HashSet<UserChartPreference>();
        chartPreferences.add(chartPreference);
        user.setChartPreference(chartPreferences);
        
        dao.save(user);
        
        dao.delete(user);
        
    }
}
