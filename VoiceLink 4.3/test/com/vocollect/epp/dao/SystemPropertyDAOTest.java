/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;


import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.SystemProperty;
import com.vocollect.epp.service.SystemPropertyManager;

import java.util.Iterator;
import java.util.List;

import org.testng.annotations.Test;

/**
 * SystemPropertyDAO test cases.
 *
 * @author ??
 */
public class SystemPropertyDAOTest extends BaseDAOTestCase {
    private SystemProperty systemProperty = new SystemProperty();
    private SystemPropertyManager systemPropertyManager;

    /**
     * @param systemPropertyManager .
     */
    @Test(enabled = false)
    public void setSystemPropertyManager(SystemPropertyManager systemPropertyManager) {
        this.systemPropertyManager = systemPropertyManager;
    }

    /**
     * @throws Exception
     */
    @Test(groups = { "dev" })
    public void testAddRetrieveSysProp() throws Exception {
        try {
            systemProperty = new SystemProperty();
            systemProperty.setName("host");
            List<SystemProperty> sList = systemPropertyManager.queryByExample(systemProperty);
            Iterator<SystemProperty> sIter = sList.iterator();

            System.out.println("**********************************");
            while (sIter.hasNext()) {
                systemProperty = sIter.next();
                System.out.println(systemProperty.getValue());
            }
            System.out.println("**********************************");

            systemProperty.setValue("Ex02.vocollect.int");
            systemPropertyManager.save(systemProperty);

            Iterator<SystemProperty> systemPropertyListIterator =
                systemPropertyManager.getAll().iterator();
            while (systemPropertyListIterator.hasNext()) {
                systemProperty = systemPropertyListIterator.next();
                System.out.println(systemProperty.getName() + ": " + systemProperty.getValue());
            }
            System.out.println("------------------------------------------");
        } catch (DataAccessException e) {
            // this is the expected exception
            System.out.println("expected exception in systemPropertyDAOTest.testAddRetrieveNotification");
            log.info("expected exception in systemPropertyDAOTest.testAddRetrieveNotification");
            e.printStackTrace();
        }
    }
}
