/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.model.DataTranslation;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import org.testng.annotations.Test;


/**
 * Tests for DataTranslationDAO.
 *
 * @author dkertis/jtauberg
 */

@Test(groups = { DAO, FAST, SECURITY })

public class DataTranslationDAOTest extends BaseDAOTestCase {
    private DataTranslationDAO dao = null;



    /**
     * This creates a dataTralslation, sets key, locale, and translation and saves it.
     * @throws Exception
     */
    @Test(enabled = false)
    public void testAddDT() throws Exception {
        DataTranslation dt = new DataTranslation();
        dt.setKey("Jimmer");
        dt.setLocale("en");
        dt.setTranslation("Astronomer");
        dao.save(dt);
    }
}
