/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dao.exceptions.DatabaseConstraintException;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import org.testng.annotations.Test;

/**
 * Test cases for the RoleDAO.
 *
 * @author ddoubleday
 */
@Test(groups = { DAO, SECURITY, FAST })
public class RoleDAOTest extends BaseDAOTestCase {

    /**
     * The name of the Administrator role, which is not changeable or
     * removable.
     */
    public static final String ADMIN_ROLE_NAME = "Administrator";

    /**
     * The default name of the User role.
     */
    public static final String USER_ROLE_NAME = "User";

    /**
     * The default name of the User role.
     */
    public static final String READONLY_ROLE_NAME = "Read-Only";

    private RoleDAO dao  = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * Called by the framework to set the RoleDAO object.
     * @param roleDAO the data access object
     */
    @Test(enabled = false)
    public void setRoleDAO(RoleDAO roleDAO) {
        this.dao = roleDAO;
    }

    /**
     * Test method for 'roleDAO.getRole(String rolename)'.
     * @throws Exception for any error
     */
    public void testGetRoleInvalid() throws Exception {
        Role role = dao.findByName("badrolename");
        assertNull(role);
    }

    /**
     * Test method for 'roleDAO.getRole(String rolename)'.
     * @throws Exception for any error
     */
    public void testGetRole() throws Exception {
        final Long adminRoleID = -1L;

        Role role = dao.findByName(ADMIN_ROLE_NAME);
        assertNotNull(role);
        assertTrue(ADMIN_ROLE_NAME.equals(role.getName()), "role names not equal");
        role = dao.get(adminRoleID);
        assertNotNull(role);
    }

    /**
     * Test method for 'roleDAO.saveRole()'.
     * @throws Exception for any error
     */
    @Test(dependsOnMethods = { "testAddAndRemoveRole", "testAddOneRole" })
    public void testUpdateRole() throws Exception {
        // The dependsOn is so this will run after those methods.
        // For
        Role role = dao.findByName(ADMIN_ROLE_NAME);
        log.info(role);
        role.setDescription("test descr");

        dao.save(role);
        assertEquals(role.getDescription(), "test descr");

        // verify that constraint violation occurs when adding new role
        // with same name
        String name = role.getName();
        role = new Role();
        role.setName(name);

        try {
            dao.save(role);
            fail("saveRole didn't throw DatabaseConstraintException");
        } catch (DatabaseConstraintException e) {
            assertNotNull(e);
            log.debug("expected exception: " + e.getMessage());
        }

    }

    /**
     * Test method for 'roleDAO.removeRole()'.
     * @throws Exception for any error
     */
    public void testAddAndRemoveRole() throws Exception {

        final String roleName = "testAddAndRemoveRole";

        Role checkRole = dao.findByName(roleName);
        if (checkRole != null) {
            fail("Role " + roleName + " already exists");
        }
        Role role = new Role();
        role.setName(roleName);
        role.setDescription("new role descr");
        role.setIsAdministrative(false);

        dao.save(role);
        assertNotNull(role.getName());

        dao.delete(role);

        role = dao.findByName(roleName);
        assertNull(role);
    }

    /**
     * Test method for 'roleDAO.addRole()'.
     * @throws Exception for any error
     */
    public void testAddRoleNoName() throws Exception {
        // adding a role without a name should throw an execption
        try {
            Role role = new Role();
            // no name -- role.setName("testrole");
            role.setDescription("new role descr");
            role.setIsAdministrative(false);

            dao.save(role);
            fail("testAddRoleNoName should have thrown an exception");
        } catch (DataAccessException e) {
            // this is the expected exception
            log.info("expected exception in roleDAOTest.testAddRoleNoName");
        }
    }

    /**
     * Test method for 'com.vocollect.epp.dao.RoleDAO.saveRole(Role role)'.
     * @throws Exception for any error
     */
    public void testAddOneRole() throws Exception {
        Role role = new Role();
        role.setName("roleToRemove");
        role.setDescription("new role descr");
        role.setIsAdministrative(false);

        dao.save(role);
    }

    /**
     * Test method for 'com.vocollect.epp.dao.RoleDAO.removeRole(Role role)'.
     * @throws Exception for any error
     */
    public void testRemoveOneRole() throws Exception {
        Role role = dao.findByName(READONLY_ROLE_NAME);
        dao.delete(role);
    }
}
