/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.model;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
/**
 * 
 * 
 * @author smittal
 */
@Test(groups = { FAST, MODEL, DA })
public class DAColumnTest {

    /**
     * . Test the hash code generates equal number
     */
    @Test()
    public void testHashCodeEqual() {
        DAColumn daColumn1 = new DAColumn();
        DAColumn daColumn2 = new DAColumn();

        daColumn1.setFieldId("field1");
        daColumn1.setFieldType(DAFieldType.INTEGER);
        daColumn1.setDisplayName("field1");
        daColumn1.setIdentityColumn(true);
        daColumn1.setUom("uom");
        daColumn1.setColumnSequence(1);
        daColumn1.setColumnState(DAColumnState.UNCHANGED);

        daColumn2.setFieldId("field1");
        daColumn2.setFieldType(DAFieldType.INTEGER);
        daColumn2.setDisplayName("field1");
        daColumn2.setIdentityColumn(true);
        daColumn2.setUom("uom");
        daColumn2.setColumnSequence(1);
        daColumn2.setColumnState(DAColumnState.UNCHANGED);

        assertEquals("Hash codes are not same", daColumn2.hashCode(),
            daColumn1.hashCode());
    }

    /**
     * . Test the hash code generates different number
     */
    @Test()
    public void testHashCodeDifferent() {
        DAColumn daColumn1 = new DAColumn();
        DAColumn daColumn2 = new DAColumn();

        daColumn1.setFieldId("field1");
        daColumn1.setFieldType(DAFieldType.INTEGER);
        daColumn1.setDisplayName("field1");
        daColumn1.setIdentityColumn(true);
        daColumn1.setUom("uom");
        daColumn1.setColumnSequence(1);
        daColumn1.setColumnState(DAColumnState.UNCHANGED);

        daColumn2.setFieldId("field2");
        daColumn2.setFieldType(DAFieldType.INTEGER);
        daColumn2.setDisplayName("field1");
        daColumn2.setIdentityColumn(true);
        daColumn2.setUom("uom");
        daColumn2.setColumnSequence(1);
        daColumn2.setColumnState(DAColumnState.UNCHANGED);

        assertEquals("Hash code should be different",
            daColumn1.hashCode() != daColumn2.hashCode(), true);
    }

    /**
     * . Tests two types are equal
     */
    @Test()
    public void testEqual() {
        DAColumn daColumn1 = new DAColumn();
        DAColumn daColumn2 = new DAColumn();

        daColumn1.setFieldId("field1");
        daColumn1.setFieldType(DAFieldType.INTEGER);
        daColumn1.setDisplayName("field1");
        daColumn1.setIdentityColumn(true);
        daColumn1.setUom("uom");
        daColumn1.setColumnSequence(1);
        daColumn1.setColumnState(DAColumnState.UNCHANGED);

        daColumn2.setFieldId("field1");
        daColumn2.setFieldType(DAFieldType.INTEGER);
        daColumn2.setDisplayName("field1");
        daColumn2.setIdentityColumn(true);
        daColumn2.setUom("uom");
        daColumn2.setColumnSequence(1);
        daColumn2.setColumnState(DAColumnState.UNCHANGED);

        assertEquals("Types should be equal", daColumn1.equals(daColumn2), true);
    }

    /**
     * . Tests two types are not equal
     */
    @Test()
    public void testNotEqual() {
        DAColumn daColumn1 = new DAColumn();
        DAColumn daColumn2 = new DAColumn();

        daColumn1.setFieldId("field1");
        daColumn1.setFieldType(DAFieldType.INTEGER);
        daColumn1.setDisplayName("field1");
        daColumn1.setIdentityColumn(true);
        daColumn1.setUom("uom");
        daColumn1.setColumnSequence(1);
        daColumn1.setColumnState(DAColumnState.UNCHANGED);

        // When compared with other object
        Object obj = new Object();

        assertEquals("Objects should not be equal", daColumn1.equals(obj),
            false);

        // Name of one daColumn is null
        daColumn2.setFieldId(null);
        daColumn2.setFieldType(DAFieldType.INTEGER);
        daColumn2.setDisplayName("field1");
        daColumn2.setIdentityColumn(true);
        daColumn2.setUom("uom");
        daColumn2.setColumnSequence(1);
        daColumn2.setColumnState(DAColumnState.UNCHANGED);

        assertEquals("Types should not be equal", daColumn1.equals(daColumn2),
            false);

        // When compared with different daColumn
        daColumn2.setFieldId("field2");
        daColumn2.setFieldType(DAFieldType.INTEGER);
        daColumn2.setDisplayName("field1");
        daColumn2.setIdentityColumn(true);
        daColumn2.setUom("uom");
        daColumn2.setColumnSequence(1);
        daColumn2.setColumnState(DAColumnState.UNCHANGED);

        assertEquals("Types should not be equal", daColumn1.equals(daColumn2),
            false);
    }
}
