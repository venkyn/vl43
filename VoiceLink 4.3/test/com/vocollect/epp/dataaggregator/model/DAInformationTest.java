/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.model;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.MODEL;

import java.util.Set;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * 
 * 
 * @author smittal
 */
@Test(groups = { FAST, MODEL, DA }, enabled = false)
public class DAInformationTest {

    private Set<DAColumn> columns = null;

    /**
     * . Test the hash code generates equal number
     */
    @Test()
    public void testHashCodeEqual() {
        DAInformation daInformation1 = new DAInformation();
        DAInformation daInformation2 = new DAInformation();

        daInformation1.setName("info1");
        daInformation1.setDisplayName("info");
        daInformation1.setColumns(columns);
        daInformation1.setMarkedForDelete(false);

        daInformation2.setName("info1");
        daInformation2.setDisplayName("info");
        daInformation2.setColumns(columns);
        daInformation2.setMarkedForDelete(false);

        assertEquals("Hash codes are not same", daInformation2.hashCode(),
            daInformation1.hashCode());
    }

    /**
     * . Test the hash code generates different number
     */
    @Test()
    public void testHashCodeDifferent() {
        DAInformation daInformation1 = new DAInformation();
        DAInformation daInformation2 = new DAInformation();

        daInformation1.setName("info1");
        daInformation1.setDisplayName("info");
        daInformation1.setColumns(columns);
        daInformation1.setMarkedForDelete(false);
        
        daInformation2.setName("info2");
        daInformation2.setDisplayName("info");
        daInformation2.setColumns(columns);
        daInformation2.setMarkedForDelete(false);

        assertEquals("Hash code should be different",
            daInformation1.hashCode() != daInformation2.hashCode(), true);
    }

    /**
     * . Tests two types are equal
     */
    @Test()
    public void testEqual() {
        DAInformation daInformation1 = new DAInformation();
        DAInformation daInformation2 = new DAInformation();

        daInformation1.setName("info1");
        daInformation1.setDisplayName("info");
        daInformation1.setColumns(columns);
        daInformation1.setMarkedForDelete(false);

        daInformation2.setName("info1");
        daInformation2.setDisplayName("info");
        daInformation2.setColumns(columns);
        daInformation2.setMarkedForDelete(false);

        assertEquals("Types should be equal", daInformation1.equals(daInformation2), true);
    }

    /**
     * . Tests two types are not equal
     */
    @Test()
    public void testNotEqual() {
        DAInformation daInformation1 = new DAInformation();
        DAInformation daInformation2 = new DAInformation();

        daInformation1.setName("info1");
        daInformation1.setDisplayName("info");
        daInformation1.setColumns(columns);
        daInformation1.setMarkedForDelete(false);

        // When compared with other object
        Object obj = new Object();

        assertEquals("Objects should not be equal", daInformation1.equals(obj), false);

        // Name of one daInformation is null
        daInformation2.setName(null);
        daInformation2.setDisplayName("info");
        daInformation2.setColumns(columns);
        daInformation2.setMarkedForDelete(false);

        assertEquals("Types should not be equal", daInformation1.equals(daInformation2), false);

        // When compared with different daInformation
        daInformation2.setName("info2");
        daInformation2.setDisplayName("info");
        daInformation2.setColumns(columns);
        daInformation2.setMarkedForDelete(false);

        assertEquals("Types should not be equal", daInformation1.equals(daInformation2), false);
    }

}
