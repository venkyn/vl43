/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator;

import org.testng.Assert;

/**
 * This is the unit test class for Generic Data Aggregator.
 *
 * @author kudupi
 */
public abstract class DataAggregatorInterfaceTest {
    
    public void testGetIdentityColumnName(GenericDataAggregator instance, String str) {
        Assert.assertNotNull(instance.getIdentityColumnName());
        Assert.assertEquals(instance.getIdentityColumnName(), str);
    }
    
    public void testGetAllOutputColumns(GenericDataAggregator instance) {
        Assert.assertNotNull(instance.getAllOutputColumns());
    }
    
    public void testExecute(GenericDataAggregator instance) {
        try {
            Assert.assertNotNull(instance.execute());
        } catch (Exception e) {
            Assert.fail();
        }
    }    
}
