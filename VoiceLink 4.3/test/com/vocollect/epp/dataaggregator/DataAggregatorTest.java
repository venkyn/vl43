/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator;

import com.vocollect.aggregators.TestDataAggregator;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


/**
 * This is the concrete unit test class for Generic Data Aggregator.
 *
 * @author kudupi
 */
@Test(groups = { DA, FAST, SECURITY })
public class DataAggregatorTest extends DataAggregatorInterfaceTest {
    
    private TestDataAggregator testDataAggregator = null;
    
    @BeforeTest
    public void testSetup() {
        testDataAggregator = new TestDataAggregator();
    }
    
    @Test(enabled = true)
    public void testGetIdentityColumnName() {
        super.testGetIdentityColumnName(testDataAggregator, "column0");
    }
    
    @Test(enabled = true)
    public void testGetAllOutputColumns() {
        super.testGetAllOutputColumns(testDataAggregator);
    }
    
    @Test(enabled = true)
    public void testExecute() {
        super.testExecute(testDataAggregator);
    } 
    
}
