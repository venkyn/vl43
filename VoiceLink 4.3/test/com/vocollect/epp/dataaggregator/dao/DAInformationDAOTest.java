/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.dao;

import com.vocollect.epp.dao.BaseDAOTestCase;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAColumnState;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.annotations.Test;

/**
 * Tests for Data Aggregator DAO.
 * Unit tests are disabled as logically
 * they run correctly when individual files are run but at package level or
 * build level they cause other unit tests referencing to DAs to fail because of
 * events raised by DAs on every applicationContext event and how they are
 * handled. 
 * @author smittal
 */
@Test(groups = { DA, DAO, FAST, SECURITY }, enabled = true)
public class DAInformationDAOTest extends BaseDAOTestCase {

    private DAInformationDAO dao = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/da-data.xml");
        setUpSiteContext(false);
    }

    /**
     * @param daInformationDAO the data aggregator information data access
     *            object
     */
    @Test(enabled = false)
    public void setDaInformationDAO(DAInformationDAO daInformationDAO) {
        this.dao = daInformationDAO;
    }

    /**
     * @throws DataAccessException
     * 
     */
    @Test(enabled = false)
    public void testCreateDAInformation() throws DataAccessException {
        DAInformation daInfo = new DAInformation();
        daInfo.setName("TestDA");
        daInfo.setDisplayName("Test DA");
        daInfo.setMarkedForDelete(false);

        DAColumn column1 = new DAColumn();
        column1.setFieldId("test1");
        column1.setDisplayName("test 1");
        column1.setFieldType(DAFieldType.STRING);
        column1.setIdentityColumn(true);
        column1.setColumnSequence(6);
        column1.setDaInformation(daInfo);
        column1.setColumnState(DAColumnState.UNCHANGED);
        
        DAColumn column2 = new DAColumn();
        column2.setFieldId("test2");
        column2.setDisplayName("test 2");
        column2.setFieldType(DAFieldType.TIME);
        column2.setIdentityColumn(false);
        column2.setColumnSequence(7);
        column2.setDaInformation(daInfo);
        column2.setColumnState(DAColumnState.UNCHANGED);
        
        Set<DAColumn> columns = new HashSet<DAColumn>();
        columns.add(column1);
        columns.add(column2);

        daInfo.setColumns(columns);

        this.dao.save(daInfo);
        List<DAInformation> daInfos = this.dao.getAll();

        assertNotNull(daInfos);
        assertTrue(daInfos.contains(daInfo));
        

    }

    /**
     * @throws DataAccessException
     * 
     */
    @Test(enabled = false)
    public void testFindByDAName() throws DataAccessException {
        
        DAInformation result = this.dao.findByName("DataAggregatorTest");

        assertNotNull(result);
        assertEquals(result.getColumns().size(), 5);

    }
    
    /**
     * @throws DataAccessException
     * 
     */
    @Test(enabled = false)
    public void testUpdateDeleteMarked() throws DataAccessException {
        DAInformation daInfo = new DAInformation();
        daInfo.setName("TestDADelete");
        daInfo.setDisplayName("TestDADelete");
        daInfo.setMarkedForDelete(false);
        
        DAColumn column1 = new DAColumn();
        column1.setFieldId("test1");
        column1.setDisplayName("test 1");
        column1.setFieldType(DAFieldType.STRING);
        column1.setIdentityColumn(true);
        column1.setColumnSequence(8);
        column1.setDaInformation(daInfo);
        column1.setColumnState(DAColumnState.UNCHANGED);
        
        DAColumn column2 = new DAColumn();
        column2.setFieldId("test2");
        column2.setDisplayName("test 2");
        column2.setFieldType(DAFieldType.TIME);
        column2.setIdentityColumn(false);
        column2.setColumnSequence(9);
        column2.setDaInformation(daInfo);
        column2.setColumnState(DAColumnState.UNCHANGED);
        
        Set<DAColumn> columns = new HashSet<DAColumn>();
        columns.add(column1);
        columns.add(column2);

        daInfo.setColumns(columns);

        this.dao.save(daInfo);
        
        DAInformation result = this.dao.findByName("TestDADelete");

        assertNotNull(result);
        assertEquals(result.getColumns().size(), 2);
        
        daInfo.setMarkedForDelete(true);
        this.dao.save(daInfo);
        
        this.dao.updateDeleteMarked();
        
        result = this.dao.findByName("TestDADelete");

        assertNull(result);

    }

}
