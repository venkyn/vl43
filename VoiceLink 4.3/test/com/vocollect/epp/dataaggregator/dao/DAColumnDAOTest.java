/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.dao;

import com.vocollect.epp.dao.BaseDAOTestCase;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAColumnState;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.dataaggregator.service.DAInformationManager;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.DAO;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.util.Set;

import org.testng.annotations.Test;

/**
 * Tests for Data Aggregator Column DAO. 
 * Unit tests are disabled as logically
 * they run correctly when individual files are run but at package level or
 * build level they cause other unit tests referencing to DAs to fail because of
 * events raised by DAs on every applicationContext event and how they are
 * handled.
 * @author smittal
 */
@Test(groups = { DA, DAO, FAST, SECURITY }, enabled = true)
public class DAColumnDAOTest extends BaseDAOTestCase {

    private DAColumnDAO daColumnDAO = null;

    private DAInformationManager daInformationManager = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/da-data.xml");
    }

    /**
     * Getter for the daColumnDAO property.
     * @return DAColumnDAO value of the property
     */
    @Test(enabled = false)
    public DAColumnDAO getDaColumnDAO() {
        return daColumnDAO;
    }

    /**
     * Setter for the daColumnDAO property.
     * @param daColumnDAO the new daColumnDAO value
     */
    @Test(enabled = false)
    public void setDaColumnDAO(DAColumnDAO daColumnDAO) {
        this.daColumnDAO = daColumnDAO;
    }

    /**
     * Getter for the daInformationManager property.
     * @return DAInformationManager value of the property
     */
    @Test(enabled = false)
    public DAInformationManager getDaInformationManager() {
        return daInformationManager;
    }

    /**
     * Setter for the daInformationManager property.
     * @param daInformationManager the new daInformationManager value
     */
    @Test(enabled = false)
    public void setDaInformationManager(DAInformationManager daInformationManager) {
        this.daInformationManager = daInformationManager;
    }

    /**
     * @throws DataAccessException
     * @throws BusinessRuleException
     * 
     */
    @Test(enabled = false)
    public void testUpdateColumnState() throws DataAccessException,
        BusinessRuleException {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.daColumnDAO.updateColumnState(DAColumnState.DISPLAYNAMEUPDATED);

        DAInformation daInfo = this.daInformationManager.get((long) -101);
        Set<DAColumn> daColumns = daInfo.getColumns();

        assertNotNull(daColumns);

        assertEquals(daColumns.size(), 5);

        int count = 0;

        for (DAColumn daColumn : daColumns) {
            if (daColumn.getColumnState() != DAColumnState.UNCHANGED) {
                count++;
            }
        }

        assertEquals(count, 5);

        this.daColumnDAO.updateColumnState(DAColumnState.UNCHANGED);

        this.daInformationManager.getPrimaryDAO().clearSession();

        daInfo = this.daInformationManager.get((long) -101);
        daColumns = daInfo.getColumns();

        assertNotNull(daColumns);
        assertEquals(daColumns.size(), 5);

        count = 0;
        for (DAColumn daColumn : daColumns) {
            if (daColumn.getColumnState() != DAColumnState.UNCHANGED) {
                count++;
            }
        }
        assertEquals(count, 0);

    }

    /**
     * @throws DataAccessException
     * @throws BusinessRuleException
     * 
     */
    @Test(enabled = false)
    public void testUpdateDeleteByState() throws DataAccessException,
        BusinessRuleException {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        DAInformation daInfo = this.daInformationManager.get((long) -101);

        Set<DAColumn> daColumns = daInfo.getColumns();

        assertNotNull(daColumns);
        assertEquals(daColumns.size(), 5);

        DAColumn newColumn = new DAColumn();
        newColumn.setFieldId("newColumn");
        newColumn.setDisplayName("newColumn");
        newColumn.setFieldType(DAFieldType.INTEGER);
        newColumn.setUom("newColumn");
        newColumn.setIdentityColumn(false);
        newColumn.setColumnSequence(6);
        newColumn.setColumnState(DAColumnState.MARKEDFORDELETE);
        newColumn.setDaInformation(daInfo);

        daInfo.getColumns().add(newColumn);

        this.daInformationManager.save(daInfo);

        this.daInformationManager.getPrimaryDAO().clearSession();

        daInfo = this.daInformationManager.get((long) -101);
        daColumns = daInfo.getColumns();

        assertNotNull(daColumns);
        assertEquals(daColumns.size(), 6);

        this.daColumnDAO.updateDeleteByState(DAColumnState.MARKEDFORDELETE);

        this.daInformationManager.getPrimaryDAO().clearSession();

        daInfo = this.daInformationManager.get((long) -101);
        daColumns = daInfo.getColumns();

        assertNotNull(daColumns);

        // Remember, system aggregators are now configured in install data
        assertEquals(daColumns.size(), 5);
    }
}
