/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.service.impl;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAColumnState;
import com.vocollect.epp.dataaggregator.model.DAFieldType;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.dataaggregator.service.DAColumnManager;
import com.vocollect.epp.dataaggregator.service.DAInformationManager;
import com.vocollect.epp.dataaggregator.util.DAUtils;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.service.impl.BaseServiceTestCase;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.DA;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Unit test class for testing DAColumn manager.
 * Unit tests are disabled as logically
 * they run correctly when individual files are run but at package level or
 * build level they cause other unit tests referencing to DAs to fail because of
 * events raised by DAs on every applicationContext event and how they are
 * handled.
 * @author smittal
 * 
 */
@Test(groups = { DA }, enabled = true)
public class DAColumnManagerImplTest extends BaseServiceTestCase {

    private DAColumnManager daColumnManager = null;

    private DAInformationManager daInformationManager = null;

    /**
     * @throws Exception
     */
    @BeforeClass
    public void classSetup() throws Exception {

        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/da-data.xml");
    }

    /**
     * Getter for the daColumnManager property.
     * @return DAColumnManager value of the property
     */
    @Test(enabled = false)
    public DAColumnManager getDaColumnManager() {
        return daColumnManager;
    }

    /**
     * Setter for the daColumnManager property.
     * @param daColumnManager the new daColumnManager value
     */
    @Test(enabled = false)
    public void setDaColumnManager(DAColumnManager daColumnManager) {
        this.daColumnManager = daColumnManager;
    }

    /**
     * Getter for the daInformationManager property.
     * @return DAInformationManager value of the property
     */
    @Test(enabled = false)
    public DAInformationManager getDaInformationManager() {
        return daInformationManager;
    }

    /**
     * Setter for the daInformationManager property.
     * @param daInformationManager the new daInformationManager value
     */
    @Test(enabled = false)
    public void setDaInformationManager(DAInformationManager daInformationManager) {
        this.daInformationManager = daInformationManager;
    }

    /**
     * @throws DataAccessException
     * @throws BusinessRuleException
     * 
     */
    @Test(enabled = false)
    public void testUpdateColumnState() throws DataAccessException,
        BusinessRuleException {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.daColumnManager
            .updateColumnState(DAColumnState.DISPLAYNAMEUPDATED);

        DAInformation daInfo = this.daInformationManager.get((long) -101);
        Set<DAColumn> daColumns = daInfo.getColumns();

        assertNotNull(daColumns);

        assertEquals(daColumns.size(), 5);

        int count = 0;

        for (DAColumn daColumn : daColumns) {
            if (daColumn.getColumnState() != DAColumnState.UNCHANGED) {
                count++;
            }
        }

        assertEquals(count, 5);

       
        this.daColumnManager.updateColumnState(DAColumnState.UNCHANGED);
        
        this.daInformationManager.getPrimaryDAO().clearSession();
        
        daInfo = this.daInformationManager.get((long) -101);
        daColumns = daInfo.getColumns();

        assertNotNull(daColumns);
        assertEquals(daColumns.size(), 5);

        count = 0;
        for (DAColumn daColumn : daColumns) {
            if (daColumn.getColumnState() != DAColumnState.UNCHANGED) {
                count++;
            }
        }
        assertEquals(count, 0);

    }

    /**
     * @throws DataAccessException
     * @throws BusinessRuleException
     * 
     */
    @Test(enabled = false)
    public void testUpdateDeleteByState() throws DataAccessException,
        BusinessRuleException {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        DAInformation daInfo = this.daInformationManager.get((long) -101);

        Set<DAColumn> daColumns = daInfo.getColumns();

        assertNotNull(daColumns);
        assertEquals(daColumns.size(), 5);

        DAColumn newColumn = new DAColumn();
        newColumn.setFieldId("newColumn");
        newColumn.setDisplayName("newColumn");
        newColumn.setFieldType(DAFieldType.INTEGER);
        newColumn.setUom("newColumn");
        newColumn.setIdentityColumn(false);
        newColumn.setColumnSequence(6);
        newColumn.setColumnState(DAColumnState.MARKEDFORDELETE);
        newColumn.setDaInformation(daInfo);

        daInfo.getColumns().add(newColumn);

        this.daInformationManager.save(daInfo);
        
        this.daInformationManager.getPrimaryDAO().clearSession();
        
        daInfo = this.daInformationManager.get((long) -101);
        daColumns = daInfo.getColumns();

        assertNotNull(daColumns);
        assertEquals(daColumns.size(), 6);

        this.daColumnManager.updateDeleteByState(DAColumnState.MARKEDFORDELETE);
        
        this.daInformationManager.getPrimaryDAO().clearSession();
        
        daInfo = this.daInformationManager.get((long) -101);
        daColumns = daInfo.getColumns();

        assertNotNull(daColumns);

        // Remember, system aggregators are now configured in install data
        assertEquals(daColumns.size(), 5);
    }
    
    @Test(enabled = false)
    public void testFindDAColumnByFieldType() throws DataAccessException {
        DAInformation daInfo = this.daInformationManager.get((long) -101);
        
        List<DAFieldType> daFieldTypes = new ArrayList<DAFieldType>();
        daFieldTypes.add(DAFieldType.STRING);
        
        List<DAColumn> daColumns = this.daColumnManager.listDAColumnByFieldType(daInfo, daFieldTypes);
        for (DAColumn daColumn : daColumns) {
            assertEquals(DAFieldType.STRING, daColumn.getFieldType());
        }
    }
    
    @Test(enabled = false)
    public void testSequencedDAColumnByFieldType() throws DataAccessException {
        DAInformation daInfo = this.daInformationManager.get((long) -101);

        List<DAColumn> daColumns = this.daColumnManager
            .listSequencedDAColumnByFieldType(daInfo,
                DAUtils.getAllFieldTypes());
        Integer i = 0;
        for (DAColumn daColumn : daColumns) {
            i++;
            assertEquals(i, daColumn.getColumnSequence());
        }
    }
    
    @Test(enabled = false)
    public void testFindDAColumnByFieldID() throws DataAccessException {
        DAInformation daInfo = this.daInformationManager.get((long) -101);
        Set<DAColumn> daColumns = daInfo.getColumns();
        
        for (DAColumn daColumn : daColumns) {
            DAColumn column = this.daColumnManager.findDAColumnByFieldId(daInfo, daColumn.getFieldId());
            assertEquals(column, daColumn);
        }
    }
}
