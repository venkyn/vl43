/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator;

import com.vocollect.epp.dao.BaseDAOTestCase;
import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.dataaggregator.model.DAColumn;
import com.vocollect.epp.dataaggregator.model.DAInformation;
import com.vocollect.epp.dataaggregator.service.DAInformationManager;
import com.vocollect.epp.exceptions.BusinessRuleException;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.DA;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.util.Map;
import java.util.Set;

import org.json.JSONException;
import org.testng.annotations.Test;

/**
 * Tests for UserDAO.
 * 
 * @author khazra
 */
@Test(groups = { DA, FAST, SECURITY })
public class DataAggregatorRegTest extends BaseDAOTestCase {

    private DataAggregatorHandler daHandler = null;

    private DAInformationManager daInformationManager = null;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {
        super.onSetUp();
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * Getter for the daHandler property.
     * @return DataAggregatorHandler value of the property
     */
    @Test(enabled = false)
    public DataAggregatorHandler getDataAggregatorHandler() {
        return daHandler;
    }

    /**
     * Setter for the daHandler property.
     * @param dataAggregatorHandler the new daHandler value
     */
    @Test(enabled = false)
    public void setDataAggregatorHandler(DataAggregatorHandler dataAggregatorHandler) {
        this.daHandler = dataAggregatorHandler;
    }

    /**
     * Getter for the daInformationManager property.
     * @return DAInformationManager value of the property
     */
    @Test(enabled = false)
    public DAInformationManager getDaInformationManager() {
        return daInformationManager;
    }

    /**
     * Setter for the daInformationManager property.
     * @param daInfoManager the new daInformationManager value
     */
    @Test(enabled = false)
    public void setDaInformationManager(DAInformationManager daInfoManager) {
        this.daInformationManager = daInfoManager;
    }

    @Test()
    public void testDataAggregatorBeanRegistered() {

        // Test getALL
        Map<String, GenericDataAggregator> daBeans = this.daHandler.getAll();
        assertNotNull(daBeans);
        assertTrue(daBeans.size() > 0);

        // Test get by name
        GenericDataAggregator testDA = this.daHandler
            .getDataAggregatorByName("testDataAggregator");

        assertNotNull(testDA);
    }

    @Test()
    public void testDataAggregatorBeanPersisted() throws BusinessRuleException,
        DataAccessException, JSONException {
        Map<String, GenericDataAggregator> daBeans = this.daHandler.getAll();

        Set<String> beanNames = daBeans.keySet();

        for (String beanName : beanNames) {
            GenericDataAggregator daBean = daBeans.get(beanName);
            this.daHandler.persistDA(beanName, daBean);
        }

        DAInformation daInfo = this.daInformationManager
            .findByName("testDataAggregator");

        // Check aggregator information is persisted
        assertNotNull(daInfo);
        assertEquals(daInfo.getName(), "testDataAggregator");
        assertEquals(daInfo.getDisplayName(),
            "com.vocollect.aggregators.testDataAggregator");

        // Check aggregator column information is persisted
        Set<DAColumn> daColumns = daInfo.getColumns();
        assertNotNull(daColumns);
        assertEquals(daColumns.size(), 8);

        DAColumn daColumn = (DAColumn) daColumns.toArray()[0];
        assertNotNull(daColumn.getFieldId());

    }
}
