/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

/**
 *
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, WEBSERVICE })
public class UserWebServiceTest extends WebServiceTestCase<UserWebService> {

    /**
     * @return get client handle
     */
    public UserWebService getClient() {
        return getClient(UserWebService.class, "UserService", null);
    }

    /**
     * @throws Exception on any failure
     */
    public void testGetUsers() throws Exception {
        UserWebService client = getClient();
        List<String> users = client.getUserNames();
        int count = client.getUserCount();
        assertEquals(count, users.size(), "userCount");
        assertTrue(users.contains("admin"), "userAdmin");
    }

    /**
     * @throws Exception on any failure
     */
    public void testCreateUser() throws Exception {
        UserWebService client = getClient();
        List<String> roleNames = new ArrayList<String>();
        roleNames.add("Administrator");
        long userId = client.createUser(
            "soapUser", roleNames, "soap@vocollect.com");
        assertTrue(userId > 0, "userId");
        List<String> users = client.getUserNames();
        assertTrue(users.contains("soapUser"), "userExists");


    }

}
