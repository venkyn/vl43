/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service;

import com.vocollect.epp.web.service.impl.LicenseCheckInfo;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

/**
 * Test case for the IntegrityCheckWebService (licensing service).
 *
 * @author ddoubleday
 */
@Test(groups = { FAST, WEBSERVICE })
public class IntegrityCheckWebServiceTest
    extends WebServiceTestCase<IntegrityCheckWebService> {

    /**
     * @return get client handle
     */
    public IntegrityCheckWebService getClient() {
        return getClient(IntegrityCheckWebService.class, "IntegrityCheckService", null);
    }

    /**
     * @throws Exception on any failure
     */
    public void testIntegrityAssert() throws Exception {
        IntegrityCheckWebService client = getClient();
        Map<String, String> licenseInfo = new HashMap<String, String>();
        licenseInfo.put("testProp", "testValue");
        LicenseCheckInfo info = new LicenseCheckInfo();
        info.setProperties(licenseInfo);
        boolean response = client.integrityAssert(info);
        assertFalse(response, "integrityAssert");
        // I can't think of a good way to unit test a true return.
    }

    /**
     * @throws Exception on any failure
     */
    public void testIntegrityRelax() throws Exception {
        IntegrityCheckWebService client = getClient();
        Map<String, String> licenseInfo = new HashMap<String, String>();
        LicenseCheckInfo info = new LicenseCheckInfo();
        info.setProperties(licenseInfo);
        client.integrityRelax(info);
    }

}
