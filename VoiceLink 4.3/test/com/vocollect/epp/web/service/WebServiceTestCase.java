/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service;

import com.vocollect.epp.dao.BaseDAOTestCase;
import com.vocollect.epp.test.DbUnitAdapter;
import com.vocollect.epp.test.DepInjectionSpringContextNGTests;
import com.vocollect.epp.web.service.impl.CXFServiceProxyCreator;

import java.util.Map;
import java.util.Properties;

import org.testng.annotations.BeforeClass;


/**
 * Base class for web services test classes.
 * @param <T> the service class being tested.
 * @author ddoubleday
 */
public class WebServiceTestCase<T> extends DepInjectionSpringContextNGTests {


    private Properties serverProperties;

    /**
     * Constructor.
     */
    public WebServiceTestCase() {
        setAutowireMode(AUTOWIRE_BY_NAME);
    }

    /**
     * Getter for the serverProperties property.
     * @return Properties value of the property
     */
    public Properties getServerProperties() {
        return serverProperties;
    }


    /**
     * Setter for the serverProperties property. Spring-injected.
     * @param serverProperties the new serverProperties value
     */
    public void setServerProperties(Properties serverProperties) {
        this.serverProperties = serverProperties;
    }



    /**
     * The classSetUp method is called at the start of testing of the
     * class that extends from this.
     * @throws Exception on any error.
     */
    @BeforeClass
    protected void classSetUp() throws Exception {
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
    }

    /**
     * @return the Service URI.
     */
    protected String getServiceContextURI() {
        return "http://" + getServerProperties().getProperty("server.name")
            + ":" + getServerProperties().getProperty("server.port")
            + "/" + getServerProperties().getProperty("server.contextRoot");
    }

    /**
     * @return the array of config locations for web services testing.
     */
    public static final String[] getWebServicesTestConfigLocations() {
        String[] daoConfigs = BaseDAOTestCase.EPP_DAO_TEST_CONFIGS;
        String[] configs = new String[daoConfigs.length + 1];
        System.arraycopy(daoConfigs, 0, configs, 0, daoConfigs.length);
        configs[configs.length -  1] =
            "classpath*:/applicationContext-epp-webServices.xml";
        return configs;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.test.DepInjectionSpringContextNGTests#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        return getWebServicesTestConfigLocations();
    }

    /**
     * @param serviceClass the generic service class
     * @param serviceName the service name
     * @param properties service properties (may be null)
     * @return a web services client of the appropriate class
     */
    public T getClient(Class<T>            serviceClass,
                       String              serviceName,
                       Map<String, Object> properties) {

        return new CXFServiceProxyCreator<T>().createProxy(
            serviceClass,
            getServiceContextURI() + "/services/" + serviceName,
            properties);
    }


}
