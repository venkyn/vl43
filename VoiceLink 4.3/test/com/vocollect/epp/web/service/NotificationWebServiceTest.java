/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.service;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.testng.annotations.Test;

/**
 * NoticationWebService test cases.
 *
 * @author ??
 */
@Test(groups = { FAST, WEBSERVICE })
public class NotificationWebServiceTest
    extends WebServiceTestCase<NotificationWebService> {

    /**
     * @return get the client handle.
     */
    public NotificationWebService getClient() {
        return getClient(NotificationWebService.class, "NotificationService", null);
    }

    /**
     * @throws Exception
     */
    public void createNotificationEvent() throws Exception {
        NotificationWebService client = getClient();
        long notId = 1;
        client.createdNotificationEvent(notId);

    }

    /**
     * @throws Exception
     */
    public void getNotificationInfo() throws Exception {

        NotificationWebService client = getClient();
        long notId = 1;
        Map<String, Object> hm = client.getNotificationInfo(notId, "en_US");
        if (hm != null) {
            Set<String> notificationKeys = hm.keySet();
            Iterator<String> it = notificationKeys.iterator();
            while (it.hasNext()) {
                String notification = it.next();
                System.out.println(notification + "  -  " + hm.get(notification));
            }
        }

    }

    /**
     * @throws Exception
     */
    public void ackNotificationInfo() throws Exception {
        NotificationWebService client = getClient();
        Long id = new Long(8);
        String ackUser = "admin";
        String ackDateTime = "11/07/2006";
        String ack = client.acknowledgeNotification(id, ackUser, ackDateTime);

        System.out.println(" Your acknowledge action is " + ack);

    }

}
