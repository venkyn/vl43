/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.model.FileView;
import com.vocollect.epp.service.LogManager;
import com.vocollect.epp.service.impl.LogManagerImpl;
import com.vocollect.epp.util.SystemUtil;
import com.vocollect.epp.util.ZipUtil;

import static com.vocollect.epp.test.TestGroups.ACTION;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


/**
 * Test class for the <code>LogFilesAction</code> class.
 *
 * @author Adam Stein
 */
@Test(groups = { FAST, ACTION, SECURITY }, enabled = true)
public class LogFilesActionTest extends BaseActionTestCase {

    private LogFilesAction action;
    private List<String> testfileContents;
    private List<String> filelist;
    private final String filename = "testfilename";
    private final String logdir = SystemUtil.getSystemLogDirectory();


    /**
     * Setter for the action property.
     * @param newAction the new action value
     */
    @Test(enabled = false)
    public void setLogFilesAction(LogFilesAction newAction) {
        this.action = newAction;
    }

    /**
     * the classSetUp method is called at the start of testing of this class.
     * @throws Exception in case something goes wrong
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        testfileContents = new ArrayList<String>();
        String x = "1,2,3";
        String y = "4,5,6";
        String z = "7,8,9";
        testfileContents.add(x);
        testfileContents.add(y);
        testfileContents.add(z);
        filelist = new ArrayList<String>();
        filelist.add("1234");
        filelist.add("5678");

    }

    /**
     * Test method for 'com.vocollect.epp.web.action.LogFilesAction.getLogManager()'.
     */
    public void testGetManager() {
        LogManager manager = new LogManagerImpl();
        String testMsg = "Testing action.getLogManager() functions";
        action.setLogManager(null);
        assertNull(action.getManager(), testMsg);
        assertNull(action.getLogManager(), testMsg);
        action.setLogManager(manager);
        assertEquals(action.getLogManager(), manager, testMsg);
        assertEquals(action.getManager(), manager, testMsg);
    }


    /**
     * Test method for various get/set methods in the class.
     * @throws Exception because it likes to
     */
    public void testGetSetMethods() throws Exception {
        action.setFileContents(testfileContents);
        action.setFileName("testfilename");
        assertTrue(action.getFileContents() == testfileContents,
            "logFilesActionSetContents");
        assertNotNull(action.getFileContents(), "logFilesActionSetContents");
        assertTrue(action.getFileLineNum() >= 0, "filelinenum test");
        assertTrue(action.getFileName() == filename, "filename test");
        assertNotNull(action.getFileTooLarge(), "getfiletoolarge test");
        assertNotNull(LogFilesAction.getMaxFileSize(), "getmaxfilesize test");
        assertTrue(LogFilesAction.getMaxFileSize() > 0, "getmaxfilesize test");
        assertNotNull(action.getLogFile(), "getLogFile test");
        action.setRequestedFile(new FileView(new File(logdir + action.getFileName())));
        assertNotNull(action.getRequestedFile(), "getrequestedfile test");
        assertTrue(action.getRequestedFile().getFileName().equals("testfilename"), "getrequestedfile test");
        assertNotNull(action.getSystemLogDir(), "systemlogdir test");
        assertTrue(action.getSystemLogDir().equals(logdir), "systemlogdir test");
        action.setSelectedFiles(filelist);
        assertNotNull(action.getSelectedFiles(), "selectedfiles test");
        assertTrue(action.getSelectedFiles() == filelist, "selectedfiles test");

    }

    /**
     * Tests the converting of the file size.
     *
     */
    public void testConvertFileSize() {
        final long smfilesize = 999L;
        final long medfilesize = 2048L;
        final long lrgfilesize = 2196608L;
        assertTrue(action.convertFileSize(smfilesize) == smfilesize, "small file size");
        assertTrue(action.convertFileSize(medfilesize) == 2L, "medium file size");
        assertTrue(action.convertFileSize(lrgfilesize) == 2L, "large file size");
    }

    /**
     * Tests the downloading of files.
     * @throws Exception when there is an issue zipping the file.
     */
    public void testDownloadFiles() throws Exception {

        Map<String, InputStream> logMap = new HashMap<String, InputStream>();

        String file1Content = " This is a simple zip test ";
        String file2Content = " This kind of test speacial characters. 1. § 2. ? 3. ¡ 4. ? 5. ?";

        InputStream bais1 = new ByteArrayInputStream(file1Content.getBytes());
        InputStream bais2 = new ByteArrayInputStream(file2Content.getBytes());

        logMap.put("file11.log", bais1);
        logMap.put("file22.log", bais2);

        HttpServletResponse resp = action.getResponse();
        resp.setContentType("application/zip");
        resp.setHeader("Content-Disposition", "attachment;filename=\""
            + "testDownloadFile.log" + "\"");
        resp.setHeader("Content-Encoding", "x-compress");

        OutputStream out = ZipUtil.zipData(logMap);
        assertNotNull(out, "testDownloadFiles test");
    }

}
