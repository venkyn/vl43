/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.TimeZone;

import org.apache.struts2.ServletActionContext;
import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Test class for the <code>UserAction</code> class.
 *
 * @author Dennis Doubleday
 */
@Test(groups = { FAST, ACTION, SECURITY }, enabled = true)
public class SiteActionTest extends BaseActionTestCase {

    private SiteAction action;
    private SiteManager siteMgr;

    /**
     * The classSetUp method is called at the start of testing of this class.
     * @throws Exception
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        // Load data used by this test class (get spreadsheet
        // relative to <project_root>/test).
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.handleFlatXmlResource("data/dbunit/sites-simple.xml", DatabaseOperation.REFRESH);
    }


    /**
     * Setter for the action property.
     * @param newAction the new action value
     */
    @Test(enabled = false)
    public void setSiteAction(SiteAction newAction) {
        this.action = newAction;
    }


    /**
     * Setter for the siteMgr property.
     * @param siteManager the new siteMgr value
     */
    @Test(enabled = false)
    public void setSiteManager(SiteManager siteManager) {
        this.siteMgr = siteManager;
    }

    /**
     * Test method for various get/set methods in the class.
     */
    public void testGetSetMethods() {
        final long siteId = 999L;
        Site site = new Site();
        site.setDescription("Test Site");
        site.setName("Default");
        site.setNotes("Test Notes");
        site.setTimeZone(TimeZone.getDefault());
        action.setSiteId(siteId);
        action.setSite(site);
        action.setSiteManager(this.siteMgr);
        assertTrue(action.getSite().equals(site), "siteActionSite");
        assertEquals(action.getSiteId().longValue(), siteId, "siteActionSiteId");
        assertTrue(action.getSiteManager().equals(this.siteMgr), "siteActionSiteManager");
        assertTrue(action.getManager().equals(this.siteMgr), "siteActionGetManager");
    }

    /**
     * Test method for 'com.vocollect.epp.web.action.SiteAction.prepare()'.
     */
    public void testPrepare() throws Exception {
        final long siteID = -1L;
        action.setSiteId(null);
        action.prepare();
        action.setSiteId(siteID);
        action.setSiteManager(this.siteMgr);
        action.prepare();
    }
    /**
     * Test method for the 'com.vocollect.epp.web.action.SiteAction.save()'.
     * @throws Exception
     */
    public void testSave() throws Exception {

        final long siteId = -1L;
        // Get a site for modifications.
        Site site = siteMgr.get(siteId);
        site.setNotes("Notes - Testing Site Action");
        action.setSite(site);
        action.setSiteId(siteId);

        ServletActionContext.setRequest(getRequest());

        assertEquals(action.save(), "success");
        assertNotNull(action.getSite());
        assertFalse(action.hasActionErrors());
    }

    /**
     * Test method for the 'com.vocollect.epp.web.action.SiteAction.delete()'.
     * @throws Exception
     */
    public void testRemove() throws Exception {

        final long siteId = -2L;
        Site site = siteMgr.get(siteId);

        action.setIds(new Long[] { site.getId() });
        assertEquals(action.delete(), com.opensymphony.xwork2.Action.SUCCESS);
        assertFalse(action.hasActionErrors());
    }
}
