/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.epp.model.Role;
import com.vocollect.epp.service.RoleManager;

import static com.vocollect.epp.test.TestGroups.ACTION;
import static com.vocollect.epp.test.TestGroups.FAST;
import static com.vocollect.epp.test.TestGroups.SECURITY;

import java.io.IOException;

import org.testng.annotations.Test;


/**
 * Test class for the <code>RoleAction</code> class.
 *
 * @author sshaulis
 */
@Test(groups = { FAST, ACTION, SECURITY })
public class RoleActionTest extends BaseActionTestCase {

    private RoleAction action;
    private RoleManager roleManager;

    /**
     * Setter for the action property.
     * @param newAction the new action value
     */
    @Test(enabled = false)
    public void setRoleAction(RoleAction newAction) {
        this.action = newAction;
    }


    /**
     * Setter for the roleManager property.
     * @param roleManager the new roleManager value
     */
    @Test(enabled = false)
    public void setRoleManager(RoleManager roleManager) {
        this.roleManager = roleManager;
    }

    /**
     * Test method for 'com.vocollect.epp.web.action.RoleAction.getManager()'.
     * And for get/set RoleManager methods.
     */
    public void testGetManager() {
        String testMsg = "Testing action.getManager() functions";
        action.setRoleManager(null);
        assertNull(action.getManager(), testMsg);
        assertNull(action.getRoleManager(), testMsg);
        action.setRoleManager(roleManager);
        assertEquals(action.getRoleManager(), roleManager, testMsg);
        assertEquals(action.getManager(), roleManager, testMsg);
    }

    /**
     * Test method for 'com.vocollect.epp.web.action.RoleAction.RoleAction()'.
     */
    public void testRoleAction() {
        RoleAction localAction = new RoleAction();
        assertTrue(localAction != null, "roleActionConstructor");
    }

    /**
     * Test get/set methods for 'com.vocollect.epp.web.action.RoleAction.RoleId()'.
     */
    public void testGetSetRoleId() {
        final Long zero = 0L;
        final Long positive = 20L;
        final Long negative = -98L;
        String testMsg = "Testing role ID boundary conditions";
        action.setRoleId(Long.MAX_VALUE);
        assertTrue(action.getRoleId() == Long.MAX_VALUE, testMsg);
        action.setRoleId(Long.MIN_VALUE);
        assertTrue(action.getRoleId() == Long.MIN_VALUE, testMsg);
        action.setRoleId(zero);
        assertEquals(action.getRoleId(), zero, testMsg);
        action.setRoleId(positive);
        assertEquals(action.getRoleId(), positive, testMsg);
        action.setRoleId(negative);
        assertEquals(action.getRoleId(), negative, testMsg);
    }

    /**
     * Test get/set methods for 'com.vocollect.epp.web.action.RoleAction.Role()'.
     */
    public void testGetSetRole() {
        Role role = new Role();

        action.setRole(role);
        assertEquals(action.getRole(), role, "roleActionRole");
    }

    /**
     * Test methods for 'com.vocollect.epp.web.action.RoleAction.setFeatures(long[])'
     * and 'com.vocollect.epp.web.action.RoleAction.getFeatures()'.
     */
    public void testSetFeatures() throws DataAccessException {

        //TODO: finish implementation

/*
        long[] features = {1,2,3};
        long[] features2;
        Role role = new Role();
        boolean arraysEqual = true;
        boolean found;



        action.setFeatures(null);
        action.setRole(null);
        assertNull("roleActionGetFeatures", action.getFeatures());
        action.setFeatures(features);
        features2 = action.getFeatures();
        arraysEqual = features.length == features2.length;

        for(int i = 0; i < features.length; i++){
            found = false;
            for(int j = 0; j < features2.length; j++){
                found = found || (features[i] == features2[j]);
            }
            arraysEqual = arraysEqual && found;
        }

        assertTrue("roleActionGetFeatures", arraysEqual);

        action.setRole(role);
        action.setFeatures(features);
        RoleManager manager = (RoleManager) ctx.getBean(roleManager);
        action.setRoleManager(manager);
        features2 = action.getFeatures();
        assertTrue("roleActionGetFeatures", features.equals(features2));
*/
    }

    /**
     * Test method for 'com.vocollect.epp.web.action.RoleAction.getFeatureGroups()'.
     */
    public void testGetFeatureGroups() throws DataAccessException {
        assertTrue(action.getFeatureGroups() != null, "roleActionGetFeatureGroups");
    }

    /**
     * Test method for 'com.vocollect.epp.web.action.RoleAction.delete()'.
     */
    public void testDelete() throws IOException, DataAccessException {
        final long roleId = 10000L;

        try {
            action.delete();
            fail("Succeeded in deleting without ID");
        } catch (Exception e) {
            // Expected exception
        }
        action.setIds(new Long[] {roleId});
        // This should return error because there shouldn't be such an
        // ID in the system.
        // the action should return SUCCESS
        assertEquals(action.delete(),
            com.opensymphony.xwork2.Action.SUCCESS, "roleActionDelete");

        // but the Json message should indicate failure
        assertTrue(action.getJsonMessage().matches(".*\"errorCode\":\"1\".*"),
                   "roleActionDelete");

    }

    /**
     * Test method for 'com.vocollect.epp.web.action.RoleAction.save()'.
     */
    public void testSave() throws Exception {

    }

    /**
     * Test method for 'com.vocollect.epp.web.action.RoleAction.prepare()'.
     */
    public void testPrepare() throws Exception {
        action.prepare();
    }

}
