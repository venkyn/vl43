/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.model.Site;
import com.vocollect.epp.model.User;
import com.vocollect.epp.service.RoleManager;
import com.vocollect.epp.service.SiteManager;
import com.vocollect.epp.service.UserManager;
import com.vocollect.epp.test.DbUnitAdapter;

import static com.vocollect.epp.test.TestGroups.*;

import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.dbunit.operation.DatabaseOperation;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Test class for the <code>UserAction</code> class.
 *
 * @author Dennis Doubleday
 */
@Test(groups = { FAST, ACTION, SECURITY }, enabled = true)
public class UserActionTest extends BaseActionTestCase {

    private UserAction action;
    private UserManager userMgr;
    private RoleManager roleMgr;
    private SiteManager siteMgr;



    /**
     * Setter for the action property.
     * @param newAction the new action value
     */
    @Test(enabled = false)
    public void setUserAction(UserAction newAction) {
        this.action = newAction;
    }


    /**
     * Setter for the roleMgr property.
     * @param roleManager the new roleMgr value
     */
   @Test(enabled = false)
   public void setRoleManager(RoleManager roleManager) {
        this.roleMgr = roleManager;
    }


    /**
     * Setter for the siteMgr property.
     * @param siteManager the new siteMgr value
     */
   @Test(enabled = false)
    public void setSiteManager(SiteManager siteManager) {
        this.siteMgr = siteManager;
    }


    /**
     * Setter for the userMgr property.
     * @param userManager the new userMgr value
     */
   @Test(enabled = false)
    public void setUserManager(UserManager userManager) {
        this.userMgr = userManager;
    }

    /**
     * The classSetUp method is called at the start of testing of this class.
     * @throws Exception
     */
    @BeforeClass
    protected void classSetUp() throws Exception {

        // Load data used by this test class (get spreadsheet
        // relative to <project_root>/test).
        DbUnitAdapter adapter = new DbUnitAdapter();
        adapter.resetInstallationData();
        adapter.handleFlatXmlResource("data/dbunit/users-simple.xml", DatabaseOperation.INSERT);
        System.out.println("Class setup**************************");
    }


    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.BaseActionTestCase#onSetUp()
     */
//    This is commented out because the save test is disabled. See the
//    comment there.
//    @Override
//    protected void onSetUp() throws Exception {
//
//        // This setup is all for the save() test, but it has to
//        // be done outside the session that the save is done in.
//        setUpSiteContext(false);
//        storeUser(userMgr.findByName("vocollect"));
//
//        Session session = getSessionFactory().openSession();
//        session.refresh(getUser());
//        Set<Role> roles = getUser().getRoles();
//        for (Role role : roles) {
//            role.getName();
//        }
//        Set<Site> allSites = getUser().getSites();
//        List<Site> siteList = new ArrayList<Site>(allSites);
//        session.close();
//
//        // regular setup, initializing save session.
//        super.onSetUp();
//
//        // More save() setup.
//        action.setAllSites(siteList);
//        action.getSession().setAttribute("userSites", allSites);
//    }


    /**
     * @throws Exception on any failure
     */
    @BeforeMethod
    protected void methodSetup() throws Exception {
    }

    /**
     * Test method for various get/set methods in the class.
     */
    public void testGetSetMethods() {
        final long userId = 999L;
        User user = new User();
        Date now = new Date();
        action.setConfirmPassword("confirm");
        action.setPassword("password");
        action.setRoleManager(this.roleMgr);
        action.setUser(user);
        action.setUserId(userId);
        action.setUserManager(this.userMgr);
        assertTrue(action.getConfirmPassword().equals("confirm"), "userActionConfirmPassword");
        assertTrue(action.getPassword().equals("password"), "userActionPassword");
        assertTrue(action.getRoleManager().equals(this.roleMgr), "userActionRoleManager");
        assertTrue(action.getUser().equals(user), "userActionUser");
        assertEquals(action.getUserId().longValue(), userId, "userActionUserId");
        assertTrue(action.getUserManager().equals(this.userMgr), "userActionUserManager");
        assertTrue(action.getManager().equals(this.userMgr), "userActionGetManager");
        assertNotNull(action.getStatusMap(), "userActionStatusMap");
        // a null time should translate to Never
        //TODO: Setup test to use ApplicationResources.properties
        //assertTrue(action.getLastLoggedIn().contains("Never"), "userActionLastLogin");
        user.setLastLoginTime(now);
        // now that login time is set, it should not be Never
        assertFalse(action.getLastLoggedIn().contains("Never"), "userActionLastLogin");
    }

    /**
     * Test method for 'com.vocollect.epp.web.action.UserAction.prepare()'.
     */
    public void testPrepare() throws Exception {

        final long adminUserID = -1L;
        action.setUserId(null);
        action.prepare();
        action.setUserId(adminUserID);
        action.setUserManager(this.userMgr);
        action.prepare();
    }
    /**
     * Test method for 'com.vocollect.epp.web.action.UserAction.getRoleIds()'.
     * and 'com.vocollect.epp.web.action.UserAction.setRoleIds()'.
     */
    public void testGetSetRoleIds() throws Exception {
        long[] roleIds = {-1};
        action.setUser(new User());
        action.setRoleIds(roleIds);
        roleIds = action.getRoleIds();
        assertNotNull(roleIds, "userActionGetRoleIds");
        assertTrue(roleIds.length > 0, "userActionGetRoleIds");
    }

    /**
     * Test method for 'com.vocollect.epp.web.action.UserAction.getSiteIds()'.
     * and 'com.vocollect.epp.web.action.UserAction.setSiteIds()'.
     */
    public void testGetSetSiteIds() throws Exception {
        long[] siteIds = {-1};
        action.setUser(new User());
        action.setSiteIds(siteIds);
        siteIds = action.getSiteIds();
        assertNotNull(siteIds, "userActionGetSiteIds");
        assertTrue(siteIds.length > 0, "userActionGetSiteIds");
    }


    /**
     * Test method for the 'com.vocollect.epp.web.action.UserAction.save()'.
     * This is disabled (along with the onsetup method that works with it
     * because it is a fragile test, and it needs to be rethought. It works
     * individually but not in a suite, for unknown reasons.
     * @throws Exception
     */
    @Test(enabled = false)
    @SuppressWarnings("unchecked")
    public void testSave() throws Exception {

        // The initial setup for this test is done in onSetUp.
        action.setUser(userMgr.findByName("vocollect"));
        action.setPassword("morgoth");
        action.setConfirmPassword("morgoth");
        action.setUserSiteAccess(new Integer(-1));

        //Set up request and aciton with site info.
        Set<Site> userSites = (Set<Site>)
            action.getSession().getAttribute("userSites");
        long[] siteIds = new long[userSites.size()];
        int index = 0;
        for (Site s : userSites) {
            siteIds[index] = s.getId().longValue();
            index++;
        }
        getUser().setSites(null);
        action.setSiteIds(siteIds);
        assertEquals(action.save(), "success");
        assertNotNull(action.getUser());
        assertFalse(action.hasActionErrors());
    }

    /**
     * Test method for the 'com.vocollect.epp.web.action.UserAction.delete()'.
     * This is disabled (along with the onsetup method that works with it
     * because it is a fragile test, and it needs to be rethought. It works
     * individually but not in a suite, for unknown reasons.
     * @throws Exception
     */
    @Test(enabled = false)
    public void testRemove() throws Exception {

        storeUser(userMgr.findByName("vocollect"));

        action.setUserId(getUser().getId());
        storeUser(userMgr.findByName("vocollect"));

        action.setIds(new Long[] { getUser().getId() });
        assertEquals(action.delete(), com.opensymphony.xwork2.Action.SUCCESS);
        assertFalse(action.hasActionErrors());
    }

    /**
     * Test method for the 'com.vocollect.epp.web.action.UserAction.getRoleMap()'.
     * @throws Exception
     */
    public void testGetRoleMap() throws Exception {
        boolean adminFlag = false;
        boolean readFlag = false;
        boolean readKey = false;
        boolean adminKey = false;
        String testMsg = "Testing UserAction role map";

        action.setRoleManager(roleMgr);
        Map<String, Long> roleMap = action.getRoleMap();
        assertNotNull(roleMap, testMsg);
        int mapsize = roleMap.size();
        Iterator<Map.Entry<String, Long>> pairs = roleMap.entrySet().iterator();
        for (int i = 0; i < mapsize; i++) {
            Map.Entry<String, Long> entry = pairs.next();
            Long value = entry.getValue();
            String key = entry.getKey();
            adminFlag = adminFlag || key.equalsIgnoreCase("Administrator");
            readFlag = readFlag || key.equalsIgnoreCase("Read-Only");
            adminKey = adminKey || value == -1;
            readKey = readKey || value == -3;
        }
        // these 6 values should have come out of the RoleMap
        assertTrue(adminFlag, testMsg);
        assertTrue(readFlag, testMsg);
        assertTrue(adminKey, testMsg);
        assertTrue(readKey, testMsg);
    }

    /**
     * This test method is intended to assist development.  It creates
     * a number of Users of the form "prefixN" where N is a sequential
     * digit within the range of startNum to stopNum.  This test is
     * disabled by default.
     * @param prefix prefix used to create user name
     * @param startNum starting sequence number for name generation
     * @param stopNum ending sequence number for name generation
     * @throws Exception if an error occurs during test execution.
    @Parameters({ "prefix", "startNum", "stopNum" })
    @Test(enabled = false, groups = { "dev" })
    public void createUsers(String prefix, int startNum, int stopNum) throws Exception {
        final Long siteId = -1L;

        Role newUserRole = roleMgr.findByName("Administrator");
        Site newUserSite = siteMgr.get(siteId);

        for (int i = startNum; i <= stopNum; i++) {
            User newUser = new User();
            String basename = prefix;
            if (i < 10) { basename += "0"; }
            if (i < 100) { basename += "0"; }
            if (i < 1000) { basename += "0"; }
            if (i < 10000) { basename += "0"; }
            basename += i;

            newUser.setName(basename);
            newUser.setEmailAddress(basename + "@testng.com");
            newUser.setEnabled(true);
            newUser.setNotes("This user created via TestNG");
            newUser.setPassword(basename);
            newUser.setLastLoginTime(new Date());
            newUser.addRole(newUserRole);
            newUser.addSite(newUserSite);
            try {
                userMgr.save(newUser);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
     */
}
