/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.dao.BaseDAOTestCase;
import com.vocollect.epp.model.User;

import com.opensymphony.xwork2.ActionContext;

import org.apache.struts2.ServletActionContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;



/**
 * Base class for Action test cases.
 *
 * @author ddoubleday
 */
public abstract class BaseActionTestCase extends BaseDAOTestCase {

    private User user;
    private MockHttpServletRequest request = new MockHttpServletRequest();
    private MockHttpServletResponse response = new MockHttpServletResponse();

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#getConfigLocations()
     */
    @Override
    protected String[] getConfigLocations() {
        String[] superConfig = super.getConfigLocations();
        String[] config = new String[superConfig.length + 1];
        System.arraycopy(superConfig, 0, config, 0, superConfig.length);
        // Needed for displayUtilties bean.
        config[superConfig.length] = "applicationContext-epp-actions.xml";
        return config;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onSetUp()
     */
    @Override
    protected void onSetUp() throws Exception {

        // populate the request so getRequest().getSession() doesn't fail in BaseAction.java
        ServletActionContext.setRequest(request);
        ServletActionContext.setResponse(response);
        super.onSetUp();
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.BaseDAOTestCase#onTearDown()
     */
    @Override
    protected void onTearDown() throws Exception {
        super.onTearDown();
        ActionContext.getContext().setSession(null);
    }

    /**
     * Getter for the request property.
     * @return the value of the property
     */
    public MockHttpServletRequest getRequest() {
        return this.request;
    }


    /**
     * Getter for the response property.
     * @return the value of the property
     */
    public MockHttpServletResponse getResponse() {
        return this.response;
    }


    /**
     * Getter for the user property.
     * @return the value of the property
     */
    public User getUser() {
        return this.user;
    }


    /**
     * Store the user in the base class.
     * @param newUser the new user value
     */
    public void storeUser(User newUser) {
        this.user = newUser;
    }
}
