/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.vocollect.epp.exceptions.VocollectException;

import static com.vocollect.epp.test.TestGroups.*;

import org.testng.annotations.Test;

/**
 * Test class for the <code>ReportAction</code> class.
 *
 * @author J Kercher
 */
@Test(groups = { FAST, ACTION }, enabled = true)
public class ReportTypeActionTest extends BaseActionTestCase {

    /**
     * @throws VocollectException
     */
    public void testGetReportPath() throws VocollectException {
        ReportTypeAction action = new ReportTypeAction();

        String path = action.parseReportPath("http://localhost:8080/epp/reports/some/big/report/parameters.action");
        assertEquals(path, "some/big/report");

        path = action.parseReportPath("http://valiant/epp/reports/MyReport/parameters.action");
        assertEquals(path, "MyReport");

        path = action.parseReportPath("http://valiant/epp/reports/VoiceLink/ShortsReport/parameters.action");
        assertEquals(path, "VoiceLink/ShortsReport");

        path = action.parseReportPath("http://valiant/epp/reports/parameters.action");
        assertEquals(path, "");

    }

    /**
     * @throws VocollectException
     */
    public void testGetReportName() throws VocollectException {
        ReportTypeAction action = new ReportTypeAction();

        String name = action.parseReportName("http://localhost:8080/epp/reports/some/big/report/parameters.action");
        assertEquals(name, "report");

        name = action.parseReportName("http://valiant:9090/epp/reports/MyReport/parameters.action");
        assertEquals(name, "MyReport");

        name = action.parseReportName("http://malicious:8080/epp/reports/VoiceLink/ShortsReport/parameters.action");
        assertEquals(name, "ShortsReport");

        name = action.parseReportName("http://valiant/epp/reports/parameters.action");
        assertEquals(name, "");
    }

}
