/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import static com.vocollect.epp.test.TestGroups.ACTION;
import static com.vocollect.epp.test.TestGroups.EXCEPTIONS;
import static com.vocollect.epp.test.TestGroups.FAST;

import com.opensymphony.xwork2.ActionSupport;

import org.testng.annotations.Test;


/**
 * Test class for the <code>UserAction</code> class.
 *
 * @author Dennis Doubleday
 */
@Test(groups = { FAST, ACTION, EXCEPTIONS })
public class ErrorActionTest extends BaseActionTestCase {

    private ErrorAction action;

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.web.action.BaseActionTestCase#onSetUp()
     */
    protected void onSetUp() throws Exception {
        this.action = new ErrorAction();
        this.action.setError(ErrorAction.ErrorPageType.error404.name());
        super.onSetUp();

    }

    /**
     * Test method for 'com.vocollect.epp.web.action.ErrorAction.execute()'.
     */
    public void testExecute() throws Exception {
        assertEquals(this.action.execute(), ActionSupport.SUCCESS, "execute");
    }

    /**
     * Test method for 'com.vocollect.epp.web.action.ErrorAction.getError()'.
     */
    public void testGetError() {
        String testMsg = "Testing error string";
        /*assertEquals(testMsg, this.action.getError(),
            ErrorAction.ErrorPageType.error404.name());*/
        // Set it to null, check to make sure "error" is returned
        this.action.setError(null);
        assertEquals(this.action.getError(),
            ErrorAction.ErrorPageType.error.name(), testMsg);
        // Set it to an invalid one, check to make sure "error" is returned
        this.action.setError("errorFoo");
        assertEquals(this.action.getError(),
            ErrorAction.ErrorPageType.error.name(), testMsg);
    }

    /**
     * Test method for 'com.vocollect.epp.web.action.ErrorAction.setError(String)'.
     */
    public void testSetError() {
        this.action.setError("error");
        assertEquals(this.action.getError(), "error", "Testing error string");
    }

    /**
     * Test method for 'com.vocollect.epp.web.action.ErrorAction.getErrorTime()'.
     */
    public void testGetErrorTime() {
        // Not much to test here, except it is not null and does not change
        assert this.action.getErrorTime() != null : "null error time";
        assertEquals(
            this.action.getErrorTime(),
            this.action.getErrorTime(), "repeat call");
    }

}
