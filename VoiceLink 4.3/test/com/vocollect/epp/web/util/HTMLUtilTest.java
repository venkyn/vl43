/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.web.util;

import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Test cases for HTMLUtil methods.
 *
 * @author J. Kercher
 */

public class HTMLUtilTest {

    /**
     * @throws Exception
     */
    @Test()
    public void testAddParameterToURL() throws Exception {

        String baseUrl = "http://localhost:8080/epp/some.action";

        String testUrl = HTMLUtil.addParameterToURL(baseUrl, "paramOne",
            "valOne");
        assertEquals("Adding first parameter;", (baseUrl + "?paramOne=valOne"),
            testUrl);

        baseUrl = testUrl;
        testUrl = HTMLUtil.addParameterToURL(testUrl, "param2", "val2");
        assertEquals("Adding second parameter;", (baseUrl + "&param2=val2"),
            testUrl);

    }

    /**
     * @throws Exception
     */
    @Test()
    public void testEscapeHTML() throws Exception {

        assertEquals("testing HTML escape;", "&gt;", HTMLUtil.escapeHTML(">"));
        assertEquals("testing HTML escape;",
            "star&nbsp;wars&nbsp;&gt;&nbsp;star&nbsp;trek", HTMLUtil
                .escapeHTML("star wars > star trek"));
        assertEquals("testing HTML escape;", "yin&nbsp;&amp;&nbsp;yang",
            HTMLUtil.escapeHTML("yin & yang"));
        assertEquals("testing HTML escape;", "steel&nbsp;&lt;&nbsp;titanium",
            HTMLUtil.escapeHTML("steel < titanium"));
        assertEquals("testing HTML escape;", "&quot;loud&nbsp;talker&quot;",
            HTMLUtil.escapeHTML("\"loud talker\""));
        assertEquals("testing HTML escape;",
            "&#39;politically&nbsp;correct&#39;", HTMLUtil
                .escapeHTML("'politically correct'"));
        assertEquals("testing HTML escape;",
            "&lt;insert&nbsp;explitive&nbsp;here&gt;", HTMLUtil
                .escapeHTML("<insert explitive here>"));

    }

}
