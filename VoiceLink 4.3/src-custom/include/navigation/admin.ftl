<#include "/include/common/navigationmacros.ftl" />
<div class="menuHeader"><@s.text name='nav.admin.menu'/></div>
<ul class="NavList">
    <@navigationitem
      name="nav.users"
      namespace="user"
      url="/admin/user/list.action"
      tabIndex="201"
      featureName="always" />
    <@navigationitem
      name="nav.roles"
      namespace="role"
      url="/admin/role/list.action"
      tabIndex="202"
      featureName="always" />
    <@navigationitem
      name="nav.logs"
      namespace="log"
      url="/admin/log/list.action"
      tabIndex="203"
      featureName="always" />
    <@navigationitem
      name="nav.schedules"
      namespace="schedule"
      url="/admin/schedule/list.action"
      tabIndex="204"
      featureName="always" />
    <@navigationitem
      name="nav.externalJob"
      namespace="externalJob"
      url="/admin/externalJob/list.action"
      tabIndex="205"
      featureName="always" />
    <@navigationitem
      name="nav.sites"
      namespace="site"
      url="/admin/site/list.action"
      tabIndex="206"
      featureName="always" />
    <@navigationitem
      name="nav.notification"
      namespace="notification"
      url="/admin/notification/list.action"
      tabIndex="207"
      featureName="always" />
    <@navigationitem
      name="nav.licensing"
      namespace="licensing"
      url="/admin/licensing/view.action"
      tabIndex="208"
      featureName="always" />
    <@navigationitem
      name="nav.systemConfiguration"
      namespace="systemconfiguration"
      url="/admin/systemconfiguration/view.action"
      tabIndex="209"
      featureName="always" />
</ul>

