<#include "/include/common/navigationmacros.ftl" />
<#include "/include/common/action.ftl" />
<div class="menuHeader"><@s.text name='nav.puttostore.menu'/></div>

<script type="text/javascript">
	var showAssignOperatorActionURL = '${base}/puttostore/operator/showAssignOperators.action';
	var regionId = 'regionId';
</script>

<ul class="NavList">
  <#if currentSiteID==-927>
	  <@navigationitem
	    name="nav.puttostoreHome"
	    namespace="home"
	    url="/puttostore/home.action?currentSiteID=-927"
	    tabIndex="201"
	    featureName="feature.puttostore.home" />
  <#else>
	  <@navigationitem
	    name="nav.puttostoreHome"
	    namespace="home"
	    url="/puttostore/home.action"
	    tabIndex="201"
	    featureName="feature.puttostore.home" />
  </#if>
  <@navigationitem
    name="nav.puttostore.customerLocation"
    namespace="customerLocation"
    url="/puttostore/customerLocation/list.action"
    tabIndex="202"
    featureName="feature.puttostore.customerlocation.view" />
  <@navigationitem
    name="nav.puttostore.licenses"
    namespace="license"
    url="/puttostore/license/list.action"
    tabIndex="203"
    featureName="feature.puttostore.license.view" />
  <@navigationitem
    name="nav.labor"
    namespace="labor"
    url="/puttostore/labor/list.action"
    tabIndex="204"
    featureName="feature.voicelink.labor.view" />

  <#assign dropDownNamespaces= ["breakType", "container",
                                "item", "locItem", "location", "operator", "print",
                                "region", "report", "summaryPrompt", "vscresponse", "workgroup"] />
  <#assign dropDownId=navigation.applicationMenu+"Select" />
  <li id="${dropDownId}Item"
  <#if dropDownNamespaces?seq_contains(navigation.actionsMenu)>
    class="highlight"
  </#if>
  >
    <div>
      <div class="navDropDownContainer">
        <select id="${dropDownId}" class="navDropDown" tabIndex="205" tid="navDropDown">
          <option value="go"><@s.text name='nav.go' /></option>
	      <@navigationOption
		    name="nav.breakTypes"
		    namespace="breakType"
		    url="/puttostore/breakType/list.action"
		    featureName="feature.voicelink.breakType.view" />
		  <@navigationOption
		    name="nav.containers"
		    namespace="container"
		    url="/puttostore/container/list.action"
		    featureName="feature.puttostore.ptscontainer.view" />
          <@navigationOption
            name="nav.items"
            namespace="item"
            url="/puttostore/item/list.action"
            featureName="feature.voicelink.item.view" />
          <@navigationOption
            name="nav.locations"
            namespace="location"
            url="/puttostore/location/list.action"
            featureName="feature.voicelink.location.view" />
		  <@navigationOption
		    name="nav.operators"
		    namespace="operator"
		    url="/puttostore/operator/list.action"
		    featureName="feature.voicelink.operator.view" />
		  <@navigationOption
		    name="nav.printer"
		    namespace="print"
		    url="/puttostore/print/list.action"
		    featureName="feature.voicelink.printer.view" />
          <@navigationOption
		    name="nav.region"
		    namespace="region"
		    url="/puttostore/region/list.action"
		    featureName="feature.puttostore.region.view" />
          <@navigationOption
            name="nav.reports"
            namespace="report"
            url="/puttostore/report/list.action"
            featureName="feature.voicelink.report.view" />
          <@navigationOption 
            name="nav.vehicleSafetyCheck"
            namespace="vscresponse"
            url="/puttostore/vscresponse/list.action" 
            featureName="feature.voicelink.vsc.view" />
		  <@navigationOption
		    name="nav.workgroups"
		    namespace="workgroup"
		    url="/puttostore/workgroup/list.action"
		    featureName="feature.voicelink.workgroup.view" />
        </select>
      </div>
      <div class="lowerEdge"></div>
      <div class="actionBubbleContainer" id="${dropDownId}_bubble">
        <div class="actionBubbleCarrot"></div>
        <div class="actionBubble actionBubbleTop"></div>
        <div class="actionBubble actionBubbleMiddle">
          <div class="actionBubbleContents" id="${dropDownId}_msg">
            <@s.text name="nav.action.message.permissions" />
          </div>
        </div>
        <div class="actionBubble actionBubbleBottom"></div>
	  </div>
    </div>
  </li>
</ul>
<#assign tabIndexNumber = 253 />
<#if summaries?has_content>
		<#list summaries?keys as summaryId>
		    <#if summaries[summaryId].descriptiveText = "TabularSummary">
		         <#assign tableId = summaries[summaryId].tableId >
		   		  <#if tableId = "puttostoreLaborSummary">
		   		    <div class="spacer"></div>
				    <div class="menuHeader"><@s.text name='nav.summary.labor.actions' /></div>
				    <div id="menuBlade_listputtostoreLaborSummaryTable">
				    <ul class="NavList" id="bladeList_listputtostoreLaborSummaryTable">
				    <@action
				      name="nav.summary.labor.viewOperatorLabor"
				      tableId="list${tableId}Table"
				      id="a_viewOperatorLabor"
				      url="javascript:viewLaborData(${tableId}Obj,\\'${base}/puttostore/labor/list.action\\');"
				      tabIndex="${tabIndexNumber}"
				      actionEnablers={"enableOnOne":"summary.labor.disabled.puttostore"}
				      featureName="feature.voicelink.labor.view" />
				    </ul>
				    </div>
		   		 </#if>
		         <#assign tabIndexNumber = tabIndexNumber + 1 >
		     </#if>
		</#list>
</#if>


<script type="text/javascript">
	<#if navigation.actionsMenu?has_content>
		initializeDropDown("${dropDownId}", "a_${navigation.actionsMenu}");
	<#else>
		initializeDropDown("${dropDownId}", null);
	</#if>
	<#if !(navigation.pageName?exists)>
		var contextHelpUrl = 240;
	<#else>
		var contextHelpUrl = -1;
	</#if>
</script>
