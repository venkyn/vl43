<#include "/include/common/navigationmacros.ftl" />
<#include "/include/common/action.ftl" />
<div class="menuHeader"><@s.text name='nav.putaway.menu'/></div>
<ul class="NavList">
  <#if currentSiteID==-927>
	  <@navigationitem
	    name="nav.putawayHome"
	    namespace="home"
	    url="/putaway/home.action?currentSiteID=-927"
	    tabIndex="201"
	    featureName="feature.voicelink.putaway.home" />
  <#else>
	  <@navigationitem
	    name="nav.putawayHome"
	    namespace="home"
	    url="/putaway/home.action"
	    tabIndex="201"
	    featureName="feature.voicelink.putaway.home" />
  </#if>
  <@navigationitem
    name="nav.putaway.licenses"
    namespace="license"
    url="/putaway/license/list.action"
    tabIndex="202"
    featureName="feature.voicelink.putaway.licenses.view" />
  <@navigationitem
    name="nav.licenseDetails"
    namespace="licenseDetail"
    url="/putaway/licenseDetail/list.action"
    tabIndex="203"
    featureName="feature.voicelink.putaway.licensedetail.view" />
  <@navigationitem
  	name="nav.labor"
	namespace="labor"
 	url="/putaway/labor/list.action"
    tabIndex="204" 	
    featureName="feature.voicelink.labor.view" />
    
  <#assign dropDownNamespaces= ["breakType", "item", "location", "lot", "operator", 
                                "reasonCodes", "region", "vscresponse", "workgroup"] />
    
  <#assign dropDownId="vlPutawayDropDown" />
  <#assign dropDownId=navigation.applicationMenu+"Select" />
  <li id="${dropDownId}Item"
  <#if dropDownNamespaces?seq_contains(navigation.actionsMenu)>
    class="highlight"
  </#if>
  >
    <div>
      <div class="navDropDownContainer">
        <select id="${dropDownId}" class="navDropDown" tabIndex="205" tid="navDropDown">
          <option value="go"><@s.text name='nav.go' /></option>
	      <@navigationOption 
		    name="nav.breakTypes"
		    namespace="breakType"
		    url="/putaway/breakType/list.action" 
		    featureName="feature.voicelink.breakType.view" />		    
          <@navigationOption
            name="nav.items"
            namespace="item"
            url="/putaway/item/list.action"
            featureName="feature.voicelink.item.view" />
          <@navigationOption
            name="nav.locations"
            namespace="location"
            url="/putaway/location/list.action"
            featureName="feature.voicelink.location.view" />
         <@navigationOption
            name="nav.lots"
            namespace="lot"
            url="/putaway/lot/list.action"
            featureName="feature.voicelink.lot.view" />            
	     <@navigationOption 
		    name="nav.operators"
		    namespace="operator"
		    url="/putaway/operator/list.action" 
		    featureName="feature.voicelink.operator.view" />
		  <@navigationOption 
		    name="nav.reasonCodes"
		    namespace="reasonCodes"
		    url="/putaway/reasonCodes/list.action" 
		    featureName="feature.voicelink.reasoncodes.view" />
          <@navigationOption 
		    name="nav.region"
		    namespace="region"
		    url="/putaway/region/list.action" 
		    featureName="feature.voicelink.putaway.region.view" />
		  <@navigationOption 
            name="nav.reports"
            namespace="report"
            url="/putaway/report/list.action" 
            featureName="feature.voicelink.report.view" />   
          <@navigationOption 
            name="nav.vehicleSafetyCheck"
            namespace="vscresponse"
            url="/putaway/vscresponse/list.action" 
            featureName="feature.voicelink.vsc.view" />
		  <@navigationOption 
		    name="nav.workgroups"
		    namespace="workgroup"
		    url="/putaway/workgroup/list.action" 
		    featureName="feature.voicelink.workgroup.view" />
        </select>
      </div>
      <div class="lowerEdge"></div>
      <div class="actionBubbleContainer" id="${dropDownId}_bubble">
        <div class="actionBubble actionBubbleTop"></div>
        <div class="actionBubble actionBubbleMiddle">
          <div class="actionBubbleContents" id="${dropDownId}_msg">
            <@s.text name="nav.action.message.permissions" />
          </div>
        </div>
        <div class="actionBubble actionBubbleBottom"></div>
	  </div>
    </div>
  </li>
</ul>
<#assign tabIndexNumber = 253 />
<#if summaries?has_content>
		<#list summaries?keys as summaryId>
		    <#if summaries[summaryId].descriptiveText = "TabularSummary"> 
		         <#assign tableId = summaries[summaryId].tableId >		
		   	     <#if tableId = "putawayLaborSummary">
		   		    <div class="spacer"></div>
				    <div class="menuHeader"><@s.text name="nav.summary.labor.actions" /></div>				    
				    <ul class="NavList">
				    <@action
				      name="nav.summary.labor.viewOperatorLabor"
				      tableId="list${tableId}Table"
				      id="a_viewOperatorLabor"
				      url="javascript:viewLaborData(${tableId}Obj,\\'${base}/putaway/labor/list.action\\');"
				      tabIndex="${tabIndexNumber}"
				      actionEnablers={"enableOnOne":"summary.labor.disabled.selection"}
				      featureName="feature.voicelink.labor.view" />
				    </ul>		   		
		   		 </#if>
		         <#assign tabIndexNumber = tabIndexNumber + 1 >
		     </#if>	   	
		</#list>
</#if>
<script type="text/javascript">
	<#if navigation.actionsMenu?has_content>
		initializeDropDown("${dropDownId}", "a_${navigation.actionsMenu}");
	<#else>
		initializeDropDown("${dropDownId}", null);
	</#if>
	<#if !(navigation.pageName?exists)>
		var contextHelpUrl = 20;
	<#else>
		var contextHelpUrl = -1;
	</#if>
</script>