<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.reasonCodes.actions' /></div>
	<script type="text/javascript">
    var deleteReasonCodeURL = '${pageContext}/reasonCodes/delete.action';

  function showDeleteReasonCodeDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.reasonCode'/>" ,
			    button1: "<@s.text name='delete.yesText.reasonCodes'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, reasonCodeObj,deleteReasonCodeURL), "");
	}	
	
  function showDeleteCurrentReasonCodeDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='resoncode.delete.prompt.body'/>" ,
			    button1: "<@s.text name='reasoncode.delete.prompt.yesText'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	
   function redirectCurrentDelete(){
      <#if reasonCodeId?has_content>
     	window.location="${pageContext}/reasonCodes/deleteCurrentReasonCode.action?reasonCodeId=${reasonCodeId?c}";
      </#if>
    }
	
</script>
	<ul class="NavList">
		<#if navigation.pageName == "list" || navigation.pageName == "default">
		   <@action
				name="nav.reasoncode.view"
				tableId="reasonCodesTable"
				id="a_viewReasoncode"	
				url="javascript:gotoUrlWithSelected(reasonCodeObj,\\'${pageContext}/reasonCodes/view.action\\',\\'reasonCodeId\\');"
				tabIndex="251"
				actionEnablers={"enableOnOne":"reasoncode.view.disabled.selection"}
				featureName="feature.voicelink.reasoncodes.view" />
			<@action
				name="nav.reasoncode.create"
				tableId="reasonCodesTable"
				id="a_createReasoncode"
				url="/${navigation.applicationMenu}/reasonCodes/create!input.action"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.reasoncodes.create" />
		   	<@action
				name="nav.reasonCode.edit"
				tableId="reasonCodesTable"
				id="a_editReasoncode"
				url="javascript:gotoUrlWithSelected(reasonCodeObj,\\'${pageContext}/reasonCodes/edit!input.action\\',\\'reasonCodeId\\');"
				tabIndex="253"
				actionEnablers={"enableOnOne":"reasoncode.edit.disabled.selection"}
				featureName="feature.voicelink.reasoncodes.edit" /> 			
			<@action
				name="nav.reasonCode.delete"
				tableId="reasonCodesTable"
				id="a_deletereasonCode"
				url="javascript:showDeleteReasonCodeDialog();"
				tabIndex="254"
				actionEnablers={"enableOnMoreThanNone":"reasoncode.delete.disabled.selection"}
				featureName="feature.voicelink.reasoncodes.delete" />
		
		<#elseif navigation.pageName == "view">
			<@action
				name="nav.reasoncode.create"
				tableId="reasonCodesTable"
				id="a_createReasoncode"
				url="/${navigation.applicationMenu}/reasonCodes/create!input.action"
				tabIndex="251"
				actionEnablers={}
				featureName="feature.voicelink.reasoncodes.create" />
			<@action
				name="nav.reasoncode.edit.current"
				tableId="reasonCodesTable"
				id="a_editReasoncode"
				url="/${navigation.applicationMenu}/reasonCodes/edit!input.action?reasonCodeId=${reasonCodeId?c}"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.reasoncodes.edit" />
			<@action
				name="nav.reasoncode.delete.current"
				tableId="reasonCodesTable"
				id="a_deletereasoncode"
				url="javascript:showDeleteCurrentReasonCodeDialog();"
				tabIndex="253"
				actionEnablers={}
				featureName="feature.voicelink.reasoncodes.delete" />
			
		<#elseif navigation.pageName == "edit">
			<@action
				name="nav.reasoncode.create"
				tableId="reasonCodesTable"
				id="a_createReasoncode"
				url="/${navigation.applicationMenu}/reasonCodes/create!input.action"
				tabIndex="251"
				actionEnablers={}
				featureName="feature.voicelink.reasoncodes.create" />
        	<@action
	 			name="nav.reasoncode.delete.current"
	 			tableId="reasonCodesTable"
				id="a_deletereasoncode"
				url="javascript:showDeleteCurrentReasonCodeDialog();"
				tabIndex="253"
				actionEnablers={}
				featureName="feature.voicelink.reasoncodes.delete" />
		
		</#if>
	</ul>
	
<script type="text/javascript">
	<#if navigation.pageName == "view">
		contextHelpUrl = 22;
	<#else>
		contextHelpUrl = 76;
	</#if>
</script>

	
