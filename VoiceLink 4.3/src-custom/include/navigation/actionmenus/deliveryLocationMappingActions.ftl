<#include "/include/common/action.ftl" />
<#assign pageContext="${base}/${navigation.applicationMenu}">
<#if navigation.pageName != "create" && navigation.pageName != "create!duplicate" && navigation.pageName != "changeMappingField">
<script type="text/javascript">
	var deleteDeliveryLocationMappingURL = '${pageContext}/deliveryLocationMapping/delete.action';
	var modifyDeliveryLocationURL = '${pageContext}/deliveryLocationMapping/modifyDeliveryLocation.action';	
	
	function showModifyDeliveryLocationDialog() {
		var dialogProperties = {
			    title: "<@s.text name='deliveryLocationMapping.modifyDeliveryLocation'/>",			    
				body: "<@s.text name='modifyDeliveryLocation.body'/> <input type='text' id='deliveryLocation' name='deliveryLocation' size='5' maxlength='9'>",
			    button1: "<@s.text name='deliveryLocationMapping.modifyDeliveryLocation'/>",
				button2: "<@s.text name='deliveryLocationMapping.modifyDeliveryLocation.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(modifyDeliveryLocation, deliveryLocationMappingObj, ""), "");
	}
	
	function showDeleteDeliveryLocationDialog() {
		var dialogProperties = {
			    title: "<@s.text name='nav.deliveryLocationMapping.delete'/>",			    
				body:"<@s.text name='deliveryLocationMapping.body.deleteConfirmText'/>" ,
			    button1: "<@s.text name='deliveryLocationMapping.delete.yesText'/>",
				button2: "<@s.text name='deliveryLocationMapping.delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, deliveryLocationMappingObj,deleteDeliveryLocationMappingURL), "");
	}

</script>
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.deliveryLocation.actions'/></div>

<#-- 
    This is a temporary IE fix that must be overridden for each actions menu 
    that has message bubbles 
-->
<!--[if IE]>
<style>
  div#sidenav div.actionBubbleContainer {
    bottom: 318px;
  }
</style>
<![endif]-->
<ul id="NavList">
	<#if navigation.pageName == "list">		
		<@action 
			name="nav.deliveryLocationMapping.list.create" 
			tableId=""
			id="a_createDeliveryLocationMapping" 
			url="/${navigation.applicationMenu}/deliveryLocationMapping/create!input.action" 
			tabIndex="251" 
			actionEnablers={} 
			featureName="feature.voicelink.deliveryLocationMapping.create" />
			
		<@action 
			name="nav.deliveryLocationMapping.list.modifyDeliveryLocation" 
			tableId="listDeliveryLocationMappingsTable"
			id="a_modifyDeliveryLocationMapping" 
			url="javascript:showModifyDeliveryLocation()" 
			tabIndex="252" 
			actionEnablers={"enableOnMoreThanNone":"deliveryLocationMapping.modifyMappingField.error.selectOneorMoreDeliveryLocationMappings"} 
			featureName="feature.voicelink.deliveryLocationMapping.modifyDeliveryLocation" />	
	   	
		<@action 
			name="nav.deliveryLocationMapping.list.delete" 
			tableId="listDeliveryLocationMappingsTable"
			id="a_deleteDeliveryLocationMapping" 
			url="javascript:showDeleteDeliveryLocationDialog();" 
			tabIndex="253" 
			actionEnablers={"enableOnMoreThanNone":"deliveryLocationMapping.delete.error.selectOneorMoreDeliveryLocationMappings"} 
			featureName="feature.voicelink.deliveryLocationMapping.delete" />						
      
		<@action 
			name="nav.deliveryLocationMapping.list.changeMappingField" 
			tableId="listDeliveryLocationMappingsTable"
			id="a_changeMappingField" 
			url="/${navigation.applicationMenu}/deliveryLocationMapping/changeMappingField!input.action" 
			tabIndex="253" 
			actionEnablers={} 
			featureName="feature.voicelink.deliveryLocationMapping.changeMappingField" />			      
	</#if>	
</ul>
</#if>

<script language="javascript" >
	function modifyDeliveryLocation(tableObject)
	{
		var actionUrl = modifyDeliveryLocationURL + "?deliveryLocationMappingIds=" + deliveryLocationMappingObj.getSelectedIds().join("&deliveryLocationMappingIds=");
		actionUrl += "&deliveryLocation=" + $("deliveryLocation").value;
		doSimpleXMLHttpRequest(actionUrl, {}).addCallbacks(
					handleUserMessages, 
					function (request) {
		            	if (request.number == 403) {
		           		writeStatusMessage(error403Text, 'actionMessage error');
		            	} else {
		                    reportError(request);
		                }
					}
		);
		
		tableObj = tableObject;		
	}
	
	function showModifyDeliveryLocation() { 
      	           showModifyDeliveryLocationDialog();
  	}

<#if navigation.pageName == "create!duplicate" >
	contextHelpUrl = 55;
<#elseif navigation.pageName == "changeMappingField">
	contextHelpUrl = 56;
<#elseif navigation.pageName == "create">
	contextHelpUrl = 55;
<#else>
	contextHelpUrl = 6;
</#if>

</script>
