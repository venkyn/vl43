<#include "/include/common/action.ftl" />
<#include "/include/common/customdialog.ftl">
<#include "/include/common/customdialogdropdown.ftl">

<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.assignment.actions'/></div>



<script type="text/javascript">
 var isEdit="new";
</script>

<#if navigation.pageName != "list">
<#assign status="${cycleCountingAssignment.status!'null'}">
</#if>

<script type="text/javascript">

var editAssignmentActionURL = '${base}/cyclecounting/assignment/checkIfEditable.action';

var editAssignmentStatusURL='${base}/cyclecounting/assignment/getCycleCountStatus.action';
var modifyAssignmentStatusURL='${base}/cyclecounting/assignment/changeCycleCountStatus.action';

var editAssignmentOperatorURL='${base}/cyclecounting/assignment/getCycleCountOperator.action';
var modifyAssignmentOperatorURL='${base}/cyclecounting/assignment/changeCycleCountOperator.action';


	function showChangeCycleCountStatusDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='assignment.editStatus.confirmation.header'/>",			    
				body: dialogBody,
				button1: "<@s.text name='assignment.editStatus.confirmation.ok.button'/>",
				button2: "<@s.text name='assignment.editStatus.confirmation.cancel.button'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelectedForCycleCountEditStatus, cyclecountingObj, modifyAssignmentStatusURL), "");
	}
	
	function showChangeCycleCountOperatorDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='cyclecounting.editOperator.confirmation.header'/>",			    
				body: dialogBody,
				button1: "<@s.text name='cyclecounting.editOperator.confirmation.ok.button'/>",
				button2: "<@s.text name='cyclecounting.editOperator.confirmation.cancel.button'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelectedForCycleCountEditOperator, cyclecountingObj, modifyAssignmentOperatorURL), "");
	}
	
   function gotoUrlWithSelectedForAssignment (tableObject, url, idParam) {
	    idToken = idParam + "=";
	    endLine = "?" + idToken + tableObject.getSelectedIds().join("&" + idToken);
	    endLine += "&isEdit=new" ;
	    window.document.location.href = url + endLine;
   }
</script>

<div id="menuBlade_listCycleCountsTable">
<ul class="NavList" id="bladeList_listCycleCountsTable">

<#if navigation.pageName == "list" || navigation.pageName == "default" >

  <@action
	name="nav.cyclecounting.view"
	tableId="listCycleCountsTable"
	id="a_viewcyclecounting"	
	url="javascript:gotoUrlWithSelected(cyclecountingObj,\\'${base}/cyclecounting/assignment/view.action\\',\\'cycleCountId\\');"
	tabIndex="251"
	actionEnablers={"enableOnOne":"cyclecounting.view.disabled.selection"}
	featureName="feature.cyclecounting.assignment.view" />

  <@action
      name="nav.cyclecounting.editAssignment"
      tableId="listCycleCountsTable"
      id="a_editAssignment"
      url="javascript:performActionOnSelected(cyclecountingObj,editAssignmentActionURL);"
      tabIndex="252"
      actionEnablers={"enableOnOne":"cyclecounting.edit.error.selectOnlyOneAssignment"}
      featureName="feature.cyclecounting.assignment.edit" />
            
   <@action
      name="nav.cyclecounting.modify.status"
      tableId="listCycleCountsTable"
      id="a_modifyAssignmentStatus"
	  url="javascript:getDataOnSelectedForCycleCountEditStatus(cyclecountingObj,editAssignmentStatusURL);"
      tabIndex="253"
      actionEnablers={"enableOnMoreThanNone":"cyclecounting.change.status.error.selectOneOrMore"}
      featureName="feature.cyclecounting.assignment.modifyStatus" />

<#-- Block below is  for the 'Change Operator' action. -->
   <@action
      name="nav.cyclecounting.change.operator"
      tableId="listCycleCountsTable"
      id="a_changeAssignmentOperator"
	  url="javascript:getDataOnSelectedForCycleCountEditOperator(cyclecountingObj,editAssignmentOperatorURL);"
      tabIndex="254"
      actionEnablers={"enableOnMoreThanNone":"cyclecounting.change.operator.error.selectOneOrMore"}
      featureName="feature.cyclecounting.assignment.changeOperator" />
       
 <#elseif navigation.pageName == "view" && (status == 'Available' || status == 'Unavailable')>     
 
   <@action
      name="nav.cyclecounting.editAssignment"
      tableId="listCycleCountsTable"
      id="a_editAssignment"
      url="/${navigation.applicationMenu}/assignment/edit!input.action?cycleCountId=${cycleCountId?c}"
      tabIndex="252"
      actionEnablers={}
      featureName="feature.cyclecounting.assignment.edit" />
 
      
</#if>
</ul>
</div>

<#-- The details view currently has no actions. -->
<div class="menuHeader"><@s.text name=''/></div>
<div id="menuBlade_listDetailTable">
<ul class="NavList" id="bladeList_listDetailTable">

</ul>
</div>

<script type="text/javascript">
<#if navigation.pageName == "edit">
	<#global helpFiles="Content/CycleCounting/CycleCountAssignments_Edit.htm" />
    contextHelpUrl = 412;
<#elseif navigation.pageName == "view">
	<#global helpFiles="Content/CycleCounting/CycleCountAssignments_Fields.htm" />
    contextHelpUrl = 411;
<#else>
    <#global helpFiles="Content/PlaceholderPages/CycleCountAsignmentsPlaceholder.htm" />
    contextHelpUrl = 410;
</#if>
</script>

