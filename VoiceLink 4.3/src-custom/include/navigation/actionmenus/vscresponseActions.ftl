<#include "/include/common/action.ftl" />
<#include "/include/common/customdialog.ftl">
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.vscresponse.actions'/></div>

<script type="text/javascript">
    
    var editNotesURL = '${pageContext}/vscresponse/editNotes.action';
    
    function editNote(){
        var noteText = (tableObj.getSelectedObjects().length == 1  
                        && tableObj.getSelectedObjects()[0].note
                        ? tableObj.getSelectedObjects()[0].note
                        : "");
                        
        var descText = "<b><@s.text name='vsc.response.editNote.description'/></b>"
        
        var dialogBody = descText + "<br/><br/>"+ 
                        "<textarea id='noteText' rows='4' cols='50' maxlength='255'>" 
                            + noteText
                            + "</textarea>";
        showEditNoteDialog(dialogBody);
        
    }
    
    function showEditNoteDialog(dialogBody) {
        var dialogProperties = {
                    title: "<@s.text name='vsc.response.editNote.title'/>",                
                    body: dialogBody ,
                    button1: "<@s.text name='vsc.response.editNote.yesText'/>",
                    button2: "<@s.text name='vsc.response.editNote.noText'/>"}
            buildCustomDialog(dialogProperties,
                partial(performEditNote, tableObj,editNotesURL), "");
    }


    function performEditNote(tableObject, action) {
        endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
        endLine = endLine + "&newNote=" + $("noteText").value;
        doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
            handleUserMessages, function(request) {
            if (request.number == 403) {
                writeStatusMessage(error403Text, 'actionMessage error');
            } else {
                reportError(request);
            }
            });
        tableObj = tableObject;
    }
</script>


<ul class="NavList">
    <#if navigation.pageName == "list" || navigation.pageName == "default">
        <@action 
            name="nav.listpage.vscresponse.editnotes" 
            tableId="listCheckResponseTable"
            id="a_editNotes" 
            url="javascript:editNote();" 
            tabIndex="251" 
            actionEnablers={"enableOnMoreThanNone":"vsc.response.editNotes.disabled.selection"} 
            featureName="feature.voicelink.vsc.checkResponse.editNotes" />
       <@action 
            name="nav.listpage.vscresponse.vehicleAndType" 
            tableId=""
            id="a_vehicleAndType" 
            url="/${navigation.applicationMenu}/vscvehicletype/list.action" 
            tabIndex="252" 
            actionEnablers={} 
            featureName="feature.voicelink.vsc.vehicleAndType.view" />
       <@action 
            name="nav.listpage.vscresponse.safetyChecks" 
            tableId=""
            id="a_safetyChecks" 
            url="/${navigation.applicationMenu}/vscsafetycheck/list.action" 
            tabIndex="252" 
            actionEnablers={} 
            featureName="feature.voicelink.vsc.safetyCheck.view" />            
    </#if>
</ul>

<script type="text/javascript">
    <#if navigation.pageName == "create" || navigation.pageName == "edit" || navigation.pageName == "view">
       <#global helpFiles="Content/PlaceholderPages/VehicleSafetyChecks.htm"/>
       contextHelpUrl = 151;
    <#else>  
       <#global helpFiles="Content/PlaceholderPages/VehicleSafetyChecksPlaceholderPage.htm"/>
       contextHelpUrl = 150;
    </#if>
</script>