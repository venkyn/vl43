<#-- 
     This will include actions menus based on the following order:
     1) /include/navigation/actionmenus/(app-name)/
     2) /include/navigation/actionmenus/
-->
<#include "${navigation.applicationMenu}/*/${navigation.actionsMenu}Actions.ftl">
 