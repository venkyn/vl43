<#include "/include/common/action.ftl" />
<#assign pageContext="${base}/${navigation.applicationMenu}" />


  <div class="spacer"></div>
  <div class="menuHeader"><@s.text name='${table1MenuHeader}'/></div>

  <script type='text/javascript'src=${jsFile}></script>  
  
  <div id="menuBlade_${table1Id}">
    <ul class="NavList" id="bladeList_${table1Id}">
      <#if table1GlobalActions??>
        ${table1GlobalActions}
      </#if>
  
      <#if navigation.pageName == "list" || navigation.pageName == "default">
        ${table1ListOrDefaultActions}
        <#if numberOfSites != 1 >
          ${table1MultiSiteListOrDefaultActions}
        </#if>
  
        <div id="cascadingSideBar">
          <ul id="cascadingNav">  
              ${table1CascadingMenuInfo}
          </ul>
        </div>

      <#elseif navigation.pageName="edit">
        ${table1EditActions}
      <#elseif navigation.pageName="view">
        ${table1ViewActions}
        
        <#if table1ViewCascadingMenuInfo?has_content>
	      	<div id="cascadingSideBar">
	          <ul id="cascadingNav">  
	              ${table1ViewCascadingMenuInfo}
	          </ul>
	        </div>
        </#if>
        
      </#if>
	
    </ul>
  </div>

  <#if navigation.pageName == "list" || navigation.pageName == "default">
    <div class="spacer"></div>
    <div class="menuHeader"><@s.text name='${table2MenuHeader}'/></div>
    <div id="menuBlade_${table2Id}">
      <ul class="NavList" id="bladeList_${table2Id}">
        ${table2ListOrDefaultActions}
      </ul>
    </div>
  </#if>

<script type="text/javascript">
  <#if navigation.pageName == "list" ||
      navigation.pageName == "view">
    var contextHelpUrl = ${listOrViewHelpUrl?c};
  <#elseif navigation.pageName == "edit">
    var contextHelpUrl = ${editUrl?c};
  <#elseif navigation.pageName == "create">
    var contextHelpUrl = ${createUrl?c};
  </#if>
  <#list otherPages as currentPage>
    <#if navigation.pageName == currentPage>
      var contextHelpUrl = ${theRestUrl?c};
      <#break>
    </#if>
  </#list>   
</script>
