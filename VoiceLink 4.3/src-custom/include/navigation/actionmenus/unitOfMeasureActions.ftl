<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.unitofmeasure.actions' /></div>
<script type="text/javascript">
    var deleteUnitOfMeasureURL = '${pageContext}/unitOfMeasure/delete.action';

  function showDeleteUnitOfMeasureDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.unitofmeasure'/>" ,
			    button1: "<@s.text name='delete.yesText.unitofmeasure'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, unitOfMeasureObj,deleteUnitOfMeasureURL), "");
	}	
	
  function showDeleteCurrentUnitOfMeasureDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='unitofmeasure.delete.prompt.body'/>" ,
			    button1: "<@s.text name='unitofmeasure.delete.prompt.yesText'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	
   function redirectCurrentDelete(){
      <#if unitOfMeasureId?has_content>
     	window.location="${pageContext}/unitOfMeasure/deleteCurrentUnitOfMeasure.action?unitOfMeasureId=${unitOfMeasureId?c}";
      </#if>
    }
	
</script>
	<ul class="NavList">
		<#if navigation.pageName == "list" || navigation.pageName == "default">
		   <@action
				name="nav.unitofmeasure.view"
				tableId="unitOfMeasureTable"
				id="a_viewUnitOfMeasure"	
				url="javascript:gotoUrlWithSelected(unitOfMeasureObj,\\'${pageContext}/unitOfMeasure/view.action\\',\\'unitOfMeasureId\\');"
				tabIndex="251"
				actionEnablers={"enableOnOne":"unitofmeasure.view.disabled.selection"}
				featureName="feature.voicelink.unitofmeasure.view" />
			<@action
				name="nav.unitofmeasure.create"
				tableId="unitOfMeasureTable"
				id="a_createUnitOfMeasure"
				url="/${navigation.applicationMenu}/unitOfMeasure/create!input.action"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.unitofmeasure.create" />
		   	<@action
				name="nav.unitofmeasure.edit"
				tableId="unitOfMeasureTable"
				id="a_editUnitOfMeasure"
				url="javascript:gotoUrlWithSelected(unitOfMeasureObj,\\'${pageContext}/unitOfMeasure/edit!input.action\\',\\'unitOfMeasureId\\');"
				tabIndex="253"
				actionEnablers={"enableOnOne":"unitofmeasure.edit.disabled.selection"}
				featureName="feature.voicelink.unitofmeasure.edit" /> 			
			<@action
				name="nav.unitofmeasure.delete"
				tableId="unitOfMeasureTable"
				id="a_deleteUnitOfMeasure"
				url="javascript:showDeleteUnitOfMeasureDialog();"
				tabIndex="254"
				actionEnablers={"enableOnMoreThanNone":"unitofmeasure.delete.disabled.selection"}
				featureName="feature.voicelink.unitofmeasure.delete" />
		
		<#elseif navigation.pageName == "view">
			<@action
				name="nav.unitofmeasure.create"
				tableId="unitOfMeasureTable"
				id="a_createUnitOfMeasure"
				url="/${navigation.applicationMenu}/unitOfMeasure/create!input.action"
				tabIndex="251"
				actionEnablers={}
				featureName="feature.voicelink.unitofmeasure.create" />
			<@action
				name="nav.unitofmeasure.edit.current"
				tableId="unitOfMeasureTable"
				id="a_editUnitOfMeasure"
				url="/${navigation.applicationMenu}/unitOfMeasure/edit!input.action?unitOfMeasureId=${unitOfMeasureId?c}"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.unitofmeasure.edit" />
			<@action
				name="nav.unitofmeasure.delete.current"
				tableId="unitOfMeasureTable"
				id="a_deleteUnitOfMeasure"
				url="javascript:showDeleteCurrentUnitOfMeasureDialog();"
				tabIndex="253"
				actionEnablers={}
				featureName="feature.voicelink.unitofmeasure.delete" />
			
		<#elseif navigation.pageName == "edit">
			<@action
				name="nav.unitofmeasure.create"
				tableId="unitOfMeasureTable"
				id="a_createUnitOfMeaure"
				url="/${navigation.applicationMenu}/unitOfMeasure/create!input.action"
				tabIndex="251"
				actionEnablers={}
				featureName="feature.voicelink.unitofmeasure.create" />
        	<@action
	 			name="nav.unitofmeasure.delete.current"
	 			tableId="unitOfMeasureTable"
				id="a_deleteUnitOfMeasure"
				url="javascript:showDeleteCurrentUnitOfMeasureDialog();"
				tabIndex="253"
				actionEnablers={}
				featureName="feature.voicelink.unitofmeasure.delete" />
		
		</#if>
	</ul>
	
<script type="text/javascript">
	<#if navigation.pageName == "view" || navigation.pageName == "edit" || navigation.pageName == "create">
    	<#global helpFiles="Content/PlaceholderPages/UOMPlaceholder.htm" />
		contextHelpUrl = 172;
	<#else>
    	<#global helpFiles="Content/PlaceholderPages/UOMPlaceholder.htm" />
		contextHelpUrl = 170;
	</#if>
</script>

	
