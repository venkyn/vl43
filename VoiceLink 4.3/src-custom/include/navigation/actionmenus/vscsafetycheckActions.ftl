<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">

<script type="text/javascript">
	var deleteSafetyCheckURL = '${pageContext}/vscsafetycheck/delete.action';
	var editSafetyCheckURL = '${pageContext}/vscsafetycheck/checkIfEditable.action';
	
	function showDeleteSafetyCheckDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.safetyChecks'/>" ,
			    button1: "<@s.text name='delete.yesText.safetyChecks'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, safetyChecksObj,deleteSafetyCheckURL ), "");
	}	
	
	function showDeleteCurrentSafetyCheckDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.safetyCheck'/>" ,
			    button1: "<@s.text name='delete.yesText.safetyCheck'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	function redirectCurrentDelete(){
      <#if safetyCheckId?has_content>
     	window.location="${pageContext}/vscsafetycheck/deleteCurrentSafetyCheck.action?safetyCheckId=${safetyCheckId?c}";
      </#if>
    }
    
    function enableOnOneOrMoreVehicleType(){
        return ${vehicleTypeMap.size()} > 0
    }
</script>

<#if navigation.pageName != "create">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.vscsafetycheck.actions'/></div>
	<ul id="NavList">
		<#if navigation.pageName == "default">
            <#assign actionName = "nav.vscsafetycheck.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.vscsafetycheck.">
        </#if>
		<#if navigation.pageName == "list" || navigation.pageName == "default">
		    <@action          
                name="nav.safetyCheck.view"
                tableId=""
                id="a_viewSafetyCheck"
                url="javascript:gotoUrlWithSelected(safetyChecksObj, \\'${pageContext}/vscsafetycheck/view.action\\', \\'safetyCheckId\\');"
                tabIndex="251"
                actionEnablers={"enableOnOne":"vsc.safetyCheck.view.disabled.selection"}
                featureName="feature.voicelink.vsc.safetyCheck.view" />
		    <@action          
                name="nav.safetyCheck.create"
                tableId=""
                id="a_createSafetyCheck"
                url="/${navigation.applicationMenu}/vscsafetycheck/create!input.action"
                tabIndex="251"
                actionEnablers={"enableOnOneOrMoreVehicleType":"vsc.safetyCheck.create.disabled.selection"}
                featureName="feature.voicelink.vsc.safetyCheck.create" />
            <@action          
                name="nav.safetyCheck.edit"
                tableId=""
                id="a_editSafetyCheck"
                url="javascript:performActionOnSelected(safetyChecksObj,editSafetyCheckURL);"
                tabIndex="252"
                actionEnablers={"enableOnOne":"vsc.safetyCheck.edit.disabled.selection"}
                featureName="feature.voicelink.vsc.safetyCheck.edit" />    
			<@action			
			    name="nav.safetyCheck.delete"
			    tableId="listSafetyChecksTable"
		        id="a_deleteSafetyCheck"
		        url="javascript: showDeleteSafetyCheckDialog();"
		        tabIndex="253"
	            actionEnablers={"enableOnMoreThanNone":"vsc.safetyCheck.delete.disabled.selection"}
		        featureName="feature.voicelink.vsc.safetyCheck.delete" />
		<#elseif navigation.pageName == "view">
		    <@action          
                name="nav.safetyCheck.create"
                tableId=""
                id="a_createSafetyCheck"
                url="/${navigation.applicationMenu}/vscsafetycheck/create!input.action"
                tabIndex="251"
                actionEnablers={}
                featureName="feature.voicelink.vsc.safetyCheck.create" />
            <@action          
                name="nav.safetyCheck.edit.current"
                tableId=""
                id="a_editSafetyCheck"
                url="javascript:performAction(${safetyCheckId?c}, \'${pageContext}/vscsafetycheck/checkIfEditable.action\');"
                tabIndex="252"
                actionEnablers={}
                featureName="feature.voicelink.vsc.safetyCheck.edit" />
            <@action            
                name="nav.safetyCheck.delete.current"
                tableId=""
                id="a_deleteSafetyCheck"
                url="javascript: showDeleteCurrentSafetyCheckDialog();"
                tabIndex="252"
                actionEnablers={}
		        featureName="feature.voicelink.vsc.safetyCheck.delete" />
        <#elseif navigation.pageName == "edit">
            <@action          
                name="nav.safetyCheck.create"
                tableId=""
                id="a_createSafetyCheck"
                url="/${navigation.applicationMenu}/vscsafetycheck/create!input.action"
                tabIndex="251"
                actionEnablers={}
                featureName="feature.voicelink.vsc.safetyCheck.create" />
            <@action            
                name="nav.safetyCheck.delete.current"
                tableId=""
                id="a_deleteSafetyCheck"
                url="javascript: showDeleteCurrentSafetyCheckDialog();"
                tabIndex="252"
                actionEnablers={}
		        featureName="feature.voicelink.vsc.safetyCheck.delete" />
		</#if>
	</ul>
</#if>

<script type="text/javascript">
    <#if navigation.pageName == "create" || navigation.pageName == "edit" || navigation.pageName == "view">
       <#global helpFiles="Content/PlaceholderPages/SafetyChecks_Fields.htm"/>
       contextHelpUrl = 155;
    <#else>  
       <#global helpFiles="Content/PlaceholderPages/SafetyChecks_Fields.htm"/>
       contextHelpUrl = 155;
    </#if>
</script>