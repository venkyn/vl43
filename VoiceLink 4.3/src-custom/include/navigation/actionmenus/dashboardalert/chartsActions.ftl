<script type="text/javascript" src="${base}/scripts/fusioncharts/fusioncharts.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_grid_chart.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/chartUtilities.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_chart_preview.js"></script>

<#include "/include/common/action.ftl" /> 
<#assign pageContext = "${base}/${navigation.applicationMenu}">

<script type="text/javascript">
	
	var noDataTextMessage = "<@s.text name='dashboard.noData.emptyData.message'/>";
	var deleteChartURL = '${pageContext}/charts/delete.action';
	
	function showDeleteChartDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.charts'/>" ,
			    button1: "<@s.text name='delete.yesText.charts'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, chartObj,deleteChartURL), "");
	}
	
  function showDeleteCurrentChartDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.chart'/>" ,
			    button1: "<@s.text name='delete.yesText.chart'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentChartDelete,"",""), "");
	}	
	
   function redirectCurrentChartDelete(){
      <#if chartId?has_content>
     	window.location="${pageContext}/charts/deleteCurrentChart.action?chartId=${chartId?c}";
      </#if>
    }
    
    function showChartPreview() {
    	var previewChartId = chartObj.getSelectedIds()[0];
     	var chartPreview = new ChartPreviewComponent({
								'chartId' : previewChartId
										});
		chartPreview.getChartData();
    }	
</script>

<#if navigation.pageName != "create">
    <div class="spacer"></div>
    <div class="menuHeader"><@s.text name='nav.dashboardalert.chart.actions'/></div>
    <ul id="NavList">
    <#if navigation.pageName == "default">
            <#assign actionName = "nav.chart.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.chart.">
    </#if>
    <#if navigation.pageName == "list" || navigation.pageName == "default">
            <@action          
                name="nav.chart.view"
                tableId="listChartsTable"
                id="a_viewChart"
                url="javascript:gotoUrlWithSelected(chartObj, \\'${pageContext}/charts/view.action\\', \\'chartId\\');"
                tabIndex="250"
                actionEnablers={"enableOnOne":"chart.view.disabled.selection"}
                featureName="feature.dnaAdmin.chart.view" />
            <@action          
                name="nav.chart.create"
                tableId="listChartsTable"
                id="a_createChart"
                url="/${navigation.applicationMenu}/charts/create!input.action"
                tabIndex="251"
                actionEnablers={}
                featureName="feature.dnaAdmin.chart.create" />
           <@action          
                name="nav.chart.edit"
                tableId="listChartsTable"
                id="a_editChart"
                url="javascript:gotoUrlWithSelected(chartObj, \\'${pageContext}/charts/edit!input.action\\', \\'chartId\\');"
                tabIndex="252"
                actionEnablers={"enableOnOne":"chart.edit.disabled.selection"}
                featureName="feature.dnaAdmin.chart.edit" />    
            <@action            
                name="nav.chart.delete"
                tableId="listChartsTable"
                id="a_deleteChart"
                url="javascript: showDeleteChartDialog();"
                tabIndex="253"
                actionEnablers={"enableOnMoreThanNone":"chart.delete.disabled.selection"}
                featureName="feature.dnaAdmin.chart.delete" />    
            <@action            
                name="nav.chart.preview"
                tableId="listChartsTable"
                id="a_previewChart"
                url="javascript: showChartPreview();"
                tabIndex="254"
                actionEnablers={"enableOnOne":"chart.preview.disabled.selection"}
                featureName="feature.dnaAdmin.chart.preview" />
        <#elseif navigation.pageName == "view">
            <@action 
                name="nav.chart.create" 
                tableId=""
                id="a_createChart" 
                url="/${navigation.applicationMenu}/charts/create!input.action"
                tabIndex="251" 
                actionEnablers={}
                featureName="feature.dnaAdmin.chart.create" />
            <@action 
                name="nav.chart.editCurrent" 
                tableId=""
                id="a_editChart" 
                url="/${navigation.applicationMenu}/charts/edit!input.action?chartId=${chartId?c}"
                tabIndex="252" 
                actionEnablers={}
                featureName="feature.dnaAdmin.chart.edit" />
            <@action            
                name="nav.chart.deleteCurrent"
                tableId=""
                id="a_deleteChart"
                url="javascript: showDeleteCurrentChartDialog();"
                tabIndex="253"
                actionEnablers={}
                featureName="feature.dnaAdmin.chart.delete" />
        <#elseif navigation.pageName == "edit">
            <@action 
                name="nav.chart.create" 
                tableId=""
                id="a_createChart" 
                url="/${navigation.applicationMenu}/charts/create!input.action"
                tabIndex="251" 
                actionEnablers={}
                featureName="feature.dnaAdmin.chart.create" />
            <@action            
                name="nav.chart.deleteCurrent"
                tableId=""
                id="a_deleteChart"
                url="javascript: showDeleteCurrentChartDialog();"
                tabIndex="252"
                actionEnablers={}
                featureName="feature.dnaAdmin.chart.delete" />
    </#if>
    </ul>
</#if>

<script type="text/javascript">
    <#if navigation.pageName == "create" || navigation.pageName == "edit">
        contextHelpUrl = 3545;
    <#elseif navigation.pageName == "view" || navigation.pageName == "list">
        contextHelpUrl = 3540;
    <#else>  
       contextHelpUrl = 150;
    </#if>
</script>
