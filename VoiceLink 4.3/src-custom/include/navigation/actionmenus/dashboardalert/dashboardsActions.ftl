<#include "/include/common/action.ftl" /> 
<#include "/include/common/customdialog.ftl">
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript">

	var addChartToDashboardURL = '${pageContext}/dashboards/addChartToDashboard.action';
	var addChartToDashboardDialogURL= '${pageContext}/dashboards/showAddChartToDashboardDialog.action';
	var setAsDefaultURL = '${pageContext}/dashboards/setAsDefault.action';
	var addDrillDownChart = '${pageContext}/dashboards/showChartsForDrillDown.action';
	var deleteDashboardURL = '${pageContext}/dashboards/delete.action';
	 
	 
	function getAddChartDataOnSelected(tableObject, action) {
		endLine = "?dashboardId=" + tableObject.getSelectedIds();
		$j.ajax({
				async : false,
				url : globals.BASE
						+ '/dashboardalert/dashboards/showAddChartToDashboardDialog.action' + endLine,
				cache: false,
				datatype : 'json',
				success : function(resp) {
					response = resp;
				}
			});
   			 createAddChartFTL(response)
   			 tableObj = tableObject;
	}

	function createAddChartFTL(response) {
		if (checkForErrorMessageInJSON(response)) {
			showAddChartToDashboardDialog(response);
    	}
	}
	
	// function to check if the request object contains a JSON object with
	// errorcode.
	function checkForErrorMessageInJSON(request) {
	    var ERROR_SUCCESS = "0";
	    var ERROR_FAILURE = "1";
	    var ERROR_PARTIAL = "2";
	    if (request.errorCode != null && request.errorCode == 1) {
			writeStatusMessage(request.generalMessage, 'actionMessage error');
			return false;
	    } else {
			return true;
	    }
	}	
	
	function showAddChartToDashboardDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='dashboard.addChart.prompt.title'/>",			    
				body: dialogBody ,
			    button1: "<@s.text name='dashboard.addChart.prompt.yesText'/>",
				button2: "<@s.text name='dashboard.addChart.prompt.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performAddChartToDashboardFromCustomDialog, dashboardObj, addChartToDashboardURL), "");
	} 
	
	function performAddChartToDashboardFromCustomDialog(tableObject, action) {
    	endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    	endLine = endLine + "&addChartId=" + $("addChartsToDashboardDropdown").value;
    	doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	   		handleUserMessages, function(request) {
			if (request.number == 403) {
		 	   writeStatusMessage(error403Text, 'actionMessage error');
			} else {
		 	   reportError(request);
			}
	    });
   		tableObj = tableObject;
    }

    function enableOnNonDefaultDashboard(tableComponent){
        var result = dashboardObj.getSelectedObjects()[0].isDefault != true ? true : false;
    	return result;
    }
    
    function setAsDefault() {
    	  var request = $j.ajax({
            type: 'POST',
            async: false,
            url: setAsDefaultURL,
            data: {dashboardId:dashboardObj.getSelectedIds()[0]}
        });
    
        request.done(function(data){
        	if(data.errorCode == 'success') {
        		writeStatusMessage(data.generalMessage, 'actionMessage success');
        		dashboardObj.clearSelectedRows();
        	}
        });
	}
	
		
	function showDeleteDashboardDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.dashboards'/>" ,
			    button1: "<@s.text name='delete.yesText.dashboards'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, dashboardObj,deleteDashboardURL), "");
	}
	
  	function showDeleteCurrentDashboardDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.dashboard'/>" ,
			    button1: "<@s.text name='delete.yesText.dashboard'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDashboardDelete,"",""), "");
	}	
	
   	function redirectCurrentDashboardDelete(){
      <#if dashboardId?has_content>
     	window.location="${pageContext}/dashboards/deleteCurrentDashboard.action?dashboardId=${dashboardId?c}";
      </#if>
    }	
	function showDeleteDashboardDetailsDialog() {
		var deleteDashboardDetailURL = '${pageContext}/dashboarddetails/delete.action';
		var objs = dashboardDetailsObj.getSelectedObjects();
		var chartNames ='';
		for(var i =0; i<objs.length;i++) {
			chartNames += '<br>'; 
			chartNames += objs[i].drillDownChart.name != "" ? objs[i].drillDownChart.name : objs[i].parentChart.name; 
		}
		var dialogProperties = {
			    title: "<@s.text name='dashboard.detail.remove.title'/>",			    
				body: "<@s.text name='delete.body.dashboard.details'/>" + chartNames,
			    button1: "<@s.text name='delete.yesText.dashboard.details'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, dashboardDetailsObj,deleteDashboardDetailURL), "");
	}
</script>
<#if navigation.pageName != "create">
    <div class="spacer"></div>
    <div class="menuHeader"><@s.text name='nav.dashboardalert.dashboard.actions'/></div>
    <ul id="NavList">
    <#if navigation.pageName == "default">
            <#assign actionName = "nav.dashboard.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.dashboard.">
    </#if>
    <#if navigation.pageName == "list" || navigation.pageName == "default">
            <@action          
                name="nav.dashboard.view"
                tableId="listDashboardsTable"
                id="a_viewDashboard"
                url="javascript:gotoUrlWithSelected(dashboardObj, \\'${pageContext}/dashboards/view.action\\', \\'dashboardId\\');"
                tabIndex="250"
                actionEnablers={"enableOnOne":"dashboard.view.disabled.selection"}
                featureName="feature.dnaAdmin.dashboard.view" />                  
            <@action          
                name="nav.dashboard.create"
                tableId="listDashboardsTable"
                id="a_createDashboard"
                url="/${navigation.applicationMenu}/dashboards/create!input.action"
                tabIndex="251"
                actionEnablers={}
                featureName="feature.dnaAdmin.dashboard.create" />  
           <@action          
                name="nav.dashboard.edit"
                tableId="listDashboardsTable"
                id="a_editDashboard"
                url="javascript:gotoUrlWithSelected(dashboardObj, \\'${pageContext}/dashboards/edit!input.action\\', \\'dashboardId\\');"
                tabIndex="252"
                actionEnablers={"enableOnOne":"dashboard.edit.disabled.selection"}
                featureName="feature.dnaAdmin.dashboard.edit" />      
            <@action            
                name="nav.dashboard.delete"
                tableId="listDashboardsTable"
                id="a_deleteDashboard"
                url="javascript: showDeleteDashboardDialog();"
                tabIndex="253"
                actionEnablers={"enableOnMoreThanNone":"dashboard.delete.disabled.selection"}
                featureName="feature.dnaAdmin.dashboard.delete" />        
			<@action
				name="nav.dashboard.setAsDefault"
				tableId="listDashboardsTable"
				id="a_setAsDefault"
				url="javascript:setAsDefault();"
				tabIndex="254"
				actionEnablers={"enableOnOne":"dashboard.setAsDefault.disabled.selection","enableOnNonDefaultDashboard":"dashboard.setAsDefault.default.error"}
				featureName="feature.dnaAdmin.dashboard.setAsDefault" />
            <@action          
                name="nav.dashboard.addChart"
                tableId="listDashboardsTable"
                id="a_addChartToDashboard"
                url="javascript:getAddChartDataOnSelected(dashboardObj,addChartToDashboardDialogURL);"
                tabIndex="255"
                actionEnablers={"enableOnOne":"dashboard.addChart.disabled.selection"}
                featureName="feature.dnaAdmin.dashboard.addChart" />
			<@action          
                name="nav.dashboard.addDrillDownChart"
                tableId="listDashboardsTable"
                id="a_addDrillDownChartToDashboard"
                url="javascript:performActionOnSelected(dashboardObj,addDrillDownChart);"
                tabIndex="256"
                actionEnablers={"enableOnOne":"dashboard.addDrillDownChart.disabled.selection"}
                featureName="feature.dnaAdmin.dashboard.addDrillDownChart" />
     	<#elseif navigation.pageName == "view">
            <@action 
                name="nav.dashboard.create" 
                tableId=""
                id="a_createDashboard" 
                url="/${navigation.applicationMenu}/dashboards/create!input.action"
                tabIndex="251" 
                actionEnablers={}
                featureName="feature.dnaAdmin.dashboard.create" />
            <@action 
                name="nav.dashboard.editCurrent" 
                tableId=""
                id="a_editDashboard" 
                url="/${navigation.applicationMenu}/dashboards/edit!input.action?dashboardId=${dashboardId?c}"
                tabIndex="252" 
                actionEnablers={}
                featureName="feature.dnaAdmin.dashboard.edit" />
            <@action            
                name="nav.dashboard.deleteCurrent"
                tableId=""
                id="a_deleteDashboard"
                url="javascript: showDeleteCurrentDashboardDialog();"
                tabIndex="253"
                actionEnablers={}
                featureName="feature.dnaAdmin.dashboard.delete" />
        <#elseif navigation.pageName == "edit">
            <@action 
                name="nav.dashboard.create" 
                tableId=""
                id="a_createDashboard" 
                url="/${navigation.applicationMenu}/dashboards/create!input.action"
                tabIndex="251" 
                actionEnablers={}
                featureName="feature.dnaAdmin.dashboard.create" />     
            <@action            
                name="nav.dashboard.deleteCurrent"
                tableId=""
                id="a_deleteDashboard"
                url="javascript: showDeleteCurrentDashboardDialog();"
                tabIndex="252"
                actionEnablers={}
                featureName="feature.dnaAdmin.dashboard.delete" />               
    </#if>
    </ul>
</#if>

<#if navigation.pageName == "list" || navigation.pageName == "default">
    <div class="spacer"></div>
    <div class="menuHeader"><@s.text name='nav.dashboardalert.dashboard.detail.actions'/></div>
    <ul id="NavList">
        <#if navigation.pageName == "default">
            <#assign actionName = "nav.dashboard.detail.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.dashboard.detail">
        </#if>
        <#if navigation.pageName == "list" || navigation.pageName == "default">
            <@action            
                name="nav.dashboard.detail.delete"
                tableId="listDashboardDetailsTable"
                id="a_deleteDashboardDetails"
                url="javascript: showDeleteDashboardDetailsDialog();"
                tabIndex="256"
                actionEnablers={"enableOnMoreThanNone":"dashboard.detail.delete.error.selectOneOrMore"}
                featureName="feature.dnaAdmin.dashboard.detail.delete" />
        </#if>
    </ul>
</#if>

<script type="text/javascript">
    <#if navigation.pageName == "create" || navigation.pageName == "edit">
        contextHelpUrl = 3525;
    <#elseif navigation.pageName == "view" || navigation.pageName == "list">
        contextHelpUrl = 3520;
    <#elseif navigation.pageName == "addDrillDownChart">
        contextHelpUrl = 3530;    
	<#else>
       contextHelpUrl = 150;
    </#if>
</script>
