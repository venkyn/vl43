<#include "/include/common/action.ftl" />
<#include "/include/common/customdialog.ftl">
<#assign pageContext = "${base}/${navigation.applicationMenu}">

<script type="text/javascript">
	var deleteAlertURL = '${pageContext}/alerts/delete.action';
	
	function showDeleteAlertDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.alerts'/>" ,
			    button1: "<@s.text name='delete.yesText.alerts'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, alertObj,deleteAlertURL), "");
	}
	
  function showDeleteCurrentAlertDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.alert'/>" ,
			    button1: "<@s.text name='delete.yesText.alert'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentAlertDelete,"",""), "");
	}	
	
   function redirectCurrentAlertDelete(){
      <#if alertId?has_content>
     	window.location="${pageContext}/alerts/deleteCurrentAlert.action?alertId=${alertId?c}";
      </#if>
    }	
</script>

<#if navigation.pageName != "create">
    <div class="spacer"></div>
    <div class="menuHeader"><@s.text name='nav.dashboardalert.alert.actions'/></div>
    <ul id="NavList">
    <#if navigation.pageName == "default">
            <#assign actionName = "nav.alert.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.alert.">
    </#if>
    <#if navigation.pageName == "list" || navigation.pageName == "default">
            <@action          
                name="nav.alert.view"
                tableId=""
                id="a_viewAlert"
                url="javascript:gotoUrlWithSelected(alertObj, \\'${pageContext}/alerts/view.action\\', \\'alertId\\');"
                tabIndex="250"
                actionEnablers={"enableOnOne":"alert.view.disabled.selection"}
                featureName="feature.dnaAdmin.alert.view" />
            <@action          
                name="nav.alert.create"
                tableId=""
                id="a_createAlert"
                url="/${navigation.applicationMenu}/alerts/create!input.action"
                tabIndex="251"
                actionEnablers={}
                featureName="feature.dnaAdmin.alert.create" />
            <@action          
                name="nav.alert.edit"
                tableId=""
                id="a_editAlert"
                url="javascript:gotoUrlWithSelected(alertObj, \\'${pageContext}/alerts/edit!input.action\\', \\'alertId\\');"
                tabIndex="252"
                actionEnablers={"enableOnOne":"alert.edit.disabled.selection"}
                featureName="feature.dnaAdmin.alert.edit" />    
            <@action            
                name="nav.alert.delete"
                tableId=""
                id="a_deleteAlert"
                url="javascript: showDeleteAlertDialog();"
                tabIndex="253"
                actionEnablers={"enableOnMoreThanNone":"alert.delete.disabled.selection"}
                featureName="feature.dnaAdmin.alert.delete" />
        <#elseif navigation.pageName == "view">
            <@action 
                name="nav.alert.create" 
                tableId=""
                id="a_createAlert" 
                url="/${navigation.applicationMenu}/alerts/create!input.action"
                tabIndex="251" 
                actionEnablers={}
                featureName="feature.dnaAdmin.alert.create" />
            <@action 
                name="nav.alert.viewpage.edit" 
                tableId=""
                id="a_editAlert" 
                url="/${navigation.applicationMenu}/alerts/edit!input.action?alertId=${alertId?c}"
                tabIndex="252" 
                actionEnablers={}
                featureName="feature.dnaAdmin.alert.edit" />
            <@action            
                name="nav.alert.viewpage.delete"
                tableId=""
                id="a_deleteAlert"
                url="javascript: showDeleteCurrentAlertDialog();"
                tabIndex="253"
                actionEnablers={}
                featureName="feature.dnaAdmin.alert.delete" />
        <#elseif navigation.pageName == "edit">
            <@action 
                name="nav.alert.create" 
                tableId=""
                id="a_createAlert" 
                url="/${navigation.applicationMenu}/alerts/create!input.action"
                tabIndex="251" 
                actionEnablers={}
                featureName="feature.dnaAdmin.alert.create" />
            <@action            
                name="nav.alert.viewpage.delete"
                tableId=""
                id="a_deleteAlert"
                url="javascript: showDeleteCurrentAlertDialog();"
                tabIndex="252"
                actionEnablers={}
                featureName="feature.dnaAdmin.alert.delete" />
        </#if>
    </ul>
</#if>

<script type="text/javascript">
    <#if navigation.pageName == "create" || navigation.pageName == "edit">
        contextHelpUrl = 3510;
    <#elseif navigation.pageName == "view" || navigation.pageName == "list">
        contextHelpUrl = 3500;
    <#else>  
       contextHelpUrl = 150;
    </#if>
</script>
