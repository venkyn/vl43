<#include "/include/common/action.ftl" />
<#assign pageContext="${base}/${navigation.applicationMenu}"> 
<#if navigation.pageName != "create" && navigation.pageName != "create!duplicate">
<script type="text/javascript">
	var deleteItemLocationMappingURL = '${pageContext}/locItem/delete.action';
	var markAsReplenishedItemLocationMappingURL = '${pageContext}/locItem/setToReplenished.action';
	var clearInvalidLotURL = '${pageContext}/locItem/clearInvalidLot.action';
		
	function showMarkAsReplenihsedDialog() {
		var dialogProperties = {
				<#assign dialogTitle> <@s.text name='nav.itemLocationMapping.markAsReplenished'/> </#assign>		
				<#assign dialogBody> <@s.text name='itemLocationMapping.body.markAsReplenishedConfirmText'/> </#assign>	
				<#assign dialogButton1> <@s.text name='itemLocationMapping.markAsReplenished.yesText'/> </#assign>
				<#assign dialogButton2> <@s.text name='itemLocationMapping.markAsReplenished.noText'/> </#assign>

				title: "${dialogTitle?js_string}",			    
				body: "${dialogBody?js_string}",
			    button1: "${dialogButton1?js_string}",
				button2: "${dialogButton2?js_string}"}				
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, locationItemObj, markAsReplenishedItemLocationMappingURL), "");
	}


    function showDeleteLocationItemDialog() {
		var dialogProperties = {
				<#assign dialogTitle> <@s.text name='nav.itemLocationMapping.delete'/> </#assign>		
				<#assign dialogBody> <@s.text name='itemLocationMapping.body.deleteConfirmText'/> </#assign>	
				<#assign dialogButton1> <@s.text name='itemLocationMapping.delete.yesText'/> </#assign>
				<#assign dialogButton2> <@s.text name='itemLocationMapping.delete.noText'/> </#assign>
				
				title: "${dialogTitle?js_string}",			    
				body: "${dialogBody?js_string}",
			    button1: "${dialogButton1?js_string}",
				button2: "${dialogButton2?js_string}"}				
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, locationItemObj,deleteItemLocationMappingURL), "");
	}	
	
	function showClearInvalidLotDialog() {
		var dialogProperties = {
				<#assign dialogTitle> <@s.text name='clearInvalidLot.title'/> </#assign>		
				<#assign dialogBody> <@s.text name='clearInvalidLot.body.locations'/> </#assign>	
				<#assign dialogButton1> <@s.text name='clearInvalidLot.yesText.locations'/> </#assign>
				<#assign dialogButton2> <@s.text name='clearInvalidLot.noText'/> </#assign>
				
				title: "${dialogTitle?js_string}",			    
				body: "${dialogBody?js_string}",
			    button1: "${dialogButton1?js_string}",
				button2: "${dialogButton2?js_string}"}				
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected,locationItemObj,clearInvalidLotURL), "");
	}
	
	
	contextHelpUrl = 75;
</script>
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.itemLocation.actions'/></div>

<#-- 
    This is a temporary IE fix that must be overridden for each actions menu 
    that has message bubbles 
-->
<!--[if IE]>
<style>
  div#sidenav div.actionBubbleContainer {
    bottom: 318px;
  }
</style>
<![endif]-->
<ul id="NavList">
	<#if navigation.pageName == "list">		
		<@action 
			name="nav.itemLocationMapping.list.create" 
			tableId=""
			id="a_createItemLocationMapping" 
			url="/${navigation.applicationMenu}/locItem/create!input.action" 
			tabIndex="251" 
			actionEnablers={} 
			featureName="feature.voicelink.itemLocationMapping.create" />
	
		<@action 
			name="nav.itemLocationMapping.list.delete" 
			tableId="listItemLocationMappingsTable"
			id="a_deleteItemLocationMapping" 
			url="javascript:showDeleteLocationItemDialog();" 
			tabIndex="253" 
			actionEnablers={"enableOnMoreThanNone":"itemLocationMapping.delete.error.selectOneorMoreItemLocationMappings"} 
			featureName="feature.voicelink.itemLocationMapping.delete" />						
	
		<@action 
			name="nav.itemLocationMapping.list.markAsReplenished" 
			tableId="listItemLocationMappingsTable"
			id="a_markAsReplenished" 
			url="javascript:showMarkAsReplenihsedDialog();" 
			tabIndex="253" 
			actionEnablers={"enableOnMoreThanNone":"itemLocationMapping.markAsReplenished.error.selectOneorMoreItemLocationMappings"} 
			featureName="feature.voicelink.itemLocationMapping.markAsReplenished" />
			
		<@action
			name="nav.itemLocationMapping.list.clearInvalidLot"
			tableId="listItemLocationMappingsTable"
			id="a_clearInvalidLot"
			url="javascript:showClearInvalidLotDialog();"
			tabIndex="254"
			actionEnablers={"enableOnMoreThanNone":"itemLocationMapping.clearInvalidLot.disabled.selection"}
		    featureName="feature.voicelink.location.clearInvalidLot" />
	     			      
	</#if>	
</ul>
<#else>
<script type="text/javascript">
	contextHelpUrl = 7;
</script>
</#if>
