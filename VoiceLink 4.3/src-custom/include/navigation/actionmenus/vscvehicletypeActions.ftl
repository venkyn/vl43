<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript">
	var deleteVehicleTypeURL = '${pageContext}/vscvehicletype/delete.action';
	var deleteVehicleURL = '${pageContext}/vscvehicles/delete.action';

	 function showDeleteVehicleTypeDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.vehicleTypes'/>" ,
			    button1: "<@s.text name='delete.yesText.vehicleTypes'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, vehicleTypeObj,deleteVehicleTypeURL), "");
	}	
	
	function showDeleteCurrentVehicleTypeDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.vehicleType'/>" ,
			    button1: "<@s.text name='delete.yesText.vehicleType'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	function redirectCurrentDelete(){
      <#if typeId?has_content>
     	window.location="${pageContext}/vscvehicletype/deleteCurrentVehicleType.action?typeId=${typeId?c}";
      </#if>
    }
    
    function showDeleteVehicleDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.vehicles'/>" ,
			    button1: "<@s.text name='delete.yesText.vehicles'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, vehicleObj,deleteVehicleURL), "");
	}	
	
	function showDeleteCurrentVehicleDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.vehicles'/>" ,
			    button1: "<@s.text name='delete.yesText.vehicles'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentVehicleDelete,"",""), "");
	}	
	
	function redirectCurrentVehicleDelete(){
      <#if vehicleId?has_content>
     	window.location="${pageContext}/vscvehicles/deleteCurrentVehicle.action?vehicleId=${vehicleId?c}";
      </#if>
    }
    
    function enableOnNormalVehicle(tableComponent){
    	return vehicleObj.getSelectedObjects()[0].vehicleCategory != 'Default';
    }
</script>
<#if navigation.pageName != "create">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.vscvehicletype.actions'/></div>
	<ul id="NavList">
		<#if navigation.pageName == "default">
            <#assign actionName = "nav.vscvehicletype.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.vehicleType.">
        </#if>
		<#if navigation.pageName == "list" || navigation.pageName == "default">
		     <@action            
                name="nav.vehilceType.view"
                tableId="listVehicleTypesTable"
                id="a_viewVehicleType"
                url="javascript:gotoUrlWithSelected(vehicleTypeObj, \\'${pageContext}/vscvehicletype/view.action\\', \\'typeId\\');"
                tabIndex="250"
                actionEnablers={"enableOnOne":"vsc.vehicleType.view.disabled.selection"}
                featureName="feature.voicelink.vsc.vehicleType.view" />
			<@action 
	            name="nav.vehilceType.create" 
	            tableId=""
	            id="a_createVehicleType" 
	            url="/${navigation.applicationMenu}/vscvehicletype/create!input.action"
	            tabIndex="251" 
	            actionEnablers={} 
	            featureName="feature.voicelink.vsc.vehicleType.create" />
			<@action 
	            name="nav.vehilceType.edit" 
	            tableId="listVehicleTypesTable"
	            id="a_editVehicleType" 
	            url="javascript:gotoUrlWithSelected(vehicleTypeObj, \\'${pageContext}/vscvehicletype/edit!input.action\\', \\'typeId\\');"
	            tabIndex="252" 
	            actionEnablers={"enableOnOne":"vsc.vehicleType.edit.disabled.selection"} 
	            featureName="feature.voicelink.vsc.vehicleType.edit" />	            
			<@action			
			    name="nav.vehilceType.delete"
			    tableId="listVehicleTypesTable"
		        id="a_deleteVehicleType"
		        url="javascript: showDeleteVehicleTypeDialog();"
		        tabIndex="253"
	            actionEnablers={"enableOnMoreThanNone":"vsc.vehicleType.delete.disabled.selection"}
		        featureName="feature.voicelink.vsc.vehicleType.delete" />
		    <@action          
                name="nav.vehilceType.createSafetyCheck"
                tableId="listVehicleTypesTable"
                id="a_createSafetyCheck"
                url="javascript:gotoUrlWithSelected(vehicleTypeObj, \\'${pageContext}/vscsafetycheck/create!input.action\\', \\'typeId\\');"
                tabIndex="254"
                actionEnablers={"enableOnOne":"vsc.vehicleType.createSafetyCheck.disabled.selection"}
                featureName="feature.voicelink.vsc.safetyCheck.create" />
		<#elseif navigation.pageName == "view">
		    <@action 
                name="nav.vehilceType.create" 
                tableId=""
                id="a_createVehicleType" 
                url="/${navigation.applicationMenu}/vscvehicletype/create!input.action"
                tabIndex="251" 
                actionEnablers={} 
                featureName="feature.voicelink.vsc.vehicleType.create" />
			<@action 
	            name="nav.vehilceType.edit" 
	            tableId=""
	            id="a_editVehicleType" 
	            url="/${navigation.applicationMenu}/vscvehicletype/edit!input.action?typeId=${typeId?c}"
	            tabIndex="252" 
	            actionEnablers={} 
	            featureName="feature.voicelink.vsc.vehicleType.edit" />                
            <@action			
			    name="nav.vehilceType.delete.current"
			    tableId="listVehicleTypesTable"
		        id="a_deleteVehicleType"
		        url="javascript: showDeleteCurrentVehicleTypeDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.voicelink.vsc.vehicleType.delete" />
		<#elseif navigation.pageName == "edit">
		    <@action 
                name="nav.vehilceType.create" 
                tableId=""
                id="a_createVehicleType" 
                url="/${navigation.applicationMenu}/vscvehicletype/create!input.action"
                tabIndex="251" 
                actionEnablers={} 
                featureName="feature.voicelink.vsc.vehicleType.create" />
            <@action			
			    name="nav.vehilceType.delete.current"
			    tableId="listVehicleTypesTable"
		        id="a_deleteVehicleType"
		        url="javascript: showDeleteCurrentVehicleTypeDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.voicelink.vsc.vehicleType.delete" />		        
		</#if>
	</ul>
</#if>
<#if navigation.pageName != "create" && navigation.pageName != "edit">
    <div class="spacer"></div>
    <div class="menuHeader"><@s.text name='nav.vscvehicle.actions'/></div>
    <ul id="NavList">
        <#if navigation.pageName == "default">
            <#assign actionName = "nav.vscvehicle.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.vehicle.">
        </#if>
        <#if navigation.pageName == "list" || navigation.pageName == "default">
             <@action            
                name="nav.vehicle.view"
                tableId="listVehiclesTable"
                id="a_viewVehicle"
                url="javascript:gotoUrlWithSelected(vehicleObj, \\'${pageContext}/vscvehicles/view.action\\', \\'vehicleId\\');"
                tabIndex="250"
                actionEnablers={"enableOnOne":"vsc.vehicle.view.disabled.selection"}
                featureName="feature.voicelink.vsc.vehicle.view" />
            <@action 
                name="nav.vehicle.create" 
                tableId="listVehicleTypesTable"
                id="a_createVehicle" 
                url="javascript:gotoUrlWithSelected(vehicleTypeObj, \\'${pageContext}/vscvehicles/create!input.action\\', \\'typeId\\');"
                tabIndex="251" 
                actionEnablers={"enableOnOne":"vsc.vehicle.create.disabled.selection"}
                featureName="feature.voicelink.vsc.vehicle.create" />
            <@action 
                name="nav.vehicle.edit" 
                tableId="listVehiclesTable"
                id="a_editVehicle" 
                url="javascript:gotoUrlWithSelected(vehicleObj, \\'${pageContext}/vscvehicles/edit!input.action\\', \\'vehicleId\\');"
                tabIndex="251" 
                actionEnablers={"enableOnOne":"vsc.vehicle.edit.disabled.selection","enableOnNormalVehicle":"vsc.vehicle.edit.default.error"}
                featureName="feature.voicelink.vsc.vehicle.edit" />
            <@action            
                name="nav.vehicle.delete"
                tableId="listVehiclesTable"
                id="a_deleteVehicle"
                url="javascript: showDeleteVehicleDialog();"
                tabIndex="252"
                actionEnablers={"enableOnMoreThanNone":"vsc.vehicle.delete.disabled.selection","enableOnNormalVehicle":"vsc.vehicle.delete.default.error"}
                featureName="feature.voicelink.vsc.vehicle.delete" />
        </#if>
    </ul>
</#if>

<script type="text/javascript">
    <#if navigation.pageName == "create" || navigation.pageName == "edit" || navigation.pageName == "view">
       <#global helpFiles="Content/PlaceholderPages/Vehicles_Fields.htm"/>
       contextHelpUrl = 152;
    <#else>  
       <#global helpFiles="Content/PlaceholderPages/Vehicles_Fields.htm"/>
       contextHelpUrl = 152;
    </#if>
</script>
