<#include "/include/common/action.ftl" />
<#include "/include/common/customdialog.ftl">
<#assign pageContext = "${base}/${navigation.applicationMenu}">

<#if navigation.pageName != "create">
    <script type="text/javascript">
        function enableOnNormalVehicle(tableComponent){
            return ${vehicleCategory} != 0;
        }
    </script>
</#if>

<script type="text/javascript">
	var deleteVehicleURL = '${pageContext}/vscvehicles/delete.action';

    function showDeleteCurrentDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.vehicle'/>" ,
			    button1: "<@s.text name='delete.yesText.vehicle'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	function redirectCurrentDelete(){
      <#if vehicleId?has_content>
     	window.location="${pageContext}/vscvehicles/deleteCurrentVehicle.action?vehicleId=${vehicleId?c}";
      </#if>
    }
    
</script>

<#if navigation.pageName != "create">
    <div class="spacer"></div>
    <div class="menuHeader"><@s.text name='nav.vscvehicle.actions'/></div>
    <ul id="NavList">
        <#if navigation.pageName == "default">
            <#assign actionName = "nav.vscvehicle.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.vehicle.">
        </#if>
        <#if navigation.pageName == "view">
            <@action 
                name="nav.vehicle.create" 
                tableId=""
                id="a_createVehicle" 
                url="/${navigation.applicationMenu}/vscvehicles/create!input.action?typeId=${typeId}"
                tabIndex="251" 
                actionEnablers={}
                featureName="feature.voicelink.vsc.vehicle.create" />
            <@action 
                name="nav.vehicle.edit" 
                tableId=""
                id="a_editVehicle" 
                url="/${navigation.applicationMenu}/vscvehicles/edit!input.action?vehicleId=${vehicleId}"
                tabIndex="251" 
                actionEnablers={"enableOnNormalVehicle":"vsc.vehicle.edit.default.error"}
                featureName="feature.voicelink.vsc.vehicle.edit" />
            <@action            
                name="nav.vehilce.delete.current"
                tableId=""
                id="a_deleteVehicle"
                url="javascript: showDeleteCurrentDialog();"
                tabIndex="252"
                actionEnablers={"enableOnNormalVehicle":"vsc.vehicle.delete.default.error"}
                featureName="feature.voicelink.vsc.vehicle.delete" />
        <#elseif navigation.pageName == "edit">
            <@action 
                name="nav.vehicle.create" 
                tableId=""
                id="a_createVehicle" 
                url="/${navigation.applicationMenu}/vscvehicles/create!input.action?typeId=${typeId}"
                tabIndex="251" 
                actionEnablers={}
                featureName="feature.voicelink.vsc.vehicle.create" />
            <@action            
                name="nav.vehilce.delete.current"
                tableId=""
                id="a_deleteVehicle"
                url="javascript: showDeleteCurrentDialog();"
                tabIndex="252"
                actionEnablers={}
                featureName="feature.voicelink.vsc.vehicle.delete" />
        </#if>
    </ul>
</#if>

<script type="text/javascript">
    <#if navigation.pageName == "create" || navigation.pageName == "edit" || navigation.pageName == "view">
       <#global helpFiles="Content/PlaceholderPages/Vehicles_Fields.htm"/>
       contextHelpUrl = 152;
    <#else>  
       <#global helpFiles="Content/PlaceholderPages/VehicleSafetyChecksPlaceholderPage.htm"/>
       contextHelpUrl = 150;
    </#if>
</script>
