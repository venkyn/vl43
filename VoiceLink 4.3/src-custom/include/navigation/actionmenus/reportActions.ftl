<#include "/include/common/action.ftl" />
<#include "/include/common/customdialogdate.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}" />
<#assign calendarButton>
    <@s.text name='report.button.showcalendar'/>
</#assign>

<script language="Javascript">
// Get the date of now to populate the links with
var now = new Date();

// Turn now into a reports friend date format yyyy-MM-dd
var nowDate = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + now.getDate();

// Turn that date format into a nicely formatted UI link.
var nowStr = convertToDate(nowDate);

	function changeDateLabel(label)
	{   if (label=="start")
	        $('calendarLinkStart').innerHTML=convertToDate($('startDate').value);
        if (label=="end")
            $('calendarLinkEnd').innerHTML=convertToDate($('endDate').value);
	}

	function convertToDate(dateString)
	{   
		// Split the date into year part[0], month part[1], and date part[2]
		var parts=dateString.split("-");
		
		// Make a new date object based on those parts
		var d=new Date(parts[0], parts[1]-1, parts[2]);	
	
		// Build an array with the localized struts strings for week day
		var weekday=new Array(7);
		weekday[0]="<@s.text name='calender.day.short.sunday'/>";
		weekday[1]="<@s.text name='calender.day.short.monday'/>";
		weekday[2]="<@s.text name='calender.day.short.tuesday'/>";
		weekday[3]="<@s.text name='calender.day.short.wednesday'/>";
		weekday[4]="<@s.text name='calender.day.short.thursday'/>";
		weekday[5]="<@s.text name='calender.day.short.friday'/>";
		weekday[6]="<@s.text name='calender.day.short.saturday'/>";
		
		// Store the day based on the numerical value of the day into a var
		var dday=weekday[d.getDay()];
		
		// Build an array with the localized struts string for month
		var month=new Array(12);
		month[0]="<@s.text name='calender.month.long.january'/>";
		month[1]="<@s.text name='calender.month.long.february'/>";
		month[2]="<@s.text name='calender.month.long.march'/>";
		month[3]="<@s.text name='calender.month.long.april'/>";
		month[4]="<@s.text name='calender.month.long.may'/>";
		month[5]="<@s.text name='calender.month.long.june'/>";
		month[6]="<@s.text name='calender.month.long.july'/>";
		month[7]="<@s.text name='calender.month.long.august'/>";
		month[8]="<@s.text name='calender.month.long.september'/>";
		month[9]="<@s.text name='calender.month.long.october'/>";
		month[10]="<@s.text name='calender.month.long.november'/>";
		month[11]="<@s.text name='calender.month.long.december'/>";
		
		// Store the month based on the numerical value of the month into a var
		var dmonth=month[d.getMonth()];
		
		// Store the date from the orginal parts array
		var ddate=parts[2];
		
		// Store the year from the original parts array
		var dyear=parts[0];

		// Finally put all of the parts together in one string and return it to the caller
		var localizedDate=dday + ", " + dmonth + " " + ddate + ", " + dyear; 
		return localizedDate;
	}
// -->
    </script>

<style type="text/css">
table.reportLaunchDialog td{
    padding-bottom: 13px;
    padding-left: 5px;
    vertical-align: bottom;
    font-family: Arial, Helvetica,  San-Serif;
    font-size: 11px;
}
</style>
<style type="text/css">
table.reportLaunchRadio td{
   padding-bottom: 10px;
   padding-left: 0px;
   vertical-align: top;
   font-family: Arial, Helvetica,  San-Serif;
   font-size: 11px;
}
</style>

<div style="display: none;">
    <@s.textfield tabindex="1"
		name="startDate"
		id="startDate"
		size="15" maxlength="50"
		show="true"
		theme="simple"
		onchange="javascript:changeDateLabel('start');"
		/>

   <@s.textfield tabindex="2"
	   	name="endDate"
	   	id="endDate"
	   	size="15" maxlength="50"
	   	show="true"
	   	theme="simple"
		onchange="javascript:changeDateLabel('end');"
	   />
</div>
<div id="tempDialogContainer">
<div id="dialogDIV" style="display: none;">
    <table class="reportLaunchRadio">
        <tr><td>
        <input type="radio" class="radioSchedule" tabindex="2" value="0" id="intervalType" name="intervalType" checked>
        </td><td>
        <table class="reportLaunchRadio">
        <tr><td>
        <b><@s.text name="%{getText('report.launch.this.timespan')}"/></b>        
        </td><td style="padding-left: 10px;">
        <div id="thisTimeSpan"></div>
        </td></tr>
        </table>
        </td></tr>
        <tr><td>
        <input type="radio" class="radioSchedule" tabindex="2" value="1" id="intervalType" name="intervalType">
        </td><td><b><@s.text name="%{getText('report.launch.different.timespan')}"/></b>        
        </tr><tr>
        <td></td>
        <td style="padding-bottom: 10px">
        <table class="reportLaunchDialog">
        <tr><td>
        <@s.text name="%{getText('report.launch.from')}"/>
        </td><td>
       <@customdialogdate
	      dialogId="priority_date_start"
	      header="report.selectdate"
	      dateInputFieldId="startDate"
	      callingButtonId="calendarLinkStart"
	      multiPage="false"/>
        <a id="calendarLinkStart" href="#"><@s.text name="%{getText('report.launch.start.date')}"/></a>
        
		</td><td>
         <@s.select tabindex="4"
            cssStyle="width: 40px"
		    theme="simple"
		    label=""
		    name="fromHours"
            id="fromHours"
            list="scheduleHoursMap"
            listKey="key"
            listValue="value"
            emptyOption="false"
            size="1"
            length="50"
            show="true"
            required="false"
            readonly="false"/>
       </td><td>
        <@s.text name="%{getText('semicolon')}"/>
       </td><td class="scheduleTime">
		<@s.select tabindex="4"
            cssStyle="width: 40px"
		    theme="simple"
		    label="%{getText('semicolon')}"
		    name="fromMinutes"
            id="fromMinutes"
            list="scheduleMinutesMap"
            listKey="value"
            listValue="key"
            emptyOption="false"
            size="1"
            length="50"
            show="true"
            required="false"
            readonly="false"/>
        </td><td>
           <@s.text name="%{getText('schedule.edit.daily.timeformat.24')}"/>
		</td></tr>
        <tr><td>
		<@s.text name="%{getText('report.launch.to')}"/>
		</td><td>
        <@customdialogdate
	      dialogId="priority_date_end"
	      header="report.selectdate"
	      dateInputFieldId="endDate"
	      callingButtonId="calendarLinkEnd"
	      multiPage="false"/>
        <a id="calendarLinkEnd" href="#"><@s.text name="%{getText('report.launch.end.date')}"/></a>        
		</td><td>
         <@s.select tabindex="4"
            cssStyle="width: 40px"
		    theme="simple"
		    label=""
		    name="toHours"
            id="toHours"
            list="scheduleHoursMap"
            listKey="key"
            listValue="value"
            emptyOption="false"
            size="1"
            length="50"
            show="true"
            required="false"
            readonly="false"/>
       </td><td>
        <@s.text name="%{getText('semicolon')}"/>
       </td><td class="scheduleTime">
		<@s.select tabindex="4"
            cssStyle="width: 40px"
		    theme="simple"
		    label=""
		    name="toMinutes"
            id="toMinutes"
            list="scheduleMinutesMap"
            listKey="value"
            listValue="key"
            emptyOption="false"
            size="1"
            length="50"
            show="true"
            required="false"
            readonly="false"/>
        </td><td>
           <@s.text name="%{getText('schedule.edit.daily.timeformat.24')}"/>
		</td></tr>
        </table>
		</td></tr>
	</table>
</div>
</div>

<script type="text/javascript">

	var deleteReportURL = '${pageContext}/report/delete.action';

    $('calendarLinkStart').innerHTML=nowStr;
    $('startDate').value=nowDate;
    $('calendarLinkEnd').innerHTML=nowStr;
    $('endDate').value=nowDate;

	 function showDeleteReportDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",
				body: "<@s.text name='delete.body.reports'/>" ,
			    button1: "<@s.text name='delete.yesText.reports'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, reportObj,deleteReportURL), "");
	}


    function showDeleteCurrentReportDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",
				body: "<@s.text name='delete.body.report'/>" ,
			    button1: "<@s.text name='delete.yesText.report'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}

	function showLaunchReportDialog() {
	    cancelLaunchReport()
		var dialogProperties = {
			    title: "<@s.text name='report.launch.title'/>",
				body: $('dialogDIV'),
			    button1: "<@s.text name='report.launch.yesText'/>",
				button2: "<@s.text name='report.launch.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectLaunchReport,"",""), cancelLaunchReport);
		$("dialogDIV").style.display = "inline";
        $("thisTimeSpan").innerHTML= reportObj.getSelectedObjects()[0].interval;
	}

	function showLaunchCurrentReportDialog() {
	    cancelLaunchReport()
		var dialogProperties = {
			    title: "<@s.text name='report.launch.title'/>",
				body: $('dialogDIV'),
			    button1: "<@s.text name='report.launch.yesText'/>",
				button2: "<@s.text name='report.launch.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectLaunchCurrentReport,"",""), cancelLaunchReport);
		$("dialogDIV").style.display="inline";
		<#if report?has_content>
		  <#if report.interval?has_content>
            $('thisTimeSpan').innerHTML= "${report.interval}";
          </#if>
        </#if>
	}

	function cancelLaunchReport() {
	    $("dialogDIV").style.display="none";
	    $("tempDialogContainer").appendChild($("dialogDIV"));
	}

	 function redirectCurrentDelete(){
      <#if reportId?has_content>
     	window.location="${pageContext}/report/deleteCurrentReport.action?reportId=${reportId?c}";
      </#if>
    }

    function getLaunchParams() {
        var urlParams = '&startDate=' + $('startDate').value + '-' + $('fromHours').value + '-' + $('fromMinutes').value;
        urlParams += '&endDate=' + $('endDate').value + '-' + $('toHours').value + '-' + $('toMinutes').value;
        return (urlParams);
    }

    function launchReport(idStr) {
        var urlParams = idStr;
        if (document.getElementsByName('intervalType')[1].checked)
            urlParams += getLaunchParams();
      	window.open('${pageContext}/report/launch.action?' + urlParams);
    }

    function redirectLaunchReport(){
        launchReport('reportId=' + reportObj.getSelectedIds());
        //cancelLaunchReport();
    }

    function redirectLaunchCurrentReport(){
    <#if reportId?has_content>
        launchReport('reportId=${reportId?c}');
    </#if>
        //cancelLaunchReport();
    }

</script>
<#if navigation.pageName != "create">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.report.actions'/></div>
	<ul id="NavList">
        <#if navigation.pageName == "default">
            <#assign actionName = "nav.report.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.report.">
        </#if>
		<#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
			    name=actionName + "view"
			    tableId="listReportsTable"
	            id="a_viewReport"
	            url="javascript:gotoUrlWithSelected(reportObj, \\'${pageContext}/report/view.action\\', \\'reportId\\');"
	            tabIndex="251"
                actionEnablers={"enableOnOne":"report.view.disabled.selection"}
	            featureName="feature.voicelink.report.view" />
	    </#if>
	    <#if navigation.pageName != "create">
		    <@action
		         name=actionName + "create"
		         tableId="listReportsTable"
	             id="a_createReport"
	             url="/${navigation.applicationMenu}/report/create!input.action"
	             tabIndex="251"
	             actionEnablers={}
	             featureName="feature.voicelink.report.create" />
	    </#if>
   	    <#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
                name=actionName + "edit"
                tableId="listReportsTable"
		        id="a_editReport"
                tabIndex="253"
                url="javascript:gotoUrlWithSelected(reportObj,\\'${pageContext}/report/edit!input.action\\',\\'reportId\\');"
                actionEnablers={"enableOnOne":"report.edit.disabled.selection"}
                featureName="feature.voicelink.report.edit" />
        </#if>
        <#if navigation.pageName == "view" || navigation.pageName == "schedule">
			<@action
                name=actionName + "edit"
                tableId="listReportsTable"
		        id="a_editReport"
                tabIndex="253"
                url="/${navigation.applicationMenu}/report/edit!input.action?reportId=${reportId?c}"
                actionEnablers={}
                featureName="feature.voicelink.report.edit" />
        </#if>
        <#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
			    name=actionName + "delete"
			    tableId="listReportsTable"
		        id="a_deleteReport"
		        url="javascript: showDeleteReportDialog();"
		        tabIndex="252"
	            actionEnablers={"enableOnMoreThanNone":"report.delete.disabled.selection"}
		        featureName="feature.voicelink.report.delete" />
		</#if>
        <#if navigation.pageName == "view" || navigation.pageName == "create" || navigation.pageName == "edit" || navigation.pageName == "schedule">
			<@action
			    name=actionName + "delete"
			    tableId="listReportsTable"
		        id="a_deleteReport"
		        url="javascript: showDeleteCurrentReportDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.voicelink.report.delete" />
        </#if>
		<#if navigation.pageName == "list" || navigation.pageName == "default" >
			<@action
			    name=actionName + "schedule"
			    tableId="listReportsTable"
	            id="a_scheduleReport"
	            url="javascript:gotoUrlWithSelected(reportObj, \\'${pageContext}/report/schedule!input.action\\', \\'reportId\\');"
	            tabIndex="251"
                actionEnablers={"enableOnOne":"report.schedule.disabled.selection"}
	            featureName="feature.voicelink.report.schedule" />
	    </#if>
		<#if navigation.pageName == "edit" || navigation.pageName == "view" >
			<@action
			    name=actionName + "schedule"
			    tableId="listReportsTable"
	            id="a_scheduleReport"
	            url="/${navigation.applicationMenu}/report/schedule!input.action?reportId=${reportId?c}"
	            tabIndex="251"
                actionEnablers={}
	            featureName="feature.voicelink.report.schedule" />
	    </#if>
	    <#if navigation.pageName == "list" || navigation.pageName == "default" >
			<@action
			    name=actionName + "launch"
			    tableId="listReportsTable"
		        id="a_launchReport"
		        url="javascript: showLaunchReportDialog();"
		        tabIndex="252"
	            actionEnablers={"enableOnOne":"report.launch.disabled.selection"}
		        featureName="feature.voicelink.report.launch" />
        </#if>
	    <#if navigation.pageName == "view" || navigation.pageName == "edit" || navigation.pageName == "schedule">
			<@action
			    name=actionName + "launch"
			    tableId="listReportsTable"
		        id="a_launchReport"
		        url="javascript: showLaunchCurrentReportDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.voicelink.report.launch" />
        </#if>
	</ul>
</#if>

<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.operatorTeam.actions'/></div>
<ul class="NavList">
	<@action
        name="nav.selection.operatorTeam.manage"
        tableId="listReportsTable"
		id="a_manageOperatorTeams"
        tabIndex="252"
        url="/selection/operatorTeam/list.action"
        actionEnablers={}
        featureName="feature.voicelink.operatorTeam.view" />
</ul>
<script type="text/javascript">
<#if navigation.pageName == "create" || navigation.pageName == "edit" || navigation.pageName == "view">
   <#global helpFiles="Core/ReportFields.htm"/>
   contextHelpUrl = 134;
<#elseif navigation.pageName == "schedule">
   <#global helpFiles="Core/ReportFields.htm"/>
   contextHelpUrl = 136;
<#else>
   <#global helpFiles="PlaceholderPages/ReportPagePlaceholder.htm"/>
   contextHelpUrl = 28;
</#if>
</script>