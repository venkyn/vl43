<#include "/include/common/action.ftl" />
<#include "/include/common/customdialog.ftl">
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript">

	var deleteLocationURL = '${pageContext}/location/delete.action';
	var setToReplenihsedURL = '${pageContext}/location/setToReplenished.action';
	var clearInvalidLotURL = '${pageContext}/location/clearInvalidLot.action';
	
	function showDeleteLocationDialog() {
		var dialogProperties = {				
				<#assign dialogTitle> <@s.text name='delete.title'/> </#assign>		
				<#assign dialogBody> <@s.text name='delete.body.locations'/> </#assign>	
				<#assign dialogButton1> <@s.text name='delete.yesText.locations'/> </#assign>
				<#assign dialogButton2> <@s.text name='delete.noText'/> </#assign>
				
				title: "${dialogTitle?js_string}",			    
				body: "${dialogBody?js_string}",
			    button1: "${dialogButton1?js_string}",
				button2: "${dialogButton2?js_string}"}	
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, locationObj,deleteLocationURL), "");
	}	

	 function showDeleteCurrentLocationDialog() {
		var dialogProperties = {			
				<#assign dialogTitle> <@s.text name='delete.title'/> </#assign>		
				<#assign dialogBody> <@s.text name='delete.body.location'/> </#assign>	
				<#assign dialogButton1> <@s.text name='delete.yesText.location'/> </#assign>
				<#assign dialogButton2> <@s.text name='delete.noText'/> </#assign>
				
				title: "${dialogTitle?js_string}",			    
				body: "${dialogBody?js_string}",
			    button1: "${dialogButton1?js_string}",
				button2: "${dialogButton2?js_string}"}	
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	function redirectCurrentDelete(){
      <#if locationId?has_content>
     	window.location="${pageContext}/location/deleteCurrentLocation.action?locationId=${locationId?c}";
      </#if>
    }
	
	
	 function showSetToReplenishedDialog() {
		var dialogProperties = {		
			    <#assign dialogTitle> <@s.text name='setToReplenished.title'/> </#assign>		
				<#assign dialogBody> <@s.text name='setToReplenished.body.locations'/> </#assign>	
				<#assign dialogButton1> <@s.text name='setToReplenished.yesText.locations'/> </#assign>
				<#assign dialogButton2> <@s.text name='setToReplenished.noText'/> </#assign>
				
				title: "${dialogTitle?js_string}",			    
				body: "${dialogBody?js_string}",
			    button1: "${dialogButton1?js_string}",
				button2: "${dialogButton2?js_string}"}	
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected,locationObj,setToReplenihsedURL), "");
	}	
	
	function showClearInvalidLotDialog() {
		var dialogProperties = {				
				<#assign dialogTitle> <@s.text name='clearInvalidLot.title'/> </#assign>		
				<#assign dialogBody> <@s.text name='clearInvalidLot.body.locations'/> </#assign>	
				<#assign dialogButton1> <@s.text name='clearInvalidLot.yesText.locations'/> </#assign>
				<#assign dialogButton2> <@s.text name='clearInvalidLot.noText'/> </#assign>
				
				title: "${dialogTitle?js_string}",			    
				body: "${dialogBody?js_string}",
			    button1: "${dialogButton1?js_string}",
				button2: "${dialogButton2?js_string}"}	
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected,locationObj,clearInvalidLotURL), "");
	}

</script>
<#if navigation.pageName != "create">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.location.actions' /></div>
	<ul class="NavList">
		<#if navigation.pageName == "list" || navigation.pageName == "default">
		    <@action
				name="nav.location.view"
				tableId="listLocationsTable"
				id="a_viewLocation"		
				url="javascript:gotoUrlWithSelected(locationObj, \\'${pageContext}/location/view.action\\', \\'locationId\\');"
				tabIndex="251"
				actionEnablers={"enableOnOne":"location.view.disabled.selection"}
				featureName="feature.voicelink.location.view" />
			<@action
				name="nav.location.create"
				tableId="listLocationsTable"
				id="a_createLocation"		
				url="/${navigation.applicationMenu}/location/create!input.action"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.location.create" />
			<@action
				name="nav.location.edit"
				tableId="listLocationsTable"
				id="a_editLocation"
				url="javascript:gotoUrlWithSelected(locationObj, \\'${pageContext}/location/edit!input.action\\', \\'locationId\\');"
				tabIndex="253"
				actionEnablers={"enableOnOne":"location.edit.disabled.selection"}
				featureName="feature.voicelink.location.edit" /> 			
			<@action
				name="nav.location.delete"
				tableId="listLocationsTable"
				id="a_deleteLocation"
				url="javascript:showDeleteLocationDialog();"
				tabIndex="254"
				actionEnablers={"enableOnMoreThanNone":"location.delete.disabled.selection"}
				featureName="feature.voicelink.location.delete" />			
			<@action
				name="nav.location.setToReplenished"
				tableId="listLocationsTable"
				id="a_setToReplenished"
				url="javascript:showSetToReplenishedDialog();"
				tabIndex="254"
				actionEnablers={"enableOnMoreThanNone":"location.setToReplenished.disabled.selection"}
				featureName="feature.voicelink.location.setToReplenished" />	
					
			<@action
				name="nav.location.clearInvalidLot"
				tableId="listLocationsTable"
				id="a_clearInvalidLot"
				url="javascript:showClearInvalidLotDialog();"
				tabIndex="254"
				actionEnablers={"enableOnMoreThanNone":"location.clearInvalidLot.disabled.selection"}
				featureName="feature.voicelink.location.clearInvalidLot" />
		<#elseif navigation.pageName == "view">
			<@action
				name="nav.location.create"
				tableId="listLocationsTable"
				id="a_createLocation"		
				url="/${navigation.applicationMenu}/location/create!input.action"
				tabIndex="251"
				actionEnablers={}
				featureName="feature.voicelink.location.create" />
			<@action
				name="nav.location.edit.current"
				tableId="listLocationsTable"
				id="a_editLocation"
				url="/${navigation.applicationMenu}/location/edit!input.action?locationId=${locationId?c}"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.location.edit" />
			<@action
				name="nav.location.delete.current"
				tableId="listLocationsTable"
				id="a_deleteLocation"
				url="javascript:showDeleteCurrentLocationDialog();"
				tabIndex="253"
				actionEnablers={}
				featureName="feature.voicelink.location.delete" />			
		
		<#elseif navigation.pageName == "edit">
			<@action
				name="nav.location.create"
				tableId="listLocationsTable"
				id="a_createLocation"		
				url="/${navigation.applicationMenu}/location/create!input.action"
				tabIndex="251"
				actionEnablers={}
				featureName="feature.voicelink.location.create" />
			<@action
				name="nav.location.delete.current"
				tableId="listLocationsTable"
				id="a_deleteLocation"
				url="javascript:showDeleteCurrentLocationDialog();"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.location.delete" />			
			
		</#if>
	</ul>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "create" || navigation.pageName == "edit" || navigation.pageName == "view">
   <#global helpFiles="Core/LocationFields.htm"/>
   contextHelpUrl = 51;
<#else>  
   <#global helpFiles="PlaceholderPages/LocationsPagePlaceholder.htm"/>
   contextHelpUrl = 14;
</#if>
</script>