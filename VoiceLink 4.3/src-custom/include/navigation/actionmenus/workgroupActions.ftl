<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript">
	var deleteWorkGroupURL = '${pageContext}/workgroup/delete.action';
	
	
	 function showDeleteWorkGroupDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='workgroup.delete.prompt.body'/>" ,
			    button1: "<@s.text name='workgroup.delete.prompt.yesText'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, tableObj,deleteWorkGroupURL), "");
	}	

	 function showDeleteCurrentWorkGroupDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='workgroup.delete.prompt.body'/>" ,
			    button1: "<@s.text name='workgroup.delete.prompt.yesText'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete, "",""), "");
	}	
	
	 function redirectCurrentDelete(){
      <#if workgroupId?has_content>
     	window.location="deleteCurrentWorkgroup.action?workgroupId=${workgroupId?c}";
      </#if>
    }
		
</script>
<#if navigation.pageName != "create">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.workgroup.actions' /></div>
	<ul class="NavList">
		 <#if navigation.pageName == "list" || navigation.pageName == "default">
		    <@action
				name="nav.workgroup.view"
				tableId="listWorkgroupsTable"
				id="a_viewWorkgroup"	
				url="javascript:gotoUrlWithSelected(tableObj, \\'${pageContext}/workgroup/view.action\\', \\'workgroupId\\');"
				tabIndex="251"
				actionEnablers={"enableOnOne":"workgroup.view.disabled.selection"}
				featureName="feature.voicelink.workgroup.view" />
			<@action
				name="nav.workgroup.create"
				tableId="listWorkgroupsTable"
				id="a_createWorkgroup"
				url="/${navigation.applicationMenu}/workgroup/create!input.action"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.workgroup.create" />
			<@action
				name="nav.workgroup.edit"
				tableId="listWorkgroupsTable"
				id="a_editWorkgroup"
				url="javascript:gotoUrlWithSelected(tableObj, \\'${pageContext}/workgroup/edit!input.action\\', \\'workgroupId\\');"
				tabIndex="253"
				actionEnablers={"enableOnOne":"workgroup.edit.disabled.selection"}
				featureName="feature.voicelink.workgroup.edit" /> 
			<@action
				name="nav.workgroup.delete"
				tableId="listWorkgroupsTable"
				id="a_deleteWorkgroups"
				url="javascript:showDeleteWorkGroupDialog();"
				tabIndex="254"
				actionEnablers={"enableOnMoreThanNone":"workgroup.delete.disabled.selection"}
				featureName="feature.voicelink.workgroup.delete" />			
			
		<#elseif navigation.pageName == "view">
			<@action
				name="nav.workgroup.create"
				tableId="listWorkgroupsTable"
				id="a_createWorkgroup"		
				url="/selection/workgroup/create!input.action"
				tabIndex="251"
				actionEnablers={}
				featureName="feature.voicelink.workgroup.create" />
			<@action
				name="nav.workgroup.edit.current"
				tableId="listWorkgroupsTable"
				id="a_editWorkgroup"
				url="/${navigation.applicationMenu}/workgroup/edit!input.action?workgroupId=${workgroupId?c}"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.workgroup.edit" />
			<@action
				name="nav.workgroup.delete.current"
				tableId="listWorkgroupsTable"
				id="a_deleteWorkgroup"
				url="javascript:showDeleteCurrentWorkGroupDialog();"
				tabIndex="253"
				actionEnablers={}
				featureName="feature.voicelink.workgroup.delete" />
			
		<#elseif navigation.pageName == "edit">
			<@action 
				name="nav.workgroup.create" 
				tableId="listWorkgroupsTable"
				id="a_createWorkgroup" 
				url="/${navigation.applicationMenu}/workgroup/create!input.action" 
				tabIndex="251" 
				actionEnablers={} 
				featureName="feature.voicelink.workgroup.create" />
			<@action
				name="nav.workgroup.delete.current"
				tableId="listWorkgroupsTable"
				id="a_deleteWorkgroup"
				url="javascript:showDeleteCurrentWorkGroupDialog();"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.workgroup.delete" />
			
		</#if>
	</ul>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "create" || navigation.pageName == "edit" || navigation.pageName == "view">
   <#global helpFiles="Core/WorkgroupFields.htm"/>
   contextHelpUrl = 54;
<#else>  
   <#global helpFiles="PlaceholderPages/WorkgroupsPagePlaceholder.htm"/>
   contextHelpUrl = 38;
</#if>
</script>
