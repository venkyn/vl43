<#include "/include/common/action.ftl" />
	 <div class="spacer"></div>
	    <div class="menuHeader"><@s.text name='nav.licensing.actions' /></div>
	    <ul class="NavList">
	    <@action name="nav.licensing.view" 
	             tableId=""
	             id="a_licensingView" 
	             url="/admin/licensing/view.action" 
	             tabIndex="251"
	             actionEnablers={} 
	             featureName="feature.appAdmin.licensing.view" />
	    <@action name="nav.licensing.import" 
	             tableId=""
	             id="a_licensingImport" 
	             url="/admin/licensing/beginLicenseFlow.action" 
	             tabIndex="252"
	             actionEnablers={} 
	             featureName="feature.appAdmin.licensing.import" />
	    <@action name="nav.licensing.agreement" 
	             tableId=""
	             id="a_licensingAgreement" 
	             url="/admin/licensing/agreement.action" 
	             tabIndex="253"
	             actionEnablers={} 
	             featureName="feature.appAdmin.licensing.view" />
	    </ul>
<script type="text/javascript">
<#if navigation.pageName == "view">
	var contextHelpUrl = 5600;
<#elseif navigation.pageName == "enterLicenseFile">
	var contextHelpUrl = 5610;
<#elseif navigation.pageName == "agreement!showAgreement">
	//var contextHelpUrl = 46;
</#if>
</script>
