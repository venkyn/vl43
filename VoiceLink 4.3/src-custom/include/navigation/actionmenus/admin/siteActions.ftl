<#assign displayEap=false>
<#list action.availablePluginModules as module>
    <#if module.name == "plugin.module.voiceconsole">
        <#assign displayEap=true>
    </#if>
</#list>
<#include "/include/common/action.ftl" />
<script type="text/javascript">
    function redirectCurrentDelete(){
      <#if siteId?has_content>
     	window.location="${base}/admin/site/deleteCurrentSite.action?siteId=${siteId?c}";
      </#if>
    }

	<#if navigation.pageName == "list">
		var contextHelpUrl = 5400;
	<#elseif navigation.pageName == "view">
		var contextHelpUrl = 5400;
	<#elseif navigation.pageName == "edit">
	    var contextHelpUrl = 5410;
	<#elseif navigation.pageName == "changeEapSiteCredentials">
		var contextHelpUrl = 52;
	<#elseif navigation.pageName == "configureBehavior" ||
			 navigation.pageName == "configureLDAP" ||
			 navigation.pageName == "credentials" ||
			 navigation.pageName == "eapSummary">
	    var contextHelpUrl = 51;
	<#elseif navigation.pageName == "create">
		var contextHelpUrl = 5410;
	<#else>
		var contextHelpUrl = 34;
	</#if>
</script>	
<#if (navigation.pageName != "viewProfile" && 
      navigation.pageName != "create" && 
      navigation.pageName != "configureBehavior" && 
      navigation.pageName != "configureLDAP" &&
      navigation.pageName != "credentials" && 
      navigation.pageName != "eapSummary" &&
      navigation.pageName != "configureEAPFlow" &&
      navigation.pageName != "changeEapSiteCredentials") ||
     showTrust?default(false)>
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.site.actions' /></div>
	<ul class="NavList">
		<#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
			    name="nav.site.view"
			    tableId="listSitesTable"
	            id="a_viewSite"
	            url="javascript:gotoUrlWithSelected(siteObj, \\'${base}/admin/site/view.action\\', \\'siteId\\');"
	            tabIndex="251"
	            actionEnablers={"enableOnOne": "site.view.disabled.selection"}
	            featureName="feature.appAdmin.site.view" />
	        <@action name="nav.site.create" 
	             tableId="listSitesTable"
	             id="a_createSite" 
	             url="/admin/site/create!input.action" 
	             tabIndex="251" 
	             actionEnablers={} 
	             featureName="feature.appAdmin.site.create" />
			<@action
				name="nav.site.edit"
				tableId="listSitesTable"
				id="a_editSite"
				url="javascript:gotoUrlWithSelected(siteObj, \\'${base}/admin/site/edit!input.action\\', \\'siteId\\');"
				tabIndex="254"
				actionEnablers={"enableOnOne":"site.edit.disabled.selection"}
				featureName="feature.appAdmin.site.edit" />
			<@action
				name="nav.site.delete"
				tableId="listSitesTable"
				id="a_deleteSite"
				url="javascript: showDeleteSiteDialog();"
				tabIndex="253"
				actionEnablers={"enableOnMoreThanNone":"site.delete.disabled.selection"}
				featureName="feature.appAdmin.site.delete" />
			<#if displayEap>
			    <@action
					name="nav.site.configureEAP"
					tableId="listSitesTable"
					id="a_configureEAP"
					url="javascript:gotoUrlWithSelected(siteObj, \\'${base}/admin/site/beginConfigureEAPFlow.action\\', \\'siteId\\');"
					tabIndex="254"
					actionEnablers={"enableOnOne":"site.configureEAP.disabled.selection"}
					featureName="feature.appAdmin.site.configureEAP" />
	            <@action
	                name="nav.site.changeEapSiteCredentials"
	                tableId="listSitesTable"
	                id="a_changeEapSiteCredentials"
	                url="javascript:gotoUrlWithSelected(siteObj, \\'${base}/admin/site/changeEapSiteCredentials!input.action\\', \\'siteId\\');"
	                tabIndex="255"
	                actionEnablers={"enableOnOne":"site.changeEapSiteCredentials.disabled.selection",
	                                "enableOnSiteEAP":"site.changeEapSiteCredentials.disabled.eap"}
	                featureName="feature.appAdmin.site.configureEAP" />
				<@action
		            name="nav.site.eapOperatorLogin"
		            tableId="listSitesTable"
		            id="a_eapOperatorLogin"
		            url="javascript:gotoUrlWithSelected(siteObj, operatorLoginURL, \\'siteId\\');"
		            tabIndex="256"
		            actionEnablers={"enableOnOne":"site.changeEapSiteCredentials.disabled.selection",
		            				"enableOnOperatorEAP":"site.changeEapOperatorCredentials.single.disabled"}
		            featureName="feature.appAdmin.site.configureEAP" />			                
			</#if>
			
		<#elseif navigation.pageName == "view">
			<@action 
				name="nav.site.create" 
				tableId="listSitesTable"
				id="a_createSite" 
				url="/admin/site/create!input.action" 
				tabIndex="251" 
				actionEnablers={} 
				featureName="feature.appAdmin.site.create" />
			<@action
				name="nav.site.edit.currentSite"
				tableId="listSitesTable"
				id="a_editSite"
				url="/admin/site/edit!input.action?siteId=${siteId?c}"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.appAdmin.site.edit" />
			<@action
				name="nav.site.delete.currentSite"
				tableId="listSitesTable"
				id="a_deleteSite"
				url="javascript: showDeleteCurrentSiteDialog();"
				tabIndex="253"
				actionEnablers={}
				featureName="feature.appAdmin.site.delete" />
			<#if displayEap>
			    <@action
				    name="nav.site.configureEAP.currentSite"
					tableId="listSitesTable"
					id="a_configureEAP"
					url="/admin/site/beginConfigureEAPFlow.action?siteId=${siteId?c}"
					tabIndex="254"
					actionEnablers={}
				    featureName="feature.appAdmin.site.configureEAP" />
		        <@action
		            name="nav.site.changeEapSiteCredentials"
		            tableId="listSitesTable"
		            id="a_changeEapSiteCredentials"
		            url="/admin/site/changeEapSiteCredentials!input.action?siteId=${siteId?c}"
		            tabIndex="255"
		            actionEnablers={"enableForEAP":"site.changeEapSiteCredentials.single.disabled"}
		            featureName="feature.appAdmin.site.configureEAP" />
				<@action
		            name="nav.site.eapOperatorLogin"
		            tableId="listSitesTable"
		            id="a_eapOperatorLogin"
		            url="/operatorLogin.action?siteId=${siteId?c}"
		            tabIndex="256"
		            actionEnablers={"enableOnOperatorEAPSingle":"site.changeEapSiteCredentials.single.disabled"}
		            featureName="feature.appAdmin.site.configureEAP" />		            
            </#if>
		<#elseif navigation.pageName == "edit">
			<@action 
				name="nav.site.create" 
				tableId="listSitesTable"
				id="a_createSite" 
				url="/admin/site/create!input.action" 
				tabIndex="252" 
				actionEnablers={} 
				featureName="feature.appAdmin.site.create" />
		   <@action
				name="nav.site.delete.currentSite"
				tableId="listSitesTable"
				id="a_deleteSite"
				url="javascript: showDeleteCurrentSiteDialog();"
				tabIndex="251"
				actionEnablers={}
				featureName="feature.appAdmin.site.delete" />
			<#if displayEap>
			    <@action
					name="nav.site.configureEAP.currentSite"
					tableId="listSitesTable"
					id="a_configureEAP"
					url="/admin/site/beginConfigureEAPFlow.action?siteId=${siteId?c}"
					tabIndex="254"
					actionEnablers={}
					featureName="feature.appAdmin.site.configureEAP" />
                <@action
                    name="nav.site.changeEapSiteCredentials"
                    tableId="listSitesTable"
                    id="a_changeEapSiteCredentials"
                    url="/admin/site/changeEapSiteCredentials!input.action?siteId=${siteId?c}"
                    tabIndex="255"
                    actionEnablers={"enableForEAP":"site.changeEapSiteCredentials.single.disabled"}
                    featureName="feature.appAdmin.site.configureEAP" />
				<@action
		            name="nav.site.eapOperatorLogin"
		            tableId="listSitesTable"
		            id="a_eapOperatorLogin"
		            url="/operatorLogin.action?siteId=${siteId?c}"
		            tabIndex="256"
		            actionEnablers={"enableOnOperatorEAPSingle":"site.changeEapSiteCredentials.single.disabled"}
		            featureName="feature.appAdmin.site.configureEAP" />		                    
			</#if>
				
		 <#elseif showTrust?default(false)>
		    <@action 
				name="nav.site.trust" 
				tableId="listSitesTable"
				id="a_trust" 
				url="javascript: getViewTrust(viewTrustPageURL);" 
				tabIndex="251" 
				actionEnablers={} 
				featureName="feature.appAdmin.site.configureEAP" />
	     </#if>
	</ul>
</#if>
