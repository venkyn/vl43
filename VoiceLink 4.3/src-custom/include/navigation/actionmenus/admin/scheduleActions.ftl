<#include "/include/common/action.ftl" />
<#if navigation.pageName != "editJob">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.schedule.actions' /></div>
	<div id="menuBlade_listSchedulesTable" style="overflow: hidden;">
	<ul id="bladeList_listSchedulesTable" class="NavList">
		<#if navigation.pageName == "list" || navigation.pageName == "default" ><#t/>
	        <@action
				name="nav.schedule.view"
				tableId="listSchedulesTable"
				id="a_viewJob"
				url="javascript:gotoUrlWithSelected(scheduleObj, \\'${base}/admin/schedule/view.action\\', \\'jobId\\');"
				tabIndex="251"
				actionEnablers={"enableOnOne":"schedule.view.disabled.selection"}
				featureName="feature.appAdmin.schedule.view"	/>
			<@action
				name="nav.schedule.edit"
				tableId="listSchedulesTable"
				id="a_editJob"
				url="javascript:gotoUrlWithSelected(scheduleObj, \\'${base}/admin/schedule/edit!input.action\\', \\'jobId\\');"
				tabIndex="252"
				actionEnablers={"enableOnOne":"schedule.edit.disabled.selection"}
				featureName="feature.appAdmin.schedule.edit"	/>
			<@action 
				name="nav.schedule.run" 
				tableId="listSchedulesTable"
				id="a_runJob" 
				url="javascript:performActionOnSelected(scheduleObj, \\'${base}/admin/schedule/run.action\\');"
				tabIndex="253" 
				actionEnablers={"enableOnMoreThanNone":"schedule.run.disabled.selection"}
				featureName="feature.appAdmin.schedule.edit" />	
<#--			<@action 
				name="nav.schedule.stop" 
				tableId="listSchedulesTable"
				id="a_stopJob" 
				url="javascript:performActionOnSelected(scheduleObj, \\'${base}/admin/schedule/stop.action\\');"
				tabIndex="254" 
				actionEnablers={"enableOnMoreThanNone":"schedule.stop.disabled.selection"}
				featureName="feature.appAdmin.schedule.edit" />		-->
		<#elseif navigation.pageName == "view">
			<@action
				name="nav.schedule.edit.currentSchedule"
				tableId=""
				id="a_editJob"
				url="/admin/schedule/edit!input.action?jobId=${jobId}"
				tabIndex="251"
				actionEnablers={}
				featureName="feature.appAdmin.schedule.edit"	/>
			<@action 
				name="nav.schedule.run.currentSchedule" 
				tableId=""
				id="a_runJob" 
				url="javascript:performAction(${jobId}, \'${base}/admin/schedule/run.action\');"
				tabIndex="252"
				actionEnablers= {}
				featureName="feature.appAdmin.schedule.edit" />	
<#--			<@action 
				name="nav.schedule.stop.currentSchedule" 
				tableId=""
				id="a_stopJob" 
				url="javascript:performAction(${jobId}, \'${base}/admin/schedule/stop.action\');"
				tabIndex="253" 
				actionEnablers= {}
				featureName="feature.appAdmin.schedule.edit" /> -->
		<#elseif navigation.pageName == "edit">
			<@action 
				name="nav.schedule.run.currentSchedule" 
				tableId=""
				id="a_runJob" 
				url="javascript:performAction(${jobId}, \'${base}/admin/schedule/run.action\');"
				tabIndex="251"
				actionEnablers= {}
				featureName="feature.appAdmin.schedule.edit" />	
<#--			<@action 
				name="nav.schedule.stop.currentSchedule" 
				tableId=""
				id="a_stopJob" 
				url="javascript:performAction(${jobId}, \'${base}/admin/schedule/stop.action\');"
				tabIndex="252" 
				actionEnablers= {}
				featureName="feature.appAdmin.schedule.edit" /> -->
		</#if>
	</ul>
	</div>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "view" || navigation.pageName == "list">
	contextHelpUrl = 5300;
<#elseif navigation.pageName == "edit">
	contextHelpUrl = 5310;
<#else>
	contextHelpUrl = 31;
</#if>

</script>
