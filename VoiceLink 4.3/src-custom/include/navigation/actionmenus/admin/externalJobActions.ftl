<#include "/include/common/action.ftl" />
<#include "/include/common/customdialog.ftl">
<#assign pageContext = "${base}/${navigation.applicationMenu}">

<script type="text/javascript">

    var deleteExternalJobURL = '${pageContext}/externalJob/delete.action';
    
     function showDeleteExternalJobDialog() {
        var dialogProperties = {
                title: "<@s.text name='delete.title'/>",
                body: "<@s.text name='delete.body.externalJobs'/>" ,
                button1: "<@s.text name='delete.yesText.externalJobs'/>",
                button2: "<@s.text name='delete.noText'/>"}
        buildCustomDialog(dialogProperties,
            partial(performActionOnSelected, externalJobObj,deleteExternalJobURL), "");
    }
    
    function showDeleteCurrentExternalJobDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.externalJob'/>" ,
			    button1: "<@s.text name='delete.yesText.externalJob'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	 function redirectCurrentDelete(){
      <#if externalJobId?has_content>
     	window.location="${pageContext}/externalJob/deleteCurrentExternalJob.action?externalJobId=${externalJobId?c}";
      </#if>
    } 
    
    function editJobSchedule(obj) {
		action = '${pageContext}/externalJob/getAdminJobId.action?externalJobId=' + obj.getSelectedIds();
		doSimpleXMLHttpRequest(action , {})
		.addCallbacks(openJobSchedule, 
			function (request) {
            	if (request.number == 403) {
            	    	writeStatusMessage(error403Text, 'actionMessage error');
            	} else {
                       reportError(request);
                }
			}
         );
  	}
  	
  	function openJobSchedule(request) {
		var data = evalJSONRequest(request);
	    var ERROR_SUCCESS = "0";
	    if( data.errorCode == ERROR_SUCCESS ){
	  		window.location = data.generalMessage;
	     }
  	}   

</script>

<#if navigation.pageName != "create">
    <div class="spacer"></div>
    <div class="menuHeader"><@s.text name='nav.externaljob.actions'/></div>
    <ul id="NavList">
        <#if navigation.pageName == "default">
            <#assign actionName = "nav.externaljob.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.externaljob.">
        </#if>
        <#if navigation.pageName == "list" || navigation.pageName == "default">
            <@action          
                name="nav.externaljob.view"
                tableId=""
                id="a_viewExternalJob"
                url="javascript:gotoUrlWithSelected(externalJobObj, \\'${pageContext}/externalJob/view.action\\', \\'externalJobId\\');"
                tabIndex="250"
                actionEnablers={"enableOnOne":"externaljob.view.disabled.selection"}
                featureName="feature.appAdmin.externalJob.view" />
            <@action          
                name="nav.externaljob.create"
                tableId=""
                id="a_createExternalJob"
                url="/${navigation.applicationMenu}/externalJob/create!input.action"
                tabIndex="251"
                actionEnablers={}
                featureName="feature.appAdmin.externalJob.create" />
            <@action          
                name="nav.externaljob.edit"
                tableId=""
                id="a_editExternalJob"
                url="javascript:gotoUrlWithSelected(externalJobObj, \\'${pageContext}/externalJob/edit!input.action\\', \\'externalJobId\\');"
                tabIndex="252"
                actionEnablers={"enableOnOne":"externaljob.edit.disabled.selection"}
                featureName="feature.appAdmin.externalJob.edit" />    
            <@action            
                name="nav.externaljob.delete"
                tableId=""
                id="a_deleteExternalJob"
                url="javascript: showDeleteExternalJobDialog();"
                tabIndex="253"
                actionEnablers={"enableOnMoreThanNone":"externaljob.delete.disabled.selection"}
                featureName="feature.appAdmin.externalJob.delete" />
            <@action          
                name="nav.externaljob.schedule.edit"
                tableId=""
                id="a_editExternalJobSchedule"
                url="javascript: editJobSchedule(externalJobObj);"
                tabIndex="254"
                actionEnablers={"enableOnOne":"externaljob.edit.schedule.disabled.selection"}
                featureName="feature.appAdmin.schedule.edit" />
        <#elseif navigation.pageName == "view">
            <@action 
                name="nav.externaljob.create" 
                tableId=""
                id="a_createExternalJob" 
                url="/${navigation.applicationMenu}/externalJob/create!input.action"
                tabIndex="251" 
                actionEnablers={}
                featureName="feature.appAdmin.externalJob.create" />
            <@action 
                name="nav.externaljob.viewpage.edit" 
                tableId=""
                id="a_editExternalJob" 
                url="/${navigation.applicationMenu}/externalJob/edit!input.action?externalJobId=${externalJobId?c}"
                tabIndex="252" 
                actionEnablers={}
                featureName="feature.appAdmin.externalJob.edit" />
            <@action            
                name="nav.externaljob.viewpage.delete"
                tableId=""
                id="a_deleteExternalJob"
                url="javascript: showDeleteCurrentExternalJobDialog();"
                tabIndex="253"
                actionEnablers={}
                featureName="feature.appAdmin.externalJob.delete" />
        <#elseif navigation.pageName == "edit">
            <@action 
                name="nav.externaljob.create" 
                tableId=""
                id="a_createExternalJob" 
                url="/${navigation.applicationMenu}/externalJob/create!input.action"
                tabIndex="251" 
                actionEnablers={}
                featureName="feature.appAdmin.externalJob.create" />
            <@action            
                name="nav.externaljob.viewpage.delete"
                tableId=""
                id="a_deleteExternalJob"
                url="javascript: showDeleteCurrentExternalJobDialog();"
                tabIndex="252"
                actionEnablers={}
                featureName="feature.appAdmin.externalJob.delete" />
        </#if>
    </ul>
</#if>

<script type="text/javascript">
    <#if navigation.pageName == "create" || navigation.pageName == "edit">
        contextHelpUrl = 5810;
    <#elseif navigation.pageName == "view" || navigation.pageName == "list">
        contextHelpUrl = 5800;
    <#else>  
       contextHelpUrl = 150;
    </#if>
</script>
