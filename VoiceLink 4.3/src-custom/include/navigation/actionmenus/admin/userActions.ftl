<#include "/include/common/action.ftl" />
<script type="text/javascript">
    function redirectCurrentDelete(){
      <#if userId?has_content>
     	window.location="${base}/admin/user/deleteCurrentUser.action?userId=${userId?c}";
      </#if>
    }
		
	<#if navigation.pageName == "view" || navigation.pageName == "list">
	   var contextHelpUrl = 5000;
	<#elseif navigation.pageName == "edit">
	   var contextHelpUrl = 5010;
	<#elseif navigation.pageName == "create">
	   var contextHelpUrl = 5010;
	<#else>  
	   var contextHelpUrl = 37;
	</#if>
</script>
<#if navigation.pageName != "viewProfile" && navigation.pageName != "create">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.user.actions' /></div>
	<ul class="NavList">
		<#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
			    name="nav.user.view"
			    tableId="listUsersTable"
	            id="a_viewUser"
	            url="javascript:gotoUrlWithSelected(userObj, \\'${base}/admin/user/view.action\\', \\'userId\\');"
	            tabIndex="251"
	            actionEnablers={"enableOnOne": "user.view.disabled.selection"}
	            featureName="feature.userAdmin.user.view" />
			<@action 
				name="nav.user.create" 
				tableId="listUsersTable"
				id="a_createUser" 
				url="/admin/user/create!input.action" 
				tabIndex="252" 
				actionEnablers={} 
				featureName="feature.userAdmin.user.create" />
			<@action
				name="nav.user.edit"
				tableId="listUsersTable"
				id="a_editUser"
				url="javascript:gotoUrlWithSelected(userObj, \\'${base}/admin/user/edit!input.action\\', \\'userId\\');"
				tabIndex="253"
				actionEnablers={"enableOnOne":"user.edit.disabled.selection"}
				featureName="feature.userAdmin.user.edit" />
			<@action
				name="nav.user.delete"
				tableId="listUsersTable"
				id="a_deleteUser"
				url="javascript: showDeleteUserDialog();"
				tabIndex="254"
				actionEnablers={"enableOnMoreThanNone":"user.delete.disabled.selection"}
				featureName="feature.userAdmin.user.delete" />
			
		<#elseif navigation.pageName == "view">
			<@action 
				name="nav.user.create" 
				tableId="listUsersTable"
				id="a_createUser" 
				url="/admin/user/create!input.action" 
				tabIndex="251" 
				actionEnablers={} 
				featureName="feature.userAdmin.user.create" />
			<@action
				name="nav.user.edit.currentUser"
				tableId="listUsersTable"
				id="a_editUser"
				url="/admin/user/edit!input.action?userId=${userId?c}"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.userAdmin.user.edit" />
			<@action
				name="nav.user.delete.currentUser"
				tableId="listUsersTable"
				id="a_deleteUser"
				url="javascript: showDeleteCurrentUserDialog();"
				tabIndex="253"
				actionEnablers={}
				featureName="feature.userAdmin.user.delete" />
		
		<#elseif navigation.pageName == "edit">
			<@action 
				name="nav.user.create" 
				tableId="listUsersTable"
				id="a_createUser" 
				url="/admin/user/create!input.action" 
				tabIndex="251" 
				actionEnablers={} 
				featureName="feature.userAdmin.user.create" />
			<@action
				name="nav.user.delete.currentUser"
				tableId="listUsersTable"
				id="a_deleteUser"
				url="javascript: showDeleteCurrentUserDialog();"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.userAdmin.user.delete" />
			</#if>
	</ul>
</#if>