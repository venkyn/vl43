<#include "/include/common/action.ftl" />
	<#if navigation.pageName == "view" || navigation.pageName == "default">
	    <div class="spacer"></div>
	    <div class="menuHeader"><@s.text name='nav.systemconfiguration.actions' /></div>
	    <ul class="NavList">
	    <@action name="nav.systemConfiguration.edit" 
	             tableId=""
	             id="a_systemConfigurationEdit" 
	             url="/admin/systemconfiguration/edit!input.action" 
	             tabIndex="251" 
	             actionEnablers={} 
	             featureName="feature.appAdmin.systemConfiguration.edit" />
	    </ul>
	</#if>

<script type="text/javascript">
<#if navigation.pageName == "edit">
	var contextHelpUrl = 5710;
<#else>
	var contextHelpUrl = 5700;
</#if>
</script>
	
