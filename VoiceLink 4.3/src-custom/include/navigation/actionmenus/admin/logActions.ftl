<#include "/include/common/action.ftl" />
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.log.actions'/></div>
<ul class="NavList">
	<#if navigation.pageName == "list" || navigation.pageName == "default">
	 	<#assign disabledText>
			<@s.text name='log.save.disabled.selection'/>
	 	</#assign>
	    <@action name="nav.log.save" 
	             tableId="listLogsTable"
	             id="a_saveLogs" 
	             url="javascript: gotoUrlForSelectedLog(logObj,\\'${base}/admin//log/zipSet.action\\', \\'selectedFiles\\');" 
	             tabIndex="252" 
	             actionEnablers={"enableOnMoreThanNone":"${disabledText}"} 
	             featureName="feature.appAdmin.log.export" />
	<#elseif navigation.pageName == "view">
	    <@action name="log.view.button.saveFileAsZip"
	             tableId="listLogsTable"
	             id="a_saveLogs" 
	             url="javascript: submitForm($(\'form1\'));"
	             tabIndex="252" 
	             actionEnablers={} 
	             featureName="feature.appAdmin.log.export" />
	</#if>
</ul>

<script type="text/javascript">
	<#if navigation.pageName == "view">
	   var contextHelpUrl = 5200;
	<#else>  
   		var contextHelpUrl = 5200;
	</#if>	
</script>


<script type="text/javascript"> 
    function gotoUrlForSelectedLog(tableObject, url, idParam) {
        
        var objects = tableObject.getSelectedObjects();
        var ids = new Array();
        for (var i=0; i < objects.length; i++) {
                ids.push(objects[i].fileName);
            }
        idToken=idParam + "=";
        endLine = "?" + idToken + ids.join("&" + idToken);
        window.document.location.href = url + endLine;
}
</script>