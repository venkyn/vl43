<#include "/include/common/action.ftl" />
<#if navigation.pageName != "create">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.role.actions'/></div>
	<script type="text/javascript">
	 function redirectCurrentDelete(){
	  <#if roleId?has_content>
	 	window.location="${base}/admin/role/deleteCurrentRole.action?roleId=${roleId?c}";
	  </#if>
	}
	</script>
	<ul class="NavList">
		<#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
			    name="nav.role.view"
			    tableId="listRolesTable"
	            id="a_viewRole"
	            url="javascript:gotoUrlWithSelected(roleObj, \\'${base}/admin/role/view.action\\', \\'roleId\\');"
	            tabIndex="251"
	            actionEnablers={"enableOnOne": "role.view.disabled.selection", "enableNoAdmins":"role.view.disabled.administrator"}
	            featureName="feature.userAdmin.role.view" />
		    <@action
		        name="nav.role.create" 
		        tableId="listRolesTable"
		        id="a_createRole" 
		        url="/admin/role/create!input.action" 
		        tabIndex="252" 
		        actionEnablers={} 
		        featureName="feature.userAdmin.role.create" />
			<@action
			    name="nav.role.edit"
			    tableId="listRolesTable"
	            id="a_editRole"
	            url="javascript:gotoUrlWithSelected(roleObj, \\'${base}/admin/role/edit!input.action\\', \\'roleId\\');"
	            tabIndex="253"
	            actionEnablers={"enableOnOne": "role.edit.disabled.selection","enableNoAdmins":"role.edit.disabled.administrator"}
	            featureName="feature.userAdmin.role.edit" />
			<@action
			    name="nav.role.delete"
			    tableId="listRolesTable"
			    id="a_deleteRole"
			    url="javascript: showDeleteRoleDialog();"
			    tabIndex="254"
			    actionEnablers={"enableNoAdmins":"role.delete.disabled.administrator", "enableOnMoreThanNone":"role.delete.disabled.selection"}
			    featureName="feature.userAdmin.role.delete" />
		
		<#elseif navigation.pageName == "view">
		    <@action
	            name="nav.role.create" 
	            tableId="listRolesTable"
		        id="a_createRole" 
		        url="/admin/role/create!input.action" 
		        tabIndex="251" 
		        actionEnablers={} 
		        featureName="feature.userAdmin.role.create" />
			<@action
			    name="nav.role.edit.currentRole"
			    tableId="listRolesTable"
	            id="a_editRole"
	            url="/admin/role/edit!input.action?roleId=${roleId?c}"
	            tabIndex="252"
	            actionEnablers={}
	            featureName="feature.userAdmin.role.edit" />
			<@action
				name="nav.role.delete.currentRole"
				tableId="listRolesTable"
				id="a_deleteRole"
				url="javascript: showDeleteCurrentRoleDialog();"
				tabIndex="253"
				actionEnablers={}
				featureName="feature.userAdmin.role.delete" />
			
		<#elseif navigation.pageName == "edit">
		    <@action
		        name="nav.role.create" 
		        tableId="listRolesTable"
		        id="a_createRole" 
		        url="/admin/role/create!input.action" 
		        tabIndex="251" 
		        actionEnablers={} 
		        featureName="feature.userAdmin.role.create" />
			<@action
				name="nav.role.delete.currentRole"
				tableId="listRolesTable"
				id="a_deleteRole"
				url="javascript: showDeleteCurrentRoleDialog();"
				tabIndex="251"
				actionEnablers={}
				featureName="feature.userAdmin.role.delete" />
			
		</#if>
	</ul>
</#if>
<script>
<#if navigation.pageName == "list">
   var contextHelpUrl = 5100;
<#elseif navigation.pageName == "create">
   var contextHelpUrl = 5110;
<#elseif navigation.pageName = "edit">
   var contextHelpUrl = 5110;
<#else>  
   contextHelpUrl = 5100;
</#if>
</script>