<#include "/include/common/action.ftl" />
<script type="text/javascript"> 
var contextHelpUrl = 16;
</script>
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.notification.actions' /></div>
<ul class="NavList">
		<@action 
			name="nav.notification.acknowledge"
			tableId="listNotificationTable"
			id="a_ackNotification" 
    		url="javaScript:performActionOnSelected(notificationObj, \\'${base}/admin/notification/ackNotification.action\\');"
    		tabIndex="251" 
			actionEnablers={"enableOnMoreThanNone":"notification.acknowledge.disabled.selection","disableAckNotification": "notification.disabled.selection"}
			featureName="feature.appAdmin.notification.acknowledge" />
</ul>

<script type="text/javascript">
    <#if navigation.pageName == "list">
       var contextHelpUrl = 5500;
    </#if>  
</script>

