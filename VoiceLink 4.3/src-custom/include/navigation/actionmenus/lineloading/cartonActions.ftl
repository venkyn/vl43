<#include "/include/common/action.ftl" />
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.carton.actions'/></div>
<script type="text/javascript">
var manualLoadURL = '${base}/lineloading/carton/manualLoad.action';
var getDataForManualLoadURL = '${base}/lineloading/carton/getDataForManualLoad.action';


 function getDataOnSelectedForManualLoadCarton(tableObject,action, shortedConfirmed){
 	paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
 	paramList += "&shortedConfirmed=" + shortedConfirmed;
	doSimpleXMLHttpRequest(action + paramList, {})
		.addCallbacks(createManualLoadFTL, 
			function (request) {
            	if (request.number == 403) {
            	    	writeStatusMessage(error403Text, 'actionMessage error');
            	} else {
                       reportError(request);
                }
			}
		);
	tableObj = tableObject;
}
 
 	function showManualLoadDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='carton.manualLoad'/>",			    
				body: dialogBody,
				button1: "<@s.text name='carton.manualLoad'/>",
				button2: "<@s.text name='carton.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelectedCartonManualLoad, cartonObj, manualLoadURL), "");
	}
	
		
  function showManualLoadShortedDialog() {
		var dialogProperties = {
			    title: "<@s.text name='carton.manualLoad'/>",			    
				body: "<@s.text name='carton.manualLoad.shorted.text'/>" ,
			    button1: "<@s.text name='carton.movetext'/>",
				button2: "<@s.text name='carton.cancelmove'/>"}
		buildCustomDialog(dialogProperties,
			partial(getDataOnSelectedForManualLoadCarton,cartonObj,getDataForManualLoadURL, false), "");
	}	
 
 function createManualLoadFTL(request){
  	if(checkForShortedMessage(request))
  	{		
  		showManualLoadDialog(request.responseText);
	}
 }
 function performActionOnSelectedCartonManualLoad(tableObject, action) {
    	endLine = "?ids=" + tableObject.getSelectedIds()[0];
    	endLine += "&palletID=" + $("palletID").value;
    	endLine += "&operatorID=" + $("operatorID").value;		
        doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
            handleUserMessages, 
    		function (request) {
                if (request.number == 403) {
                	writeStatusMessage(error403Text, 'actionMessage error');
                } else {
                    reportError(request);
                }
    		}
    	);
    	
    	tableObject.clearSelectedRows();
    	tableObj = tableObject;
 }
 function checkForShortedMessage(request)
 {
  	  var ERROR_SUCCESS = "0";
      var ERROR_FAILURE = "1";
      var ERROR_PARTIAL = "2";
 	if(request.responseText.indexOf("errorCode") != -1)
 	{
 		try {
 			var data = evalJSONRequest(request);
 		}catch(err) {
 			log(err.toString()); 		
 		}	
 		if( data.errorCode == ERROR_SUCCESS ){
		        writeStatusMessage(data.generalMessage, 'actionMessage');
        } else if( data.errorCode == ERROR_FAILURE ){
        		writeStatusMessage(data.generalMessage, 'actionMessage error');
        } else if( data.errorCode == ERROR_PARTIAL ) {
        		showManualLoadShortedDialog();
        }
        return false;
 	}
 	else { 	
 		return true;
 	}
 }
		


 contextHelpUrl = 4;
</script>

<ul class="NavList">
<@action
      name="nav.carton.manualLoad"
      tableId="listCartonsTable"
      id="a_manualLoad"
      url="javascript:getDataOnSelectedForManualLoadCarton(cartonObj,getDataForManualLoadURL,true)"
      tabIndex="253"
      actionEnablers={"enableOnOne":"carton.manualLoad.error.selectOnlyOneCarton"}
      featureName="feature.lineloading.carton.manualLoad" />
 </ul>
