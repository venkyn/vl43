<#include "/include/common/action.ftl" />
<#include "/include/common/customdialog.ftl">
<#include "/include/common/customdialogdropdown.ftl">
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<#if navigation.pageName != "create">

<script type="text/javascript">
	var printManifestReportUrl = '${pageContext}/routeStop/printManifestReport.action';
	var validateIfAllCartonsLoaded = '${pageContext}/routeStop/validateIfAllCartonsLoaded.action';
	var closeTheRouteStop = '${pageContext}/routeStop/closeRouteStop.action';
	
	function validateIfCartonsLoaded(tableObject, action) {

		endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
		doSimpleXMLHttpRequest(action + endLine, {})
			.addCallbacks(cartonLoadValidated, 
				function (request) {
	            	if (request.number == 403) {
	            	    	writeStatusMessage(error403Text, 'actionMessage error');
	            	} else {
	                       reportError(request);
	                }
				}
	         );
	         tableObj = tableObject;		
    }


    function launchPalletReport(tableObject) {
   
   		action = '${pageContext}/routeStop/printManifestReport.action?ids='+ tableObject.getSelectedIds();
   		doSimpleXMLHttpRequest(action , {})
   			.addCallbacks(callReportParamater, 
   				function (request) {
   	            	if (request.number == 403) {
   	            	    	writeStatusMessage(error403Text, 'actionMessage error');
   	            	} else {
   	                       reportError(request);
   	                }
   				}
   	         );
   	         tableObj = tableObject;		
    }

    function callReportParamater(request) {    
	     var data = evalJSONRequest(request);
	     var ERROR_REDIRECT = "1";
	     var ERROR_SUCCESS = "0";
	     if( data.errorCode == ERROR_SUCCESS ){
        	 window.open('${pageContext}/reports/voicelink/PalletManifest/parameters.action');
             }else if( data.errorCode == ERROR_REDIRECT ) {
             	writeStatusMessage(data.generalMessage, "error");
             }  
     }

    function cartonLoadValidated(request) {    
  
  	     var data ;
	     var ERROR_REDIRECT = "3";
	     var ERROR_SUCCESS = "0";
	     var isError = false;
		
		try {
        	data = evalJSONRequest(request);
        } catch(err) {
    	    // evalJSONRequest throws an error in Firefox.
            isError = true;
     	}
		
		if(data == undefined) {
		    // evalJSONRequest will return undefined in IE.
			isError = true;
     	}
     	
 		if(isError) {
    		showCloseRouteStopDialog(request.responseText);
			return;
		}
	     
	     if( data.errorCode == ERROR_SUCCESS ){
        	 writeStatusMessage(data.generalMessage, 'actionMessage');
         }else if( data.errorCode == ERROR_REDIRECT ) {
             window.location=data.generalMessage;
         }else{    
             showCloseRouteStopDialog(request.responseText);
         }
   }
			    
    function showCloseRouteStopDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='close.routeStop'/>",			    
				body: "<@s.text name='routeStop.closeRouteStop.body'/>",
				button1: "<@s.text name='close.routeStop'/>",
				button2: "<@s.text name='pick.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, tableObj, closeTheRouteStop), "");
	}
	
	contextHelpUrl = 30;
	
</script>


	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.routeStop.actions' /></div>
	<ul class="NavList">
		 <#if navigation.pageName == "list" || navigation.pageName == "default">
		    <@action
				name="nav.routeStop.printManifestReport"
				tableId="listRouteStopsTable"
				id="a_printManifestReport"	
				url="javascript:launchPalletReport(tableObj);"
				tabIndex="251"
				actionEnablers={"enableOnOne":"routestop.printmanifest.disabled.selection"}
				featureName="feature.lineloading.routestop.printManifest" />
			<@action
				name="nav.routeStop.closeRouteStop"
				tableId="listRouteStopsTable"
				id="a_closeRouteStop"	
				url="javascript:validateIfCartonsLoaded(tableObj,validateIfAllCartonsLoaded);"
				tabIndex="252"
				actionEnablers={"enableOnOne":"routestop.closeroutestop.disabled.selection"}
				featureName="feature.lineloading.routestop.closeRouteStop" />
		</#if>
	</ul>
</#if>
