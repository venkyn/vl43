<#include "/include/common/action.ftl" />
<#include "/include/common/customdialog.ftl">
<#assign pageContext = "${base}/${navigation.applicationMenu}">

<#if navigation.pageName != "create">

<script type="text/javascript">
	var viewLineLoadingRegionURL = '${pageContext}/region/view.action';
	var editLineLoadingRegionURL = '${pageContext}/region/edit!input.action';
	var deleteLineLoadingRegionURL = '${pageContext}/region/deleteCurrentRegion.action';
	
	var validateIfAllCartonsLoaded = '${pageContext}/routeStop/validateIfAllCartonsLoaded.action';
	var closeTheRouteStop = '${pageContext}/routeStop/closeRouteStop.action';

  var deleteRegionURL = '${base}/lineloading/region/delete.action';

  function showDeleteRegionDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.regions'/>" ,
			    button1: "<@s.text name='delete.yesText.regions'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, tableObj,deleteRegionURL), "");
	}	
	
		
  function showDeleteCurrentRegionDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.region'/>" ,
			    button1: "<@s.text name='delete.yesText.region'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
   function redirectCurrentDelete(){
      <#if regionId?has_content>
     	window.location="${pageContext}/region/deleteCurrentRegion.action?regionId=${regionId?c}";
      </#if>
    }
	
</script>


	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.lineLoadingRegion.actions' /></div>
	<ul class="NavList">
		 <#if navigation.pageName == "list" || navigation.pageName == "default">
		     <@action
			    name="nav.lineLoadingRegion.view"
			    tableId="listLineLoadingRegionTable"
	            id="a_viewRegion"
	            url="javascript:gotoUrlWithSelected(tableObj, viewLineLoadingRegionURL, \\'regionId\\');"
	            tabIndex="251"
                actionEnablers={"enableOnOne":"lineLoadingRegion.view.disabled.selection"} 
	            featureName="feature.lineloading.region.view" />
			 <@action
		         name="nav.lineLoadingRegion.create"
		         tableId="listLineLoadingRegionTable"
	             id="a_createRegion" 
	             url="/lineloading/region/create!input.action" 
	             tabIndex="251" 
	             actionEnablers={} 
	             featureName="feature.lineloading.region.create" />	        
            <@action
                name="nav.lineLoadingRegion.edit"
                tableId="listLineLoadingRegionTable"
		        id="a_editRegion"
                tabIndex="253"
                url="javascript:performActionOnSelected(tableObj, \\'${pageContext}/region/checkIfEditable.action\\');"                
                actionEnablers={"enableOnOne":"lineLoadingRegion.edit.disabled.selection"}
                featureName="feature.lineloading.region.edit" />
            <@action			
			    name="nav.lineLoadingRegion.delete"
			    tableId="listLineLoadingRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteRegionDialog();"
		        tabIndex="252"
	            actionEnablers={"enableOnMoreThanNone":"lineLoadingRegion.delete.disabled.selection"}
		        featureName="feature.lineloading.region.delete" />
		  
		</#if>
		
		<#if navigation.pageName == "view">
		
		    <@action
		         name="nav.lineLoadingRegion.create"
		         tableId="listLineLoadingRegionTable"
	             id="a_createRegion" 
	             url="/lineloading/region/create!input.action" 
	             tabIndex="251" 
	             actionEnablers={} 
	             featureName="feature.lineloading.region.create" />
	        <@action
                name="nav.lineLoadingRegion.edit"
                tableId="listLineLoadingRegionTable"
		        id="a_editRegion"
                tabIndex="253"
                url="edit!input.action?regionId=${regionId?c}"           
                actionEnablers={}
                featureName="feature.lineloading.region.edit" />
            <@action			
			    name="nav.lineLoadingRegion.delete"
			    tableId="listLineLoadingRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteCurrentRegionDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.lineloading.region.delete" />
		  
		</#if>
		
		<#if navigation.pageName == "edit">
		
		    <@action
			    name="nav.lineLoadingRegion.view"
			    tableId="listLineLoadingRegionTable"
	            id="a_viewRegion"
	            url="view.action?regionId=${regionId?c}"
	            tabIndex="251"
                actionEnablers={} 
	            featureName="feature.lineloading.region.view" />
		    <@action
		         name="nav.lineLoadingRegion.create"
		         tableId="listLineLoadingRegionTable"
	             id="a_createRegion" 
	             url="/lineloading/region/create!input.action" 
	             tabIndex="251" 
	             actionEnablers={} 
	             featureName="feature.lineloading.region.create" />
	        <@action			
			    name="nav.lineLoadingRegion.delete"
			    tableId="listLineLoadingRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteCurrentRegionDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.lineloading.region.delete" />
			
		</#if>
	</ul>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "list">
   <#global helpFiles="PlaceholderPages/RegionsPagePlaceholder.htm"/>
   contextHelpUrl = 13;
<#--   <#global helpFiles="Core/Operators/Assign.htm"/>
   contextHelpUrl = 59;
<#elseif navigation.pageName =="create" || navigation.pageName == "create!input">
   <#global helpFiles="Selection/Region_Create.htm"/>
   contextHelpUrl = 60;
<#elseif navigation.pageName =="edit" || navigation.pageName == "edit!input">
   <#global helpFiles="Selection/Region_Edit.htm"/>
   contextHelpUrl = 60;
<#elseif navigation.pageName == "viewRegion!input" || navigation.pageName == "viewRegion">
    <#global helpFiles="Selection/Region_View.htm"/>
    contextHelpUrl = 62;
<#elseif navigation.pageName == "create!duplicate">
    <#global helpFiles="Selection/Region_Duplicate.htm"/>
    contextHelpUrl = 61; -->//do nothing; these numbers are incorrect
<#else>  
   <#global helpFiles="PlaceholderPages/RegionsPagePlaceholder.htm"/>
   contextHelpUrl = 74;
</#if>
</script>