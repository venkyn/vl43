<#include "/include/common/action.ftl" />
<#if navigation.pageName == "list" || navigation.pageName == "default" || navigation.pageName = "view" || navigation.pageName == "edit">
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.region.actions'/></div>
<script type="text/javascript">
var deleteRegionURL = '${base}/replenishment/region/delete.action';

  function showDeleteRegionDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.regions'/>" ,
			    button1: "<@s.text name='delete.yesText.regions'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, regionObj,deleteRegionURL), "");
	}	
	
  function showDeleteCurrentRegionDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.region'/>" ,
			    button1: "<@s.text name='delete.yesText.region'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	
   function redirectCurrentDelete(){
      <#if regionId?has_content>
     	window.location="${base}/replenishment/region/deleteCurrentRegion.action?regionId=${regionId?c}";
      </#if>
    }
	
</script>
<#-- 
    This is a temporary IE fix that must be overridden for each actions menu 
    that has message bubbles 
-->
<!--[if IE]>
<style>
  div#sidenav div.actionBubbleContainer {
    bottom: 318px;
  }
</style>
<![endif]-->
<ul id="NavList">
        <#if navigation.pageName == "default">
            <#assign actionName = "nav.region.listpage.">
        <#else>
        	<#if navigation.pageName != "viewSelectionProfile">
	            <#assign actionName = "nav." + navigation.pageName + "page.region.">
	        </#if>
        </#if>
		<#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
			    name=actionName + "view"
			    tableId="listRegionTable"
	            id="a_viewRegion"
	            url="javascript:gotoUrlWithSelected(regionObj, \\'${base}/replenishment/region/view.action\\', \\'regionId\\');"
	            tabIndex="251"
                actionEnablers={"enableOnOne":"region.view.disabled.selection"} 
	            featureName="feature.replenishment.region.view" />
	    </#if>
	    <#if navigation.pageName == "list" || navigation.pageName == "default" || navigation.pageName = "view" || navigation.pageName == "edit">
		    <@action
		         name=actionName + "create" 
		         tableId="listRegionTable"
	             id="a_createRegion" 
	             url="/replenishment/region/create!input.action" 
	             tabIndex="251" 
	             actionEnablers={} 
	             featureName="feature.replenishment.region.create" />	    
	    </#if>
   	    <#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
                name=actionName + "edit"
                tableId="listRegionTable"
		        id="a_editRegion"
                tabIndex="253"
                url="javascript:performActionOnSelected(regionObj, \\'${base}/replenishment/region/checkIfEditable.action\\');"                
                actionEnablers={"enableOnOne":"region.edit.disabled.selection"}
                featureName="feature.replenishment.region.edit" />         
        </#if>
        <#if navigation.pageName == "view">
			<@action
                name=actionName + "edit"
                tableId="listRegionTable"
		        id="a_editRegion"
                tabIndex="253"
                url="javascript:performAction(${regionId?c}, \'${base}/replenishment/region/checkIfEditable.action\');"
                actionEnablers={}
                featureName="feature.replenishment.region.edit" />                   
        </#if>
        <#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action			
			    name=actionName + "delete"
			    tableId="listRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteRegionDialog();"
		        tabIndex="252"
	            actionEnablers={"enableOnMoreThanNone":"region.delete.disabled.selection"}
		        featureName="feature.replenishment.region.delete" />
		</#if>
        <#if navigation.pageName == "view" || navigation.pageName == "edit">		   
			<@action			
			    name=actionName + "delete"
			    tableId="listRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteCurrentRegionDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.replenishment.region.delete" />
        </#if>   
       
</ul>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "list">
   <#global helpFiles="PlaceholderPages/RegionsPagePlaceholder.htm"/>
   contextHelpUrl = 27;
<#--   <#global helpFiles="Core/Operators/Assign.htm"/>
   contextHelpUrl = 59;
<#elseif navigation.pageName =="create" || navigation.pageName == "create!input">
   <#global helpFiles="Selection/Region_Create.htm"/>
   contextHelpUrl = 60;
<#elseif navigation.pageName =="edit" || navigation.pageName == "edit!input">
   <#global helpFiles="Selection/Region_Edit.htm"/>
   contextHelpUrl = 60;
<#elseif navigation.pageName == "viewRegion!input" || navigation.pageName == "viewRegion">
    <#global helpFiles="Selection/Region_View.htm"/>
    contextHelpUrl = 62;
<#elseif navigation.pageName == "create!duplicate">
    <#global helpFiles="Selection/Region_Duplicate.htm"/>
    contextHelpUrl = 61; -->//do nothing; these numbers are incorrect
<#else>  
   <#global helpFiles="PlaceholderPages/RegionsPagePlaceholder.htm"/>
   contextHelpUrl = 73;
</#if>
</script>
