<#include "/include/common/action.ftl" />
<#include "/include/common/customdialog.ftl">
<#include "/include/common/customdialogdropdown.ftl">

<#if navigation.pageName == "resequencelist">
	<#include "/include/navigation/actionmenus/resequenceActions.ftl">
</#if>
<#if navigation.pageName != "resequencelist">
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.assignment.actions'/></div>
<ul class="NavList">

<script type="text/javascript">
 var isEdit="new";
</script>

<script type="text/javascript">

var editAssignmentActionURL = '${base}/replenishment/assignment/checkIfEditable.action';

var editAssignmentStatusURL='${base}/replenishment/assignment/getReplenishmentStatus.action';
var modifyAssignmentStatusURL='${base}/replenishment/assignment/changeReplenishmentStatus.action';
var getResequenceRegionURL = '${base}/replenishment/assignment/getResequenceRegion.action';
var assignmentResequenceURL = '${base}/replenishment/assignment/resequencelist.action';

	
	
	function showChangeAssignmentStatusDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='replenishment.modify.status.title'/>",			    
				body: dialogBody,
				button1: "<@s.text name='replenishment.status.yesText'/>",
				button2: "<@s.text name='replenishment.status.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelectedForReplenishmentEditStatus, replenishmentObj, modifyAssignmentStatusURL), "");
	}
	
	function showAssignmentResequenceDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='nav.replenishment.resequence.selectRegion'/>",			    
				body: dialogBody,
				button1: "<@s.text name='replenishment.resequence.selectRegion.yesText'/>",
				button2: "<@s.text name='replenishment.resequence.selectRegion.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnResequenceRegion, replenishmentObj, assignmentResequenceURL), "");
	}
	
	
	contextHelpUrl = 25;

	function gotoUrlWithSelectedForAssignmentDetail(tableObject, url, idParam) {
	idToken=tableObject.getSelectedObjects();
		value = idToken[0].number;
	    var filter = "?submittedFilterCriterion={'viewId':'-1029','columnId':'-12713','operandId':'-4','value1':'" + value + "','value2':'','critNum':1,'locked':false}";
        window.document.location.href = url + filter;
    }

   function gotoUrlWithSelectedForAssignment (tableObject, url, idParam) {
	    idToken = idParam + "=";
	    endLine = "?" + idToken + tableObject.getSelectedIds().join("&" + idToken);
	    endLine += "&isEdit=new" ;
	    window.document.location.href = url + endLine;
   }
</script>
<#if navigation.pageName == "list" || navigation.pageName == "default" >

  <@action
	name="nav.replenishment.view"
	tableId="listReplenishmentsTable"
	id="a_viewReplenishment"	
	url="javascript:gotoUrlWithSelected(replenishmentObj,\\'${base}/replenishment/assignment/view.action\\',\\'replenishmentId\\');"
	tabIndex="251"
	actionEnablers={"enableOnOne":"replenishment.view.disabled.selection"}
	featureName="feature.replenishment.assignment.view" />

  <@action
      name="nav.replenishment.editAssignment"
      tableId="listReplenishmentsTable"
      id="a_editAssignment"
      url="javascript:performActionOnSelected(replenishmentObj,editAssignmentActionURL);"
      tabIndex="252"
      actionEnablers={"enableOnOne":"replenishment.edit.error.selectOnlyOneAssignment"}
      featureName="feature.replenishment.assignment.edit" />
      
  <@action
      name="nav.replenishment.viewAssignmentDetails"
      tableId="listReplenishmentsTable"
      id="a_viewAssignmentDetails"
      url="javascript:gotoUrlWithSelectedForAssignmentDetail(replenishmentObj,\\'${base}/replenishment/assignmentDetail/list.action\\',\\'assignmentId\\');"
      tabIndex="253"
      actionEnablers={"enableOnOne":"replenishment.view.details.selection"}
      featureName="feature.replenishment.assignment.view" />
      
   <@action
      name="nav.replenishment.modify.status"
      tableId="listReplenishmentsTable"
      id="a_modifyAssignmentStatus"
	  url="javascript:getDataOnSelectedForReplenishmentEditStatus(replenishmentObj,editAssignmentStatusURL);"
      tabIndex="254"
      actionEnablers={"enableOnMoreThanNone":"replenishment.change.status.error.selectOneOrMore"}
      featureName="feature.replenishment.assignment.modifyStatus" />

     <@action
       name="nav.replenishment.resequence"
       tableId="listReplenishmentsTable"
       id="a_resequenceAssignments"
       url="javascript:getDataOnSelectedForAssignmentResequence(replenishmentObj,getResequenceRegionURL);"
       tabIndex="255"
       actionEnablers={}
       featureName="feature.voicelink.assignment.resequence" />
       
 <#elseif navigation.pageName == "view">     
 
   <@action
      name="nav.replenishment.editAssignment"
      tableId="listReplenishmentsTable"
      id="a_editAssignment"
      url="/${navigation.applicationMenu}/assignment/edit!input.action?replenishmentId=${replenishmentId?c}"
      tabIndex="252"
      actionEnablers={}
      featureName="feature.replenishment.assignment.edit" />
      
  <@action
      name="nav.replenishment.viewAssignmentDetails"
      tableId="listReplenishmentsTable"
      id="a_viewAssignmentDetails"
      url="/${navigation.applicationMenu}/assignmentDetail/list.action?replenishmentId=${replenishmentId?c}"
      tabIndex="253"
      actionEnablers={}
      featureName="feature.replenishment.assignmentdetails.view" />

</#if>
</#if>
</ul>
