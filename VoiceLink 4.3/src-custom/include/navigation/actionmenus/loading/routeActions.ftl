<#assign pageContext = "${base}/${navigation.applicationMenu}">
<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript" src="${base}/scripts/customDialog.js"></script>

<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.loading.route.actions'/></div>
<script type="text/javascript">
	var editRouteActionURL = '${pageContext}/route/checkIfEditable.action';
	var viewRouteActionURL = '${pageContext}/route/view.action';
	var editStopActionURL = '${pageContext}/stop/edit!input.action'; 
	var viewStopActionURL = '${pageContext}/stop/view.action';
	
    function launchTruckDiagram(routeId) {
    
		action = '${pageContext}/route/checkIfPrintable.action?routeId=' + routeId;
		doSimpleXMLHttpRequest(action , {})
		.addCallbacks(openTruckDiagram, 
			function (request) {
            	if (request.number == 403) {
            	    	writeStatusMessage(error403Text, 'actionMessage error');
            	} else {
                       reportError(request);
                }
			}
         );
   	        
    }

    function openTruckDiagram(request) {    
	     var data = evalJSONRequest(request);
	     var ERROR_REDIRECT = "1";
	     var ERROR_SUCCESS = "0";
	    if( data.errorCode == ERROR_SUCCESS ){
        	 window.open('${pageContext}/route/launch.action?routeId=' + routeObj.getSelectedIds());
         }else if( data.errorCode == ERROR_REDIRECT ) {
         	 writeStatusMessage(data.generalMessage, "error");
        }  
     }	
</script>

<div id="menuBlade_listRoutesTable">
<ul class="NavList" id="bladeList_listRoutesTable">
	<#if navigation.pageName == "list" || navigation.pageName == "default">
	   <@action
	      name="nav.loading.route.view"
	      tableId="listRoutesTable"
	      id="a_viewRoute"
	      url="javascript:gotoUrlWithSelected(routeObj,viewRouteActionURL,\\'routeId\\');"
	      tabIndex="252"
	      actionEnablers={"enableOnOne":"loading.route.view.error.selectOnlyOneRoute"}
	      featureName="feature.loading.route.view" />
	      
	   <@action
	      name="nav.loading.route.edit"
	      tableId="listRoutesTable"
	      id="a_editRoute"
	      url="javascript:performActionOnSelected(routeObj,editRouteActionURL);"
	      tabIndex="253"
	      actionEnablers={"enableOnOne":"loading.route.edit.error.selectOnlyOneRoute"}
	      featureName="feature.loading.route.edit" />
	      
	   <@action
	      name="nav.loading.route.load.diagram"
	      tableId="listRoutesTable"
	      id="a_printDiagram"
	      url="javascript:launchTruckDiagram(routeObj.getSelectedIds());"
	      tabIndex="254"
	      actionEnablers={"enableOnOne":"loading.route.print.error.selectOnlyOneRoute"}
	      featureName="feature.loading.loadDiagram.print" />
	</#if>
	      
	<#if navigation.pageName == "view">
	   <@action
	      name="nav.loading.route.edit"
	      tableId="listRoutesTable"
	      id="a_editRoute"
	      url="javascript:performAction(${routeId?c}, \'${pageContext}/route/checkIfEditable.action\');"
	      tabIndex="253"
	      actionEnablers={}
	      featureName="feature.loading.route.edit" />
	      
	   <@action
	      name="nav.loading.route.load.diagram"
	      tableId="listRoutesTable"
	      id="a_printDiagram"
	      url="javascript:launchTruckDiagram(${routeId?c});"
	      tabIndex="254"
	      actionEnablers={}
	      featureName="feature.loading.loadDiagram.print" />
	      
	<#elseif navigation.pageName == "edit">
	   <@action
	      name="nav.loading.route.view"
	      tableId="listRoutesTable"
	      id="a_viewRoute"
	      url="view.action?routeId=${routeId?c}"
	      tabIndex="253"
	      actionEnablers={}
	      featureName="feature.loading.route.view" />
	      	
	   <@action
	      name="nav.loading.route.load.diagram"
	      tableId="listRoutesTable"
	      id="a_printDiagram"
	      url="javascript:launchTruckDiagram(${routeId?c});"
	      tabIndex="254"
	      actionEnablers={}
	      featureName="feature.loading.loadDiagram.print" />
	</#if>
	
</ul>
</div>
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.loading.stop.actions'/></div>
<div id="menuBlade_listStopsTable">
<ul class="NavList" id="bladeList_listStopsTable">
	<@action
      name="nav.loading.stop.view"
      tableId="listStopsTable"
      id="a_viewStop"
      url="javascript:gotoUrlWithSelected(stopObj,viewStopActionURL,\\'stopId\\');"
      tabIndex="255"
      actionEnablers={"enableOnOne":"loading.stop.view.error.selectOnlyOneStop"}
      featureName="feature.loading.stop.view" />
      
   <@action
      name="nav.loading.stop.edit"
      tableId="listStopsTable"
      id="a_editStop"
      url="javascript:gotoUrlWithSelected(stopObj,editStopActionURL,\\'stopId\\');"
      tabIndex="256"
      actionEnablers={"enableOnOne":"loading.stop.edit.error.selectOnlyOneStop"}
      featureName="feature.loading.stop.edit" />
</ul>
</div>

<!--  implementing help links --->

<script type="text/javascript">
<#if navigation.pageName == "edit!input" || navigation.pageName == "edit">
    <#global helpFiles="Content/Loading/Routes_and_Container_Fields.htm" />
    contextHelpUrl = 312;
<#else>
    <#global helpFiles="Content/PlaceholderPages/RoutesAndContainersPagePlaceholder.htm" />
    contextHelpUrl = 311;
</#if>
</script>
