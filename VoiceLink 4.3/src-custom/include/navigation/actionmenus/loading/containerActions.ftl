<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript" src="${base}/scripts/customDialog.js"></script>

<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.loading.container.actions'/></div>
<script type="text/javascript">
	var editContainerActionURL = '${pageContext}/container/checkIfEditable.action';
	var viewContainerActionURL = '${pageContext}/container/view.action';
</script>

<div id="menuBlade_listLoadingContainerTable">
<ul class="NavList" id="bladeList_listContainerTable">
	<#if navigation.pageName == "list" || navigation.pageName == "default">
		<@action
	      name="nav.loading.container.view"
	      tableId="listLoadingContainerTable"
	      id="a_viewLoadingContainer"
	      url="javascript:gotoUrlWithSelected(containerObj,viewContainerActionURL,\\'containerId\\');"
	      tabIndex="252"
	      actionEnablers={"enableOnOne":"loading.container.view.error.selectOnlyOneContainer"}
	      featureName="feature.loading.container.view" />
	      
	   <@action
	      name="nav.loading.container.edit"
	      tableId="listLoadingContainerTable"
	      id="a_editLoadingContainer"
	      url="javascript:performActionOnSelected(containerObj,editContainerActionURL);"
	      tabIndex="253"
	      actionEnablers={"enableOnOne":"loading.container.edit.error.selectOnlyOneContainer"}
	      featureName="feature.loading.container.edit" />
	</#if>
	
	<#if navigation.pageName == "view">
	   <@action
	      name="nav.loading.container.edit"
	      tableId="listLoadingContainerTable"
	      id="a_editLoadingContainer"
	      url="javascript:performAction(${containerId?c}, \'${pageContext}/container/checkIfEditable.action\');"
	      tabIndex="253"
	      actionEnablers={}
	      featureName="feature.loading.container.edit" />
	</#if>		
</ul>
</div>

<script type="text/javascript">
<#if navigation.pageName == "edit" || navigation.pageName == "view">
	<#global helpFiles="Content/Loading/Routes_and_Container_Fields.htm" />
    contextHelpUrl = 317;
<#else>
    <#global helpFiles="Content/PlaceholderPages/RoutesAndContainersPagePlaceholder.htm" />
    contextHelpUrl = 317;
</#if>
</script>