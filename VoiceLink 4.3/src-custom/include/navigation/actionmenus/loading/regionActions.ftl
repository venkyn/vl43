<#include "/include/common/action.ftl" />
<#include "/include/common/customdialog.ftl">
<#assign pageContext = "${base}/${navigation.applicationMenu}">

<#if navigation.pageName != "create">

<script type="text/javascript">
	var viewLoadingRegionURL = '${pageContext}/region/view.action';
	var editLoadingRegionURL = '${pageContext}/region/edit!input.action';
	var deleteLoadingRegionURL = '${pageContext}/region/deleteCurrentRegion.action';
    var deleteRegionURL = '${base}/loading/region/delete.action';

  function showDeleteRegionDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.regions'/>" ,
			    button1: "<@s.text name='delete.yesText.regions'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, tableObj,deleteRegionURL), "");
	}	
	
		
  function showDeleteCurrentRegionDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.region'/>" ,
			    button1: "<@s.text name='delete.yesText.region'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
   function redirectCurrentDelete(){
      <#if regionId?has_content>
     	window.location="${pageContext}/region/deleteCurrentRegion.action?regionId=${regionId?c}";
      </#if>
    }
	
</script>


	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.loading.region.actions' /></div>
	<ul class="NavList">
		 <#if navigation.pageName == "list" || navigation.pageName == "default">
		     <@action
			    name="nav.loading.region.view"
			    tableId="listRegionTable"
	            id="a_viewRegion"
	            url="javascript:gotoUrlWithSelected(tableObj, viewLoadingRegionURL, \\'regionId\\');"
	            tabIndex="250"
                actionEnablers={"enableOnOne":"loadingRegion.view.disabled.selection"} 
	            featureName="feature.loading.region.view" />
			 <@action
		         name="nav.loading.region.create"
		         tableId="listRegionTable"
	             id="a_createRegion" 
	             url="/loading/region/create!input.action" 
	             tabIndex="251" 
	             actionEnablers={} 
	             featureName="feature.loading.region.create" />	        
            <@action
                name="nav.loading.region.edit"
                tableId="listRegionTable"
		        id="a_editRegion"
                tabIndex="253"
                url="javascript:performActionOnSelected(tableObj, \\'${pageContext}/region/checkIfEditable.action\\');"                
                actionEnablers={"enableOnOne":"loadingRegion.edit.disabled.selection"}
                featureName="feature.loading.region.edit" />
            <@action			
			    name="nav.loading.region.delete"
			    tableId="listRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteRegionDialog();"
		        tabIndex="252"
	            actionEnablers={"enableOnMoreThanNone":"loadingRegion.delete.disabled.selection"}
		        featureName="feature.loading.region.delete" />
		</#if>
		
		<#if navigation.pageName == "view">
		
		    <@action
		         name="nav.loading.region.create"
		         tableId="listRegionTable"
	             id="a_createRegion" 
	             url="/loading/region/create!input.action" 
	             tabIndex="251" 
	             actionEnablers={} 
	             featureName="feature.loading.region.create" />
	        <@action
                name="nav.loading.region.edit"
                tableId="listRegionTable"
		        id="a_editRegion"
                tabIndex="253"
                url="javascript:performAction(${regionId?c}, \'${pageContext}/region/checkIfEditable.action\');"           
                actionEnablers={}
                featureName="feature.loading.region.edit" />
            <@action			
			    name="nav.loading.region.delete"
			    tableId="listRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteCurrentRegionDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.loading.region.delete" />
		  
		</#if>
		
		<#if navigation.pageName == "edit">
		
		    <@action
			    name="nav.loading.region.view"
			    tableId="listRegionTable"
	            id="a_viewRegion"
	            url="view.action?regionId=${regionId?c}"
	            tabIndex="250"
                actionEnablers={} 
	            featureName="feature.loading.region.view" />
		    <@action
		         name="nav.loading.region.create"
		         tableId="listRegionTable"
	             id="a_createRegion" 
	             url="/loading/region/create!input.action" 
	             tabIndex="251" 
	             actionEnablers={} 
	             featureName="feature.loading.region.create" />
	        <@action			
			    name="nav.loading.region.delete"
			    tableId="listRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteCurrentRegionDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.loading.region.delete" />
			
		</#if>
	</ul>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "list">
   <#global helpFiles="/Content/PlaceholderPages/LoadingRegionPlaceholder.htm"/>
   contextHelpUrl = 320;
<#--   <#global helpFiles="Core/Operators/Assign.htm"/>
   contextHelpUrl = 59; -->
<#elseif navigation.pageName =="create" || navigation.pageName == "create!input">
   <#global helpFiles="/Content/Loading/LoadingRegions.htm"/>
   contextHelpUrl = 330;
<#elseif navigation.pageName =="edit" || navigation.pageName == "edit!input">
   <#global helpFiles="/Content/Loading/LoadingRegions.htm"/>
   contextHelpUrl = 330;
<#elseif navigation.pageName == "viewRegion!input" || navigation.pageName == "viewRegion">
    <#global helpFiles="/Content/Loading/LoadingRegions.htm"/>
    contextHelpUrl = 330;
<#elseif navigation.pageName == "create!duplicate">
    <#global helpFiles="/Content/Loading/LoadingRegions.htm"/>
    contextHelpUrl = 330; 
<#else>  
   <#global helpFiles="/Content/PlaceholderPages/LoadingRegionPlaceholder.htm"/>
   contextHelpUrl = 330;
</#if>
</script>