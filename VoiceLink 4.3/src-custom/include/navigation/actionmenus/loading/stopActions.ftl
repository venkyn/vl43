<#assign pageContext = "${base}/${navigation.applicationMenu}">
<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript" src="${base}/scripts/customDialog.js"></script>


<script type="text/javascript">
</script>

<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.loading.stop.actions'/></div>
<div id="menuBlade_listStopsTable">
<ul class="NavList" id="bladeList_listStopsTable">
	<#if navigation.pageName == "edit">
		<@action
	      name="nav.loading.stop.view"
	      tableId="listStopsTable"
	      id="a_viewStop"
	      url="view.action?stopId=${stopId?c}"
	      tabIndex="255"
	      actionEnablers={}
	      featureName="feature.loading.stop.view" />
	</#if>      
    <#if navigation.pageName == "view">
	   <@action
	      name="nav.loading.stop.edit"
	      tableId="listStopsTable"
	      id="a_editStop"
	      url="edit!input.action?stopId=${stopId?c}"
	      tabIndex="256"
	      actionEnablers={}
	      featureName="feature.loading.stop.edit" />
    </#if>	      
</ul>
</div>

<!--  implementing help links --->
<script type="text/javascript">
<#if navigation.pageName == "edit!input" || navigation.pageName == "edit">
    <#global helpFiles="Content/Loading/Routes_and_Container_Fields.htm" />
    contextHelpUrl = 312;
<#else>
    <#global helpFiles="Content/PlaceholderPages/RoutesAndContainersPagePlaceholder.htm" />
    contextHelpUrl = 311;
</#if>
</script>
