<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript">
	var deleteCustomerLocationURL = '${pageContext}/customerLocation/delete.action';

	 function showDeleteCustomerLocationDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.customerLocations'/>" ,
			    button1: "<@s.text name='delete.yesText.customerLocations'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, customerLocationObj,deleteCustomerLocationURL), "");
	}	
		
</script>
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.puttostore.customerLocation.actions'/></div>
	<ul id="NavList">
		<@action			
			name="nav.puttostore.customerLocations.delete"
			tableId="listCustomerLocationsTable"
		    id="a_deleteCustomerLocation"
		    url="javascript: showDeleteCustomerLocationDialog();"
		    tabIndex="252"
	        actionEnablers={"enableOnMoreThanNone":"puttostore.customerLocation.delete.disabled.selection"}
		    featureName="feature.puttostore.customerlocation.delete" />
	</ul>
<script type="text/javascript">
   <#global helpFiles="PlaceholderPages/customerLocationsPagePlaceholder.htm"/>
   contextHelpUrl = 230;
</script>