<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">

<!-- The edit action was removed due to a lack of sufficent use cases.  In order to
     reimplement it do the following:
     - Uncomment the code below
     - Uncomment the edit section for licenses in the struts put to store file
     - Finish implementing the status change for puts in the PtsLicensRoot,
       ProcessStatusChange function
     - Add user rolls by uncommenting sections in install-data-voicelink.xml and
       app-security-voicelink.xml
-->
<#--
<#if navigation.pageName != "isEditable" && navigation.pageName != "edit">

  <div class="spacer"></div>
  <div class="menuHeader"><@s.text name='nav.puttostore.license.actions'/></div>
  <script type="text/javascript">
    var editLicenseActionURL = '${base}/puttostore/license/checkIfEditable.action';
  </script>
  <div id="menuBlade_listLicensesTable">
  <ul class="NavList" id="bladeList_listLicensesTable">

   <@action
      name="nav.puttostore.license.edit"
      tableId="listLicensesTable"
      id="a_editLicense"
      url="javascript:performActionOnSelected(licenseObj,editLicenseActionURL);"
      tabIndex="253"
      actionEnablers={"enableOnOne":"puttostore.license.edit.error.selectOnlyOneLicense"}
      featureName="feature.puttostore.license.edit" />

  </ul>
  </div>
</#if>
-->

<script type="text/javascript">
<#if navigation.pageName == "edit!input" || navigation.pageName == "edit">
    <#global helpFiles="/Content/PuttoStore/PTS_License_Edit.htm" />
    contextHelpUrl = 250;
<#else>
    <#global helpFiles="Content/PlaceholderPages/PtsLicensePagePlaceholder.htm" />
    contextHelpUrl = 220;
</#if>
</script>