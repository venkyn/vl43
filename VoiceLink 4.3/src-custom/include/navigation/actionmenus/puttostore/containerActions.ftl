<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">


<script type="text/javascript">
   <#global helpFiles="/Content/PlaceholderPages/PTSContainersPagePlaceholder.htm"/>
   contextHelpUrl = 260;

	var	getPrintLabelUrl = '${base}/puttostore/print/getContainerPrintLabel.action'
	var printLabelURL = '${base}/puttostore/print/containerPrintLabel.action';
	var closePtsContainerURL =  '${base}/puttostore/container/closeContainer.action';

	function showContainerPrintLabelDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='nav.printLabel'/>",			    
				body: dialogBody,
				button1: "<@s.text name='printLabel.yesText'/>",
				button2: "<@s.text name='printlabel.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnPrintLabel, ptsContainerObj, printLabelURL), "");
	}
		
	function showCloseConfirmDialog() {
		var dialogProperties = {
			    title: "<@s.text name='nav.puttostore.ptsContainer.closeContainer'/>",			    
				body: "<@s.text name='ptsContainer.body.closeConfirmText'/>",
				button1: "<@s.text name='ptsContainer.close.yesText'/>",
				button2: "<@s.text name='ptsContainer.close.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, ptsContainerObj, closePtsContainerURL), "");
	}
	
</script>

<ul class="NavList">
	<@action
      name="nav.puttostore.ptsContainer.closeContainer"
      tableId="listPtsContainersTable"
      id="a_closeContainer"
      url="javascript:showCloseConfirmDialog();"
      tabIndex="253"
      actionEnablers={"enableOnMoreThanNone":"puttostore.container.close.disabled.selection"}
      featureName="feature.puttostore.ptscontainer.close" />	

     <@action
       name="nav.container.printLabel"
       tableId="listPtsContainersTable"
       id="a_printLabelPtsContainer"
       url="javascript:getDataOnSelectedForContainerPrintLabel(ptsContainerObj,getPrintLabelUrl);"
       tabIndex="254"
       actionEnablers={"enableOnMoreThanNone":"print.label.error.selectOneorMoreContainers"}
       featureName="feature.puttostore.ptscontainer.print" />
</ul>

