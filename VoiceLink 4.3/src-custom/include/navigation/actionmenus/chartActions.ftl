<#include "/include/common/action.ftl"/>
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.chart.actions'/></div>
    <ul class="NavList">	
		<#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
          		name="nav.chart.launchChart"
          		tableId="listChartsTable"
	      		id="a_launchChart"
	      		url="javascript:launchChart();"
	      		tabIndex="251"
          		actionEnablers={"enableOnOne":"chart.launch.disabled.selection"} 
	      		featureName="feature.voicelink.chart.launchChart" />
		    <@action
				name="nav.chart.view"
				tableId="listChartsTable"
				id="a_viewChart"		
				url="javascript:gotoUrlWithSelected(chartObj, \\'${base}/${navigation.applicationMenu}/chart/view.action\\', \\'chartId\\');"
				tabIndex="252"
				actionEnablers={"enableOnOne":"chart.view.disabled.selection"}
				featureName="feature.voicelink.chart.view" />
			<@action
				name="nav.chart.edit"
				tableId="listChartsTable"
				id="a_editChart"
				url="javascript:gotoUrlWithSelected(chartObj, \\'${base}/${navigation.applicationMenu}/chart/edit!input.action\\', \\'chartId\\');"
				tabIndex="253"
				actionEnablers={"enableOnOne":"chart.edit.disabled.selection"}
				featureName="feature.voicelink.chart.edit" /> 			
		<#elseif navigation.pageName == "view">
			<@action
				name="nav.chart.viewpage.edit"
				tableId="listChartsTable"
				id="a_editViewChart"
				url="/${navigation.applicationMenu}/chart/edit!input.action?chartId=${chartId?c}"
				tabIndex="254"
				actionEnablers={}
				featureName="feature.voicelink.chart.edit" />
		</#if>		
    </ul>
<script type="text/javascript">	
	
	function launchChart() {
	
		var displayWidth = 550;
		var displayHeight = 500;
	
		// determine offsets to center the new window
		var leftOffset = ((screen.width/2) - (displayWidth/2));
		var topOffset = ((screen.height/2) - (displayHeight/2));
			
		// open the chart in a new window
		window.open('${base}/${navigation.applicationMenu}/chart/launchChart.action?chartId=' + chartObj.getSelectedIds(), 
					'_blank','left=' + leftOffset + ',top=' + topOffset + ',width=' + displayWidth + ',height=' + displayHeight +
					',menubar=0,scrollbars=1,location=0,status=0,directories=0,toolbar=0,resizable=1');
					
	}

<#if navigation.pageName == "create">
   <#global helpFiles="Core/ChartsFields.htm"/>
   contextHelpUrl = 124;
<#elseif navigation.pageName == "view" || navigation.pageName == "edit">
   <#global helpFiles="Core/ChartsFields.htm"/>
   contextHelpUrl = 122;
<#else>  
   <#global helpFiles="PlaceholderPages/ChartsPagePlaceholder.htm"/>
   contextHelpUrl = 120;
</#if>

</script>