<#if navigation.pageName != "pickDetail.list">
<#include "/include/common/action.ftl" />
<#include "/include/common/customdialogdate.ftl" />

<#assign calendarButton>
    <@s.text name='route.modify.departureDate.showcalendar'/>
</#assign>

<#if navigation.pageName == "resequencelist">
     <#include "/include/navigation/actionmenus/resequenceActions.ftl">
</#if>

<#if navigation.pageName != "isEditable" && navigation.pageName != "resequencelist" && navigation.pageName != "edit" &&  navigation.pageName != "prioritize">
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.assignment.actions'/></div>
<script type="text/javascript">

	var groupingActionURL = '${base}/selection/assignment/groupAssignments.action';
	var ungroupingActionURL = '${base}/selection/assignment/ungroupAssignments.action';
	var editAssignmentActionURL = '${base}/selection/assignment/checkIfEditable.action';
	var validateSplitURL = '${base}/selection/pick/validateSplit.action';
	var splitPicksURL = '${base}/selection/pick/splitPicks.action';
	var validateManualPickURL =  '${base}/selection/pick/validateManualPick.action';
	var ManualPickURL = '${base}/selection/pick/manualPick.action';
	var cancelPickURL =  '${base}/selection/pick/cancelPick.action';
	var	getAssignmentPrintLabelUrl = '${base}/selection/print/getAssignmentPrintLabel.action'
	var assignmentPrintLabelURL = '${base}/selection/print/assignmentPrintLabel.action';
	var getResequenceRegionURL = '${base}/selection/assignment/getResequenceRegion.action';
	var assignmentResequenceURL = '${base}/selection/assignment/resequencelist.action';
	var	getPickPrintLabelUrl = '${base}/selection/print/getPickPrintLabel.action'
	var pickPrintLabelURL = '${base}/selection/print/pickPrintLabel.action';
	var modifyAssignmentDeliveryLocationURL = '${base}/selection/assignment/modifyAssignmentDeliveryLocation.action';
	var getModifyRouteURL = '${base}/selection/assignment/getRouteWithDeliveryDate.action';

  function modifyDeliveryLocation(tableObject) {
		var actionUrl = modifyAssignmentDeliveryLocationURL + "?assignmentIds=" + tableObject.getSelectedIds().join("&assignmentIds=");
		actionUrl += "&assignmentDeliveryLocation=" + $("assignmentDeliveryLocation").value;
		doSimpleXMLHttpRequest(actionUrl, {}).addCallbacks(
					handleUserMessages, 
					function (request) {
		            	if (request.number == 403) {
		           		writeStatusMessage(error403Text, 'actionMessage error');
		            	} else {
		                    reportError(request);
		                }
					}
		);
		
		tableObj = tableObject;
   }
	
	function showAssignmentResequenceDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='nav.assignment.resequence.selectRegion'/>",			    
				body: dialogBody,
				button1: "<@s.text name='assignment.resequence.selectRegion.yesText'/>",
				button2: "<@s.text name='assignment.resequence.selectRegion.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnResequenceRegion, assignmentObj, assignmentResequenceURL), "");
	}

	function showModifyDeliveryLocationDialog() { 
		var dialogProperties = {
			    title: "<@s.text name='nav.assignment.modifyDeliveryLocation'/>",			    
				body: "<@s.text name='modifyDeliveryLocation.body'/> <input type='text' id='assignmentDeliveryLocation' name='assignmentDeliveryLocation' size='5' maxlength='9'>",
				button1: "<@s.text name='modifyDeliveryLocation.yesText'/>",
				button2: "<@s.text name='modifyDeliveryLocation.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(modifyDeliveryLocation,assignmentObj), "");
	}

	function showAssignmentPrintLabelDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='nav.printLabel'/>",			    
				body: dialogBody,
				button1: "<@s.text name='printLabel.yesText'/>",
				button2: "<@s.text name='printlabel.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnPrintLabel, assignmentObj, assignmentPrintLabelURL), "");
	}

	function showUngroupAssignmentsDialog() {
		var dialogProperties = {
			    title: "<@s.text name='ungroup.title'/>",			    
				body: "<@s.text name='assignment.body.ungroup'/>",
				button1: "<@s.text name='assignment.yesText.ungroup'/>",
				button2: "<@s.text name='ungroup.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, assignmentObj, ungroupingActionURL), "");
	}

	function showGroupAssignmentsDialog() {
		var dialogProperties = {
			    title: "<@s.text name='grouping.title'/>",			    
				body: "<@s.text name='assignment.body.group'/>",
				button1: "<@s.text name='assignment.yesText.group'/>",
				button2: "<@s.text name='group.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, assignmentObj, groupingActionURL), "");
	}
	
	function showCancelConfirmDialog() {
		var dialogProperties = {
			    title: "<@s.text name='nav.pick.cancelPick'/>",			    
				body: "<@s.text name='pick.body.cancelConfirmText'/>",
				button1: "<@s.text name='pick.cancel.yesText'/>",
				button2: "<@s.text name='pick.cancel.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, pickObj, cancelPickURL), "");
	}

	function showManualPickRecheckDialog(qe) {
		var dialogProperties = {
			    title: "<@s.text name='pick.manualPick'/>",			    
				body: "<@s.text name='pick.dialogbox.lessText'/>",
				button1: "<@s.text name='pick.yesText'/>",
				button2: "<@s.text name='pick.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(makeAjaxCallForManualPick, pickObj, ManualPickURL, qe), reDisplayManualPickDialog);
	}

	function showManualPickRecheckMaxDialog() {
		var dialogProperties = {
			    title: "<@s.text name='pick.manualPick'/>",			    
				body: "<@s.text name='pick.dialogbox.moreText'/>",
				button1: "<@s.text name='pick.maxAmountExceededText'/>",
				button2: "<@s.text name='pick.noText'/>"}
		buildCustomDialog(dialogProperties,
			reDisplayManualPickDialog, "");
	}
		
	function showManualPickRecheckNotNumberDialog() {
		var dialogProperties = {
			    title: "<@s.text name='pick.manualPick'/>",			    
				body: "<@s.text name='pick.dialogbox.notNumber'/>",
				button1: "<@s.text name='pick.maxAmountExceededText'/>",
				button2: "<@s.text name='pick.noText'/>"}
		buildCustomDialog(dialogProperties,
			reDisplayManualPickDialog, "");
	}

	function showPickPrintLabelDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='nav.printLabel'/>",			    
				body: dialogBody,
				button1: "<@s.text name='printLabel.yesText'/>",
				button2: "<@s.text name='printlabel.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnPrintLabel, pickObj, pickPrintLabelURL), "");
	}

	function showSplitAssignmentDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='nav.assignment.splitAssignment'/>",			    
				body: dialogBody,
				button1: "<@s.text name='pick.yesText.split'/>",
				button2: "<@s.text name='ungroup.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionFromCustomDialog, pickObj, splitPicksURL), "");
	}

	function showManualPickDropDownDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='pick.manualPick'/>",			    
				body: dialogBody,
				button1: "<@s.text name='pick.manualPick'/>",
				button2: "<@s.text name='pick.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(manualPickCustomDialogValidation, pickObj, ManualPickURL), "");
	}
	
	function reDisplayManualPickDialog() {
		validateSelectedPick(pickObj, validateManualPickURL);		
	}

    function showRouteModificationDialog() {
        cancelChangeRouteDDT();
        $("dialogDIV").style.display = "inline";
        var dialogProperties = {
              title: "<@s.text name='nav.route.modify'/>",
              body: $('dialogDIV'),
              button1: "<@s.text name='route.modify.selectRoute.yesText'/>",
              button2: "<@s.text name='route.modify.selectRoute.noText'/>"}
        buildCustomDialog(dialogProperties,
              partial(updateDDT,"",""), cancelChangeRouteDDT);
        
        getDeptDateBySelectedRoute();
    }

     function getDeptDateBySelectedRoute() {
        var selectedRouteAtDeliveryDate = $j('#routeDDT option:selected').val();
        
        $j
           .ajax({
                    async : false,
                    url : globals.BASE + '/selection/assignment/getDepartureTimeByRouteAndDeliveryDate.action',
                    data : {
                         routeAtDeliveryDate : selectedRouteAtDeliveryDate
                    },
                    datatype : 'json',
                    success : function(output) {
                       modifyRouteDialogInit(output);
                    }
                });
    }
    
    function modifyRouteDialogInit(routeDepartureDateTime) {
    
        var deptDateTime = routeDepartureDateTime.routeDepartureDate
    
        var routeDeptDateVar = parseInt(deptDateTime.substring(0,4)) + '-' + parseInt(deptDateTime.substring(5,7)) + '-' + parseInt(deptDateTime.substring(8,10));

        var routeDeptDateStr = convertToDate(routeDeptDateVar);

        $('calendarLinkDepartureDate').innerHTML=routeDeptDateStr;
        $('routeDepartureDate').value=routeDeptDateVar;

        $j('#fromHours').val(parseInt(deptDateTime.substring(11,13))).attr("selected", "selected");
        $j('#fromMinutes').val(parseInt(deptDateTime.substring(14,16))).attr("selected", "selected");
    }

    function changeDepartureDate() {
        $('calendarLinkDepartureDate').innerHTML=convertToDate($('routeDepartureDate').value);
    }

    function cancelChangeRouteDDT() {
        $("dialogDIV").style.display="none";
        $("tempDialogContainer").appendChild($("dialogDIV"));
    }

    function convertToDate(dateString) {   
         // Split the date into year part[0], month part[1], and date part[2]
         var parts=dateString.split("-");

         // Make a new date object based on those parts
         var d=new Date(parts[0], parts[1]-1, parts[2]);

         // Build an array with the localized struts strings for week day
         var weekday=new Array(7);
         weekday[0]="<@s.text name='calender.day.short.sunday'/>";
         weekday[1]="<@s.text name='calender.day.short.monday'/>";
         weekday[2]="<@s.text name='calender.day.short.tuesday'/>";
         weekday[3]="<@s.text name='calender.day.short.wednesday'/>";
         weekday[4]="<@s.text name='calender.day.short.thursday'/>";
         weekday[5]="<@s.text name='calender.day.short.friday'/>";
         weekday[6]="<@s.text name='calender.day.short.saturday'/>";

         // Store the day based on the numerical value of the day into a var
         var dday=weekday[d.getDay()];

         // Build an array with the localized struts string for month
         var month=new Array(12);
         month[0]="<@s.text name='calender.month.long.january'/>";
         month[1]="<@s.text name='calender.month.long.february'/>";
         month[2]="<@s.text name='calender.month.long.march'/>";
         month[3]="<@s.text name='calender.month.long.april'/>";
         month[4]="<@s.text name='calender.month.long.may'/>";
         month[5]="<@s.text name='calender.month.long.june'/>";
         month[6]="<@s.text name='calender.month.long.july'/>";
         month[7]="<@s.text name='calender.month.long.august'/>";
         month[8]="<@s.text name='calender.month.long.september'/>";
         month[9]="<@s.text name='calender.month.long.october'/>";
         month[10]="<@s.text name='calender.month.long.november'/>";
         month[11]="<@s.text name='calender.month.long.december'/>";

         // Store the month based on the numerical value of the month into a var
         var dmonth=month[d.getMonth()];

         // Store the date from the orginal parts array
         var ddate=parts[2];
 
         // Store the year from the original parts array
         var dyear=parts[0];

         // Finally put all of the parts together in one string and return it to the caller
         var localizedDate=dday + ", " + dmonth + " " + ddate + ", " + dyear; 
         return localizedDate;
    }
    
    function updateDDT() {
        $j
           .ajax({
                    async : false,
                    url : globals.BASE + '/selection/assignment/updateDepartureDate.action',
                    data : {
                         routeAtDeliveryDate : $('routeDDT').value,
                         routeDepartureDate  : $('routeDepartureDate').value + '-' + $('fromHours').value + '-' + $('fromMinutes').value + '-0'
                    },
                    datatype : 'json',
                    success : function(d) {
                       response = d; 
                    }
                });
       onError(response);
    }
    
    function onError(request) {
        if (request.errorCode != null && request.errorCode == 1) {
            writeStatusMessage(request.message,"actionMessage error");
        } else {
            writeStatusMessage(request.generalMessage, 'actionMessage success');
        }
    }

</script>

<div id="menuBlade_listAssignmentsTable">
<ul class="NavList" id="bladeList_listAssignmentsTable">
    <@action
      name="nav.assignment.groupAssignments"
      tableId="listAssignmentsTable"
      id="a_groupAssignments"
      url="javascript:showGroupAssignmentsDialog();"
      tabIndex="252"
      actionEnablers={"enableOnMoreThanOne":"assignment.group.error.twoOrMoreAssignmentsRequired"}
      featureName="feature.selection.assignment.group" /> 
	  	
    <@action
      name="nav.assignment.ungroupAssignments"
      tableId="listAssignmentsTable"
      id="a_ungroupAssignments"
      url="javascript:showUngroupAssignmentsDialog();"
      tabIndex="253"
      actionEnablers={"enableOnMoreThanNone":"assignment.ungroup.error.selectOneOrMore"}
      featureName="feature.selection.assignment.ungroup" />

   <@action
      name="nav.assignment.editAssignment"
      tableId="listAssignmentsTable"
      id="a_editAssignment"
      url="javascript:performActionOnSelected(assignmentObj,editAssignmentActionURL);"
      tabIndex="253"
      actionEnablers={"enableOnOne":"assignment.edit.error.selectOnlyOneAssignment"}
      featureName="feature.selection.assignment.edit" />
      
     <@action
       name="nav.assignment.modifyDeliveryLocation"
       tableId="listAssignmentsTable"
       id="a_modifyDeliveryLocation"
       url="javascript:showModifyDeliveryLocationDialog()" 
       tabIndex="255"
       actionEnablers={"enableOnMoreThanNone":"modifyDeliveryLocation.label.error.selectOneorMoreAssignments"}
       featureName="feature.voicelink.deliveryLocationMapping.modifyDeliveryLocation" />
      
     <@action
       name="nav.assignment.printLabel"
       tableId="listAssignmentsTable"
       id="a_printLabelAssignment"
       url="javascript:getDataOnSelectedForAssignmentPrintLabel(assignmentObj,getAssignmentPrintLabelUrl);"
       tabIndex="256"
       actionEnablers={"enableOnMoreThanNone":"print.label.error.selectOneorMoreAssignments"}
       featureName="feature.selection.assignment.printLabel" />
       	
     <@action
       name="nav.assignment.resequence"
       tableId="listAssignmentsTable"
       id="a_resequenceAssignments"
       url="javascript:getDataOnSelectedForAssignmentResequence(assignmentObj,getResequenceRegionURL);"
       tabIndex="254"
       actionEnablers={}
       featureName="feature.voicelink.assignment.resequence" />    
       
	<@action 
		name="nav.assignment.priority" 
		tableId=""
		id="a_prioritizeAssignments" 
		url="/selection/assignment/prioritize!input.action" 
		tabIndex="255" 
		actionEnablers={} 
		featureName="feature.selection.assignment.prioritize" />    
       
	<@action 
		name="nav.route.departureDate" 
		tableId=""
		id="a_modifyDepartureDate" 
		url="javascript:getRouteWithDeliveryDateForDepTimeUpdate(getModifyRouteURL);"
		tabIndex="256" 
		actionEnablers={} 
		featureName="feature.selection.route.change.departureDate" />
</ul>
</div>

<div id="tempDialogContainer">
<div id="dialogDIV" style="display: none;">

<div style="display: none;">
    <@s.textfield tabindex="1"
       name="routeDepartureDate"
       id="routeDepartureDate"
       size="15" maxlength="50"
       show="true"
       theme="simple"
       onchange="javascript:changeDepartureDate();"
    />
</div>

<table border="0" class="content" width="250px" class="modifyRouteDeptDateDialog">
    <tr>
        <td colspan="6">
             <div id="textContainer"><@s.text name="route.modify.departure.time.text" /></div> 
        </td>
    </tr>
    <tr><td></br></td></tr>
    <tr>
    <td style="font-weight: bold;"><@s.text name="entity.route" /><@s.text name='semicolon'/></td>
    <td class="routeSelect" align="right">
    <@s.select tabindex="2"
        cssStyle="width: 140px"
        theme="css_nonbreaking"
        label="%{getText('entity.route')}"
        name="routeDDT"
        id="routeDDT"
        list="routeDeliveryDateMap"
        listKey="key"
        listValue="value"
        emptyOption="false"
        size="1"
        length="50"
        show="true"
        required="false"
        readonly="false"
        onchange="getDeptDateBySelectedRoute();"/>
    </td>
    </tr>
    <tr><td></br></td></tr>
    <tr>
    <td style="font-weight: bold;">
        <@s.text name="%{getText('route.selected.departureDate')}" /><@s.text name='semicolon'/>
    </td>
    <td style="padding-left:5px;">
       <@customdialogdate
           dialogId="route_dept_date"
           header="route.departureDate.select"
           dateInputFieldId="routeDepartureDate"
           callingButtonId="calendarLinkDepartureDate"
           multiPage="false"/>
        <a id="calendarLinkDepartureDate" href="#"><@s.text name="%{getText('route.selected.departureDate.select')}"/></a>
    </td>
    <td>
         <@s.select tabindex="3"
            cssStyle="width: 40px"
            theme="simple"
            label=""
            name="fromHours"
            id="fromHours"
            list="hoursMap"
            listKey="key"
            listValue="value"
            emptyOption="false"
            size="1"
            length="50"
            show="true"
            required="false"
            readonly="false"/>
       </td>
       <td>
        <@s.text name="%{getText('semicolon')}"/>
       </td>
       <td class="scheduleTime">
         <@s.select tabindex="4"
            cssStyle="width: 40px"
            theme="simple"
            label="%{getText('semicolon')}"
            name="fromMinutes"
            id="fromMinutes"
            list="minutesMap"
            listKey="key"
            listValue="value"
            emptyOption="false"
            size="1"
            length="50"
            show="true"
            required="false"
            readonly="false"/>
        </td>
        <td>
           <@s.text name="%{getText('schedule.edit.daily.timeformat.24')}"/>
        </td>
        </tr>     
</table>

</div>
</div>

<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.pick.actions'/></div>
<div id="menuBlade_listPicksTable">
<ul class="NavList" id="bladeList_listPicksTable">
      <@action
      name="nav.assignment.splitAssignment"
      tableId="listPicksTable"
      id="a_splitPicks"
      url="javascript:getDataOnSelected(pickObj,validateSplitURL)"
      tabIndex="253"
      actionEnablers={"enableOnOneAssignmentMoreThanNonePicks":"assignment.split.error.oneAssignmentMultiplePicks"}
      featureName="feature.selection.pick.split" />
           
      <@action
      name="nav.pick.manualPick"
      tableId="listPicksTable"
      id="a_manualPick"
      url="javascript:validateSelectedPick(pickObj, validateManualPickURL, \\'pickId\\');"
      tabIndex="253"
      actionEnablers={"enableOnOne":"assignment.manualpick.error.selectOnlyOnePick"}
      featureName="feature.selection.pick.manualpick" />

      <@action
      name="nav.pick.cancelPick"
      tableId="listPicksTable"
      id="a_cancelPick"
      url="javascript:showCancelConfirmDialog();"
      tabIndex="253"
      actionEnablers={"enableOnMoreThanNone":"assignment.split.error.selectOneorMorePicks"}
      featureName="feature.selection.pick.cancelpick" />
            
     <@action
      name="nav.pick.printLabel"
      tableId="listPicksTable"
      id="a_printLabel"
      url="javascript:getDataOnSelectedForPickPrintLabel(pickObj,getPickPrintLabelUrl);"
      tabIndex="254"
      actionEnablers={"enableOnMoreThanNone":"print.label.error.selectOneorMorePicks"}
      featureName="feature.selection.pick.printLabel" />
</ul>
</div>
</#if>
</#if>


<!--  implementing help links --->

<script type="text/javascript">
<#if navigation.pageName == "edit!input" || navigation.pageName == "edit">
    <#global helpFiles="Content/Selection/Assignments_and_Picks_Fields.htm" />
    contextHelpUrl = 48;
<#elseif navigation.pageName == "prioritize"/>
    <#global helpFiles="Content/Selection/Assignments_and_Picks_Fields.htm" />
    contextHelpUrl = 97;
<#else>
    <#global helpFiles="Content/PlaceholderPages/AssignmentsAndPicksPagePlaceholder.htm" />
    contextHelpUrl = 2;
</#if>
</script>
