<#include "/include/common/action.ftl" />
<#global helpFiles="PlaceholderPages/ContainersAndPickDetailsPagePlaceholder.htm"/>
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.container.actions'/></div>
<script type="text/javascript">
	var	getPrintLabelUrl = '${base}/selection/print/getContainerPrintLabel.action'
	var printLabelURL = '${base}/selection/print/containerPrintLabel.action';
	
	function showContainerPrintLabelDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='nav.printLabel'/>",			    
				body: dialogBody,
				button1: "<@s.text name='printLabel.yesText'/>",
				button2: "<@s.text name='printlabel.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnPrintLabel, containerObj, printLabelURL), "");
	}
	
	contextHelpUrl = 5;
</script>
<ul class="NavList">
     <@action
       name="nav.container.printLabel"
       tableId="listContainersTable"
       id="a_printLabel"
       url="javascript:getDataOnSelectedForContainerPrintLabel(containerObj,getPrintLabelUrl);"
       tabIndex="254"
       actionEnablers={"enableOnMoreThanNone":"print.label.error.selectOneorMoreContainers"}
       featureName="feature.selection.container.printLabel" />
</ul>
