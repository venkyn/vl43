<#include "/include/common/action.ftl" />
<#if navigation.pageName == "list" || navigation.pageName == "default" || navigation.pageName = "viewRegion" || navigation.pageName == "edit">
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.region.actions'/></div>
<script type="text/javascript">
	var changeSummaryPromptURL = '${base}/selection/region/getSummaryPrompt.action';
	var deleteRegionURL = '${base}/selection/region/delete.action';
	var assignSummaryPromptURL= '${base}/selection/region/assignSummaryPrompt.action';
	
	 function showDeleteRegionDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.regions'/>" ,
			    button1: "<@s.text name='delete.yesText.regions'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, regionObj,deleteRegionURL), "");
	}	

	 function showDeleteCurrentRegionDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.region'/>" ,
			    button1: "<@s.text name='delete.yesText.region'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	function showSummaryPromptRegionDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='region.summaryPrompt.title'/>",			    
				body: dialogBody ,
			    button1: "<@s.text name='region.summaryPrompt.yesText'/>",
				button2: "<@s.text name='region.summaryPrompt.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSummaryPromptForRegions, regionObj, assignSummaryPromptURL), "");
	}	
	
   function redirectCurrentDelete(){
      <#if regionId?has_content>
     	window.location="${base}/selection/region/deleteCurrentRegion.action?regionId=${regionId?c}";
      </#if>
    }
	
</script>
<#-- 
    This is a temporary IE fix that must be overridden for each actions menu 
    that has message bubbles 
-->
<!--[if IE]>
<style>
  div#sidenav div.actionBubbleContainer {
    bottom: 318px;
  }
</style>
<![endif]-->
<ul id="NavList">

        <#if navigation.pageName == "default">
            <#assign actionName = "nav.region.listpage.">
        <#elseif navigation.pageName == "viewRegion">
             <#assign actionName = "nav.viewpage.region.">
        <#else>
        	<#if navigation.pageName != "viewSelectionProfile">
	            <#assign actionName = "nav." + navigation.pageName + "page.region.">
	        </#if>
        </#if>        

		<#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
			    name=actionName + "view"
			    tableId="listRegionTable"
	            id="a_viewRegion"
	            url="javascript:gotoUrlWithSelected(regionObj, \\'${base}/selection/region/viewRegion!input.action\\', \\'regionId\\');"
	            tabIndex="251"
                actionEnablers={"enableOnOne":"region.view.disabled.selection"} 
	            featureName="feature.selection.region.view" />
	    </#if>
	    <#if navigation.pageName == "list" || navigation.pageName == "default" || navigation.pageName = "view" || navigation.pageName == "edit">
		    <@action
		         name=actionName + "create" 
		         tableId="listRegionTable"
	             id="a_createRegion" 
	             url="/selection/region/create!input.action" 
	             tabIndex="251" 
	             actionEnablers={} 
	             featureName="feature.selection.region.create" />	    
	    </#if>
   	    <#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
                name=actionName + "edit"
                tableId="listRegionTable"
		        id="a_editRegion"
                tabIndex="253"
                url="javascript:performActionOnSelected(regionObj, \\'${base}/selection/region/checkIfEditable.action\\');"                
                actionEnablers={"enableOnOne":"region.edit.disabled.selection"}
                featureName="feature.selection.region.edit" />         
        </#if>
        <#if navigation.pageName == "viewRegion">
			<@action
                name=actionName + "edit"
                tableId="listRegionTable"
		        id="a_editRegion"
                tabIndex="253"
                url="javascript:performAction(${regionId?c}, \'${base}/selection/region/checkIfEditable.action\');"
                actionEnablers={}
                featureName="feature.selection.region.edit" />                   
        </#if>
        <#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action			
			    name=actionName + "delete"
			    tableId="listRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteRegionDialog();"
		        tabIndex="252"
	            actionEnablers={"enableOnMoreThanNone":"region.delete.disabled.selection"}
		        featureName="feature.selection.region.delete" />
		</#if>
        <#if navigation.pageName == "viewRegion" || navigation.pageName == "edit">		   
			<@action			
			    name=actionName + "delete"
			    tableId="listRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteCurrentRegionDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.selection.region.delete" />
        </#if>   

        <#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action			
   	    	    name=actionName + "duplicate"
			    tableId="listRegionTable"
		        id="a_duplicateRegion"
		        url="javascript:gotoUrlWithSelected(regionObj, \\'${base}/selection/region/create!duplicate.action\\', \\'regionId\\');"
		        tabIndex="253"
	            actionEnablers={"enableOnOne":"region.duplicate.disabled.selection"}
		        featureName="feature.selection.region.create" />
		</#if>        
        <#if navigation.pageName == "viewRegion" || navigation.pageName == "edit">		   
			<@action			
   	    	    name=actionName + "duplicate"
			    tableId="listRegionTable"
		        id="a_duplicateRegion"
		        url="/selection/region/create!duplicate.action?regionId=${regionId?c}"
		        tabIndex="253"
	            actionEnablers={}
		        featureName="feature.selection.region.create" />
		</#if>
		 <#if navigation.pageName == "list" || navigation.pageName == "default">			
	    
	          <@action 
				name="nav.region.assignPrompt" 
				tableId="listRegionTable"
				id="a_assignPrompt"
				url="javascript:getSummaryPromptForRegions(regionObj, changeSummaryPromptURL)" 
				tabIndex="253" 
				actionEnablers={"enableOnMoreThanNone":"region.delete.disabled.selection"}
		        featureName="feature.selection.region.edit" />
				
       </#if>
	    
</ul>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "showAssignOperators">
   <#global helpFiles="Core/Operators/Assign.htm"/>
   contextHelpUrl = 59;
<#elseif navigation.pageName =="create" || navigation.pageName == "create!input">
   <#global helpFiles="Selection/Region_Create.htm"/>
   contextHelpUrl = 60;
<#elseif navigation.pageName =="edit" || navigation.pageName == "edit!input">
   <#global helpFiles="Selection/Region_Edit.htm"/>
   contextHelpUrl = 60;
<#elseif navigation.pageName == "viewRegion!input" || navigation.pageName == "viewRegion">
    <#global helpFiles="Selection/Region_View.htm"/>
    contextHelpUrl = 62;
<#elseif navigation.pageName == "create!duplicate">
    <#global helpFiles="Selection/Region_Duplicate.htm"/>
    contextHelpUrl = 61;
<#else>  
   <#global helpFiles="PlaceholderPages/RegionsPagePlaceholder.htm"/>
   contextHelpUrl = 23;
</#if>
</script>