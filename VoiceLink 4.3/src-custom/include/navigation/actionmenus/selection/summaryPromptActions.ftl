<#include "/include/common/action.ftl" />

<#assign pageContext = "${base}/${navigation.applicationMenu}">
<#if navigation.pageName != "create">
<#if !language?has_content>
    <#assign language=""/>
</#if>
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.summaryPrompt.actions'/></div>
<script type="text/javascript">
	
	var editSummaryPromptURL = '${pageContext}/summaryPrompt/edit!input.action';	
	var viewSummaryPromptURL = '${pageContext}/summaryPrompt/view.action';
	var addLanguageURL = '${pageContext}/summaryPrompt/checkIfLanguagesPresent.action';
	var editLanguageURL = '${pageContext}/summaryPrompt/editLanguage.action';
	var viewLanguageURL = '${pageContext}/summaryPrompt/viewLanguage.action';
	var getLanguagesURL = '${pageContext}/summaryPrompt/getLanguage.action';
	var deleteSummaryPromptURL = '${pageContext}/summaryPrompt/delete.action';
	var deleteSummaryPromptLanguageURL = '${pageContext}/summaryPrompt/deleteLanguage.action';
	var deleteSummaryPromptLanguageRedirectURL = '${pageContext}/summaryPrompt/default.action';
	
  	
  function showDeleteSummaryPromptDialog() {
		var dialogProperties = {
			    title: "<@s.text name='summaryPrompt.delete.prompt.title'/>",			    
				body: "<@s.text name='summaryPrompt.delete.prompts.body'/>" ,
			    button1: "<@s.text name='summaryPrompt.delete.prompts.yesText'/>",
				button2: "<@s.text name='summaryPrompt.delete.prompt.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, summaryPromptObj,deleteSummaryPromptURL), "");
	}	
   
   function showDeleteCurrentSummaryPromptDialog() {
		var dialogProperties = {
			    title: "<@s.text name='summaryPrompt.delete.prompt.title'/>",			    
				body: "<@s.text name='summaryPrompt.delete.prompt.body'/>" ,
			    button1: "<@s.text name='summaryPrompt.delete.prompt.yesText'/>",
				button2: "<@s.text name='summaryPrompt.delete.prompt.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	

	 function showDeleteSummaryPromptLanguageDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='summaryPrompt.language.delete.title'/>",			    
				body: dialogBody ,
			    button1: "<@s.text name='summaryPrompt.language.remove.yesText'/>",
				button2: "<@s.text name='summaryPrompt.language.remove.noText'/>"}
		 buildCustomDialog(dialogProperties,
			partial(performActionOnSelectedForLanguageSummaryPrompt, summaryPromptObj,deleteSummaryPromptLanguageURL, deleteSummaryPromptLanguageRedirectURL), "");
	}	
	
	 function showDeleteCurrentSummaryPromptLanguageDialog() {
		var dialogProperties = {
			    title: "<@s.text name='summaryPrompt.language.delete.title'/>",			    
				body: "<@s.text name='summaryPrompt.language.delete.body'/>" ,
			    button1: "<@s.text name='summaryPrompt.currentLanguage.delete.yesText'/>",
				button2: "<@s.text name='summaryPrompt.currentLanguage.delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentlanguageDelete,"",""), "");
	}	

    function showViewSummaryPromptLanguageDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='summaryPrompt.language.view.title'/>",			    
				body: dialogBody ,
			    button1: "<@s.text name='summaryPrompt.language.view.yesText'/>",
				button2: "<@s.text name='summaryPrompt.language.view.noText'/>"}
		 buildCustomDialog(dialogProperties,
			partial(performActionOnSelectedForLanguageView, summaryPromptObj,viewLanguageURL), "");
	}	
	
    function showEditSummaryPromptLanguageDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='summaryPrompt.language.edit.title'/>",			    
				body: dialogBody ,
			    button1: "<@s.text name='summaryPrompt.language.edit.yesText'/>",
				button2: "<@s.text name='summaryPrompt.language.edit.noText'/>"}
		 buildCustomDialog(dialogProperties,
			partial(performActionOnSelectedForLanguageSummaryPromptForEdit, summaryPromptObj,editLanguageURL), "");
	}	
	
	function redirectCurrentlanguageDelete(){
	   <#if summaryPromptId?has_content>
          <#if language?has_content>
      	   window.location="${pageContext}/summaryPrompt/deleteCurrentLanguage.action?summaryPromptId=${summaryPromptId?c}&language=${language}";
     	 </#if>
      </#if>
    }
	
   function redirectCurrentDelete(){
      <#if summaryPromptId?has_content>
     	window.location="${pageContext}/summaryPrompt/deleteCurrentSummaryPrompt.action?summaryPromptId=${summaryPromptId?c}";
      </#if>
    }
	
</script>

<ul class="NavList">
	<#if navigation.pageName == "list" || navigation.pageName == "default">
	<script type="text/javascript">
		contextHelpUrl = 35;
	</script>
		<@action 
			name="nav.summaryPrompt.view" 
			tableId="listSummaryPromptTable"
			id="a_viewSummaryPrompt" 
			url="javascript:gotoUrlWithSelected(summaryPromptObj, viewSummaryPromptURL, \\'summaryPromptId\\');" 
			tabIndex="251" 
			actionEnablers={"enableOnOne":"summaryPrompt.view.disabled.selection"} 
			featureName="feature.selection.summaryPrompt.view" />
	
		<@action 
			name="nav.summaryPrompt.create" 
			tableId="listSummaryPromptTable"
			id="a_createSummaryPrompt" 
			url="/${navigation.applicationMenu}/summaryPrompt/create!input.action"
			tabIndex="252" 
			actionEnablers={} 
			featureName="feature.selection.summaryPrompt.create" />
	
		<@action 
			name="nav.summaryPrompt.edit" 
			tableId="listSummaryPromptTable"
			id="a_editSummaryPrompt"  
			url="javascript:gotoUrlWithSelected(summaryPromptObj, editSummaryPromptURL, \\'summaryPromptId\\');" 
			tabIndex="253" 
			actionEnablers={"enableOnOne":"summaryPrompt.edit.disabled.selection"} 
			featureName="feature.selection.summaryPrompt.edit" />
	
		<@action 
			name="nav.summaryPrompt.delete" 
			tableId="listSummaryPromptTable"
			id="a_deleteOperator"  
			url="javascript: showDeleteSummaryPromptDialog();" 
			tabIndex="254" 
			actionEnablers={"enableOnMoreThanNone":"summaryPrompt.delete.disabled.selection"} 
			featureName="feature.selection.summaryPrompt.delete" />
	
	<#elseif navigation.pageName == "edit">
	<script type="text/javascript">
		contextHelpUrl = 69;
	</script>
		
		<@action 
			name="nav.summaryPrompt.create" 
			tableId="listSummaryPromptTable"
			id="a_createSummaryPrompt" 
			url="/${navigation.applicationMenu}/summaryPrompt/create!input.action" 
			tabIndex="252" 
			actionEnablers={} 
			featureName="feature.selection.summaryPrompt.create" />
	
		<@action
			name="nav.summaryPrompt.delete"
			tableId="listSummaryPromptTable"
			id="a_deleteSummaryPrompt"
			url="javascript: showDeleteCurrentSummaryPromptDialog();"
			tabIndex="252"
			actionEnablers={}
			featureName="feature.selection.summaryPrompt.delete" />
		
	<#elseif navigation.pageName == "view" || navigation.pageName == "viewLanguage" || navigation.pageName == "editLanguage" || navigation.pageName == "createLanguage">		
	<script type="text/javascript">
		contextHelpUrl = 65;
	</script>
		<@action 
			name="nav.summaryPrompt.create" 
			tableId="listSummaryPromptTable"
			id="a_createSummaryPrompt" 
			url="/${navigation.applicationMenu}/summaryPrompt/create!input.action" 
			tabIndex="252" 
			actionEnablers={} 
			featureName="feature.selection.summaryPrompt.create" />
			
		<@action 
			name="nav.summaryPrompt.edit" 
			tableId="listSummaryPromptTable"
			id="a_editSummaryPrompt"  
			url="/${navigation.applicationMenu}/summaryPrompt/edit!input.action?summaryPromptId=${summaryPromptId?c}"
			tabIndex="253" 
			actionEnablers={} 
			featureName="feature.selection.summaryPrompt.edit" />
	
		<@action 
			name="nav.summaryPrompt.delete" 
			tableId="listSummaryPromptTable"
			id="a_deleteSummaryPrompt"  
			url="javascript: showDeleteCurrentSummaryPromptDialog();" 
			tabIndex="254" 
			actionEnablers={} 
			featureName="feature.selection.summaryPrompt.delete" />
	
	</#if>		
</ul>
<#if navigation.pageName != "createLanguage">
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.language.actions'/></div>
<ul class="NavList">
<#if navigation.pageName == "list" || navigation.pageName == "default">

		<@action 
			name="nav.language.view" 
			tableId="listSummaryPromptTable"
			id="a_viewLanguage" 
			url="javascript:getLanguagesOnSelectedSummaryPrompt(summaryPromptObj, getLanguagesURL)" 
			tabIndex="251" 
			actionEnablers={"enableOnOne":"summaryPrompt.viewLanguage.disabled.selection"} 
			featureName="feature.selection.summaryPrompt.view" />
	
		<@action 
			name="nav.language.create" 
			tableId="listSummaryPromptTable"
			id="a_createLanguage" 
			url="javascript:performActionOnSelected(summaryPromptObj, addLanguageURL);"
            tabIndex="252" 
			actionEnablers={"enableOnOne":"summaryPrompt.createLanguage.disabled.selection"}
			featureName="feature.selection.summaryPrompt.create" />
	
		<@action 
			name="nav.language.edit" 
			tableId="listSummaryPromptTable"
			id="a_editLanguage"
			url="javascript:getLanguagesOnSelectedSummaryPromptEdit(summaryPromptObj, getLanguagesURL)" 
			tabIndex="253" 
			actionEnablers={"enableOnOne":"summaryPrompt.editLanguage.disabled.selection"}
			featureName="feature.selection.summaryPrompt.edit" />
	
		<@action 
			name="nav.language.delete" 
			tableId="listSummaryPromptTable"
			id="a_deleteLanguage"  
			url="javascript:getLanguagesOnSelectedSummaryPromptDelete(summaryPromptObj, getLanguagesURL)" 
			tabIndex="254" 
			actionEnablers={"enableOnOne":"summaryPrompt.deleteLanguage.disabled.selection"} 
			featureName="feature.selection.summaryPrompt.delete" />
		
	<#elseif navigation.pageName == "editLanguage">
	<script type="text/javascript">
		contextHelpUrl = 70;
	</script>
		
		<@action 
			name="nav.language.create" 
			tableId="listSummaryPromptTable"
			id="a_createLanguage" 
		    url="/${navigation.applicationMenu}/summaryPrompt/createLanguage!input.action?summaryPromptId=${summaryPromptId?c}"
			tabIndex="252" 
			actionEnablers={}
			featureName="feature.selection.summaryPrompt.create" />
	
	   <@action
	 		name="nav.language.delete"
	 		tableId="listSummaryPromptTable"
			id="a_deleteLanguage"
			url="javascript:showDeleteCurrentSummaryPromptLanguageDialog();"
			tabIndex="253"
			actionEnablers={}
			featureName="feature.selection.summaryPrompt.delete" />
			
	<#elseif  navigation.pageName == "view" || navigation.pageName == "edit">
	<script type="text/javascript">
		contextHelpUrl = 68;
	</script>
     
     <@action 
		name="nav.language.create" 
		tableId="listSummaryPromptTable"
		id="a_createLanguage" 
		url="/${navigation.applicationMenu}/summaryPrompt/createLanguage!input.action?summaryPromptId=${summaryPromptId?c}"
		tabIndex="252" 
		actionEnablers={}
		featureName="feature.selection.summaryPrompt.create" />
	
	<#elseif navigation.pageName == "viewLanguage">		
	<script type="text/javascript">
		contextHelpUrl = 69;
	</script>
	
		<@action 
			name="nav.language.create" 
			tableId="listSummaryPromptTable"
			id="a_createLanguage" 
			url="/${navigation.applicationMenu}/summaryPrompt/createLanguage!input.action?summaryPromptId=${summaryPromptId?c}"
			tabIndex="252" 
			actionEnablers={}
			featureName="feature.selection.summaryPrompt.create" />
			
	   <@action 
			name="nav.language.edit" 
			tableId="listSummaryPromptTable"
			id="a_editLanguage"
			url="/${navigation.applicationMenu}/summaryPrompt/editLanguage.action?summaryPromptId=${summaryPromptId?c}&language=${language}"
			tabIndex="253" 
			actionEnablers={}
			featureName="feature.selection.summaryPrompt.edit" />
				
	   <@action
	 		name="nav.language.delete"
	 		tableId="listSummaryPromptTable"
			id="a_deleteLanguage"
			url="javascript:showDeleteCurrentSummaryPromptLanguageDialog();"
			tabIndex="253"
			actionEnablers={}
			featureName="feature.selection.summaryPrompt.delete" />
	  
	</#if>		
</ul>
</#if>
</#if>

<script type="text/javascript">
<#if navigation.pageName?starts_with("createLanguage")>
	contextHelpUrl = 67;
<#elseif navigation.pageName?starts_with("editLanguage")>
	contextHelpUrl = 70;
<#elseif navigation.pageName?starts_with("create")>
	contextHelpUrl = 68;
<#elseif navigation.pageName?starts_with("edit")>
    contextHelpUrl = 69;
<#elseif navigation.pageName == "viewLanguage">
	contextHelpUrl = 69;
<#elseif navigation.pageName != "view" && navigation.pageName != "edit">
	contextHelpUrl = 35;
</#if>
</script>
