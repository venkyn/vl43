<#include "/include/common/action.ftl" />
<#global helpFiles="PlaceholderPages/ShortsPagePlaceholder.htm"/>
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.short.actions'/></div>
<script type="text/javascript">
	var createChaseAssignmentURL = '${base}/selection/short/createChase.action';
	var setToReplenishedURL = '${base}/selection/short/setToReplenished.action';
	var markoutActionURL = '${base}/selection/short/markout.action';
	var acceptQuantityPickedActionURL = '${base}/selection/short/acceptQuantityPicked.action';	
	var getOperatorsURL = '${base}/selection/short/getOperatorsDialogue.action';
	
	function showCreateChaseAssignmentDialog(dialogBody) {
		var dialogProperties = {
				<#assign dialogTitle> <@s.text name='short.createChaseAssignment.confirmation.header'/> </#assign>			
				<#assign dialogButton1> <@s.text name='short.createChaseAssignment.confirmation.ok.button'/> </#assign>
				<#assign dialogButton2> <@s.text name='short.createChaseAssignment.confirmation.cancel.button'/> </#assign>				
			    title: "${dialogTitle?js_string}",			    
				body: dialogBody,
			    button1: "${dialogButton1?js_string}",
				button2: "${dialogButton2?js_string}"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelectedShort, shortObj, createChaseAssignmentURL), "");
	}
	
	 function showMarkOutShortDialog() {
		var dialogProperties = {
				<#assign dialogTitle> <@s.text name='short.markout.confirmation.header'/> </#assign>			
				<#assign dialogBody> <@s.text name='short.markout.confirmation.body'/> </#assign>
				<#assign dialogButton1> <@s.text name='short.markout.confirmation.ok.button'/> </#assign>
				<#assign dialogButton2> <@s.text name='short.markout.confirmation.cancel.button'/> </#assign>				
			    title: "${dialogTitle?js_string}",			    			    
			    body: "${dialogBody?js_string}",
			    button1: "${dialogButton1?js_string}",
				button2: "${dialogButton2?js_string}"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, shortObj,markoutActionURL), "");
	  }	
	
	 function showAcceptQuantityDialog() {
		var dialogProperties = {
				<#assign dialogTitle> <@s.text name='short.acceptQuantityPicked.confirmation.header'/> </#assign>
				<#assign dialogBody> <@s.text name='short.acceptQuantityPicked.confirmation.body'/> </#assign>							
				<#assign dialogButton1> <@s.text name='short.acceptQuantityPicked.confirmation.ok.button'/> </#assign>
				<#assign dialogButton2> <@s.text name='short.acceptQuantityPicked.confirmation.cancel.button'/> </#assign>				
			    title: "${dialogTitle?js_string}",
			    body: "${dialogBody?js_string}",
			    button1: "${dialogButton1?js_string}",
				button2: "${dialogButton2?js_string}"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, shortObj,acceptQuantityPickedActionURL), "");
	  }	
	  
	   function showSetToReplenishedDialog() {
		var dialogProperties = {
				<#assign dialogTitle> <@s.text name='short.setToReplenished.confirmation.header'/> </#assign>
				<#assign dialogBody> <@s.text name='short.setToReplenished.confirmation.body'/> </#assign>							
				<#assign dialogButton1> <@s.text name='short.setToReplenished.confirmation.ok.button'/> </#assign>
				<#assign dialogButton2> <@s.text name='short.setToReplenished.confirmation.cancel.button'/> </#assign>				

			    title: "${dialogTitle?js_string}",
			    body: "${dialogBody?js_string}",			    					    
			    button1: "${dialogButton1?js_string}",
				button2: "${dialogButton2?js_string}"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, shortObj,setToReplenishedURL), "");
	  }	
	  
	
	contextHelpUrl = 33;
</script>

<ul class="NavList">
	<@action
		name="nav.short.markout"
		tableId="listShortsTable"
		id="a_markoutShort"
		url="javascript: showMarkOutShortDialog();"
		tabIndex="251"
		actionEnablers={"enableOnMoreThanNone":"short.markout.disabled.selection"}
		featureName="feature.selection.short.markout" />
	<#include "/include/common/customdialog.ftl">
	
	<@action
		name="nav.short.acceptQuantityPicked"
		tableId="listShortsTable"
		id="a_acceptQuantityPicked"
		url="javascript: showAcceptQuantityDialog()"
		tabIndex="252"
		actionEnablers={"enableOnOne":"short.acceptQuantityPicked.disabled.selection"}
		featureName="feature.selection.short.acceptQuantityPicked" /> 
	
	<@action
		name="nav.short.createChaseAssignment"
		tableId="listShortsTable"
		id="a_createChaseAssignment"
		url="javascript:getOperatorListOnSelectedShort(shortObj,getOperatorsURL)"
		tabIndex="253"
		actionEnablers={"enableOnMoreThanNone":"short.createChaseAssignment.disabled.selection"}
		featureName="feature.selection.short.createChaseAssignment" />

	<@action
		name="nav.short.setToReplenished"
		tableId="listShortsTable"
		id="a_setToReplenished"
		url="javascript:showSetToReplenishedDialog();"
		tabIndex="253"
		actionEnablers={"enableOnMoreThanNone":"short.setToReplenished.disabled.selection"}
		featureName="feature.selection.short.setToReplenished" />

</ul>
