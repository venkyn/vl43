<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<#if navigation.pageName != "create" && navigation.pageName != "showAssignOperators"
           && navigation.pageName != "assignOperators" && navigation.pageName != "unassignOperators">
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.operator.actions'/></div>
<script type="text/javascript">
	var signOffOperatorActionURL = '${pageContext}/operator/signOffOperator.action';
	var editOperatorURL = '${pageContext}/operator/edit!input.action';	
	var viewOperatorURL = '${pageContext}/operator/view.action';
	var workgroupDialogURL = '${pageContext}/operator/showChangeWorkgroupDialog.action';
	var assignWorkgroupURL = '${pageContext}/operator/assignToWorkgroup.action';
	var viewLaborHistoryURL = '${pageContext}/labor/list.action';
	var deleteOperatorURL = '${pageContext}/operator/delete.action';
	
	
	function gotoUrlWithOperator (tableObj, url) {
		var operatorId = tableObj.getSelectedObjects()[0].common.operatorIdentifier;
		var filter = "?submittedFilterCriterion={'viewId':'-1022','columnId':'-12101','operandId':'-4','value1':'" + operatorId + "','value2':'','critNum':1,'locked':false}";
		window.document.location.href = url + filter + "&filterByFunction=false";;
	}
	
  	var getSignOffTimeUrl = "${pageContext}/operator/asyncGetOperSignOffTime.action";
  	var updateSignOffTime = "${pageContext}/operator/asyncUpdateOperSignOffTime.action";
    var dateValue;

  	function getOperatorSignOffTime() {
	  	var id = operatorObj.getSelectedIds();
  		var deferredGetTime = loadJSONDoc(getSignOffTimeUrl, {'operatorId': 
  				 id});
		deferredGetTime.addCallbacks(showSignOffDlg, updateError);  
  	}
  	
  	function signOff() {
  	    var signOffTime;
	    var ids = operatorObj.getSelectedIds();
  	    if ($('useCurrent').checked) {
  		    signOffTime = 'current';
	  	} else {
            signOffTime = dateValue.toString();			
	    }
	    performActionWithParams(operatorObj, updateSignOffTime, {'signOffDate': signOffTime, 'ids':ids});
  	}

  	function showSignOffDlg(objects) {
  		if (objects.errorCode != 0) {
			showOKDialog("<@s.text name='epp.error'/>", objects.errorMessage);	
  		} else {
  		    dateValue = objects.dateValue;
	
	        showSignOffDialog("<@s.text name='operator.signOff.msgText'/>" 
  		                     + objects.dateValue 
  		                     + "<br><br><table cellpadding='5' style='font-size:14;font-weight:normal;'><tr><td><input type='radio' style='border:0;' name='useTime' value='0'  id='useCurrent' checked='true'/></td><td><label for='useCurrent'><@s.text name='operator.signOff.useCurrent'/></label></td></tr>"
  		                     + "<tr><td><input type='radio' style='border:0;' name='useTime' value='1' id='useEarly'  /></td><td> <label for='useEarliest'><@s.text name='operator.signOff.useEarliest'/></label></td></tr></table>");
  		}
  	}
  	
  	// custom action enabler
  	function enableOnSignedOnOnly(tableComponent) {
  	    var objects = tableComponent.getSelectedObjects();
  	    for(i in objects) {
  	        if (objects[i].status != "<@s.text name="com.vocollect.voicelink.core.model.OperatorStatus.1" />") {
  	            return false;
  	        }
  	    }
  	    return true;
  	}
  	
  		
	 function showDeleteOperatorDialog() {
		var dialogProperties = {
			    title: "<@s.text name='operator.delete.prompt.title'/>",			    
				body: "<@s.text name='operator.delete.prompt.body'/>" ,
			    button1: "<@s.text name='operator.delete.prompt.yesText'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, operatorObj,deleteOperatorURL), "");
	}	

	 function showDeleteCurrentOperatorDialog() {
		var dialogProperties = {
			    title: "<@s.text name='operator.delete.prompt.title'/>",			    
				body: "<@s.text name='operator.delete.prompt.body'/>" ,
			    button1: "<@s.text name='operator.delete.prompt.yesText'/>",
				button2: "<@s.text name='operator.delete.prompt.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	
	 function showSignOffOperatorDialog() {
		var dialogProperties = {
			    title: "<@s.text name='operator.signOff.prompt.title'/>",			    
				body: "<@s.text name='operator.signOff.prompt.body'/>" ,
			    button1: "<@s.text name='operator.signOff.prompt.yesText'/>",
				button2: "<@s.text name='operator.signOff.prompt.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, operatorObj, signOffOperatorActionURL), "");
	}	
	
	 function showAssignOperatorToWorkgroupDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='operator.assign.prompt.title'/>",			    
				body: dialogBody ,
			    button1: "<@s.text name='operator.assign.prompt.yesText'/>",
				button2: "<@s.text name='operator.assign.prompt.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performAssignmentFromCustomDialog, operatorObj, assignWorkgroupURL), "");
	}
	
	function showSignOffDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='operator.signOff.prompt.title'/>",			    
				body: dialogBody ,
			    button1: "<@s.text name='operator.signOff.prompt.modifyDate'/>",
				button2: "<@s.text name='operator.signOff.prompt.cancel'/>",
				useDialog: true}
		buildCustomDialog(dialogProperties,
			signOff, "");
	
	}
	
	 function redirectCurrentDelete(){
      <#if operatorId?has_content>
     	window.location="${pageContext}/operator/deleteCurrentOperator.action?operatorId=${operatorId?c}";
      </#if>
    }
  	 

</script>

<ul class="NavList">
	<#if navigation.pageName == "list" || navigation.pageName == "default">
		<@action 
			name="nav.listpage.operator.view" 
			tableId="listOperatorsTable"
			id="a_viewOperator" 
			url="javascript:gotoUrlWithSelected(operatorObj, viewOperatorURL, \\'operatorId\\');" 
			tabIndex="251" 
			actionEnablers={"enableOnOne":"operator.view.disabled.selection"} 
			featureName="feature.voicelink.operator.view" />
		<@action 
			name="nav.listpage.operator.create" 
			tableId="listOperatorsTable"
			id="a_createOperator" 
			url="/${navigation.applicationMenu}/operator/create!input.action" 
			tabIndex="252" 
			actionEnablers={} 
			featureName="feature.voicelink.operator.create" />
		<@action 
			name="nav.listpage.operator.edit" 
			tableId="listOperatorsTable"
			id="a_editOperator"  
			url="javascript:gotoUrlWithSelected(operatorObj, editOperatorURL, \\'operatorId\\');" 
			tabIndex="253" 
			actionEnablers={"enableOnOne":"operator.edit.disabled.selection"} 
			featureName="feature.voicelink.operator.edit" />
		<@action 
			name="nav.listpage.operator.delete" 
			tableId="listOperatorsTable"
			id="a_deleteOperator"  
			url="javascript: showDeleteOperatorDialog();" 
			tabIndex="254" 
			actionEnablers={"enableOnMoreThanNone":"operator.delete.disabled.selection"} 
			featureName="feature.voicelink.operator.delete" />
		<#-- this id is temporary until customdialogdropdown is generic'd -->
		<@action
			name="nav.listpage.operator.workgroup"
			tableId="listOperatorsTable"
			id="a_assignOperator"
			url="javascript:getAssignOperatorDataOnSelected(operatorObj,workgroupDialogURL);"
			tabIndex="255"
			actionEnablers={"enableOnMoreThanNone":"operator.assign.disabled.selection"}
			featureName="feature.voicelink.operator.assign" />	
		<@action 
			name="nav.listpage.operator.signOffOperatorAction" 
			tableId="listOperatorsTable"
			id="a_signOffOperator"  
			url="javascript: getOperatorSignOffTime();" 
			tabIndex="256" 
			actionEnablers={"enableOnOne":"operator.signOffAction.disabled.selection",
			                "enableOnSignedOnOnly":"operator.signOffAction.disabled.signedOnOnly"} 
			featureName="feature.voicelink.operator.signOffOperators" />
	
		<@action 
			name="nav.listpage.operator.labor.history.view" 
			tableId="listOperatorsTable"
			id="a_viewOperatorLaborHistory" 
			url="javascript:gotoUrlWithOperator(operatorObj, viewLaborHistoryURL);" 
			tabIndex="257" 
			actionEnablers={"enableOnOne":"operator.view.disabled.selection"} 
			featureName="feature.voicelink.labor.view" />
	<#-- <#elseif navigation.pageName == "create">
		<@action 
			name="nav.createpage.operator.create" 
			tableId="listOperatorsTable"
			id="a_createOperator" 
			url="/${navigation.applicationMenu}/operator/create!input.action" 
			tabIndex="251" 
			actionEnablers={} 
			featureName="feature.voicelink.operator.create" /> -->
	<#elseif navigation.pageName == "edit">
		<@action 
			name="nav.editpage.operator.create" 
			tableId="listOperatorsTable"
			id="a_createOperator" 
			url="/${navigation.applicationMenu}/operator/create!input.action" 
			tabIndex="251" 
			actionEnablers={} 
			featureName="feature.voicelink.operator.create" />
		<@action
			name="nav.editpage.operator.delete"
			tableId="listOperatorsTable"
			id="a_deleteOperator"
			url="javascript: showDeleteCurrentOperatorDialog();"
			tabIndex="252"
			actionEnablers={}
			featureName="feature.voicelink.operator.delete" />
		<#include "/include/common/customdialog.ftl">
		
	<#elseif navigation.pageName == "view">		
		<@action 
			name="nav.viewpage.operator.create" 
			tableId="listOperatorsTable"
			id="a_createOperator" 
			url="/${navigation.applicationMenu}/operator/create!input.action" 
			tabIndex="251" 
			actionEnablers={} 
			featureName="feature.voicelink.operator.create" />
		<@action
			name="nav.viewpage.operator.edit"
			tableId="listOperatorsTable"
			id="a_editOperator"
			url="/${navigation.applicationMenu}/operator/edit!input.action?operatorId=${operatorId?c}"
			tabIndex="252"
			actionEnablers={}
			featureName="feature.voicelink.operator.edit" />
		<@action
			name="nav.viewpage.operator.delete"
			tableId="listOperatorsTable"
			id="a_deleteOperator"
			url="javascript: showDeleteCurrentOperatorDialog();"
			tabIndex="253"
			actionEnablers={}
			featureName="feature.voicelink.operator.delete" />
		
	</#if>		
</ul>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "create" || navigation.pageName == "edit" || navigation.pageName == "view">
   <#global helpFiles="Core/OperatorFields.htm"/>
   contextHelpUrl = 52;
<#else>  
   <#global helpFiles="PlaceholderPages/OperatorsPagePlaceholder.htm"/>
   contextHelpUrl = 17;
</#if>
</script>