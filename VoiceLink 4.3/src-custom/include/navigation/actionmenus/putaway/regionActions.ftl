<#include "/include/common/action.ftl" />
<#if navigation.pageName != "create">
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.region.actions'/></div>
<script type="text/javascript">
var deleteRegionURL = '${base}/putaway/region/delete.action';

  function showDeleteRegionDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.regions'/>" ,
			    button1: "<@s.text name='delete.yesText.regions'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, regionObj,deleteRegionURL), "");
	}	
	
  function showDeleteCurrentRegionDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.region'/>" ,
			    button1: "<@s.text name='delete.yesText.region'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	
   function redirectCurrentDelete(){
      <#if regionId?has_content>
     	window.location="${base}/putaway/region/deleteCurrentRegion.action?regionId=${regionId?c}";
      </#if>
    }
	
</script>
<ul id="NavList">
    <#if navigation.pageName == "list" || navigation.pageName == "default">
            <@action
                name="nav.putaway.region.view"
			    tableId="listRegionTable"
	            id="a_viewRegion"
	            url="javascript:gotoUrlWithSelected(regionObj, \\'${base}/putaway/region/view.action\\', \\'regionId\\');"
	            tabIndex="251"
                actionEnablers={"enableOnOne":"region.view.disabled.selection"} 
	            featureName="feature.voicelink.putaway.region.view" />
		    <@action
		        name="nav.putaway.region.create" 
		        tableId="listRegionTable"
	            id="a_createRegion" 
	            url="/putaway/region/create!input.action" 
	            tabIndex="251" 
	            actionEnablers={}
	            featureName="feature.voicelink.putaway.region.create" />	    
			<@action
                name="nav.putaway.region.edit"
                tableId="listRegionTable"
		        id="a_editRegion"
                tabIndex="253"
                url="javascript:performActionOnSelected(regionObj, \\'${base}/putaway/region/checkIfEditable.action\\');"                
                actionEnablers={"enableOnOne":"region.edit.disabled.selection"}
                featureName="feature.voicelink.putaway.region.edit" />
			<@action			
			    name="nav.putaway.region.delete"
			    tableId="listRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteRegionDialog();"
		        tabIndex="252"
	            actionEnablers={"enableOnMoreThanNone":"region.delete.disabled.selection"}
		        featureName="feature.voicelink.putaway.region.delete" />
          
    <#elseif navigation.pageName == "view">
		    <@action
		        name="nav.putaway.region.create" 
		        tableId="listRegionTable"
	            id="a_createRegion" 
	            url="/putaway/region/create!input.action" 
	            tabIndex="251" 
	            actionEnablers={}
	            featureName="feature.voicelink.putaway.region.create" />	
			<@action
                name="nav.putaway.region.edit.current"
                tableId="listRegionTable"
		        id="a_editRegion"
                tabIndex="253"
                url="javascript:performAction(${regionId?c}, \'${base}/putaway/region/checkIfEditable.action\');"
                actionEnablers={}
                featureName="feature.voicelink.putaway.region.edit" />                   
			<@action			
			    name="nav.putaway.region.delete.current"
			    tableId="listRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteCurrentRegionDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.voicelink.putaway.region.delete" />
		   
	<#elseif navigation.pageName == "edit">
		    <@action
		        name="nav.putaway.region.create" 
		        tableId="listRegionTable"
	            id="a_createRegion" 
	            url="/putaway/region/create!input.action" 
	            tabIndex="251" 
	            actionEnablers={}
	            featureName="feature.voicelink.putaway.region.create" />	         
			<@action			
			    name="nav.putaway.region.delete.current"
			    tableId="listRegionTable"
		        id="a_deleteRegion"
		        url="javascript: showDeleteCurrentRegionDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.voicelink.putaway.region.delete" />
		   
    </#if>
</ul>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "list">
   <#global helpFiles="PlaceholderPages/RegionsPagePlaceholder.htm"/>
   contextHelpUrl = 21;
<#--   <#global helpFiles="Core/Operators/Assign.htm"/>
   contextHelpUrl = 59;
<#elseif navigation.pageName =="create" || navigation.pageName == "create!input">
   <#global helpFiles="Selection/Region_Create.htm"/>
   contextHelpUrl = 60;
<#elseif navigation.pageName =="edit" || navigation.pageName == "edit!input">
   <#global helpFiles="Selection/Region_Edit.htm"/>
   contextHelpUrl = 60;
<#elseif navigation.pageName == "viewRegion!input" || navigation.pageName == "viewRegion">
    <#global helpFiles="Selection/Region_View.htm"/>
    contextHelpUrl = 62;
<#elseif navigation.pageName == "create!duplicate">
    <#global helpFiles="Selection/Region_Duplicate.htm"/>
    contextHelpUrl = 61; -->//do nothing; these numbers are incorrect
<#else>  
   <#global helpFiles="Content/PutAway/PARegions.htm"/>
   contextHelpUrl = 72;
</#if>
</script>
