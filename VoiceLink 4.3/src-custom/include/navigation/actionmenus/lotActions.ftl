<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript">
	var deleteLotURL = '${pageContext}/lot/delete.action';

	 function showDeleteLotDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.lots'/>" ,
			    button1: "<@s.text name='delete.yesText.lots'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, lotObj,deleteLotURL), "");
	}	


    function showDeleteCurrentLotDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.lot'/>" ,
			    button1: "<@s.text name='delete.yesText.lot'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	 function redirectCurrentDelete(){
      <#if lotId?has_content>
     	window.location="${pageContext}/lot/deleteCurrentLot.action?lotId=${lotId?c}";
      </#if>
    }
		
</script>
<#if navigation.pageName != "create">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.lot.actions'/></div>
	<ul id="NavList">
        <#if navigation.pageName == "default">
            <#assign actionName = "nav.lot.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.lot.">
        </#if>
		<#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
			    name=actionName + "view"
			    tableId="listLotsTable"
	            id="a_viewLot"
	            url="javascript:gotoUrlWithSelected(lotObj, \\'${pageContext}/lot/view.action\\', \\'lotId\\');"
	            tabIndex="251"
                actionEnablers={"enableOnOne":"lot.view.disabled.selection"} 
	            featureName="feature.voicelink.lot.view" />
	    </#if>
        <#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action			
			    name=actionName + "delete"
			    tableId="listLotsTable"
		        id="a_deleteLot"
		        url="javascript: showDeleteLotDialog();"
		        tabIndex="252"
	            actionEnablers={"enableOnMoreThanNone":"lot.delete.disabled.selection"}
		        featureName="feature.voicelink.lot.delete" />
		</#if>
        <#if navigation.pageName == "view" || navigation.pageName == "create" || navigation.pageName == "edit"  >		   
			<@action			
			    name=actionName + "delete"
			    tableId="listLotsTable"
		        id="a_deleteLot"
		        url="javascript: showDeleteCurrentLotDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.voicelink.lot.delete" />
        </#if>          
	</ul>
</#if>
<script type="text/javascript">
  <#global helpFiles="PlaceholderPages/LotsPagePlaceholder.htm"/>
   contextHelpUrl = 100;
</script>