<#include "/include/common/action.ftl" />
<#include "/include/common/customdialog.ftl">
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript">
	var deleteItemURL = '${pageContext}/item/delete.action';

	 function showDeleteItemDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.items'/>" ,
			    button1: "<@s.text name='delete.yesText.items'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, itemObj,deleteItemURL), "");
	}	


    function showDeleteCurrentItemDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.item'/>" ,
			    button1: "<@s.text name='delete.yesText.item'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	 function redirectCurrentDelete(){
      <#if itemId?has_content>
     	window.location="${pageContext}/item/deleteCurrentItem.action?itemId=${itemId?c}";
      </#if>
    }
    
    	
</script>
<#if navigation.pageName != "create">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.item.actions' /></div>
	<ul class="NavList">
		 <#if navigation.pageName == "list" || navigation.pageName == "default">
		    <@action
				name="nav.item.view"
				tableId="listItemsTable"
				id="a_viewItem"		
				url="javascript:gotoUrlWithSelected(itemObj, \\'${pageContext}/item/view.action\\', \\'itemId\\');"
				tabIndex="251"
				actionEnablers={"enableOnOne":"item.view.disabled.selection"}
				featureName="feature.voicelink.item.view" />
			<@action
				name="nav.item.create"
				tableId="listItemsTable"
				id="a_createItem"		
				url="/${navigation.applicationMenu}/item/create!input.action"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.item.create" />
			<@action
				name="nav.item.edit"
				tableId="listItemsTable"
				id="a_editItem"
				url="javascript:gotoUrlWithSelected(itemObj, \\'${pageContext}/item/edit!input.action\\', \\'itemId\\');"
				tabIndex="253"
				actionEnablers={"enableOnOne":"item.edit.disabled.selection"}
				featureName="feature.voicelink.item.edit" /> 
			<@action
				name="nav.item.delete"
				tableId="listItemsTable"
				id="a_deleteItem"
				url="javascript:showDeleteItemDialog();"
				tabIndex="254"
				actionEnablers={"enableOnMoreThanNone":"item.delete.disabled.selection"}
				featureName="feature.voicelink.item.delete" />			
			
		<#elseif navigation.pageName == "view">
			<@action
				name="nav.item.create"
				tableId="listItemsTable"
				id="a_createItem"		
				url="/${navigation.applicationMenu}/item/create!input.action"
				tabIndex="251"
				actionEnablers={}
				featureName="feature.voicelink.item.create" />
			<@action
				name="nav.item.viewpage.edit"
				tableId="listItemsTable"
				id="a_editItem"
				url="/${navigation.applicationMenu}/item/edit!input.action?itemId=${itemId?c}"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.item.edit" />
			<@action
				name="nav.item.viewpage.delete"
				tableId=""
				id="a_deleteItem"
				url="javascript:showDeleteCurrentItemDialog();"
				tabIndex="253"
				actionEnablers={}
				featureName="feature.voicelink.item.delete" />
		
		<#elseif navigation.pageName == "edit">
			<@action 
				name="nav.item.create" 
				tableId="listItemsTable"
				id="a_createItem" 
				url="/${navigation.applicationMenu}/item/create!input.action" 
				tabIndex="251" 
				actionEnablers={} 
				featureName="feature.voicelink.item.create" />
			<@action
				name="nav.item.viewpage.delete"
				tableId="listItemsTable"
				id="a_deleteItem"
				url="javascript:showDeleteCurrentItemDialog();"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.item.delete" />
		
		</#if>
	</ul>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "create" || navigation.pageName == "edit" || navigation.pageName == "view">
   <#global helpFiles="Core/ItemFields.htm"/>
   contextHelpUrl = 50;
<#else>  
   <#global helpFiles="PlaceholderPages/ItemsPagePlaceholder.htm"/>
   contextHelpUrl = 8;
</#if>
</script>
