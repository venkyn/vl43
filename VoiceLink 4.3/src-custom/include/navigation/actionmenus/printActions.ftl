<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript">


var deletePrintURL = '${pageContext}/print/delete.action';
 

  function showDeletePrintDialog() {
 		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.printers'/>" ,
			    button1: "<@s.text name='delete.yesText.printers'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, printerObj,deletePrintURL), "");
	}	
	
</script>
	
<#if navigation.pageName != "create">
	<div class="spacer"></div>	
	<div class="menuHeader"><@s.text name='nav.printer.actions' /></div>
	<ul class="NavList">
		<#if navigation.pageName == "list" || navigation.pageName == "default">
		   	<@action
				name="nav.printer.view"
				tableId="printerTable"
				id="a_viewPrinter"
				url="javascript:gotoUrlWithSelected(printerObj, \\'${pageContext}/print/view.action\\', \\'printerId\\');"
				tabIndex="253"
				actionEnablers={"enableOnOne":"printer.view.disabled.selection"}
				featureName="feature.voicelink.printer.view" /> 
		   	<@action
				name="nav.printer.edit"
				tableId="printerTable"
				id="a_editPrinter"
				url="javascript:gotoUrlWithSelected(printerObj, \\'${pageContext}/print/edit!input.action\\', \\'printerId\\');"
				tabIndex="254"
				actionEnablers={"enableOnOne":"printer.edit.disabled.selection"}
				featureName="feature.voicelink.printer.edit" /> 			
			<@action
				name="nav.printer.delete"
				tableId="printerTable"
				id="a_deletePrinter"
				url="javascript:showDeletePrintDialog();"
				tabIndex="255"
				actionEnablers={"enableOnMoreThanNone":"printer.delete.disabled.selection"}
				featureName="feature.voicelink.printer.delete" />
			</#if>
			<#if navigation.pageName == "view">
			<@action
				name="nav.printer.edit.current"
				tableId="printerTable"
				id="a_editThisPrinter"
				url="/${navigation.applicationMenu}/print/edit!input.action?printerId=${printerId?c}"
				tabIndex="252"
				actionEnablers={}
				featureName="feature.voicelink.printer.edit" />
			</#if>
			<@action
				name="nav.printer.create"
				tableId="printerTable"
				id="a_createPrinter"
				url="/${navigation.applicationMenu}/print/create!input.action"
				tabIndex="255"
				actionEnablers={}
				featureName="feature.voicelink.printer.create" /> <#-- javascript:YAHOO.simpledialog.div_createPrinter_confirm.show(); -->
	</ul>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "edit" || navigation.pageName == "view">
   <#global helpFiles="Selection/PrintersFields.htm"/>
   contextHelpUrl = 57;
<#elseif navigation.pageName == "create">
   <#global helpFiles="Selection/Printer_Add.htm"/>
   contextHelpUrl = 57;
<#else>  
   <#global helpFiles="PlaceholderPages/PrintersPagePlaceholder.htm"/>
   contextHelpUrl = 19;
</#if>
</script>