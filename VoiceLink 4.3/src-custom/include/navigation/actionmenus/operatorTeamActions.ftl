<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript">
	var deleteOperatorTeamURL = '${pageContext}/operatorTeam/delete.action';

	 function showDeleteOperatorTeamDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.operatorTeams'/>" ,
			    button1: "<@s.text name='delete.yesText.operatorTeams'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, operatorTeamObj,deleteOperatorTeamURL), "");
	}	


    function showDeleteCurrentOperatorTeamDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.operatorTeam'/>" ,
			    button1: "<@s.text name='delete.yesText.operatorTeam'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	 function redirectCurrentDelete(){
      <#if operatorTeamId?has_content>
     	window.location="${pageContext}/operatorTeam/deleteCurrentOperatorTeam.action?operatorTeamId=${operatorTeamId?c}";
      </#if>
    }
		
</script>
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.report.actions'/></div>
<ul class="NavList">
	<@action
        name="nav.selection.report.manage"
        tableId="listOperatorTeamsTable"
		id="a_manageReports"
        tabIndex="250"
        url="/selection/report/list.action"			
        actionEnablers={}
        featureName="feature.voicelink.report.view" />     
    </ul>     
<#if navigation.pageName != "create">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.operatorTeam.actions'/></div>
	<ul id="NavList">
        <#if navigation.pageName == "default">
            <#assign actionName = "nav.operatorTeam.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.operatorTeam.">
        </#if>
		<#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
			    name=actionName + "view"
			    tableId="listOperatorTeamsTable"
	            id="a_viewOperatorTeam"
	            url="javascript:gotoUrlWithSelected(operatorTeamObj, \\'${pageContext}/operatorTeam/view.action\\', \\'operatorTeamId\\');"
	            tabIndex="251"
                actionEnablers={"enableOnOne":"operatorTeam.view.disabled.selection"} 
	            featureName="feature.voicelink.operatorTeam.view" />
	    </#if>
	    <#if navigation.pageName != "create">
		    <@action
		         name=actionName + "create" 
		         tableId="listOperatorTeamsTable"
	             id="a_createOperatorTeam" 
	             url="/${navigation.applicationMenu}/operatorTeam/create!input.action" 
	             tabIndex="251" 
	             actionEnablers={} 
	             featureName="feature.voicelink.operatorTeam.create" />	    
	    </#if>
   	    <#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
                name=actionName + "edit"
                tableId="listOperatorTeamsTable"
		        id="a_editOperatorTeam"
                tabIndex="253"
                url="javascript:gotoUrlWithSelected(operatorTeamObj,\\'${pageContext}/operatorTeam/edit!input.action\\',\\'operatorTeamId\\');"
                actionEnablers={"enableOnOne":"operatorTeam.edit.disabled.selection"}
                featureName="feature.voicelink.operatorTeam.edit" />         
        </#if>
        <#if navigation.pageName == "view">
			<@action
                name=actionName + "edit"
                tableId="listOperatorTeamsTable"
		        id="a_editOperatorTeam"
                tabIndex="253"
                url="/${navigation.applicationMenu}/operatorTeam/edit!input.action?operatorTeamId=${operatorTeamId?c}"			
                actionEnablers={}
                featureName="feature.voicelink.operatorTeam.edit" />                   
        </#if>
        <#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action			
			    name=actionName + "delete"
			    tableId="listOperatorTeamsTable"
		        id="a_deleteOperatorTeam"
		        url="javascript: showDeleteOperatorTeamDialog();"
		        tabIndex="252"
	            actionEnablers={"enableOnMoreThanNone":"operatorTeam.delete.disabled.selection"}
		        featureName="feature.voicelink.operatorTeam.delete" />
		</#if>
        <#if navigation.pageName == "view" || navigation.pageName == "create" || navigation.pageName == "edit"  >		   
			<@action			
			    name=actionName + "delete"
			    tableId="listOperatorTeamsTable"
		        id="a_deleteOperatorTeam"
		        url="javascript: showDeleteCurrentOperatorTeamDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.voicelink.operatorTeam.delete" />
        </#if>          
	</ul>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "create" || navigation.pageName == "edit" || navigation.pageName == "view">
   <#global helpFiles="Core/OperatorTeamFields.htm"/>
   contextHelpUrl = 132;
<#else>  
   <#global helpFiles="PlaceholderPages/OperatorTeamPagePlaceholder.htm"/>
   contextHelpUrl = 130;
</#if>
</script>