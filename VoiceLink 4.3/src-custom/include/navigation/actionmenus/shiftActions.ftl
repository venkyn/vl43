<#include "/include/common/action.ftl" /> 
<#include "/include/common/customdialog.ftl">
<#assign pageContext = "${base}/${navigation.applicationMenu}">

<script type="text/javascript">
var deleteShiftURL = '${pageContext}/shift/delete.action';

function showDeleteShiftDialog() {
	var dialogProperties = {
		    title: "<@s.text name='delete.title'/>",			    
			body: "<@s.text name='shift.delete.prompt.body'/>" ,
		    button1: "<@s.text name='shift.delete.prompt.yesText'/>",
			button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, shiftObj,deleteShiftURL), "");
	}	
	
  function showDeleteCurrentShiftDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
			body: "<@s.text name='shift.delete.prompt.body'/>" ,
		    button1: "<@s.text name='shift.delete.prompt.yesText'/>",
			button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentShiftDelete,"",""), "");
	}	
	
   function redirectCurrentShiftDelete(){
      <#if shiftId?has_content>
 	window.location="${pageContext}/shift/deleteCurrentShift.action?shiftId=${shiftId?c}";
  </#if>
}
</script>

<#if navigation.pageName != "create">
    <div class="spacer"></div>
    <div class="menuHeader"><@s.text name='nav.shift.actions'/></div>
    <ul id="NavList">
    <#if navigation.pageName == "default">
            <#assign actionName = "nav.shift.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.shift.">
    </#if>
    <#if navigation.pageName == "list" || navigation.pageName == "default">
            <@action          
                name="nav.shift.view"
                tableId=""
                id="a_viewShift"
                url="javascript:gotoUrlWithSelected(shiftObj, \\'${pageContext}/shift/view.action\\', \\'shiftId\\');"
                tabIndex="250"
                actionEnablers={"enableOnOne":"shift.view.disabled.selection"}
                featureName="feature.voicelink.shift.view" />
            <@action          
                name="nav.shift.create"
                tableId=""
                id="a_createShift"
                url="/${navigation.applicationMenu}/shift/create!input.action"
                tabIndex="251"
                actionEnablers={}
                featureName="feature.voicelink.shift.create" />
            <@action          
                name="nav.shift.edit"
                tableId=""
                id="a_editShift"
                url="javascript:gotoUrlWithSelected(shiftObj, \\'${pageContext}/shift/edit!input.action\\', \\'shiftId\\');"
                tabIndex="252"
                actionEnablers={"enableOnOne":"shift.edit.disabled.selection"}
                featureName="feature.voicelink.shift.edit" />    
            <@action            
                name="nav.shift.delete"
                tableId=""
                id="a_deleteShift"
                url="javascript: showDeleteShiftDialog();"
                tabIndex="253"
                actionEnablers={"enableOnMoreThanNone":"shift.delete.disabled.selection"}
                featureName="feature.voicelink.shift.delete" />
     <#elseif navigation.pageName == "view">
            <@action 
                name="nav.shift.create" 
                tableId=""
                id="a_createShift" 
                url="/${navigation.applicationMenu}/shift/create!input.action"
                tabIndex="251" 
                actionEnablers={}
                featureName="feature.voicelink.shift.create" />
            <@action 
                name="nav.shift.viewpage.edit" 
                tableId=""
                id="a_editShift" 
                url="/${navigation.applicationMenu}/shift/edit!input.action?shiftId=${shiftId?c}"
                tabIndex="252" 
                actionEnablers={}
                featureName="feature.voicelink.shift.edit" />
            <@action            
                name="nav.shift.delete.current"
                tableId=""
                id="a_deleteShift"
                url="javascript: showDeleteCurrentShiftDialog();"
                tabIndex="253"
                actionEnablers={}
                featureName="feature.voicelink.shift.delete" />
        <#elseif navigation.pageName == "edit">
            <@action 
                name="nav.shift.create" 
                tableId=""
                id="a_createShift" 
                url="/${navigation.applicationMenu}/shift/create!input.action"
                tabIndex="251" 
                actionEnablers={}
                featureName="feature.voicelink.shift.create" />
            <@action            
                name="nav.shift.delete.current"
                tableId=""
                id="a_deleteShift"
                url="javascript: showDeleteCurrentShiftDialog();"
                tabIndex="252"
                actionEnablers={}
                featureName="feature.voicelink.shift.delete" />
        </#if>
    </ul>
</#if>

<script type="text/javascript">
    <#if navigation.pageName == "create" || navigation.pageName == "edit">
        contextHelpUrl = 3510;
    <#elseif navigation.pageName == "view" || navigation.pageName == "list">
        contextHelpUrl = 3500;
    <#else>  
       contextHelpUrl = 150;
    </#if>
</script>
