<#include "/include/common/action.ftl" />

<#assign pageContext = "${base}/${navigation.applicationMenu}">

<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.assignment.resequence.actions'/></div>
<script type="text/javascript">
 		var moveURL = '${pageContext}/assignment/resequenceMove.action';
 		var applyURL = '${pageContext}/assignment/resequenceApply.action'; 		
 		var cancelURL = '${pageContext}/assignment/checkForOutstandingChanges.action';
 		var assignmentURL = '${pageContext}/assignment/list.action';
 		var moveUp = '1';
 		var moveDown = '2';
 		var moveTop = '3';
 		var moveBottom = '4';
 		var cancel = '5';
</script>

<ul class="NavList">
	<@action
		name="assignment.resequence.button.moveTop"
		tableId="listAssignmentsTable"
		id="a_resequenceTop"
		url="javascript:performActionOnResequence(assignmentObj,moveURL,moveTop);"
		tabIndex="253"
		actionEnablers={"enableOnMoreThanNone":"assignment.resequence.move.disabled.selection"}
		featureName="feature.voicelink.assignment.resequence" />

	<@action
		name="assignment.resequence.button.moveUp"
		tableId="listAssignmentsTable"
		id="a_resequenceUp"
		url="javascript:performActionOnResequence(assignmentObj,moveURL,moveUp);"
		tabIndex="253"
		actionEnablers={"enableOnMoreThanNone":"assignment.resequence.move.disabled.selection"}
		featureName="feature.voicelink.assignment.resequence" />
		
	<@action
		name="assignment.resequence.button.moveDown"
		tableId="listAssignmentsTable"
		id="a_resequenceDown"
		url="javascript:performActionOnResequence(assignmentObj,moveURL,moveDown);"
		tabIndex="253"
		actionEnablers={"enableOnMoreThanNone":"assignment.resequence.move.disabled.selection"}
		featureName="feature.voicelink.assignment.resequence" />
		
	<@action
		name="assignment.resequence.button.moveBottom"
		tableId="listAssignmentsTable"
		id="a_resequenceBottom"
		url="javascript:performActionOnResequence(assignmentObj,moveURL,moveBottom);"
		tabIndex="253"
		actionEnablers={"enableOnMoreThanNone":"assignment.resequence.move.disabled.selection"}
		featureName="feature.voicelink.assignment.resequence" />
		
	<@action
		name="assignment.resequence.button.apply"
		tableId="listAssignmentsTable"
		id="a_resequenceApply"
		url="javascript:performActionOnResequence(assignmentObj,applyURL,moveBottom);"
		tabIndex="253"
		actionEnablers={}
		featureName="feature.voicelink.assignment.resequence" />
		
	<@action
		name="assignment.resequence.button.cancel"
		tableId="listAssignmentsTable"
		id="a_resequenceApply"
		url="javascript:performActionOnResequence(assignmentObj,cancelURL,cancel);"
		tabIndex="253"
		actionEnablers={}
		featureName="feature.voicelink.assignment.resequence" />

<script type="text/javascript">
	function cancelRedirect() {
		window.location = assignmentURL;
	}

	function showCancelResequenceDialog() {
		var dialogProperties = {
			    title: "<@s.text name='nav.assignment.resequence.cancel'/>",			    
				body: "<@s.text name='assignment.resequence.body.cancelResequenceText'/>",
				button1: "<@s.text name='assignment.resequence.cancelResequence.yesText'/>",
				button2: "<@s.text name='assignment.resequence.cancelResequence.noText'/>"}
		buildCustomDialog(dialogProperties,
			cancelRedirect, "");
	}
</script>
</ul>
