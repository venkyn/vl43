<#assign page=navigation.pageName>
<#global helpFiles="PlaceholderPages/LaborPagePlaceholder.htm"/>
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript">
	contextHelpUrl = 180;
</script>
<#if page != "assignmentlist">
<#include "/include/common/action.ftl" />
<#include "/include/common/customdialogdropdown.ftl">
<div class="spacer"></div>
<div class="menuHeader"><@s.text name='nav.labor.actions'/></div>
<script type="text/javascript">
	// Custom action enablers
	function enableOnClosedLaborOnly(tableComponent) {
	    var selectedObjects = tableComponent.getSelectedObjects();
        for (var i=0; i < selectedObjects.length; i++) {
		    if (selectedObjects[i].endTime.length <= 0) {
			    return false;
            }
    	}
		return true;
	}
	function enableOnSelectionLaborOnly(tableComponent) {
        // we have only one object because we are preceeded by
        // the 'enableOnOne' enabler.
        var selectedObject = tableComponent.getSelectedObjects()[0];
	    if (selectedObject.actionType == "<@s.text name="OperatorLaborActionType.Selection.name"/>") {
	        return true;
	    }
	    return false;
	}
	function enableOnPutToStoreLaborOnly(tableComponent) {
        // we have only one object because we are preceeded by
        // the 'enableOnOne' enabler.
        var selectedObject = tableComponent.getSelectedObjects()[0];
	    if (selectedObject.actionType == "<@s.text name="OperatorLaborActionType.PutToStore.name"/>") {
	        return true;
	    }
	    return false;
	}
	function enableOnLoadingLaborOnly(tableComponent) {
        // we have only one object because we are preceeded by
        // the 'enableOnOne' enabler.
        var selectedObject = tableComponent.getSelectedObjects()[0];
	    if (selectedObject.actionType == "<@s.text name="OperatorLaborActionType.Loading.name"/>") {
	        return true;
	    }
	    return false;
	}
	
	function enableOnCycleCountingLaborOnly(tableComponent) {
        // we have only one object because we are preceeded by
        // the 'enableOnOne' enabler.
        var selectedObject = tableComponent.getSelectedObjects()[0];
        if (selectedObject.actionType == "<@s.text name="OperatorLaborActionType.CycleCounting.name"/>") {
            return true;
        }
        return false;
    }
	function enableOnFunctionLaborOnly(tableComponent) {
	    var selectedObject = tableComponent.getSelectedObjects()[0];
	    if (selectedObject.filterType != "<@s.text name="com.vocollect.voicelink.core.model.OperatorLaborFilterType.0"/>") {
	        return true;
	    }
	    return false;
	}

 	var modifyEndTimeUrl = '${pageContext}/labor/modifyEndTime.action';

  	function getEndTimeForOperatorLabor(tableObj, url) {
		var params = "?ids=" + tableObj.getSelectedIds().join("&ids=");	
  		var deferredGetTime = loadJSONDoc(url+params, {});
		deferredGetTime.addCallbacks(showEndTimeDlg, updateError);
  	}
  	
  	var endTime;
  	function signOff() {
  	    var ids = laborHistoryObj.getSelectedIds();
  		performActionWithParams(laborHistoryObj, modifyEndTimeUrl, {'endTime': endTime.toString(), 'ids': ids});
  	}

  	function showEndTimeDlg(objects) {
    	if (objects.errorCode != 0) {
			showOKDialog("<@s.text name='epp.error'/>", objects.errorMessage);	
  		} else {
  		    endTime = objects.endTime;
  			
  			showSignOffLaborDialog("<@s.text name='operator.endTime.msgText'/>" + objects.endTime);
  		}
  	}
 	
	 function showSignOffLaborDialog(dialogBody) {
		var dialogProperties = {
			    title: "<@s.text name='labor.endTimeText.modify.title'/>",			    
				body: dialogBody,
			    button1: "<@s.text name='labor.endTimeText.modify.title'/>",
				button2: "<@s.text name='labor.endTimeText.cancel'/>"}
		buildCustomDialog(dialogProperties,
			partial(signOff,"",""), "");
	}	

</script>
	
<ul class="NavList">
	<#if page == "list">
	   <#if navigation.applicationMenu == "selection">
    	   <@action
              name="nav.labor.modify.endTime"
              tableId="laborHistoryTable"
              id="a_laborEndTime"
              url="javascript:getEndTimeForOperatorLabor(laborHistoryObj,\\'${pageContext}/labor/getEndTime.action\\')"
              tabIndex="251"
              actionEnablers={"enableOnOne":"labor.endTime.disabled.oneLaborOnly",
                              "enableOnFunctionLaborOnly":"labor.endTime.disabled.functionOnly",
                              "enableOnClosedLaborOnly":"labor.endTime.disabled.closedOnly"}
              featureName="feature.voicelink.labor.editEndTime" />
    	    <@action
    	      name="nav.labor.view.assignments"
    	      tableId="laborHistoryTable"
    	      id="a_laborViewAssignments"
    		  url="javascript:gotoUrlWithSelected(laborHistoryObj,\\'${base}/selection/labor/assignmentlist.action\\',\\'operatorLaborId\\');" 
    	      tabIndex="252"
    	      actionEnablers={"enableOnOne":"labor.viewAssignments.disabled.selection",
    	                      "enableOnSelectionLaborOnly":"labor.viewAssignments.disabled.selection"}
    	      featureName="feature.voicelink.labor.view" />
	   </#if>
	   <#if navigation.applicationMenu == "cyclecounting">
    	   <@action
              name="nav.labor.modify.endTime"
              tableId="laborHistoryTable"
              id="a_laborEndTime"
              url="javascript:getEndTimeForOperatorLabor(laborHistoryObj,\\'${pageContext}/labor/getEndTime.action\\')"
              tabIndex="251"
              actionEnablers={"enableOnOne":"labor.endTime.disabled.oneLaborOnly",
                              "enableOnFunctionLaborOnly":"labor.endTime.disabled.functionOnly",
                              "enableOnClosedLaborOnly":"labor.endTime.disabled.closedOnly"}
              featureName="feature.cyclecounting.labor.editEndTime" />
            <@action
              name="nav.labor.view.assignments"
              tableId="laborHistoryTable"
              id="a_laborViewAssignments"
              url="javascript:gotoUrlWithSelected(laborHistoryObj,\\'${base}/cyclecounting/labor/assignmentlist.action\\',\\'operatorLaborId\\');" 
              tabIndex="252"
              actionEnablers={"enableOnOne":"cyclecounting.view.disabled.selection",
                              "enableOnCycleCountingLaborOnly":"labor.viewAssignments.disabled.functionOnly"}
              featureName="feature.cyclecounting.labor.view" />
       </#if>
	   <#if navigation.applicationMenu == "puttostore">
	   <@action
              name="nav.labor.modify.endTime"
              tableId="laborHistoryTable"
              id="a_laborEndTime"
              url="javascript:getEndTimeForOperatorLabor(laborHistoryObj,\\'${pageContext}/labor/getEndTime.action\\')"
              tabIndex="251"
              actionEnablers={"enableOnOne":"labor.endTime.disabled.oneLaborOnly",
                              "enableOnFunctionLaborOnly":"labor.endTime.disabled.functionOnly",
                              "enableOnClosedLaborOnly":"labor.endTime.disabled.closedOnly"}
              featureName="feature.lineloading.labor.editEndTime" />
	    <@action
	      name="nav.labor.view.licenses"
	      tableId="laborHistoryTable"
	      id="a_laborViewLicenses"
		  url="javascript:gotoUrlWithSelected(laborHistoryObj,\\'${base}/puttostore/labor/licenselist.action\\',\\'operatorLaborId\\');" 
	      tabIndex="253"
	      actionEnablers={"enableOnOne":"labor.viewLicenses.disabled.selection",
	                      "enableOnPutToStoreLaborOnly":"labor.viewLicenses.disabled.selection"}
	      featureName="feature.voicelink.labor.view" />
	      
	   </#if>
       <#if navigation.applicationMenu == "loading">
    	   <@action
              name="nav.labor.modify.endTime"
              tableId="laborHistoryTable"
              id="a_laborEndTime"
              url="javascript:getEndTimeForOperatorLabor(laborHistoryObj,\\'${pageContext}/labor/getEndTime.action\\')"
              tabIndex="251"
              actionEnablers={"enableOnOne":"labor.endTime.disabled.oneLaborOnly",
                              "enableOnFunctionLaborOnly":"labor.endTime.disabled.functionOnly",
                              "enableOnClosedLaborOnly":"labor.endTime.disabled.closedOnly"}
              featureName="feature.loading.labor.editEndTime" />
            <@action
              name="nav.labor.view.loading.routes"
              tableId="laborHistoryTable"
              id="a_laborViewRoutes"
              url="javascript:gotoUrlWithSelected(laborHistoryObj,\\'${base}/loading/labor/routelist.action\\',\\'operatorLaborId\\');" 
              tabIndex="252"
              actionEnablers={"enableOnOne":"loading.view.disabled.selection",
                              "enableOnLoadingLaborOnly":"labor.viewRoutes.disabled.functionOnly"}
              featureName="feature.loading.labor.view" />
       </#if>
       <#if navigation.applicationMenu == "replenishment">
           <@action
              name="nav.labor.modify.endTime"
              tableId="laborHistoryTable"
              id="a_laborEndTime"
              url="javascript:getEndTimeForOperatorLabor(laborHistoryObj,\\'${pageContext}/labor/getEndTime.action\\')"
              tabIndex="251"
              actionEnablers={"enableOnOne":"labor.endTime.disabled.oneLaborOnly",
                              "enableOnFunctionLaborOnly":"labor.endTime.disabled.functionOnly",
                              "enableOnClosedLaborOnly":"labor.endTime.disabled.closedOnly"}
              featureName="feature.replenishment.labor.editEndTime" />
            
       </#if>
       <#if navigation.applicationMenu == "putaway">
           <@action
              name="nav.labor.modify.endTime"
              tableId="laborHistoryTable"
              id="a_laborEndTime"
              url="javascript:getEndTimeForOperatorLabor(laborHistoryObj,\\'${pageContext}/labor/getEndTime.action\\')"
              tabIndex="251"
              actionEnablers={"enableOnOne":"labor.endTime.disabled.oneLaborOnly",
                              "enableOnFunctionLaborOnly":"labor.endTime.disabled.functionOnly",
                              "enableOnClosedLaborOnly":"labor.endTime.disabled.closedOnly"}
              featureName="feature.putaway.labor.editEndTime" />
           
       </#if>
       <#if navigation.applicationMenu == "lineloading">
           <@action
              name="nav.labor.modify.endTime"
              tableId="laborHistoryTable"
              id="a_laborEndTime"
              url="javascript:getEndTimeForOperatorLabor(laborHistoryObj,\\'${pageContext}/labor/getEndTime.action\\')"
              tabIndex="251"
              actionEnablers={"enableOnOne":"labor.endTime.disabled.oneLaborOnly",
                              "enableOnFunctionLaborOnly":"labor.endTime.disabled.functionOnly",
                              "enableOnClosedLaborOnly":"labor.endTime.disabled.closedOnly"}
              featureName="feature.lineloading.labor.editEndTime" />
            
       </#if>
	   
	</#if>
<ul>
</#if>
