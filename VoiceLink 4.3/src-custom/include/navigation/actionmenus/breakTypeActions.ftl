<#include "/include/common/action.ftl" />
<#assign pageContext = "${base}/${navigation.applicationMenu}">
<script type="text/javascript">
	var deleteBreakTypeURL = '${pageContext}/breakType/delete.action';

	 function showDeleteBreakTypeDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.breakTypes'/>" ,
			    button1: "<@s.text name='delete.yesText.breakTypes'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, breakTypeObj,deleteBreakTypeURL), "");
	}	


    function showDeleteCurrentBreakTypeDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.breakType'/>" ,
			    button1: "<@s.text name='delete.yesText.breakType'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
	
	 function redirectCurrentDelete(){
      <#if breakTypeId?has_content>
     	window.location="${pageContext}/breakType/deleteCurrentBreakType.action?breakTypeId=${breakTypeId?c}";
      </#if>
    }
		
</script>
<#if navigation.pageName != "create">
	<div class="spacer"></div>
	<div class="menuHeader"><@s.text name='nav.breakType.actions'/></div>
	<ul id="NavList">
        <#if navigation.pageName == "default">
            <#assign actionName = "nav.breakType.listpage.">
        <#else>
            <#assign actionName = "nav." + navigation.pageName + "page.breakType.">
        </#if>
		<#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
			    name=actionName + "view"
			    tableId="listBreakTypesTable"
	            id="a_viewBreakType"
	            url="javascript:gotoUrlWithSelected(breakTypeObj, \\'${pageContext}/breakType/view.action\\', \\'breakTypeId\\');"
	            tabIndex="251"
                actionEnablers={"enableOnOne":"breakType.view.disabled.selection"} 
	            featureName="feature.voicelink.breakType.view" />
	    </#if>
	    <#if navigation.pageName != "create">
		    <@action
		         name=actionName + "create" 
		         tableId="listBreakTypesTable"
	             id="a_createBreakType" 
	             url="/${navigation.applicationMenu}/breakType/create!input.action" 
	             tabIndex="251" 
	             actionEnablers={} 
	             featureName="feature.voicelink.breakType.create" />	    
	    </#if>
   	    <#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action
                name=actionName + "edit"
                tableId="listBreakTypesTable"
		        id="a_editBreakType"
                tabIndex="253"
                url="javascript:gotoUrlWithSelected(breakTypeObj,\\'${pageContext}/breakType/edit!input.action\\',\\'breakTypeId\\');"
                actionEnablers={"enableOnOne":"breakType.edit.disabled.selection"}
                featureName="feature.voicelink.breakType.edit" />         
        </#if>
        <#if navigation.pageName == "view">
			<@action
                name=actionName + "edit"
                tableId="listBreakTypesTable"
		        id="a_editBreakType"
                tabIndex="253"
                url="/${navigation.applicationMenu}/breakType/edit!input.action?breakTypeId=${breakTypeId?c}"			
                actionEnablers={}
                featureName="feature.voicelink.breakType.edit" />                   
        </#if>
        <#if navigation.pageName == "list" || navigation.pageName == "default">
			<@action			
			    name=actionName + "delete"
			    tableId="listBreakTypesTable"
		        id="a_deleteBreakType"
		        url="javascript: showDeleteBreakTypeDialog();"
		        tabIndex="252"
	            actionEnablers={"enableOnMoreThanNone":"breakType.delete.disabled.selection"}
		        featureName="feature.voicelink.breakType.delete" />
		</#if>
        <#if navigation.pageName == "view" || navigation.pageName == "create" || navigation.pageName == "edit"  >		   
			<@action			
			    name=actionName + "delete"
			    tableId="listBreakTypesTable"
		        id="a_deleteBreakType"
		        url="javascript: showDeleteCurrentBreakTypeDialog();"
		        tabIndex="252"
	            actionEnablers={}
		        featureName="feature.voicelink.breakType.delete" />
        </#if>          
	</ul>
</#if>
<script type="text/javascript">
<#if navigation.pageName == "create" || navigation.pageName == "edit" || navigation.pageName == "view">
   <#global helpFiles="Core/BreakTypeFields.htm"/>
   contextHelpUrl = 49;
<#else>  
   <#global helpFiles="PlaceholderPages/BreakTypesPagePlaceholder.htm"/>
   contextHelpUrl = 3;
</#if>
</script>