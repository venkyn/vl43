<#include "/include/common/action.ftl" />
<#include "/include/common/navigationmacros.ftl" />
<div class="menuHeader"><@s.text name='nav.cyclecounting.menu'/></div>

<script type="text/javascript">
	var showAssignOperatorActionURL = '${base}/cyclecounting/operator/showAssignOperators.action';
	var regionId = 'regionId';
</script>

<ul class="NavList">
  <#if currentSiteID==-927>
      <@navigationitem
        name="nav.cyclecountingHome"
        namespace="home"
        url="/cyclecounting/home.action?currentSiteID=-927"
        tabIndex="201"
        featureName="feature.cyclecounting.home" />
  <#else>
      <@navigationitem
        name="nav.cyclecountingHome"
        namespace="home"
        url="/cyclecounting/home.action"
        tabIndex="201"
        featureName="feature.cyclecounting.home" />
  </#if>
  <@navigationitem
    name="nav.cyclecountingAssignments"
    namespace="assignment"
    url="/cyclecounting/assignment/list.action"
    tabIndex="202"
    featureName="feature.cyclecounting.assignment.view" />
  <@navigationitem
    name="nav.labor"
    namespace="labor"
    url="/cyclecounting/labor/list.action"
    tabIndex="203"
    featureName="feature.cyclecounting.labor.view" />
 <#assign dropDownNamespaces= ["breakType", "item", "itemLocationMappings", "location",  
                               "operator", "reasonCodes", "region", "report", "unitofmeasure", "workgroup"] />

  <#assign dropDownId=navigation.applicationMenu+"Select" />
  <li id="${dropDownId}Item"
  <#if dropDownNamespaces?seq_contains(navigation.actionsMenu)>
    class="highlight"
  </#if>
  >

    <div>
      <div class="navDropDownContainer">
        <select id="${dropDownId}" class="navDropDown" tabIndex="205" tid="navDropDown">
          <option value="go"><@s.text name='nav.go' /></option>
          <@navigationOption 
		    name="nav.breakTypes"
		    namespace="breakType"
		    url="/cyclecounting/breakType/list.action" 
		    featureName="feature.voicelink.breakType.view" />
		  <@navigationOption
            name="nav.items"
            namespace="item"
            url="/cyclecounting/item/list.action"
            featureName="feature.voicelink.item.view" />
          <@navigationOption 
            name="nav.itemLocationMapping"
            namespace="locItem"
            url="/cyclecounting/locItem/list.action" 
            featureName="feature.voicelink.itemLocationMapping.view" />
          <@navigationOption
            name="nav.locations"
            namespace="location"
            url="/cyclecounting/location/list.action"
            featureName="feature.voicelink.location.view" />
          <@navigationOption
            name="nav.operators"
            namespace="operator"
            url="/cyclecounting/operator/list.action"
            featureName="feature.voicelink.operator.view" />
	     <@navigationOption 
		    name="nav.reasonCodes"
		    namespace="reasonCodes"
		    url="/cyclecounting/reasonCodes/list.action" 
		    featureName="feature.voicelink.reasoncodes.view" />
		  <@navigationOption 
		    name="nav.region"
		    namespace="region"
		    url="/cyclecounting/region/list.action" 
		    featureName="feature.cyclecounting.region.view" />
		  <@navigationOption 
		    name="nav.reports"
		    namespace="report"
		    url="/cyclecounting/report/list.action" 
		    featureName="feature.voicelink.report.view" />
	      <@navigationOption 
		    name="nav.unitofmeasure"
		    namespace="unitOfMeasure"
		    url="/cyclecounting/unitOfMeasure/list.action" 
		    featureName="feature.voicelink.unitofmeasure.view" />
          <@navigationOption
            name="nav.workgroups"
            namespace="workgroup"
            url="/cyclecounting/workgroup/list.action"
            featureName="feature.voicelink.workgroup.view" />
        </select>
      </div>
      <div class="lowerEdge"></div>
      <div class="actionBubbleContainer" id="${dropDownId}_bubble">
        <div class="actionBubble actionBubbleTop"></div>
        <div class="actionBubble actionBubbleMiddle">
          <div class="actionBubbleContents" id="${dropDownId}_msg">
            <@s.text name="nav.action.message.permissions" />
          </div>
        </div>
        <div class="actionBubble actionBubbleBottom"></div>
      </div>
    </div>
  </li>
</ul>
<#assign tabIndexNumber = 253 />
<#if summaries?has_content>
		<#list summaries?keys as summaryId>
		    <#if summaries[summaryId].descriptiveText = "TabularSummary"> 
		         <#assign tableId = summaries[summaryId].tableId >
		   	     <#if tableId = "cyclecountingLaborSummary">
		   		    <div class="spacer"></div>
				    <div class="menuHeader"><@s.text name='nav.summary.labor.actions' /></div>	
				    <div id="menuBlade_listcyclecountingLaborSummaryTable">			    
				    <ul class="NavList" id="bladeList_listcyclecountingLaborSummaryTable">
				    <@action
				      name="nav.summary.labor.viewOperatorLabor"
				      tableId="list${tableId}Table"
				      id="a_viewOperatorLabor"
				      url="javascript:viewLaborData(${tableId}Obj,\\'${base}/cyclecounting/labor/list.action\\');"
				      tabIndex="${tabIndexNumber}"
				      actionEnablers={"enableOnOne":"summary.labor.disabled.cyclecounting"}
				      featureName="feature.voicelink.labor.view" />
				    </ul>		   		
				    </div>
		   		 </#if>
		         <#assign tabIndexNumber = tabIndexNumber + 1 >
		     </#if>	   	
		</#list>
</#if>
		   	
<script type="text/javascript">
    <#if navigation.actionsMenu?has_content>
        initializeDropDown("${dropDownId}", "a_${navigation.actionsMenu}");
    <#else>
        initializeDropDown("${dropDownId}", null);
    </#if>
    <#if !(navigation.pageName?exists)>
        var contextHelpUrl = 400;
    <#else>
        var contextHelpUrl = -1;
    </#if>
</script>
