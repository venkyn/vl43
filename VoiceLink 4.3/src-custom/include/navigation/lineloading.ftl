<#include "/include/common/action.ftl" />
<#include "/include/common/navigationmacros.ftl" />
<div class="menuHeader"><@s.text name='nav.lineloading.menu'/></div>
<ul class="NavList">
  <#if currentSiteID==-927>
	  <@navigationitem
	    name="nav.lineLoadingHome"
	    namespace="home"
	    url="/lineloading/home.action?currentSiteID=-927"
	    tabIndex="201"
	    featureName="feature.lineloading.home" />
  <#else>
	  <@navigationitem
	    name="nav.lineLoadingHome"
	    namespace="home"
	    url="/lineloading/home.action"
	    tabIndex="201"
	    featureName="feature.lineloading.home" />
  </#if>
  <@navigationitem
    name="nav.routeStops"
    namespace="routeStop"
    url="/lineloading/routeStop/list.action"
    tabIndex="201"
    featureName="feature.lineloading.routestop.view" />
  <@navigationitem
    name="nav.pallets"
    namespace="pallet"
    url="/lineloading/pallet/list.action"
    tabIndex="202"
    featureName="feature.lineloading.pallet.view" />
  <@navigationitem
    name="nav.cartons"
    namespace="carton"
    url="/lineloading/carton/list.action"
    tabIndex="203"
    featureName="feature.lineloading.carton.view" />
  <@navigationitem
    name="nav.labor"
    namespace="labor"
    url="/lineloading/labor/list.action"
    tabIndex="204"
    featureName="feature.voicelink.labor.view" />
  <#assign dropDownNamespaces= ["operator", "region", "reports", "workgroup"] />
    
  <#assign dropDownId=navigation.applicationMenu+"Select" />
  <li id="${dropDownId}Item"
  <#if dropDownNamespaces?seq_contains(navigation.actionsMenu)>
    class="highlight"
  </#if>
  >

    <div>
      <div class="navDropDownContainer">
        <select id="${dropDownId}" class="navDropDown" tabIndex="205" tid="navDropDown">
          <option value="go"><@s.text name='nav.go' /></option>
		  <@navigationOption 
		    name="nav.operators"
		    namespace="operator"
		    url="/lineloading/operator/list.action" 
		    featureName="feature.voicelink.operator.view" />
          <@navigationOption 
		    name="nav.region"
		    namespace="region"
		    url="/lineloading/region/list.action" 
		    featureName="feature.lineloading.region.view" />
		  <@navigationOption 
            name="nav.reports"
            namespace="report"
            url="/lineloading/report/list.action" 
            featureName="feature.voicelink.report.view" />   
		  <@navigationOption 
		    name="nav.workgroups"
		    namespace="workgroup"
		    url="/lineloading/workgroup/list.action" 
		    featureName="feature.voicelink.workgroup.view" />
        </select>
      </div>
      <div class="lowerEdge"></div>
      <div class="actionBubbleContainer" id="${dropDownId}_bubble">
        <div class="actionBubble actionBubbleTop"></div>
        <div class="actionBubble actionBubbleMiddle">
          <div class="actionBubbleContents" id="${dropDownId}_msg">
            <@s.text name="nav.action.message.permissions" />
          </div>
        </div>
        <div class="actionBubble actionBubbleBottom"></div>
	  </div>
    </div>
  </li>
</ul>
<#assign tabIndexNumber = 253 />
<#if summaries?has_content>
		<#list summaries?keys as summaryId>
		    <#if summaries[summaryId].descriptiveText = "TabularSummary"> 
		         <#assign tableId = summaries[summaryId].tableId >		
		   	     <#if tableId = "lineloadingLaborSummary">
		   		    <div class="spacer"></div>
				    <div class="menuHeader"><@s.text name="nav.summary.labor.actions" /></div>				    
				    <ul class="NavList">
				    <@action
				      name="nav.summary.labor.viewOperatorLabor"
				      tableId="list${tableId}Table"
				      id="a_viewOperatorLabor"
				      url="javascript:viewLaborData(${tableId}Obj,\\'${base}/lineloading/labor/list.action\\');"
				      tabIndex="${tabIndexNumber}"
				      actionEnablers={"enableOnOne":"summary.labor.disabled.selection"}
				      featureName="feature.voicelink.labor.view" />
				    </ul>		   		
		   		 </#if>
		         <#assign tabIndexNumber = tabIndexNumber + 1 >
		     </#if>	   	
		</#list>
</#if>
<script type="text/javascript">
	<#if navigation.actionsMenu?has_content>
		initializeDropDown("${dropDownId}", "a_${navigation.actionsMenu}");
	<#else>
		initializeDropDown("${dropDownId}", null);
	</#if>
	<#if !(navigation.pageName?exists)>
		var contextHelpUrl = 12;
	<#else>
		var contextHelpUrl = -1;
	</#if>
</script>
