<#include "/include/common/navigationmacros.ftl" />
<div class="menuHeader"><@s.text name='nav.dashboardAlert.menu'/></div>
<ul class="NavList">
    <@navigationitem
      name="nav.dashboardalert.myDashboard"
      namespace="mydashboard"
      url="/dashboardalert/mydashboard/myDashboard.action"
      tabIndex="201"
      featureName="always" />
      
    <@navigationitem
      name="nav.dashboardalert.dashboards"
      namespace="dashboards"
      url="/dashboardalert/dashboards/list.action"
      tabIndex="202"
      featureName="always" />
      
    <@navigationitem
      name="nav.dashboardalert.charts"
      namespace="charts"
      url="/dashboardalert/charts/list.action"
      tabIndex="203"
      featureName="always" />
      
    <@navigationitem
      name="nav.dashboardalert.alerts"
      namespace="alerts"
      url="/dashboardalert/alerts/list.action"
      tabIndex="204"
      featureName="always" />
</ul>
