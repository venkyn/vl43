<#include "/include/common/action.ftl" />
<#include "/include/common/navigationmacros.ftl" />
<div class="menuHeader"><@s.text name='nav.loading.menu'/></div>
<ul class="NavList">
  <#if currentSiteID==-927>
      <@navigationitem
        name="nav.loadingHome"
        namespace="home"
        url="/loading/home.action?currentSiteID=-927"
        tabIndex="201"
        featureName="feature.loading.home" />
  <#else>
      <@navigationitem
        name="nav.loadingHome"
        namespace="home"
        url="/loading/home.action"
        tabIndex="201"
        featureName="feature.loading.home" />
  </#if>
  <@navigationitem
    name="nav.loading.routesAndStops"
    namespace="route"
    url="/loading/route/list.action"
    tabIndex="202"
    featureName="feature.loading.route.view" />
  <@navigationitem
    name="nav.loading.containers"
    namespace="container"
    url="/loading/container/list.action"
    tabIndex="203"
    featureName="feature.loading.container.view" />    
  <@navigationitem
    name="nav.labor"
    namespace="labor"
    url="/loading/labor/list.action"
    tabIndex="204"
    featureName="feature.loading.labor.view" />
  <#assign dropDownNamespaces= ["breakType", "operator", "print", "region", "report",
                                 "deliveryLocationMapping", "vscresponse", "workgroup"] />

  <#assign dropDownId=navigation.applicationMenu+"Select" />
  <li id="${dropDownId}Item"
  <#if dropDownNamespaces?seq_contains(navigation.actionsMenu)>
    class="highlight"
  </#if>
  >

    <div>
      <div class="navDropDownContainer">
        <select id="${dropDownId}" class="navDropDown" tabIndex="205" tid="navDropDown">
          <option value="go"><@s.text name='nav.go' /></option>
          <@navigationOption
            name="nav.breakTypes"
            namespace="breakType"
            url="/loading/breakType/list.action"
            featureName="feature.voicelink.breakType.view" />
          <@navigationOption
            name="nav.locations"
            namespace="location"
            url="/loading/location/list.action"
            featureName="feature.voicelink.location.view" />            
          <@navigationOption
            name="nav.operators"
            namespace="operator"
            url="/loading/operator/list.action"
            featureName="feature.voicelink.operator.view" />
          <@navigationOption
            name="nav.printer"
            namespace="print"
            url="/loading/print/list.action"
            featureName="feature.voicelink.printer.view" />
          <@navigationOption
            name="nav.region"
            namespace="region"
            url="/loading/region/list.action"
            featureName="feature.loading.region.view" />
          <@navigationOption
            name="nav.reports"
            namespace="report"
            url="/loading/report/list.action"
            featureName="feature.voicelink.report.view" />
          <@navigationOption 
            name="nav.vehicleSafetyCheck"
            namespace="vscresponse"
            url="/loading/vscresponse/list.action" 
            featureName="feature.voicelink.vsc.view" />
          <@navigationOption
            name="nav.workgroups"
            namespace="workgroup"
            url="/loading/workgroup/list.action"
            featureName="feature.voicelink.workgroup.view" />
        </select>
      </div>
      <div class="lowerEdge"></div>
      <div class="actionBubbleContainer" id="${dropDownId}_bubble">
        <div class="actionBubble actionBubbleTop"></div>
        <div class="actionBubble actionBubbleMiddle">
          <div class="actionBubbleContents" id="${dropDownId}_msg">
            <@s.text name="nav.action.message.permissions" />
          </div>
        </div>
        <div class="actionBubble actionBubbleBottom"></div>
      </div>
    </div>
  </li>
</ul>
<#assign tabIndexNumber = 253 />
<#if summaries?has_content>
		<#list summaries?keys as summaryId>
		    <#if summaries[summaryId].descriptiveText = "TabularSummary"> 
		         <#assign tableId = summaries[summaryId].tableId >
		   	     <#if tableId = "loadingLaborSummary">
		   		    <div class="spacer"></div>
				    <div class="menuHeader"><@s.text name='nav.summary.labor.actions' /></div>	
				    <div id="menuBlade_listloadingLaborSummaryTable">			    
				    <ul class="NavList" id="bladeList_listloadingLaborSummaryTable">
				    <@action
				      name="nav.summary.labor.viewOperatorLabor"
				      tableId="list${tableId}Table"
				      id="a_viewOperatorLabor"
				      url="javascript:viewLaborData(${tableId}Obj,\\'${base}/loading/labor/list.action\\');"
				      tabIndex="${tabIndexNumber}"
				      actionEnablers={"enableOnOne":"summary.labor.disabled.loading"}
				      featureName="feature.voicelink.labor.view" />
				    </ul>		   		
				    </div>
		   		 </#if>
		         <#assign tabIndexNumber = tabIndexNumber + 1 >
		     </#if>	   	
		</#list>
</#if>


<script type="text/javascript">
    <#if navigation.actionsMenu?has_content>
        initializeDropDown("${dropDownId}", "a_${navigation.actionsMenu}");
    <#else>
        initializeDropDown("${dropDownId}", null);
    </#if>
    <#if !(navigation.pageName?exists)>
        var contextHelpUrl = 300;
    <#else>
        var contextHelpUrl = -1;
    </#if>
</script>
