<#include "/include/common/navigationmacros.ftl" />
<#include "/include/common/action.ftl" />
<div class="menuHeader"><@s.text name='nav.selection.menu'/></div>

<script type="text/javascript">
    var showAssignOperatorActionURL = '${base}/selection/operator/showAssignOperators.action';
    var regionId = 'regionId';
</script>

<ul class="NavList">
  <#if currentSiteID==-927>
      <@navigationitem
        name="nav.selectionHome"
        namespace="home"
        url="/selection/home.action?currentSiteID=-927"
        tabIndex="201"
        featureName="feature.selection.home" />
  <#else>
      <@navigationitem
        name="nav.selectionHome"
        namespace="home"
        url="/selection/home.action"
        tabIndex="201"
        featureName="feature.selection.home" />
  </#if>
  <@navigationitem
    name="nav.assignmentsAndPicks"
    namespace="assignment"
    url="/selection/assignment/list.action"
    tabIndex="202"
    featureName="feature.selection.assignment.view" />
  <@navigationitem
    name="nav.labor"
    namespace="labor"
    url="/selection/labor/list.action"
    tabIndex="203"
    featureName="feature.voicelink.labor.view" />
  <@navigationitem
    name="nav.shorts"
    namespace="short"
    url="/selection/short/list.action"
    tabIndex="204"
    featureName="feature.selection.short.view" />
  
  <#assign dropDownNamespaces= ["breakType", "container", "deliveryLocationMapping", 
                                "item", "locItem", "location", "lot", "operator", "print",
                                "region", "report", "summaryPrompt", "vscresponse", "workgroup"] />
  <#assign dropDownId=navigation.applicationMenu+"Select" />
  <li id="${dropDownId}Item"
  <#if dropDownNamespaces?seq_contains(navigation.actionsMenu)>
    class="highlight"
  </#if>
  >
    <div>
      <div class="navDropDownContainer">
        <select id="${dropDownId}" class="navDropDown" tabIndex="205" tid="navDropDown">
          <option value="go"><@s.text name='nav.go' /></option>
          <@navigationOption 
            name="nav.breakTypes"
            namespace="breakType"
            url="/selection/breakType/list.action" 
            featureName="feature.voicelink.breakType.view" />
          <@navigationOption 
            name="nav.containers"
            namespace="container"
            url="/selection/container/list.action" 
            featureName="feature.selection.container.view" />          
          <@navigationOption 
            name="nav.deliveryLocationMapping"
            namespace="deliveryLocationMapping"
            url="/selection/deliveryLocationMapping/list.action" 
            featureName="feature.voicelink.deliveryLocationMapping.view" />                  
          <@navigationOption
            name="nav.items"
            namespace="item"
            url="/selection/item/list.action"
            featureName="feature.voicelink.item.view" />
          <@navigationOption 
            name="nav.itemLocationMapping"
            namespace="locItem"
            url="/selection/locItem/list.action" 
            featureName="feature.voicelink.itemLocationMapping.view" />
          <@navigationOption
            name="nav.locations"
            namespace="location"
            url="/selection/location/list.action"
            featureName="feature.voicelink.location.view" />
          <@navigationOption
            name="nav.lots"
            namespace="lot"
            url="/selection/lot/list.action"
            featureName="feature.voicelink.lot.view" />            
          <@navigationOption 
            name="nav.operators"
            namespace="operator"
            url="/selection/operator/list.action" 
            featureName="feature.voicelink.operator.view" />
          <@navigationOption 
            name="nav.printer"
            namespace="print"
            url="/selection/print/list.action" 
            featureName="feature.voicelink.printer.view" />
          <@navigationOption 
            name="nav.region"
            namespace="region"
            url="/selection/region/list.action" 
            featureName="feature.selection.region.view" />
          <@navigationOption 
            name="nav.reports"
            namespace="report"
            url="/selection/report/list.action" 
            featureName="feature.voicelink.report.view" />                        
          <@navigationOption 
            name="nav.summaryPrompts"
            namespace="summaryPrompt"
            url="/selection/summaryPrompt/list.action" 
            featureName="feature.selection.summaryPrompt.view" />
          <@navigationOption 
            name="nav.vehicleSafetyCheck"
            namespace="vscresponse"
            url="/selection/vscresponse/list.action" 
            featureName="feature.voicelink.vsc.view" />
          <@navigationOption 
            name="nav.workgroups"
            namespace="workgroup"
            url="/selection/workgroup/list.action" 
            featureName="feature.voicelink.workgroup.view" />
        </select>
      </div>
      <div class="lowerEdge"></div>
      <div class="actionBubbleContainer" id="${dropDownId}_bubble">
        <div class="actionBubbleCarrot"></div>
        <div class="actionBubble actionBubbleTop"></div>
        <div class="actionBubble actionBubbleMiddle">
          <div class="actionBubbleContents" id="${dropDownId}_msg">
            <@s.text name="nav.action.message.permissions" />
          </div>
        </div>
        <div class="actionBubble actionBubbleBottom"></div>
      </div>
    </div>
  </li>
</ul>
<#assign tabIndexNumber = 253 />
<#if summaries?has_content>
        <#list summaries?keys as summaryId>
            <#if summaries[summaryId].descriptiveText = "TabularSummary"> 
                 <#assign tableId = summaries[summaryId].tableId >
                    <#if tableId = "selectionCurrentWorkSummary">                   
                       <div class="spacer"></div>
                    <div class="menuHeader"><@s.text name='nav.selection.summary.selectionCurrentWork.actions'/></div>        
                    <div id="menuBlade_listselectionCurrentWorkSummaryTable">            
                    <ul class="NavList" id="bladeList_listselectionCurrentWorkSummaryTable">
                    <@action
                      name="nav.selection.summary.selectionCurrentWork.assignOperators"
                      tableId="listselectionCurrentWorkSummaryTable"
                      id="a_assignOperators"
                      url="javascript:gotoUrlWithSelected(selectionCurrentWorkSummaryObj, showAssignOperatorActionURL , regionId)"
                      tabIndex="${tabIndexNumber}"
                      actionEnablers={"enableOnOne":"region.assignOperator.disabled.selection"}
                      featureName="feature.selection.region.assignOperators" />
                    </ul>
                    </div>
                       <ul></ul><ul></ul>
                    </#if>           
                    <#if tableId = "selectionLaborSummary">
                       <div class="spacer"></div>
                    <div class="menuHeader"><@s.text name='nav.summary.labor.actions' /></div>    
                    <div id="menuBlade_listselectionLaborSummaryTable">                
                    <ul class="NavList" id="bladeList_listselectionLaborSummaryTable">
                    <@action
                      name="nav.summary.labor.viewOperatorLabor"
                      tableId="list${tableId}Table"
                      id="a_viewOperatorLabor"
                      url="javascript:viewLaborData(${tableId}Obj,\\'${base}/selection/labor/list.action\\');"
                      tabIndex="${tabIndexNumber}"
                      actionEnablers={"enableOnOne":"summary.labor.disabled.selection"}
                      featureName="feature.voicelink.labor.view" />
                    </ul>                   
                    </div>
                    </#if>
                 <#assign tabIndexNumber = tabIndexNumber + 1 >
             </#if>           
        </#list>
</#if>
               

<script type="text/javascript">
    <#if navigation.actionsMenu?has_content>
        initializeDropDown("${dropDownId}", "a_${navigation.actionsMenu}");
    <#else>
        initializeDropDown("${dropDownId}", null);
    </#if>
    <#if !(navigation.pageName?exists)>
        var contextHelpUrl = 32;
    <#else>
        var contextHelpUrl = -1;
    </#if>
</script>
