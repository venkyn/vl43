<#if !readOnly?has_content || readOnly=="false">
  <#if !submitValue?has_content>
	  <#assign submitValue>
		<@s.text name="form.button.submit"/>
	  </#assign>
  </#if>

<input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>

  <#if !formId?has_content>
      <#assign formId="form1">
  </#if>
  
	<script type="text/javascript">
		var submitted = false;
		var submitId = "${submitId?default('${formId}.submit1')}";
		var cancelId = "${cancelId?default('${formId}.cancel1')}";
	</script>

  <#if !noSubmitButton?exists>
		
	<a
	href="javascript:submitForm($('${formId}'));"
	onClick="${customOnClick?default('return checkSubmitted(submitId)')}"
	id="${submitId?default('${formId}.submit1')}"
	class="${submitCssClass?default('anchorButton')}"
	tabindex="${submitIndex?default('-1')}"
	>${submitValue}</a>
	
	<#if submitOnChangeForm?exists>
		<script type="text/javascript">	
		    var anchorElement = $("${submitId?default('${formId}.submit1')}");
		    function connectSubmit() {	
			var connectObjects = {};
			formElements = $("${submitOnChangeForm}").elements;
			for( var i=0; i < formElements.length; i++){
				if( formElements[i].type != "hidden" && formElements[i].type != "submit" ){
				    connectObjects[formElements[i].id] = new Array();
					if( formElements[i].type == "checkbox" || formElements[i].type == "radio"){
						connectObjects[formElements[i].id].push(
						  connect(formElements[i],"onkeypress",anchorElement,enableSubmitButton));
						connectObjects[formElements[i].id].push(
						  connect(formElements[i],"onclick",anchorElement,enableSubmitButton));
					}else{
						connectObjects[formElements[i].id].push(
						  connect(formElements[i],"onkeydown",anchorElement,enableSubmitButton));
						connectObjects[formElements[i].id].push(
						  connect(formElements[i],"onkeypress",anchorElement,enableSubmitButton));
						connectObjects[formElements[i].id].push(
						  connect(formElements[i],"onchange",anchorElement,enableSubmitButton));
					}
				}		
			}
			}
			connectSubmit();
			setEnableSubmit(false);
			addElementClass(anchorElement,"disabled");
		</script>
	</#if>
	<script type="text/javascript">
		MochiKit.Signal.connect("${submitId?default('${formId}.submit1')}","onfocus",showFocusOnLink);
		MochiKit.Signal.connect("${submitId?default('${formId}.submit1')}","onblur",hideFocusOnLink);
	</script>
  </#if>

  <#if !cancelValue?has_content>
	<#assign cancelValue>
		<@s.text name="form.button.cancel"/>
	</#assign>
  </#if>

  <#if !noCancelButton?exists>

	<a
	<#if cancelHref?has_content>
	href="${cancelHref}"
	<#else>
	href="javascript:modifySubmitVariable('${formId}','submitTypeElement','method:cancel');"
	</#if>
	onClick="return checkSubmitted(cancelId)"
	id="${cancelId?default('${formId}.cancel1')}"
	class="${cancelCssClass?default('anchorButton')}"
	tabindex="${cancelIndex?default('-1')}"
	>${cancelValue}</a>
	
	<script type="text/javascript">
		MochiKit.Signal.connect("${cancelId?default('${formId}.cancel1')}","onfocus",showFocusOnLink);
		MochiKit.Signal.connect("${cancelId?default('${formId}.cancel1')}","onblur",hideFocusOnLink);
	</script>
	
  </#if>
</#if>
