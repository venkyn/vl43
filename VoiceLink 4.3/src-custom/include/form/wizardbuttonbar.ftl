<input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>
  <#if !formId?has_content>
	  <#assign formId="form1"/>
  </#if>

  <script type="text/javascript">
    var submitted = false;
    var prevId = "${previousId?default('${formId}.previous1')}";
    var nextId = "${submitId?default('${formId}.submit1')}";
    var cancelId = "${cancelId?default('${formId}.cancel1')}";
  </script>
<#-- PREVIOUS BUTTON -->
	
	<#if !previousButtonText?has_content>
		<#assign previousButtonText>
			<@s.text name="form.button.wizard.previous"/>
		</#assign>
	</#if>
	
	<a
	<#if previousHref?has_content>
		href="${previousHref}" onClick="return checkSubmitted(prevId)"
	</#if>
	id="${previousId?default('${formId}.previous1')}"
	class="${previousCssClass?default('anchorButton')}"
	tabindex="${previousIndex?default('-1')}"
	>${previousButtonText}</a>
	
	<script type="text/javascript">
		MochiKit.Signal.connect("${previousId?default('${formId}.previous1')}","onfocus",showFocusOnLink);
		MochiKit.Signal.connect("${previousId?default('${formId}.previous1')}","onblur",hideFocusOnLink);
	</script>
	
	<#if !previousHref?has_content>
		<script type="text/javascript">
			var anchorElement = $("${previousId?default('${formId}.previous1')}");
			addElementClass(anchorElement,"disabled");
		</script>
	</#if>
	
<#-- NEXT BUTTON -->
	
	<#if !nextButtonText?has_content>
		<#assign nextButtonText>
			<@s.text name="form.button.wizard.next"/>
		</#assign>
	</#if>
	
	<a
	href="javascript:submitForm($('${formId}'));"
	onClick="return checkSubmitted(nextId)"
	id="${submitId?default('${formId}.submit1')}"
	class="${submitCssClass?default('anchorButton')}"
	tabindex="${submitIndex?default('-1')}"
	>${nextButtonText}</a>
	
	<#if submitOnChangeForm?exists>
		<script type="text/javascript">		
			var anchorElement = $("${submitId?default('${formId}.submit1')}");
			formElements = $("${submitOnChangeForm}").elements;
			for( var i=0; i < formElements.length; i++){
				if( formElements[i].type != "hidden" && formElements[i].type != "submit" ){
					if( formElements[i].type == "checkbox" || formElements[i].type == "radio"){
						connect(formElements[i],"onkeypress",anchorElement,enableSubmitButton);
						connect(formElements[i],"onclick",anchorElement,enableSubmitButton);
					}else{
						connect(formElements[i],"onkeydown",anchorElement,enableSubmitButton);
						connect(formElements[i],"onkeypress",anchorElement,enableSubmitButton);
						connect(formElements[i],"onchange",anchorElement,enableSubmitButton);
					}
				}		
			}
			setEnableSubmit(false);
			addElementClass(anchorElement,"disabled");
		</script>
	</#if>
	<script type="text/javascript">
		MochiKit.Signal.connect("${submitId?default('${formId}.submit1')}","onfocus",showFocusOnLink);
		MochiKit.Signal.connect("${submitId?default('${formId}.submit1')}","onblur",hideFocusOnLink);
	</script>
	
<#-- CANCEL BUTTON -->

	<#if !cancelButtonText?has_content>
		<#assign cancelButtonText>
			<@s.text name="form.button.wizard.cancel"/>
		</#assign>
	</#if>

	<a
	<#if cancelHref?has_content>
	href="${cancelHref}"
	<#else>
	href="javascript:modifySubmitVariable('${formId}','submitTypeElement','method:cancel');"
	</#if>
	onClick="return checkSubmitted(cancelId)"
	id="${cancelId?default('${formId}.cancel1')}"
	class="${cancelCssClass?default('anchorButton')}"
	tabindex="${cancelIndex?default('-1')}"
	>${cancelButtonText}</a>
	
	<script type="text/javascript">
		MochiKit.Signal.connect("${cancelId?default('${formId}.cancel1')}","onfocus",showFocusOnLink);
		MochiKit.Signal.connect("${cancelId?default('${formId}.cancel1')}","onblur",hideFocusOnLink);
	</script>

