<#assign authzx=JspTaglibs["/WEB-INF/authzx.tld"]>
<#-- 
	Macro definition of an action for the actions menu. I know, i'm JavaDoc'ing
	in a place that doesnt really do that... but at least everyone knows the syntax.
	Note: I reused existing locale resource keys that were already in use elsewhere.
	@param sizeInBytes The size in bytes of a file.  Make sure to use ?c to computer-ify the number.
	@author Tim Reed
-->
<#macro formatdays days>
	<#--Display the appropriate form of days.  Display "Disabled" if days is null.-->	
	<#if days = 1>
		${days} <@s.text name='unit.day'/>
	<#elseif days &gt; 1>
		${days} <@s.text name='unit.days'/>
	<#elseif days = 0>
		${days} <@s.text name='unit.days'/>
	<#else>
		<@s.text name='status.disabled'/>
	</#if>
</#macro>
