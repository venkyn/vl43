<#-- 
	Macro definition for a grouping of controls similar in appearance to a Windows GroupBox form object
	@param groupId the id of the resulting group box, which will ultimately be "grp_" + groupId
	@param caption The key of the caption text to display
	@param addpadding set to true if the content does not clip properly-- this happens when the final control's label spans 2 lines but nothing is drawn for the actual result
	
	@author John Zeitler
-->
<style type="text/css">
div.controlgroup {
	width: 100%;
}
div.controlgroup span.caption {
	position: relative;
	background-color: white;
	top: 8px;
	left: 5px;
	padding-left: 3px;
	padding-right: 3px;
	font-weight: bold;
}
div.controlgroup div.borderbox {
	border: thin solid black;
	padding-left: 10px;
	padding-bottom: 8px;
	padding-top: 10px;
}
div.disabledgroup {
	color: gray;
}
div.disabledgroup div.borderbox {
	border: thin solid gray;
}
div.controlgroup div.innerbox {
	position: relative;
	top: -8px;
}
div.controlgroup .label {
	width: 95%;
}
div.controlgroup div.wwlbl {
	width: 100%;
}
div.controlgroup div.wwctrl {
}

div.controlgroup div.wwerr{
	position:relative;
	margin-bottom: 2px;
	margin-top: 0px;
	display:block;
	height:20px;
}

table.wwctrlgrp_table{
	width:95%;
}

table.wwctrlgrp_table tr td.wwlbl_td{
	width:40%;
}
</style>
<#macro controlgroup groupId caption disabled addpadding="false">
<#if disabled == "true">
<div id="grp_${groupId}" class="controlgroup disabledgroup">
<#else>
<div id="grp_${groupId}" class="controlgroup">    
</#if>    
	<span class="caption"><@s.text name="${caption}" /></span>
	<div class="borderbox">
		<div class="innerbox">
			<#nested>
			<#if addpadding == "true">
				<br />
			</#if>
		</div>
	</div>
</div>
<script type="text/javascript">
$('grp_${groupId}').disable = function() {
	if( !hasElementClass('grp_${groupId}', 'disabledgroup' ) ) {
		this.addClassName('disabledgroup');
		this.exceptions = new Array();
		var children = getElementsByTagAndClassName( 'input', null, 'grp_${groupId}' );
		children.concat( getElementsByTagAndClassName( 'select', null, 'grp_${groupId}' ) );
		for(i=0;i<children.length;i++) {
			if( children[i].disabled == true ) {
				//controls already disabled when the group is disabled must not be re-enabled
				this.exceptions.push(children[i]);
			} else {
				children[i].disabled=true;
			}
		}
	}
}
$('grp_${groupId}').enable = function() {
	if( hasElementClass('grp_${groupId}', 'disabledgroup' ) ) {
		this.removeClassName('disabledgroup');
		var children = getElementsByTagAndClassName( 'input', null, 'grp_${groupId}' );
		children.concat( getElementsByTagAndClassName( 'select', null, 'grp_${groupId}' ) );
		for(i=0;i<children.length;i++) {
			if( children[i].disabled == false ) {
				//controls enabled while the group was disabled must not be re-disabled; remove them from the list to restore
				for(j=0;j<this.exceptions.length;j++) {
					if( this.exceptions[j].id == children[i].id ) {
						this.exceptions.splice(j, 1);
						break;
					}
				}
			}
			children[i].disabled=false;
		}
		//re-disable any controls which were already disabled when the group was disabled (but have not been re-enabled in the meantime)
		for(i=0;i<this.exceptions.length;i++) {
			this.exceptions[i].disabled=true;
		}
	}
}
<#--it should be noted that you can re-enable and re-disable a control within a disabled group as many times as you want without changing the control's status
 	within the exceptions array; all that matters is the disabled status at the time that the entire group is enabled/disabled. -->
</script>
</#macro>
