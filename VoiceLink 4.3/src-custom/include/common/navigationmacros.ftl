<#assign authzx=JspTaglibs["/WEB-INF/authzx.tld"]>

<#macro navigationitem name namespace url tabIndex featureName>
  <#assign id="a_"+namespace />
  <li id="${id}"
  <#if namespace == navigation.actionsMenu>
    class="highlight"
  </#if>
  >
  	<#assign isAccessible=false>
    <@authzx.authorizeByFeature featureName=featureName>
      <#assign isAccessible=true>
    </@authzx.authorizeByFeature>
    <#if !isAccessible>
    <div>
      <a id="${id}_link" class="disabledAction" tabindex="${tabIndex}" href="javascript:void(0);">
          <@s.text name=name/>
      </a>
  	  <div class="lowerEdge"></div>
  	</div>
    <div class="actionBubbleContainer" id="${id}_link_bubble">
      <div class="actionBubbleCarrot"></div>
      <div class="actionBubble actionBubbleTop"></div>
      <div class="actionBubble actionBubbleMiddle">
        <div class="actionBubbleContents" id="${id}_link_msg">
          <@s.text name="nav.action.message.permissions" />
        </div>
      </div>
      <div class="actionBubble actionBubbleBottom"></div>
	</div>
	<script type="text/javascript">
		var actionsMenu = getActionsMenu();
	 	connect('${id}_link', 'onmouseover', actionsMenu, 'showActionBubble');
	 	connect('${id}_link', 'onmouseout', actionsMenu, 'hideActionBubble');
	</script>
	<#else>
	<div>
      <a id="${id}_link" class="enabledAction" tabindex="${tabIndex}" href="<@s.url includeParams='none'  value=url />"><@s.text name=name /></a>
  	  <div class="lowerEdge"></div>
  	</div>
	</#if>
  </li>
</#macro>

<#macro navigationOption name namespace url featureName>
  <#assign className="disabledItem">
  <@authzx.authorizeByFeature featureName=featureName>
    <#assign className="enabledItem">
  </@authzx.authorizeByFeature>
  <#if namespace == navigation.actionsMenu>
    <#assign className=className+" optionHighlight">
  </#if>
  <#assign id="a_"+namespace />
  <option class="${className}" id="${id}" value="<@s.url includeParams='none'  value=url />"><@s.text name=name /></option>
</#macro>
