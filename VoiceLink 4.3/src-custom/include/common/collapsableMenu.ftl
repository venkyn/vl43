<#if navigation.pageName?has_content>
	<#assign pageName = navigation.pageName>
<#else>
	<#assign pageName = "homepage">
</#if>
<#if currentUser.navMenuOpen>
<#assign menuImg = "open_arrow" />
<#assign openMenu = "false" />
<#assign option = "hide" />
<#else>
<#assign menuImg = "close_arrow" />
<#assign openMenu = "true" />
<#assign option = "show" />
</#if>
<script type="text/javascript">	
	function showHideMenuDialog() {
		var dialogProperties = {
			    title: "<@s.text name='showhide.${option}.title'/>",			    
				body: "<@s.text name='showhide.body.${option}'/>",
				button1: "<@s.text name='showhide.${option}.yesText'/>",
				button2: "<@s.text name='showhide.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(setOpenMenu, ${openMenu}), "");
	}
</script>
<#if pageName == "edit" || pageName == "create" || pageName == "viewProfile">
	<#assign isInputPage = true />
	<#assign jsFunction = "javascript:showHideMenuDialog();" />
<#else>
	<#assign isInputPage = false />
	<#assign jsFunction = "javascript:setOpenMenu(${openMenu});" />
</#if>
<@s.form name="openCloseNavMenu" theme="css_xhtml" action="/openCloseNavMenu.action" method="POST">
		  <@s.hidden name="lastRequestedURL"/>
		  <@s.hidden name="sideNavHeight"/>
		  <@s.hidden name="selectedObjects"/>
		   <@s.hidden name="navMenuOpen" value=true/>
		   <A href = "${jsFunction}">
		   <img id="${menuImg}" class="arrow" border = 0 src="${base}/images/${menuImg}.gif" />
		   </A>
		  </@s.form>