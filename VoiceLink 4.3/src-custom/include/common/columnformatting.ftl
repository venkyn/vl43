<link type="text/css" rel="stylesheet" href="${base}/scripts/yui/reset/reset-min.css" />
<link type="text/css" rel="stylesheet" href="${base}/scripts/yui/fonts/fonts-min.css" />
<link type="text/css" rel="stylesheet" href="${base}/scripts/yui/grids/grids-min.css" />

<#-- 
	Macro definition for column formatting of data/CRUD objects.
	@param header key of text to display in header of column layout
	
	@author John Zeitler
-->
<style type="text/css">
div#doc3 div#hd {
	font-weight: bold;
	text-decoration: underline;
}
div.yui-u {
	position: relative;
	left: 0px;
	/* reinforcing correct behavior for firefox/IE7 */
	overflow: visible;
} 
<#-- these are temporary and may be moved to style.css in the future -->
.label {
	width: 100%;
}
div.wwlbl {
	width: 100%;
}
div.wwctrl {
	margin-right: 2px;
	text-align: right;
}

div.wwerr{
	position:relative;
	margin-top: 0px;
	margin-bottom: 2px;
	display:block;
}

div.wwctrl input {
	padding-left: 3px;
}

<#-- this corrects a rendering bug introduced by the inclusion of YUI reset-min.css -->
div#tabs a {
	height: auto;
	padding-top: 5px;
    padding-bottom: 5px;
}

<#-- these are fixes applied to non-standard forms - mainly any Create Region pages -->
div.errorMessage{
	float:right;
}

div.moreErrorsMiddle{
	text-align:left;
}

</style>
<!--[if IE]>
<#-- IE Only Fix -->
<style type="text/css">
div.yui-u {
	/* fix for IE only */
	word-wrap: break-word;
}
</style>
<![endif]-->
<#macro columnlayout header="">
<div id="doc3">
	<#if header != "">
	<div id="hd"><@s.text name="${header}" /></div>
	</#if>
	<div class="yui-b">
		<div class="yui-g" style="position: relative;">
			<#nested>
		</div>
	</div>
</div>
</#macro>
