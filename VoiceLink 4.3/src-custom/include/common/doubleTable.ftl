<#macro doubleTable title jsFile varName1 columns1 tableId1 url1 tableTitle1 viewId1
    		varName2 columns2 tableId2 url2 urlMethod2 tableTitle2 viewId2 
    		objectIdName pageName type1="" extraParams1=extraParamsHash?default("") 
    		type2="" extraParams2=extraParamsHash?default("") 
    	 
>

  <title><@s.text name='${title}'/></title>

  <script type="text/javascript">
    pageContext = "${pageContext}";
  </script>
  <script type="text/javascript" src=${jsFile}></script>

  <#include "/include/common/tablecomponent.ftl">
	
  <@tablecomponent
 		varName=varName1
		columns=columns1
		tableId=tableId1
		url=url1
		tableTitle=tableTitle1 
		viewId=viewId1
    	type=type1
    	extraParams=extraParams1 />     			

  <#assign urlMethod=urlMethod2>
  <@tablecomponent
 		varName=varName2
		columns=columns2
		tableId=tableId2
		url=url2
		tableTitle=tableTitle2 
		viewId=viewId2
    	type=type2
    	extraParams=extraParams2 />          			

  <script type="text/javascript">
	function getNewData(e) {
		${varName2}.refresh({'${objectIdName}': ${varName1}.getSelectedIds(), '${pageName}': true});
	}
	   		
	connect($('${tableId1}'), 'onSelectedRowsChanged', this, getNewData);
	connect(currentWindow(), 'onload', this, getNewData);
	   	    
	if( $("menuBlade_${tableId1}") != null ) {
	    //set up and register handlers
	    ${varName1}.menuBladeHeight = $("menuBlade_${tableId1}").clientHeight;
	    ${varName2}.menuBladeHeight = $("menuBlade_${tableId2}").clientHeight;
	   	    
		function openTable1Blade(evt) {
	   		showActionsMenu(${varName1});
	   	    hideActionsMenu(${varName2});
	   	}
	   	    
	   	function openTable2Blade(evt) {
	   		showActionsMenu(${varName2});
	   	    hideActionsMenu(${varName1});
		}
	   	    
	   	${varName1}.registerHandlers(openTable1Blade, null);
	   	${varName2}.registerHandlers(openTable2Blade, null); 
	}
  </script>
    	
</#macro>