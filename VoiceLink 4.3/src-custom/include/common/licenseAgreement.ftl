<p>
<h3>VOCOLLECT, INC. LICENSE AGREEMENT</h3>
<br/>
<div class="emphasizeText">This License Agreement ("Agreement") is by and between 
Vocollect, Inc. ("Vocollect") and the undersigned licensee ("Licensee").  Intending 
to be legally bound, Vocollect and Licensee agree as follows:
</div>
<ol style="padding:20px"> 
<li><b>License</b> 
<ol> 
<li type="i"> 
For purposes of this Agreement, Software shall be defined as (i) the 
Vocollect software initially licensed by Licensee as specified in Schedule A; 
(ii) any software later acquired by Licensee from Vocollect or an authorized reseller 
of Vocollect; and (iii) all modifications, enhancements, new versions and new releases 
related to the above which may be provided by Vocollect or its authorized 
representative from time to time pursuant to a support agreement.  Vocollect 
hereby grants Licensee a limited, non-exclusive, non-transferable, perpetual 
license to use the Software in object code solely for use in accordance with the 
product specifications during the term of this Agreement (the "License").  The 
License is subject to the license restrictions for the applicable Software specified 
in Schedule A.  In addition, Licensee agrees that the terms of this Agreement, 
including the license terms set forth in Schedule A, shall apply to all Software 
licensed by Licensee after execution of this Agreement.
</li><br><br>
<li type="i">
The Software may be used by Licensee or its Affiliates.  For purposes of this 
Agreement, "Affiliate" shall mean any entity that controls, is controlled by, or 
is under common control with Licensee, and any reference to control shall mean 
ownership of more than fifty percent (50%) of the outstanding voting stock of an 
entity.  Licensee shall be responsible for the breach by any Affiliate of the terms 
of this Agreement and Licensee shall indemnify Vocollect for any such breach.
</li><br><br>
<li type="i">
Vocollect and its licensors and/or suppliers, as applicable, shall retain all right, 
title and interest to the Software including all patents, copyrights, trademarks, 
trade secret and other proprietary rights thereto.  Licensee may not make copies of 
the Software, except for one (1) additional copy for archival or back-up purposes.  
All copies of the Software, whether authorized or unauthorized, are subject to the 
terms and conditions of this Agreement.  Licensee will not, nor will Licensee 
authorize any third party to, (i) modify, translate, localize, or create derivative 
works of the Software; (ii) distribute, sell, lend, rent, transfer, convey, 
decompile, disassemble, reverse engineer or attempt to reconstruct, identify or 
discover any source code, underlying user interface techniques or algorithms of the 
Software by any means whatsoever, for any purpose whatsoever; (iii) grant any 
sublicense, leases or other rights in the Software to any third party; or (iv) take 
any action that would cause the Software to be placed in the public domain.  The 
copyright notices and other proprietary legends shall not be removed from the Software.
</li><br><br>
<li type="i">
The Software includes or is distributed with certain third party software 
("Third Party Software").  Such Third Party Software is licensed to Licensee 
pursuant to the license terms of such Third Party Software which are accessible at 
www.vocollect.com/sla.  By signing below, Licensee acknowledges it agrees to the 
license terms of the Third Party Software.  Vocollect shall not be responsible for 
providing Licensee with any Microsoft&#174; client access licenses which may be necessary 
to use the Software.
</li>
</ol>
</li><br>
<li><b>Term and Termination</b>
<ol>
This Agreement is effective until terminated.  Licensee may terminate this Agreement 
by destroying all copies of the Software and purging same from all memory devices.  
In the event of a material breach of this Agreement by Licensee, Vocollect may 
immediately terminate this Agreement by written notice to Licensee. For purposes of 
this Agreement, a material breach shall be an infringement or misappropriation of 
Vocollect&#146;s intellectual property rights or a breach of Section 1.3 or Section 3.  
For any other breach of this Agreement, Vocollect will provide Licensee with fifteen 
(15) days written notice of such breach and if Licensee does not cure the breach 
within the fifteen (15) day notice period, Vocollect may immediately terminate this 
Agreement.  Upon any termination, Licensee will destroy all copies of the Software 
or, if requested by Vocollect, return all copies of the Software to Vocollect and 
have an appropriate authorized representative certify in writing the return or 
destruction of all copies of the Software.
</ol>
</li><br>
<li><b>Confidentiality</b>
<ol>
<li type="i">
Licensee acknowledges that the Software, related documentation, and other 
confidential information related to the Vocollect products which may be provided by 
Vocollect or its authorized representative (collectively "Confidential Information") 
is confidential information of Vocollect.  Licensee agrees not to disclose the 
Confidential Information to third parties or use the Confidential Information other 
than in connection with its license rights under this Agreement.  Licensee will use 
at least the same security measures as Licensee uses to protect its own confidential 
and trade secret information but no less than reasonable measures to protect the 
Confidential Information.  Confidential Information shall not include information: 
(i) already in Licensee&#146;s possession at the time of disclosure, (ii) that is or later 
becomes part of the public domain through no fault of Licensee, or (iii) is required 
to be disclosed pursuant to law or court order provided that Licensee shall notify 
Vocollect prior to such disclosure and assist Vocollect in preventing or limiting 
such required disclosure.
</li><br><br>
<li type="i">
Licensee agrees and acknowledges that any breach of the provisions regarding 
ownership or confidentiality contained in this Agreement shall cause Vocollect 
irreparable harm and Vocollect may obtain injunctive relief without the requirement 
to post a bond as well as seek all other remedies available to Vocollect in law and 
in equity in the event of breach or threatened breach of such provisions.
</li>
</ol>
</li><br>
<li><b>Limited Warranty and Disclaimer</b>
<ol>
<li type="i">
Provided that Licensee is not in breach of any of Licensee&#146;s obligations under this 
Agreement, Vocollect warrants that the Software will substantially conform to the 
published documentation for the Software for a period of ninety (90) days from the 
date the Software is shipped.  Licensee&#146;s sole remedy and Vocollect&#146;s sole obligation 
in the event of breach of this warranty, is, at Vocollect&#146;s option, correction of 
the substantial nonconformity or a refund of the applicable license fee for the 
Software.  The above warranty does not apply to the extent: (i) the Software is 
subjected to misuse or unauthorized use or modified by a party other than Vocollect; 
(ii) claims result from acts or omissions caused by persons other than Vocollect or 
from products, material or software not provided by Vocollect; (iii) claims are not 
reported to Vocollect within the warranty period or are not documented by Licensee; 
or (iv) Licensee uses the release of the Software other than the most recent release 
of the Software provided to Licensee by Vocollect.
</li><br><br>
<li type="i">
<b>VOCOLLECT DISCLAIMS ALL OTHER WARRANTIES FOR THE SOFTWARE, EXPRESS OR IMPLIED, 
INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT AND WARRANTIES ARISING FROM COURSE OF 
DEALING OR USAGE OF TRADE.</b>
</li>
</ol>
</li><br>
<li><b>Limitation of Liability</b>
<ol>
<li type="i">
IN NO EVENT SHALL VOCOLLECT OR ANY OF ITS LICENSORS BE LIABLE TO LICENSEE OR ANY 
THIRD PARTY FOR SPECIAL, INDIRECT, CONSEQUENTIAL, PUNITIVE OR INCIDENTAL DAMAGES, 
INCLUDING, BUT NOT LIMITED TO, LOSS OF REVENUES AND LOSS OF PROFITS, WHETHER ARISING 
UNDER CONTRACT, WARRANTY, OR TORT (INCLUDING NEGLIGENCE OR STRICT LIABILITY) OR ANY 
OTHER THEORY OF LIABILITY ARISING OUT OF OR IN CONNECTION WITH THE USE OF OR 
INABILITY TO USE THE SOFTWARE EVEN IF VOCOLLECT HAS BEEN ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGES.
</li><br><br>
<li type="i">
Regardless of whether any remedy set forth herein fails of its essential purpose or 
otherwise, Vocollect&#146;s or any of its licensors&#146; total liability, regardless of the 
form of the action, shall not exceed the license fee for the applicable Software.
</li>
</ol>
</li><br>
<li><b>Infringement</b>
<ol>
Vocollect shall indemnify, defend and hold Licensee harmless from and against any 
claim that the Software infringes the intellectual property rights of any third party 
provided that Licensee provides prompt written notice of such claim, provides 
reasonable assistance to Vocollect in the defense of such claim, and grants 
Vocollect the sole control over the defense or settlement of such claim.  If any 
Software becomes or, in Vocollect&#146;s opinion is likely to become the subject of an 
infringement claim or action, Vocollect may, at its option: (i) procure, at no cost 
to Licensee, the right for Licensee to continue using the Software; (ii) replace or 
modify the Software to render it non-infringing, provided there is no material loss 
of functionality; or (c) if, in Vocollect&#146;s reasonable opinion, neither (i) nor (ii) 
above is commercially feasible, terminate the license rights for such Software and 
refund a pro rata portion of the amount paid by Licensee for such Software based on 
a five (5) year depreciation schedule beginning the date the Software was licensed 
by Licensee.  The foregoing states Vocollect&#146;s sole obligation and Licensee&#146;s 
exclusive remedy in the event any such claim or action is commenced or is likely 
to be commenced.
</ol>
</li><br>
<li><b>General</b>
<ol>
Licensee acknowledges that the Software is subject to the laws and export regulations 
of the United States and Licensee agrees to comply with all of such laws and 
regulations.  This Agreement states the entire agreement between Vocollect and 
Licensee regarding the Software and supersedes all prior written and oral 
communications relating to the Software.  Any terms and conditions of a purchase 
order or other document issued by Licensee in connection with the order of the 
Software shall be superseded by the terms and conditions of this Agreement. This 
Agreement may be amended only by a written agreement executed by Licensee and 
Vocollect.  The failure of Vocollect to require performance of any provision hereof 
shall in no manner affect the right at a later time to enforce such provision.  This 
Agreement shall be construed in accordance with the laws of the Commonwealth of 
Pennsylvania without regard to its conflict of laws provisions.  The terms of the 
U.N. Convention on Contracts for the International Sale of Goods shall not apply.  
Licensee may not assign its rights under this Agreement without the prior written 
consent of Vocollect which will not be unreasonably withheld. This Agreement shall be 
binding on and inure to the benefit of Licensee, its successors, permitted assigns 
and legal representatives. Sections 2, 3, 4.2, 5, 6 and 7 shall survive termination 
or expiration of this Agreement for any reason.
</ol>
</li>
</ol>
