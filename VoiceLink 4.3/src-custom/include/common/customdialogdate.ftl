<#-- 
	Macro definition of a custom dialog.
	@param dialogId Id of the dialog box
	@param header The heading text of the dialog box
	@param dateInputFieldId Id of the text field where the 
							date will be displayed
	@param callingButtonId Id of the button or the link who will 
							can this dialog
	@param multiPage This is a boolean variable, if it is true, the macro will display
					 3 months at a time, other wise 1 month.
	@author Kaushik Hazra
-->
<#macro customdialogdate dialogId header dateInputFieldId callingButtonId multiPage>
	<script type="text/javascript">
			
		function calendarSelectHandler_${dialogId} (type,args,obj){
			var dates = args[0];
			var date = dates[0];
			var year = date[0], month = date[1], day = date[2];
			
			var dateInputField = document.getElementById("${dateInputFieldId}");
			dateInputField.value = year + "-" + month + "-" + day;
			YAHOO.simpledialog.${dialogId}.hide();
			dateInputField.onchange();
		}
		
		function init(){
			YAHOO.namespace("simpledialog");
			
			var escKeyHandle = function(e) {
				this.hide();
			}
			
			var showDateDialog = function(e){
				YAHOO.simpledialog.${dialogId}.show();
			}
			
		
			YAHOO.simpledialog.${dialogId} = new YAHOO.widget.SimpleDialog("${dialogId}", { visible:false, zIndex:200, modal: true, fixedcenter:true, draggable:false });
	
			YAHOO.simpledialog.${dialogId}.setHeader("<@s.text name='${header}'/>");	
			YAHOO.simpledialog.${dialogId}.setBody("<div id='calContainer_${dialogId}'></div>");
			
			var listeners = new YAHOO.util.KeyListener(document, { keys : 27 }, {fn:escKeyHandle ,scope:YAHOO.simpledialog.${dialogId}, correctScope:true} );
			YAHOO.simpledialog.${dialogId}.cfg.queueProperty("keylisteners", listeners);
			
			YAHOO.simpledialog.${dialogId}.render(document.body);
			YAHOO.util.Event.addListener(document.getElementById("${callingButtonId}"), "click", showDateDialog);
			
			<#if multiPage == "true">
				YAHOO.simpledialog.calendar_${dialogId} = new YAHOO.widget.CalendarGroup("cal","calContainer_${dialogId}", { pages:3} );
			<#else>
				YAHOO.simpledialog.calendar_${dialogId} = new YAHOO.widget.Calendar("cal","calContainer_${dialogId}");
			</#if>
			
			YAHOO.simpledialog.calendar_${dialogId}.cfg.setProperty("MONTHS_LONG", [
																"<@s.text name='calender.month.long.january'/>",
																"<@s.text name='calender.month.long.february'/>",
																"<@s.text name='calender.month.long.march'/>",
																"<@s.text name='calender.month.long.april'/>",
																"<@s.text name='calender.month.long.may'/>",
																"<@s.text name='calender.month.long.june'/>",
																"<@s.text name='calender.month.long.july'/>",
																"<@s.text name='calender.month.long.august'/>",
																"<@s.text name='calender.month.long.september'/>",
																"<@s.text name='calender.month.long.october'/>",
																"<@s.text name='calender.month.long.november'/>",
																"<@s.text name='calender.month.long.december'/>"
														]);
			YAHOO.simpledialog.calendar_${dialogId}.cfg.setProperty("WEEKDAYS_SHORT", 	[
															    "<@s.text name='calender.day.short.sunday'/>",
																"<@s.text name='calender.day.short.monday'/>",
																"<@s.text name='calender.day.short.tuesday'/>",
																"<@s.text name='calender.day.short.wednesday'/>",
																"<@s.text name='calender.day.short.thursday'/>",
																"<@s.text name='calender.day.short.friday'/>",
																"<@s.text name='calender.day.short.saturday'/>"
														]);
			YAHOO.simpledialog.calendar_${dialogId}.selectEvent.subscribe(calendarSelectHandler_${dialogId}, YAHOO.simpledialog.calendar_${dialogId}, true);		
			YAHOO.simpledialog.calendar_${dialogId}.render();		
																											

		}
		
		
		YAHOO.util.Event.addListener(window, "load", init);
												
	</script>
</#macro>