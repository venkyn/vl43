<#macro chartDataFields id name chartId chartDataSourceId readonly>
    <div id="${id}Div" style="width: 700px">
    </div>
    
    <@s.hidden id="${id}" name="${id}"/>
    
    <script type="text/javascript">
        $j(function(){
           chartDataFields = new ChartDataFields({
                                                'id':'${id}',
                                                'container_id':${id}Div,
                                                'chart_id':'${chartId}',
                                                'chart_data_source_id':'${chartDataSourceId}',
                                                'chart_data_fields':${chartDataFieldProperties},
                                                'read_only':${readonly},
                                              });
           chartDataFields.draw();
           <#if navigation.pageName="edit">
	           connectSubmit();
	       </#if>
        });
    </script>
</#macro>

