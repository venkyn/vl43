<#assign authzx=JspTaglibs["/WEB-INF/authzx.tld"]>
<#-- 
	testing this change
	Macro to be used to build a link based on access to a particular feature.
	@param accessLink - The link that is being secured.
	@param linkText - The text shown in the link.
	@param thisFeatureName - The apache feature name.
	@param tid - An id for testing the link.
	@param id - A unique link id for creating unique JS function and handler.
	@author Jim Geisler
-->
<#macro securelink accessLink linkText thisFeatureName tid id>
	<#assign isAccessible=false>
	<script type="text/javascript">
		function handler${id}() {
			// we don't need to do anything here
		}
		function showSecurityWarning${id}() {
			var dialogProperties = {
				    title: "<@s.text name='security.noaccess.title'/>",			    
					body: "<@s.text name='nav.action.message.permissions'/>" ,
				    button1: "<@s.text name='security.noaccess.OK'/>"}
			buildCustomDialog(dialogProperties,handler${id},"");
		}
	</script>
	<@authzx.authorizeByFeature featureName=thisFeatureName>
		<#assign isAccessible=true>
	</@authzx.authorizeByFeature>
	<#if isAccessible>
		<a href="${accessLink}" tid="${tid}">${linkText}</a>
	<#else>
		<a href="javascript:showSecurityWarning${id}();" tid="${tid}">${linkText}</a>
	</#if>
</#macro>