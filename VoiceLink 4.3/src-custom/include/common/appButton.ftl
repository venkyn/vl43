<#macro appButton url text id class>
<table class="appButton">
  <tr>
    <td class="app_left" valign="top">
    	<img src="${base}/images/appHeader/app_left<#if class=="select">_select</#if>.png" border="0"/>
    </td>
    <td class="app_middle">
		<a <#if class=="select">class="select"</#if> href="<@s.url includeParams='none'  value='${url}'/>" id="${id}">
			<@s.text name='${text}'/>
		</a>
	</td>
	<td class="app_right" valign="top">
    	<img src="${base}/images/appHeader/app_right<#if class=="select">_select</#if>.png" border="0"/>
	</td>
  </tr>
 </table>
</#macro>

<#--
  This macro instantiates a standard application tab.
  @param text The message key of the visible text for the tab's link.
  @param name The name of the first level of the URL (i.e. selection).
  @param uri  The URI that the tab directs to.
  @param accessible Boolean value of whether this tab may be accessed.
-->
<#macro appTab text name uri accessible>
<li id="${name}-tab" 
    <#if name==navigation.applicationMenu>
        class="select"
    </#if>
    <#if !accessible>
        class="disabledTab"
    </#if>
>
    <#if accessible>
        <a href="<@s.url includeParams='none'  value='${uri}' />"><@s.text name=text /></a>
    <#else>
        <a href="javascript:void(0)" ><@s.text name=text /></a>
        <script type="text/javascript" >
            
            function createBubbleForAppButton() {
	            var detailsBubbleContainer = createDetailsBubbleContainer('${name}');
	
	            <#-- Connect events to the tab -->
			 	connect('${name}-tab', 'onmouseover', showNoAccessMessage);
			 	connect('${name}-tab', 'onmouseout', hideNoAccessMessage);
	
		 		appendChildNodes('container', detailsBubbleContainer);
	
	            <#-- Position the bubble relative to the tab. -->
			 	var tabPosition = elementPosition('${name}-tab');
	
			 	tabPosition.x = parseInt(tabPosition.x);
			 	tabPosition.x -= 155;
			 	tabPosition.y += 23;
			 	setElementPosition('${name}-tab-bubble', tabPosition);
		 	}
		 	
		 	<#-- This JS must be called after the DOM is finished initializing. -->
		 	connect(window, 'onload', createBubbleForAppButton);

        </script>
    </#if>
</li>
</#macro>

