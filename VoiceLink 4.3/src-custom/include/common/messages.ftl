<div id="messages" class="messageContainer" align="center">
<#assign sessionMsgs = sessionActionMessages />
<#if sessionMsgs?has_content>
	<#if sessionMsgs["global"]?has_content>
		<#assign sessionMessages = sessionMsgs["global"]>
		<#list sessionMessages as sessionMessage>
			<span class="actionMessage">${sessionMessage}</span><br/>
		</#list>
	</#if>
	<#if sessionMsgs["error"]?has_content>
		<#assign sessionErrorMessages = sessionMsgs["error"]>
		<#list sessionErrorMessages as sessionErrorMessage>
			<span id="viewDetailsLink" class="actionMessage">${sessionErrorMessage}
		    <#if sessionMsgs["more_info"]?has_content>
               <a href="javascript:void(0);">
                  <@s.text name='status.view.details'/>
               </a>
            </#if>
			</span><br/>
		</#list>
		<#assign setMessageDivToError = true>
	</#if>
	<#-- If there is a partial success message show the first element in the list as 
	     the general message, this implementation may change.-->
	<#if sessionMsgs["partial_success"]?has_content>
		<#assign sessionErrorMessages = sessionMsgs["partial_success"]>
		    <div class="partial">		
		    <span id="viewDetailsLink" class="actionMessage">${sessionErrorMessages[0]}
		    <#if sessionMsgs["more_info"]?has_content>
		       <a href="javascript:void(0);">
				  <@s.text name='status.view.details'/>
			   </a>
			</#if>
			</span></div>
			<br/>			
	</#if>
</#if>
<#if actionMessages?has_content>
	<#list actionMessages as actionMessage>
		<span class="actionMessage">${actionMessage}</span><br/>
	</#list>
</#if>
<#if actionErrors?has_content>
	<#list actionErrors as actionError>
		<span class="actionMessage">${actionError}</span><br/>
	</#list>
	<#assign setMessageDivToError = true>
</#if>
<div id="messageX" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='${base}/images/close_x_<#if setMessageDivToError?has_content && setMessageDivToError>white<#else>black</#if>.png', sizingMethod='scale');" class="<#if setMessageDivToError?has_content && setMessageDivToError>white<#else>black</#if>">
</div>
</div>

<#-- If there is a partial success message create a div for showing the bubble with the 
     list of detail messages, also generate the required java scripts to handle the
     display of  the message bubble -->
 <#if sessionMsgs?has_content>
   <#if sessionMsgs["more_info"]?has_content>
		<#assign sessionErrorMessages = sessionMsgs["more_info"]>
		   <div id="detailContentsDiv" class="midContentsBubble"  style="visibility:hidden">
			  <span>
			     <ul>
				   <#list sessionErrorMessages as sessionErrorMessage>	
				      <#if sessionErrorMessage_index != 0>
					    <li>
					       ${sessionErrorMessage}<br/>								
					    </li>
					  </#if>
				   </#list>
				 </ul>
			  </span>
			</div>	
		
   <script type="text/javascript">	
	   connect(window, "onload", gen_ViewDetailsForSessionMessage);	
	   connect($('viewDetailsLink'),"onclick",toggleViewDetailsForSessionMessage);
	   connect(window, "onclick", closeViewDetailsForSessionMessage);
   </script>
		    
   </#if>
 </#if>

<script type="text/javascript">
	
	<#if setMessageDivToError?has_content && setMessageDivToError>
		addElementClass("messages", "error");
	</#if>
	connect(window, "onscroll", resetMessagesDiv);
	connect(window, "onresize", resetMessagesDiv);
	vdConnections["messageX"] = connect("messageX", "onclick", clearMessagesDiv);
	
	resetMessagesDiv();
	
</script>
