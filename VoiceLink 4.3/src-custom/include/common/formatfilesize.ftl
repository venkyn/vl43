<#assign authzx=JspTaglibs["/WEB-INF/authzx.tld"]>
<#-- 
	Macro definition of an action for the actions menu. I know, i'm JavaDoc'ing
	in a place that doesnt really do that... but at least everyone knows the syntax.
	Note: I reused existing locale resource keys that were already in use elsewhere.
	@param sizeInBytes The size in bytes of a file.  Make sure to use ?c to computer-ify the number.
	@author Tim Reed
-->
<#macro formatfilesize sizeInBytes>
	<#assign size=sizeInBytes?number>
	
	<#--Set up kilobyte and megabyte numbers-->
	<#assign kilobyte=1024>
	<#assign megabyte=kilobyte * kilobyte>
	
	<#--Assume bytes at beginning-->
	<#assign displaySize=size>
	<#assign sizeUnits>
			<@s.text name='logs.view.column.size.suffix.bytes'/>
	</#assign>
	
	<#--Check if larger than megabyte or kilobyte-->
	<#if size &gt;= megabyte>
		<#assign displaySize=size/megabyte>
		<#assign sizeUnits>
			<@s.text name='logs.view.column.size.suffix.MB'/>
	 	</#assign>
	<#elseif size &gt;= kilobyte>
		<#assign displaySize=size/kilobyte>
		<#assign sizeUnits>
			<@s.text name='logs.view.column.size.suffix.KB'/>
	 	</#assign>
	</#if>
	
	<#--Output the formated localized size and units-->
	${displaySize} ${sizeUnits}
</#macro>
