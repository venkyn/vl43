<#assign authzx=JspTaglibs["/WEB-INF/authzx.tld"]>
<#-- 
    Macro definition of an action for the actions menu. I know, i'm JavaDoc'ing
    in a place that doesnt really do that... but at least everyone knows the syntax.
    @param name The resource key for the name of the action to display to the user.
    @param tableId The ID of the table this action is sensitive to.
    @param id The ID to use in the HTML for the action's anchor element.
    @param url The URL the action will point to. May be JavaScript HREF.
    @param tabIndex The tab index, should begin at 251 and increment as you add actions.
    @param actionEnablers A hash of [function name]->[message key], used for checking
    wether an action should be disabled and what message it should use to present to
    the user.
    @param featureName The string identifier of the feature to test for security
    purposes. Also, the words 'always' and 'never' can be used to override any
    security checks.
    @author Brian Rupert
-->
<#macro action name tableId id url tabIndex actionEnablers featureName>
    <li>
        <@s.url includeParams='none'  value=url id='internalUrl'/>
        <#assign isAccessible=false>
        <@authzx.authorizeByFeature featureName=featureName>
            <#assign isAccessible=true>
        </@authzx.authorizeByFeature>
        <#if !isAccessible>
        <div>
            <a class="disabledAction" tabindex="${tabIndex}" href="javascript:void(0);" id="${id}">
                <@s.text name=name />
            </a>
            <div class="lowerEdge"></div>
            <div class="actionBubbleContainer" id="${id}_bubble">
                <div class="actionBubbleCarrot"></div>
                <div class="actionBubble actionBubbleTop"></div>
                <div class="actionBubble actionBubbleMiddle">
                    <div class="actionBubbleContents" id="${id}_msg">
                        <@s.text name="nav.action.message.permissions" />
                    </div>
                </div>
                <div class="actionBubble actionBubbleBottom"></div>
            </div>
        </div>
        <script type="text/javascript">
            var actionsMenu = getActionsMenu();
            connect('${id}', 'onmouseover', actionsMenu, 'showActionBubble');
            connect('${id}', 'onmouseout', actionsMenu, 'hideActionBubble');
        </script>
        <#elseif actionEnablers?has_content>
        <div style="position:relative">
            <a class="disabledAction" tabindex="${tabIndex}" href="javascript:verifyActionURL('${id}','${internalUrl}')" id="${id}">
                <@s.text name=name />
            </a>
            <div class="lowerEdge"></div>
            <div class="actionBubbleContainer" id="${id}_bubble">
                <div class="actionBubbleCarrot"></div>
                <div class="actionBubble actionBubbleTop"></div>
                <div class="actionBubble actionBubbleMiddle">
                    <div class="actionBubbleContents" id="${id}_msg"></div>
                </div>
                <div class="actionBubble actionBubbleBottom"></div>
            </div>
        </div>
        <script type="text/javascript">
            var actionsMenu = getActionsMenu();
            connect('${id}', 'onmouseover', actionsMenu, 'showActionBubble');
            connect('${id}', 'onmouseout', actionsMenu, 'hideActionBubble');
            <#assign keys = actionEnablers?keys>
            <#list keys as enablerName>
              <#assign disabledText>
                <@s.text name=actionEnablers[enablerName]/>
              </#assign>
              <#if tableId?has_content>
              actionsMenu.addActionEnabler('${tableId}', '${id}', ${enablerName}, "${disabledText?html}");
              <#else>
              actionsMenu.addActionEnabler('alwaysRun', '${id}', ${enablerName}, "${disabledText?html}");
              </#if>
            </#list>
        </script>
        <#else>
        <div>
            <a class="enabledAction" tabindex="${tabIndex}" href="${internalUrl}" id="${id}"><@s.text name=name /></a>
            <div class="lowerEdge"></div>
        </div>
        </#if>
    </li>
</#macro>
