<#macro alertCriteria id name alertId alertOnId readonly>
    <div id="${id}Div" style="width: 700px">
    </div>
    
    <@s.hidden id="${id}" name="${id}"/>
    
    <script type="text/javascript">
        $j(function(){
           alertCriteria = new AlertCriteria({
                                                'id':'${id}',
                                                'container_id':${id}Div,
                                                'alert_id':'${alertId}',
                                                'alert_on_id':'${alertOnId}',
                                                'criteria_properties':${criteriaProperties},
                                                'read_only':${readonly},
                                                'error_message':${criteriaErrors}
                                              });
           alertCriteria.draw();
           <#if navigation.pageName="edit">
	           connectSubmit();
	       </#if>
        });
    </script>
</#macro>

