<#-- Include links to VL-specific global JS files that are needed in the head.ftl decorator -->
<#-- Application module specific GUI functions -->
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_functions-voicelink.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_functions-voicelink-cyclecounting.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_functions-voicelink-lineloading.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_functions-voicelink-loading.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_functions-voicelink-putaway.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_functions-voicelink-puttostore.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_functions-voicelink-replenishment.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_functions-voicelink-selection.js"></script>

<#-- Application Module specific painter functions -->
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_painters-voicelink.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_painters-voicelink-cyclecounting.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_painters-voicelink-lineloading.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_painters-voicelink-loading.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_painters-voicelink-putaway.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_painters-voicelink-puttostore.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_painters-voicelink-replenishment.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_gui_painters-voicelink-selection.js"></script>

<#--Misc Functions -->
<script type="text/javascript" src="${base}/scripts/vocollect/voc_prompt_options-voicelink.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_prompt_selector-voicelink.js"></script>
