<#-- 
    Macro definition of a table component.
    @param columns a list of Column objects necessary to build the column headers
        and point to the appropriate painter functions for the column content.
    @param tableId the id of the table
    @param tableTitle the localized title text
    @param url url called to retrieve data for rendering
    @param varName variable name used to refer to this tabled component
        within javascript
    @param type specify "summary" for summary views on the home page; append "_time" to show time-filtering dropdown
    @param extraParams hash of additional optional parameters that can be
        set on the table component
    @param viewId id of a specific view, specifically, a 'named' set of
        columnar data
        
    @author Jim Geisler
-->
<#if !column_map?has_content>
    <#assign column_map = jSONColumns />
</#if>
<#macro tablecomponent columns tableId url tableTitle varName type extraParams viewId>
    <#if !type?contains('summary')>
        <script type="text/javascript">
            if ($j('#canvas').length == 0) {
                $j('#content').append($j('<div id="canvas" />'));
            } else {
                $j('#canvas').height($j('#canvas').height() + 610);
            }
            
            $j('#canvas').append($j('<div id="${tableId}Div" />'));
        </script>
    </#if>
    <script type="text/javascript">
    <#-- Freemarker thought this was a number on some screens
         and not a number on other screens, Freemarker threw exceptions
         when using the "c" operator, but you need that if it is a number. -->
    <#if viewId?is_number>
        <#assign viewIdString = viewId?c>
    <#else>
        <#assign viewIdString = viewId>
    </#if>

    <#if !urlMethod?has_content>
        <#assign urlMethod = "getData">
    </#if>
    
    <#-- Grab the extraParams hash and do what we need with it -->
    <#if extraParams?has_content>
        <#assign keys = extraParams?keys>
        <#list keys as key>
            <#if key = "publishers">
                <#assign publishingTables = extraParams[key]>                
            <#elseif key = "extraURLParams">
                <#assign extraURLParams = extraParams[key]>
            <#elseif key = "rowHighlightCondition">
                <#assign rowHighlightCondition = extraParams[key]>
            <#elseif key = "rowHighlightTwoConditions">
                <#assign rowHighlightTwoConditions = extraParams[key]>
            </#if>
        </#list>
    </#if>
    
    
    <#-- Populates the JavaScript object with which tables to listen for row selection from -->
    var publishers = [];
    <#if publishingTables?has_content>
        <#list publishingTables as table>
            publishers.push({"name":"${table.name}"
            				<#if table.selector?has_content>,"selector":"${table.selector}"</#if>
            				<#if table.selectorFunction?has_content>,"selectorFunction":${table.selectorFunction}</#if>
            				});
        </#list>
    </#if>
     
    <#-- Populates the JavaScript object with extraURLParams -->
    var extraURLParams = [];
    <#if extraURLParams?has_content>
        <#list extraURLParams as extraURLParam>
            extraURLParams.push({"name":"${extraURLParam.name}","value":"${extraURLParam.value}"});
        </#list>
    </#if>
    
    <#-- These two might be able to be re-factored to make it more general for now it is what it is
         if there are two conditions both must be true to highlight -->
    <#-- Highlights rows based on a single field matching a value -->
    var rowsToHighlight = [];
    <#if rowHighlightCondition?has_content>
        <#list rowHighlightCondition as table>
            rowsToHighlight.push({"field":"${table.field}",
                                  "value":"${table.value}"});
        </#list>
    </#if>
    <#-- Highlights rows based on a two fields matching two values -->
    <#if rowHighlightTwoConditions?has_content>
        <#list rowHighlightTwoConditions as table>
            rowsToHighlight.push({"field":"${table.field}",
                                  "value":"${table.value}",
                                  "field1":"${table.field1}",
                                  "value1":"${table.value1}"});
        </#list>
    </#if>
    
    <#-- TODO: Would like to refactor this. Leaving it for now. -->
    var filterCriterion = new Array();
    <#if filters?has_content>
            <#list filters as filter>
                <#if filter?has_content>
                    <#if filter.view.id?c == viewIdString>
                        <#list filter.filterCriteria as filterCriteria>
                            vocFilterCriteria = { 
                                viewId: "${filter.view.id?c}",
                                <#if filterCriteria.id?exists>
                                    id: "${filterCriteria.id?c}",
                                </#if>
                                columnId: "${filterCriteria.column.id?c}", 
                                operandId: "${filterCriteria.operand.id?c}",
                                <#if filterCriteria.value1?exists>
                                    value1: "${filterCriteria.value1}",
                                <#else>
                                    value1: null,
                                </#if>
                                <#if filterCriteria.value2?exists>
                                    value2: "${filterCriteria.value2}",
                                <#else>
                                    value2: null,
                                </#if>
                                <#if filterCriteria.locked>
                                    locked: true
                                <#else>
                                    locked: false
                                </#if>
                            }; 
                            filterCriterion.push(vocFilterCriteria);
                        </#list>
                    </#if>
                </#if>
            </#list>
        </#if>
        
    var time_filter = null;
    <#if (type?ends_with("_time"))>        
        <#if timeFilterSaved?exists >
        time_filter = ${timeFilterSaved};
        <#else>
        time_filter = 1;
        </#if>
    </#if>
    
    var ${varName};
    ${varName} = new TableComponent({
        id: '${tableId}',
        name: '${varName}',
        columns: $j.parseJSON('${column_map}')['${viewIdString}'],
        viewId: '${viewIdString}',
        url: '${url}/${urlMethod}.action',
        type: '${type}',
        title: '<@s.text name=tableTitle/>',
        filters: filterCriterion,
        publishers : publishers,
        printUrl: '${url}/${urlMethod}ForPrint.action',
        autoCompleteUrl: '${url}/asyncGetFilterAutoCompleteData.action',
        timeFilter: time_filter,
        extraURLParams: extraURLParams,
        rowsToHighlight: rowsToHighlight
    });
    
    $j(document).ready(function() {

        // Fire off table components when everything is ready
        $j(document).bind(events.GO_TABLE_COMPONENT, function() {
            ${varName}.init(function() {
                $j(document).trigger(events.TABLE_COMPONENT_LOADED, ['${varName}']);
            });
        });
        
        // Fire table component event so the appropriate pre-req's can occur
        $j(document).trigger(events.TABLE_COMPONENT_PAGE, ['${varName}']);
    });

    </script>
</#macro>