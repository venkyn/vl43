
<#-- 
	Macro definition of a custom dialog.
	@param dom element to insert the dialog (typically document.body)
	@param body the text of the confirmation message.
	@param button1 the text for the first button
	@param button2 the text for the second button
	@param result1 the function to call when first button is pressed
	@param result2 the function to call when the second button is pressed
	
	@author Brent Nichols
-->
<#macro customdialogdropdown dialogId header body button1 button2 result1 result2>

	<script type="text/javascript">

	YAHOO.namespace("simpledialog");

	function init() {
		var handle1 = function(e) {
			this.hide();
			${result1};
		}

		var handle2 = function(e) {
			this.hide();
			<#if result2 != "">
				${result2};
			</#if>
		}		

		YAHOO.simpledialog.${dialogId} = new YAHOO.widget.Dialog("${dialogId}", { visible:false, zIndex:200, modal: true, fixedcenter:true, draggable:false });
		
		YAHOO.simpledialog.${dialogId}.setHeader("<@s.text name='${header}'/>");
		
		YAHOO.simpledialog.${dialogId}.setBody(" ");

		YAHOO.simpledialog.${dialogId}.cfg.queueProperty("buttons", [ 
										{ text:"<@s.text name='${button1}'/>", handler:handle1 },
										{ text:"<@s.text name='${button2}'/>", handler:handle2, isDefault:true }
									]);

		var listeners = new YAHOO.util.KeyListener(document, { keys : 27 }, {fn:handle2 ,scope:YAHOO.simpledialog.${dialogId}, correctScope:true} );
		YAHOO.simpledialog.${dialogId}.cfg.queueProperty("keylisteners", listeners);

		YAHOO.simpledialog.${dialogId}.render(document.body);
	}

	YAHOO.util.Event.addListener(window, "load", init);

	</script>

</#macro>
