<#-- 
	Macro definition of a configuarablehomepage.
	@param div
    	
	@author 
-->
<#include "/include/common/updatingsummary.ftl">
<#include "/include/common/tablecomponent.ftl">
<#include "/include/common/normalsummary.ftl">
<#include "/include/common/customdialog.ftl">
    
<#macro configurablehomepage  summaries summariesLocation rows columns homepageId>

<div id="panelContainer" style="position:relative;">
</div>
<#assign addSummaryText>
		<@s.text name='add.a.summary'/>
</#assign>

<script type="text/javascript">
   var addASummary = "${addSummaryText}";
   var basePath = "${base}";
   var pnlTable = new Table('panelContainer', ${rows}, ${columns},${homepageId},${currentSiteID?c});
   var numTables = 0;
</script>
	
	 <#if summaries?has_content>
         <#assign keys = summaries?keys>
		   <#list keys as summaryId>
		   	<#assign type="${summaries[summaryId].getDescriptiveText()}"/>
        	 
        	 <#if type = "Summary">
               <@normalsummary 
                      summaries[summaryId] />
                    
                <#assign titleText>
		         <@s.text name="${summaries[summaryId].title}"/>
                </#assign>
                      <script type="text/javascript">
                         pnlTable.setArea("${summaries[summaryId].title}" , ${summariesLocation[summaryId]}, false, ${summaryId});
                      </script>
             
             <#elseif type = "TabularSummary">  
              
               <#assign tableId ="list"+summaries[summaryId].tableId+"Table" />
          
               <#assign titleText ="${summaries[summaryId].title}"/>
               
               <#if summariesLocation[summaryId]?has_content>
                       <#assign first  = summariesLocation[summaryId]?first/>
                       <#if (summariesLocation[summaryId]?size > 0) >
                          <#assign lasts  = summariesLocation[summaryId]?last/>
                          <#assign tableheight =lasts.substring(0,1)?number-first.substring(0,1)?number +1 >  
                        <#else>
                          <#assign tableheight =first.substring(0,1)?number  >  
                        </#if>
               </#if>
               <#if tableheight == 1>
                   <#assign tableheight=125>
               <#elseif tableheight == 2>
                   <#assign tableheight=360>
               <#else>
                   <#assign tableheight=595>
               </#if>
               <#if summaries[summaryId].extraParams?has_content>
                   <#assign extraParamsHash = {"Height":tableheight} + summaries[summaryId].extraParams>
               <#else>
	               <#assign extraParamsHash = {"Height":tableheight}>
               </#if>   
                <#assign tableObj=summaries[summaryId].tableId+"Obj">   
                <#if summaries[summaryId].actionMethod?has_content> 
				    <#assign urlMethod = summaries[summaryId].actionMethod>
                </#if>
                <#if summaries[summaryId].timeWindow?has_content && summaries[summaryId].timeWindow!=0>
                	<#assign summaryType = "summary_time"/>
                	<#assign timeFilterSaved = summaries[summaryId].timeWindow/>
                <#else>
                	<#assign summaryType = "summary"/>
                </#if>
                 
                <#assign tableDiv=tableId+'Div'>
                
                <#if !currentSiteID?has_content> 
					<#assign currentSiteID = -927 />
				</#if>
				<!-- if there is only 1 site, get all the sites -->
				<#if numberOfSites = 1>
					<#assign currentSiteID = -927 />
				</#if>

	           <script type="text/javascript">
	           pnlTable.setArea("${tableId}" , ${summariesLocation[summaryId]},true, ${summaryId});
	           </script>
	                   
               <@tablecomponent
   		               varName=tableObj
    	               columns=summaries[summaryId].columns
                       tableId=tableId
                       url="${base}${summaries[summaryId].actionUrl}"
                       tableTitle=titleText
                       type=summaryType
                       extraParams={"extraURLParams":[{"name":"tempSiteID","value":"${currentSiteID?c}"}]}
                       viewId="${summaries[summaryId].viewId?c}"/>	                   

                      <script type="text/javascript">
                      	${tableObj}.post_format = function() {
	                      	 numTables += 1;
	                         tblDiv = $j('#${tableDiv}');
	                		 tblDiv.width('100%');
	                		 tblDiv.find('.table_component_summary').height(tblDiv.find('.table_component_summary').height() - 60);
	                		 tblDiv.find('.slick-viewport').height(tblDiv.find('.slick-viewport').height() - 60);
	                		 this.grid.resizeCanvas();
							 parsed = parseArray(${summariesLocation[summaryId]});
							 pnlTable.cellvarname[parsed[0]-1][parsed[1]-1] = this;
				        };
	                   </script>                       

             <#elseif type = "UpdatingSummary">
               <@updatingsummary 
                       updatingSummary=summaries[summaryId]
                       div="" />
                       
                  <#assign titleText ="${summaries[summaryId].title}"/>
                       
                      <script type="text/javascript">
                         pnlTable.setArea("${summaries[summaryId].title}" , ${summariesLocation[summaryId]}, false, ${summaryId});
                      </script>
            </#if>
       	 </#list>
	  </#if>
	
<script type="text/javascript">
var num_loaded = 0;
$j(document).bind(events.TABLE_COMPONENT_LOADED, function() {
	num_loaded++;
	if (num_loaded == ${summaries?size}) {
	pnlTable.displayTable();
}
});
// Code added to handle the scenario of closing all summary screens.
if (num_loaded == ${summaries?size} && num_loaded == 0) {
	pnlTable.displayTable();
}
</script>

<@customdialog
	dialogId="div_addSummary_confirm"
	header="select.a.summary.confirmation.header"
	body=""
	button1="add.this.summary.ok.button"
	button2="cancel.this.summary.cancel.button"
	result1="javascript:performActionOnSummarySelected('${base}/addSummary.action');"
	result2=""/>


<@customdialog
	dialogId="div_deleteSummary_confirm"
	header="remove.a.summary.confirmation.header"
	body="delete.body.summary"
	button1="delete.yesText.summary"
	button2="delete.noText"
	result1="javascript:removeSummary('${base}/admin/removeSummary.action');"
	result2=""/>
</#macro>




