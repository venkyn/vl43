<#-- 
	Macro definition of a normalsummary .
	@param sourceFile - a reference to an included freemarker file 
	
	@author 
-->

<#macro normalsummary summary>
    <div id="${summary.title}Div">
	
	   <#if summary.sourceFile?has_content>
       		<#include "${summary.sourceFile}">
       </#if>
       
     </div>
     
</#macro>


