<#-- 
	Macro definition of a updating summary.
	@param div - div to which the summary is appended 
	@param updatingSummary - UpdatingSummary object which contains refreshrate 
	                         and actionUrl to get the data
		
	@author 
-->    
<#macro updatingsummary div updatingSummary>

	<div id="${updatingSummary.title}Div">
	</div>

   <div id="reportError">
   </div>
   
   
<script type="text/javascript">
   function getData(request){
     
      if(request.responseText == ""){
      	   return;
	      } else {
          element = document.getElementById("${updatingSummary.title}-contentDiv") ;
	      if(element== null )
	      {
	        var contentDiv = DIV({'id':'${updatingSummary.title}-contentDiv'},null);
	        var mainDiv = document.getElementById("${updatingSummary.title}Div")
	        appendChildNodes(mainDiv,contentDiv);
	      }
	       document.getElementById("${updatingSummary.title}-contentDiv").innerHTML = request.responseText;
	    }
	    
     }
  
  <#if updatingSummary?has_content>
	   <#assign refreshRate ="${updatingSummary.refreshRate}"/>
       <#assign actionUrl="${updatingSummary.actionUrl}"/>
       <#if updatingSummary.actionMethod?has_content>
           <#assign actionMethod = "${updatingSummary.actionMethod}"/>
       <#else>
		   <#assign actionMethod = "getData"/>
       </#if>       
       <#assign title="${updatingSummary.title}"/>
        <#if ( refreshRate?number > 0 )>
          <#if !(actionUrl=='')>
                 getDataForUpdatingSummary(${refreshRate},"${base}${actionUrl}/${actionMethod}.action" );
          </#if>
        </#if>
      </#if>
</script>
</#macro>
