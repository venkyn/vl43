<#assign authzx=JspTaglibs["/WEB-INF/authzx.tld"]>

<#-- 
    Macro definition of cascading action menu. 
    Using Yahoo! Markup cascading menu.version 2.3.1.
    @param name- The resource key for the name of the cascading menu to display to the user.
    @param actions - the actions to be included in submenu.
    @author Swathi Voruganti
-->
<#macro cascadingMenu name actions>
<#assign subMenuName>
 <@s.text name="${name}" />
</#assign>
<li class="subNav">
        <a  href="#${subMenuName}" > ${subMenuName}</a>
           <ul>
                          ${actions}
           </ul>
         <div class="lowerEdge">  
       
 </li>

<script type="text/javascript" charset="utf-8">
            function Nav(navId) {               
                var navObj = $(navId);
                var currentMouseOverItem = null;
                var navIsOpen = false;
                var cachedTimeout;
                
                function waitThenAddHoverClass(event) {
                    clearTimeout(cachedTimeout);
                    if (event.src() != currentMouseOverItem) {
                        removeHoverClass(currentMouseOverItem);
                    }
                    currentMouseOverItem = event.src();
                    if (!navIsOpen) {
                        cachedTimeout = setTimeout(function() {addHoverClass(event.src());}, 150);
                    } else {
                        addHoverClass(event.src());
                    }

                }
                
                function addHoverClass(element) {
                    if (currentMouseOverItem == element) {
                        navIsOpen = true;
                        addElementClass(element, "hover");
                    }
                }

                function areYouMyMommy(parentElement, element) {
                    while(!!element && element != parentElement) {
                        element = element.parentNode;
                    }
                    return !!element;
                }
                
                function removeHoverClass(element) {
                    currentMouseOverItem = null;
                    if (element) {
                        removeElementClass(element, "hover");
                    }
                }
                    
                function resetNavStatus(event) {
                    var targetElement = event.target();
                    if (navIsOpen) {
                        clearTimeout(cachedTimeout);
                        if (!areYouMyMommy(navObj, targetElement)) {
                            cachedTimeout = setTimeout(function() {navIsOpen = false;removeHoverClass(currentMouseOverItem);}, 450);
                        }
                    }
                }               
                
                this.init = function(navId) {
                    navObj = navId ? $(navId) : navObj;
                    if (navObj) {
                        for (var i = 0 , j = navObj.childNodes.length; i < j; i++) {
                            var li = navObj.childNodes[i];
                            if (li.nodeType == 1 && li.tagName.toLowerCase() == 'li') {
                                MochiKit.Signal.connect(li, "onmouseover", waitThenAddHoverClass);
                            }
                        }
                        MochiKit.Signal.connect(document, "onmouseover", resetNavStatus);
                    }
                };
            };
       
        </script>
        
        <script type="text/javascript" charset="utf-8">
        
             YAHOO.util.Event.onContentReady("cascadingNav", function () {
              (new Nav()).init('cascadingNav');
            });
        </script>
 </#macro>