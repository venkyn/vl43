<#-- These lines are inserted from body snippets -->
<script type="text/javascript">
	function checkText(searchText) {
		if(searchText.length == 0) {
 	    	return false;     	
    	}
		document.searchForm.submit();
	}
</script>
		
<script type="text/javascript">
    <#-- Custom action enabler for context-sensitive help. -->
	function checkForHelp(tableComponent) {
		return (contextHelpUrl != undefined && contextHelpUrl != -1);
	}
	
	<#-- Opens the main help page. -->
	function openGlobalHelp() {
		window.open('${base}/<@s.text name='nav.help.urls.globalroot' />','globalHelp','');
	}
	<#-- Opens the main hardware help page. -->
	function openGlobalHardwareHelp() {
		window.open('${base}/<@s.text name='nav.help.urls.hardware.globalroot' />','globalHelp','');
	}
	
	<#-- Opens a context-sensitive help page. -->
	function openContextHelp() {
		window.open( "${base}/<@s.text name='nav.help.urls.contextbase' />" + contextHelpUrl,'globalHelp','');
	}
</script>
	    
<#-- Source: epp/src-custom/page/logging/viewLogs.ftl -->
<script type="text/javascript">

function viewLog(action){
	var waitMessage = '<@s.text name="wait.message"/>';
	window.location=action;
	writeStatusMessage(waitMessage, "waiting");
}
 
</script>
<#-- End viewLogs.ftl -->

<style type="text/css">
	a span {margin-left:20em;}
</style>

<script type="text/javascript">
	function submitAndAppendForm(formId, tableObject){
		formElement = $(formId);
		actionUrl = formElement.action;
		originalUrl = actionUrl;
		var objects = tableObject.getSelectedObjects();

		var ids = new Array();
		for (var i=0; i < objects.length; i++) {
			ids.push(objects[i].fileName);
		}		

		if( ids.length >= 1 ){
			actionUrl += "?selectedFiles=" + ids.join("&selectedFiles=");
			formElement.action = actionUrl;
			formElement.submit();			
		}
		formElement.action = originalUrl;
	}
</script>

<script type="text/javascript"> 
function disableAckNotification(tableComponent){
	if (tableComponent.getSelectedObjects ) {
		var selectedObjects = tableComponent.getSelectedObjects();
		for (var i=0; i < selectedObjects.length; i++) {
			if (selectedObjects[i].acknowledgedUser != undefined) {
				return (false);
			}
		}
	    return true;
	} else {
		return false;
	}
}
</script>

<#-- Source: /epp/src-custom/include/navigation/actionmenus/admin/roleActions.ftl -->
<script type="text/javascript">

function disableAdministratorEdit(selection) {
	    return true;
}
var deleteRoleURL = '${base}/admin/role/delete.action';


 function showDeleteRoleDialog() {
	var dialogProperties = {
		    title: "<@s.text name='delete.title'/>",			    
			body: "<@s.text name='delete.body.roles'/>" ,
		    button1: "<@s.text name='delete.yesText.roles'/>",
			button2: "<@s.text name='delete.noText'/>"}
	buildCustomDialog(dialogProperties,
		partial(performActionOnSelected, roleObj,deleteRoleURL), "");
}	

 function showDeleteCurrentRoleDialog() {
	var dialogProperties = {
		    title: "<@s.text name='delete.title'/>",			    
			body: "<@s.text name='delete.body.role'/>" ,
		    button1: "<@s.text name='delete.yesText.role'/>",
			button2: "<@s.text name='delete.noText'/>"}
	buildCustomDialog(dialogProperties,
		partial(redirectCurrentDelete,"",""), "");
}	
	

</script>
<#-- End roleActions.ftl -->

<#-- Source: /epp/src-custom/include/navigation/actionmenus/admin/siteActions.ftl -->
<script type="text/javascript">
	var deleteSiteURL = '${base}/admin/site/delete.action';
	var operatorLoginURL = '${base}/operatorLogin.action';

    function showDeleteSiteDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.sites'/>" ,
			    button1: "<@s.text name='delete.yesText.sites'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, siteObj, deleteSiteURL), "");
	}	

	function showDeleteCurrentSiteDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.site'/>" ,
			    button1: "<@s.text name='delete.yesText.site'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
		
    var viewTrustPageURL = '${base}/admin/site/getViewTrustDialog.action';
    function getViewTrust(action){
		window.open(action, 'trustWindow', 'height=400,width=680');
	}
    
    function enableOnOperatorEAP(tableComponent) {
	    if (tableComponent.getSelectedObjects()[0].credentialAssociation == 2) {
	        return true;
	    }
	    return false;
	}
	
	function enableOnOperatorEAPSingle() {
	    var retval = false;
	    <#if site?has_content>
	    <#if site.credentialAssociation?has_content>
	        var creds = ${site.credentialAssociation}
	        if (${site.credentialAssociation} == 2) {
                return true;
            }
        </#if> 
        </#if>
        return false;
    }
    
    function enableOnSiteEAP(tableComponent) {
	    if (tableComponent.getSelectedObjects()[0].credentialAssociation == 0) {
	        return true;
	    }
	    return false;
	}
	

	function enableForEAP() {
	    var retval = false;
	    <#if site?has_content>
	    <#if site.credentialAssociation?has_content>
	        var creds = ${site.credentialAssociation}
	        if (${site.credentialAssociation} == 0) {
                return true;
            }
        </#if> 
        </#if>
        return false;
    }
	
</script>
<#-- End siteActions.ftl -->

<#-- Source: /epp/src-custom/include/navigation/actionmenus/admin/userActions.ftl -->
<script type="text/javascript">
	
	var deleteUserURL = '${base}/admin/user/delete.action';

    function showDeleteUserDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.users'/>" ,
			    button1: "<@s.text name='delete.yesText.users'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(performActionOnSelected, userObj, deleteUserURL), "");
	}	

	 function showDeleteCurrentUserDialog() {
		var dialogProperties = {
			    title: "<@s.text name='delete.title'/>",			    
				body: "<@s.text name='delete.body.user'/>" ,
			    button1: "<@s.text name='delete.yesText.user'/>",
				button2: "<@s.text name='delete.noText'/>"}
		buildCustomDialog(dialogProperties,
			partial(redirectCurrentDelete,"",""), "");
	}	
		
</script>
<#-- End userActions.ftl -->

<#-- Source: /epp/src-custom/page/security/role.ftl -->
<#-- End role.ftl -->

<#-- Source: epp/src-custom/page/notification/notification.ftl -->
<script type="text/javascript">
/**
* Filler function for the row painter.
*/
function checkIfAck(rowElement, obj){
	if (obj.acknowledgedUser == undefined && obj.priority == "<@s.text name='notification.column.keyname.Priority.CRITICAL'/>") {
		addElementClass(rowElement, "unread");
	}
}
</script>
<#-- End notification.ftl -->

<#-- Source: epp/src-custom/page/reports/parameters.ftl -->
<#-- End parameters.ftl -->

<#-- Source: epp/src-custom/page/licensing/viewLicense.ftl -->
 <style type="text/css">
 	 table.viewLicenseTable { position:relative; }
     table.viewLicenseTable td{ font: 12px }
 </style>
<#-- End viewLicense.ftl -->

<#-- Source: epp/src-custom/include/common/appButton.ftl -->
<script type="text/javascript">
function createDetailsBubbleContainer(name) { 

	<#assign messagePermissions> <@s.text name='nav.action.message.permissions'/> </#assign>

	return DIV({'class':'detailsBubbleContainer'}, 
            		DIV({'class':'viewDetailsDiv', 
            	         'id':name + '-tab-bubble'}, 
            	        DIV({'class':'topBubble'}),
            	        DIV({'class':'midBubble',
            	             'style':'height:auto;margin-left:0;'}, 
            	             '${messagePermissions?js_string}'),
            	        DIV({'class':'botBubble'})
            	    )
                );
}
</script>
<#-- End appButton.ftl -->

<#-- Source: /epp/src-custom/page/systemConfiguration/systemConfiguration.ftl -->
<script language="javascript">

function displayObject(checkbox, obj) {
    obj = getObject(obj);
    if (obj == null) {
        return;
    }
    if(checkbox.checked)
    {
         obj.style.display =  'block';
         obj.style.visibility = 'visible';
    }
    else
    {
         obj.style.display = 'none';
         obj.style.visibility='hidden';
    }
}

function getObject(obj) {
    if (document.getElementById) {
        obj = document.getElementById(obj);
    } else if (document.all) {
        obj = document.all.item(obj);
    } else {
        obj = null;
    }
    return obj;
}

function changeValue(obj) {
	if ( obj.id == "smtpAuthenticationRequired" ) {
		hiddenAuthenticationRequired = getObject('hiddenSmtpAuthenticationRequired');
	} else {
		hiddenAuthenticationRequired = getObject('hiddenDirectoryServerAuthenticationRequired');
		if ( obj.checked == false ) {
			// If it isn't checked now - then it was checked previously - 
			// so go figure out how many users impacted
			var _url = "numberOfUsersAuthenticatingAgainstDirectoryServer.action";
		    doSimpleXMLHttpRequest(_url, {})
			    .addCallbacks(asyncNumberOfUsersCheckResponse, 
			            function (request) {
			                    reportError(request.responseText);
			            }
			    );
		} else {
			// it is checked now - so go ahead and remove the error label that
			// may have been created
			var labelDiv = getObject('wwctrl_directoryServerAuthenticationRequired');
   			var existingErrorLabel = getObject('changeAuthenticationErrorLabel');
   			if ( existingErrorLabel != null ) {
	   			labelDiv.removeChild(existingErrorLabel);
	   		}
   		}	
	}	

	if ( obj.checked == true ) {
		hiddenAuthenticationRequired.value = 'true';
	} else {
		hiddenAuthenticationRequired.value = 'false';
	}	
}

function asyncNumberOfUsersCheckResponse(request) {
	var labelDiv = getObject('wwctrl_directoryServerAuthenticationRequired');
   	var data = evalJSONRequest(request);
   	if(data != null) {
   		var existingErrorLabel = getObject('changeAuthenticationErrorLabel');
   		var newErrorLabel = 
   			LABEL({'id':'changeAuthenticationErrorLabel',
   				   'name':'changeAuthenticationErrorLabel',
   				   'class':'listErrorMessage'}, data.value);
		if ( existingErrorLabel == null ) {
			// We haven't had this before - so we need to create it
   			appendChildNodes(labelDiv, newErrorLabel);
   		} else {
   			// The label exists - replace it with the new one
   			labelDiv.replaceChild(existingErrorLabel, newErrorLabel);
   		}	
   	} 
}

function testConnectionToDirectoryServer() {
	var _url = "testConnectionToDirectoryServer.action";
	
	var hostName = getObject('directoryServerHost');
	var hostPort = getObject('directoryServerPort');
	var searchUser = getObject('directoryServerSearchUserName');
	var searchPass = getObject('directoryServerSearchPassword');
	var searchBase = getObject('directoryServerSearchBase');
	var searchAttr = getObject('directoryServerSearchableAttribute');
	var testUser = getObject('directoryServerTestUser');
		
    doSimpleXMLHttpRequest(_url, {directoryServerHost: hostName.value,
                                  directoryServerPort: hostPort.value,
				                  directoryServerSearchUserName: searchUser.value,
				                  directoryServerSearchPassword: searchPass.value,
				                  directoryServerSearchBase: searchBase.value,
				                  directoryServerSearchableAttribute: searchAttr.value,
				                  directoryServerTestUser: testUser.value
                               })
		.addCallbacks(asyncReturnOfTestConnectionToDirectoryServer, 
	            function (request) {
	                    reportError(request.responseText);
	            }
	    );
}

function asyncReturnOfTestConnectionToDirectoryServer(request) {
   	var data = evalJSONRequest(request);
   	if(data != null) {
   		var testAreaDiv = getObject('testArea');
		var label=getObject('testResults');   		
		var newErrorLabel = null;
   		
		if (data.connected=='true') {
	   		newErrorLabel = 
   				LABEL({'id':'testResults',
   			   	 	  'name':'testResults',
   			   	 	  'class':'listSuccessMessage'}, data.value);
   		} else {
	   		newErrorLabel = 
   				LABEL({'id':'testResults',
   			   	 	  'name':'testResults',
   			   	 	  'class':'listErrorMessage'}, data.value);
		}   		
   			   	 	  
		if (label != null) {
	   		testAreaDiv.removeChild(label);
	   	}
		appendChildNodes(testAreaDiv, newErrorLabel);
	}
}
<#-- Displays day warning -->
<#-- may want to change this to a pop up-->

var errorList = new Array();

function displayErrors(index) {
   
   errorDiv = getObject('purgeLimitError_' + index);
   
   var archiveValue;
   var purgeValue;
   
   // this may need to be changed later, to use $ syntax   
   purgeValue = document.getElementById("purgeValue_"+index).value;
   archiveElement = document.getElementById("archiveValue_"+index);
   archiveValue = 0;
   if (archiveElement) {
	   archiveValue = document.getElementById("archiveValue_"+index).value;
	}
   

    if (purgeValue > 7 && (archiveElement && archiveValue > 0))
    {           
         errorDiv.style.display =  'block';
         errorDiv.style.visibility = 'visible';
         errorList[index]=true;
         
    }
    else
    {         
         errorDiv.style.display = 'none';
         errorDiv.style.visibility='hidden';
         errorList[index]=false;
    }
}


function displayDayWarning(obj,purgeOrArchive,index) {
	warningDiv = getObject(purgeOrArchive + 'DayWarning_' + index);
	
    var dayValue=parseInt(obj.value);

    if(dayValue > 90)
    {
         warningDiv.style.display =  'block';
         warningDiv.style.visibility = 'visible';     
    }
    else
    {
         warningDiv.style.display = 'none';
         warningDiv.style.visibility='hidden';
    }
}


<#-- Called after each purge/archive line is rendered.
	Needed because ww.hidden disabled parameter doesn't work. -->
function hiddenEnable(hiddenFieldId,enabled) {
	hiddenField = getObject(hiddenFieldId);

	if ( enabled == 'true' ) {
		hiddenField.disabled = null;
	} else {
		hiddenField.disabled = 'true';
	}	
}

<#-- Called after each purge/archive line is rendered.
	Needed because xwork validation doesn't deal with fields of the same name
	nicely.  We need to make sure the form looks the same after a failed validation. -->
function undoValidationFieldPopulation(purgeOrArchive,index) {
	hiddenField = getObject(purgeOrArchive + 'Key_' + index);
	textbox = getObject(purgeOrArchive + 'Value_' + index);
	
	if ( hiddenField.disabled ) {
		textbox.value = "";
	}
}

</script>

<script type="text/javascript">
	function checkIfErrors(e){ 
		for( var i=0; i < errorList.length; i++) {	   
		   if(errorList[i] == true){			   
			   addElementClass(e.src(),"disabled");
			   setEnableSubmit(false);
			   break;
		   }
	    }
	}
</script>
<#-- End systemConfiguration.ftl -->

<#-- Source: /epp/src-custom/page/home.ftl -->
<script type="text/javascript">
		
		function paintAllRowsRed(tr, obj) {
			if(obj.tagId){
				addElementClass( tr, "unread" );
			}
		}
</script>
<#-- End home.ftl --> 

<#-- Source: /epp/src-custom/template/ajax/form.ftl -->
<script type="text/javascript" src="${base}/struts/validationClient.js"></script>
<script type="text/javascript" src="${base}/dwr/interface/validator.js"></script>
<#-- This file exists in the head <script type="text/javascript" src="${base}/dwr/engine.js"></script> -->
<script type="text/javascript" src="${base}/struts/ajax/validation.js"></script>
<#-- End form.ftl --> 

<#-- Source: /epp/web/decorators/nav.ftl -->
<script type="text/javascript">
			      	function handleSiteChange(evt) {
			             window.location='?currentSiteID='+evt.src().value;
			        }
</script>
<#-- End nav.ftl --> 

<#-- Source: /epp/src-custom/page/scheduling/schedules.ftl -->
<#-- End schedules.ftl --> 

<#-- These lines are inserted from body snippets -->

<#-- Source: epp/src-custom/common/include/messages.ftl -->
<script type="text/javascript">
function gen_ViewDetailsForSessionMessage(e){          
  var bubbleDiv = buildDetailsDiv($('detailContentsDiv'));       
  appendChildNodes($('viewDetailsLink'),bubbleDiv);    
  
  <#if sessionActionMessages?has_content>
	  <#assign sessionMsgs = sessionActionMessages />
	  <#if sessionMsgs["error"]?has_content>
	  	<#assign setMessageDivToError = true>
	  </#if>
  </#if>  
   
  <#if setMessageDivToError?has_content && setMessageDivToError>     
      writeStatusMessage($('viewDetailsLink'), "error"); 
  <#else>
      writeStatusMessage($('viewDetailsLink'), "partial");
  </#if>
}

function toggleViewDetailsForSessionMessage(e){   
	if (!e) var e = window.event
		e.cancelBubble = true;
	if (e.stopPropagation) e.stopPropagation();    
	
	if( $('detailContentsDiv').style.visibility == "visible" ){		    
    	$('viewDetailsDiv').style.visibility="hidden";
    	$('detailContentsDiv').style.visibility="hidden";
    }else{	        
    	$('viewDetailsDiv').style.visibility="visible";
    	$('detailContentsDiv').style.visibility="visible";
	}
}       
	
function closeViewDetailsForSessionMessage(e){   
		$('viewDetailsDiv').style.visibility="hidden";  
		$('detailContentsDiv').style.visibility="hidden";                 
}
function resetMessagesDiv() {
	msg = $j("#messages");
	msg.css({"left": "40%"});
	if(msg.children("span").length > 0)
	{
		msg.css("visibility", "visible");
		msg.css("display", "inline");
	}
}
</script>
<#-- End messages.ftl -->

<#-- Source: epp/src-custom/common/include/tablecomponent.ftl -->
<#-- End messages.ftl -->

<#-- Source: epp/src-custom/common/include/updatingSummary.ftl -->
<#-- End updatingSummary.ftl -->

<#-- Source: epp/src-custom/include/form/buttonbar.ftl -->
<script type="text/javascript">
function checkSubmitted(elementId) {
    var buttonIsDisabled = hasElementClass(elementId, "disabled");
    if (!buttonIsDisabled) {
        if(submitted) {
            alertSubmitted();
            return false;
        } else {
            submitted = true;
            return true;
        }
    }
}
function alertSubmitted() {
    writeStatusMessage("<@s.text name='form.button.submitted'/>", "partial");
}
</script>
<#-- End buttonbar.ftl -->

<#-- Source: epp/src-custom/include/form/wizardbuttonbar.ftl -->
<script type="text/javascript">
function checkSubmitted(elementId) {
    if (!hasElementClass($(elementId),"disabled")) {
        if(submitted) {
            alertSubmitted();
            return false;
        } else {
            submitted = true;
            return true;
        }
    }
}

function alertSubmitted() {
    writeStatusMessage("<@s.text name='form.button.submitted'/>", "partial");
}
</script>
<#-- End wizardbuttonbar.ftl -->

<#-- End body snippets -->

<#-- End body snippets -->
