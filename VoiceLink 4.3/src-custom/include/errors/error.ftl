        <@s.text name="${messageText}">
          <@s.param>
             ${exception?default("No exception message available")}
          </@s.param>
          <@s.param>
             ${request.getAttribute("javax.servlet.forward.request_uri")?default("Unknown URI")}
          </@s.param>
          <@s.param>
             ${errorTime?datetime}
          </@s.param>
        </@s.text>
