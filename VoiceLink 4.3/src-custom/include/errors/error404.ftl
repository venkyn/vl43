        <@s.text name="${messageText}">
          <@s.param>
             ${request.getAttribute("javax.servlet.error.request_uri")?default("Unknown URI")}
          </@s.param>
        </@s.text>
