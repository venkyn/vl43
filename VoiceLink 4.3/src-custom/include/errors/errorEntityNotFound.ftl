<#assign unknown>
  <@s.text name="errorPage.errorEntityNotFound.noEntityName"/>
</#assign>
        <@s.text name="${messageText}">
          <@s.param>
             ${(exception.entityType)?default(unknown)}
          </@s.param>
          <@s.param>
             ${request.getAttribute("javax.servlet.forward.request_uri")?default("Unknown URI")}
          </@s.param>
          <@s.param>
             ${errorTime?datetime}
          </@s.param>
        </@s.text>
