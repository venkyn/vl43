/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.puttostore.action;


/**
 * This is the Struts action class that handles operations on
 * <code>PtsContainer</code> objects.
 * 
 */
public class PtsContainerAction extends PtsContainerActionRoot {

    //
    private static final long serialVersionUID = -2771823940176450640L;


    }
