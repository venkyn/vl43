/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.puttostore.action;


/**
 *  This is an extendable action class for the PtsPutDetail portion of the
 *  PTS Containers screen.
 *
 * @author jtauberg
 */
public class PtsContainerDetailAction extends PtsContainerDetailActionRoot {

    //
    private static final long serialVersionUID = -7898811593704738140L;


}
