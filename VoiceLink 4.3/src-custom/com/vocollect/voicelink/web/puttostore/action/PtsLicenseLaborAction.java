/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.puttostore.action;

/**
 * This class handles all puttostore labor actions from the user interface.
 * @author svoruganti
 */
public class PtsLicenseLaborAction extends PtsLicenseLaborActionRoot {

    private static final long serialVersionUID = 7788566119007193610L;

}
