/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.loading.action;

/**
 * This class handles all puttostore labor actions from the user interface.
 *
 */
public class LoadingLaborAction extends LoadingLaborActionRoot {

    
    private static final long serialVersionUID = 217485777276246722L;

}

