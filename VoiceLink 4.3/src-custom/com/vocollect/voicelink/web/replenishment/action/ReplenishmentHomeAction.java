/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.replenishment.action;


/**
 * Action class for configurable homepages for Replenishments.
 *
 * @author svoruganti
 */
public class ReplenishmentHomeAction extends ReplenishmentHomeActionRoot {

    //generated serialVersion ID
    private static final long serialVersionUID = -146245306955335098L;

   
    
}
