/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.service.impl;

import javax.jws.WebService;

/**
 * Implementation of Delivery Location Mapping web service.
 * 
 */
@WebService(endpointInterface = "com.vocollect.voicelink.web.service.DeliveryLocationWebService")
public class DeliveryLocationWebServiceImpl extends
    DeliveryLocationWebServiceImplRoot {

}
