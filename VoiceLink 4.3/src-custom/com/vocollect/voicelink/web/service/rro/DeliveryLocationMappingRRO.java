/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.web.service.rro;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Delivery Location Request Response Object (RRO)
 * 
 * @author khazra
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "deliveryMapping")
public class DeliveryLocationMappingRRO extends DeliveryLocationMappingRRORoot {

}
