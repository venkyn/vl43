/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.opensymphony.xwork2.Preparable;

/**
 * Action code for print label.
 */
@SuppressWarnings("serial")
public class PrinterAction extends PrinterActionRoot
                     implements Preparable {

    //The serial version id.
     private static final long serialVersionUID = 6478026686916495773L;

 }
