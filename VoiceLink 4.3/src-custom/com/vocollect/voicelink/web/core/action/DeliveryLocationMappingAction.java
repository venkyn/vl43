/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.opensymphony.xwork2.Preparable;

/**
 * Custom action for Delivery Location feature.
 * @see com.vocollect.voicelink.web.core.action.DeliveryLocationMappingActionRoot
 * 
 * @author bnorthrop
 */
public class DeliveryLocationMappingAction extends
    DeliveryLocationMappingActionRoot implements Preparable {

    /**
     * Generated SUID from Eclipse.
     */
    private static final long serialVersionUID = 3513494778733968972L;

}
