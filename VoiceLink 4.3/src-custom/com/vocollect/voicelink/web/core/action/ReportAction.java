/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.core.action;

import com.opensymphony.xwork2.Preparable;

/**
 * This is the Struts action class that handles operations on
 * <code>Report</code> objects.
 * 
 */
public class ReportAction extends ReportActionRoot implements Preparable {

    //
    private static final long serialVersionUID = -1881441411238923665L;


}
