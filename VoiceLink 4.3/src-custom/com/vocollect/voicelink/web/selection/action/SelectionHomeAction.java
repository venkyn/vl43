/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;


/**
 * Decides on a default page for the current user. 
 * @author Brian Rupert
 */
public class SelectionHomeAction extends SelectionHomeActionRoot {

    //
    private static final long serialVersionUID = 8039274222240340635L;
    
}
