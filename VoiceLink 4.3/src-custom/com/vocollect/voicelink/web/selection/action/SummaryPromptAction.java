/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.opensymphony.xwork2.Preparable;


/**
 * Action for Summary Prompt.
 * @see com.vocollect.voicelink.web.selection.action.SummaryPromptActionRoot
 *
 * @author svoruganti
 */
public class SummaryPromptAction extends SummaryPromptActionRoot
implements Preparable {

    //generated serial version id
    private static final long serialVersionUID = 6238080699286218680L;

}
