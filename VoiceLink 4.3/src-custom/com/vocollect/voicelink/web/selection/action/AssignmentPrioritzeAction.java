/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.selection.action;

import com.opensymphony.xwork2.Preparable;


/**
 * Actions for the assignment priority update view.
 *
 */
public class AssignmentPrioritzeAction extends AssignmentPrioritzeActionRoot 
implements Preparable {

    //
    private static final long serialVersionUID = -6199826327172076923L;


}
