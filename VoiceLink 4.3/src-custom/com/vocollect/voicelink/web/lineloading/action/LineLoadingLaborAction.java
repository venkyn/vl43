/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.lineloading.action;

/**
 * This class handles all line loading labor actions from the user interface.
 * @author brupert
 */
public class LineLoadingLaborAction extends LineLoadingLaborActionRoot {

    private static final long serialVersionUID = 7971369194951117105L;

}
