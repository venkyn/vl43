/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.lineloading.action;

import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.Validateable;


/**
 * Struts action class to handle lineloading summary actions.
 *
 * @author ypore
 */
public class LineLoadingSummaryAction extends LineLoadingSummaryActionRoot
    implements Preparable, Validateable {

    //
    private static final long serialVersionUID = -3498058232861651005L;

}
