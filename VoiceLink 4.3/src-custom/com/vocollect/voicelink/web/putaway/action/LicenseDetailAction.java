/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.putaway.action;

import com.opensymphony.xwork2.Preparable;

/**
 * Putaway License Detail Action.
 */
public class LicenseDetailAction extends LicenseDetailActionRoot implements Preparable {

    private static final long serialVersionUID = -8027096056410630490L;

}
