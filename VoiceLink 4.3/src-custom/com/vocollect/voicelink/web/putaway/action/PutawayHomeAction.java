/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.putaway.action;

/**
 * Action class for configurable homepages for Replenishments.
 * @author brupert
 */
public class PutawayHomeAction extends PutawayHomeActionRoot {

    private static final long serialVersionUID = 5334206529121048195L;
    

}
