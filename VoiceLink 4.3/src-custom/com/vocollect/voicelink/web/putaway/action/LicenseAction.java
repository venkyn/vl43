/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.web.putaway.action;

import com.opensymphony.xwork2.Preparable;

import org.hibernate.classic.Validatable;

/**
 * Putaway License Action.
 */
public class LicenseAction extends LicenseActionRoot implements Preparable,
    Validatable {

    private static final long serialVersionUID = -8853914882438920790L;

}
