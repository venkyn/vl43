/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.putaway.model;

import com.vocollect.epp.model.Taggable;

import java.io.Serializable;


/**
 * Main Putaway License class. 
 *
 */
public class License extends LicenseRoot implements Serializable, Taggable {

    //
    private static final long serialVersionUID = 4953348240151978588L;
}
