/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.service.impl;

import com.vocollect.voicelink.putaway.dao.PutawayRegionDAO;
import com.vocollect.voicelink.putaway.service.PutawayRegionManager;


/**
 * Additional service methods for the <code>PutawayRegion</code> model object.
 *
 */
public class PutawayRegionManagerImpl extends
    PutawayRegionManagerImplRoot implements PutawayRegionManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PutawayRegionManagerImpl(PutawayRegionDAO primaryDAO) {
        super(primaryDAO);
    }
    
    
    
    

}
