/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.putaway.service.impl;

import com.vocollect.voicelink.putaway.dao.LicenseImportDataDAO;
import com.vocollect.voicelink.putaway.service.LicenseImportDataManager;



/**
 * Custom stub for the LicenseImportData manager class.
 * @author jtauberg
 *
 */
public class LicenseImportDataManagerImpl extends LicenseImportDataManagerImplRoot
    implements LicenseImportDataManager {
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager.
     */
    public LicenseImportDataManagerImpl(LicenseImportDataDAO primaryDAO) {
        super(primaryDAO);
    }
}
