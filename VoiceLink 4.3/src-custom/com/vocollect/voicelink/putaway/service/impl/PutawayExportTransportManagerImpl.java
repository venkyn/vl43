/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.putaway.service.impl;



import com.vocollect.voicelink.putaway.dao.LicenseDAO;
import com.vocollect.voicelink.putaway.service.PutawayExportTransportManager;


/**
 * 
 * @author vsubramani
 *
 */
public class PutawayExportTransportManagerImpl extends PutawayExportTransportManagerImplRoot
    implements PutawayExportTransportManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PutawayExportTransportManagerImpl(LicenseDAO primaryDAO) {
        super(primaryDAO);
    }

}
