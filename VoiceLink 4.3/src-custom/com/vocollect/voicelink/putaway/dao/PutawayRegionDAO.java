/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.putaway.model.PutawayRegion;

import java.util.List;


/**
 * Putaway Region Data Access Object (DAO) interface.
 * 
 */
public interface PutawayRegionDAO extends PutawayRegionDAORoot {
    
    /**
     * @return List<PutawayRegion>
     * @throws DataAccessException
     */
    List<PutawayRegion> listAllRegionsOrderByNumber() throws DataAccessException;
    
}
