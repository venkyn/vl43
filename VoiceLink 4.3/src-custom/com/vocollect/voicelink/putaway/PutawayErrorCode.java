/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.putaway;



/**
 * Class to hold the instances of error codes for selection business. Although the
 * range for these error codes is 4000-4999, please use only 4000-4499 for
 * the predefined ones; 4500-4999 should be reserved for customization error
 * codes.
 * 
 */
public final class PutawayErrorCode extends PutawayErrorCodeRoot {
   
    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected PutawayErrorCode(long err) {
        // The error is defined in the PutawayErrorCode context.
        super(err);
    }

}
