/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.voicelink.cyclecounting.dao.CycleCountingDetailImportDataDAO;

/**
 * Custom stub for the CycleCountingDetailImportData manager class.
 * 
 */
public class CycleCountingDetailImportDataManagerImpl extends
CycleCountingDetailImportDataManagerImplRoot {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public CycleCountingDetailImportDataManagerImpl(CycleCountingDetailImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

}
