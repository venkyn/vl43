/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.voicelink.cyclecounting.dao.CycleCountingDetailDAO;
import com.vocollect.voicelink.cyclecounting.service.CycleCountingDetailExportTransportManager;


/**
 *@author amukherjee
 */
public class CycleCountingDetailExportTransportManagerImpl 
    extends CycleCountingDetailExportTransportManagerImplRoot 
    implements CycleCountingDetailExportTransportManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public CycleCountingDetailExportTransportManagerImpl(CycleCountingDetailDAO primaryDAO) {
        super(primaryDAO);
    }
    
}
