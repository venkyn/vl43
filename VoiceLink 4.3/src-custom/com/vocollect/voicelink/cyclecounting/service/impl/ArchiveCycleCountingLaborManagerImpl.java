/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.voicelink.cyclecounting.dao.ArchiveCycleCountingLaborDAO;
import com.vocollect.voicelink.cyclecounting.service.ArchiveCycleCountingLaborManager;

/**
 * @author ktanneru
 *
 */
public class ArchiveCycleCountingLaborManagerImpl extends
        ArchiveCycleCountingLaborManagerImplRoot implements
        ArchiveCycleCountingLaborManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ArchiveCycleCountingLaborManagerImpl(ArchiveCycleCountingLaborDAO primaryDAO) {
        super(primaryDAO);
    }
}
