/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.voicelink.cyclecounting.dao.CycleCountingAssignmentDAO;

/**
 * @author pkolonay
 *
 */
public class CycleCountingAssignmentManagerImpl extends
        CycleCountingAssignmentManagerImplRoot {

    /**
     * @param primaryDAO - primary DAO
     */
    public CycleCountingAssignmentManagerImpl(
            CycleCountingAssignmentDAO primaryDAO) {
        super(primaryDAO);
        // TODO Auto-generated constructor stub
    }

}
