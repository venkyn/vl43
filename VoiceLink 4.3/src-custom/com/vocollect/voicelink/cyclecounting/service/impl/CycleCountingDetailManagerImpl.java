/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.cyclecounting.service.impl;

import com.vocollect.voicelink.cyclecounting.dao.CycleCountingDetailDAO;

/**
 * 
 *
 */
public class CycleCountingDetailManagerImpl extends
        CycleCountingDetailManagerImplRoot {

    /**
     * @param primaryDAO - the primaryDAO
     */
    public CycleCountingDetailManagerImpl(CycleCountingDetailDAO primaryDAO) {
        super(primaryDAO);
    }


}
