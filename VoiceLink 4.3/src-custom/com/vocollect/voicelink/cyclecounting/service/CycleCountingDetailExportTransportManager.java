/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

/**
 * @author amukherjee
 */
package com.vocollect.voicelink.cyclecounting.service;


/**
 *
 */
public interface CycleCountingDetailExportTransportManager extends 
    CycleCountingDetailExportTransportManagerRoot {

}
