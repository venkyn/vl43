/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * @author pkolonay
 *
 */
public enum CycleCountStatus implements ValueBasedEnum<CycleCountStatus> {
    Unavailable(1), 
    Available(2), 
    InProgress(3), 
    Complete(4), 
    Canceled(5),
    Skipped(6);
    
    private int value;

    // Internal map of int values to enum values.
    private static ReverseEnumMap<CycleCountStatus> toValueMap = new ReverseEnumMap<CycleCountStatus>(
            CycleCountStatus.class);

    /**
     * @param value - cyclecount status
     */
    private CycleCountStatus(int value) {
        this.value = value;
    }

    @Override
    public int toValue() {
        return this.value;
    }

    @Override
    public CycleCountStatus fromValue(int val) {
        return toValueMap.get(val);
    }

    /* (non-Javadoc)
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(java.lang.Integer)
     */
    /**
     * @param valObject - the valObject
     * @return CycleCountStatus
     */
    @Override
    public CycleCountStatus fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * @param statuses the set of statuses to compare to
     * @return true if this is in the set, false otherwise.
     */
    public boolean isInSet(CycleCountStatus... statuses) {
        for (CycleCountStatus status : statuses) {
            if (this.equals(status)) {
                return true;
            }
        }
        // No match
        return false;
    }
    
    
    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static CycleCountStatus toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
    
}
