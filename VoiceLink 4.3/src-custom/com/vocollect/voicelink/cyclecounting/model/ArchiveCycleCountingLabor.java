/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;


/**
 * Model object representing a VoiceLink Cycle Counting Archive Assignment Labor
 * Object.
 * 
 * @author ktanneru
 * 
 */
public class ArchiveCycleCountingLabor extends ArchiveCycleCountingLaborRoot {

    // Generated serial version
    private static final long serialVersionUID = -2573642686418996040L;

    /**
     * 
     * Constructor.
     * 
     * @param cycleCountingLabor
     *            cycleCountingLabor used to construct an Archive CycleCountingLabor
     *            object.
     * @param archiveCycleCountingAssignment
     *            ArchiveCycleCountingAssignment used to construct an 
     *            ArchiveCycleCountingAssignment object.
     */

    public ArchiveCycleCountingLabor(CycleCountingLabor cycleCountingLabor,
            ArchiveCycleCountingAssignment archiveCycleCountingAssignment) {
        super(cycleCountingLabor, archiveCycleCountingAssignment);
    }

    /**
     * 
     * Constructor.
     */
    public ArchiveCycleCountingLabor() {
        super();
    }

}
