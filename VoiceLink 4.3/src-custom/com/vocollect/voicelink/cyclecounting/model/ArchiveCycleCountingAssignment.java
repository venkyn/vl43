/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;


/**
 * Model object representing a VoiceLink Archive Cycle Counting Assignment.
 * 
 * @author ktanneru
 *
 */
public class ArchiveCycleCountingAssignment extends 
ArchiveCycleCountingAssignmentRoot {

    private static final long serialVersionUID = -7297630176847132871L;

    /**
     * 
     * Constructor.
     * @param cycleCountingAssignment CycleCountingAssignment to use to construct an 
     * ArchiveCycleCountingAssignment.
     */
    public ArchiveCycleCountingAssignment(CycleCountingAssignment cycleCountingAssignment) {
        super(cycleCountingAssignment);
    }
    
    /**
     * 
     * Constructor.
     */
    public ArchiveCycleCountingAssignment() {
        super();
    }
}
