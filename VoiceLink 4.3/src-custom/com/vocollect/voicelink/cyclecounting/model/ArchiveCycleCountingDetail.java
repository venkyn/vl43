/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.cyclecounting.model;


/**
 * Model object representing a VoiceLink Archive Cycle Counting Assignment Detail.
 * @author ktanneru
 *
 */
public class ArchiveCycleCountingDetail extends ArchiveCycleCountingDetailRoot {

    /**
     * 
     */
    private static final long serialVersionUID = 1831014634379119712L;
    
    /**
     * 
     * Constructor.
     */
    public ArchiveCycleCountingDetail() {
        super();
    }
   
    /**
     * 
     * Constructor.
     * @param cycleCountingDetail used to construct ArchivecycleCountingDetail.
     * @param archiveCycleCountingAssignment  used to construct ArchiveCycleCountingAssignment
     */
    public ArchiveCycleCountingDetail(CycleCountingDetail cycleCountingDetail,
                                          ArchiveCycleCountingAssignment archiveCycleCountingAssignment) {
        super(cycleCountingDetail, archiveCycleCountingAssignment);
        
    }

}
