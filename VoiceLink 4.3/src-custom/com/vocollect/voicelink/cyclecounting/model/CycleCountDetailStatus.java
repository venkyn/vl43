/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.cyclecounting.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * @author pkolonay
 *
 */
public enum CycleCountDetailStatus implements ValueBasedEnum<CycleCountDetailStatus> {
    NotCounted(1), 
    Counted(2), 
    Canceled(3);

    /**
     * 
     */
    private int     value;
    
    
    // Internal map of int values to enum values.
    private static ReverseEnumMap<CycleCountDetailStatus> toValueMap = new ReverseEnumMap<CycleCountDetailStatus>(
            CycleCountDetailStatus.class);

    /**
     * @param value - the value
     */
    private CycleCountDetailStatus(int value) {
        this.value = value;
    }
    
    @Override
    public int toValue() {
        return this.value;
    }


    @Override
    public CycleCountDetailStatus fromValue(Integer valObject) {
        return toValueMap.get(valObject);
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    @Override
    public CycleCountDetailStatus fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * @param statuses the set of statuses to compare to
     * @return true if this is in the set, false otherwise.
     */
    public boolean isInSet(CycleCountDetailStatus... statuses) {
        for (CycleCountDetailStatus status : statuses) {
            if (this.equals(status)) {
                return true;
            }
        }
        // No match
        return false;
    }
    
    
    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static CycleCountDetailStatus toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
