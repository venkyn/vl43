/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.cyclecounting.model;

/**
 * Customization point for CycleCounting Assignment import data.
 *
 */
public class CycleCountingAssignmentImportData extends
        CycleCountingAssignmentImportDataRoot {

    /**
     * 
     */
    private static final long serialVersionUID = 2923950241760162943L;

}
