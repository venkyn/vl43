/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.cyclecounting.model;

/**
 * Customization point for Cycle Counting import data.
 *
 */
public class CycleCountingDetailImportData extends
        CycleCountingDetailImportDataRoot {

    /**
     * 
     */
    private static final long serialVersionUID = -3263057382036124974L;

}
