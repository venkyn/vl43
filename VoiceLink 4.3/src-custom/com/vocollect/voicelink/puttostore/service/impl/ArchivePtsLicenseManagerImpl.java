/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.voicelink.puttostore.dao.ArchivePtsLicenseDAO;
import com.vocollect.voicelink.puttostore.service.ArchivePtsLicenseManager;

/**
 * Additional service methods for the <code>Assignment</code> model object.
 *
 */
public class ArchivePtsLicenseManagerImpl extends
    ArchivePtsLicenseManagerImplRoot implements ArchivePtsLicenseManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ArchivePtsLicenseManagerImpl(ArchivePtsLicenseDAO primaryDAO) {
        super(primaryDAO);
    }

}
