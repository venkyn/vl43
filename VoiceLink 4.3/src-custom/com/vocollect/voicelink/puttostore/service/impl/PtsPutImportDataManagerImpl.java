/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.voicelink.puttostore.dao.PtsPutImportDataDAO;

/**
 * Custom stub for the pts Put ImportData manager class.
 * @author svoruganti
 *
 */
public class PtsPutImportDataManagerImpl extends PtsPutImportDataManagerImplRoot {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsPutImportDataManagerImpl(PtsPutImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

}
