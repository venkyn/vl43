/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.voicelink.puttostore.dao.PtsContainerDetailDAO;
import com.vocollect.voicelink.puttostore.service.PtsContainerDetailManager;


/**
 * Service layer for Container Detail.
 *
 * @author mkoenig
 */
public class PtsContainerDetailManagerImpl extends
     PtsContainerDetailManagerImplRoot implements PtsContainerDetailManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsContainerDetailManagerImpl(PtsContainerDetailDAO primaryDAO) {
        super(primaryDAO);
    }
}

