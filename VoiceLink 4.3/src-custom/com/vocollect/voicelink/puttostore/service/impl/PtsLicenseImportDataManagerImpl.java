/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;


import com.vocollect.voicelink.puttostore.dao.PtsLicenseImportDataDAO;

/**
 * @author svoruganti
 *
 */
public class PtsLicenseImportDataManagerImpl extends
        PtsLicenseImportDataManagerImplRoot {
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsLicenseImportDataManagerImpl(PtsLicenseImportDataDAO primaryDAO) {
        super(primaryDAO);
    }


}
