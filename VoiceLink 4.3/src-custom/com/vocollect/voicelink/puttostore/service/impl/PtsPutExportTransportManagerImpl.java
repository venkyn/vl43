/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.voicelink.puttostore.dao.PtsPutDAO;
import com.vocollect.voicelink.puttostore.service.PtsPutExportTransportManager;

/**
 *
 */
public class PtsPutExportTransportManagerImpl 
    extends PtsPutExportTransportManagerImplRoot 
    implements PtsPutExportTransportManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsPutExportTransportManagerImpl(PtsPutDAO primaryDAO) {
        super(primaryDAO);
    }

}
