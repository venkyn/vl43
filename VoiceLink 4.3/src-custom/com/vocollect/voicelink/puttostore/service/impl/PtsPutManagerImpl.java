/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.voicelink.puttostore.dao.PtsPutDAO;
import com.vocollect.voicelink.puttostore.service.PtsPutManager;



/**
 * Additional service methods for the <code>Put</code> model object.
 *
 */
public class PtsPutManagerImpl extends
    PtsPutManagerImplRoot implements PtsPutManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsPutManagerImpl(PtsPutDAO primaryDAO) {
        super(primaryDAO);
    }
}
