/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;


import com.vocollect.voicelink.puttostore.dao.PtsCustomerLocationImportDataDAO;
import com.vocollect.voicelink.puttostore.service.PtsCustomerLocationImportDataManager;

/**
 * Custom stub for the PtsCustomerLocationImportData manager class.
 * @author mnichols
 *
 */
public class PtsCustomerLocationImportDataManagerImpl extends PtsCustomerLocationImportDataManagerImplRoot
        implements PtsCustomerLocationImportDataManager {
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsCustomerLocationImportDataManagerImpl(PtsCustomerLocationImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

}
