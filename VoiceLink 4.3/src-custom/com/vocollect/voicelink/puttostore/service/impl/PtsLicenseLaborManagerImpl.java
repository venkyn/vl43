/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.service.impl;

import com.vocollect.voicelink.puttostore.dao.PtsLicenseLaborDAO;
import com.vocollect.voicelink.puttostore.service.PtsLicenseLaborManager;


/**
 *
 *
 * @author treed
 */
public class PtsLicenseLaborManagerImpl extends PtsLicenseLaborManagerImplRoot
    implements PtsLicenseLaborManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PtsLicenseLaborManagerImpl(PtsLicenseLaborDAO primaryDAO) {
        super(primaryDAO);
    }
}
