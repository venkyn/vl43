/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

/**
 * Customization extension for the Importable Customer Location.
 * @author mnichols
 *
 */
public class PtsCustomerLocationImportData extends PtsCustomerLocationImportDataRoot {

    private static final long serialVersionUID = -75179876363779621L;

}
