/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import java.io.Serializable;

/**
 * Model object representing a VoiceLink Put To Store Archive Put Object.
 *
 * @author mlashinsky
 */
public class ArchivePtsPut extends ArchivePtsPutRoot implements Serializable {

    /**
     *
     * Constructor.
     */
    public ArchivePtsPut() {

    }

    /**
     *
     * Constructor.
     * @param put Put used to construct ArchivePtsPut.
     * @param archivePtsLicense ArchivePtsLicense used to construct an
     *            ArchivePtsPut object. ArchivePtsPut object.
     */
    public ArchivePtsPut(PtsPut put, ArchivePtsLicense archivePtsLicense) {
        super(put, archivePtsLicense);
    }

    //
    private static final long serialVersionUID = 419529754514645952L;

}
