/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import java.util.Date;

/**
 *
 *
 * @author mlashinsky
 */
public class PtsContainerReport extends PtsContainerReportRoot {

    /**
     * Constructor.
     * @param containerNumber String value of the container number
     * @param containerStatus PtsContainerStatus of the container
     * @param quantityPut Integer value of the quantity put into the container
     * @param itemDescription String value of the item description
     * @param itemNumber String value of the item number
     * @param operatorName String value of the operator name (may be null)
     * @param operatorId String value of the operator Identifier
     * @param putTime Date value of the put time
     * @param customerNumber String value of the Customer Number
     * @param customerName String value of the Customer Name
     * @param route String value of the route
     * @param customerAddress String value of the Customer Address
     * @param aisle String value of the aisle direction
     * @param postAisle String value of the post-aisle direction
     * @param preAisle String value of the pre-aisle direction
     * @param slot String value of the slot direction
     * @param licenseNumber String value of the licenseNumber
     */
    public PtsContainerReport(String containerNumber,
                              PtsContainerStatus containerStatus,
                              Integer quantityPut,
                              String itemDescription,
                              String itemNumber,
                              String operatorName,
                              String operatorId,
                              Date putTime,
                              String customerNumber,
                              String customerName,
                              String route,
                              String customerAddress,
                              String aisle,
                              String postAisle,
                              String preAisle,
                              String slot,
                              String licenseNumber) {
        super(containerNumber, containerStatus, quantityPut, itemDescription,
              itemNumber, operatorName, operatorId, putTime, customerNumber,
              customerName, route, customerAddress, aisle, postAisle, preAisle,
              slot, licenseNumber);
    }

}
