/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import java.io.Serializable;

/**
 *
 */
public class ArchivePtsContainerDetail extends ArchivePtsContainerDetailRoot
    implements Serializable {

    //
    private static final long serialVersionUID = 1983622703946730069L;

    /**
     *
     * Constructor.
     */
    public ArchivePtsContainerDetail() {

    }

    /**
     *
     * Constructor.
     * @param archivePtsContainer - Archive container that this archive detail belongs to
     * @param ptsContainerDetail Container - Detail used to construct ArchiveContainer
     */
    public ArchivePtsContainerDetail(PtsContainerDetail ptsContainerDetail,
                                     ArchivePtsContainer archivePtsContainer) {
        super(ptsContainerDetail, archivePtsContainer);
    }

}
