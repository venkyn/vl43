/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;
/**
 *
 * @author pfunyak
 *
 */
public enum PtsLicenseStatus  implements ValueBasedEnum<PtsLicenseStatus>  {

    InProgress(1),
    Suspended(2),
    Available(3),
    Short(4),
    Complete(5),
    Passed(11);

    // Internal map of int values to enum values.
    private static ReverseEnumMap<PtsLicenseStatus> toValueMap =
        new ReverseEnumMap<PtsLicenseStatus>(PtsLicenseStatus.class);

    private int value;

    /**
     * Constructor.
     * @param value the associated int rep.
     */
    private PtsLicenseStatus(int value) {
        this.value = value;
    }

    /**
     * method to get resource key associated with the enum value.
     * @return - String for resource key
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * @param statuses the set of statuses to compare to
     * @return true if this is in the set, false otherwise.
     */
    public boolean isInSet(PtsLicenseStatus... statuses) {
        for (PtsLicenseStatus status : statuses) {
            if (this.equals(status)) {
                return true;
            }
        }
        // No match
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public PtsLicenseStatus fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.AssignmentStatus#fromValue(Integer)
     */
    public PtsLicenseStatus fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }


    /**
     * @return the int value of this status.
     */
    public int getValue() {
        return value;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static PtsLicenseStatus toEnum(int val)
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }

}
