/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import java.util.Date;

/**
 * 
 *
 * @author mnichols
 */
public class PtsLicenseLaborReport extends PtsLicenseLaborReportRoot {

    /**
     * Constructor.
     * @param operatorName - the operator name.
     * @param operatorId - the operator id.
     * @param endTime - the end time.
     * @param startTime - the start time.
     * @param proRateCount - the pro rate count.
     * @param percentOfGoal - the percent of the goal.
     * @param quantityPut - the quantity put.
     * @param groupNumber - the group number.
     * @param groupCount - the group count.
     * @param groupProRateCount - the group pro rate count.
     * @param licenseNumber - the license number.
     */
    public PtsLicenseLaborReport(String operatorName,
                                 String operatorId,
                                 Date endTime,
                                 Date startTime,
                                 Integer proRateCount,
                                 Double percentOfGoal,
                                 Integer quantityPut,
                                 Long groupNumber,
                                 Integer groupCount,
                                 Integer groupProRateCount,
                                 String licenseNumber) {
        super(
            operatorName, operatorId, endTime, startTime, proRateCount, percentOfGoal,
            quantityPut, groupNumber, groupCount, groupProRateCount, licenseNumber);
        // TODO Auto-generated constructor stub
    }

}
