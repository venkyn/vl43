/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.epp.model.Taggable;

import java.io.Serializable;


/**
 * 
 */
public class PtsContainer extends PtsContainerRoot implements Serializable, Taggable {

    //
    private static final long serialVersionUID = -3657499873087189673L;

}
