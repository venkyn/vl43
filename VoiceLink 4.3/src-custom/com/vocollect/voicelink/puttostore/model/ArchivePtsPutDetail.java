/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import java.io.Serializable;

/**
 * Model object representing a VoiceLink Put To Store Archive Put Detail Object.
 *
 * @author mlashinsky
 */
public class ArchivePtsPutDetail extends ArchivePtsPutDetailRoot implements
    Serializable {

    //
    private static final long serialVersionUID = 2969623300218090733L;

    /**
     *
     * Constructor.
     * @param pd PtsPutDetail used to construct ArchivePtsPutDetails.
     * @param archivePtsPut ArchivePtsPut used to construct
     *            ArchivePtsPutDetails.
     */
    public ArchivePtsPutDetail(PtsPutDetail pd, ArchivePtsPut archivePtsPut) {
        super(pd, archivePtsPut);
    }

    /**
     *
     * Constructor.
     */
    public ArchivePtsPutDetail() {
        super();
    }


}
