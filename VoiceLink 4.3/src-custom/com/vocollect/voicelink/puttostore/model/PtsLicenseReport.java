/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import java.util.Date;


/**
 * 
 *
 * @author mnichols
 */
public class PtsLicenseReport extends PtsLicenseReportRoot {

    /**
     * Constructor.
     * @param licenseNumber the license number
     * @param licenseStatus the status
     * @param containerNumber the container number
     * @param customerName the customer name
     * @param deliveryLocation the delivery location
     * @param route the route
     * @param putID the put id
     * @param operatorID the operator id
     * @param operatorName the operator name
     * @param quantityPut the quantity put
     * @param quantityToPut the quantity to put
     * @param residualQuantity the residual quantity
     * @param itemNumber the item number
     * @param itemDescription the item description
     * @param preAisle the pre-aisle
     * @param aisle the aisle
     * @param postAisle the post-aisle
     * @param slot the slot
     * @param putTime the put time
     * @param putStatus the put status
     */
    public PtsLicenseReport(String licenseNumber,
                            PtsLicenseStatus licenseStatus,
                            String containerNumber,
                            String customerName,
                            String deliveryLocation,
                            String route,
                            Long putID,
                            String operatorID,
                            String operatorName,
                            Integer quantityPut,
                            Integer quantityToPut,
                            Integer residualQuantity,
                            String itemNumber,
                            String itemDescription,
                            String preAisle,
                            String aisle,
                            String postAisle,
                            String slot,
                            Date putTime,
                            PtsPutStatus putStatus) {
        super(
            licenseNumber, licenseStatus, containerNumber, customerName,
            deliveryLocation, route, putID, operatorID, operatorName, quantityPut,
            quantityToPut, residualQuantity, itemNumber, itemDescription, preAisle,
            aisle, postAisle, slot, putTime, putStatus);
        // TODO Auto-generated constructor stub
    }

}
