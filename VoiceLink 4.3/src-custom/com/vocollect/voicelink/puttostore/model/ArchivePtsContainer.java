/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import java.io.Serializable;

/**
 * Model object representing a VoiceLink Put To Store Archive Container Object.
 *
 * @author mlashinsky
 */
public class ArchivePtsContainer extends ArchivePtsContainerRoot implements
    Serializable {

    /**
     *
     * Constructor.
     */
    public ArchivePtsContainer() {

    }

    /**
     *
     * Constructor.
     * @param ptsContainer Container to use to construct ArchiveContainer
     */
    public ArchivePtsContainer(PtsContainer ptsContainer) {
        super(ptsContainer);
    }

    //
    private static final long serialVersionUID = -38531262450703528L;

}
