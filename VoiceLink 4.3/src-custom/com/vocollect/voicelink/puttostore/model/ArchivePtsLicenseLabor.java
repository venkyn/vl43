/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import java.io.Serializable;

/**
 * Model object representing a VoiceLink Put To Store Archive License Labor
 * Object.
 *
 * @author mlashinsky
 */
public class ArchivePtsLicenseLabor extends ArchivePtsLicenseLaborRoot
    implements Serializable {

    // Generated serial version
    private static final long serialVersionUID = 8338586155761254117L;

    /**
     *
     * Constructor.
     * @param pl PtsLicenseLabor used to construct an ArchivePtsLicenseLabor
     *            object.
     * @param archivePtsLicense ArchivePtsLicense used to construct an
     *            ArchovePtsLicenseLabor object.
     */

    public ArchivePtsLicenseLabor(PtsLicenseLabor pl,
                                  ArchivePtsLicense archivePtsLicense) {
        super(pl, archivePtsLicense);
    }
    /**
     *
     * Constructor.
     */
    public ArchivePtsLicenseLabor() {
        super();
    }

}

