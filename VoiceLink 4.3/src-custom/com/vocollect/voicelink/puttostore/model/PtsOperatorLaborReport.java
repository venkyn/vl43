/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.model;

import com.vocollect.voicelink.core.model.OperatorLaborActionType;
import com.vocollect.voicelink.core.model.OperatorLaborFilterType;

/**
 *
 *
 * @author mlashinsky
 */
public class PtsOperatorLaborReport extends PtsOperatorLaborReportRoot {

    /**
     * Constructor.
     * @param operatorName Operator Name
     * @param operatorId Operator Identifier
     * @param regionName Region Name
     * @param regionNumber Region Number
     * @param actionType Operator Labor Action Type
     * @param totalQuantity Total Quantity
     * @param totalTimeHours Hours in Total Time
     * @param totalTimeMins Minutes in Total Time
     * @param goalRate Goal Rate (picks/hour)
     * @param actualRate Actual Operator Rate (picks/hour)
     * @param percentOfGoal Percent of Goal (percent)
     * @param filterType Filter Type
     */
    public PtsOperatorLaborReport(String operatorName,
                                  String operatorId,
                                  String regionName,
                                  Integer regionNumber,
                                  OperatorLaborActionType actionType,
                                  Long totalQuantity,
                                  Double totalTimeHours,
                                  Double totalTimeMins,
                                  Integer goalRate,
                                  Double actualRate,
                                  Double percentOfGoal,
                                  OperatorLaborFilterType filterType) {
        super(operatorName, operatorId, regionName, regionNumber, actionType,
              totalQuantity, totalTimeHours, totalTimeMins, goalRate,
              actualRate, percentOfGoal, filterType);
    }

}
