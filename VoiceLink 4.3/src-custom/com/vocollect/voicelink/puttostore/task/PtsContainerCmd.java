/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore.task;


/**
 * Extenxdable Task Command for Put To Store Container
 * Allows Open, Close, and get list.
 *
 * @author jtauberg
 */
public class PtsContainerCmd extends PtsContainerCmdRoot {

    //
    private static final long serialVersionUID = 8638102103205513949L;



}
