/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.puttostore;



/**
 * Class to hold the instances of error codes for Put-To-Store business. 
 * Although the range for these error codes is 10000-10999, please use 
 * only 10000-10499 for the predefined ones; 10500-10999 should be 
 * reserved for customization error codes.
 */
public final class PutToStoreErrorCode extends PutToStoreErrorCodeRoot {

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected PutToStoreErrorCode(long err) {
        // The error is defined in the PutToStoreErrorCode context.
        super(err);
    }

}
