/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service.impl;

import com.vocollect.voicelink.lineloading.dao.RouteStopDAO;
import com.vocollect.voicelink.lineloading.service.RouteStopManager;


/**
 * Additional service methods for the <code>RouteStop</code> model object.
 *
 */
public class RouteStopManagerImpl extends RouteStopManagerImplRoot implements
    RouteStopManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public RouteStopManagerImpl(RouteStopDAO primaryDAO) {
        super(primaryDAO);
    }
}
