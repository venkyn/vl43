/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service.impl;

import com.vocollect.voicelink.lineloading.dao.CartonDetailDAO;
import com.vocollect.voicelink.lineloading.service.CartonDetailManager;


/**
 * Additional service methods for the <code>CartonDetail</code> model object.
 *
 */
public class CartonDetailManagerImpl extends CartonDetailManagerImplRoot
    implements CartonDetailManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public CartonDetailManagerImpl(CartonDetailDAO primaryDAO) {
        super(primaryDAO);
    }
}
