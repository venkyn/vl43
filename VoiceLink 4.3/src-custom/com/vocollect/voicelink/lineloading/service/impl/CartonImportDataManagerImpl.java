/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.service.impl;

import com.vocollect.voicelink.lineloading.dao.CartonImportDataDAO;
import com.vocollect.voicelink.lineloading.service.CartonImportDataManager;

/**
 * Customization point for the service layer implementation for the Carton (LINELOADING) import.
 * @author dgold
 *
 */
public class CartonImportDataManagerImpl extends
        CartonImportDataManagerImplRoot implements CartonImportDataManager {

    /**
     * Default c'tor.
     * @param primaryDAO the DAO used to acces the DB.
     */
    public CartonImportDataManagerImpl(CartonImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

}
