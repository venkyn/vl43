/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * Carton Detail Actions.
 */
public enum CartonDetailAction implements ValueBasedEnum<CartonDetailAction> {
    InProgress(2),
    Loaded(3),
    SetAside(4),
    Shorted(5),
    Moved(6);

    // Internal map of int values to enum values.
    private static ReverseEnumMap<CartonDetailAction> toValueMap =
        new ReverseEnumMap<CartonDetailAction>(CartonDetailAction.class);
    
    private int value;

    /**
     * Constructor.
     * @param value the int rep value.
     */
    private CartonDetailAction(int value) {
        this.value = value;
    }

    /**
     * @return the associated resource key.
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public CartonDetailAction fromValue(int val) {
        return toEnum(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.CartonDetailAction#fromValue(Integer)
     */
    public CartonDetailAction fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static CartonDetailAction toEnum(int val) throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
