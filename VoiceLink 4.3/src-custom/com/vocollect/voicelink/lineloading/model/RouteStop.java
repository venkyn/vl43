/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.model;

import com.vocollect.epp.model.Taggable;

import java.io.Serializable;


/**
 * 
 *
 * @author mkoenig
 */
public class RouteStop extends RouteStopRoot 
implements Serializable, Taggable {

    //
    private static final long serialVersionUID = 8046810178187470006L;

}
