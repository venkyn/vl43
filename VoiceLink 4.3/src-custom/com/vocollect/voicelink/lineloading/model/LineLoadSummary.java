/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.lineloading.model;

import com.vocollect.epp.model.Taggable;

import java.io.Serializable;


/**
 * This model is used for both summary by wave and summary by wave and region
 * since the only different is the region which will be null if only doing summary
 * by wave.
 *
 */
public class LineLoadSummary extends LineLoadSummaryRoot
implements Serializable, Taggable {

    //
    private static final long serialVersionUID = 6816475813496813536L;

}
