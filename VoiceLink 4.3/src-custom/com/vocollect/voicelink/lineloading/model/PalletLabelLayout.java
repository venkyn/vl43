/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.lineloading.model;


/**
 * Line Loading Pallet Laboe Layout Definition. 
 */
public class PalletLabelLayout extends PalletLabelLayoutRoot {

    //
    private static final long serialVersionUID = -7015203164511798661L;

}
