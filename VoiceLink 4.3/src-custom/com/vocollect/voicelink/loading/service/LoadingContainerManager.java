/*
 * Copyright (c) 2006 Vocollect, Inc. Pittsburgh, PA 15235 All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service;

/**
 * Business Service Interface to handle communication between presentation and
 * persistence layer for loading container related operations.
 */
public interface LoadingContainerManager extends LoadingContainerManagerRoot {

}
