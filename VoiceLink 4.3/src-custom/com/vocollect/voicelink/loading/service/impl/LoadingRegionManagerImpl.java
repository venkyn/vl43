/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.voicelink.loading.dao.LoadingRegionDAO;
import com.vocollect.voicelink.loading.service.LoadingRegionManager;


/**
 * Additional service methods for the <code>LoadingRegion</code> model object.
 * 
 */
public class LoadingRegionManagerImpl extends LoadingRegionManagerImplRoot
     implements LoadingRegionManager {  
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LoadingRegionManagerImpl(LoadingRegionDAO primaryDAO) {
        super(primaryDAO);
    }

}
