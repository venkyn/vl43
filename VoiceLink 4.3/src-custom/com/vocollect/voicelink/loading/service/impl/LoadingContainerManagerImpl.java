/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import com.vocollect.voicelink.loading.dao.LoadingContainerDAO;
import com.vocollect.voicelink.loading.service.LoadingContainerManager;

/**
 * Additional service methods for Loading container manager go in this class
 */
public class LoadingContainerManagerImpl extends
    LoadingContainerManagerImplRoot implements LoadingContainerManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LoadingContainerManagerImpl(LoadingContainerDAO primaryDAO) {
        super(primaryDAO);
    }

}
