/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import java.util.Date;
import java.util.List;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.loading.dao.ArchiveLoadingLaborDAO;
import com.vocollect.voicelink.loading.service.ArchiveLoadingLaborManager;

/**
 * 
 *
 * @author yarora
 */
public class ArchiveLoadingLaborManagerImpl extends ArchiveLoadingLaborManagerImplRoot
implements ArchiveLoadingLaborManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ArchiveLoadingLaborManagerImpl(ArchiveLoadingLaborDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.service.LoadingRouteLaborManagerRoot#listLaborSummaryReportRecords(java.lang.Long,
     *      java.util.Date, java.util.Date)
     */
    public List<Object[]> listLaborSummaryReportRecords(
            String operatorIdentifier, Date startDate, Date endDate)
            throws DataAccessException {
        return this.getPrimaryDAO().listLaborSummaryReportRecords(
                operatorIdentifier, startDate, endDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.service.LoadingRouteLaborManagerRoot#listLaborDetailReportRecords(java.lang.Long,
     *      java.util.Date, java.util.Date)
     */
    public List<Object[]> listLaborDetailReportRecords(String operatorId,
            Date startDate, Date endDate) throws DataAccessException {
        return this.getPrimaryDAO().listLaborDetailReportRecords(
                operatorId, startDate, endDate);
    }
}
