/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.service.impl;

import java.util.Date;
import java.util.List;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.loading.dao.LoadingRouteLaborDAO;
import com.vocollect.voicelink.loading.service.LoadingRouteLaborManager;

/**
 * 
 *
 * @author ktanneru
 */
public class LoadingRouteLaborManagerImpl extends LoadingRouteLaborManagerImplRoot
implements LoadingRouteLaborManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LoadingRouteLaborManagerImpl(LoadingRouteLaborDAO primaryDAO) {
        super(primaryDAO);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.service.LoadingRouteLaborManagerRoot#listLaborSummaryReportRecords(java.lang.Long,
     *      java.util.Date, java.util.Date)
     */
    public List<Object[]> listLaborSummaryReportRecords(
            Long operatorIdentifier, Date startDate, Date endDate)
            throws DataAccessException {
        return this.getPrimaryDAO().listLaborSummaryReportRecords(
                operatorIdentifier, startDate, endDate);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.loading.service.LoadingRouteLaborManagerRoot#listLaborDetailReportRecords(java.lang.Long,
     *      java.util.Date, java.util.Date)
     */
    public List<Object[]> listLaborDetailReportRecords(Long operatorId,
            Date startDate, Date endDate) throws DataAccessException {
        return this.getPrimaryDAO().listLaborDetailReportRecords(
                operatorId, startDate, endDate);
    }
}
