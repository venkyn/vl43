/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.task;


/**
 * Command class for Loading Request Region.
 * 
 * @author kudupi
 */
public class LoadingRequestRegionCmd extends LoadingRequestRegionCmdRoot {

    /**
     * 
     */
    private static final long serialVersionUID = -6777059049636729842L;
}
