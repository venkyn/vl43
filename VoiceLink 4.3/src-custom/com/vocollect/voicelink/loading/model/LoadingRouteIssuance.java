/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * Enum definition for Loading Route Issuance Region parameter. 
 * @author kudupi
 *
 */
public enum LoadingRouteIssuance 
        implements ValueBasedEnum<LoadingRouteIssuance> {
    
    Automatic(0), // We will be not using Automatic for VoiceLink 4.1 Release
    Manual(1);
    
    // Internal map of int values to enum values.
    private static ReverseEnumMap<LoadingRouteIssuance> toValueMap = 
        new ReverseEnumMap<LoadingRouteIssuance>(
                LoadingRouteIssuance.class);

    private int value;
    
    /**
     * Private Constructor
     * @param value the associated int representation.
     */
    private LoadingRouteIssuance(int value) {
        this.value = value;
    }
    
    /**
     * method to get resource key associated with the enum value.
     * @return - String for resource key
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public LoadingRouteIssuance fromValue(int val) {
        return toValueMap.get(val);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.SelectionRegionGoBackForShorts#fromValue(Integer)
     */
    public LoadingRouteIssuance fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }
    
    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static LoadingRouteIssuance toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }

}
