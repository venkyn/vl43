/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

/**
 * @author mraj
 * 
 */
public class LoadingTruckDiagramRow extends LoadingTruckDiagramRowRoot {

    /**
     * Constructor.
     */
    public LoadingTruckDiagramRow() {
        super();
    }

    /**
     * Constructor.
     * @param routeNumber number of the route
     * @param trailer trailer number of the route
     * @param dockDoor dock door location of the route
     * @param truckWeight summation of all the containers loaded on the truck
     * @param containerNumberLeft container number
     * @param customerNumberLeft customer number
     * @param containerWeightLeft container weight
     * @param containerPosition loaded container position
     */
    public LoadingTruckDiagramRow(String routeNumber,
                                  String trailer,
                                  String dockDoor,
                                  Double truckWeight,
                                  String containerNumberLeft,
                                  String customerNumberLeft,
                                  Double containerWeightLeft,
                                  Integer containerPosition) {
        super(routeNumber, trailer, dockDoor, truckWeight, containerNumberLeft,
              customerNumberLeft, containerWeightLeft, containerPosition);
    }

}
