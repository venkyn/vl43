/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * @author pfunyak
 *
 */
public enum LoadingContainerType  implements ValueBasedEnum<LoadingContainerType>  {
    Unknown(1),
    Imported(2),
    Selection(3),
    PutToStore(4);

    
    private int value;
    
    // Internal map of int values to enum values.
    private static ReverseEnumMap<LoadingContainerType> toValueMap =
        new ReverseEnumMap<LoadingContainerType>(LoadingContainerType.class);

    
    /**
     * Constructor.
     * @param value the associated int rep.
     */
    private LoadingContainerType(int value) {
        this.value = value;
    }
    
    /**
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     * @return value
     */
    @Override
    public int toValue() {
        return this.value;
    }

    /**
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     *  @param val - value to convert
     *  @return status * 
     */
    @Override
    public LoadingContainerType fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(java.lang.Integer)
     * @param valObject 
     * @return status
     */
    @Override
    public LoadingContainerType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#getResourceKey()
     * @return key
     */
    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

}
