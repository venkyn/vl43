/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * This enumeration holds the special load positions like "LEFT" or "RIGHT"
 * 
 * The primary intention of this enum is to hold the numeric value of the words
 * "LEFT", "RIGHT" etc spoken by the operator at capture load position prompt in
 * loading. This enumeration is not mapped with hibernate as this moment.
 * 
 * @author khazra
 * 
 */
public enum LoadPosition implements ValueBasedEnum<LoadPosition> {
    LEFT(-1), RIGHT(-2);

    private int value;
    
    // Internal map of int values to enum values.
    private static ReverseEnumMap<LoadPosition> toValueMap =
        new ReverseEnumMap<LoadPosition>(LoadPosition.class);

    /**
     * Constructor.
     * 
     * @param value the int value
     */
    private LoadPosition(int value) {
        this.value = value;
    }

    @Override
    public int toValue() {
        return this.value;
    }

    @Override
    public LoadPosition fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public LoadPosition fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }
    
    /**
     * @param statuses the set of statuses to compare to
     * @return true if this is in the set, false otherwise.
     */
    public boolean isInSet(LoadPosition... statuses) {
        for (LoadPosition status : statuses) {
            if (this.equals(status)) {
                return true;
            }
        }
        // No match
        return false;
    }

}
