/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

/**
 * @author mraj
 * 
 */
public class ArchiveLoadingContainer extends ArchiveLoadingContainerRoot {

    /**
     * 
     */
    private static final long serialVersionUID = 7198415269509591462L;

    /**
     * Default constructor
     */
    public ArchiveLoadingContainer() {
        super();
    }

    /**
     * @param container Loading container object to be archived
     * @param arStop Archive stop object
     */
    public ArchiveLoadingContainer(LoadingContainer container,
                                   ArchiveLoadingStop arStop) {
        super(container, arStop);
    }

}
