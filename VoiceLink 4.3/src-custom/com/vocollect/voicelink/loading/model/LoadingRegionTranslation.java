/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

import com.vocollect.voicelink.core.model.RegionTranslation;

/**
 * Translation class for LoadingRegion Sorting. This has
 * no additional properties, it is just here to support
 * Hibernate mapping.
 * 
 * Typically, if you add properties, you will add them to
 * <code>RegionTranslation</code> so all Region types get them.
 * 
 */
public class LoadingRegionTranslation extends RegionTranslation {

}
