/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.model.Taggable;

import java.io.Serializable;

/**
 * Class for Loading Summary Screens
 * 
 */
public class LoadingSummary extends LoadingSummaryRoot implements Serializable,
    Taggable {

    /**
     * 
     */
    private static final long serialVersionUID = -675312188092102884L;

}
