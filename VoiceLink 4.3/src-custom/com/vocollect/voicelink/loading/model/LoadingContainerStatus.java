/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * @author pfunyak
 *
 */
public enum LoadingContainerStatus implements ValueBasedEnum<LoadingContainerStatus> {
    NotLoaded(1),
    InProgress(2),
    Loaded(3),
    Consolidated(4),
    Unloaded(5),
    Cancelled(6);
    
    private int value;
    
    // Internal map of int values to enum values.
    private static ReverseEnumMap<LoadingContainerStatus> toValueMap =
        new ReverseEnumMap<LoadingContainerStatus>(LoadingContainerStatus.class);
    
    /**
     * Constructor.
     * @param value the associated int rep.
     */
    private LoadingContainerStatus(int value) {
        this.value = value;
    }
        
    
    /**
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     * @return value
     */
    @Override
    public int toValue() {
        return this.value;
    }

    /**
     *  @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     *  @param val - value to convert
     *  @return status 
     */
    @Override
    public LoadingContainerStatus fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(java.lang.Integer)
     * @param valObject 
     * @return status
     */
    @Override
    public LoadingContainerStatus fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#getResourceKey()
     * @return key
     */
    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }


    /**
     * @param statuses the set of statuses to compare to
     * @return true if this is in the set, false otherwise.
     */
    public boolean isInSet(LoadingContainerStatus... statuses) {
        for (LoadingContainerStatus status : statuses) {
            if (this.equals(status)) {
                return true;
            }
        }
        // No match
        return false;
    }
    
    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static LoadingContainerStatus toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
