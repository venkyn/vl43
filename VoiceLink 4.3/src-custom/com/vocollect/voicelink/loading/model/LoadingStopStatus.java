/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * @author pfunyak
 *
 */
public enum LoadingStopStatus implements ValueBasedEnum<LoadingStopStatus> {
    NotLoaded(1), 
    InProgress(2),
    Loaded(3);
    
    // Internal map of int values to enum values.
    private static ReverseEnumMap<LoadingStopStatus> toValueMap =
        new ReverseEnumMap<LoadingStopStatus>(LoadingStopStatus.class);
    
    private int value;
    
    /**
     * Constructor.
     * @param value the associated int rep.
     */
    private LoadingStopStatus(int value) {
        this.value = value;
    }
    

    @Override
    public int toValue() {
        return this.value;
    }

    @Override
    public LoadingStopStatus fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public LoadingStopStatus fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }


    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

}
