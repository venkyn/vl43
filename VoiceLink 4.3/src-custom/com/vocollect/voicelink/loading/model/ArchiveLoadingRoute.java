/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;


/**
 * @author mraj
 *
 */
public class ArchiveLoadingRoute extends ArchiveLoadingRouteRoot {

    /**
     * 
     */
    public ArchiveLoadingRoute() {
        super();
    }

    /**
     * @param route Loading route object to be archived
     */
    public ArchiveLoadingRoute(LoadingRoute route) {
        super(route);
    }

    
}
