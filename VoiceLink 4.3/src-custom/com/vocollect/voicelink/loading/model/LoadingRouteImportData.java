/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;


/**
 * Customization point for Loading Route import data.
 *
 */
public class LoadingRouteImportData extends LoadingRouteImportDataRoot {

    /**
     * 
     */
    private static final long serialVersionUID = -6987810275632651728L;

}
