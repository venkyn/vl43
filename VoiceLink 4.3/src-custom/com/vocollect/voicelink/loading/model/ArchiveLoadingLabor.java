/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;


/**
 * @author mraj
 *
 */
public class ArchiveLoadingLabor extends ArchiveLoadingLaborRoot {

    /**
     * 
     */
    private static final long serialVersionUID = -191055629049673377L;

    /**
     * @param labor Loading labor object to be archived
     * @param arRoute Archive Loading Route object 
     */
    public ArchiveLoadingLabor(LoadingRouteLabor labor,
                               ArchiveLoadingRoute arRoute) {
        super(labor, arRoute);
    }

}
