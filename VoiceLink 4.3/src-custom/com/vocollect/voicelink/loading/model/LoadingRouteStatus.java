/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.loading.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * Loading route status enums
 *
 */
public enum LoadingRouteStatus implements ValueBasedEnum<LoadingRouteStatus> {
    Available(1), 
    Reserved(2), 
    InProgress(3),
    Complete(4),
    UnAvailable(5),
    Suspended(6);
    

    // Internal map of int values to enum values.
    private static ReverseEnumMap<LoadingRouteStatus> toValueMap =
        new ReverseEnumMap<LoadingRouteStatus>(LoadingRouteStatus.class);
    
    private int value;
    
    /**
     * Constructor.
     * @param value the associated int rep.
     */
    private LoadingRouteStatus(int value) {
        this.value = value;
    }
    
    @Override
    public int toValue() {
        return this.value;
    }

    @Override
    public LoadingRouteStatus fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public LoadingRouteStatus fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }
    
    /**
     * @param statuses the set of statuses to compare to
     * @return true if this is in the set, false otherwise.
     */
    public boolean isInSet(LoadingRouteStatus... statuses) {
        for (LoadingRouteStatus status : statuses) {
            if (this.equals(status)) {
                return true;
            }
        }
        // No match
        return false;
    }
    

}
