/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading.model;


/**
 * Additional properties for Loading Route object model go here 
 * 
 */
public class LoadingRoute extends LoadingRouteRoot {

    /**
     * 
     */
    private static final long serialVersionUID = 3427636365055281031L;

}
