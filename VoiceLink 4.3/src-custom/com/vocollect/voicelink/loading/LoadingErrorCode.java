/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.loading;

/**
 * Class to hold the instances of error codes for loading business. Although the
 * range for these error codes is 11000-11999, please use only 11000-11499 for
 * the predefined ones; 11500-11999 should be reserved for customization error
 * codes.
 * 
 */
public final class LoadingErrorCode extends LoadingErrorCodeRoot {
    
    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected LoadingErrorCode(long err) {
        // The error is defined in the LoadingErrorCode context.
        super(err);
    }

}
