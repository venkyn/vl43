/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;


import com.vocollect.voicelink.selection.dao.AssignmentImportDataDAO;

/**
 * @author dgold
 *
 */
public class AssignmentImportDataManagerImpl extends
        AssignmentImportDataManagerImplRoot {
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public AssignmentImportDataManagerImpl(AssignmentImportDataDAO primaryDAO) {
        super(primaryDAO);
    }


}
