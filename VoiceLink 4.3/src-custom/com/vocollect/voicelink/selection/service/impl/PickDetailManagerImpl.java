/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.voicelink.selection.dao.PickDetailDAO;
import com.vocollect.voicelink.selection.service.PickDetailManager;


/**
 * Additional service methods for the <code>PickDetail</code> model object.
 *
 */
public class PickDetailManagerImpl extends PickDetailManagerImplRoot 
    implements PickDetailManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PickDetailManagerImpl(PickDetailDAO primaryDAO) {
        super(primaryDAO);
    }

}

