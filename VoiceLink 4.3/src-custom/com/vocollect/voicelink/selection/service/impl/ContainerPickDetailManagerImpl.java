/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.voicelink.selection.dao.PickDetailDAO;
import com.vocollect.voicelink.selection.service.ContainerPickDetailManager;

/**
 * Additional service methods for the <code>ContainerPickDetail</code> model object
 * combined with the <code>Pick</code> model object.
 *
 */
public class ContainerPickDetailManagerImpl 
    extends ContainerPickDetailManagerImplRoot 
    implements ContainerPickDetailManager {
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ContainerPickDetailManagerImpl(PickDetailDAO primaryDAO) {
        super(primaryDAO);
    }
    
}
