/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.voicelink.selection.dao.AssignmentLaborDAO;
import com.vocollect.voicelink.selection.service.AssignmentLaborManager;


/**
 * Additional service methods for the <code>AssignmentLabor</code> model object.
 *
 */
public class AssignmentLaborManagerImpl extends AssignmentLaborManagerImplRoot 
    implements AssignmentLaborManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public AssignmentLaborManagerImpl(AssignmentLaborDAO primaryDAO) {
        super(primaryDAO);
    }
}
