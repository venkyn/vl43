/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.voicelink.selection.dao.ArchivePickDAO;
import com.vocollect.voicelink.selection.service.ArchivePickManager;


/**
 * Additional service methods for the <code>Pick</code> model object.
 *
 */
public class ArchivePickManagerImpl extends ArchivePickManagerImplRoot
    implements ArchivePickManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ArchivePickManagerImpl(ArchivePickDAO primaryDAO) {
        super(primaryDAO);
    }

}
