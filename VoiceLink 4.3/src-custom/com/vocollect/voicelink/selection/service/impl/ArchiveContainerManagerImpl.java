/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.voicelink.selection.dao.ArchiveContainerDAO;
import com.vocollect.voicelink.selection.service.ArchiveContainerManager;

/**
 *
 *
 * @author mraj
 */
public class ArchiveContainerManagerImpl extends
    ArchiveContainerManagerImplRoot implements ArchiveContainerManager {

    /**
     * Constructor.
     * @param primaryDAO primaryDAO
     */
    public ArchiveContainerManagerImpl(ArchiveContainerDAO primaryDAO) {
        super(primaryDAO);
    }

}
