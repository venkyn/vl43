/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.service.impl;

import com.vocollect.voicelink.selection.dao.SummaryPromptDefinitionDAO;
import com.vocollect.voicelink.selection.service.SummaryPromptDefinitionManager;


/**
 * Additional service methods for the <code>SummaryPromptDefintion</code> model object.
 *
 * @author mkoenig
 */
public class SummaryPromptDefinitionManagerImpl extends
    SummaryPromptDefinitionManagerImplRoot 
    implements SummaryPromptDefinitionManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for the manager
     */
    public SummaryPromptDefinitionManagerImpl(SummaryPromptDefinitionDAO primaryDAO) {
        super(primaryDAO);
    }

}
