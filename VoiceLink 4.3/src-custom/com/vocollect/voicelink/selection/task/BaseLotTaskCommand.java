/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.task;

/**
 * Base task command object for lot commands. Contains commonly used
 * methods for lot validation.
 */
public class BaseLotTaskCommand extends BaseLotTaskCommandRoot {

    private static final long serialVersionUID = -1603245905731592055L;

}
