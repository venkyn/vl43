/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

/**
 * Customization extension for the Importable Assignment.
 * @author dgold
 *
 */
public class AssignmentImportData extends AssignmentImportDataRoot {

    private static final long serialVersionUID = -1183938922463144826L;

}
