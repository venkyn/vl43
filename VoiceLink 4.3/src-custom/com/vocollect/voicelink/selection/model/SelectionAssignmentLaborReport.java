package com.vocollect.voicelink.selection.model;

import java.util.Date;

public class SelectionAssignmentLaborReport extends
        SelectionAssignmentLaborReportRoot {

    public SelectionAssignmentLaborReport(String operatorName,
            String operatorId, Date endTime, Date startTime,
            Integer proRateCount, Double percentOfGoal, Integer quantityPicked,
            Long groupNumber, Integer groupCount, Integer groupProRateCount,
            String assignmentNumber) {
        super(operatorName, operatorId, endTime, startTime, proRateCount,
                percentOfGoal, quantityPicked, groupNumber, groupCount,
                groupProRateCount, assignmentNumber);
        // TODO Auto-generated constructor stub
    }

}
