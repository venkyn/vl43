/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import java.util.Set;


/**
 * Model object representing a VoiceLink Archive Selection Pick.
 * 
 */
public class ArchivePick extends ArchivePickRoot {

    //
    private static final long serialVersionUID = 4148390107580118549L;
    
    /**
     * 
     * Constructor.
     * @param pick Pick to use to consturct ArchivePick.
     * @param archiveAssignment - archive assignment to add to 
     * @param containers - archive containers in assignment
     */
    public ArchivePick(Pick pick, ArchiveAssignment archiveAssignment,
                       Set<ArchiveContainer> containers) {
        super(pick, archiveAssignment, containers);
    }

    /**
     * 
     * Constructor.
     */
    public ArchivePick() {
        super();
    }

}
