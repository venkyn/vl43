/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import java.util.Date;

/**
 *
 *
 * @author mnichols
 */
public class ContainerReport extends ContainerReportRoot {

    /**
     * Constructor.
     * @param containerNumber - the container number
     * @param containerStatus - the container status
     * @param quantityPicked - the quantity picked
     * @param itemDescription - the item description
     * @param itemNumber - the item number
     * @param operatorName - the operator number
     * @param operatorId - the operator id
     * @param pickTime - the pick time
     * @param aisle - the aisle
     * @param postAisle - the post aisle
     * @param preAisle - the pre aisle
     * @param slot - the slot
     * @param pickId - the pick id
     * @param pickDetailId - the pick detail id
     * @param pickStatus - the pick status
     * @param serialNumber - the serial number
     * @param lotNumber - the lot number
     * @param variableWeight - the variable weight
     */
    public ContainerReport(String containerNumber,
                           ContainerStatus containerStatus,
                           Integer quantityPicked,
                           String itemDescription,
                           String itemNumber,
                           String operatorName,
                           String operatorId,
                           Date pickTime,
                           String aisle,
                           String postAisle,
                           String preAisle,
                           String slot,
                           Long pickId,
                           Long pickDetailId,
                           PickStatus pickStatus,
                           String serialNumber,
                           String lotNumber,
                           Double variableWeight) {
        super(
            containerNumber, containerStatus, quantityPicked, itemDescription,
            itemNumber, operatorName, operatorId, pickTime, aisle, postAisle, preAisle,
            slot, pickId, pickDetailId, pickStatus, serialNumber, lotNumber,
            variableWeight);
    }


}
