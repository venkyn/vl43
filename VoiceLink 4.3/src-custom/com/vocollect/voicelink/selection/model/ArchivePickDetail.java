/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import java.util.Set;


/**
 * Model object representing a VoiceLink Archive Selection Pick Details.
 * 
 */
public class ArchivePickDetail extends ArchivePickDetailRoot {

    //
    private static final long serialVersionUID = 8006873534852570538L;
    
    /**
     * 
     * Constructor.
     * @param pd PickDetail used to construct ArchivePickDetail
     * @param archivePick - archive pick detail belongs to 
     * @param containers - archive containers for assignment
     */
    public ArchivePickDetail(PickDetail pd, ArchivePick archivePick, 
                             Set<ArchiveContainer> containers) {
        super(pd, archivePick, containers);
    }

    /**
     * 
     * Constructor.
     */
    public ArchivePickDetail() {
        super();
    }
    
}
