/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 *
 *
 * @author mkoenig
 */
public enum PickStatus implements ValueBasedEnum<PickStatus> {
    BaseItem(1), 
    NotPicked(2),
    Skipped(3),
    Shorted(4),
    Picked(5),
    Assigned(6),
    Markout(7),
    Canceled(8),
    ShortsGoBack(9),
    Partial(10);
// NOTE:  If the above enum is modified in any way, also update the documentation
//        in Pick.hbm.xml for the db export fields.
    
    
    
    // Internal map of int values to enum values.
    private static ReverseEnumMap<PickStatus> toValueMap =
        new ReverseEnumMap<PickStatus>(PickStatus.class);
    
    private int value;

    /**
     * Constructor.
     * @param value - value to intialize to
     */
    private PickStatus(int value) {
        this.value = value;
    }

    /**
     * method to get resource key value.
     * @return - String for resource key
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * @param statuses the set of statuses to compare to
     * @return true if this is in the set, false otherwise.
     */
    public boolean isInSet(PickStatus... statuses) {
        for (PickStatus status : statuses) {
            if (this.equals(status)) {
                return true;
            }
        }
        // No match
        return false;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public PickStatus fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.PickStatus#fromValue(Integer)
     */
    public PickStatus fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static PickStatus toEnum(int val) throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
