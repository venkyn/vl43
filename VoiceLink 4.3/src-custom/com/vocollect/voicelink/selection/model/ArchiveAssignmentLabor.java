/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;


/**
 * Model object representing a VoiceLink Archive Selection Assignment.
 * 
 */
public class ArchiveAssignmentLabor extends ArchiveAssignmentLaborRoot {

    //
    private static final long serialVersionUID = 6480571401517467618L;
    
    /**
     * 
     * Constructor.
     * @param assignmentLabor - original labor record.
     * @param assignment - Archive assignment labor belongs to.
     */
    public ArchiveAssignmentLabor(AssignmentLabor assignmentLabor, 
                                  ArchiveAssignment assignment) {
        super(assignmentLabor, assignment);
    }
    
    /**
     * 
     * Constructor.
     */
    public ArchiveAssignmentLabor() {
        super();
    }

}
