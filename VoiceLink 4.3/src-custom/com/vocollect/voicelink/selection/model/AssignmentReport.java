/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import java.util.Date;


/**
 * 
 *
 * @author mnichols
 */
public class AssignmentReport extends AssignmentReportRoot {

    /**
     * Constructor.
     * @param assignmentNumber assignment number
     * @param workIdentifier work identifier
     * @param assignmentStatus assignment status
     * @param customerNumber customer number
     * @param route route
     * @param containerNumber container number
     * @param containerStatus container status
     * @param containerType container type
     * @param pickID pick id
     * @param preAisle location pre-aisle
     * @param aisle location aisle
     * @param postAisle location post-aisle
     * @param slot location slot
     * @param itemNumber item number
     * @param itemDescription item description
     * @param quantityPicked pick quantity
     * @param quantityToPick picks remaining
     * @param pickTime time of pick
     * @param operatorId operator ID
     * @param operatorName operator name
     * @param variableWeight variable weight
     * @param serialNumber serial number
     * @param lotNumber lot number
     * @param pickStatus status of pick
     */
    public AssignmentReport(Long assignmentNumber,
                                String workIdentifier,
                                AssignmentStatus assignmentStatus,
                                String customerNumber,
                                String route,
                                String containerNumber,
                                ContainerStatus containerStatus,
                                Long containerType,
                                Long pickID,
                                String preAisle,
                                String aisle,
                                String postAisle,
                                String slot,
                                String itemNumber,
                                String itemDescription,
                                Integer quantityPicked,
                                Integer quantityToPick,
                                Date pickTime,
                                String operatorId,
                                String operatorName,
                                Double variableWeight,
                                String serialNumber,
                                String lotNumber,
                                PickStatus pickStatus) {
        
        
        super(
            assignmentNumber, workIdentifier, assignmentStatus, customerNumber, route,
            containerNumber, containerStatus, containerType, pickID, preAisle, aisle,
            postAisle, slot, itemNumber, itemDescription, quantityPicked,
            quantityToPick, pickTime, operatorId, operatorName, variableWeight,
            serialNumber, lotNumber, pickStatus);
    }    
    
    /**
     * Constructor.
     * @param assignmentNumber assignment number
     * @param workIdentifier work identifier
     * @param assignmentStatus assignment status
     * @param customerNumber customer number
     * @param route route
     * @param containerNumber container number
     * @param containerStatus container status
     * @param containerType container type
     * @param pickID pick id
     * @param preAisle location pre-aisle
     * @param aisle location aisle
     * @param postAisle location post-aisle
     * @param slot location slot
     * @param itemNumber item number
     * @param itemDescription item description
     * @param quantityPicked pick quantity
     * @param quantityToPick picks remaining
     * @param pickTime time of pick
     * @param operatorId operator ID
     * @param operatorName operator name
     * @param variableWeight variable weight
     * @param serialNumber serial number
     * @param lotNumber lot number
     * @param pickStatus status of pick
     */
    public AssignmentReport(String assignmentNumber,
                            String workIdentifier,
                            AssignmentStatus assignmentStatus,
                            String customerNumber,
                            String route,
                            String containerNumber,
                            ContainerStatus containerStatus,
                            Long containerType,
                            Long pickID,
                            String preAisle,
                            String aisle,
                            String postAisle,
                            String slot,
                            String itemNumber,
                            String itemDescription,
                            Integer quantityPicked,
                            Integer quantityToPick,
                            Date pickTime,
                            String operatorId,
                            String operatorName,
                            Double variableWeight,
                            String serialNumber,
                            String lotNumber,
                            PickStatus pickStatus) {
        super(
            assignmentNumber, workIdentifier, assignmentStatus, customerNumber, route,
            containerNumber, containerStatus, containerType, pickID, preAisle, aisle,
            postAisle, slot, itemNumber, itemDescription, quantityPicked,
            quantityToPick, pickTime, operatorId, operatorName, variableWeight,
            serialNumber, lotNumber, pickStatus);
    }

}
