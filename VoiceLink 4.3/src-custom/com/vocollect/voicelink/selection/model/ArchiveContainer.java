/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;


/**
 * Model object representing a VoiceLink Archive Selection Container.
 * 
 */
public class ArchiveContainer extends ArchiveContainerRoot {

    //
    private static final long serialVersionUID = -3657499873087189673L;
    
    /**
     * 
     * Constructor.
     * @param container container to use to construct ArchiveContainer
     * @param assignment - assignment container belongs to.
     */
    public ArchiveContainer(Container container, ArchiveAssignment assignment) {
        super(container, assignment);
    }
    
    /**
     * 
     * Constructor.
     */
    public ArchiveContainer() {
        super();
    }
    

}
