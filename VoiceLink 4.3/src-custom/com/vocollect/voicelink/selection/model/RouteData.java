/*
 * Copyright (c) 2009 Vocollect, Inc. Pittsburgh, PA 15235 All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.selection.model;

/**
 * Model representing data for a route.
 * @author amracna
 *
 */
public class RouteData {
    
    private static final double ONE_HUND_PERCENT = 100.00;
    private int casesPicked = 0;
    private int casesRemaining = 0;

    /**
     * @return the casesPicked
     */
    public int getCasesPicked() {
        return casesPicked;
    }

    /**
     * @param casesPicked the casesPicked to set
     */
    public void setCasesPicked(int casesPicked) {
        this.casesPicked = casesPicked;
    }

    /**
     * @return the casesRemaining
     */
    public int getCasesRemaining() {
        return casesRemaining;
    }

    /**
     * @param casesRemaining the casesRemaining to set
     */
    public void setCasesRemaining(int casesRemaining) {
        this.casesRemaining = casesRemaining;
    }

    /**
     * @return Double
     */
    public Double getCompletionPercentage() {
        Double totalCases = 
            Integer.valueOf(casesPicked + casesRemaining).doubleValue();
        return casesPicked / totalCases * ONE_HUND_PERCENT;
    }
    
    /**
     * Returns the status of the route based on
     * whether or not there are any cases remaining.
     * @return RouteStatus - the route status
     */
    public RouteStatus getStatus() {
        if (casesRemaining == 0) {
            return RouteStatus.Complete;
        } else if (casesPicked == 0) {
            return RouteStatus.NotStarted;
        }
        
        return RouteStatus.InProgress;
    }

}
