/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * 
 *
 * @author mkoenig
 */
public enum SelectionRegionAutoMarkOutShorts 
    implements ValueBasedEnum<SelectionRegionAutoMarkOutShorts> {
    
    Never(1), 
    WhenNotReplenished(2);

    // Internal map of int values to enum values.
    private static ReverseEnumMap<SelectionRegionAutoMarkOutShorts> toValueMap =
        new ReverseEnumMap<SelectionRegionAutoMarkOutShorts>(
            SelectionRegionAutoMarkOutShorts.class);
    
    private int value;

    /**
     * Constructor.
     * @param value the associated int rep.
     */
    private SelectionRegionAutoMarkOutShorts(int value) {
        this.value = value;
    }

    /**
     * method to get resource key associated with the enum value.
     * @return - String for resource key
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public SelectionRegionAutoMarkOutShorts fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.SelectionRegionAutoMarkOutShorts#fromValue(Integer)
     */
    public SelectionRegionAutoMarkOutShorts fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static SelectionRegionAutoMarkOutShorts toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
