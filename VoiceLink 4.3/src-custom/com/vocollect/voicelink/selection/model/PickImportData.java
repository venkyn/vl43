/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;


/**
 * Customization point for Pick import data.
 *
 */
public class PickImportData extends PickImportDataRoot {

    private static final long serialVersionUID = -4791478502146046315L;

}
