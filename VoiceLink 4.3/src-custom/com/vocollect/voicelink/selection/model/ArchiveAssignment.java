/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;


/**
 * Model object representing a VoiceLink Archive Selection Assignment.
 * 
 */
public class ArchiveAssignment extends ArchiveAssignmentRoot {

    //
    private static final long serialVersionUID = 6480571401517467618L;
    
    /**
     * 
     * Constructor.
     * @param assignment Assignment to use to construct an ArchiveAssignment.
     */
    public ArchiveAssignment(Assignment assignment) {
        super(assignment);
    }
    
    /**
     * 
     * Constructor.
     */
    public ArchiveAssignment() {
        super();
    }

}
