/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import java.io.Serializable;

/**
 * This class is a component that groups together the fields that
 * make up the user defineable settings for a region.
 */
public class RegionUserSettings 
    extends RegionUserSettingsRoot implements Serializable, Cloneable {
    //
    private static final long serialVersionUID = 7208903822651043377L;
}
