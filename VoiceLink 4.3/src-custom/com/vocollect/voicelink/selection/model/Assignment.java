/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.model;

import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.Resequencable;

import java.io.Serializable;

/**
 * Model object representing a VoiceLink Selection assignment.
 * 
 */
public class Assignment extends AssignmentRoot implements Serializable, Taggable, Resequencable {

   private static final long serialVersionUID = 6480571401517467618L;

}
