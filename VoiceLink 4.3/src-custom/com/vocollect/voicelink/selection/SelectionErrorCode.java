/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection;



/**
 * Class to hold the instances of error codes for selection business. Although the
 * range for these error codes is 3000-3999, please use only 3000-3499 for
 * the predefined ones; 3500-3999 should be reserved for customization error
 * codes.
 */
public final class SelectionErrorCode extends SelectionErrorCodeRoot {

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected SelectionErrorCode(long err) {
        // The error is defined in the SelectionErrorCode context.
        super(err);
    }

}
