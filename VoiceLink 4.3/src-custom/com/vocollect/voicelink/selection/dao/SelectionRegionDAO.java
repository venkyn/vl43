/*
 * Copyright (c) 2006 Vocollect, Inc. Pittsburgh, PA 15235 All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.selection.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.selection.model.SelectionRegion;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Selection Region Data Access Object (DAO) interface.
 * 
 */
public interface SelectionRegionDAO extends SelectionRegionDAORoot {
    
    /**
     * @return List<SelectionRegion>
     * @throws DataAccessException
     */
    List<SelectionRegion> listAllRegionsOrderByNumber() throws DataAccessException;
    
}
