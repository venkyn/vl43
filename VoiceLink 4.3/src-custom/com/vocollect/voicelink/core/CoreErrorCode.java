/*
 * Copyright (c) 2006 Vocollect, Inc. 
 * Pittsburgh, PA 15235 
 * All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core;



/**
 * Class to hold the instances of error codes for core business. Although the
 * range for these error codes is 2000-2999, please use only 2000-2499 for
 * the predefined ones; 2500-2999 should be reserved for customization error
 * codes.
 * 
 * @author mkoenig
 */
public class CoreErrorCode extends CoreErrorCodeRoot {

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected CoreErrorCode(long err) {
        // The error is defined in the TaskErrorCode context.
        super(err);
    }
    
    public static final CoreErrorCode ERROR_RETRIEVING_CHART_DISPLAY_SIZE = new CoreErrorCode(2500);
    
}
