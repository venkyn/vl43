/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * This enumeration holds the repair action codes to a vehicle safety check.
 * 
 * The primary intention of this enum is to hold the numeric value of the
 * possible repair action responses "No Action", "New Equipment", "Quick Repair"
 * spoken by the operator at Quick repair prompt prompt for vehicles which
 * failed any particular safety check and before finally failing the safety
 * check of that vehicle
 * 
 * @author mraj
 * 
 */
public enum VehicleSafetyCheckRepairActionType implements
    ValueBasedEnum<VehicleSafetyCheckRepairActionType> {
    NoAction(0), NewEquipment(1), QuickRepair(2);

    private int value;
    
    // Internal map of int values to enum values.
    private static ReverseEnumMap<VehicleSafetyCheckRepairActionType> toValueMap = 
        new ReverseEnumMap<VehicleSafetyCheckRepairActionType>(
            VehicleSafetyCheckRepairActionType.class);
    
    /**
     * Constructor.
     * 
     * @param value the int value
     */
    private VehicleSafetyCheckRepairActionType(int value) {
        this.value = value;
    }

    @Override
    public int toValue() {
        return this.value;
    }

    @Override
    public VehicleSafetyCheckRepairActionType fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public VehicleSafetyCheckRepairActionType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }
    
    /**
     * @param statuses the set of statuses to compare to
     * @return true if this is in the set, false otherwise.
     */
    public boolean isInSet(VehicleSafetyCheckRepairActionType... statuses) {
        for (VehicleSafetyCheckRepairActionType status : statuses) {
            if (this.equals(status)) {
                return true;
            }
        }
        // No match
        return false;
    }

}
