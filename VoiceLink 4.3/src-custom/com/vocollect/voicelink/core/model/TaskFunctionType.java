/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * Enum representing the types of available TaskFunctions.
 *
 * @author ddoubleday
 */
public enum TaskFunctionType implements ValueBasedEnum<TaskFunctionType> {
    Putaway(1), 
    Replenishment(2), 
    NormalAssignments(3),
    ChaseAssignments(4),
    NormalAndChaseAssignments(6),
    LineLoading(7),
    PutToStore(8),
    CycleCounting(9),
    Loading(10);
    
  
    // Internal map of int values to enum values.
    private static ReverseEnumMap<TaskFunctionType> toValueMap =
        new ReverseEnumMap<TaskFunctionType>(TaskFunctionType.class);
    
    private int value;

    /**
     * Constructor.
     * @param value the int rep value.
     */
    private TaskFunctionType(int value) {
        this.value = value;
    }

    /**
     * @return the associated resource key.
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public TaskFunctionType fromValue(int val) {
        return toEnum(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(java.lang.Integer)
     */
    public TaskFunctionType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static TaskFunctionType toEnum(int val) throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
