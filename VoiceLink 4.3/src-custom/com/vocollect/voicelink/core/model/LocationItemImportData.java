/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;



/**
 * @author jtauberg
 * 
 * Image of a LocationItem that we can import.
 * Using delegate methods, we can get data set when we get the importable completed, and can use the 
 * tool to do it. Additionally, any methods we need to override exist in the body.
 */
public class LocationItemImportData  extends LocationItemImportDataRoot {

    private static final long serialVersionUID = 865224150138564298L;
}
