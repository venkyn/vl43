/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * 
 *
 * @author mkoenig
 */
public enum ExportStatus implements ValueBasedEnum<ExportStatus> {
    NotExported(0), 
    InProgress(1), 
    Exported(2);

    // Internal map of int values to enum values.
    private static ReverseEnumMap<ExportStatus> toValueMap =
        new ReverseEnumMap<ExportStatus>(ExportStatus.class);
    
    private int value;

    /**
     * Constructor.
     * @param value the int rep value.
     */
    private ExportStatus(int value) {
        this.value = value;
    }

    /**
     * @return the associated resource key.
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public ExportStatus fromValue(int val) {
        return toEnum(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ExportStatus#fromValue(Integer)
     */
    public ExportStatus fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static ExportStatus toEnum(int val) throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
