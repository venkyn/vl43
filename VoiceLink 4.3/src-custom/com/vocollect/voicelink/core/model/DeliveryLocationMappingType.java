/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * A type of mapping for delivery locations. For example, could define rules
 * from customer number to delivery location, such that all assignments with
 * customer number '1234' go to delivery location '15'.
 * 
 * @author bnorthrop
 */
public enum DeliveryLocationMappingType implements
    ValueBasedEnum<DeliveryLocationMappingType> {

    /**
     * The different mapping types types.
     */
    CustomerNumber(1, false), Route(2, true);

    /**
     * Whether this mapping type is the default across VoiceLink. Important
     * Note: Only one mapping type should be set as the default.
     */
    private boolean isDefault;

    /**
     * Internal map of int values to enum values.
     */
    private static ReverseEnumMap<DeliveryLocationMappingType> toValueMap = 
        new ReverseEnumMap<DeliveryLocationMappingType>(
        DeliveryLocationMappingType.class);

    /**
     * The value of the mapping type.
     */
    private int value;

    /**
     * Constructor.
     * @param value the associated int rep.
     * @param isDefault - whether this is the default mapping type
     */
    private DeliveryLocationMappingType(int value, boolean isDefault) {
        this.value = value;
        this.isDefault = isDefault;
    }

    /**
     * method to get resource key associated with the enum value.
     * @return - String for resource key
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * get the value of the type.
     * @return - value of the enum
     */
    public Integer getValue() {
        return value;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public DeliveryLocationMappingType fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.voicelink.core.model.DeliveryLocationMappingType#fromValue(Integer)
     */
    public DeliveryLocationMappingType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static DeliveryLocationMappingType toEnum(int val)
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }

    /**
     * Getter for the isDefault property.
     * @return boolean value of the property
     */
    public boolean isDefault() {
        return this.isDefault;
    }

    /**
     * Alternate getter for the isDefault property.
     * @return boolean value of the property
     */
    public boolean getIsDefault() {
        return this.isDefault;
    }

    /**
     * Static helper method to loop through all mapping types and return the
     * default.
     * @return DeliveryLocationMappingType - the default.
     */
    public static DeliveryLocationMappingType getDefault() {
        for (DeliveryLocationMappingType mappingType : DeliveryLocationMappingType
            .values()) {
            if (mappingType.isDefault()) {
                return mappingType;
            }
        }
        throw new IllegalStateException(
            "No mapping type is configured as default.  Illegal customization.  Check DeliveryLocationMappingType.");
    }
    
    /**
     * Return the DeliveryLocationMappingType with the corresponding id.
     * @param id - the id of the mapping type to find.
     * @return DeliveryLocationMappingType - the mapping type with the given id.
     */
    public static DeliveryLocationMappingType get(Integer id) {
        for (DeliveryLocationMappingType mappingType : DeliveryLocationMappingType
            .values()) {
            if (mappingType.getValue().equals(id)) {
                return mappingType;
            }
        }
        throw new IllegalStateException(
            "No mapping type is configured with id: " + id + ".");
    }
}
