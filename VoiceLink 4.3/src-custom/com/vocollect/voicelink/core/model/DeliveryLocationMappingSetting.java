/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;


/**
 * A type of mapping for delivery locations. For example, could define rules
 * from customer number to delivery location, such that all assignments with
 * customer number '1234' go to delivery location '15'.
 * 
 * @author bnorthrop
 */
public class DeliveryLocationMappingSetting extends
    DeliveryLocationMappingSettingRoot {

    private static final long serialVersionUID = 6411938870900656891L;

    /**
     * Default Constructor.
     */
    public DeliveryLocationMappingSetting() {
        super();
    }

    /**
     * Convenience constructor.
     * @param mappingType - the type of mapping (e.g. customer number or route).
     */
    public DeliveryLocationMappingSetting(DeliveryLocationMappingType mappingType) {
        super(mappingType);
    }

}
