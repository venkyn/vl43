/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * 
 *
 * @author mkoenig
 */
//1=Selection, 2=Put Away, 3=Replenishment, 4=Line Loading 5=Put To Store
public enum RegionType implements ValueBasedEnum<RegionType> {
    Selection(1),
    PutAway(2),
    Replenishment(3),
    LineLoading(4),
    PutToStore(5),
    CycleCounting(6),
    Loading(7);

    // Internal map of int values to enum values.
    private static ReverseEnumMap<RegionType> toValueMap =
        new ReverseEnumMap<RegionType>(RegionType.class);
    
    private int value;

    /**
     * Constructor.
     * @param value the associated int rep.
     */
    private RegionType(int value) {
        this.value = value;
    }

    /**
     * method to get resource key associated with the enum value.
     * @return - String for resource key
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public RegionType fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.RegionType#fromValue(Integer)
     */
    public RegionType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static RegionType toEnum(int val) throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
