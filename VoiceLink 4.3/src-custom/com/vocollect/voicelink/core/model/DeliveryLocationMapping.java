/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;


/**
 * Model class for a delivery location mapping.
 * @see com.vocollect.voicelink.core.model.DeliveryLocationMappingRoot
 * @author Ben Northrop
 */
public class DeliveryLocationMapping extends DeliveryLocationMappingRoot {

    private static final long serialVersionUID = 3506670265103016411L;

    /**
     * Default constructor.
     */
    public DeliveryLocationMapping() {
        // default, do nothing
    }

    /**
     * Convenience constructor.
     * @param mappingValue - the value (e.g. customer number, route, etc.)
     * @param deliveryLocation - the id of the deliveryLocation
     * @param mappingType - the type of mapping (e.g. customer number, etc.)
     */
    public DeliveryLocationMapping(String mappingValue,
                                   String deliveryLocation,
                                   DeliveryLocationMappingType mappingType) {
        super(mappingValue, deliveryLocation, mappingType);
    }
}
