/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.DataObject;
import com.vocollect.epp.model.Sharable;
import com.vocollect.epp.model.Taggable;

import java.io.Serializable;

/**
 * Operator Model Object.
 */
public class Operator extends OperatorRoot 
    implements Serializable, DataObject, Taggable, Sharable {

    private static final long serialVersionUID = 5417486216405499229L;

}
