/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;


/**
 * 
 *
 * @author bnorthrop
 */
public enum ApplicationType {
    Selection(1), PutAway(2), LineLoading(3);
    
    private int id;
    
    /**
     * Constructor.
     * @param id the type ID.
     */
    private ApplicationType(int id) { 
        this.id = id;
    }

    
    /**
     * Getter for the id property.
     * @return int value of the property
     */
    public int getId() {
        return this.id;
    }

    
    /**
     * Setter for the id property.
     * @param id the new id value
     */
    public void setId(int id) {
        this.id = id;
    }
}
