/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 *
 *
 * @author mkoenig
 */
public enum OperatorLaborActionType
    implements ValueBasedEnum<OperatorLaborActionType> {
    SignOn(1),
    SignOff(2),
    Break(3),
    Selection(4),
    PutAway(5),
    Replenishment(6),
    LineLoading(7),
    PutToStore(8),
    DownTime(9),
    Loading(10),
    CycleCounting(11);
    //DownTime type added as part of requirement in Operator Labor detail report 
    // to be displayed as Operator action amongst others.

    // Internal map of int values to enum values.
    private static ReverseEnumMap<OperatorLaborActionType> toValueMap =
        new ReverseEnumMap<OperatorLaborActionType>(OperatorLaborActionType.class);

    private int value;

    /**
     * Constructor.
     * @param value the associated int rep.
     */
    private OperatorLaborActionType(int value) {
        this.value = value;
    }

    /**
     * method to get resource key associated with the enum value.
     * @return - String for resource key
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public OperatorLaborActionType fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.OperatorLaborActionType#fromValue(Integer)
     */
    public OperatorLaborActionType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static OperatorLaborActionType toEnum(int val)
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }

    /**
     * @return The Value
     */
    public int getValue() {
        return this.value;
    }
    
    /**
     * Gets the display value by resolving the resource key
     * @return the resource key translated to the proper local string
     */
    public String getDisplayValue() {
        return ResourceUtil.getLocalizedEnumName(this);
    }
}
