/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * @author mraj
 *
 */
public enum DataAggregatorFieldType implements
    ValueBasedEnum<DataAggregatorFieldType> {
        Minutes(0), Count(1), Time(2), Date(3), milliseconds(4);

        // Internal map of int values to enum values.
        private static ReverseEnumMap<DataAggregatorFieldType> toValueMap = new ReverseEnumMap<DataAggregatorFieldType>(
            DataAggregatorFieldType.class);
        
        private int value;
        
        /**
         * Constructor.
         * @param value the int rep value.
         */
        private DataAggregatorFieldType(int value) {
            this.value = value;
        }

        @Override
        public int toValue() {
            return this.value;
        }

        @Override
        public DataAggregatorFieldType fromValue(int val) {
            return toEnum(val);
        }

        @Override
        public DataAggregatorFieldType fromValue(Integer valObject) {
            return toValueMap.get(valObject.intValue());
        }

        @Override
        public String getResourceKey() {
            return ResourceUtil.makeEnumResourceKey(this);
        }
        
        /**
         * Return the enum constant that is mapped to the specified int.
         * @param val the int to map.
         * @return the enum constant.
         * @throws IllegalArgumentException if the val doesn't map to a constant.
         */
        public static DataAggregatorFieldType toEnum(int val)
            throws IllegalArgumentException {
            return toValueMap.get(val);
        }
}
