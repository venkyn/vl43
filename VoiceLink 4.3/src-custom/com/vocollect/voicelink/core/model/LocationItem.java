/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.model.Taggable;

import java.io.Serializable;


/**
 * Model for items defined at a location.
 *
 */
public class LocationItem extends LocationItemRoot 
implements Serializable, Taggable {

    //
    private static final long serialVersionUID = 12231137211755502L;

}
