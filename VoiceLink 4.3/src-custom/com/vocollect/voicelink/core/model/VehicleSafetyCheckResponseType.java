/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * This enumeration holds the responses to a vehicle safety check
 * 
 * The primary intention of this enum is to hold the numeric values:<BR>
 * "YES" and "NO" - responses spoken by the operator and passed in LUT requests<BR>
 * "INITIAL" - state of pre-created safety check responses <BR>
 * "BOOLEAN" and "NUMERIC" - flag values, sent in LUT responses indicating type
 * of response to be spoken by the operator for a safety check
 * 
 * @author mraj
 * 
 */
public enum VehicleSafetyCheckResponseType implements
    ValueBasedEnum<VehicleSafetyCheckResponseType> {
    YES(-1), NO(-2), INITIAL(-3), NOBUTCONTINUE(-4);

    private int value;

    // Internal map of int values to enum values.
    private static ReverseEnumMap<VehicleSafetyCheckResponseType> toValueMap = 
        new ReverseEnumMap<VehicleSafetyCheckResponseType>(
        VehicleSafetyCheckResponseType.class);

    /**
     * Constructor.
     * 
     * @param value the int value
     */
    private VehicleSafetyCheckResponseType(int value) {
        this.value = value;
    }

    @Override
    public int toValue() {
        return this.value;
    }

    @Override
    public VehicleSafetyCheckResponseType fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public VehicleSafetyCheckResponseType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * @param statuses the set of statuses to compare to
     * @return true if this is in the set, false otherwise.
     */
    public boolean isInSet(VehicleSafetyCheckResponseType... statuses) {
        for (VehicleSafetyCheckResponseType status : statuses) {
            if (this.equals(status)) {
                return true;
            }
        }
        // No match
        return false;
    }
}
