/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;



/**
 * This is the Command class to handle Send Safety Checklist LUT request.
 * This message is sent after the operator completes the safety checklist. 
 * The safety checklist is complete when the operator has said, "Yes," to 
 * all checks or, "No," to a single check. 
 *
 *@author kudupi
 */
public class SendSafetyChecklistCmd extends SendSafetyChecklistCmdRoot {

    //
    private static final long serialVersionUID = 2713004859817769804L;
}
