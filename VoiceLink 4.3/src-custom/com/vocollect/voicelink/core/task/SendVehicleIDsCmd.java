/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.task;



/**
 * This is the Command class to handle Send Vehicle ID LUT request.
 * Response of this task command will be either an empty list if
 * Vehicle ID is not sent and will send list of safety checks for 
 * the VehicleType sent in the request
 *
 *@author Ed Stoll
 *@author kudupi
 */
public class SendVehicleIDsCmd extends SendVehicleIDsCmdRoot {

    // Serial ID version 
    private static final long serialVersionUID = -8906841672455997644L;
    
    
}
