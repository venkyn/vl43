/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.core.model.OperatorTeam;

import java.util.List;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for OperatorTeam related operations.
 *
 * @author mnichols
 */
public interface OperatorTeamManager extends OperatorTeamManagerRoot {

    /**
     * @param regionId to retrieve operators by
     * @return list of operators currently working in region
     * @throws DataAccessException for any database exceptions
     */
    List<OperatorTeam> listAllOrderByName()
    throws DataAccessException;
}
