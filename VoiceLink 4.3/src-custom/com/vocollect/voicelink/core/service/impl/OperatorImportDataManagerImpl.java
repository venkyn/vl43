/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;


import com.vocollect.voicelink.core.dao.OperatorImportDataDAO;
import com.vocollect.voicelink.core.service.OperatorImportDataManager;

/**
 * Custom stub for the OperatorImportData manager class.
 * @author jtauberg
 *
 */
public class OperatorImportDataManagerImpl extends OperatorImportDataManagerImplRoot
    implements OperatorImportDataManager {
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager.
     */
    public OperatorImportDataManagerImpl(OperatorImportDataDAO primaryDAO) {
        super(primaryDAO);
    }
}
