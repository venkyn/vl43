/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.OperatorLaborDAO;
import com.vocollect.voicelink.core.service.OperatorActionsExportTransportManager;

/**
 *
 */
public class OperatorActionsExportTransportManagerImpl extends
        OperatorActionsExportTransportManagerImplRoot implements
        OperatorActionsExportTransportManager {

    /**
     * Default c'tor.
     * @param primaryDAO the DAO used to look up data.
     */
    public OperatorActionsExportTransportManagerImpl(OperatorLaborDAO primaryDAO) {
        super(primaryDAO);
    }
    
}
