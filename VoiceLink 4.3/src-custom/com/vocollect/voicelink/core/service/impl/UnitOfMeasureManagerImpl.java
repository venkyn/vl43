/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.UnitOfMeasureDAO;
import com.vocollect.voicelink.core.service.UnitOfMeasureManager;

/**
 * 
 *
 */
public class UnitOfMeasureManagerImpl extends UnitOfMeasureManagerImplRoot implements
        UnitOfMeasureManager {

    /**
     * @param primaryDAO - the primary DAO
     */
    public UnitOfMeasureManagerImpl(UnitOfMeasureDAO primaryDAO) {
        super(primaryDAO);
    }

}
