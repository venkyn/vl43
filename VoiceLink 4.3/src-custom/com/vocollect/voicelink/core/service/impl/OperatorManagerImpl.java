/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.OperatorDAO;
import com.vocollect.voicelink.core.service.OperatorManager;

/**
 * Additional service methods for the <code>Operator</code> model object.
 * 
 * @author ddoubleday
 */
public class OperatorManagerImpl
    extends OperatorManagerImplRoot implements OperatorManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public OperatorManagerImpl(OperatorDAO primaryDAO) {
        super(primaryDAO);
    }
}
