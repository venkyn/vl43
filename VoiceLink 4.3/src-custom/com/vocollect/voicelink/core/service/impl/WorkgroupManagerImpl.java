/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.WorkgroupDAO;
import com.vocollect.voicelink.core.service.WorkgroupManager;

/**
 * Additional service methods for the <code>Operator</code> model object.
 *
 */
public class WorkgroupManagerImpl extends
    WorkgroupManagerImplRoot implements WorkgroupManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public WorkgroupManagerImpl(WorkgroupDAO primaryDAO) {
        super(primaryDAO);
    }

}
