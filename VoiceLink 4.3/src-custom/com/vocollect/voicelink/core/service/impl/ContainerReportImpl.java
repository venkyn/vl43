/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.selection.model.ContainerReport;


/**
 * Container Report Implementation.
 *
 */
public class ContainerReportImpl extends ContainerReportImplRoot {

    /**
     * Constructor.
     * @param beanClass containerReportItem bean for populating data
     */
    public ContainerReportImpl(Class<ContainerReport> beanClass) {
        super(ContainerReport.class);
        // TODO Auto-generated constructor stub
    }

    /**
     * Constructor.
     */
    public ContainerReportImpl() {
        super(ContainerReport.class);
    }

}
