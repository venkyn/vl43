/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.selection.model.AssignmentReport;

/**
 * Assignment Report Implementation. 
 * 
 * @author kudupi
 */
public class AssignmentReportImpl extends AssignmentReportImplRoot {

    /**
     * Constructor.
     * @param beanClass - class of bean report is based on
     */
    public AssignmentReportImpl(Class<AssignmentReport> beanClass) {
        super(AssignmentReport.class);
    }

    /**
     * Constructor.
     */
    public AssignmentReportImpl() {
        super(AssignmentReport.class);
    }
}
