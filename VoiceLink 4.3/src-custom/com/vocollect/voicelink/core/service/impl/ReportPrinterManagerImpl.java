/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.service.ReportPrinterManager;

import org.hibernate.SessionFactory;

/**
 * Additional service methods for the <code>ReportPrinterManager</code>
 * manager class.
 * 
 * @author bnorthrop
 */
public class ReportPrinterManagerImpl extends ReportPrinterManagerImplRoot
    implements ReportPrinterManager {

    /**
     * Default constructor.
     * @param sessionFactory - session factor to use
     */
    public ReportPrinterManagerImpl(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }
}
