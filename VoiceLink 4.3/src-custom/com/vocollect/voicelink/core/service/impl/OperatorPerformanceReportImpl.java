/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

/**
 * Operator Performance Report Implementation.
 *
 */
public class OperatorPerformanceReportImpl extends OperatorPerformanceReportImplRoot {

    /**
     * Constructor.
     * @param beanClass - class of bean report is based on
     */
    public OperatorPerformanceReportImpl(Class<OperatorPerformace> beanClass) {
        super(OperatorPerformace.class);
    }

    /**
     * Constructor.
     */
    public OperatorPerformanceReportImpl() {
        super(OperatorPerformace.class);
    }
}
