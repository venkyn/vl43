/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.DataTranslationImportDataDAO;
import com.vocollect.voicelink.core.service.DataTranslationImportDataManager;

/**
 * Customization popint for the DataTranslation import manager implementation.
 * @author dgold
 *
 */
public class DataTranslationImportDataManagerImpl extends
        DataTranslationImportDataManagerImplRoot implements
        DataTranslationImportDataManager {

    /**
     * Default c'tor.
     * @param primaryDAO the DAO used to create the DB.
     */
    public DataTranslationImportDataManagerImpl(DataTranslationImportDataDAO primaryDAO) {
        super(primaryDAO);
        // Nothing to do.
    }

}
