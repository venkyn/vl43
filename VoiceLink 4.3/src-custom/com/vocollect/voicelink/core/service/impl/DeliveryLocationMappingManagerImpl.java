/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.DeliveryLocationMappingDAO;
import com.vocollect.voicelink.core.service.DeliveryLocationMappingManager;

/**
 * The implementation for this manager.
 * 
 * @author bnorthrop
 */
public class DeliveryLocationMappingManagerImpl extends
    DeliveryLocationMappingManagerImplRoot implements

    DeliveryLocationMappingManager {
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public DeliveryLocationMappingManagerImpl(DeliveryLocationMappingDAO primaryDAO) {
        super(primaryDAO);
    }
}
