/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.puttostore.model.PtsLicenseLaborReport;

/**
 * Put To Store License Labor Report Implementation. 
 *
 * @author kudupi
 */
public class PtsLicenseLaborReportImpl extends PtsLicenseLaborReportImplRoot {

    /**
     * Parameterized Constructor.
     * @param beanClass - Bean Class on which report is based.
     */
    public PtsLicenseLaborReportImpl(Class<PtsLicenseLaborReport> beanClass) {
        super(PtsLicenseLaborReport.class);
    }

    /**
     * Default Constructor.
     */
    public PtsLicenseLaborReportImpl() {
        super(PtsLicenseLaborReport.class);
    }

}
