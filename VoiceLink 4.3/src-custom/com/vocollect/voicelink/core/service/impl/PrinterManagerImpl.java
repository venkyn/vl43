/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.PrinterDAO;
import com.vocollect.voicelink.core.service.PrinterManager;


/**
 * Additional service methods for the <code>Printer</code> model object.
 *
 */
public class PrinterManagerImpl extends
    PrinterManagerImplRoot implements PrinterManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public PrinterManagerImpl(PrinterDAO primaryDAO) {
        super(primaryDAO);
    }

}
