/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.RegionDAO;
import com.vocollect.voicelink.core.service.RegionManager;


/**
 * Additional service methods for the <code>Region</code> model object.
 *
 */
public class RegionManagerImpl extends
    RegionManagerImplRoot implements RegionManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public RegionManagerImpl(RegionDAO primaryDAO) {
        super(primaryDAO);
    }   
}
