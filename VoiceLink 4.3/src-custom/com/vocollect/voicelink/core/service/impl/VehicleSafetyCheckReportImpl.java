/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.model.VehicleSafetyCheckResponse;


/**
 * @author mraj
 *
 */
public class VehicleSafetyCheckReportImpl extends
    VehicleSafetyCheckReportImplRoot {

    /**
     * @param beanClass - the bean class
     */
    public VehicleSafetyCheckReportImpl(Class<VehicleSafetyCheckResponse> beanClass) {
        super(VehicleSafetyCheckResponse.class);
    }

    
    /**
     * Default constructor
     */
    public VehicleSafetyCheckReportImpl() {
        super(VehicleSafetyCheckResponse.class);
    }
}
