/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.VehicleTypeDAO;
import com.vocollect.voicelink.core.service.VehicleTypeManager;


/**
/**
 * Additional service methods for the <code>VehicleType</code> model object.
 *
 * @author kudupi
 */
public class VehicleTypeManagerImpl 
    extends VehicleTypeManagerImplRoot implements VehicleTypeManager {
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public VehicleTypeManagerImpl(VehicleTypeDAO primaryDAO) {
        super(primaryDAO);
    } 

}
