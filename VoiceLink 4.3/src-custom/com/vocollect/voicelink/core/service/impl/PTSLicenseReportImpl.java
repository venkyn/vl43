/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.puttostore.model.PtsLicenseReport;

/**
 * Put To Store License Report Implementation. 
 * 
 * @author kudupi
 */
public class PTSLicenseReportImpl extends PTSLicenseReportImplRoot {

    /**
     * Parameterized Constructor.
     * @param beanClass - Bean Class on which report is based.
     */
    public PTSLicenseReportImpl(Class<PtsLicenseReport> beanClass) {
        super(PtsLicenseReport.class);
    }

    /**
     * Default Constructor.
     */
    public PTSLicenseReportImpl() {
        super(PtsLicenseReport.class);
    }
}
