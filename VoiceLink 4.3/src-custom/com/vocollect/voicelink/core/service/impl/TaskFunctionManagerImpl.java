/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.TaskFunctionDAO;
import com.vocollect.voicelink.core.service.TaskFunctionManager;


/**
 * Additional service methods for the <code>TaskFunction</code> model object.
 *
 */
public class TaskFunctionManagerImpl extends
    TaskFunctionManagerImplRoot 
    implements TaskFunctionManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public TaskFunctionManagerImpl(TaskFunctionDAO primaryDAO) {
        super(primaryDAO);
    }

}
