/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.BreakTypeDAO;
import com.vocollect.voicelink.core.service.BreakTypeManager;


/**
 * Additional service methods for the <code>BreakType</code> model object.
 *
 */
public class BreakTypeManagerImpl 
    extends BreakTypeManagerImplRoot implements BreakTypeManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public BreakTypeManagerImpl(BreakTypeDAO primaryDAO) {
        super(primaryDAO);
    } 
}
