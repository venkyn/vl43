/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.VehicleSafetyCheckDAO;
import com.vocollect.voicelink.core.service.VehicleSafetyCheckManager;


/**
/**
 * Additional service methods for the <code>VehicleSafetyCheck</code> model object.
 *
 * @author khazra
 */
public class VehicleSafetyCheckManagerImpl 
    extends VehicleSafetyCheckManagerImplRoot implements VehicleSafetyCheckManager {
    
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public VehicleSafetyCheckManagerImpl(VehicleSafetyCheckDAO primaryDAO) {
        super(primaryDAO);
    } 

}
