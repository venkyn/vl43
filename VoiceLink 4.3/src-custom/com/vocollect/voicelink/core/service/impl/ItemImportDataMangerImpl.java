/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;


import com.vocollect.voicelink.core.dao.ItemImportDataDAO;
import com.vocollect.voicelink.core.service.ItemImportDataManager;

/**
 * Custom stub for the ItemImportData manager class.
 * @author dgold
 *
 */
public class ItemImportDataMangerImpl extends ItemImportDataManagerImplRoot
        implements ItemImportDataManager {
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ItemImportDataMangerImpl(ItemImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

}
