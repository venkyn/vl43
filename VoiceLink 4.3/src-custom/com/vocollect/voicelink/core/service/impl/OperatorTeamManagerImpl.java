/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.OperatorTeamDAO;
import com.vocollect.voicelink.core.service.OperatorTeamManager;


/**
 * Additional service methods for the <code>OperatorTeam</code> model object.
 *
 * @author mnichols
 */
public class OperatorTeamManagerImpl extends OperatorTeamManagerImplRoot implements OperatorTeamManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public OperatorTeamManagerImpl(OperatorTeamDAO primaryDAO) {
        super(primaryDAO);
    }     
}
