/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.TerminalDAO;
import com.vocollect.voicelink.core.service.TerminalManager;


/**
 * Additional service methods for the <code>Device</code> model object.
 *
 */
public class TerminalManagerImpl extends
    TerminalManagerImplRoot implements TerminalManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public TerminalManagerImpl(TerminalDAO primaryDAO) {
        super(primaryDAO);
    }

}
