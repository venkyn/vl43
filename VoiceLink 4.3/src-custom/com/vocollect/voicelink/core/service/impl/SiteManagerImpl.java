/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.epp.dao.SiteDAO;
/**
 * Implementation of SiteManager interface.
 * 
 * @author Nick Kocur
 */
public class SiteManagerImpl extends SiteManagerImplRoot {
 
    /**
     * Constructor.
     * @param primaryDAO the primary DAO 
     * (the instantiation for the persistent class)
     */
    public SiteManagerImpl(SiteDAO primaryDAO) {
        super(primaryDAO);
    }

}
