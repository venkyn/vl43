/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;


import com.vocollect.voicelink.core.dao.LotImportDataDAO;
import com.vocollect.voicelink.core.service.LotImportDataManager;

/**
 * Custom stub for the LotImportData manager class.
 * @author mnichols
 *
 */
public class LotImportDataManagerImpl extends LotImportDataManagerImplRoot
        implements LotImportDataManager {
    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public LotImportDataManagerImpl(LotImportDataDAO primaryDAO) {
        super(primaryDAO);
    }

}
