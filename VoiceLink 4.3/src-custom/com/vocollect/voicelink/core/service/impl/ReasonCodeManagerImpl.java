/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service.impl;

import com.vocollect.voicelink.core.dao.ReasonCodeDAO;
import com.vocollect.voicelink.core.service.ReasonCodeManager;


/**
 * Additional service methods for the <code>ReasonCode</code> model object.
 *
 * @author Ed Stoll
 */
public class ReasonCodeManagerImpl 
    extends ReasonCodeManagerImplRoot 
    implements ReasonCodeManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ReasonCodeManagerImpl(ReasonCodeDAO primaryDAO) {
        super(primaryDAO);
    }

}
