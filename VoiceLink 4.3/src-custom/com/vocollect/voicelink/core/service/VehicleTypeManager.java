/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.core.service;


/**
 * Business Service Interface to handle communication between web and
 * persistence layer for VehicleType-related operations.
 *
 * @author kudupi
 */
public interface VehicleTypeManager extends VehicleTypeManagerRoot {

}
