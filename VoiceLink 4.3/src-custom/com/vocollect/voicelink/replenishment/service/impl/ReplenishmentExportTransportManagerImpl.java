/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.replenishment.service.impl;

import com.vocollect.voicelink.replenishment.dao.ReplenishmentDAO;
import com.vocollect.voicelink.replenishment.service.ReplenishmentExportTransportManager;


/**
 * @author jtauberg
 *
 */
public class ReplenishmentExportTransportManagerImpl extends
 ReplenishmentExportTransportManagerImplRoot implements
 ReplenishmentExportTransportManager {

    

/**
 * Constructor.
 * @param primaryDAO the DAO for this manager
 */
public ReplenishmentExportTransportManagerImpl(ReplenishmentDAO primaryDAO) {
    super(primaryDAO);
}

}
