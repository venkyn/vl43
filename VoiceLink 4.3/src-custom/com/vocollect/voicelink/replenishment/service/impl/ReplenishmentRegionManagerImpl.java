/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.service.impl;

import com.vocollect.voicelink.replenishment.dao.ReplenishmentRegionDAO;
import com.vocollect.voicelink.replenishment.service.ReplenishmentRegionManager;


/**
 * 
 *
 */
public class ReplenishmentRegionManagerImpl
    extends ReplenishmentRegionManagerImplRoot
    implements ReplenishmentRegionManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ReplenishmentRegionManagerImpl(ReplenishmentRegionDAO primaryDAO) {
        super(primaryDAO);
    }

}
