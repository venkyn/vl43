/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.dao;

import com.vocollect.epp.dao.exceptions.DataAccessException;
import com.vocollect.voicelink.replenishment.model.ReplenishmentRegion;

import java.util.List;

/**
 * 
 */
public interface ReplenishmentRegionDAO extends ReplenishmentRegionDAORoot {

    /**
     * @return List<ReplenishmentRegion>
     * @throws DataAccessException
     */
    List<ReplenishmentRegion> listAllRegionsOrderByNumber() throws DataAccessException;
    
}
