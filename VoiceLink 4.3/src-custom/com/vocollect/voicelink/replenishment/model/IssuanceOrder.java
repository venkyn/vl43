/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.model;

/**
 * This class is a component that groups together the fields that
 * make up the order of issuance for replenishments.
 */
public class IssuanceOrder extends IssuanceOrderRoot {

    //
    private static final long serialVersionUID = 8925325088868647192L;

}
