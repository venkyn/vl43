/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * 
 *
 * @author sfahnestock
 */
public enum ReplenishStatus implements ValueBasedEnum<ReplenishStatus> {
    Unavailable(0),
    Available(1),
    InProgress(2),
    Complete(3),
    Canceled(4);

//  Internal map of int values to enum values.
    private static ReverseEnumMap<ReplenishStatus> toValueMap =
        new ReverseEnumMap<ReplenishStatus>(
            ReplenishStatus.class);
    
    private int value;

    /**
     * Constructor.
     * @param value the associated int rep.
     */
    private ReplenishStatus(int value) {
        this.value = value;
    }

    /**
     * method to get resource key associated with the enum value.
     * @return - String for resource key
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public ReplenishStatus fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ReplenishStatus#fromValue(Integer)
     */
    public ReplenishStatus fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * @param statuses the set of statuses to compare to
     * @return true if this is in the set, false otherwise.
     */
    public boolean isInSet(ReplenishStatus... statuses) {
        for (ReplenishStatus status : statuses) {
            if (this.equals(status)) {
                return true;
            }
        }
        // No match
        return false;
    }
    
    
    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static ReplenishStatus toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
