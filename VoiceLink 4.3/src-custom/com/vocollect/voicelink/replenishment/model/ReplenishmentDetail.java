/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.model;

import com.vocollect.epp.model.Taggable;

import java.io.Serializable;


/**
 * 
 */
public class ReplenishmentDetail extends ReplenishmentDetailRoot
    implements Serializable, Taggable {
    
    private static final long serialVersionUID = 3231495093487133756L;
    
}
