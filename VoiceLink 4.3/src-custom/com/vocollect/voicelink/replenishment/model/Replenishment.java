/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.replenishment.model;

import com.vocollect.epp.model.Taggable;
import com.vocollect.voicelink.core.model.Resequencable;

import java.io.Serializable;



/**
 *
 */
public class Replenishment extends ReplenishmentRoot 
implements Serializable, Taggable, Resequencable {

    private static final long serialVersionUID = 4953348240151978588L;
    
}
