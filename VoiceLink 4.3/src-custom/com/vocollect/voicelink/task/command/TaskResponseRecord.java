/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;

import java.io.Serializable;


/**
 * 
 */
public class TaskResponseRecord extends TaskResponseRecordRoot
    implements ResponseRecord, Serializable {

    private static final long serialVersionUID = 6893229645166797547L;

    /**
     * Constructor.
     */
    public TaskResponseRecord() {
        super();
    }

    /**
     * Constructor.
     * @param initialCapacity initial storage size of the record.
     */
    public TaskResponseRecord(int initialCapacity) {
        super(initialCapacity);
    }

 
}
