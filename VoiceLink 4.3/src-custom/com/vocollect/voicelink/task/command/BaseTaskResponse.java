/*
 * Copyright (c) 2004 Vocollect, Inc.
 * Pittsburgh, PA 15235, USA
 * All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.task.command;

import java.io.Serializable;

/**
 * Define a response consisting of multiple records.
 */
public class BaseTaskResponse 
    extends BaseTaskResponseRoot implements Response, Serializable {

    private static final long serialVersionUID = 6843023626761967611L;

}
