/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;


/**
 * Class to hold the instances of error codes for task commands. Although the
 * range for these error codes is 1000-1999, please use only 1000-1499 for the
 * predefined ones; 1500-1999 should be reserved for customization error codes.
 * 
 * Recommended usage: Selection/Core use 1001-1100 customizations use 1501-1600
 * Putaway use 1100-1125 customizations use 1601-1625 Replensihement 1126-1150
 * customizations use 1626-1650 LineLoading 1151-1175 customizations use
 * 1651-1675
 * 
 */
public final class TaskErrorCode extends TaskErrorCodeRoot {

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected TaskErrorCode(long err) {
        // The error is defined in the TaskErrorCode context.
        super(err);
    }
}
