/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.task.command;

import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.context.ApplicationContextAware;

/**
 * Registry for name to bean name command mappings -- the name is what the 
 * outside world uses, the bean name is what Spring knows.
 * <p>
 * The dispatcher is a Spring-managed singleton that is auto-populated with
 * all the Spring-managed TaskCommand beans. To dispatch to a specific command,
 * call the dispatch method, passing the configured "dispatchName" property
 * as the argument.
 */
public class CommandDispatcher extends CommandDispatcherRoot
    implements ApplicationContextAware, BeanFactoryPostProcessor {

}
