/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.task.command;




/**
 * 
 */
public interface Command extends CommandRoot {

    /**
     * Set names of command properties the outside world can set.
     * @param args the argument list
     */
    void setDispatchArgs(String[] args);
    
    /**
     * @return names of command properties the outside world can set.
     */
    String[] getDispatchArgs();
    
    /**
     * Set the name by which this command is known to the outside world.
     * @param name the external name of the command
     */
    void setDispatchName(String name);

    /**
     * @return name by which this command is known to the outside world.
     */
    String getDispatchName();

    /**
     * @return whether or not this command is a read-only operation.
     */
    boolean isReadOnly();

    /**
     * Execute the command and return the associated Response.
     * @return the Response to the Command execution
     * @throws Exception on any error.
     */
    Response execute() throws Exception;
}
