/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.voicelink.task.command;



/**
 * Base task command object for selection commands. Contains the most 
 * commonly used service managers Assignment, Pick, and Container.
 *
 */
public abstract class BasePutawayTaskCommand extends BasePutawayTaskCommandRoot {

    private static final long serialVersionUID = -2267561764952674529L;

}
