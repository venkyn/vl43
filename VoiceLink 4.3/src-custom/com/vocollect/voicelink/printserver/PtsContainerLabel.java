/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.voicelink.puttostore.model.PtsContainer;

/**
 * 
 *
 * @author estoll
 */
public class PtsContainerLabel extends PtsContainerLabelRoot {
    
    /**
     * Constructor.
     * @param container - pts container
     * @param copies - copies to print
     */
    public PtsContainerLabel(PtsContainer container, Short copies) {
        super(container, copies);
    }

}
