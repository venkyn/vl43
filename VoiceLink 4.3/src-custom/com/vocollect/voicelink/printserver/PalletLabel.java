/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.voicelink.selection.model.Assignment;

/**
 * 
 *
 * @author estoll
 */
public class PalletLabel extends PalletLabelRoot {
    
    /**
     * Constructor.
     * @param assignment - assignment object
     * @param copies - copies to print
     */
    public PalletLabel(Assignment assignment, Short copies) {
        super(assignment, copies);
    }
}
