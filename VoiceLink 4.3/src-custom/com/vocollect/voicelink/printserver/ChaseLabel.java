/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.voicelink.printserver;

import com.vocollect.voicelink.selection.model.Pick;

/**
 * 
 *
 * @author estoll
 */
public class ChaseLabel extends ChaseLabelRoot {
    /**
     * Constructor.
     * @param pick - pick object
     * @param copies - copies to print
     */
    public ChaseLabel(Pick pick, Short copies) {
        super(pick, copies);
    }
}
