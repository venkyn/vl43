/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.util.List;


/**
 * Container for criteria per view per user.
 *
 */
public class Filter extends FilterRoot {

    //
    private static final long serialVersionUID = 3613699376942393790L;

    /**
     * Constructor.
     */
    public Filter() {
    }

    /**
     * Constructor.
     * @param user The User this filter is for.
     * @param view The View this filter is on.
     * @param filterCriteria The List of FilterCriterion.
     */
    public Filter(User user, View view, List<FilterCriterion> filterCriteria) {
        super(user, view, filterCriteria);
    }

    /**
     * When we get serialized JSON from the web tie
     * Constructor.
     * @param submittedSerializedJSON the serialized JSON filter to create from
     */
    public Filter(String submittedSerializedJSON) {
        super(submittedSerializedJSON);
    }

}
