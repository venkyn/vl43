/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * Enumeration for Notification priority values.
 *
 * @author dkertis
 */
public enum LogFileType implements ValueBasedEnum<NotificationPriority> {
    LOG(0),
    ERROR(1),
    UNKNOWN(2);
    
    private int value;
    
    private static ReverseEnumMap<NotificationPriority> toValueMap =
        new ReverseEnumMap<NotificationPriority>(NotificationPriority.class);
    
    /**
     * Constructor.
     * @param val - The int representation of enumeration.
     */
    private LogFileType(int val) {
        this.value = val;
    }
    
    /**
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#getResourceKey()
     * @return The message key for this enumeration.
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return value;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public NotificationPriority fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * @return the int value of this enumeration.
     */
    public int getValue() {
        return value;
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(Integer)
     */
    public NotificationPriority fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }
    
}
