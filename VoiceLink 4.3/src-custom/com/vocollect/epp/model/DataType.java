/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;


/**
 * DataType class - represents a the data type of a table component
 * element. 
 * 
 */
public class DataType extends DataTypeRoot {

    private static final long serialVersionUID = -7698234690627067914L;

}
