/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;

/**
 * This class represents a Feature of the application. Feature definitions
 * are used for access control purposes.
 *
 */
public class Feature extends FeatureRoot implements Serializable {

    static final long serialVersionUID = -8893394286122203288L;

}
