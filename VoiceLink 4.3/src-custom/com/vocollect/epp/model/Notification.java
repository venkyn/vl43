/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;


/**
 * This class defines model for notification.
 *
 */
public class Notification extends NotificationRoot implements Taggable {

    private static final long serialVersionUID = -1359004368520254241L;
}
