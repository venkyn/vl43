/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;


/**
 * View class - Represents a view in the GUI.
 * 
 */
public class View extends ViewRoot {

    private static final long serialVersionUID = -7698234690627067914L;

}
