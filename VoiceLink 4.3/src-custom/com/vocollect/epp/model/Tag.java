/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;



/**
 * Class representing a tagging of an object model.
 *
 */
public class Tag extends TagRoot {
    //
    private static final long serialVersionUID = 5668116041299956360L;

}
