/*
 * Copyright (c) 2011 Vocollect, Inc.
 * Pittsburgh, PA 15235
 * All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.DateUtil;
import com.vocollect.epp.util.ResourceUtil;
import com.vocollect.epp.util.SiteContextHolder;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * 
 *
 * @author mnichols
 */
public enum ReportInterval implements ValueBasedEnum<ReportInterval> {
    Today(1), 
    Yesterday(2),
    WeekToDate(3),
    LastWeek(4),
    MonthToDate(5);
    
    private static final int HOURS_IN_DAY = 24;
    private static final int DAYS_IN_WEEK = 7;
    

    // Internal map of int values to enum values.
    private static ReverseEnumMap<ReportInterval> toValueMap =
        new ReverseEnumMap<ReportInterval>(ReportInterval.class);
    
    private int value;
    
    /**
     * @param now
     * @return - the start date
     */
    public Date getStartDate() {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        GregorianCalendar startTime = new GregorianCalendar();
        startTime.set(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
        TimeZone siteTz = SiteContextHolder.getSiteContext().getCurrentSite()
                .getTimeZone();
        Date siteCurrDate = DateUtil.convertDateTimeZone(calendar,
                calendar.getTimeZone(), siteTz);
        Date serverStartDate;
        Calendar cal = Calendar.getInstance(siteTz);
        cal.setTime(siteCurrDate);
        switch (value) {
        // Today
        case 1:
            startTime.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
            break;
            
        // Yesterday
        case 2:
            // Move the start time back one day
            startTime.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH) - 1, 0, 0, 0);
            break;
            
        // WeektoDate
        case 3:
            // Move the start time back to Sunday
            startTime.add(Calendar.DAY_OF_MONTH,
                    -startTime.get(Calendar.DAY_OF_WEEK) + 1);
            break;
            
        // LastWeek
        case 4:
            // Move the start time back to Sunday
            startTime.add(Calendar.DAY_OF_MONTH,
                    -startTime.get(Calendar.DAY_OF_WEEK) + 1);
            // Move the start time back one week
            startTime.add(Calendar.DAY_OF_MONTH, -DAYS_IN_WEEK);
            break;
            
        // MonthToDate
        case 5:
            startTime.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), 1,
                    0, 0, 0);
            break;
        default:
            break;
        }
        serverStartDate = DateUtil.convertDateTimeZone(startTime, siteTz,
                startTime.getTimeZone());
        startTime.setTime(serverStartDate);
        return new Date(startTime.getTimeInMillis());
    }
  
    /**
     * @param now
     * @return - the end date
     */
    public Date getEndDate() {
        Date now = new Date();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(now);

        GregorianCalendar endTime = new GregorianCalendar();
        TimeZone siteTz = SiteContextHolder.getSiteContext().getCurrentSite()
            .getTimeZone();
        Date siteEndDate = DateUtil.convertDateTimeZone(calendar, calendar.getTimeZone(), siteTz);
        endTime.setTime(siteEndDate);
        
        endTime.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH), HOURS_IN_DAY, 0, 0);
        Date serverEndDate;
        switch (value) {
        
        //Yesterday
        case 2: 
            // Set end time to yesterday
            endTime.add(Calendar.DAY_OF_MONTH, -1);
            endTime.add(Calendar.SECOND, -1);
            serverEndDate = DateUtil.convertDateTimeZone(endTime, siteTz, endTime.getTimeZone());

            break;
            
        //LastWeek
        case 4: 
            // Set end time to last week
            endTime.add(Calendar.DAY_OF_MONTH, (-endTime.get(Calendar.DAY_OF_WEEK)) + 1);
            endTime.add(Calendar.SECOND, -1);
            serverEndDate = DateUtil.convertDateTimeZone(endTime, siteTz, endTime.getTimeZone());
            break;
            
        // MonthToDate, WeekToDate, Today
        default:
            endTime.setTime(now);
            serverEndDate = endTime.getTime();
            break;
        }
        return new Date(serverEndDate.getTime());
    }
    
    /**
     * Constructor.
     * @param value the associated int rep.
     */
    private ReportInterval(int value) {
        this.setValue(value);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public ReportInterval fromValue(int val) {
        return (toValueMap.get(val));
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(java.lang.Integer)
     */
    public ReportInterval fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#getResourceKey()
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * Getter for the value property.
     * @return the value of the property
     */
    public int getValue() {
        return this.value;
        
    }
    
    /**
     * Setter for the value property.
     * @param value the new value value
     */
    public void setValue(int value) {
        this.value = value;
    }    
    
    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static ReportInterval toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
