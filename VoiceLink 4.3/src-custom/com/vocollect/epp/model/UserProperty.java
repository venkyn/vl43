/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;



/**
 * UserProperty class.
 */
public class UserProperty extends UserPropertyRoot  {

    private static final long serialVersionUID = 3832626162173359411L;
}
