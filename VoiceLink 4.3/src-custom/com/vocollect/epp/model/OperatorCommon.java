/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;



/**
 * Model object for the shared Common Operator object used by all apps.
 */
public class OperatorCommon extends OperatorCommonRoot {

    private static final long serialVersionUID = 6638780187754103800L;

}
