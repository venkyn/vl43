/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;
import java.util.Date;


/**
 * The model definition of the job history.
 *
 */
public class JobHistory extends JobHistoryRoot implements Serializable {

    // Unique ID for class serialization.
    private static final long serialVersionUID = -4236784349066454570L;

    /**
     * Constructor.
     */
    public JobHistory() {
        super();
    }

    /**
     * Constructor.
     * @param jobId the id of the <code>Job</code>
     * @param jobName the name of the job
     * @param started when the job started
     */
    public JobHistory(Long jobId, String jobName, Date started) {
        super(jobId, jobName, started);
    }


}
