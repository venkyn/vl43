/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;


/**
 * 
 *
 * @author mnichols
 */
public class ReportParameter extends ReportParameterRoot {

    /**
     * 
     * Constructor.
     */
    public ReportParameter() {
        super();
    }
    
    /**
     * Constructor.
     * @param repParam
     * @param value
     */
    public ReportParameter(ReportTypeParameter repParam, String value) {
        super(repParam, value);
    }

    private static final long serialVersionUID = 1L;
}
