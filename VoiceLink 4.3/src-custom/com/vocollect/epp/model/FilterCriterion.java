/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.ui.Operand;


/**
 * Model object used for saving a user's preferred filter
 * criteria per view.
 *
 */
public class FilterCriterion extends FilterCriterionRoot {

    private static final long serialVersionUID = 4185423194548965368L;

    /**
     * Empty constructor
     * Constructor.
     */
    public FilterCriterion() {
    }

    /**
     * This is the major constructor which accepts all required fields
     * Constructor.
     * @param column the column associated with the filter criterion
     * @param operand the operand associated with the filter criterion
     * @param value1 the main filter value
     * @param value2 the secondary filter value
     * @param locked determines whether the user can remove.
     */
    public FilterCriterion(Column column,
                           Operand operand,
                           String value1,
                           String value2,
                           boolean locked) {
        super(column, operand, value1, value2, locked);
    }


}
