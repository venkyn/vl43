/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * @author smittal
 *
 */
public enum TimeWindowFilterType implements ValueBasedEnum<TimeWindowFilterType> {
    NO_FILTER(0),
    NEXT_2_HRS(2),
    NEXT_4_HRS(4),
    NEXT_8_HRS(8),
    NEXT_12_HRS(12),
    NEXT_18_HRS(18),
    NEXT_24_HRS(24),
    NEXT_48_HRS(48);

    private int value;
    
    private static ReverseEnumMap<TimeWindowFilterType> toValueMap =
        new ReverseEnumMap<TimeWindowFilterType>(TimeWindowFilterType.class);
    
    
    /**
     * Constructor.
     * @param val - The int representation of enumeration.
     */
    private TimeWindowFilterType(int val) {
        this.value = val;
    }

    @Override
    public int toValue() {
        return value;
    }

    @Override
    public TimeWindowFilterType fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public TimeWindowFilterType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static TimeWindowFilterType toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
