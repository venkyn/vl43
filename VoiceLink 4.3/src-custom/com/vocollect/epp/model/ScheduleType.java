/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * Enumeration for Schedule Types (Interval, Daily, etc).
 * 
 * @author jkercher
 */
public enum ScheduleType implements ValueBasedEnum<ScheduleType> {
    INTERVAL(1),
    DAILY(2);

    private int value;
    
    private static ReverseEnumMap<ScheduleType> toValueMap =
        new ReverseEnumMap<ScheduleType>(ScheduleType.class);
    
    /**
     * Constructor.
     * @param val - The int representation of enumeration.
     */
    private ScheduleType(int val) {
        this.value = val;
    }
    
    /**
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#getResourceKey()
     * @return The message key for this enumeration.
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return value;
    }
    
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public ScheduleType fromValue(int val) {
        return toValueMap.get(val);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ScheduleType#fromValue(Integer)
     */
    public ScheduleType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * @return the int value of this enumeration.
     */
    public int getValue() {
        return value;
    }
    
}
