/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;

/**
 * This class represents a logical grouping of Feature objects.
 *
 */
public class FeatureGroup extends FeatureGroupRoot implements Serializable {

    static final long serialVersionUID = -6809133511147293707L;

}
