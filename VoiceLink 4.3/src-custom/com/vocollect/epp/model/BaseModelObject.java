/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;



/**
 * Base class for Model objects.  Child objects must implement 
 * <code>equals()</code> and <code>hashCode()</code>. 
 */
public abstract class BaseModelObject extends BaseModelObjectRoot implements DataObject, Serializable {

}
