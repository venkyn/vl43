/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;




/**
 * Class representing localized translations of all system data that might
 * be displayed to an end user and require localization.
 *
 */
public class SystemTranslation extends SystemTranslationRoot {

    private static final long serialVersionUID = -1614320013755509359L;

}
