/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;

import org.quartz.JobDetail;



/**
 * This class defines the model of a schedule.
 * 
 */
public class Job extends JobRoot implements Serializable {

    // Unique ID for class serialization.
    private static final long serialVersionUID = -5003634036266454570L;

    /**
     * Constructor.
     */
    public Job() {
    }

    /**
     * This constructor should only be used on startup from the configuration
     * file. Use the <code>(JobDetail, Job)</code> constructor whenever
     * possible.
     * 
     * @param jobDetail Quartz object that contains properties (both framework
     *            and custom) describing a given job.
     */
    public Job(JobDetail jobDetail) {
        super(jobDetail);
    }


}
