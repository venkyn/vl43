/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.File;
import java.io.Serializable;


/** 
 * Class to hold the details of the file that we are interested in
 * for the front end.
 *
 */
public class FileView extends FileViewRoot implements DataObject, Serializable {

    private static final long serialVersionUID = 7962938609761894018L;

    /**
     * Constructor.
     * @param source the source file.
     */
    public FileView(File source) {
        super(source);
    }
    
    /**
     * Constructor. Emprty constructor for a client to instantiate to get the enum values.
     */
    public FileView() {
    
    }

}
