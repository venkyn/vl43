/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

/**
 * Site class - represents a site of the EPP-based application.
 *
 */
public class Site extends SiteRoot
    implements DataObject, Serializable, Comparable<Site> {

    private static final long serialVersionUID = 5694781418898273277L;
    
    private String shiftStartTime = "00:00";
        
    private static final long MILLISEC_PER_DAY = 86400000L;
    
    /**
     * @return Date of the time value
     */
    public Date getShiftStartDate() {
        // get the shift start hour and shift start minute
        String[] time = this.getShiftStartTime().split(":");
        
        Integer shiftStartHour = Integer.parseInt(time[0]);
        Integer shiftStartMinute = Integer.parseInt(time[1]);
        
        // get a calendar set to the current time
        Calendar calendar = Calendar.getInstance();
        
        // adjust the calendar to the shift start time
        calendar.set(Calendar.HOUR_OF_DAY, shiftStartHour);
        calendar.set(Calendar.MINUTE, shiftStartMinute);
        
        // compare the updated calendar time to the current time
        // if the update calendar time is in the future, it means that 
        // the shift started yesterday
        if (calendar.getTime().compareTo(new Date()) > 0) {
            // subtract 1 day from the shift start time
            Long timeInMillis = calendar.getTimeInMillis();
            timeInMillis -= MILLISEC_PER_DAY;
            calendar.setTimeInMillis(timeInMillis);
        }
        
        return calendar.getTime();
    }

    /**
     * @return the shiftStartTime
     */
    public String getShiftStartTime() {
        return shiftStartTime;
    }

    /**
     * @param shiftStartTime the shiftStartTime to set
     */
    public void setShiftStartTime(String shiftStartTime) {
        this.shiftStartTime = shiftStartTime;
    }

}
