/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * 
 *
 * @author mnichols
 */
public enum ReportFormat implements ValueBasedEnum<ReportFormat> {

    HTML(1), 
    PDF(2),
    RTF(3),
    XLS(4);

    // Internal map of int values to enum values.
    private static ReverseEnumMap<ReportFormat> toValueMap =
        new ReverseEnumMap<ReportFormat>(ReportFormat.class);
    
    private int value;
    
    /**
     * Constructor.
     * @param value the associated int rep.
     */
    private ReportFormat(int value) {
        this.setValue(value);
    }
    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(int)
     */
    public ReportFormat fromValue(int val) {
        return (toValueMap.get(val));
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#fromValue(java.lang.Integer)
     */
    public ReportFormat fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#getResourceKey()
     */
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * {@inheritDoc}
     * @see com.vocollect.epp.dao.hibernate.ValueBasedEnum#toValue()
     */
    public int toValue() {
        return this.value;
    }

    /**
     * Getter for the value property.
     * @return the value of the property
     */
    public int getValue() {
        return this.value;
        
    }
    
    /**
     * Setter for the value property.
     * @param value the new value value
     */
    public void setValue(int value) {
        this.value = value;
    }    
    
    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static ReportFormat toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
