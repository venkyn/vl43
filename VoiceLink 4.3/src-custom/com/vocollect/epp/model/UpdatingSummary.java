/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;



/**
 * This class defines model for UpdatingSummary.
 *
 */
public class UpdatingSummary extends UpdatingSummaryRoot {

    //serialVerion id for UpdatingSummary class
    private static final long serialVersionUID = -7698223690627054434L;

}
