/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.model;

import com.vocollect.epp.ui.ColumnDTO;


/**
 * UserColumn class - represents a overridden column.
 * 
 */
public class UserColumn extends UserColumnRoot {

    private static final long serialVersionUID = -7698234690627067914L;

    /**
     * Default Constructor. (used by hibernate.)
     */
    public UserColumn() {

    }

    /**
     * Constructor to create object from values.
     * @param user - User value of column
     * @param c - Column user is overriding
     * @param col - Values of column to override
     */
    public UserColumn(User user, Column c, ColumnDTO col) {
        super(user, c, col);
    }

}
