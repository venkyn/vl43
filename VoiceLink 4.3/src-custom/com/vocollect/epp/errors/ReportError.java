/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.errors;


/**
 * Error code for reporting.
 */
public class ReportError extends ReportErrorRoot {
    
    /**
     * Constructor.
     */
    protected ReportError() {
        super();
    }
    
    /**
     * Constructor.
     * @param code the error code
     */
    protected ReportError(long code) {
        super(code);
    }
}
