/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.chart.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * @author smittal
 *
 */
public enum ChartType implements ValueBasedEnum<ChartType> {
    PIE(0),
    COLUMN(1),
    GRID(2),
    SCROLLCOLUMN(3);

    private int value;
    
    private static ReverseEnumMap<ChartType> toValueMap =
        new ReverseEnumMap<ChartType>(ChartType.class);
    
    
    /**
     * Constructor.
     * @param val - The int representation of enumeration.
     */
    private ChartType(int val) {
        this.value = val;
    }

    @Override
    public int toValue() {
        return value;
    }

    @Override
    public ChartType fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public ChartType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static ChartType toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
