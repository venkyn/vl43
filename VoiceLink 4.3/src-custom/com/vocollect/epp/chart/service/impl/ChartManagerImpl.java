/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.chart.service.impl;

import com.vocollect.epp.chart.dao.ChartDAO;
import com.vocollect.epp.chart.service.ChartManagerImplRoot;


/**
 * @author mraj
 *
 */
public class ChartManagerImpl extends ChartManagerImplRoot {

    /**
     * @param primaryDAO - the primary DAO
     */
    public ChartManagerImpl(ChartDAO primaryDAO) {
        super(primaryDAO);
    }

}
