/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.evaluator;

/**
 * This is an interface for creating different evaluator. The implementation is
 * two dimensional. Means, implementor can choose a specific evaluation
 * technique and can also choose what object it is going to work on. For
 * example, implementor may want to implement a JEXL evaluator for Alert, the
 * signature will be
 * 
 * <blockquote>
 * <code>public Class JexlAlertEvaluator&ltAlert,JSONArray>(){ public JSONArray evaluate(Alert alertObject){ ... } }
 * </code>
 * </blockquote>
 * 
 * @param <T> Any object that is getting evaluated.
 * @param <O> The output object expected from the evaluation.
 * 
 * @author khazra
 */
public interface GenericEvaluator<T, O> extends GenericEvaluatorRoot<T, O> {

}
