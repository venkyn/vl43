/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.evaluator;


/**
 * This is an interface for creating different filtering mechanisms. Implementor 
 * can choose a specific filtering technique and can also choose what object is
 * to be filtered. For example, implementor may want to implement a drill down filtering for 
 * Chart, the signature will be
 * 
 * <blockquote>
 * <code>public Class ChartDrillDownFilter<Chart,JSONArray>(){ public JSONArray filter(<code>Chart</code> chartObject){ ... } }
 * </code>
 * </blockquote>
 * 
 * @param <T> Any object that is getting filtered.
 * @param <O> The output object expected from the filtering.
 * 
 * @author kudupi
 */
public interface GenericFilter<T, O> extends GenericFilterRoot<T, O> {

}
