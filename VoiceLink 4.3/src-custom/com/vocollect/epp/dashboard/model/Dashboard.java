/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dashboard.model;

import com.vocollect.epp.model.Taggable;

/**
 * Model of Dashboard Object. 
 *
 */
public class Dashboard extends DashboardRoot implements Taggable {

    // Serial Version ID
    private static final long serialVersionUID = -5892198995907517863L;

}
