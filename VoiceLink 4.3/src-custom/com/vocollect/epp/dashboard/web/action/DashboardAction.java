/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.dashboard.web.action;


/**
 * Action class for Dashboards.
 *
 */
public class DashboardAction extends DashboardActionRoot {

    // Serial Version ID
    private static final long serialVersionUID = -3175547391068986857L;

}
