/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.dashboard.web.action;


/**
 * @author mraj
 *
 */
public class DashboardDetailAction extends DashboardDetailActionRoot {

    /**
     * 
     */
    private static final long serialVersionUID = 3879453000866176882L;

}
