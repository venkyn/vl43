/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;


/**
 * Manager for implementing data sources for complex reports that need more 
 * than just a hibernate query.
 */
public interface ReportDataSourceManager extends ReportDataSourceManagerRoot {

}
