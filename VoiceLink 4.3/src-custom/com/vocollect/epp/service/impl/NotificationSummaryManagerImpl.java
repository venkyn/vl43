/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.NotificationDAO;
import com.vocollect.epp.service.NotificationSummaryManager;

/**
 * Implementation of NotificationSummaryManager interface.
 *
 */
public class NotificationSummaryManagerImpl extends
    NotificationSummaryManagerImplRoot implements
    NotificationSummaryManager {

    /**
     * Constructor for notificationSummaryManagerImpl.
     * @param primaryDAO the DAO primarily used.
     */
    public NotificationSummaryManagerImpl(NotificationDAO primaryDAO) {
        super(primaryDAO);
    }

}
