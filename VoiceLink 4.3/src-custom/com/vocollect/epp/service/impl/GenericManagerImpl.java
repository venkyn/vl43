/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.service.GenericManager;


/**
 * A GenericManager implementation that other service objects can inherit.
 * @param <T> The (primary) domain model type associated with this manager
 * @param <DAO> The (primary) DAO associated with this manager. It must be
 * a DAO for the primary domain model type.
 *
 */
public class GenericManagerImpl<T, DAO extends GenericDAO<T>>
    extends GenericManagerImplRoot<T, DAO>
    implements GenericManager<T, DAO> {

    /**
     * Constructor.
     * @param primaryDAO the primary DAO for this Manager. Subclasses can
     * add additional DAOs if needed.
     */
    public GenericManagerImpl(DAO primaryDAO) {
        super(primaryDAO);
    }

}
