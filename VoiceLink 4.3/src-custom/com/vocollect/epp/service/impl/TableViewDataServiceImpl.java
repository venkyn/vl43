/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.service.TableViewDataService;


/**
 * A service that provides data for Table Component views.
 */
public class TableViewDataServiceImpl extends TableViewDataServiceImplRoot
    implements TableViewDataService {

}
