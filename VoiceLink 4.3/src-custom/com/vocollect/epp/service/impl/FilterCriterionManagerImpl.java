/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.model.FilterCriterion;
import com.vocollect.epp.service.FilterCriterionManager;

/**
 * Implementation of the FilterCriterionManager.
 *
 */
public class FilterCriterionManagerImpl extends FilterCriterionManagerImplRoot
                                        implements FilterCriterionManager {

    /**
     * Constructor.
     * @param primaryDAO The Primary DAO.
     */
    public FilterCriterionManagerImpl(GenericDAO<FilterCriterion> primaryDAO) {
        super(primaryDAO);
    }

}
