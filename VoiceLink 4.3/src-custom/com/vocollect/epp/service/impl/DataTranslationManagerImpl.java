/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.DataTranslationDAO;
import com.vocollect.epp.service.DataTranslationManager;

/**
 * Implementation of DataTranslationManager interface.
 *
 */
public class DataTranslationManagerImpl extends DataTranslationManagerImplRoot
    implements DataTranslationManager {


    /**
     * Constructor.
     * @param primaryDAO the primary DAO
     * (the instantiation for the persistent class)
     */
    public DataTranslationManagerImpl(DataTranslationDAO primaryDAO) {
        super(primaryDAO);
    }

}
