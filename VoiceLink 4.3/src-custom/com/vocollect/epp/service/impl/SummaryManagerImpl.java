/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.SummaryDAO;
import com.vocollect.epp.service.SummaryManager;

import java.io.IOException;


/**
 * Implementation for SummaryManager.
 *
 */
public class SummaryManagerImpl extends SummaryManagerImplRoot
    implements SummaryManager {


    /**
     * Constructor for SummaryManagerImpl.
     * @throws IOException  - on failure to find resource file.
     * @param primaryDAO - summaryDAO
     */
    public SummaryManagerImpl(SummaryDAO primaryDAO) throws IOException {
        super(primaryDAO);

    }
}
