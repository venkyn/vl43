/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.HomepageDAO;
import com.vocollect.epp.service.HomepageManager;

import java.io.IOException;


/**
 * Implementation for HomepageManager.
 *
 */
public class HomepageManagerImpl extends
    HomepageManagerImplRoot implements HomepageManager {

    /**
     * Constructor for HomepageManagerImpl.
     *
     * @param primaryDAO - HomepageDAO
     * @throws IOException - if cannot be retrieved
     */
    public HomepageManagerImpl(HomepageDAO primaryDAO) throws IOException {
        super(primaryDAO);

    }

}
