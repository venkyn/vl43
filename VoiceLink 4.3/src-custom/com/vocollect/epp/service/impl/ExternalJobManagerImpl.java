/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.ExternalJobDAO;
import com.vocollect.epp.service.impl.ExternalJobManagerImplRoot;


/**
 * 
 *
 * @author khazra
 */
public class ExternalJobManagerImpl extends ExternalJobManagerImplRoot {

    /**
     * Constructor.
     * @param primaryDAO ExternalJobDAO
     */
    public ExternalJobManagerImpl(ExternalJobDAO primaryDAO) {
        super(primaryDAO);
    }

}
