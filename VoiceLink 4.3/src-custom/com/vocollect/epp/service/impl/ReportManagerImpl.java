/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.ReportDAO;
import com.vocollect.epp.service.ReportManager;

/**
 * Additional service methods for the <code>Report</code> model object.
 *
 */
public class ReportManagerImpl 
    extends ReportManagerImplRoot implements ReportManager {

    /**
     * Constructor.
     * @param primaryDAO the DAO for this manager
     */
    public ReportManagerImpl(ReportDAO primaryDAO) {
        super(primaryDAO);
    } 
}
