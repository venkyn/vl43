/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.dao.NotificationDAO;
import com.vocollect.epp.service.NotificationManager;

/**
 * Implementation of NotificationManager interface.
 *
 */
public class NotificationManagerImpl extends
    NotificationManagerImplRoot implements
    NotificationManager {


    /**
     * Constructor.
     * @param primaryDAO the notification DAO.
     */
    public NotificationManagerImpl(NotificationDAO primaryDAO) {
        super(primaryDAO);
    }

}
