/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service.impl;

import com.vocollect.epp.service.LogManager;

/**
 * Class to implement the interface LogManager.
 * @see com.vocollect.epp.service.LogManager
 */
public class LogManagerImpl extends LogManagerImplRoot implements LogManager {

}
