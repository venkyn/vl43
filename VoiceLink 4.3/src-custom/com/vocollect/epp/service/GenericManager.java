/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.service;

import com.vocollect.epp.dao.GenericDAO;


/**
 * Generic ServiceManager interface that handles generic operations that
 * apply to most model objects.
 * @param <T> the parameterized type to manage
 * @param <DAO> the parameterized DAO to assist in management.
 *
 */
public interface GenericManager<T, DAO extends GenericDAO<T>> extends
    GenericManagerRoot<T, DAO> {

}
