/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.util;

/**
 * Customization extension of the JasperReportWrapperRoot class.
 */
public class JasperReportWrapper extends JasperReportWrapperRoot {

    /**
     * Constructor.
     * @param reportsDir String value of the path of the directory where reports
     *            are located
     * @param reportName String value of the filename of the report file
     *            (including .jrxml)
     */
    public JasperReportWrapper(String reportsDir, String reportName) {
        super(reportsDir, reportName);
        // TODO Auto-generated constructor stub
    }

}
