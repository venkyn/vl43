/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security;

/**
 * Extension of the Acegi AuthenticationProcessingFilter that does our
 * additional processing at login time.
 *
 * @author treed
 */
public class AuthenticationFilter extends AuthenticationFilterRoot {

}
