/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.security;

/**
 * Voter that takes into account feature-based security associated with
 * dynamic roles.
 * Votes if any {@link ConfigAttribute#getAttribute()} is
 * <ul>
 *   <li> ROLE_ANONYMOUS (any access OK)
 *   <li> ROLE_ANY (any user role is OK if the user is logged in), or
 *   <li> FEATURE_CHECK (the user roles are checked to see if they allow
 * access to the specified feature.)
 * </ul>
 * <p>
 * Access is always denied if none of the above attributes are specified.
 *
 * @author treed
 *
 */
public class FeatureVoter extends FeatureVoterRoot {

}
