/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.opensymphony.xwork2.Preparable;
/**
 * Action class to
 * 1) Get the user supplied license key from the webpage
 *    as enctype="multipart/form-data".
 * 2) Create a valid License object.
 * 3) Store it in session for later use.
 *
 */
public class DocLicenseAction extends DocLicenseActionRoot implements Preparable {

    /**
     *    Serialization identifier.
     */
    private static final long serialVersionUID = -1160667176644207865L;

}
