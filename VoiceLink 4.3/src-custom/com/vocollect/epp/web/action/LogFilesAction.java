/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.opensymphony.xwork2.Validateable;


/**
 * Public interface for the log browsing utility for the EPP. This will allow
 * the user to see all of the logs, select one for viewing, or one or more for
 * zipped download.
 */
public class LogFilesAction extends LogFilesActionRoot implements Validateable {

    private static final long serialVersionUID = -3717301458591246875L;

}
