/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import com.opensymphony.xwork2.Preparable;

/**
 * Action class to 
 * 1) Get the license object  from the session.
 * 2) Store it in database(VOC_EPP_LICENSE).
 * 3) Display the license data to the user.
 *  
 */
public class LicenseDisplayAction extends LicenseDisplayActionRoot implements Preparable {
    /**
     *    Serialization identifier.
     */
    private static final long serialVersionUID = 5377606498299914214L;
    
}
