/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;


/**
 * This interface is used to define an action that will occur on every
 * object in a list of selected objects sent from a table component.
 */
public interface CustomAction extends CustomActionRoot {
    
}
