/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.web.action;

import com.opensymphony.xwork2.Preparable;

/**
 * This class provides Struts action methods that support the
 * System Configuration feature.
 * 
 */
public class SystemConfigurationAction extends SystemConfigurationActionRoot 
    implements Preparable {

    private static final long serialVersionUID = -5461768494660014871L;

}
    

