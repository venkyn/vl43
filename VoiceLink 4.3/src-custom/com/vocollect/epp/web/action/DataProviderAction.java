/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;

import org.springframework.context.ApplicationContextAware;

/**
 * This class provides an abstract implementation for classes that provide data
 * to the table component. It is responsible for serializing business objects
 * (provided by a subclass) into a JSON-formatted string.
 * 
 */
public abstract class DataProviderAction extends DataProviderActionRoot 
    implements ApplicationContextAware {

}
