/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;



/**
 * Errors related to the DataProvider interface.
 *
 */
public class DataProviderErrorCode extends DataProviderErrorCodeRoot {

    /**
     * Constructor. This is not private to allow for customization by adding
     * additional error codes.
     * @param err - error to be logged
     */
    protected DataProviderErrorCode(long err) {
        // The error is defined in the TaskErrorCode context.
        super(err);
    }

}
