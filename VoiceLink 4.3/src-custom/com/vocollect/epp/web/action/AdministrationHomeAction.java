/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;



/**
 * Action associated with the Administration home page. It tries to 
 * be smart and show the user the first Administration view link he
 * has access to, if any.
 *
 */
public class AdministrationHomeAction extends AdministrationHomeActionRoot {

    private static final long serialVersionUID = 1730608416142573772L;

}
