/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;


/**
 * Action that displays the About page.
 */
public class AboutPageAction extends AboutPageActionRoot {
    
    private static final long serialVersionUID = 7331095916904264250L;

}
