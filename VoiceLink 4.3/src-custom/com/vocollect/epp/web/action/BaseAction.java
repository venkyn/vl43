/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;


/**
 * Implementation of <code>ActionSupport</code> that contains convenience 
 * methods for subclasses. For example, getting the current user and saving 
 * messages/errors. This class is intended to be a base class for all Action
 * classes.
 * 
 */
public abstract class BaseAction extends BaseActionRoot {

}
