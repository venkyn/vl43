/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;


/**
 * Action that handles searches.
 */
public class SearchAction extends SearchActionRoot {

    // Serialization identifier.
    private static final long serialVersionUID = -3453435986168842667L;

}
