/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.web.action;



/**
 * This is the Struts action class that handles operations on
 * <code>Summary</code> objects.
 *
 */
public class ConfigurableHomepagesAction extends ConfigurableHomepagesActionRoot {

    private static final long serialVersionUID = 1730608416142573772L;

}
