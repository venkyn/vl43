/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao;



/**
 * This is generic DAO interface that handles basic functions that most
 * DAO instances will need.
 * @param <T> the type to operate on
 */
public interface GenericDAO<T> extends GenericDAORoot<T> {

}
