/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.dao.GenericDAO;
import com.vocollect.epp.dao.RemovableDataProviderDAO;
import com.vocollect.epp.dao.hibernate.finder.FinderExecutor;
import com.vocollect.epp.model.BaseModelObject;


/**
 * This is generic DAO interface that handles basic functions that most
 * DAO instances will need.
 * @param <T> the type to operate on
 */
public class GenericDAOImpl<T extends BaseModelObject>
    extends GenericDAOImplRoot<T>
    implements RemovableDataProviderDAO, GenericDAO<T>, FinderExecutor<T> {

    /**
     * Constructor that determines the type of object the generic DAO
     * will work with.
     * @param type the generic class associated with the generic T
     */
    public GenericDAOImpl(Class<T> type) {
        super(type);
    }


}
