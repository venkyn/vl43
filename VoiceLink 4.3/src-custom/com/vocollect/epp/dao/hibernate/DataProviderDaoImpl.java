/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dao.hibernate;

import com.vocollect.epp.dao.DataProviderDAO;
import com.vocollect.epp.model.BaseModelObject;

/**
 * A class to support the table component. Returns the data necessary for
 * rendering.
 * @param <T> the generic type we are providing data for.
 */
public abstract class DataProviderDaoImpl<T extends BaseModelObject>
    extends DataProviderDaoImplRoot<T> implements DataProviderDAO {

}
