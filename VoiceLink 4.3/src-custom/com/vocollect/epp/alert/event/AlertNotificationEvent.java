/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.event;


/**
 * This class is responsible for capturing the Event when
 * an alertable situation arises.
 * 
 */
public class AlertNotificationEvent extends AlertNotificationEventRoot {

    //
    private static final long serialVersionUID = -3100181915260377274L;

}
