/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.event.listeners;


/**
 * This listener mainly listens to an Alert Event to be generated.
 * And once an AlertEvent got generated, it creates an Alert Notification
 * and sends an Email if Alert is configured to send an email.
 * 
 * 
 */
public class AlertNotificationEventListener extends AlertNotificationEventListenerRoot {

}
