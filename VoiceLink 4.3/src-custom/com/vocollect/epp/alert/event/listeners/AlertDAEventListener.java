/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.event.listeners;


/**
 * This listener mainly listens to an DAUpdate Event to be generated.
 * And once an AlertDAUpdateEvent got generated, it creates an DAUpdate Notification
 * 
 * 
 */
public class AlertDAEventListener extends AlertDAEventListenerRoot {

}
