/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.web.action;

/**
 * Action associated with the Dashboards and Alerts home page. It tries to be
 * smart and show the user the first Dashboards and Alerts view link he has
 * access to, if any.
 * 
 */
public class DashboardAlertHomeAction extends DashboardAlertHomeActionRoot {

    // Serial Version ID
    private static final long serialVersionUID = 2195519321766477828L;

}
