/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.alert.service.impl;


import com.vocollect.epp.alert.dao.AlertCriteriaDAO;
import com.vocollect.epp.alert.service.AlertCriteriaManager;

/** 
 * @author smittal
 *
 */
public class AlertCriteriaManagerImpl extends AlertCriteriaManagerImplRoot implements
    AlertCriteriaManager {

    /**
     * @param primaryDAO - the primary DAO
     */
    public AlertCriteriaManagerImpl(AlertCriteriaDAO primaryDAO) {
        super(primaryDAO);
    }

}
