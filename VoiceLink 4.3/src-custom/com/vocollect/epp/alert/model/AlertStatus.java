/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.alert.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * @author kudupi
 *
 */
public enum AlertStatus implements ValueBasedEnum<AlertStatus> {
    ENABLED(1),
    DISABLED(0);

    private int value;
    
    private static ReverseEnumMap<AlertStatus> toValueMap =
        new ReverseEnumMap<AlertStatus>(AlertStatus.class);
    
    
    /**
     * Constructor.
     * @param val - The int representation of enumeration.
     */
    private AlertStatus(int val) {
        this.value = val;
    }

    @Override
    public int toValue() {
        return value;
    }

    @Override
    public AlertStatus fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public AlertStatus fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static AlertStatus toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
