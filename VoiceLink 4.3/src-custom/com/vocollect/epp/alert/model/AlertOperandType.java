/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.alert.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * 
 * 
 * @author khazra
 */
public enum AlertOperandType implements ValueBasedEnum<AlertOperandType> {
    EQUALSTO(1),
    GREATERTHAN(2),
    GREATERTHANOREQUALSTO(3),
    LESSTHAN(4),
    LESSTHANOREQUALTO(5),
    NOTEQUALSTO(6),
    STRINGEQUALS(7),
    STRINGNOTEQUALS(8),
    STARTSWITH(9),
    ENDSWITH(10),
    CONTAINS(11),
    AFTER(12),
    BEFORE(13);

    private int value;

    // Internal map of int values to enum values.
    private static ReverseEnumMap<AlertOperandType> toValueMap = new ReverseEnumMap<AlertOperandType>(
        AlertOperandType.class);

    /**
     * Constructor.
     * @param value the enum value
     */
    private AlertOperandType(int value) {
        this.value = value;
    }

    @Override
    public int toValue() {
        return this.value;
    }

    @Override
    public AlertOperandType fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public AlertOperandType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * @param statuses the set of statuses to compare to
     * @return true if this is in the set, false otherwise.
     */
    public boolean isInSet(AlertOperandType... statuses) {
        for (AlertOperandType status : statuses) {
            if (this.equals(status)) {
                return true;
            }
        }
        // No match
        return false;
    }

    /**
     * @return the value map of this enum
     */
    public static ReverseEnumMap<AlertOperandType> getToValueMap() {
        return toValueMap;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static AlertOperandType toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
