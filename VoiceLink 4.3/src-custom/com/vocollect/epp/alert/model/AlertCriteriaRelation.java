/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
package com.vocollect.epp.alert.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * @author khazra
 *
 */
public enum AlertCriteriaRelation implements ValueBasedEnum<AlertCriteriaRelation> {
    WHEN(0),
    AND(1),
    OR(2);

    private int value;
    
    private static ReverseEnumMap<AlertCriteriaRelation> toValueMap =
        new ReverseEnumMap<AlertCriteriaRelation>(AlertCriteriaRelation.class);
    
    
    /**
     * Constructor.
     * @param val - The int representation of enumeration.
     */
    private AlertCriteriaRelation(int val) {
        this.value = val;
    }

    @Override
    public int toValue() {
        return value;
    }

    @Override
    public AlertCriteriaRelation fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public AlertCriteriaRelation fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static AlertCriteriaRelation toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
