/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.event;

import com.vocollect.epp.dataaggregator.model.DAEventType;


/**
 * @author mraj
 *
 */
public class DAUpdateEvent extends DAUpdateEventRoot {


    /**
     * 
     */
    private static final long serialVersionUID = 6351704852948000837L;

    /**
     * 
     * @param event - the event type
     */
    public DAUpdateEvent(DAEventType event) {
        super(event);
    }
}
