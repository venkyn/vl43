/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.service.impl;

import com.vocollect.epp.dataaggregator.dao.DAColumnDAO;


/**
 * @author mraj
 *
 */
public class DAColumnManagerImpl extends DAColumnManagerImplRoot {

    /**
     * Constructor.
     * @param primaryDAO The data aggregator column DAO
     */
    public DAColumnManagerImpl(DAColumnDAO primaryDAO) {
        super(primaryDAO);
    }

}
