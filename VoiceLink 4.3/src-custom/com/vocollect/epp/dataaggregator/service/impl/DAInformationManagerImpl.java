/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.service.impl;

import com.vocollect.epp.dataaggregator.dao.DAInformationDAO;

/**
 * 
 * 
 * @author khazra
 */
public class DAInformationManagerImpl extends DAInformationManagerImplRoot {

    /**
     * Constructor.
     * @param primaryDAO The data aggregator information DAO
     */
    public DAInformationManagerImpl(DAInformationDAO primaryDAO) {
        super(primaryDAO);
    }

}
