/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.impl;

import com.vocollect.epp.dataaggregator.DataAggregatorHandler;
import com.vocollect.epp.dataaggregator.impl.DataAggregatorHandlerImplRoot;

import org.springframework.context.ApplicationContextAware;

/**
 * 
 * 
 * @author khazra
 */
public class DataAggregatorHandlerImpl extends DataAggregatorHandlerImplRoot implements DataAggregatorHandler,
    ApplicationContextAware {


}
