/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;


/**
 * @author mraj
 *
 */
public enum DAEventType implements ValueBasedEnum<DAEventType> {
    UPDATED(0),
    DELETED(1);
    
    
    private int value;

    // Internal map of int values to enum values.
    private static ReverseEnumMap<DAEventType> toValueMap = new ReverseEnumMap<DAEventType>(
        DAEventType.class);

    /**
     * Constructor.
     * @param value the enum value
     */
    private DAEventType(int value) {
        this.value = value;
    }

    @Override
    public int toValue() {
        return this.value;
    }

    @Override
    public DAEventType fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public DAEventType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * @param statuses the set of statuses to compare to
     * @return true if this is in the set, false otherwise.
     */
    public boolean isInSet(DAEventType... statuses) {
        for (DAEventType status : statuses) {
            if (this.equals(status)) {
                return true;
            }
        }
        // No match
        return false;
    }

    /**
     * @return the value map of this enum
     */
    public static ReverseEnumMap<DAEventType> getToValueMap() {
        return toValueMap;
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static DAEventType toEnum(int val) 
        throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
