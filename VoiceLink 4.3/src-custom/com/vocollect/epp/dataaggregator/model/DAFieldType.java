/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

package com.vocollect.epp.dataaggregator.model;

import com.vocollect.epp.dao.hibernate.ReverseEnumMap;
import com.vocollect.epp.dao.hibernate.ValueBasedEnum;
import com.vocollect.epp.util.ResourceUtil;

/**
 * 
 * 
 * @author khazra
 */
public enum DAFieldType implements ValueBasedEnum<DAFieldType> {
    STRING(1),
    INTEGER(2),
    FLOAT(3),
    TIME(4),
    DATE(5);

    private int value;

    private static ReverseEnumMap<DAFieldType> toValueMap = new ReverseEnumMap<DAFieldType>(
        DAFieldType.class);

    /**
     * Constructor.
     * @param val - The int representation of enumeration.
     */
    private DAFieldType(int val) {
        this.value = val;
    }

    @Override
    public int toValue() {
        return value;
    }

    @Override
    public DAFieldType fromValue(int val) {
        return toValueMap.get(val);
    }

    @Override
    public DAFieldType fromValue(Integer valObject) {
        return toValueMap.get(valObject.intValue());
    }

    @Override
    public String getResourceKey() {
        return ResourceUtil.makeEnumResourceKey(this);
    }

    /**
     * Return the enum constant that is mapped to the specified int.
     * @param val the int to map.
     * @return the enum constant.
     * @throws IllegalArgumentException if the val doesn't map to a constant.
     */
    public static DAFieldType toEnum(int val) throws IllegalArgumentException {
        return toValueMap.get(val);
    }
}
