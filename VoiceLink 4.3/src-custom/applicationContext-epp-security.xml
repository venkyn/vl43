<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:util="http://www.springframework.org/schema/util"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.0.xsd
                           http://www.springframework.org/schema/util http://www.springframework.org/schema/util/spring-util-3.0.xsd">

    <!-- Automatically receives AuthenticationEvent messages -->
    <bean id="loggerListener"
        class="org.acegisecurity.event.authentication.LoggerListener" />

    <!--
      =- filterChainProxy is called from web.xml
      =-
      =- recommended filter order: ChannelProcessingFilter,
      =- ConcurrentSessionFilter, HttpSessionContextIntegrationFilter,
      =- auth processing mechanisms; <AuthenticationProcessingFilter,
      =- CasProcessingFilter, BasicProcessingFilter,
      =- HttpRequestIntegrationFilter, JbossIntegrationFilter, etc.>,
      =- ContextHolderAwarenessRequestFilter,
      =- RememberMeProcessingFilter, AnonymousProcessingFilter,
      =- SecurityEnforcementFilter, FilterSecurityInterceptor 
     -->
    <bean id="filterChainProxy"
        class="org.acegisecurity.util.FilterChainProxy">
        <property name="filterInvocationDefinitionSource">
            <value>
                CONVERT_URL_TO_LOWERCASE_BEFORE_COMPARISON
                PATTERN_TYPE_APACHE_ANT
                /**=httpSessionContextIntegrationFilter,authenticationProcessingFilter,anonymousProcessingFilter,exceptionTranslationFilter,filterSecurityInterceptor 
            </value>
        </property>
    </bean>

    <!-- This filter is responsible for keeping the SecurityContextHolder
         and the HttpSession in sync. 
    -->
    <bean id="httpSessionContextIntegrationFilter"
        class="org.acegisecurity.context.HttpSessionContextIntegrationFilter" />

    <!-- This filter handles authentication attempts. The parameters specified are: 
        "authenticationManager" : the bean that provides the list of authentication providers
        "userManager" : our service that allows for updating the user object after successful login
        "autheticationFailureUrl" : the URL to go to when authentication fails.
        "defaultTargetUrl" : the URL to go to after successful authentication, unless a specific
                             request was intercepted and redirected to login
        "filterProcessesUrl" : the URL that activates this filter. 
    -->
    <bean id="authenticationProcessingFilter" class="com.vocollect.epp.security.AuthenticationFilter">
        <property name="authenticationManager" ref="authenticationManager"/>
        <property name="userManager" ref="userManager"/>
        <property name="authenticationFailureUrl" value="/login.action?error=true"/>
        <property name="defaultTargetUrl" value="/"/>
        <property name="filterProcessesUrl" value="/j_acegi_security_check"/>
    </bean>

    <!-- The bean that is the wrapper for the set of AuthenticationProviders -->
    <bean id="authenticationManager" class="org.acegisecurity.providers.ProviderManager">
        <property name="providers">
            <list>
                <ref bean="daoAuthenticationProvider"/>
            	<ref local="ldapAuthenticationProvider"/>
                <ref local="anonymousAuthenticationProvider"/>
            </list>
        </property>
    </bean>
   
    <!-- Used by the SecurityEnforcementFilter to commence authentication via
        the AuthenticationProcessingFilter. The "loginFormUrl" is the URL to redirect
        to request user credentials. The "forceHttps" parameter determines whether or
        not authentication should be forced to go over the HTTPS channel.
    -->
    <bean id="authenticationProcessingFilterEntryPoint" class="org.acegisecurity.ui.webapp.AuthenticationProcessingFilterEntryPoint">
        <property name="loginFormUrl" value="/login.action"/>
        <property name="forceHttps" value="false"/>
    </bean>

    <!-- This filter creates an anonymous user pseudo-authentication for the session
        which is only in effect until real authentication takes place.
    -->
    <bean id="anonymousProcessingFilter" class="org.acegisecurity.providers.anonymous.AnonymousProcessingFilter">
        <property name="key" value="anonymous"/>
        <property name="userAttribute"><value>anonymous,ROLE_ANONYMOUS</value></property>
    </bean>
    
    <!-- This filter is the wrapper for the entire authentication and access
        control process.
    -->
    <bean id="exceptionTranslationFilter"
        class="org.acegisecurity.ui.ExceptionTranslationFilter">

<!--
        <property name="filterSecurityInterceptor">
            <ref local="filterSecurityInterceptor" />
        </property>
-->
                
        <property name="authenticationEntryPoint">
            <ref local="authenticationProcessingFilterEntryPoint" />
        </property>
    </bean>

    <!-- This interceptor performs the security checks on HTTP resources.
        The "objectDefinitionSource" contains newline-separated arguments
        that specify behaviors and resources for the filter:
        The behavior specifications tell the filter to convert the URL
        to lowercase before doing comparisons, and to use Ant-style path
        specifications for the following resources. The resources are
        associated with directives that tell the filter how to decide if
        specified URL is accessible:
        
        ROLE_ANONYMOUS means that the URL is available to anyone, authenticated
                       or not.
        ROLE_ANY       means that the URL is available to any authenticated user
        FEATURE_CHECK  means that the the URL must be evaluated against the
                       authenticated user's Roles to determine whether or not
                       those Roles support access to the feature.
    -->
    <bean id="filterSecurityInterceptor"
        class="org.acegisecurity.intercept.web.FilterSecurityInterceptor">
        <property name="authenticationManager">
            <ref local="authenticationManager" />
        </property>

        <property name="accessDecisionManager">
            <ref local="httpRequestAccessDecisionManager" />
        </property>

        <property name="objectDefinitionSource">
            <value>
                CONVERT_URL_TO_LOWERCASE_BEFORE_COMPARISON
                PATTERN_TYPE_APACHE_ANT
                /login.action*=ROLE_ANONYMOUS,ROLE_ANY
                /operatorlogin.action*=ROLE_ANONYMOUS,ROLE_ANY
                /submitoperatorlogindialog.action*=ROLE_ANONYMOUS,ROLE_ANY
                /logout.action*=ROLE_ANONYMOUS,ROLE_ANY
                /home.action*=ROLE_ANY
                /dashboardalert/default.action*=ROLE_ANY
                /refresh.action*=ROLE_ANY
				/saveuserpreferences.action*=ROLE_ANY
				/saveuserselection.action*=ROLE_ANY
				/restoredefaultpreferences.action*=ROLE_ANY
                /getallsummaries.action*=ROLE_ANY
                /addsummary.action*=ROLE_ANY
                /removesummary.action*=ROLE_ANY
                /dragdropandresize.action*=ROLE_ANY
                /swapsummaries.action*=ROLE_ANY
				/**/search/result.action*=ROLE_ANY
                /**/getallsummaries.action*=ROLE_ANY
                /**/addsummary.action*=ROLE_ANY
                /**/removesummary.action*=ROLE_ANY
                /**/dragdropandresize.action*=ROLE_ANY
                /**/swapsummaries.action*=ROLE_ANY
                /**/savetimewindow.action*=ROLE_ANY
                /savetimewindow.action*=ROLE_ANY
                /openclosenavmenu.action*=ROLE_ANY
				/addfiltercriteria.action*=ROLE_ANY
				/editfiltercriteria.action*=ROLE_ANY
				/deletefiltercriteria.action*=ROLE_ANY
				/getoperands.action*=ROLE_ANY
				/getdatevalues.action*=ROLE_ANY
				/getenumerationvalues.action*=ROLE_ANY
				/showprintwindow.action*=ROLE_ANY
                /**/home.action*=ROLE_ANY
                /admin/default.action*=ROLE_ANY
                /viewprofile.action=ROLE_ANY
                /profile.action=ROLE_ANY
				/admin/licensing/agreement*=ROLE_ANY 
                /admin/aboutpage.action=ROLE_ANY
                /getcolumns.action=ROLE_ANY
				/gettableloctext.action=ROLE_ANY
                /index.jsp*=ROLE_ANONYMOUS,ROLE_ANY
                /*chart*.action*=ROLE_ANY
                /**/*.action*=FEATURE_CHECK 
            </value>
        </property>
    </bean>

    <!-- The psuedo-authentication provider for users who haven't logged in yet. -->
    <bean id="anonymousAuthenticationProvider" class="org.acegisecurity.providers.anonymous.AnonymousAuthenticationProvider">
        <property name="key" value="anonymous"/>
    </bean>

    
    <bean id="ldapAuthenticationProvider" class="com.vocollect.epp.security.impl.LdapAuthenticationProviderImpl">
        <property name="userManager" ref="userManager"/>     
        <property name="systemPropertyManager" ref="systemPropertyManager"/>      
    </bean>


    <!-- This encodes user-entered passwords with MD5 for comparison to saved passwords -->
    <bean id="passwordEncoder" class="org.acegisecurity.providers.encoding.Md5PasswordEncoder"/>
    
    <!-- A wrapper class that provides a list of Voters that determine whether
        or not a user has access to an HTTP resource.
    -->
    <bean id="httpRequestAccessDecisionManager"
        class="org.acegisecurity.vote.AffirmativeBased">
        <property name="allowIfAllAbstainDecisions">
            <value>false</value>
        </property>
        <property name="decisionVoters">
            <list>
                <ref local="featureVoter" />
            </list>
        </property>
    </bean>

    <!--
      =- An access decision voter that decides based on feature/role mapping.
     -->
    <bean id="featureVoter" class="com.vocollect.epp.security.FeatureVoter">
    	<property name="roleFeatureMap" ref="roleFeatureMap"/>
    </bean>
    
    <!-- An internal map of Roles to Features and Features to URIs that supports
        access control decision-making.
    -->
    <bean id="roleFeatureMap" class="com.vocollect.epp.security.RoleFeatureMap">
    	<property name="roleManager" ref="roleManager"/>
        <property name="appSecurityFileNames">
            <list>
                <value>/app-security.xml</value>
            </list>
        </property>
		<property name="systemPropertyManager" ref="systemPropertyManager"/>
    </bean>

    <bean id="roleFeatureMapFactory" 
          class="com.vocollect.epp.security.RoleFeatureMapFactory"/>
	
	<bean id="roleFeatureListener"
		class="com.vocollect.epp.security.RoleFeatureChangeListener">
		<property name="delay" value="1000"/>
		<property name="roleManager" ref="roleManager"/>
		<property name="roleFeatureMap" ref="roleFeatureMap"/>
	    <property name="systemPropertyManager" ref="systemPropertyManager"/>
		<property name="sessionFactory" ref="sessionFactory"/>
	</bean>
        	
</beans>
