<#if parameters.label?exists>
<div <#rt/><#if parameters.id?exists>id="wwlbl_${parameters.id}"<#rt/></#if> class="wwlbl">
    <label class="label">
        ${parameters.label?html}</div><#t/>
</#if>
