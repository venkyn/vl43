<#--
	Only show message if errors are available.
	This will be done if ActionSupport is used.
-->
<#assign hasFieldErrors = parameters.name?exists && fieldErrors?exists && fieldErrors[parameters.name]?exists/> 

<div <#rt/><#if parameters.id?exists>id="wwgrp_${parameters.id}"<#rt/></#if> class="wwnbgrp">
<#-- This is included here so it can be retrieved if needed during Ajax validation -->
<input type="hidden" id="multipleErrorsText_${parameters.id}" value="${parameters.label?html} ${stack.findValue("getText('form.validation.multipleFieldErrors')")}"/>
	
<div <#rt/><#if parameters.id?exists>id="wwerr_${parameters.id}"<#rt/></#if> class="wwerr">
    <div <#rt/><#if parameters.id?exists>id="moreErrors_${parameters.id}"<#rt/></#if> class="moreErrors">
      <div class="moreErrorsTop"></div>
      <div class="moreErrorsMiddle">
        <span class="moreErrorsIntro"><strong>${parameters.label?html}</strong> ${stack.findValue("getText('form.validation.moreErrorsIntro')")}</span>
        <ol <#rt/><#if parameters.id?exists>id="moreErrorsList_${parameters.id}"<#rt/></#if> class="moreErrorsList">
<#if hasFieldErrors>
        <#list fieldErrors[parameters.name] as error>
          <li> 
          	<span class="narrowListErrorMessage" errorFor="${parameters.id}">
          		${error?html}
            </span>
          </li>
        </#list>
</#if>
        </ol>
      </div><#t/>
      <div class="moreErrorsBottom"></div>
    </div><#t/>
<#if hasFieldErrors>
  <#if (fieldErrors[parameters.name].size() > 1)>
    <div<#rt/>
    <#if parameters.id?exists>
       errorFor="${parameters.id}"<#rt/>
    </#if>
      classname="errorMessage" class="errorMessage">
             ${parameters.label?html} ${stack.findValue("getText('form.validation.multipleFieldErrors')")}
             <a href="javascript: showMoreErrors('moreErrors_${parameters.id}');" class="moreErrorsLink">${stack.findValue("getText('form.validation.moreErrorsLink')")}</a>
    </div><#t/>
  <#else>
    <#list fieldErrors[parameters.name] as error>
      <div<#rt/>
      <#if parameters.id?exists>
       errorFor="${parameters.id}"<#rt/>
      </#if>
      class="errorMessage">
             ${error}
      </div><#t/>
    </#list>
  </#if>
</#if>
</div><#t/>
<br>
<#if parameters.label?exists && parameters.displayLabel?exists && parameters.displayLabel?html == "true">
<div class="wwctrlgrp" style="float: left"><#t/>
	<div <#rt/><#if parameters.id?exists>id="wwlbl_${parameters.id}"<#rt/></#if> class="wwlbl">
    	<label <#if parameters.id?exists> for="${parameters.id}"<#rt/></#if> class="label">${parameters.label?html}</label>
    <#t/></div>
<#else>    
<div class="wwctrlgrp" style="float: left">
</#if>

