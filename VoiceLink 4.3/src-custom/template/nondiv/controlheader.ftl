<#--
	Only show message if errors are available.
	This will be done if ActionSupport is used.
-->
<#assign hasFieldErrors = parameters.name?exists && fieldErrors?exists && fieldErrors[parameters.name]?exists/> 

<#if hasFieldErrors>
  <#if (fieldErrors[parameters.name].size() > 1)>
    <div<#rt/>
    <#if parameters.id?exists>
       errorFor="${parameters.id}"<#rt/>
    </#if>
      class="errorMessage" style="float:none;">
             ${parameters.label?html} ${stack.findValue("getText('form.validation.multipleFieldErrors')")}
             <a href="javascript: showMoreErrors('moreErrors_${parameters.id}');" class="moreErrorsLink">${stack.findValue("getText('form.validation.moreErrorsLink')")}</a>
    </div><#t/>
  <#else>
    <#list fieldErrors[parameters.name] as error>
      <div<#rt/>
      <#if parameters.id?exists>
       errorFor="${parameters.id}"<#rt/>
      </#if>
      class="errorMessage" style="float:none;">
             ${error}
      </div><#t/>
    </#list>
  </#if>
  <script type="text/javascript">
    addToElementTouchedArray('${parameters.id}');
  </script>
</#if>