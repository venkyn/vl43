<#assign readOnly = parameters.readonly?exists && parameters.readonly=="true"/>
<#assign dynamicUI = parameters.dynamicUI?exists && parameters.dynamicUI=="true"/>
<#if readOnly>
<#-- ${parameters.label?html}<#rt/> -->

<#if parameters.trueValue?exists>
  <#assign trueText = '${parameters.trueValue}'/>
<#else>
  <#assign trueText = 'checkbox.readonly.true'/>
</#if>

<#if parameters.falseValue?exists>
  <#assign falseText = '${parameters.falseValue}'/>
<#else>
  <#assign falseText = 'checkbox.readonly.false'/>
</#if>

<#if parameters.nameValue?exists && parameters.nameValue>
<@s.text name=trueText /><#rt/>
<#else>
<@s.text name=falseText /><#rt/>
</#if>
<#if dynamicUI>
	<div style="display:none">
		<input type="checkbox" name="${parameters.name?html}" value="${parameters.fieldValue?html}"<#rt/>
		<#if parameters.nameValue?exists && parameters.nameValue>
		 checked="checked"<#rt/>
		</#if>
		<#if parameters.id?exists>
		 id="${parameters.id?html}"<#rt/>
		</#if>
		/>
	</div>
</#if>	

<#else>
<input type="checkbox" name="${parameters.name?html}" value="${parameters.fieldValue?html}"<#rt/>
<#if parameters.nameValue?exists && parameters.nameValue>
 checked="checked"<#rt/>
</#if>
<#if parameters.disabled?default(false)>
 disabled="disabled"<#rt/>
</#if>
<#if parameters.tabindex?exists>
 tabindex="${parameters.tabindex?html}"<#rt/>
</#if>
<#if parameters.id?exists>
 id="${parameters.id?html}"<#rt/>
</#if>
<#if parameters.cssClass?exists>
 class="${parameters.cssClass?html}"<#rt/>
</#if>
<#if parameters.cssStyle?exists>
 style="${parameters.cssStyle?html}"<#rt/>
</#if>
<#if parameters.title?exists>
 title="${parameters.title?html}"<#rt/>
</#if>
<#if parameters.tid?exists>
 tid="${parameters.tid?html}"<#rt/>
</#if>
<#include "/${parameters.templateDir}/simple/scripting-events.ftl" />
<#include "/${parameters.templateDir}/simple/common-attributes.ftl" />
/> 
<#if !(parameters.stayleft?exists && parameters.stayleft=="true")>

<label <#if parameters.id?exists>for="${parameters.id?html}"</#if> >
 ${parameters.label?html} </label>


</#if>

<#-- This test is a Vocollect addition. It works around Struts 2
     bug WW-2239 by allowing us to specify that we don't want
     special checkbox handling. -ddoubleday -->
<#if !(parameters.stateless?exists && parameters.stateless=="true")>
 <input type="hidden" name="__checkbox_${parameters.name?html}" value="${parameters.fieldValue?html}" />
</#if>
</#if>
