<#-- This template is overridden partly to fix Struts bug #WW-1300. 
     It now has other changes for layout, too. dday.  -->
<#if !maxBoxWide?has_content>
	<#assign maxBoxWide=3>
</#if>
<#assign itemCount = 0/>
<#assign readOnly = parameters.readonly?exists && parameters.readonly="true"/>
<#if parameters.list?exists>
    <#if parameters.tabindex?exists>
    	<#assign currIndex=parameters.tabindex?number>
    </#if>
	<table>
    <@s.iterator value="parameters.list">
		<#if parameters.listKey?exists>
		    <#assign itemKey = stack.findValue(parameters.listKey)/>
		<#else>
		    <#assign itemKey = stack.findValue('top')/>
		</#if>
		<#if !readOnly || tag.contains(parameters.nameValue, itemKey)>
            <#assign itemCount = itemCount + 1/>
            <#if itemCount = 1>
        	  <tr>
            <#else>
	            <#if (itemCount - 1) % maxBoxWide = 0>
				</tr>
				<tr>
	            </#if>
            </#if>
            <td class="checkBoxList">
                <#if parameters.listValue?exists>
                    <#assign itemValue = stack.findString(parameters.listValue)/>
                <#else>
                    <#assign itemValue = stack.findString('top')/>
                </#if>
                <#-- This is the change mentioned above -->
                <#assign itemKeyStr = itemKey.toString()/>
                <span id="role${itemCount}">
                <#if !readOnly>
                <input
		          <#if parameters.tabindex?exists>
                 tabindex="${currIndex}"
			        <#assign currIndex=currIndex + 1>
		          </#if>
                type="checkbox" name="${parameters.name?html}" value="${itemKeyStr?html}" id="${parameters.name?html}-${itemCount}"<#rt/>
                   <#if tag.contains(parameters.nameValue, itemKey)>
                checked="checked"<#rt/>
                </#if>
                <#if parameters.cssClass?exists>
                    class="${parameters.cssClass}"<#rt/>
                </#if>
                <#if parameters.disabled?default(false)>
                    disabled="disabled"<#rt/>
                </#if>
                <#if parameters.title?exists>
                    title="${parameters.title?html}"<#rt/>
                </#if>
				<#include "/${parameters.templateDir}/simple/scripting-events.ftl" />
				<#include "/${parameters.templateDir}/simple/common-attributes.ftl" />
                />
         		<script type="text/javascript"><#rt/>
         			MochiKit.Signal.connect("${parameters.name?html}-${itemCount}","onfocus",showFocusOnCheckbox);<#rt/>
         			MochiKit.Signal.connect("${parameters.name?html}-${itemCount}","onblur",loseFocusOnCheckbox);<#rt/>
         		</script><#rt/>
                <label for="${parameters.name?html}-${itemCount}" class="checkboxLabel">${itemValue?html}</label>
                <#else>
                <label class="checkboxLabel readonly">${itemValue?html}</label>            
                </#if>
                </span>
	       </td>
        </#if>
    </@s.iterator>
    </table>
    <br/>
    <br/>
<#else>
  &nbsp;
</#if>
