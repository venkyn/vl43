/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
 
/**
 * This is code that is adapted from Struts 2.2.2 to handle AJAX errors
 * in the manner we want.
 * @author ddoubleday -- modifications only
 */
 
var TouchedElements = new Array();
var StrutsValidator = new ValidationClient("$!base/validation");
var selectionStrutsValidator = new SelectedValidationClient("$!base/validation");

/**
 * Override the struts implementation to fix radio buttons once and for all.
 * @see struts-2.0.6.jar/org/apache/struts2/static/validationClient.js
 */
StrutsValidator.validate = function(input, namespace, actionName) {
	var vc = this;
	var form = input.form;
	var params = new Object();
    for (var i = 0; i < form.elements.length; i++) {
        var e = form.elements[i];
        if (e.name != null && e.name != '') {
			if (e.type != null && e.type == 'radio') {
				// Only add the checked radio buttons to the validation request.
				// Otherwise, radio buttons grouped by name would always 
				// overwrite each other - last-in won.
				if (e.checked == true) {
				    params[e.name] = e.value;
				}
		    } else {
                params[e.name] = e.value;
			}
        }
    }

	validator.doPost(function(action) {
        if (action) {
            vc.onErrors(input, action);
        }
    }, namespace, actionName, params);
}

StrutsValidator.onErrors = function(input, errors) {

	var form = input.form;

	clearErrorMessages(form);

        for (var fieldName in errors.fieldErrors) {
		//		alert("Looking for " + fieldName);
		if ( form.elements[fieldName] != null) {
            if (form.elements[fieldName].touched) {
                // EPP modification: multiple errors are summarized in one
                // message, and the error list is added to a hidden div
                // which can be displayed if desired. The multipleErrorsText
                // object is auto-populated in the form.ftl  -- ddoubleday
                if (errors.fieldErrors[fieldName].length > 1) {
                    // There are multiple errors, must display summary
                    addErrors(form.elements[fieldName],
                              null,
                              errors.fieldErrors[fieldName]);
                } else {
                    // Only one error
                    addErrors(form.elements[fieldName], 
                              errors.fieldErrors[fieldName][0]);
                }
            }
		} else {
			//			alert("Field " + fieldName + " does not exist in form.elements...");
        }
    }


}

function findSelected(elementId) {
	var element = $(elementId);
	for(var i = 0; i < element.length; i++) {
		var item = element.item(i);
		log("item ==> " + item.checked);
		if(item.checked) {
			return item.value;
		}
	}
	return -1;
}

/**
 * This is used to validate fields associated with a radio group.
 * @param selectionId - the id of the selection group
 * @param vKey - the key of the selection group that indicates whether to
 * continue validation.  If vKey == selectionId.value, it should be validated.
 * @param validateOn - if this value is present in the error fields, the
 * errors will be shown.
 * @param element - the element being validated.
 */
function validateSelection(selectionId, vKey, validateOn, element) {
	makeElementTouched(element);
	var namespace = element.form.attributes['namespace'].nodeValue;
    var actionName = element.form.attributes['name'].nodeValue; 
	
	var selected = findSelected(selectionId);
	if(vKey == selected) {
		selectionStrutsValidator.validate(element, validateOn, namespace, actionName);
	}
}

/**
 * This should be used to validate in conjunction with the validateSelected
 * method. This should be used for the selection element itself.
 * @param element - the selection element
 * @param mapKey - a key that corresponds to the selection options, this should
 * correspond to the validateOn parameter in validateSelection
 */
function validateSelectElement(element, mapKey) {
	clearErrorMessages(element.form);
	makeElementTouched(element);
	var namespace = element.form.attributes['namespace'].nodeValue;
    var actionName = element.form.attributes['name'].nodeValue; 
    var elemId = parseInt(element.id.toString().substr(element.id.toString().length - 1, 1)) - 1;
	selectionStrutsValidator.validate(element, mapKey[elemId], namespace, actionName);
}

/**
 * Unmodified code from Struts 2.2.2
 */
function validate(element) {
	log("Validating -- ********************************** " + element.name);
    makeElementTouched(element);
    var namespace = element.form.attributes['namespace'].nodeValue;
    var actionName = element.form.attributes['name'].nodeValue;  
	StrutsValidator.validate(element, namespace, actionName);
}

function addToElementTouchedArray(name) {
	var touchedlength = TouchedElements.length;
	TouchedElements[touchedlength]=name;
}

function makeElementTouched(element) {
	if (element) {
		element.touched = true;
	}
}

function parseThroughElementTouchedArray() {
	for (var i = 0; i<TouchedElements.length; i++) {
		var thiselement = document.getElementById(TouchedElements[i]);
		makeElementTouched(thiselement);
	}
}

/**
 *
 * Interface to handle group dependent validating.  This came up during the
 * creation of the schedule edit component, but it should be general enough
 * that it can be applied elsewhere.
 * History: before complaining about the structure, here is why it must happen
 * this way.  Text fields needed to be validated based upon the value of a
 * radio group.  However, Struts does not provide the correct value through
 * validation, so the valildation logic must be handled at this level.
 * Another problem is that regardless of params that are assigned in the validate
 * method below, validation is conducted on all fields on the form.  This means
 * that error messages are shown for fields the user does not care about 
 * (fields not associated with the selected radio button).
 * So, the work around is to display only the error messages related to the fields
 * the user is editing and has selected through the radio group.  This is done
 * by naming the associated fields with a common, distinct word.  In this case 
 * "interval" and "daily" were used.  If the error field has that word in it,
 * the error message is displayed.
 * Also, see the validateSelection method above.
 */

function SelectedValidationClient(servletUrl) {

	this.servletUrl = servletUrl;
	// the pattern of the field name. if the field has this
	// pattern, its error message will be displayed.
	this.selectedFieldPattern;

	// @param input - the input element to be validated
	// @param validateOn - the pattern of the element name to be shown
	this.validate = function(input, validateOn, namespace, actionName) {
		this.selectedFieldPattern = validateOn;
		
		var vc = this;
		var form = input.form;
		var params = new Object();
	    for (var i = 0; i < form.elements.length; i++) {
	        var e = form.elements[i];
            if (e.name != null && e.name != '') {
                params[e.name] = e.value;
            }
        }

		validator.doPost(function(action) {
            if (action) {
                vc.onErrors(input, action);
            }
        }, namespace, actionName, params);
    }
    

	// @param formObject - the form object that triggered the validate call
	// @param errors - a javascript object representing the action errors and field errors
	// client should overwrite this handler to display the new error messages
	this.onErrors = function(inputObject, errors) {
		log("on errors..... " + inputObject.name);
		var form = inputObject.form;
		clearErrorMessages(form);

    	if (errors.fieldErrors) {

        	for (var fieldName in errors.fieldErrors) {
        		log("Field Name ==> " + fieldName);
        		log("Selected fields ==> " + selectionStrutsValidator.selectedFieldPattern);
        	
        		if((fieldName) && (selectionStrutsValidator.selectedFieldPattern)) {
        			if ((form.elements[fieldName].touched) &&
        				(fieldName.indexOf(selectionStrutsValidator.selectedFieldPattern)) != -1) {
                	
                		if (errors.fieldErrors[fieldName].length > 1) {
                    	// There are multiple errors, must display summary
                    		addErrors(form.elements[fieldName],
                              	null,
                              	errors.fieldErrors[fieldName]);
                		} else {
                    // Only one error
                    		addErrors(form.elements[fieldName], 
                              	errors.fieldErrors[fieldName][0]);
                		}
            		}
        		}
        	}
    	}
	
		return this;
	}
}
