<#if parameters.resultDivId?exists || parameters.onLoadJS?exists>
<#include "/${parameters.templateDir}/ajax/submit-ajax.ftl" />
${tag.addFormParameter("ajaxSubmit", "false")}
<#else>
<#include "/${parameters.templateDir}/css_xhtml/submit.ftl" />
</#if>