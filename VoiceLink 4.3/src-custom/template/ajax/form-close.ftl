<#-- Note: This is completely different from the Struts core
     template. Much dojo-related stuff removed, so Struts
     features that depend on dojo are not available. ddoubleday -->
</form>
<script type="text/javascript">
    function ${parameters.id}_ajax_submit() {
        return ${parameters.ajaxSubmit};
    }
    <#if parameters.validate?exists>
   		parseThroughElementTouchedArray();
   	</#if>
</script>
