<#--
/*
 * $Id: pom.xml 560558 2007-07-28 15:47:10Z apetrelli $
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
-->
<#if parameters.validate?exists>
<script type="text/javascript" src="${base}/struts/${themeProperties.parent}/validation.js"></script>
<script type="text/JavaScript">
<#if parameters.id?exists>
    var formId="${parameters.id?default('form1')}";<#rt/>
<#else>
    var formId = null;
</#if>

    var focused = false;
    function selectFirstTextField() {
        if(formId != null) {
            var elements = new Array();
            var formObject = $(formId);
            walkNodes(formObject);
        }
    }
    
    function walkNodes(elem) {
      if (elem == null) {
        return;
      }
      if (elem.length > 1) {
        for (var i=0; i<elem.length; i++) {
          if (!focused) {
            walkNodes(checkNode(elem[i]));
          } else {
            return;
          }
        }
      } else {
        walkNodes(checkNode(elem));
      }
    }
    
    function checkNode(elem) {
      var name = elem.name;
      if (MochiKit.Base.isNotEmpty(name)) {
        var tagName = elem.nodeName;
        if (elem.type != "hidden" &&
            !elem.disabled &&
            elem.style.visibility != "hidden" &&
            ((tagName == "INPUT" && elem.type == "text")  ||
             tagName == "TEXTAREA")) {
             if (!focused) {
               try {
               		elem.focus();
               		focused = true;
               } catch(err) {
               		// Element is visible, but its parent must not be. Don't
               		// set focus here.
               }
             }
             return null;
        }
      }
      return elem.childNodes;
    }
    
    connect(window, "onload", selectFirstTextField);
    connect(document.getElementsByTagName('body')[0], 'onclick', hideMoreErrors);
</script>
</#if>
<form<#rt/>
<#if parameters.namespaceVal?exists>
 namespace="${parameters.namespaceVal?html}"<#rt/>
</#if>
<#if parameters.id?exists>
 id="${parameters.id?default('form1')}"<#rt/>
</#if>
<#if parameters.name?exists>
 name="${parameters.name?html}"<#rt/>
</#if>
<#if parameters.action?exists>
 action="${parameters.action?html}"<#rt/>
</#if>
<#if parameters.target?exists>
 target="${parameters.target?html}"<#rt/>
</#if>
<#if parameters.method?exists>
 method="${parameters.method?html}"<#rt/>
</#if>
<#if parameters.enctype?exists>
 enctype="${parameters.enctype?html}"<#rt/>
</#if>
<#if parameters.cssClass?exists>
 class="${parameters.cssClass?html}"<#rt/>
</#if>
<#if parameters.acceptcharset?exists>
 accept-charset="${parameters.acceptcharset?html}"<#rt/>
</#if>
<#if parameters.cssStyle?exists>
 style="${parameters.cssStyle?html}"<#rt/>
</#if>
<#if parameters.onchange?exists>
 onchange="javascript:${parameters.onchange?html};"<#rt/>
</#if>
 ${tag.addParameter("ajaxSubmit", "true")}
>
<#include "/${parameters.templateDir}/${themeProperties.parent}/control.ftl" />
<#-- This hidden input is placed here in support of AJAX validation. 
     When there are multiple messages, the EPP standard calls for a summary 
     message and a More Info link. In order for the text of these messages to
     be localized, it is easier if the text is resolved in advance and placed
     in the form for use later if needed. Otherwise, the AJAX validation method
     would have to make another AJAX call to resolve the messages against
     the Locale. The More Info link message is here because it does not change
     between fields; the summary message is different for each field, so that
     is included in the control header template for the field.  ddoubleday -->
<input type="hidden" id="moreErrorsLinkText" value="${stack.findValue("getText('form.validation.moreErrorsLink')")}"/>
