<#if !(parameters.readonly??) || (parameters.readonly?? && parameters.readonly?string == 'false') >

${parameters.after?if_exists}<#t/>
    <#lt/>

    <#if parameters.id?exists>
    <script type="text/JavaScript">
	connect('wwctrl_${parameters.id}', 'onfocus', showExistingErrorsBubble);
	</script>
	</#if>
</#if>

</div>
</td></tr></table>
</div>
</div>