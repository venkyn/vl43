/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */
 
/**
 * This is code that is adapted from WebWork 2.2.2 to handle AJAX errors
 * in the manner we want. TODO: This should possibly be updated with
 * the code from Struts2, but it seems to be working.
 * @author ddoubleday -- modifications only
 */

/*
 * This keeps track of the currently visible moreErrors div, if any
 */
var visibleMoreErrors;

/*
 * This is used to check whether to hide MoreErrorBubble or not depending on 
 * whether onclick was called by itself or after onfocus
 */
var hide_bubble=true;

/**
 * Unmodified
 */
function clearErrorMessages(form) {
	// clear out any rows with an "errorFor" attribute
	var divs = form.getElementsByTagName("div");
    var paragraphsToDelete = new Array();

    for(var i = 0; i < divs.length; i++) {
        var p = divs[i];
        if (p.getAttribute("errorFor")) {
            paragraphsToDelete.push(p);
        }
    }

    // now delete the paragraphsToDelete
    for (var i = 0; i < paragraphsToDelete.length; i++) {
        var r = paragraphsToDelete[i];
        var parent = r.parentNode;
        parent.removeChild(r);
    }
}

/**
 * Add the error to the specified field. This has been modified to place
 * the error message in an existing "wwerr" div. (It also creates the "wwerr"
 * div if it isn't found, but it should be found unless something went wrong.)
 * @param e the field that has an error
 * @param errorText the error message to be displayed (if the errorList
 * parameter is non-null, then this is the summary message.
 * @param errorList a list of multiple error messages to display. This can be
 * null when there is only one error.
 */
 

 
function addErrors(e, errorText, errorList) {
    try {
        var ctrlDiv = e.parentNode; // wwctrl_ div or span
        var enclosingDiv = ctrlDiv.parentNode.parentNode.parentNode.parentNode.parentNode; // wwctrlgrp_ div
		if (!ctrlDiv || (ctrlDiv.nodeName != "DIV" && ctrlDiv.nodeName != "SPAN") || !enclosingDiv || enclosingDiv.nodeName != "DIV") {
			return;
		}
		
		var firstDiv = enclosingDiv.getElementsByTagName("div")[0]; // either wwctrl_ or wwlbl_
		if (!firstDiv) {
			firstDiv = enclosingDiv.getElementsByTagName("span")[0];
		}
		
		// EPP addition for AJAX-CSS - put error messages into enclosing 
		// wwerr_id div. The code below replicates the structure created
		// by the controlheader template when errors are found as a result
		// of a form submission. If it is changed there, it must be changed
		// here, and vice versa. -- ddoubleday
		var wwerrDiv = document.getElementById("wwerr_" + e.id);
		// The wwerr div should have been found.
        if(!wwerrDiv) {
			return;
        }
		

			// Add the multiple messages to the "More Info" bubble.
			// First, find the list to add them to.
			var multMessageList = document.getElementById("moreErrorsList_" + e.id);
			if (!multMessageList) {
				return;
			}
			
			// Remove any existing children from the list
            while (multMessageList.childNodes[0]) {
                multMessageList.removeChild(multMessageList.childNodes[0]);
            }
			

			if (errorList) {
			// Now add the list item elements.
			for (i = 0; i < errorList.length; i++) {
				var newItem = document.createElement("li");
				var newSpan = document.createElement("span");
				newSpan.className = "listErrorMessage";
				newSpan.innerHTML = errorList[i];
				newItem.appendChild(newSpan);
				multMessageList.appendChild(newItem);
			}
		}
		// Add the single message, or the summary message.
		var mainMessage;
		if (!errorText) {
			// Need the summary message
			var multMessage = document.getElementById("multipleErrorsText_" + e.id);
			if (multMessage) {
			    mainMessage = multMessage.value + " ";
			} else {
			    // Fallback to prevent JS blowup if field not found.
			    mainMessage = "Field has multiple errors";
			}
		} else {
			mainMessage = errorText;
		}		
        var error = document.createTextNode(mainMessage);
        var errorDiv = document.createElement("div");

        errorDiv.setAttribute("class", "errorMessage");//standard way.. works for ie mozilla
        errorDiv.setAttribute("errorFor", e.id);;
        errorDiv.appendChild(error);
        // Show the "More Info" link, if needed
        if (errorList) {
            errorDiv.appendChild(moreErrorsLink(e.id));
        }
        wwerrDiv.appendChild(errorDiv);
        
        
   } catch (e) {
        alert(e);
    }
}

function moreErrorsLink(fieldId) {
    var link = document.createElement("a");
    link.href = "javascript: showMoreErrors('moreErrors_" + fieldId + "');";
    link.innerHTML = document.getElementById("moreErrorsLinkText").value;
    link.className = "moreErrorsLink";
    link.onmouseover = doSetFocus;
    return link;
}

function doSetFocus(){
	this.focus();
	return true;
}

var savedElements = null;
var parentNodes = null;

function modifyElementVisibility(elements, visibleProperty) {
    savedElements = new Array(elements.length);
    parentNodes = new Array(elements.length);
    for(var i=0; i < elements.length; i++) {
        if (elements[i] != null) {
	        selectNode = getElement(elements[i].id);
	        savedElements[i] = selectNode;
            displayValue = selectNode.options[selectNode.selectedIndex].innerHTML;
            parentNodes[i] = selectNode.parentNode;
            selectNode.style.display = "none";
            appendChildNodes(parentNodes[i], displayValue);
            
            // Change the styling to keep the page from changing shape.
            parentNodes[i].style.paddingTop = "5px";
            parentNodes[i].style.paddingBottom = "2px";
        }
    }
}

function showInputElements() {
    if (parentNodes != null && savedElements != null) {
	    for (var i=0; i < parentNodes.length; i++) {
	        // Put the old element back
	        savedElements[i].style.display = "block";
	        replaceChildNodes(parentNodes[i], savedElements[i]);
	        
	        // Change the styling to keep the page from changing shape.
	        parentNodes[i].style.paddingTop = "3px";
	        parentNodes[i].style.paddingBottom = "0px";
	    }
	}
    parentNodes = null;
    savedElements = null;
}

function hideInputElements() {
    selectInputs = getElementsByTagAndClassName("SELECT");
    if (selectInputs != null && selectInputs.length > 0) {
        modifyElementVisibility(selectInputs);
    }
}

function showMoreErrors(elementId){
	hideMoreErrors();
	/**
	 *  @author amukherjee
	 *  Commenting the code to change the visibility of the drop down list
	 *  The alignment of the drop down list was having problems and and hence the code
	 *  has been commented.
	 */
	//hideInputElements();
    validateElement = document.getElementById(elementId);
 	validateElement.style.visibility = 'visible';
 	var middleDiv = getElementsByTagAndClassName('div', 'moreErrorsMiddle', validateElement)[0];
 	if (elementDimensions(middleDiv).h > 100) {
 	    middleDiv.style.height = '100px';
 	}
	visibleMoreErrors = validateElement;
}

function hideMoreErrors(elementId){
if(hide_bubble)
{
    if (visibleMoreErrors) {
        visibleMoreErrors.style.visibility = 'hidden';
        showInputElements();
        visibleMoreErrors = null;
    }
}
hide_bubble=true;
}

//**********************************************
/**
Function to display more-errors bubble when focus shifts back to violating field.
*/

function showExistingErrorsBubble(e){
	   hideMoreErrors();
       element=e.src().id;
       
       //This is used to obtain error div for violating field
 	   elementId='moreErrors_' + element;
       validateElement = document.getElementById(elementId);
       
       /* 
        * This is used to check if there are more than 1 errors in the errorListDiv
        * related to calling element.
        */ 
       divErrList='moreErrorsList_' + element;
       var divNumErrors = null;
       if( document.getElementById(divErrList) != null){
	       divNumErrors= document.getElementById(divErrList).childNodes;
       }
       
       /* If condition to check if number of errors is greater than 1
        * The bubble is only displayed if it is greater than 1 as otherwise
        * only a single message without a bubble is to be displayed
        */
	   if (divNumErrors != null && divNumErrors.length>1) {
		    hide_bubble=false;
		    validateElement.style.visibility = 'visible';
		    visibleMoreErrors = validateElement;
		}		
}	
	









