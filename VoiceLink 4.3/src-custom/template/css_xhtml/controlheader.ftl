<#--
	Only show message if errors are available.
	This will be done if ActionSupport is used.
-->
<#assign hasFieldErrors = parameters.name?exists && fieldErrors?exists && fieldErrors[parameters.name]?exists/> 

<div <#rt/><#if parameters.id?exists>id="wwgrp_${parameters.id}"<#rt/></#if> class="wwgrp">
<#-- This is included here so it can be retrieved if needed during Ajax validation -->
<input type="hidden" id="multipleErrorsText_${parameters.id}" value="${parameters.label?html} ${stack.findValue("getText('form.validation.multipleFieldErrors')")}"/>
	
<div class="wwctrlgrp">
<table class="wwctrlgrp_table">
<#if !(parameters.readonly??) || (parameters.readonly?? && parameters.readonly?string == 'false') >

<tr><td></td><td>
<div class="wwctrl">
<div <#rt/><#if parameters.id?exists>id="wwerr_${parameters.id}"<#rt/></#if> class="wwerr">
    <div <#rt/><#if parameters.id?exists>id="moreErrors_${parameters.id}"<#rt/></#if> class="moreErrors">
      <div class="moreErrorsTop"></div>
      <div class="moreErrorsMiddle">
        <span class="moreErrorsIntro"><strong>${parameters.label?html}</strong> ${stack.findValue("getText('form.validation.moreErrorsIntro')")}</span>
        <ol <#rt/><#if parameters.id?exists>id="moreErrorsList_${parameters.id}"<#rt/></#if> class="moreErrorsList">
<#if hasFieldErrors>
        <#list fieldErrors[parameters.name] as error>
          <li class="listErrorMessage" errorFor="${parameters.id}">
            ${error?html}
          </li>
        </#list>
</#if>
        </ol>
      </div><#t/>
      <div class="moreErrorsBottom"></div>
    </div><#t/>
<#if hasFieldErrors>
  <#if (fieldErrors[parameters.name].size() > 1)>
    <div<#rt/>
    <#if parameters.id?exists>
       errorFor="${parameters.id}"<#rt/>
    </#if>
      class="errorMessage">
             ${parameters.label?html} ${stack.findValue("getText('form.validation.multipleFieldErrors')")}
             <a href="javascript: showMoreErrors('moreErrors_${parameters.id}');" class="moreErrorsLink">${stack.findValue("getText('form.validation.moreErrorsLink')")}</a>
    </div><#t/>
  <#else>
    <#list fieldErrors[parameters.name] as error>
      <div<#rt/>
      <#if parameters.id?exists>
       errorFor="${parameters.id}"<#rt/>
      </#if>
      class="errorMessage">
             ${error}
      </div><#t/>
    </#list>
  </#if>
  <script type="text/javascript">
    addToElementTouchedArray('${parameters.id}');
  </script>
</#if>
</div><#t/>
</div></td></tr>
</#if>
<tr>
<td class="wwlbl_td" valign="top">
<#if parameters.label?exists>
<div <#rt/><#if parameters.id?exists>id="wwlbl_${parameters.id}"<#rt/></#if> class="wwlbl">
    <label <#t/>
		<#if parameters.disabled?default(false)>
 			class="label disabled"<#rt/>
 		<#else>
 			class="label"<#rt/>
		</#if>
		
		<#if parameters.id?exists>
            for="${parameters.id}"<#rt/>
        </#if>
        
		
    ><#t/>
        ${parameters.label}<#if parameters.required?default(false)>&nbsp;<span class="required">*</span><#t/></#if>
<#include "/${parameters.templateDir}/xhtml/tooltip.ftl" />
        </label></div><#t/>
</#if>
</td>
    <#lt/>
<td>
<div <#rt/><#if parameters.id?exists>id="wwctrl_${parameters.id}"<#rt/></#if> class="wwctrl">
