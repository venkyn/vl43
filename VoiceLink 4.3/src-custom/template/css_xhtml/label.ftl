<#if parameters.trueValue?exists >
  <#assign trueText = '${parameters.trueValue}'/>
<#else>
  <#assign trueText = 'label.readonly.true'/>
</#if>

<#if parameters.falseValue?exists >
  <#assign falseText = '${parameters.falseValue}'/>
<#else>
  <#assign falseText = 'label.readonly.false'/>
</#if>

<#-- Assume a label is readonly, unless otherwise specified -->
<#if !(parameters.readonly??)>
	<#assign parameters = parameters + {'readonly':true} />
</#if>

<#include "/${parameters.templateDir}/css_xhtml/controlheader.ftl" />


<#if parameters.url?exists>
<a href="${parameters.url}">
</#if>
 <div<#rt/>
 <#if parameters.id?exists>
 id="${parameters.id?html}"<#rt/>
</#if>
<#if parameters.cssClass?exists>
 class="${parameters.cssClass?html}"<#rt/>
</#if>
<#if parameters.cssStyle?exists>
 style="${parameters.cssStyle?html}"<#rt/>
</#if>
<#if parameters.title?exists>
 title="${parameters.title?html}"<#rt/>
</#if>
<#if parameters.for?exists>
 for="${parameters.for?html}"<#rt/>
</#if>
><#rt/>
<#if parameters.nameValue?exists>
<#if parameters.nameValue="true">
     <@s.text name=trueText /><#rt/>
<#elseif parameters.nameValue="false">
     <@s.text name=falseText /><#rt/>
<#else>
     <@s.property value="parameters.nameValue"/><#t/>
</#if>
</#if>
</div>
<#if parameters.url?exists>
</a>
</#if>
<#include "/${parameters.templateDir}/css_xhtml/controlfooter.ftl" />