<#--
NOTE: The 'header' stuff that follows is in this one file for checkbox due to the fact
that for checkboxes we do not want the label field to show up as checkboxes handle their own
lables
-->
<#assign hasFieldErrors = fieldErrors?exists && fieldErrors[parameters.name]?exists/>
<div <#rt/><#if parameters.id?exists>id="wwgrp_${parameters.id}"<#rt/></#if> class="wwgrp">
<br>
<#if hasFieldErrors>
<div <#rt/><#if parameters.id?exists>id="wwerr_${parameters.id}"<#rt/></#if> class="wwerr">
<#list fieldErrors[parameters.name] as error>
    <div<#rt/>
    <#if parameters.id?exists>
     errorFor="${parameters.id}"<#rt/>
    </#if>
    class="errorMessage">
             ${error?html}
    </div><#t/><#-- end errorFor div -->
</#list>
</div><#t/><#-- end wwerr div -->
</#if>
<div class="wwctrlgrp">
<table class="wwctrlgrp_table"><tr><td class="wwlbl_td">
<#if ( (parameters.readonly?exists && parameters.readonly=="true") || (parameters.stayleft?exists && parameters.stayleft=="true") )>
	<#if parameters.label?exists>
	<div <#rt/><#if parameters.id?exists>id="wwlbl_${parameters.id}"<#rt/></#if> class="wwlbl">
	    <label class="label" <#t/> <#if parameters.id?exists>for="${parameters.id}"<#rt/></#if>
	    ><#t/>
	        ${parameters.label?html}<#if parameters.required?default(false)>&nbsp;<span class="required">*</span><#t/></#if>
	<#include "/${parameters.templateDir}/xhtml/tooltip.ftl" />
	        </label></div><#t/>
</#if>
<#else>
<div <#rt/><#if parameters.id?exists>id="wwlbl_${parameters.id}"<#rt/></#if> class="wwlbl">&nbsp;</div>
</#if>
</td><td>
<#if parameters.labelposition?default("top") == 'top'>
<div <#rt/>
<#else>
<span <#rt/>
</#if>
<#if parameters.id?exists>id="wwctrl_${parameters.id}"<#rt/></#if> class="wwctrl">

<#-- added check for label to avoid redundant 'required' indicators -->
<#if parameters.required?default(false) && !parameters.label?exists>
        <span class="required">*</span><#t/>
</#if>

<#-- Above Here Is To The Left Of The Checkbox -->
<#include "/${parameters.templateDir}/simple/checkbox.ftl" />
<#-- Below Here Is To The Right Of The Checkbox -->
<#if parameters.labelposition?default("top") == 'top'>
</div> <#rt/><#-- end wwctrl div -->
<#else>
</span>  <#rt/>
</#if>
<#if parameters.label?exists> 
<#if parameters.labelposition?default("top") == 'top'>
<div <#rt/>
<#else>
<span <#rt/>
</#if>
<#if parameters.id?exists>id="wwlbl_${parameters.id}"<#rt/></#if> class="wwlbl">
<#-- <label<#t/>
<#if parameters.id?exists>
 for="${parameters.id?html}"<#rt/>
</#if>
<#if hasFieldErrors>
 class="checkboxErrorLabel"<#rt/>
<#else>
 class="checkboxLabel"<#rt/>
</#if>
>${parameters.label?html}</label><#rt/>-->
</#if> 
<#include "/${parameters.templateDir}/css_xhtml/controlfooter.ftl" /><#nt/>