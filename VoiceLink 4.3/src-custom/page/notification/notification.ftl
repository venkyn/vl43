<html>
    <title><@s.text name='notification.view.title'/></title>
    <script type="text/javascript">
        function displaySite(obj) {
            if ( obj.site.id ) {
                return A({'id': 'id-'+obj.id+'_'+obj.site.name, 'href':'${base}/admin/site/view.action?siteId='+obj.site.id}, obj.site.name);
            } else {
 			    return obj.site.name;
            }
        }
    </script>
 
    <#assign title>
        <@s.text name='notification.detail.view.title'/>
    </#assign>
    
    <#include "/include/common/tablecomponent.ftl">

    <#if !currentSiteID?has_content> 
	   <#assign currentSiteID = -927 />
    </#if>
    
    <#if numberOfSites = 1>
	   <#assign currentSiteID = -927 />
    </#if>
    
    <@tablecomponent
       		varName="notificationObj"
        	columns=notificationColumns
            tableId="listNotificationTable"
            url="${base}/admin/notification"
            tableTitle='notification.view.title'
            type=""
            extraParams={"extraURLParams":[{"name":"tempSiteID","value":"${currentSiteID?c}"}],"rowHighlightTwoConditions" :[{"field":"priority","value":"notification.column.keyname.Priority.CRITICAL","field1":"acknowledgedDateTime", "value1":""}]}
            viewId=notificationViewId/>
    
    <br/><br/>
    
    <script type="text/javascript">
    	function getNewData(e) {
			 var url="getDetailData.action";
			 if(notificationObj.getSelectedIds()!=null && !(notificationObj.getSelectedIds()=="") )
			   {
			      doSimpleXMLHttpRequest(url, {'notificationId': notificationObj.getSelectedIds()})
			      .addCallbacks(getDetails, function (request) {reportError(request.responseText);});
			   }        
		 }
		 
        function getDetails(request){
            if(request.responseText == "") {
                var elements = $("notificationD");
                var notificationDiv = DIV({'id':'notificationD'},null);
                swapDOM(elements, notificationDiv);
                return;
            }
            
            var data = evalJSONRequest(request);
          	
            if(data != null) {
                var notificationDiv = DIV({'id':'notificationD','class':'notDiv'},null);
                var notificationTitle = DIV({'class':'titleBar'},'${title}');
                var notificationDetails = DIV({'class':'notFormBody'},null);
                var notTable = TABLE({},null);
                var notTableBody = TBODY({},null);
                
                for(i = 0; i < data.objects.length; i++) {
                    var tr = TR({},null);
                    var td1 = TD({'class':'notTd'},' '+ data.objects[i].label);
                    var x = data.objects[i].value;
                    x=x.toString();
                    
                    if(x.indexOf('http') > -1) {
                        var val = A({'class':'notLink','href':x}, x);
                        var td2 = TD({}, ': ');
                        appendChildNodes(td2,val);
                    }else{
                        var val = x;
                        var td2 = TD({}, ': '+val);
                    }
                    appendChildNodes( tr, td1,td2);
                    appendChildNodes( notTableBody,tr);
                }                    
                appendChildNodes( notTable,notTableBody);
                appendChildNodes( notificationDetails, notTable);
                appendChildNodes(notificationDiv,notificationTitle, notificationDetails);
            } else {
                var notificationDiv = DIV({'id':'notificationD'},null);
            }
            
            var element = $("notificationD");
            if( element == null ) {
                appendChildNodes($("canvas"), notificationDiv);
            } else {
            	swapDOM(element, notificationDiv);
            }
        }
        
        $j(function(){
        	$j(document).bind(events.SELECTION_CHANGE, getNewData);
        });  
 </script>
</html>

 

 
