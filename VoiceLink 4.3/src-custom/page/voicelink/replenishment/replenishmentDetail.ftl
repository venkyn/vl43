<title><@s.text name='replenishmentDetail.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">
	<#assign pageContext="${base}/${navigation.applicationMenu}">	
 	
     <#assign urlMethod="getReplenishmentDetailData"/>
     <@tablecomponent
 		varName="replenishmentDetailObj"
		columns=replenishmentDetailColumns
		tableId="listReplenishmentsDetailsTable"
		url="${pageContext}/assignmentDetail"
		tableTitle='replenishmentDetail.view.title' 
    	viewId=replenishmentDetailViewId
    	type=""
    	extraParams=extraParamsHash?default("") />    	
 