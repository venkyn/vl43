<#assign readOnly="false">
<#assign yesreadOnly="true">

 <#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='replenishment.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='replenishment.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#else>  
  <#assign title>
    <@s.text name='replenishment.view.single.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
 </#if>

<#assign saveValue>
   <@s.text name="form.button.submit"/>
  </#assign>
  <#assign cancelValue>
    <@s.text name="form.button.cancel"/>
</#assign>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/replenishment/assignment" action="${formName}.action" method="POST">
<input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>
<#if replenishmentId?has_content>
    <@s.hidden name="replenishmentId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>

<div class="formTop">

			       <@s.textfield tabindex="1" 
                     label="%{getText('replenishment.assignment.label.priority')}" 
                     name="replenishPriority" 
                     id="replenishPriority" 
                     size="25" 
                     maxlength="40" 
                     show="true"
                     readonly="${yesreadOnly}" />                   

                   <#if readOnly="true" >
                     <@s.label label="%{getText('replenishment.assignment.label.status')}" 
                     name="replenishStatus" 
                     id="replenishStatus" 
                     size="25" 
                     maxlength="128" />

                   <#else>
                   <@s.select tabindex="3" 
                     label="%{getText('replenishment.assignment.label.status')}" 
                     name="replenStatus" 
                     id="replenStatus" 
                     list="replenishmentStatusMap" 
                     cssClass="statusSelect" 
                     readonly="${readOnly}" />
                   </#if>                   

                   <@s.textfield tabindex="2" 
                     label="%{getText('replenishment.assignment.label.type')}" 
                     name="replenishType"
                     id="replenishType" 
                     size="25" maxlength="40" 
                     show="true"
                     readonly="${yesreadOnly}" />
                                      
                   <@s.textfield tabindex="3" 
    			   label="%{getText('replenishment.assignment.label.sequenceNumber')}" 
    			   name="replenishment.sequenceNumbers" 
    			   id="replenishment.sequenceNumber" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
    			   
    			   <@s.textfield tabindex="4" 
    			   label="%{getText('replenishment.assignment.label.regionName')}" 
    			   name="replenishment.region.name" 
    			   id="replenishment.region.name" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="5" 
    			   label="%{getText('replenishment.assignment.label.reserveLocation')}" 
    			   name="replenishment.fromLocation.scannedVerification" 
    			   id="replenishment.fromLocation.scannedVerification" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="66" 
    			   label="%{getText('replenishment.assignment.labe.destinationLocation')}" 
    			   name="replenishment.toLocation.scannedVerification" 
    			   id="replenishment.toLocation.scannedVerification" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                    <#if (allOperatorsInRegion?size > 1)>
	                   <@s.select tabindex="7" 
    				   label="%{getText('replenishment.assignment.label.operatorName')}" 
    				   name="operatorId" 
    				   id="operatorId" 
    				   list="allOperatorsInRegion" 
	    			   readonly="${readOnly}"
    				    />
    			   <#else>
	                   <@s.textfield tabindex="7" 
    					   label="%{getText('replenishment.assignment.label.operatorName')}" 
    					   name="operatorId" 
    				       id="operatorId" 
    					   size="25" maxlength="40" 
    					   show="true"
		                   readonly="${yesreadOnly}" />
    			   </#if>
    			   
                   <@s.textfield tabindex="8" 
    			   label="%{getText('replenishment.assignment.label.startTime')}" 
    			   name="replenishment.startTime" 
    			   id="replenishment.startTime" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="9" 
    			   label="%{getText('replenishment.assignment.label.endTime')}" 
    			   name="replenishment.endTime" 
    			   id="replenishment.endTime" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="10" 
    			   label="%{getText('replenishment.assignment.label.replenishQuantity')}" 
    			   name="replenishment.replenishQuantity" 
    			   id="replenishment.replenishQuantity" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="11" 
    			   label="%{getText('replenishment.assignment.label.totalReplenishedQuantity')}" 
    			   name="replenishment.totalReplenishedQuantity" 
    			   id="replenishment.totalReplenishedQuantity" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="12" 
    			   label="%{getText('replenishment.assignment.label.licenseNumber')}" 
    			   name="replenishment.licenseNumber" 
    			   id="replenishment.licenseNumber" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="13" 
    			   label="%{getText('replenishment.assignment.label.spokenValidation')}" 
    			   name="replenishment.spokenValidation" 
    			   id="replenishment.spokenValidation" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="14" 
    			   label="%{getText('replenishment.assignment.label.scannedValidation')}" 
    			   name="replenishment.scannedValidation" 
    			   id="replenishment.scannedValidation" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="15" 
    			   label="%{getText('replenishment.assignment.label.itemNumber')}" 
    			   name="replenishment.item.number" 
    			   id="replenishment.item.number" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="16" 
    			   label="%{getText('replenishment.assignment.label.itemDescription')}" 
    			   name="replenishment.item.description" 
    			   id="replenishment.item.description" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="17" 
    			   label="%{getText('replenishment.assignment.label.goalRate')}" 
    			   name="replenishment.region.goalRate" 
    			   id="replenishment.region.goalRate" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="18" 
    			   label="%{getText('replenishment.assignment.label.id')}" 
    			   name="replenishment.number" 
    			   id="replenishment.number" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="19" 
    			   label="%{getText('replenishment.assignment.label.scanVerification')}" 
    			   name="replenishment.item.scanVerificationCode" 
    			   id="replenishment.item.scanVerificationCode" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="20" 
    			   label="%{getText('replenishment.assignment.label.spokenVerification')}" 
    			   name="replenishment.item.spokenVerificationCode" 
    			   id="replenishment.item.spokenVerificationCode" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="21" 
    			   label="%{getText('replenishment.assignment.label.detailCount')}" 
    			   name="replenishment.detailCount" 
    			   id="replenishment.detailCount" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="21" 
    			   label="%{getText('replenishment.assignment.label.regionNumber')}" 
    			   name="replenishment.region.number" 
    			   id="replenishment.region.number" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="22" 
    			   label="%{getText('replenishment.assignment.label.dateReceived')}" 
    			   name="replenishment.dateReceived" 
    			   id="replenishment.dateReceived" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="23" 
    			   label="%{getText('replenishment.assignment.label.upc')}" 
    			   name="replenishment.item.upc" 
    			   id="replenishment.item.upc" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="24" 
    			   label="%{getText('replenishment.assignment.label.originalSequenceNumber')}" 
    			   name="replenishment.originalSequenceNumber" 
    			   id="replenishment.originalSequenceNumber" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                    
                   
</div>
<div class="formAction">
	
	<#if replenishmentId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='assignment.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('replenishment.assignment.label.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('replenishment.assignment.label.status')}" name="modifiedEntity.status"/>
	    
	    <@s.label label="%{getText('replenishment.assignment.label.operatorname')}" name="operatorID"/>
	    
	    <@s.label label="%{getText('replenishment.assignment.label.edit.sequenceNumber')}" name="modifiedEntity.sequence.sequenceNumber"/>
	
    
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</body>
</html>

