<title><@s.text name='replenishment.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
	<#assign pageContext="${base}/${navigation.applicationMenu}">
    
        <#assign urlMethod="getReplenishmentData"/>
 	 	<@tablecomponent
 		varName="replenishmentObj"
		columns=replenishmentColumns
		tableId="listReplenishmentsTable"
		url="${pageContext}/assignment"
		tableTitle='replenishment.view.title' 
    	viewId=replenishmentViewId
    	type=""
    	extraParams=extraParamsHash?default("") />
