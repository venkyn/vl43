<#include "/include/common/columnformatting.ftl" />
<#include "/include/common/controlgrouping.ftl" />

<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='replenishment.region.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='replenishment.region.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='replenishment.region.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='replenishment.region.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='replenishment.region.view.single.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/replenishment/region" action="${formName}.action?actionValue='save'" method="POST">
<#if regionId?has_content>
    <@s.hidden name="regionId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@columnlayout header="">
    <div class="yui-u first">

    <#if readOnly=="true">
		<@s.textfield 
	    	tabindex="1" 
	    	label="%{getText('replenishment.region.create.label.name')}" 
	    	name="translatedRegionName" 
	    	id="replenishmentRegion.name" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	required="true" 
	    	readonly="${readOnly}"/>
	<#else>
		<@s.textfield 
	    	tabindex="1" 
	    	label="%{getText('replenishment.region.create.label.name')}" 
	    	name="replenishmentRegion.name" 
	    	id="replenishmentRegion.name" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	required="true" 
	    	readonly="${readOnly}"/>
    		<#if translatedRegionNameOrNull?has_content>	
            	<@s.label 
    		   		label="" 
    				name="translatedRegionNameMessage" 
    				id="replenishmentRegion.name.translation"/>
     		</#if>	
   	</#if>

    <@s.textfield 
        tabindex="2" 
        label="%{getText('replenishment.region.create.label.number')}" 
        name="replenishmentRegion.number" 
        id="replenishmentRegion.number" 
        size="10" 
        maxlength="9" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>

    <#if readOnly=="true">
	 	<@s.textfield 
	    	tabindex="3" 
	    	label="%{getText('replenishment.region.create.label.description')}" 
	    	name="translatedRegionDescription" 
	    	id="replenishmentRegion.description" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	readonly="${readOnly}"/> 
	<#else>
	 	<@s.textfield 
	    	tabindex="3" 
	    	label="%{getText('replenishment.region.create.label.description')}" 
	    	name="replenishmentRegion.description" 
	    	id="replenishmentRegion.description" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	readonly="${readOnly}"/> 
    		<#if translatedRegionDescriptionOrNull?has_content>	
            	<@s.label 
    		   		label="" 
    				name="translatedRegionDescriptionMessage" 
    				id="replenishmentRegion.description.translation"/>
     		</#if>	
   	</#if>

	<@s.textfield 
	    tabindex="4" 
	    label="%{getText('replenishment.region.create.label.goalRate')}" 
	    name="replenishmentRegion.goalRate" 
	    id="replenishmentRegion.goalRate" 
	    size="25" 
	    maxlength="5" 
	    show="true" 
	    required="true" 
	    readonly="${readOnly}"/>
	</div>
   	</@columnlayout>
    </div>
    <div class="formMiddle">
      <@columnlayout header="">
      <div class="yui-u first">
      <#-- General Task Settings -->
      <@controlgroup groupId="general" caption="replenishment.group.caption.general" disabled="false">
   	
   	
	 <@s.checkbox 
	    tabindex="5" 
        theme="css_xhtml" 	    
	    label="%{getText('replenishment.region.create.label.allowCancelLicense')}" 
	    name="replenishmentRegion.allowCancelLicense" 
	    id="replenishmentRegion.allowCancelLicense" 
	    show="true"  
	    stayleft="true"
        trueValue="replenishment.region.view.true"
        falseValue="replenishment.region.view.false"	    
        cssClass="checkbox"  
	    readonly="${readOnly}"/>   
	    
	 <@s.checkbox 
	    tabindex="6" 
	    label="%{getText('replenishment.region.create.label.allowOverrideLocation')}" 
	    name="replenishmentRegion.allowOverrideLocation" 
	    id="replenishmentRegion.allowOverrideLocation"  
	    show="true"  
	    stayleft="true"
        trueValue="replenishment.region.view.true"
        falseValue="replenishment.region.view.false"	    
        cssClass="checkbox"  	    
	    readonly="${readOnly}"/>   
	    
	 <@s.checkbox 
	    tabindex="7" 
	    label="%{getText('replenishment.region.create.label.allowPartialPut')}" 
	    name="replenishmentRegion.allowPartialPut" 
	    id="replenishmentRegion.allowPartialPut"  
	    show="true"  
	    stayleft="true"
        trueValue="replenishment.region.view.true"
        falseValue="replenishment.region.view.false"	    
        cssClass="checkbox"  
	    readonly="${readOnly}"/> 	    
	    
	 <@s.textfield 
	    tabindex="8" 
	    label="%{getText('replenishment.region.create.label.exceptionLocation')}" 
	    name="replenishmentRegion.exceptionLocation" 
	    id="replenishmentRegion.exceptionLocation" 
	    size="25" 
	    maxlength="50" 
	    show="true" 
	    readonly="${readOnly}"/> 	    

	 <@s.checkbox 
	    tabindex="9" 
	    label="%{getText('replenishment.region.create.label.verifySpokenLicenseLocation')}" 
	    name="replenishmentRegion.verifySpokenLicenseLocation" 
	    id="replenishmentRegion.verifySpokenLicenseLocation"  
	    show="true"  
	    stayleft="true"
        trueValue="replenishment.region.view.true"
        falseValue="replenishment.region.view.false"	    
        cssClass="checkbox"  	    
	    readonly="${readOnly}"/> 
	    
   	</@controlgroup> 
    <#-- Quantity Prompts -->
    <@controlgroup groupId="quantity" caption="replenishment.group.caption.quantity" disabled="false">
	    
	    
	 <@s.checkbox 
	    tabindex="10" 
	    label="%{getText('replenishment.region.create.label.capturePickUpQty')}" 
	    name="replenishmentRegion.capturePickUpQty" 
	    id="replenishmentRegion.capturePickUpQty" 
	    show="true" 
	    stayleft="true"
        trueValue="replenishment.region.view.true"
        falseValue="replenishment.region.view.false"	    
        cssClass="checkbox"  	    
	    readonly="${readOnly}"/> 	
	    
	 <@s.checkbox 
	    tabindex="11" 
	    label="%{getText('replenishment.region.create.label.allowOverridePickUpQty')}" 
	    name="replenishmentRegion.allowOverridePickUpQty" 
	    id="replenishmentRegion.allowOverridePickUpQty" 
	    show="true" 
	    stayleft="true"
        trueValue="replenishment.region.view.true"
        falseValue="replenishment.region.view.false"	    
        cssClass="checkbox"  	    
	    readonly="${readOnly}"/>   
	        
	 <@s.checkbox 
	    tabindex="12" 
	    label="%{getText('replenishment.region.create.label.capturePutQty')}" 
	    name="replenishmentRegion.capturePutQty" 
	    id="replenishmentRegion.capturePutQty" 
	    show="true" 
	    stayleft="true"
        cssClass="checkbox"  
        trueValue="replenishment.region.view.true"
        falseValue="replenishment.region.view.false"
	    readonly="${readOnly}"/>   

     </@controlgroup>
     <#-- Task Data Entry -->
     <@controlgroup groupId="task" caption="replenishment.group.caption.task" disabled="false">
	    
	 <@s.textfield 
	    tabindex="13" 
	    label="%{getText('replenishment.region.create.label.licDigitsTaskSpeaks')}" 
	    name="replenishmentRegion.licDigitsTaskSpeaks" 
	    id="replenishmentRegion.licDigitsTaskSpeaks" 
	    size="25" 
	    maxlength="2" 
	    show="true" 
	    required="true" 
	    readonly="${readOnly}"/> 
	    
	 <@s.select 
		tabindex="14" 
	    label="%{getText('replenishment.region.create.label.locDigitsOperSpeaks')}" 
	    name="replenishmentRegion.locDigitsOperSpeaks" 
	    id="replenishmentRegion.locDigitsOperSpeaks" 
	    list="locDigitsOperatorSpeaksOptions" 
	    readonly="${readOnly}" 
		cssClass="digitsDropdown"
		disabled="false" />
		
     <@s.select 
	    tabindex="15" 
	    label="%{getText('replenishment.region.create.label.checkDigitsOperSpeaks')}" 
	    name="replenishmentRegion.checkDigitsOperSpeaks" 
	    id="replenishmentRegion.checkDigitsOperSpeaks"
	    list="checkDigitsOperatorSpeaksOptions" 
		readonly="${readOnly}" 
		cssClass="digitsDropdown"
		disabled="false" />	
	           
   	        </@controlgroup>
   	      </div>
   	      
   	      </@columnlayout>
        </div>

<div class="formAction">
	
	<#if regionId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modifiedEntity" style="display:none;">
	<div class="modifiedTitle">
		<@s.text name='modifiedEntity.title'/>
	</div>
		<div class="formTop">
		    <@s.label label="%{getText('replenishment.region.label.name')}" name="modifiedEntity.name" />
		    <@s.label label="%{getText('replenishment.region.label.number')}" name="modifiedEntity.number" />
		    <@s.label label="%{getText('replenishment.region.label.description')}" name="modifiedEntity.description" />
		    <@s.label label="%{getText('replenishment.region.label.goalRate')}" name="modifiedEntity.goalRate" />
		</div>
		<div class="formMiddle">
		    <#-- General Task Settings -->
		    <@sw.label label="%{getText('replenishment.region.label.allowCancelLicense')}" name="modifiedEntity.allowCancelLicense" />
		    <@sw.label label="%{getText('replenishment.region.label.allowOverrideLocation')}" name="modifiedEntity.allowOverrideLocation" />
		    <@sw.label label="%{getText('replenishment.region.label.allowPartialPut')}" name="modifiedEntity.allowPartialPut" />
		    <@sw.label label="%{getText('replenishment.region.label.capturePickupQuantity')}" name="modifiedEntity.capturePickUpQty" />
		    <@sw.label label="%{getText('replenishment.region.label.allowOverrideQuantity')}" name="modifiedEntity.allowOverridePickUpQty" />		    	
		    <@sw.label label="%{getText('replenishment.region.label.allowOverrideQuantity')}" name="modifiedEntity.capturePutQty" />		    			    
		    <@sw.label label="%{getText('replenishment.region.label.licDigitsTaskSpeaks')}" name="modifiedEntity.licDigitsTaskSpeaks" />
		    <@sw.label label="%{getText('replenishment.region.label.locDigitsOperSpeaks')}" name="modifiedEntity.locDigitsOperSpeaks" />		    
		    <@sw.label label="%{getText('replenishment.region.label.checkDigitsOperSpeaks')}" name="modifiedEntity.checkDigitsOperSpeaks" />
		    <@sw.label label="%{getText('replenishment.region.label.verifySpokenLicOrLoc')}" name="modifiedEntity.verifySpokenLicenseLocation" />
		    <@sw.label label="%{getText('replenishment.region.label.verifyLicense')}" name="modifiedEntity.exceptionLocation" />
		</div>
</div>
</#if>
</@s.form>
</div>

</body>
</html>

