<#assign readOnly="false">
<#assign disabled="false">
<#assign selectorActive="true">
<#assign languagePage="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='summaryPrompt.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='summaryPrompt.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='summaryPrompt.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='summaryPrompt.create.button.submit'/>
  </#assign>
<#elseif navigation.pageName="createLanguage">
  <#assign title>
    <@s.text name='summaryPrompt.createLanguage.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='summaryPrompt.createLanguage.button.submit'/>
  </#assign>
  <#assign languagePage="true">
  <#assign selectorActive="false">
<#elseif navigation.pageName="editLanguage">
  <#assign title>
    <@s.text name='summaryPrompt.editLanguage.title'/>
  </#assign>
  <#assign formName="editLanguage">
  <#assign submitValue>
    <@s.text name='summaryPrompt.editLanguage.button.submit'/>
  </#assign>
  <#assign languagePage="true">  
  <#assign selectorActive="false">
<#elseif navigation.pageName="viewLanguage">
  <#assign title>
    <@s.text name='summaryPrompt.viewLanguage.title'/>
  </#assign>
  <#assign formName="viewLanguage">
  <#assign submitValue>
    <@s.text name='summaryPrompt.viewLanguage.button.submit'/>
  </#assign>
  <#assign readOnly="true"> 
  <#assign languagePage="true">  
  <#assign selectorActive="false">
<#else>  
  <#assign title>
    <@s.text name='summaryPrompt.view.single.title'/>
  </#assign>
  <#assign selectorActive="false">
  <#assign formName="view">
  <#assign readOnly="true"> 
</#if>
<#if !formId?has_content>
  <#assign formId="form1">
</#if>
<html>
<head>

<title>${title}</title>
</head>
<body>
<#include "/include/common/controlgrouping.ftl" />

<script type="text/javascript" src="${base}/scripts/vocollect/promptSelector.js"></script> 
<script type="text/javascript" src="${base}/scripts/vocollect/promptOptions.js"></script>    
<script type="text/javascript">
    
        var selTotalArray = new Array();
        <#if totalValues?has_content>
           <#list totalValues as totalValue>
               selTotalArray.push('${totalValue}');
           </#list>
        </#if>
        var selCharArray = new Array();
        <#if charSpeakValues?has_content>
           <#list charSpeakValues as charSpeakValue>
               selCharArray.push('${charSpeakValue}');
           </#list>
        </#if>
        var selNumCharArray = new Array();
        <#if numCharSpeakValues?has_content>
           <#list numCharSpeakValues as numCharSpeakValue>
               selNumCharArray.push('${numCharSpeakValue}');
           </#list>
        </#if>
		var promptItemArray = new Array();
		<#if promptItems?has_content>
			<#list promptItems as promptItem>
				<#if promptItem?has_content>
				    promptItem = {id: "${promptItem.id?c}",
				    			  <#if promptItem.showOptions>
						            showOptions: true,
					        	  <#else>
						        	showOptions: false,
					        	  </#if>
				                  editBoxCount: "${promptItem.editBoxCount}",
				                  optionTitle1: "<@s.text name = 'definition.optionTitle1.' + '${promptItem.id?c}'/>",
				                  optionTitle2: "<@s.text name = 'definition.optionTitle2.' + '${promptItem.id?c}'/>",
				                  optionTitle3: "<@s.text name = 'definition.optionTitle3.' + '${promptItem.id?c}'/>",
				                  optionDefault1: "<@s.text name = 'definition.optionDefault1.' + '${promptItem.id?c}'/>",
				                  optionDefault2: "<@s.text name = 'definition.optionDefault2.' + '${promptItem.id?c}'/>",
				                  optionDefault3: "<@s.text name = 'definition.optionDefault3.' + '${promptItem.id?c}'/>",
				                  fieldType: "${promptItem.fieldType}" }; 
				    promptItemArray.push(promptItem);
				</#if>
			</#list>
		</#if>
		
		var chk_title="<@s.text name = 'summaryPrompt.view.speakindividual' />";
		var totals_title="<@s.text name = 'summaryPrompt.view.totals' />";
		var chars_title="<@s.text name = 'summaryPrompt.view.chars' />";
		var form_name="${formName}";
		var lang=false;
	
	    
		var defaultPromptArray = new Array();
		<#if summaryPrompt?has_content>
		  <#if summaryPrompt.definitions?has_content>
			<#list summaryPrompt.definitions as definition>
				<#if definition?has_content>
				    promptItem = {position: "${definition.position?c}",
  				                  <#if definition.id?has_content>
				                    id: "${definition.id?c}",
				                  <#else>
				                    id: null,
				                  </#if>
				                  promptId: "${definition.promptItem.id?c}",
				                  name: "<@s.text name = '${definition.promptItem.name}'/>",
				                  
  				                  <#if definition.promptValue1?has_content>
				                    promptValue1: "${definition.promptValue1?js_string}",
				                  <#else>
				                    promptValue1: "",
				                  </#if>
				                  
   				                  <#if definition.promptValue2?has_content>  
				                    promptValue2: "${definition.promptValue2?js_string}",
				                  <#else>
				                    promptValue2: "",
				                  </#if>
				                  
				                  <#if definition.promptValue3?has_content>
				                    promptValue3: "${definition.promptValue3?js_string}",
				                  <#else>
				                    promptValue3: "",
				                  </#if>
				                  
				                  <#if definition.allowSpeakingPhonetically>
						            allowSpeakingPhonetically: true,
					        	  <#else>
						        	allowSpeakingPhonetically: false,
					        	  </#if>
				                  speakCharactersIndicator: "${definition.speakCharactersIndicator}",
				                  numberOfCharacters: "${definition.numberOfCharacters}",
				                  speakTotalsIndicator: "${definition.speakTotalsIndicator}" };					
				    defaultPromptArray.push(promptItem);

				</#if>
			</#list>
		  </#if>
		</#if>

    MochiKit.Signal.connect(window, 'onload', this, addDefaultValues);

    setDraggable(${selectorActive});
    function addDefaultValues() {
        for (var i = 0; i < defaultPromptArray.length; i++) {
            addPromptItem(defaultPromptArray[i].position - 1, defaultPromptArray[i].name, defaultPromptArray[i].promptId, defaultPromptArray[i].id);    
        }
	    setItemPositions();
    } 

</script>

<div class="formBody">
<div class="titleBar">
	${title}
</div>
<script type="text/javascript">
	<#-- If the formName starts with 'view' (view or viewLanguage), the page is read-only-->
	if('${formName}'.indexOf('view')==0){
		_readOnly=true;
	}
</script>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/summaryPrompt" action="${formName}.action" method="POST">
<#if summaryPromptId?has_content>
    <@s.hidden name="summaryPromptId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>

<style>
div.promptAddWrapper table.wwctrlgrp_table{
	width:350px;
	float:left;
}

div.formTop table.wwctrlgrp_table{
	width:350px;
}
</style>

<div class="formTop">
  <#if languagePage!="true">
    <@s.textfield 
      tabindex="1" 
      label="%{getText('summaryPrompt.create.label.name')}" 
      name="summaryPrompt.name" 
      id="summaryPrompt.name" 
      size="25" 
      maxlength="50" 
      required="true" 
      readonly="${readOnly}"/>
   
      <#else>
      
      <@s.textfield 
      tabindex="1" 
      label="%{getText('summaryPrompt.create.label.name')}" 
      name="summaryPrompt.name" 
      id="summaryPrompt.name" 
      size="25" 
      maxlength="50" 
      required="true" 
      readonly="true"/>
      </#if>
      
<div class="promptConfiguration">
	<span id="promptTitle" class="promptConfigTitle"><@s.text name='summaryPrompt.view.promptconfig'/></span>
		<#if selectorActive="true">
		<div id="wrapper1" class="promptAddWrapper">
		    <@s.select 
		      tabindex="3" 
		      label="%{getText('summaryPrompt.create.label.type')}" 
		      name="promptTypeId"
		      id="promptTypeId"
		      list="promptTypeMap"
		      readonly="false" 
		      cssClass="promptSelect" />
			<a
			href="javascript:addNewPromptItem();"
			id="addButton"
			class="anchorButton"
			tabindex="4">
			<@s.text name="summaryPrompt.config.button.add"/>
			</a>
		</div>
		<div style="clear:both;"></div>
		</#if>

	<#if languagePage="true">
		<div id="wrapper2" class="promptAddWrapper">	
		<script type="text/javascript">
			lang=true;
		</script>
		<#if formName="create" >
		   <@s.select 
		      tabindex="3" 
		      label="%{getText('summaryPrompt.create.label.language')}" 
		      maxlength="128" 
		      name="language"
		      id="language"
		      list="languageMapForAdd"
		      readonly="${readOnly}" 
		      cssClass="promptSelect" />
		<#else>
			<@s.textfield 
			      tabindex="1" 
			      label="%{getText('summaryPrompt.create.label.language')}" 
			      name="languageSummaryPrompt" 
			      id="languageSummaryPrompt" 
			      size="25" 
			      maxlength="128" 
			      required="true" 
			      readonly="true"/>
	       <@s.hidden name="language"/>
		</#if>
 	    </div>
	</#if>
	<br>
	<div id="wrapper2" class="promptRemoveWrapper">
    	<div class="promptSelector" id="promptSelectorDiv">
	      <div id="itemDiv" class="promptItemContainer">
    	  </div>
	    </div>
	    <#if languagePage="false">
	    	<#if formName!="view">
	    	<a
			href="javascript:removePromptItem();"
			id="removeButton"
			class="anchorButton"
			tabindex="3">
			<@s.text name="summaryPrompt.config.button.remove"/></a>
			</#if>
		</#if>
	</div>
</div>
</div>
<script type="text/JavaScript">
if (form_name == "edit") {
    var anchorElement = $("${submitId?default('${formId}.submit1')}");
    connect($("removeButton"),"onclick",anchorElement,enableSubmitButton);
    connect($("addButton"),"onclick",anchorElement,enableSubmitButton);    
}
</script>

<div class="formMiddle" id="formMiddle">
<div id="itemOptions"></div>
<div id="preview" class="promptItemOptions">
	<div class="innerPromptItemOptions">
	<span id="previewSpan1" class="previewTitle"><@s.text name='summaryPrompt.view.preview'/></span>
	<span class="optionsTitle"><@s.text name='summaryPrompt.view.preview_zero'/></span>
	<div id="preview_zero"></div>
	</div>
	<div class="innerPromptItemOptions">
	<span id="previewSpan2" class="optionsTitle"><@s.text name='summaryPrompt.view.preview_one'/></span>
	<div id="preview_one"></div>
	</div>
	<div class="innerPromptItemOptions">
	<span id="previewSpan3" class="optionsTitle"><@s.text name='summaryPrompt.view.preview_notone'/></span>
	<div id="preview_notone"></div>	
	</div>
</div>
<div class="formBottom"">
<div class="formAction">
	
	<#if summaryPromptId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
</div>
<script language="javascript">
if('${navigation.pageName}' == 'createLanguage'){
           var anchorElement = $("${submitId?default('${formId}.submit1')}");
           enableSubmitButton(anchorElement);
       }
</script>       
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='summaryPrompt.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	  	    <@s.label label="%{getText('summaryPrompt.create.label.name')}" name="modifiedEntity.name"/>
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>
<script type="text/JavaScript">
	YAHOO.util.Event.addListener($('${formId}.submit1'),"click",convertChars);	
</script>

</body>
</html>

