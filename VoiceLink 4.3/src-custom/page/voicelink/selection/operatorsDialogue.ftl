<table border="0" class="content" width="290px"> 
<tr>
<td>
<div id="textContainer" style="font-weight: bold;"><@s.text name="short.createChaseAssignment.operator.label" /><@s.text name='semicolon'/></div>
</td>
<td align="right">
				<@s.select tabindex="3" 
    			   name="operatorID" 
    			   id="operatorID" 
    			   list="operatorList"
    			   readonly="false"
    			   theme="css_nonbreaking"
    			   class="inputElement"/>
</td></tr>
</table>