<#assign readOnly="false">
<#assign yesreadOnly="true">

  <#assign formName="edit">
  
  <#assign saveValue>
    <@s.text name="form.button.submit"/>
  </#assign>
  <#assign cancelValue>
    <@s.text name="form.button.cancel"/>
  </#assign>
<html>
<head>
<title><@s.text name='assignment.edit.title'/></title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	<@s.text name='assignment.edit.title'/>
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/selection/assignment" action="${formName}.action" method="POST">
<input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>
<#if assignmentId?has_content>
    <@s.hidden name="assignmentId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<script type="text/javascript">
	function showEditAssignmentConfirmDialog() {
		var dialogProperties = {
			    title: "<@s.text name='assignment.editStatus.confirmation.header'/>",			    
				body: "<@s.text name='assignment.editStatus.confirmation.body'/>",
				button1: "<@s.text name='assignment.editStatus.confirmation.ok.button'/>",
				button2: "<@s.text name='assignment.editStatus.confirmation.cancel.button'/>"}
		buildCustomDialog(dialogProperties,submitForm, "");
	}
	
	function submitForm() {
	    var formObject = document.forms['${formName}'];
		$(formObject).submit();
	}
</script>

<div class="formTop">

                   <@s.textfield tabindex="1" 
    			   label="%{getText('assignment.label.number')}" 
    			   name="assignment.viewableAssignmentNumber" 
    			   id="assignment.viewableAssignmentNumber" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="2" 
    			   label="%{getText('assignment.label.workidentifier')}" 
    			   name="assignment.workIdentifier.workIdentifierValue" 
    			   id="assignment.workIdentifier.workIdentifierValue" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
           
    			   <@s.select
	               tabindex="3" 
	               label="%{getText('assignment.label.status')}" 
    			   name="assignmentStatus" 
    			   id="assignmentStatus" 
    			   list="assignment.GUITransitionStatuses"
    			   readonly="${readOnly}"/>
                   
                   <#if (allOperatorsInRegion?size > 1)>
	                   <@s.select tabindex="4" 
    				   label="%{getText('assignment.label.operatorname')}" 
    				   name="operatorID" 
    				   id="operatorID" 
    				   list="allOperatorsInRegion" 
	    			   readonly="${readOnly}"
    				    />
    			   <#else>
	                   <@s.textfield tabindex="4" 
    					   label="%{getText('assignment.label.operatorname')}" 
    					   name="operatorID" 
		    			   id="operatorID" 
    					   size="25" maxlength="40" 
    					   show="true"
		                   readonly="${yesreadOnly}" />
    			   </#if>
    			   
    			   <#if assignmentStatus = 3 || assignmentStatus =10>     
                   <@s.select
	                  tabindex="5" 
	                  label="%{getText('assignment.label.priority')}" 
    			      name="assignment.priority" 
    			      id="assignment.priority" 
    			      list="availablePriorities"
    			      readonly="${readOnly}"/>
    			   <#else>
    			     <@s.textfield tabindex="5" 
    				   label="%{getText('assignment.label.priority')}" 
    				   name="assignment.priority" 
		    		   id="assignment.priority" 
    				   size="25" maxlength="40" 
    				   show="true"
		               readonly="${yesreadOnly}" />
		            </#if>
    			    			 
    			    
                   <@s.textfield tabindex="6" 
    			   label="%{getText('assignment.label.starttime')}" 
    			   name="assignmentStartTime" 
    			   id="assignment.startTime" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="7" 
    			   label="%{getText('assignment.label.endtime')}" 
    			   name="assignmentEndTime"
    			   id="assignment.endTime"
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="8" 
    			   label="%{getText('assignment.label.edit.customernumber')}" 
    			   name="assignment.customerInfo.customerNumber" 
    			   id="assignment.customerInfo.customerNumber" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="9" 
    			   label="%{getText('assignment.label.edit.route')}" 
    			   name="assignment.localizedRoute" 
    			   id="assignment.route" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="10" 
    			   label="%{getText('assignment.label.edit.totalitemQuantity')}" 
    			   name="assignment.summaryInfo.totalItemQuantity" 
    			   id="assignment.summaryInfo.totalItemQuantity" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="11" 
    			   label="%{getText('assignment.label.edit.containers')}" 
    			   name="assignment.containerCount" 
    			   id="assignment.containerCount" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   
                   <@s.textfield tabindex="12" 
    			   label="%{getText('assignment.label.edit.variableWeights')}" 
    			   name="assignment.variableWeightTotal" 
    			   id="assignment.variableWeightTotal" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                    <@s.textfield tabindex="13" 
    			   label="%{getText('assignment.label.edit.deliveryDate')}" 
    			   name="assignment.deliveryDate" 
    			   id="assignment.deliveryDate" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                                      
                   <@s.textfield tabindex="14" 
    			   label="%{getText('assignment.label.edit.sequenceNumber')}" 
    			   name="assignment.sequence.sequenceNumber" 
    			   id="assignment.sequence.sequenceNumber" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                                      
                   <@s.textfield tabindex="15" 
    			   label="%{getText('assignment.label.edit.group')}" 
    			   name="assignment.groupInfo.groupNumber" 
    			   id="assignment.groupInfo.groupNumber" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="16" 
    			   label="%{getText('assignment.label.edit.regionName')}" 
    			   name="assignment.region.name" 
    			   id="assignment.region.name" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="17" 
    			   label="%{getText('assignment.label.edit.totalWeight')}" 
    			   name="assignment.summaryInfo.totalWeight" 
    			   id="assignment.summaryInfo.totalWeight" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="18" 
    			   label="%{getText('assignment.label.edit.totalCube')}" 
    			   name="assignment.summaryInfo.totalCube" 
    			   id="assignment.summaryInfo.totalCube" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                                     
                   <@s.textfield tabindex="19" 
    			   label="%{getText('assignment.label.edit.deliveryLocation')}" 
    			   name="assignment.deliveryLocation" 
    			   id="assignment.deliveryLocation" 
    			   size="9" maxlength="9" 
    			   show="true"
                   readonly="${readOnly}" />
                                      
                   <@s.textfield tabindex="20" 
    			   label="%{getText('assignment.label.edit.type')}" 
    			   name="assignmentType" 
    			   id="assignmentType" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                                      
                   <@s.textfield tabindex="21" 
    			   label="%{getText('assignment.label.edit.customerName')}" 
    			   name="assignment.customerInfo.customerName" 
    			   id="assignment.customerInfo.customerName" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="22" 
    			   label="%{getText('assignment.label.edit.customerAddress')}" 
    			   name="assignment.customerInfo.customerAddress" 
    			   id="assignment.customerInfo.customerAddress" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="23" 
    			   label="%{getText('assignment.label.edit.goalTime')}" 
    			   name="assignment.summaryInfo.goalTime" 
    			   id="assignment.summaryInfo.goalTime" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />            
                   
                   <@s.textfield tabindex="24" 
    			   label="%{getText('assignment.label.edit.exported')}" 
    			   name="exportStatus" 
    			   id="exportStatus" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="25" 
    			   label="%{getText('assignment.label.edit.regionNumber')}" 
    			   name="assignment.region.number" 
    			   id="assignment.region.number" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" /> 
</div>
<div class="formAction">

    <a
	href="javascript:saveForm();"
	id="${formName}.save"
	class="${cancelCssClass?default('anchorButton')}"
	tabindex="199"
	>${saveValue}</a>
	
    <a
	href="javascript:modifySubmitVariable('form1','submitTypeElement','method:cancel');"
	id="${formName}.cancel"
	class="${cancelCssClass?default('anchorButton')}"
	tabindex="200"
	>${cancelValue}</a>	
	
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='assignment.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('assignment.label.number')}" name="modifiedEntity.viewableAssignmentNumber"/>
	    
	    <@s.label label="%{getText('assignment.label.status')}" name="modifiedEntity.status"/>
	    
	    <@s.label label="%{getText('assignment.label.operatorname')}" name="operatorID"/>
	    
	    <@s.label label="%{getText('assignment.label.edit.customernumber')}" name="modifiedEntity.customerInfo.customerNumber"/>
	  
	    <@s.label label="%{getText('assignment.label.edit.route')}" name="modifiedEntity.route"/>
	    
	     <@s.label label="%{getText('assignment.label.edit.deliveryDate')}" name="modifiedEntity.deliveryDate"/>
	    
	    <@s.label label="%{getText('assignment.label.edit.sequenceNumber')}" name="modifiedEntity.sequence.sequenceNumber"/>
	    
	    <@s.label label="%{getText('assignment.label.edit.group')}" name="modifiedEntity..groupInfo.groupNumber"/>
	
	    <@s.label label="%{getText('assignment.label.edit.deliveryLocation')}" name="modifiedEntity.deliveryLocation"/>
	     
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>
<script type="text/JavaScript">
    
    // holds present delivery location
    var oldLocation = $("assignment.deliveryLocation").value;
    var newLocation = "-1";
    function saveForm() {
    
      // holds modified delivery location
      newLocation = $("assignment.deliveryLocation").value;
    
    	if ((document.getElementById("assignmentStatus").value == 5 ||
    		document.getElementById("assignmentStatus").value == 8)  &&
    		 newLocation == oldLocation ) {
			showEditAssignmentConfirmDialog();
		} else {		
			submitForm();
		}
    }  

</script>

</body>
</html>

