<title><@s.text name='assignoperators.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">		
    <#include "/include/common/controlgrouping.ftl" />
    <#assign pageContext="${base}/${navigation.applicationMenu}">   
    
<#assign formName="showAssignOperators">

<@s.form name="${formName}"  id="form1" validate="true" namespaceVal="/selection/operator" action="${formName}.action" method="POST">
<div style="position:relative;">	   
    <@controlgroup addpadding="true" groupId="operatorSummary" caption="${selectedRegionName} :" disabled="false">
                  
		    <@s.textfield tabindex="7" label="${assignedOperatorText}" readonly="true" />		   
		    <@s.textfield tabindex="8" label="${availableOperatorText}" readonly="true" />		  
		    <@s.textfield tabindex="9" label="${estimatedTimeText}" readonly="true" />		  		   
		    <@s.hidden tabindex="10" label="" name="regionId" id="regionId" />   
		    
		    
    </@controlgroup>                    
</div>		
 	
<br> </br>

<div>
 	<@tablecomponent
 		varName="operatorObj"
		columns=operatorColumns
		tableId="listOperatorsTable"
		url="${pageContext}/operator"
		tableTitle='operator.view.title' 
    	viewId=assignOperatorViewId
    	type=""
    	extraParams={"extraURLParams":[{"name":"regionId","value":"${regionId?c}"}]} />
</div>  

<input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>
</@s.form>

        <#if !formId?has_content>
	  		<#assign formId="form1">
  	    </#if>
  	    
  	    <#if !assignButtonLabel?has_content>
			<#assign assignButtonLabel>
				<@s.text name="assignoperators.assign.buttonText"/>
			</#assign>
		</#if>
  	    
  	    <#if !unassignButtonLabel?has_content>
			<#assign unassignButtonLabel>
				<@s.text name="assignoperators.unassign.buttonText"/>
			</#assign>
		</#if>
		
     <br> </br>
  <div>
		 <div id="assignSummary" align="left">
		 </div>     
     <br>    	  
        <a
		 href="javascript:gotoUrlWithSelected(operatorObj,'${pageContext}/operator/assignOperators.action','ids');"
         id="${submitId?default('${formId}.assign')}"
		 class="${submitCssClass?default('anchorButton')}"
		 tabindex="${submitIndex?default('-1')}"
		 >&nbsp;&nbsp;&nbsp;&nbsp;${assignButtonLabel}&nbsp;&nbsp;&nbsp;&nbsp;</a>
	 <br> <br>
		<div id="unassignSummary" align="left">
	    </div>
	 <br>
		<a			
		 href="javascript:gotoUrlWithSelected(operatorObj,'${pageContext}/operator/unassignOperators.action','ids');"
         id="${cancelId?default('${formId}.unassign')}"
		 class="${submitCssClass?default('anchorButton')}"		
		 tabindex="${submitIndex?default('-1')}"
		 >${unassignButtonLabel}</a>
  </div>   

   	
	<script type="text/javascript">
	   var enableSubmit = true;
	   
	   function gotoUrlWithSelected (tableObject, url, idParam) {
	   
	        if(!enableSubmit){
				return;
			}			
			idToken = idParam + "=";
			endLine = "?" + idToken + tableObject.getSelectedIds().join("&" + idToken);
			endLine = endLine + "&regionId="+${regionId?c};	
			window.document.location.href = url + endLine;
		}
	    
        function getAsynchronourSummary(e) {
    	    getDataOnSelectedForOprAssign (operatorObj, '${pageContext}/operator/showAssignSummary.action'); 
    	    getDataOnSelectedForOprUnassign (operatorObj, '${pageContext}/operator/showUnassignSummary.action'); 
        }      
        
        function getDataOnSelectedForOprAssign(tableObject,action){
        
            enableDisableButtonsOnTableComponentSelect($("form1.assign"), tableObject);
                        
		 	paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");		 	
		 	paramList = paramList + "&regionId="+${regionId?c};		 	
		 	
			doSimpleXMLHttpRequest(action + paramList, {})
				.addCallbacks(appendAssignSummary, 
					function (request) {
		            	if (request.number == 403) {
		            	    	writeStatusMessage(error403Text, 'actionMessage error');
		            	} else {
		                       reportError(request);
		                }
					}
				);
			tableObj = tableObject;
		}
		
		function getDataOnSelectedForOprUnassign(tableObject,action){		
		    
		    enableDisableButtonsOnTableComponentSelect($("form1.unassign"), tableObject);
		                
		 	paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
		 	paramList = paramList + "&regionId="+${regionId?c};		 	
		 			 	
			doSimpleXMLHttpRequest(action + paramList, {})
				.addCallbacks(appendUnassignSummary, 
					function (request) {
		            	if (request.number == 403) {
		            	    	writeStatusMessage(error403Text, 'actionMessage error');
		            	} else {
		                       reportError(request);
		                }
					}
				);
			tableObj = tableObject;
		}
		 
		 function appendAssignSummary(request){ 		 
			    var data = evalJSONRequest(request);		    
			    replaceChildNodes($('assignSummary'), SPAN({'class': 'midBubble'},data.generalMessage));
		 }
		 
		 function appendUnassignSummary(request){		      
			    var data = evalJSONRequest(request);		    
			    replaceChildNodes($('unassignSummary'), SPAN({'class': 'midBubble'},data.generalMessage));
		 }
		 
		 function enableDisableButtonsOnTableComponentSelect(anchorElement, tableObject){		 
			 if(tableObject.getSelectedIds() == ''){ 
	              addElementClass(anchorElement,"disabled");
	              enableSubmit = false;
	            }else{ 
	              removeElementClass(anchorElement,"disabled"); 
	              enableSubmit = true;
	            }		 
		 }		 
           		
         connect($('listOperatorsTable'), 'onSelectedRowsChanged', this, getAsynchronourSummary);  
         connect($('listOperatorsTable'), 'onTableInitialized', this, getAsynchronourSummary); 		
   	    
   	     contextHelpUrl = 59;
   	    
    </script type="text/javascript">
