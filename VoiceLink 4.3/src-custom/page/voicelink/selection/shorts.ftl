<title><@s.text name='short.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
    
 	<@tablecomponent
 		varName="shortObj"
		columns=shortColumns
		tableId="listShortsTable"
		url="${base}/selection/short"
		tableTitle='short.view.title' 
		viewId=shortViewId
    	type=""
    	extraParams=extraParamsHash?default("") />
		