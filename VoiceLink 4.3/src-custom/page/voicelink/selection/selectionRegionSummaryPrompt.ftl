<table border="0" class="content" width="100%">
<tr><td colspan="2"> 
<div id="textContainer">
   <@s.text name="summaryPrompt.assign.region.label" /><@s.text name='semicolon'/>
</div> 
</td>
</tr>
<tr>
<td>
	<div id="textContainer" style="font-weight: bold;">
				<@s.text name="region.normal.summaryPrompt" /><@s.text name='semicolon'/>
</div> 
</td>
<td>
<div id="textContainer">	
 <@s.select tabindex="1"
            label=""
            name="normalSummaryPromptId" 
	        id="normalSummaryPromptId" 
	        list="assignmentSummaryPromptMap" 
	        cssClass="regionSummaryPromptSelect"  
	        listValue="value" 
	        listKey="key" 
	        value="key " 
	        readonly="false"
	        theme="css_nonbreaking" 
	        class="inputElement"/>
 </div>
</td>
	</tr>
<tr>	
<td>
	<div id="textContainer" style="font-weight: bold;">
				<@s.text name="region.chase.summaryPrompt" /><@s.text name='semicolon'/>
</div> 
</td>
<td>
<div id="textContainer">
<@s.select tabindex="1"
            label=""
            name="chaseSummaryPromptId" 
	        id="chaseSummaryPromptId" 
	        list="assignmentSummaryPromptMap" 
	        cssClass="regionSummaryPromptSelect"  
	        listValue="value" 
	        listKey="key" 
	        value="key " 
	        readonly="false"
	        theme="css_nonbreaking" 
	        class="inputElement"/>
</div>
</td>
</tr>
</table>
