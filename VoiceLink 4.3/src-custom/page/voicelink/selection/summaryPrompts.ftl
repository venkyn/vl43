
<title><@s.text name='summaryPrompt.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
	<script type="text/javascript">
        function displayLanguageLink(obj, languageCode) {
            if (obj.definitions[languageCode] != null) {
                return painters.builders.link({
        'href' : globals.PAGE_CONTEXT + '/summaryPrompt/viewLanguage.action?language=' + languageCode + '&summaryPromptId=' + obj.id}, '<@s.text name='summaryPrompt.language.true'/>');
         
            } else {
                return '<@s.text name='summaryPrompt.language.false'/>';
            }
        }    
    </script>
    
    <#assign urlMethod="getSummaryPromptData"/>
 	<@tablecomponent
 		varName="summaryPromptObj"
		columns=summaryPromptColumns
		tableId="listSummaryPromptTable"
	    url="${base}/selection/summaryPrompt"
        tableTitle='summaryPrompt.view.title' 
    	viewId=summaryPromptViewId
    	type=""
    	extraParams=extraParamsHash?default("") />
    	
    