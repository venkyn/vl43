<div id="manualpickselect">
<table border="0" class="content" width="80%"> 
	<tr height="24px">
		<td>
			<div id="textContainer" style="font-weight: bold;">
				<@s.text name="pick.label.status" /><@s.text name='semicolon'/>
			</div>
		</td>
		<td align="right">
			<div id="textContainer">
 				<@s.textfield tabindex="1" 
	  			   name="pickStatus" 
    			   id="pickStatus"
    			   size="25" maxlength="40" 
    			   show="true"
    			   readonly="true"
    			   theme="css_nonbreaking"/>
		   </div>
		</td>
	</tr>
	<tr height="24px">
		<td>
			<div id="textContainer" style="font-weight: bold;">
				<@s.text name="pick.label.description" /><@s.text name='semicolon'/>
			</div> 
		</td>
		<td align="right">
			<div id="textContainer">		
				<@s.textfield tabindex="2" 
    			   name="pick.item.description" 
    			   id="abc" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="true" 
                   theme="css_nonbreaking"/>
            </div>
		</td>
	</tr>
	<tr height="24px">
		<td>
			<div id="textContainer" style="font-weight: bold;">
				<@s.text name="pick.label.quantitytopick" /><@s.text name='semicolon'/>
			</div>
		</td>
		<td align="right">
			<div id="textContainer">			   
    			   <@s.textfield tabindex="3" 
    			   name="pick.quantityToPick" 
    			   id="pick.quantityToPick" 
    			   size="25" maxlength="40" 
    			   show="true" 
    			   readonly="true"
    			   theme="css_nonbreaking"/>
		   </div>
 		</td>
    </tr>
    <tr height="24px">
    	<td>
 				<div id="textContainer" style="font-weight: bold;">
					<@s.text name="pick.label.quantitypicked"  /><@s.text name='semicolon'/>
				</div>
	    </td>
    	<td align="right">
    		<div id="textContainer">			   
    			   <@s.textfield tabindex="4" 
    			   name="pick.quantityPicked" 
    			   id="pick.quantityPicked" 
    			   size="25" maxlength="40" 
    			   show="true" 
    			   readonly="true"
    			   theme="css_nonbreaking"/>
		   </div>
		</td>
	</tr>
	<tr height="24px">
		<td>
			<div id="textContainer" style="font-weight: bold;">
			<@s.text name="pick.label.manuallypick" /><@s.text name='semicolon'/>
			</div>
		</td> 
		<td align="right">
			<div id="textContainer">
    			   <@s.textfield tabindex="4" 
    			   name="quantityEntered" 
    			   id="quantityEntered" 
    			   size="6" maxlength="40" 
    			   show="true" 
    			   readonly="false"
    			   theme="css_nonbreaking"/>
		   </div>
		</td>
	</tr>
	</table>    		  		
    	<@s.hidden name="pickQuant" id="topickQuantity" value=pick.quantityToPick/>
    	<@s.hidden name="pickQuant" id="pickedQuantity" value=pick.quantityPicked/>    	    		  
</div>