<title><@s.text name='container.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	

	<#assign extraParamsHash = {}>
 	
    <#assign urlMethod="getContainerData"/> 
 	<@tablecomponent
 		varName="containerObj"
		columns=containerColumns
		tableId="listContainersTable"
		url="${base}/selection/container"
		tableTitle='container.view.title' 
		viewId=containerViewId
    	type=""
    	extraParams=extraParamsHash?default("") />

 	<#assign urlMethod="getPickDetailData"/> 
 	<@tablecomponent
 		varName="pickDetailObj"
		columns=pickDetailColumns
		tableId="listPickDetailsTable"
		url="${base}/selection/containerPickDetail"
		tableTitle='pickDetail.view.title' 
		viewId=pickDetailViewId
    	type=""
		extraParams={"publishers":[{"name" : "containerObj", "selector": "containerID"}]} />
    	
