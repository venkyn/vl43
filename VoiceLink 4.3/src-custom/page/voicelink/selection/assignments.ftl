<title><@s.text name='assignment.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
  	<script type="text/javascript"> 	
  		 function getFilter(viewId, columnId, val, operand) {
        	return "submittedFilterCriterion={'viewId':'"+viewId+"','columnId':'"+columnId+"','operandId':'"+operand+"','value1':'"+val+"','value2':'','locked':false}";
    	 }
  		 function getLockedFilter(viewId, columnId, val, operand) {
        	return "submittedFilterCriterion={'viewId':'"+viewId+"','columnId':'"+columnId+"','operandId':'"+operand+"','value1':'"+val+"','value2':'','locked':true}";
    	 }
		
	<#assign urlMethod="getAssignmentData" />
    </script type="text/javascript">
 	<@tablecomponent
 		varName="assignmentObj"
		columns=assignmentColumns
		tableId="listAssignmentsTable"
		url="${base}/selection/assignment"
		tableTitle='assignment.view.title' 
    	viewId=assignmentViewId
    	type=""
    	extraParams=extraParamsHash?default("") />

	<#assign urlMethod="getPickData"/>
 	<@tablecomponent
 		varName="pickObj"
		columns=pickColumns
		tableId="listPicksTable"
		url="${base}/selection/assignmentPick"
		tableTitle='pick.view.title' 
        viewId=pickViewId
    	type=""
    	extraParams={"publishers":[{"name" : "assignmentObj", "selector": "assignmentID"}]} />

    <script type="text/javascript">
                
   	    if( $("menuBlade_listAssignmentsTable") != null ) {
	   	    //set up and register handlers
	   	    assignmentObj.menuBladeHeight = $("menuBlade_listAssignmentsTable").clientHeight;
	   	    pickObj.menuBladeHeight = $("menuBlade_listPicksTable").clientHeight;
	   	    
	   	    function openAssignmentBlade(evt) {
	   	    	showActionsMenu(assignmentObj);
	   	    	hideActionsMenu(pickObj);
	   	    }
	   	    
	   	    function openPickBlade(evt) {
	   	    	showActionsMenu(pickObj);
	   	    	hideActionsMenu(assignmentObj);
	   	    }
   	    
   	    	//assignmentObj.registerHandlers(openAssignmentBlade, null);
   	    	//pickObj.registerHandlers(openPickBlade, null); 
   	    }
    </script type="text/javascript">
    
    
