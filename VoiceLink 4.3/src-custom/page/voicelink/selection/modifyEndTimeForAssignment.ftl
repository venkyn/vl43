<div id="textContainer">
<@s.text name="labor.end.time.message" >
	<@s.param>${operatorLabor.actionType}</@s.param>
	<@s.param>${operatorLabor.operator.operatorIdentifier}</@s.param>
</@s.text>
</div>
<@s.component template="prelabel" id="preLabel" label="%{getText('labor.editEndtime.label.date')}" theme="css_nonbreaking"/>

<div id="wwctrl_time" class="wwctrl"> 
			<@s.textfield 
				tabindex="1" 
				name="assignmentHours" 
				style="float: right" 
				id="assignmentHours" 
				size="1" 
				maxlength="2" 
				theme="css_nonbreaking" /><@s.text name='semicolon'/>
			<@s.textfield 
				tabindex="2" 
				name="assignmentMinutes" 
				id="assignmentMinutes" 
				grpId="assignmentHours" 
				size="1" 
				maxlength="2"  
				theme="css_nonbreaking" /><@s.text name='semicolon'/>
			<@s.textfield 
				tabindex="3" 
				name="assignmentSeconds" 
				id="assignmentSeconds" 
				grpId="assignmentHours" 
				size="1" 
				maxlength="2"  
				theme="css_nonbreaking" />&nbsp;
			<@s.select 
				tabindex="4" 
				name="hourType" 
				id="hourType" 
				list="hourTypes"  
				theme="css_nonbreaking" 
				class="inputElement"/>&nbsp;${timeZone}	
		    <div style="height:5px;"></div>
		    <@s.textfield
		    	tabIndex="5"
		    	name=""
		    	id="dateTextField"
		    	maxlength="20"
		    	theme="css_nonbreaking"
		    	 />
		    <span class="button-group">
		        <button type="button" onClick="javascript:toggleCalendar();">Calendar</button>
		    </span>
		    <@s.hidden id="hiddenDateField" name="day" />
		    <@s.hidden id="timeZoneOffset" name="timeZoneOffset" />
</div>


