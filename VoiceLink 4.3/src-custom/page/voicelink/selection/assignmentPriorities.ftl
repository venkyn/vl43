<#include "/include/common/customdialogdate.ftl">
<style>
	#boldDiv {font-weight: bold;}
</style>
<style>
	#boldDivLarge {font-weight: bold; font-size: 14px;}
</style>

<#assign formName="prioritize">
<#assign submitValue>
	<@s.text name='assignment.prioritize.button.submit'/>
</#assign>	
<#assign title>
	<@s.text name='assignment.prioritize.title'/>
</#assign>	
<#assign addAnotherMapping>
	<@s.text name="assignment.prioritize.button.addAnotherMapping"/>
</#assign>
<#assign remove>
     <@s.text name="assignment.prioritize.button.remove"/>
</#assign>
<title>${title}</title>
<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/selection/assignment" action="${formName}.action" method="GET">
<@s.hidden name="numberOfMappings" id="numberOfMappings" />
<@s.hidden name="defaultPriority" id="defaultPriority" />

<script language="javascript">

	var count = ${numberOfMappings};

<#-- method used to copy select boxes -->
	function copyElement(e) {
    	var eId      = $(e);
		var copyE    = eId.cloneNode(true);
		var cLength  = copyE.childNodes.length -1;

	    copyE.id     = e+'-copy';

    	for(var i = 0; cLength >= i;  i++)	{
	    	if(copyE.childNodes[i].id) {
		    	var cNode   = copyE.childNodes[i];
			    var firstId = cNode.id;
			    cNode.id    = firstId+'-copy'; 
			}
		}
   		return copyE;
	}
	
<#-- Add the Add Another mapping Button -->
	function addAddButton() 
	{
		var linkAdd = A({'id':'mappingAddButton','class':'anchorButton', 'href':'javascript:addMappingRule()'},'${addAnotherMapping}');
		var rows = [
		   ['', '', linkAdd]
		];		
		row_display = function (row) {
		    return TR({'id':'mappingRowAddButton','height':'25'}, map(partial(TD, {'valign':'middle','class':'td'}), row));
		}			
		var addButtonRow = map(row_display, rows);
		var mappingTable = document.getElementById('mappingTable');	
	    appendChildNodes(mappingTable,  TBODY(null,addButtonRow));
     
	}
	
<#-- Remove the Add Another mapping Button -->
	function removeAddButton()
	{
		var linkAdd = document.getElementById('mappingRowAddButton');
		if(linkAdd != null) { 
			removeElement(linkAdd);
		}
	}
	
<#-- Add A new mapping -->
	function addMappingRule()
	{
		removeAddButton();
		
		//Determine which apply by list box to add
		var selApplyVal;
		if ($("howToApply").value == 0) {
			selApplyVal = copyElement("listA");
		} else {
			selApplyVal = copyElement("listB");
		}
		selApplyVal.setAttribute('id', 'mappings[' + count + '].applyTo');
		selApplyVal.setAttribute('name', 'mappings[' + count + '].applyTo');
		
		//Add new priority
		var selPriority = copyElement("priority");
		selPriority.setAttribute('id', 'mappings[' + count + '].priority');
		selPriority.setAttribute('name', 'mappings[' + count + '].priority');
		selPriority.setAttribute('value', '5');

		var linkRemove = A({'class':'anchorButton','href':'javascript:removeMappingRule(' + count + ')'}, '${remove}');
		
		var rows = [
		    [selApplyVal, selPriority, linkRemove]
		];		
		row_display = function (row) {
		    return TR({'height':'25','id':'mappingRow' + count}, map(partial(TD, {'valign':'middle','class':'td'}), row));
		}		
		var container = document.getElementById('mappingContainer');
		var mappingTable = document.getElementById('mappingTable');
		
		var mappingRow = map(row_display, rows);
		appendChildNodes(mappingTable, TBODY(null,mappingRow));

		var numberOfMappings = document.getElementById('numberOfMappings');
		
		addAddButton();
		count++;			
		numberOfMappings.value = count;		
	}
	
<#-- Remove a mapping -->
	function removeMappingRule(id)
	{
	    if (count > 1) {
	    
	    	//Move all values of following rows up 1 row starting with row to delete
            for (i = id+1; i < count; i++ ) {
            	var applyValue1 = document.getElementById('mappings[' + (i-1) + '].applyTo');
            	var applyValue2 = document.getElementById('mappings[' + (i) + '].applyTo');
            	var priority1 = document.getElementById('mappings[' + (i-1) + '].priority');
            	var priority2 = document.getElementById('mappings[' + (i) + '].priority');
            	
            	applyValue1.value = applyValue2.value
            	priority1.value = priority2.value
            }
	    	
	    	//always delete the last row
			count--;
			var mappingRow = document.getElementById('mappingRow' + count);
			removeElement(mappingRow);

			var numberOfMappings = document.getElementById('numberOfMappings');
			numberOfMappings.value = count;		
		}
	}
	
	function clearMappings()
	{
        for (i = 0; i < count; i++ ) {
			var mappingRow = document.getElementById('mappingRow' + i);
			removeElement(mappingRow);
		}
		count = 0;
		addMappingRule();
	}

	function changeDateLabel() 
	{
		var calRef;
		var myDate;
		
		calRef = document.getElementById('calendarLinkStart');
		var startDate = document.getElementById('startDate');
		myDate = convertToDate(startDate.value);
		calRef.innerHTML=myDate.toLocaleDateString();
		
		calRef = document.getElementById('calendarLinkEnd');
		var endDate = document.getElementById('endDate');
		myDate = convertToDate(endDate.value);
		calRef.innerHTML=myDate.toLocaleDateString();
	}
	
	function convertToDate(dateString) 
	{
		var parts = dateString.split("-");
		return new Date(parts[0], parts[1]-1, parts[2]);
	}
	
</script>

<#-- The following 3 list boxes are only used to 
	 copy when a new mapping row is created -->
<div style="visibility: hidden;">
	<@s.select
	   label=""
	   name="listA" 
	   id="listA" 
	   list="applyValue0" 
	   cssClass="prioritySelect"  
	   theme="simple"
	   multiple="false"/>
	
	<@s.select
	   label=""
	   name="listB" 
	   id="listB" 
	   list="applyValue1" 
	   cssClass="prioritySelect"  
	   theme="simple"
	   multiple="false"/>

	<@s.select
	   label=""
	   name="priority" 
	   id="priority" 
	   list="availablePriorities" 
       cssClass="statusSelect" 
	   theme="simple"
	   value="${defaultPriority}"
	   multiple="false"/>

    <@s.textfield tabindex="1" 
		name="startDate" 
		id="startDate" 
		size="15" maxlength="50" 
		show="true" 
		theme="simple"
		onchange="javascript:changeDateLabel();"
		/>	

   <@s.textfield tabindex="2" 
	   	name="endDate" 
	   	id="endDate" 
	   	size="15" maxlength="50" 
	   	show="true" 
	   	theme="simple"
		onchange="javascript:changeDateLabel();"
	   />	
</div>

<div class="formTop">
	<#-- TODO: Change controls to date controls -->
	<div id="boldDivLarge">
		<@s.text name="assignment.prioritize.label.dates"/>						
		&nbsp;

	    <@customdialogdate
	      dialogId="priority_date_start"
	      header="report.selectdate"
	      dateInputFieldId="startDate"
	      callingButtonId="calendarLinkStart"
	      multiPage="false"/>
        <a id="calendarLinkStart" href="#">Start Date</a>
		&nbsp;

		<@s.text name="assignment.prioritize.label.datesAnd" />						
		&nbsp;

        <@customdialogdate
	      dialogId="priority_date_end"
	      header="report.selectdate"
	      dateInputFieldId="endDate"
	      callingButtonId="calendarLinkEnd"
	      multiPage="false"/>
        <a id="calendarLinkEnd" href="#">End Date</a>
	</div>

	<div class="listErrorMessage">
		<#if fieldErrors['dateError']?exists>
			<br>
			<#assign loopCount = 0>
			<#list fieldErrors['dateError'] as error ><#if 0 < loopCount  >, </#if>			
			${error}<#assign loopCount = (loopCount + 1)></#list>		
		</#if>
	</div>
</div>

<div class="formMiddle">
<br>
<div id="boldDiv">
	<@s.text name="%{getText('assignment.prioritize.label.applyBy')}" />						
</div>
<table id="mappingTable"  >
	<tr height="25">
		<td class="labelRelative">
  			<@s.select
  			   label=""
  			   labelposition="left"
			   name="howToApply" 
			   id="howToApply" 
  			   list="applyBy" 
	  	       cssClass="prioritySelect"  
			   theme="simple"
			   onchange="javascript:clearMappings();"
			   multiple="false"/>
 			    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		<td class="labelRelative">
			<br>
			<@s.text name="%{getText('assignment.label.priority')}" />						
		</td>
	</tr>

	<#if numberOfMappings != 0>	
		<#list 0..numberOfMappings - 1 as x>
			<tr id="mappingRow${x}" height="25">
				<td>
					<#if howToApply == 0>
					<@s.select
					   name="mappings[${x}].applyTo"  id="mappings[${x}].applyTo" 
					   list="applyValue0"  cssClass="prioritySelect"  
					   theme="simple" multiple="false"/>
					</#if>
					<#if howToApply == 1>
					<@s.select
					   name="mappings[${x}].applyTo"  id="mappings[${x}].applyTo" 
					   list="applyValue1" cssClass="prioritySelect"  
					   theme="simple" multiple="false"/>
					</#if>
		    	</td>
		    	<td>
					<@s.select
					   name="mappings[${x}].priority" id="mappings[${x}].priority" 
					   list="availablePriorities" cssClass="statusSelect" 
					   theme="simple"  multiple="false"/>
		    	</td>
				<td style="width:200";>
					<a class="anchorButton" href="javascript:removeMappingRule(${x})">${remove}</a>
				</td>
				<td class="listErrorMessage">
					<#if fieldErrors['priorityError[${x}]']?exists>
					<#assign loopCount = 0>
					<#list fieldErrors['priorityError[${x}]'] as error ><#if 0 < loopCount  >, </#if>			
					${error}<#assign loopCount = (loopCount + 1)></#list>		
					</#if>
				</td>
			</tr>
		</#list>
	</#if>
</table>

</div>
<div class="formAction">
</div>
<#include "/include/form/buttonbar.ftl">

<script for="window" event="onload" language="javascript">
	var numberOfMappings = document.getElementById('numberOfMappings');
	addAddButton();
	changeDateLabel();
	if (numberOfMappings.value == 0) {
		clearMappings();
	}
</script>

</@s.form>

