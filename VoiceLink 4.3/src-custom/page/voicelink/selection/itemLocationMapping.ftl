
<title><@s.text name='itemLocationMapping.view.title'/></title>
<#include "/include/common/tablecomponent.ftl">	

 	<@tablecomponent
 		varName="itemLocationMappingObj"
		columns=itemLocationMappingColumns
		tableId="listItemLocationMappingsTable"
		url="${base}/selection/itemLocationMapping"
		tableTitle='itemLocationMapping.view.title' 
		viewId=itemLocationMappingViewId
    	type=""
    	extraParams=extraParamsHash?default("") />