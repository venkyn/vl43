
<title><@s.text name='selection.reports.view.title'/></title>
<#include "/include/common/tablecomponent.ftl">	


 	<@tablecomponent
 		varName="selectionReportObj"
		columns=selectionReportColumns
		tableId="listSelectionReportsTable"
		url="${base}/selection/report"
		tableTitle='selection.reports.view.title' 
		viewId=viewId
    	type=""
    	extraParams=extraParamsHash?default("") />
