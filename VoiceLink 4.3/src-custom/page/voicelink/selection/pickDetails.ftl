<title><@s.text name='pickDetail.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">
	<#assign pageContext="${base}/${navigation.applicationMenu}">	

    <#assign urlMethod="getPickDetailData"/>
 	<@tablecomponent
 		varName="pickDetailObj"
		columns=pickDetailColumns
		tableId="listPickDetailsTable"
		url="${pageContext}/assignment/pickDetail"
		tableTitle='pickDetail.view.title' 
		viewId=pickDetail_View_Id
    	type=""
    	extraParams={"extraURLParams":[{"name":"assignmentId","value":"${assignmentId?c}"},{"name":"pickId","value":"${pickId?c}"}]} />        
