<div id="textContainer">
<@s.text name="pick.dialogbox.assignOperatorsText" />
</div>
<br>
<div id="operator" class="wwctrlopr">
<table border="0" class="content" width="310px"> 
<tr>
<td>
<div id="textContainer" style="font-weight: bold;"><@s.text name="assignment.label.oldOperator" theme="css_nonbreaking"/><@s.text name='semicolon'/></div> 
</td>
<td align="right">
<@s.select tabindex="1" 
    			   name="oldOperatorsInRegion" 
    			   id="oldOperatorsInRegion" 
    			   list="AllOperatorsInRegion"
    			   readonly="false"
			       theme="css_nonbreaking"
			       class="inputElement"/>
</td>
<td>&nbsp;&nbsp;</td>
</tr>
<tr><td colspan='3'>&nbsp;</td></tr>
<tr>
<td>
<div id="textContainer" style="font-weight: bold;"><@s.text name="assignment.label.newOperator" theme="css_nonbreaking"/><@s.text name='semicolon'/></div>
</td>
<td align="right">
<@s.select tabindex="2"
				 	name="newOperatorsInRegion" 
				 	id="newOperatorsInRegion" 
				 	list="AllOperatorsInRegion"
				 	readonly="false"
			        theme="css_nonbreaking" 
			        class="inputElement" />
</td>
<td>&nbsp;&nbsp;</td>
</tr>
</table>
</div>
<div id="operator" class="wwctrl">
