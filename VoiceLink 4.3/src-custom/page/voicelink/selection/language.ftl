<table border="0" class="content" width="220px"> 
<tr>
<td colspan="2">
<div id="textContainer">
   <@s.text name="summaryPrompt.language.body.selectText" /><@s.text name='semicolon'/>
</div> 
</td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
<td>
<div id="textLabel" style="font-weight: bold;">
<@s.text name="entity.language" />&nbsp;<@s.text name='semicolon'/>&nbsp;
</div>
</td>
<td align="right">
<div id="languageSelect">
<@s.select tabindex="1"
            title=""
            name="language" 
	        id="language" 
	        list="localesForSummaryPrompt" 
	        cssClass="languageSelect"  
	        listValue="value" 
	        listKey="key" 
	        value="key" 
	        readonly="false" 
	        theme="css_nonbreaking"
	        class="inputElement"/>
	  
</div>
</td>
</tr>
</table>