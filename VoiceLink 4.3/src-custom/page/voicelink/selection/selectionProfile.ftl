<#assign readOnly="false">
<#if navigation.pageName="edit">
 <#assign title>
    <@s.text name='select.region.normal.profile.edit.title'/>
  </#assign>
  <#assign formName="edit">
<#else>  
<#if selectionNormalProfileId?has_content>
   <#assign title>
    <@s.text name='select.region.normal.profile.view.title'/>
 </#assign>
  <#elseif selectionChaseProfileId?has_content>
   <#assign title>
    <@s.text name='select.region.chase.profile.view.title'/>
  </#assign>
 </#if>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>
<#include "/include/common/columnformatting.ftl" />
<#include "/include/common/controlgrouping.ftl" />
 <html>
<head>
 <title>${title}</title>
</head>
<body >

<div class="formBody">
<div class="titleBar">
	${title}
</div>
<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/selection/region" action="${formName}.action" method="POST">
    <div class="formMiddle">
    <BR>
    <@columnlayout header="">
    <div  class="yui-u first">

<#if selectionNormalProfileId?has_content>

        <@s.textfield 
            tabindex="1" 
            label="%{getText('region.create.label.name')}" 
            name="normalProfileName" 
            value="%{getText('profile.id.${selectionRegion.profileNormalAssignment.number}')}" 
            id="name" 
            size="25" 
            maxlength="128" 
            required="true" 
            readonly="${readOnly}"/>
            
  <#elseif selectionChaseProfileId?has_content>

        <@s.textfield 
            tabindex="1" 
            label="%{getText('region.create.label.name')}" 
            name="chaseProfileName" 
            value="%{getText('profile.id.${selectionRegion.profileChaseAssignment.number}')}" 
            id="name" 
            size="25" 
            maxlength="128" 
            required="true" 
            readonly="${readOnly}"/>
 </#if>
    
    

        <#if selectionRegionProfile.autoIssuance>
    	    <#assign promptType>
                <@s.text name='region.create.label.assignment.issuance.automatic'/>
            </#assign> 
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.assignment.issuance.manual'/>
            </#assign>
        </#if>
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.assignment.issuance')}" 
            name="selectionRegionProfile.autoIssuance" 
            id="selectionRegionProfile.autoIssuance"
            value="${promptType}"/>
    
        <#if selectionRegionProfile.allowMultipleAssignments>
    	    <#assign promptType>
                <@s.text name='region.create.label.allow.multiple.assignments.yes'/>
            </#assign> 
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.allow.multiple.assignments.no'/>
            </#assign>
        </#if>               
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.allow.multiple.assignments')}" 
            name="selectionRegionProfile.allowMultipleAssignments" 
            id="selectionRegionProfile.allowMultipleAssignments"
            value="${promptType}"/>            

        <#if selectionRegionProfile.allowPassingAssignment>
    	    <#assign promptType>
                <@s.text name='region.create.label.allow.pass.assignments.yes'/>
            </#assign> 
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.allow.pass.assignments.no'/>
            </#assign>
        </#if>                           
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.allow.pass.assignments')}" 
            name="selectionRegionProfile.allowPassingAssignment" 
            id="selectionRegionProfile.allowPassingAssignment"
            value="${promptType}"/>                        

        <#if selectionRegionProfile.allowBaseItems>
    	    <#assign promptType>
                <@s.text name='region.create.label.allow.base.items.yes'/>
            </#assign> 
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.allow.base.items.no'/>
            </#assign>
        </#if>             
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.allow.base.items')}" 
            name="selectionRegionProfile.allowBaseItems" 
            id="selectionRegionProfile.allowBaseItems"
            value="${promptType}"/>                                    

        <#if selectionRegionProfile.requireCaseLabelCheckDigits>
    	    <#assign promptType>
                <@s.text name='region.create.label.case.label.check.digits.yes'/>
            </#assign> 
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.case.label.check.digits.no'/>
            </#assign>
        </#if>                         
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.case.label.check.digits')}" 
            name="selectionRegionProfile.requireCaseLabelCheckDigits" 
            id="selectionRegionProfile.requireCaseLabelCheckDigits"
            value="${promptType}"/>                                                
                                             
        <@controlgroup groupId="pickpromptTop" caption="region.create.caption.pickpromptTop" disabled="false">

        <#if selectionRegionProfile.multiplePickPrompt>
    	    <#assign promptType>
                <@s.text name='region.create.label.prompt.type.multiple'/>          
            </#assign>
                
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.prompt.type.single'/>
            </#assign>
        </#if>   
                 
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.prompt.type')}" 
            name="selectionRegionProfile.promptType" 
            id="selectionRegionProfile.promptType"
            value="${promptType}" />

        <#if selectionRegionProfile.requireQuantityVerification>
    	    <#assign promptType>
                <@s.text name='region.create.label.quantity.verification.yes'/>          
            </#assign>
                
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.quantity.verification.no'/>
            </#assign>
        </#if>
    
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.quantity.verification')}" 
            name="selectionRegionProfile.requireQuantityVerification" 
            id="selectionRegionProfile.requireQuantityVerification"
            value="${promptType}" />
            
        </@controlgroup>
     </div>
     <div class="yui-u">

        <#if selectionRegionProfile.requireDeliveryPrompt>
    	    <#assign promptType>
                <@s.text name='region.create.label.allow.delivery.yes'/>          
            </#assign>
                
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.allow.delivery.no'/>
            </#assign>
        </#if>
        
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.allow.delivery')}" 
            name="selectionRegionProfile.requireDeliveryPrompt" 
            id="selectionRegionProfile.requireDeliveryPrompt"
            value="${promptType}" />

        <#if selectionRegionProfile.printContainerLabelsIndicator="DoNotPrint">
    	    <#assign promptType>
                <@s.text name='region.create.label.print.labels.doNotPrint'/>          
            </#assign>
                
        <#elseif selectionRegionProfile.printContainerLabelsIndicator="WhenContainerOpened">
            <#assign promptType>           
                <@s.text name='region.create.label.print.labels.whenContainerOpened'/>
            </#assign>
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.print.labels.whenContainerClosed'/>
            </#assign>
        </#if>

        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.print.labels')}" 
            name="selectionRegionProfile.printContainerLabelsIndicator" 
            id="selectionRegionProfile.printContainerLabelsIndicator"
            value="${promptType}" />
            
         <#if selectionRegionProfile.containerType?has_content>
	          <#assign groupDisabled="false">
	     <#else>
	          <#assign groupDisabled="true">
	          <#assign promptType="---"/>
	     </#if>	
        
  		<@controlgroup groupId="containersTop" caption="region.create.caption.containersTop" disabled="${groupDisabled}"> 

        <#if selectionRegionProfile.containerType?has_content>
            <#if selectionRegionProfile.containerType.allowPreCreationOfContainers>
        	    <#assign promptType>
                    <@s.text name='region.create.label.precreate.containers.yes'/>          
                </#assign>
                
            <#else>
                <#assign promptType>           
                    <@s.text name='region.create.label.precreate.containers.no'/>
                </#assign>
            </#if>
        </#if>
  		 
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.precreate.containers')}" 
            name="selectionRegionProfile.containerType.allowPreCreationOfContainers" 
            id="selectionRegionProfile.containerType.allowPreCreationOfContainers"
            disabled="${groupDisabled}"
            value="${promptType}" />

        <#if selectionRegionProfile.containerType?has_content>
            <#if selectionRegionProfile.containerType.promptOperatorForContainerID>
        	    <#assign promptType>
                    <@s.text name='region.create.label.prompt.id.yes'/>          
                </#assign>
                
            <#else>
                <#assign promptType>           
                    <@s.text name='region.create.label.prompt.id.no'/>
                </#assign>
            </#if>
        </#if>
            
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.prompt.id')}" 
            name="selectionRegionProfile.containerType.promptOperatorForContainerID" 
            id="selectionRegionProfile.containerType.promptOperatorForContainerID"
            disabled="${groupDisabled}"
            value="${promptType}" />            
            
        <#if selectionRegionProfile.containerType?has_content>
            <#if selectionRegionProfile.containerType.requireDeliveryAfterClosed>
        	    <#assign promptType>
                    <@s.text name='region.create.label.prompt.delivery.on.container.close.yes'/>          
                </#assign>
                
            <#else>
                <#assign promptType>           
                    <@s.text name='region.create.label.prompt.delivery.on.container.close.no'/>
                </#assign>
            </#if>
        </#if>

        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.prompt.delivery.on.container.close')}" 
            name="selectionRegionProfile.containerType.requireDeliveryAfterClosed" 
            id="selectionRegionProfile.containerType.requireDeliveryAfterClosed"
            disabled="${groupDisabled}"
            value="${promptType}" />            

        <#if selectionRegionProfile.containerType?has_content>
            <#if selectionRegionProfile.containerType.allowMultipleOpenContainers>
        	    <#assign promptType>
                    <@s.text name='region.create.label.allow.multiple.open.yes'/>          
                </#assign>
                
            <#else>
                <#assign promptType>           
                    <@s.text name='region.create.label.allow.multiple.open.no'/>
                </#assign>
            </#if>
        </#if>
            
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.allow.multiple.open')}" 
            name="selectionRegionProfile.containerType.allowMultipleOpenContainers" 
            id="selectionRegionProfile.containerType.allowMultipleOpenContainers"
            disabled="${groupDisabled}"
            value="${promptType}" />            

        <#if selectionRegionProfile.containerType?has_content>
            <#if selectionRegionProfile.containerType.requireTargetContainers>
        	    <#assign promptType>
                    <@s.text name='region.create.label.requires.target.containers.yes'/>          
                </#assign>
                
            <#else>
                <#assign promptType>           
                    <@s.text name='region.create.label.requires.target.containers.no'/>
                </#assign>
            </#if>
        </#if>
            
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.requires.target.containers')}" 
            name="selectionRegionProfile.containerType.requireTargetContainers" 
            id="selectionRegionProfile.containerType.requireTargetContainers"
            disabled="${groupDisabled}"
            value="${promptType}" />            
            
        </@controlgroup>
    </div>
    </@columnlayout>
    </div>

</@s.form>