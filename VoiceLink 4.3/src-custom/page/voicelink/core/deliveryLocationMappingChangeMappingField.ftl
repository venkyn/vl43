<#assign pageContext="${base}/${navigation.applicationMenu}">

<#assign formName="changeMappingField">
  
<#assign saveValue>
   <@s.text name="deliveryLocationMapping.form.button.submit"/>
</#assign>
<#assign cancelValue>
   <@s.text name="form.button.cancel"/>
</#assign>
<html>
<head>
<title><@s.text name='deliveryLocationMapping.changeMappingField.title'/></title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	<@s.text name='deliveryLocationMapping.changeMappingField.title'/>
</div>

<@s.form name="${formName}" id="form1" validate="false" namespaceVal="${pageContext}/deliveryLocationMapping" action="${formName}.action" method="GET">
<input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>
<div class="formTop">
<br>
<@s.text name="deliveryLocationMapping.changeMappingField.label.header" />

<@s.select 
	tabindex="1"
	label="%{getText('deliveryLocationMapping.changeMappingField.label.mappingField')}" 
	name="deliveryLocationMappingTypeId" 
	id="deliveryLocationMappingTypeId" 
	list="deliveryLocationMappingTypesMap" 

/>
</div>
<div class="formAction">

    <a
	href="javascript:saveForm();"
	id="${formName}.save"
	class="${cancelCssClass?default('anchorButton')}"
	tabindex="199"
	>${saveValue}</a>
	
    <a
	href="javascript:modifySubmitVariable('form1','submitTypeElement','method:cancel');"
	id="${formName}.cancel"
	class="${cancelCssClass?default('anchorButton')}"
	tabindex="200"
	>${cancelValue}</a>	
	
</div>
<#-- Include a modified data div if there is modified data to display -->

</@s.form>
</div>
<script type="text/JavaScript">
    
    function saveForm() {
    		var formObject = document.forms['${formName}'];
 			$(formObject).submit();
	
    }  

</script>

</body>
</html>