<title><@s.text name='print.view.title'/></title>
<#include "/include/common/tablecomponent.ftl">
<#assign pageContext="${base}/${navigation.applicationMenu}">

 	<@tablecomponent
 		varName="printerObj"
		columns=printerColumns
		tableId="printerTable"
		url="${pageContext}/print"
		tableTitle='print.view.title' 
		viewId=PrintLabelViewId
    	type=""
    	extraParams=extraParamsHash?default("") />
	