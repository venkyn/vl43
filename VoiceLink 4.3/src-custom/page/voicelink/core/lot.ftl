<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='lot.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='lot.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='lot.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='lot.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='lot.view.single.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/lot" action="${formName}.action" method="POST">
<#if lotId?has_content>
    <@s.hidden name="lotId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@s.textfield tabindex="1" 
        label="%{getText('lot.label.number')}" 
        name="lot.number" 
        id="lot.number" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>
	
	<@s.textfield tabindex="2" 
	   	label="%{getText('lot.label.speakableNumber')}" 
	   	name="lot.speakableNumber" 
	   	id="lot.speakableNumber" 
	   	size="25" 
	   	maxlength="50" 
	   	show="true" 
	   	required="true" 
	   	readonly="${readOnly}"/>
	   		
	<@s.textfield tabindex="4" 
	    label="%{getText('lot.label.location')}" 
	    name="lot.location.scannedVerification" 
	    id="lot.location.scannedVerification" 
	    size="25" 
	    maxlength="50" 
	    show="true" 
	    readonly="${readOnly}"/>	    	

	<@s.textfield tabindex="5" 
	    label="%{getText('lot.label.itemNumber')}" 
	    name="lot.item.number" 
	    id="lot.item.number" 
	    size="25" 
	    maxlength="50" 
	    show="true" 
	    required="true" 
	    readonly="${readOnly}"/>
	    
	<@s.textfield tabindex="6" 
	    label="%{getText('lot.label.expirationDate')}" 
	    name="lot.expirationDate" 
	    id="lot.expirationDate" 
	    size="25" 
	    maxlength="50" 
	    show="true" 
	    readonly="${readOnly}"/>   
		    
	
</div>
<div class="formAction">
	
	<#if lotId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='lot.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('lot.create.label.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('lot.create.label.name')}" name="modifiedEntity.name"/>
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>

</body>
</html>

