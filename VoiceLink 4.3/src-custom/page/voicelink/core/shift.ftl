<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='shift.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='shift.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='shift.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='shift.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='shift.view.title.readOnly'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
    ${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/shift" action="${formName}.action" method="POST">
<#if shiftId?has_content>
    <@s.hidden name="shiftId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@s.textfield tabindex="1" 
       label="%{getText('shift.label.name')}" 
       name="shift.name" 
       id="shift.name" 
       size="15" 
       maxlength="50" 
       show="true" 
       readonly="${readOnly}" 
       required="true"/>  
    <@s.textfield tabindex="2" 
       label="%{getText('shift.label.startTime')}" 
       name="startTime" 
       id="startTime"
       size="5" 
       maxlength="7" 
       show="true" 
       readonly="${readOnly}" 
       required="true"/>  
   <@s.textfield tabindex="3" 
       label="%{getText('shift.label.endTime')}" 
       name="endTime" 
       id="endTime"
       size="5" 
       maxlength="7" 
       show="true" 
       readonly="${readOnly}" 
       required="true"/>        
</div>

<div class="formAction">

   <#if shiftId?has_content>
       <#if !actionErrors?has_content>
           <#assign submitOnChangeForm="form1">
       </#if>
   </#if>

   <#assign submitIndex=199>
   <#assign cancelIndex=200>

   <#include "/include/form/buttonbar.ftl">
</div>

<#-- Include a modified data div if there is modified data to display -->

<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
   <div class="modifiedTitle">
      <@s.text name='shift.edit.modifiedEntity.title'/>
   </div>
   
   <div class="formTop">
      <@s.label label="%{getText('shift.label.name')}" name="modifiedEntity.name"/>
      <@s.label label="%{getText('shift.label.startTime')}" name="modifiedEntity.startTime" />
      <@s.label label="%{getText('shift.label.endTime')}" name="modifiedEntity.endTime" />
   </div>
</div>

<script language="javascript">
   YAHOO.namespace("example.resize");

   function init() {

   YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
   {
     width:"450",
     left: 300,
     top: 200,
     constraintoviewport: true,
     fixedcenter: true,
     underlay:"shadow",
     close:true,
     visible:false,
     draggable:true,
     modal:false } );
   YAHOO.example.resize.panel.render();
   }

   YAHOO.util.Event.addListener(window, "load", init);
</script>

</#if>
</@s.form>
</div>


</body>
</html>

