<#assign pageContext="${base}/${navigation.applicationMenu}">
<#assign formName="create">
<#assign submitValue>
	<@s.text name='itemLocationMapping.create.button.submit'/>
</#assign>	
<#assign title>
	<@s.text name='itemLocationMapping.create.title'/>
</#assign>	
<#assign saveValue>
	<@s.text name="form.button.submit"/>
</#assign>
<#assign cancelValue>
	<@s.text name="form.button.cancel"/>
</#assign>
<#assign addAnotherMapping>
	<@s.text name="deliveryLocationMapping.create.button.addAnotherMapping"/>
</#assign>
<#assign remove>
     <@s.text name="deliveryLocationMapping.create.button.remove"/>
</#assign>
<title>${title}</title>
<@s.form name="${formName}" id="form1" validate="true" namespaceVal="${pageContext}/locItem" action="${formName}.action" method="GET">
<@s.hidden name="numberOfMappings" id="numberOfMappings" />

<#assign boxWidth=35 />
<#assign maxLength=50 />
<script language="javascript">
	var count = ${numberOfMappings};
	
	function addAddButton() 
	{
		var linkAdd = A({'id':'mappingAddButton','class':'anchorButton', 'href':'javascript:addMappingRule()'},'${addAnotherMapping}');
		var rows = [
		    ['','', linkAdd]
		];		
		row_display = function (row) {
		    return TR({'id':'mappingRowAddButton'}, map(partial(TD, {'valign':'middle','class':'td'}), row));
		}			
		var addButtonRow = map(row_display, rows);			
		var mappingTable = document.getElementById('mappingTable');		
		appendChildNodes(mappingTable, TBODY(null,addButtonRow));
	}
	
	function removeAddButton()
	{
		var linkAdd = document.getElementById('mappingRowAddButton');
		if(linkAdd != null) { 
			removeElement(linkAdd);
		}
	}
	
	function addMappingRule()
	{
		removeAddButton();
		var lblLocationId = LABEL({'class':'label'}, "Location ID:");
		var lblItemNumber = LABEL({'class':'label'}, "Item Number:");
		var txtLocationId = INPUT({'id':'locationItems[' + count + '].location.scannedVerification',
		                           'name':'locationItems[' + count + '].location.scannedVerification',
		                           'type':'text',
		                           'size':'${boxWidth}',
		                           'maxlength':'${maxLength}',
                                   'class':'inputElement'});
		var txtItemNumber = INPUT({'id':'locationItems[' + count + '].item.number',
		                           'name':'locationItems[' + count + '].item.number',
		                           'type':'text',
		                           'size':'${boxWidth}',
		                           'maxlength':'${maxLength}',
                                   'class':'inputElement'});
		var linkRemove = A({'class':'anchorButton','href':'javascript:removeMappingRule(' + count + ')'}, '${remove}');
		var rows = [
		    [txtLocationId, txtItemNumber, linkRemove]
		];		
		row_display = function (row) {
		    return TR({'id':'mappingRow' + count}, map(partial(TD, {'valign':'middle','class':'td'}), row));
		}		
		var container = document.getElementById('mappingContainer');
		var mappingTable = document.getElementById('mappingTable');
		
		var mappingRow = map(row_display, rows);
		appendChildNodes(mappingTable, TBODY(null,mappingRow));

		var numberOfMappings = document.getElementById('numberOfMappings');
		
		addAddButton();
		count++;			
		numberOfMappings.value = count;
		
	}
	
	function removeMappingRule(id)
	{
	
	  var mappingRow = document.getElementById('mappingRow' + id);
	    if (count > 1){
		 removeElement(mappingRow);
		 count--;
		 reArrangeIndexes(id);
		 }
	}
	
	function reArrangeIndexes(id) {
	
	var mapVal = "";
	var delLoc = "";
	var counterValue = count;
	var  map_value = new Array();
	var del_loc = new Array();

	 var isNextRowExist = document.getElementById('locationItems[' + (id+1) + '].item.number');
	 var isPrevRowExist = document.getElementById('locationItems[' + (id-1) + '].item.number');

	 if ( ((isNextRowExist != null) && (isPrevRowExist != null )) || (isPrevRowExist == null )  ) {
	        
	        var index = 0;
	         for (i = id+1; i <= count; i++ ) {
	           del_loc[index] = document.getElementById('locationItems[' + i + '].item.number').value;
	           map_value[index] = document.getElementById('locationItems[' + i + '].location.scannedVerification').value;
	           // delete rows..
	           var mappingRow = document.getElementById('mappingRow' + i);
	           removeElement(mappingRow);
	           index = index+1;
	         }
	        
	        // rearranging the indexes (i.e deleting all records and recreating once again )
	        var newInd = 0;
	        for (i = id; i < count; i++){
	          // create new records
	          delLoc = del_loc[newInd];
	          mapVal = map_value[newInd];
	          // create new records.
	          addMappingRuleReCreate(i,mapVal,delLoc);
	          newInd = newInd+1;
	        }

	    }
	    
	    var numberOfMappings = document.getElementById('numberOfMappings');
		numberOfMappings.value = count;
  
	}
	
  function addMappingRuleReCreate(i,mapvalue,delLoc){

	var txtLocation;
	var txtItemNumber;
	removeAddButton();
		
		if (mapvalue.length == 0){
		  
			 txtLocation = INPUT({'id':'locationItems[' + i + '].location.scannedVerification',
                     'name':'locationItems[' + i + '].location.scannedVerification',
                     'type':'text',
                     'size':'${boxWidth}',
                     'maxlength':'${maxLength}',
                     'class':'inputElement'});
		
		}else {
				 txtLocation = INPUT({'id':'locationItems[' + i + '].location.scannedVerification',
		                         'name':'locationItems[' + i + '].location.scannedVerification',
		                         'value':mapvalue,
		                         'type':'text',
		                         'size':'${boxWidth}',
		                         'maxlength':'${maxLength}',
                                 'class':'inputElement'});
		}

       if (delLoc.length == 0){
   		      txtItemNumber = INPUT({'id':'locationItems[' + i + '].item.number',
	                           'name':'locationItems[' + i + '].item.number',
	                           'type':'text',
	                           'size':'${boxWidth}',
	                           'maxlength':'${maxLength}',
                               'class':'inputElement'});
       }else {
          		  txtItemNumber = INPUT({'id':'locationItems[' + i + '].item.number',
	                                 'name':'locationItems[' + i + '].item.number',
	                                 'value':delLoc,
	                                 'type':'text',
	                                 'size':'${boxWidth}',
	                                 'maxlength':'${maxLength}',
                                     'class':'inputElement'});
       }
		                                 
		var linkRemove = A({'class':'anchorButton','href':'javascript:removeMappingRule(' + i + ')'}, '${remove}');
		var rows = [
		    [txtLocation, txtItemNumber, linkRemove]
		];		
		row_display = function (row) {
		    return TR({'id':'mappingRow' + i}, map(partial(TD, {'valign':'middle','class':'td'}), row));
		}		
		var container = document.getElementById('mappingContainer');
		var mappingTable = document.getElementById('mappingTable');
		
		var mappingRow = map(row_display, rows);
		appendChildNodes(mappingTable, TBODY(null,mappingRow));

		addAddButton();
	
	}
</script>
<style type="text/css">
table#mappingTable tr {
    height: 25px;
}
table#mappingTable tr td input {
    margin-right: 10px;
}
</style>
<div class="formTop">
<table id="mappingTable" style="position:relative;">
	<tr>
		<td>
			<@s.label label="%{getText('locationItem.label.locationId')}"/>
		</td>
		<td>
			<@s.label label="%{getText('locationItem.label.itemNumber')}"/>
		</td>
	</tr>
<#list 0..numberOfMappings - 1 as x>
	<tr id="mappingRow${x}">
		<td>
               <@s.textfield tabindex="1" 
    			   label="Customer Number" 
    			   name="locationItems[${x}].location.scannedVerification" 
    			   id="locationItems[${x}].location.scannedVerification" 
    			   size="${boxWidth}" 
    			   maxlength="${maxLength}" 
    			   show="true" 
    			   theme="simple" />	
    	</td>
    	<td>
               <@s.textfield tabindex="1" 
    			   label="Delivery Location" 
    			   name="locationItems[${x}].item.number" 
    			   id="locationItems[${x}].item.number" 
    			   size="${boxWidth}"
    			   maxlength="${maxLength}" 
    			   show="true" 
    			   theme="simple" />	    			   
		</td>
		<td>
			<a class="anchorButton" href="javascript:removeMappingRule(${x})">${remove}</a>
		</td>
		<td class="listErrorMessage">
			<#if fieldErrors['locationItems[${x}]']?exists>
			<#assign loopCount = 0>
			<#list fieldErrors['locationItems[${x}]'] as error ><#if 0 < loopCount  >, </#if>			
			${error}<#assign loopCount = (loopCount + 1)></#list>		
			</#if>
		</td>
	</tr>
</#list>
</table>

</div>
<div class="formAction">
  <#include "/include/form/buttonbar.ftl">
  <script for="window" event="onload" language="javascript">
	var numberOfMappings = document.getElementById('numberOfMappings');	
	addAddButton();
  </script>
</div>
</@s.form>
