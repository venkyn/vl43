<title><@s.text name='itemLocationMapping.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
    <#include "/include/common/tablecomponent.ftl">	
  	<script type="text/javascript"> 
	  	function displayLocId(obj) {
			return A({'locationId': obj.location.scannedVerification, 'href':'${pageContext}/location/view.action?locationId='+obj.location.id}, obj.location.scannedVerification); 
		}
		
    	function displayItemNum(obj){
			return A({'itemId': obj.item.number, 'href':'${pageContext}/item/view.action?itemId='+obj.item.id}, obj.item.number); 			
  		}
    </script>

    <@tablecomponent
 		varName="locationItemObj"
		columns=locationItemColumns
		tableId="listItemLocationMappingsTable"
		url="${pageContext}/locItem"
		tableTitle='itemLocationMapping.view.title' 
		viewId=locationItemViewId
    	type=""
    	extraParams=extraParamsHash?default("") />
    	
    	