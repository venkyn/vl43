<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='unitofmeasure.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='unitofmeasure.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='unitofmeasure.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='unitofmeasure.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='unitofmeasure.view.single.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>
<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/selection/unitOfMeasure" action="${formName}.action" method="POST">
<#if unitOfMeasureId?has_content>
    <@s.hidden name="unitOfMeasureId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>

<div class="formTop">

	               
	
    			<#if readOnly=="true">
	          		<@s.textfield 
	               		tabindex="3" 
	               		label="%{getText('unitofmeasure.create.label.name')}" 
	               		name="unitOfMeasure.name" 
	               		id="unitOfMeasure.name" 
		           		size="25"
		           		maxlength="50" 
		           		show="true" 
		           		required="true"
		           		readonly="${readOnly}"/>	
				<#else>
	          		<@s.textfield 
	               		tabindex="3" 
	               		label="%{getText('unitofmeasure.create.label.name')}" 
	               		name="unitOfMeasure.name" 
	               		id="unitOfMeasure.name" 
		           		size="25"
		           		maxlength="50" 
		           		show="true" 
		           		required="true"
		           		readonly="${readOnly}"/>	
    				<#if translatedUnitOfMeasureOrNull?has_content>	
        				<@s.label label="" name="translatedUnitOfMeasureMessage" id="unitOfMeasure.name.translation"/>
     				</#if>	
				</#if>
	    	 						
</div>
<div class="formAction">
	
	<#if unitOfMeasureId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>

<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="display:hidden;">
	<div class="modifiedTitle">
		<@s.text name='unitofmeasure.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('unitofmeasure.create.label.name')}" name="modifiedEntity.name"/>
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>

