<title><@s.text name='reasonCodes.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
    <#include "/include/common/tablecomponent.ftl">

 	<@tablecomponent
 		varName="reasonCodeObj"
		columns=reasonCodesColumns
		tableId="reasonCodesTable"
		url="${pageContext}/reasonCodes"
		tableTitle='reasonCodes.view.title'
		viewId=reasonCodesViewId
    	type=""
    	extraParams=extraParamsHash?default("") />


		
	