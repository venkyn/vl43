<title><@s.text name='item.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
	<#include "/include/common/tablecomponent.ftl">

 	<@tablecomponent
 		varName="itemObj"
		columns=itemColumns
		tableId="listItemsTable"
		url="${pageContext}/item"
		tableTitle='item.view.title' 
		viewId=itemViewId
    	type=""
    	extraParams=extraParamsHash?default("") />
    	