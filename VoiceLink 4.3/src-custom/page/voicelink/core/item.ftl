
<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='item.edit.title'/>    
  </#assign>
  <#assign disabled="true">
  <#assign formName="edit">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='item.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='item.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='item.view.title.readOnly'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
 <title>${title}</title>
</head>
<body >

<div class="formBody" >
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}"  id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/item" action="${formName}.action" method="POST">
<#if itemId?has_content>
    <@s.hidden name="itemId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@s.textfield tabindex="1" label="%{getText('item.label.number')}" name="item.number" id="item.number" size="25" maxlength="50" 
    	required="true" disabled="${disabled}" readonly="${readOnly}"/>
	
    <#if readOnly=="true">
		<@s.textfield tabindex="2" label="%{getText('item.label.description')}" name="translatedItemDescription" id="item.description" size="25" maxlength="255" 
			show="true" required="true" disabled="${disabled}" readonly="${readOnly}"/>
	
		<@s.textfield tabindex="3" label="%{getText('item.label.phoneticDescription')}" name="translatedPhoneticDescription" id="item.phoneticDescription" 
			size="25" maxlength="255" show="true" required="true" readonly="${readOnly}"/>
	<#else>
		<@s.textfield tabindex="2" label="%{getText('item.label.description')}" name="item.description" id="item.description" size="25" maxlength="255" 
			show="true" required="true" disabled="${disabled}" readonly="${readOnly}"/>
	
    	<#if translatedItemDescriptionOrNull?has_content>	
        	<@s.label label="" name="translatedItemDescriptionMessage" id="item.description.translation"/>
     	</#if>
     		
		<@s.textfield tabindex="3" label="%{getText('item.label.phoneticDescription')}" name="item.phoneticDescription" id="item.phoneticDescription" 
			size="25" maxlength="255" show="true" required="true" readonly="${readOnly}"/>
    	<#if translatedPhoneticDescriptionOrNull?has_content>	
        	<@s.label label="" name="translatedPhoneticDescriptionMessage" id="item.phoneticDescription.translation"/>
     	</#if>	
	</#if>
</div>
<div class="formMiddle">    
    <@s.textfield tabindex="5" label="%{getText('item.label.size')}" name="item.size" id="item.size" size="25" maxlength="50" 
    	required="true" disabled="${disabled}" readonly="${readOnly}"/>
    
    <@s.textfield tabindex="6" label="%{getText('item.label.cube')}" name="item.cube" id="item.cube" size="25" maxlength="25" 
    	required="true" disabled="${disabled}" readonly="${readOnly}"/>
    
    <@s.textfield tabindex="7" label="%{getText('item.label.weight')}" name="item.weight" id="item.weight" size="25" maxlength="25" 
    	required="true" disabled="${disabled}" readonly="${readOnly}"/>

    <@s.checkbox tabindex="8" label="%{getText('item.label.isVariableWeightItem')}" name="item.isVariableWeightItem" id="item.isVariableWeightItem" 
    	fieldValue="true" disabled="${disabled}" trueValue="item.isVariableWeightItem.true" falseValue="item.isVariableWeightItem.false"
    	readonly="${readOnly}"/>

    <@s.textfield tabindex="9" label="%{getText('item.label.variableWeightTolerance')}" name="item.variableWeightTolerance" id="item.variableWeightTolerance" 
    	size="25" maxlength="2" required="true" disabled="${disabled}" readonly="${readOnly}" />
    	
    <@s.checkbox tabindex="10" label="%{getText('item.label.isCaptureSerialNumbers')}" name="item.isSerialNumberItem" id="item.isSerialNumberItem" 
    	fieldValue="true" disabled="${disabled}" trueValue="item.isCaptureSerialNumbers.true" falseValue="item.isCaptureSerialNumbers.false" readonly="${readOnly}"/>
    
    <@s.textfield tabindex="11" label="%{getText('item.label.pack')}" name="item.pack" id="item.pack" size="25" maxlength="50" 
    	required="false" disabled="${disabled}" readonly="${readOnly}"/>
</div>
<div class="formBottom">
    <@s.textfield tabindex="12" label="%{getText('item.label.scannedVerification')}" name="item.scanVerificationCode" id="item.scanVerificationCode" 
    	size="25" maxlength="50" disabled="${disabled}" readonly="${readOnly}"/>
    
    <@s.textfield tabindex="13" label="%{getText('item.label.spokenVerification')}" name="item.spokenVerificationCode" id="item.spokenVerificationCode" 
    	size="25" maxlength="5" disabled="${disabled}" readonly="${readOnly}"/>
    
    <@s.textfield tabindex="14" label="%{getText('item.label.upc')}" name="item.upc" id="item.upc" size="25" maxlength="50" 
    	required="true" disabled="${disabled}" readonly="${readOnly}"/>
</div>
<div class="formAction">
	
	<#if itemId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('item.label.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('item.label.description')}" name="modifiedEntity.description"/>
	    
	    <@s.label label="%{getText('item.label.phoneticDescription')}" name="modifiedEntity.phoneticDescription"/>
	</div>
	<div class="formMiddle">
	    <@s.label label="%{getText('item.label.size')}" name="modifiedEntity.size"/>
	
	    <@s.label label="%{getText('item.label.cube')}" name="modifiedEntity.cube"/>
	    
	    <@s.label label="%{getText('item.label.weight')}" name="modifiedEntity.weight"/>
	    
	    <@s.label label="%{getText('item.label.isVariableWeightItem')}" name="modifiedEntity.isVariableWeightItem"/>
	    
	    <@s.label label="%{getText('item.label.variableWeightTolerance')}" name="modifiedEntity.variableWeightTolerance"/>
	    
	    <@s.label label="%{getText('item.label.pack')}" name="modifiedEntity.pack"/>
	</div>
	<div class="formBottom">
	    <@s.label label="%{getText('item.label.scannedVerification')}" name="modifiedEntity.scanVerificationCode"/>
	
	    <@s.label label="%{getText('item.label.spokenVerification')}" name="modifiedEntity.spokenVerificationCode"/>
	    
	    <@s.label label="%{getText('item.label.upc')}" name="modifiedEntity.upc"/>
	</div>
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>
<script type="text/JavaScript">
	
	var isVariableWeightItem = $("item.isVariableWeightItem");	
	var isSerialNumberItem = $("item.isSerialNumberItem");
    var variableWeightTolerance = $("item.variableWeightTolerance");
	<#if navigation.pageName="create">	
		function enableDisableTolerance() {			   
			if (!isVariableWeightItem.checked) {
			  	variableWeightTolerance.disabled = true;
			  	variableWeightTolerance.value = 0;
			} else {
			  	variableWeightTolerance.disabled = false;
			}
		}	    
	
		connect($("item.isVariableWeightItem"),"onclick",enableDisableTolerance);
		enableDisableTolerance();		
	</#if>
	
	<#if !readOnly?has_content || readOnly=="false">
		MochiKit.Signal.connect("form1.submit1","onclick",enableCheckBoxBeforeSubmission);	
		function enableCheckBoxBeforeSubmission(){	  
		  if(isVariableWeightItem.disabled = true)
		   {	
		  	isVariableWeightItem.disabled = false;
		   }	  
		  if(isSerialNumberItem.disabled = true)
		  { 
		  	isSerialNumberItem.disabled = false;
		  }
		}
	</#if>
</script>

</body>
</html>

