<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='breakType.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='breakType.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='breakType.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='breakType.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='breakType.view.single.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/breakType" action="${formName}.action" method="POST">
<#if breakTypeId?has_content>
    <@s.hidden name="breakTypeId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@s.textfield tabindex="1" 
        label="%{getText('breakType.create.label.number')}" 
        name="breakType.number" 
        id="breakType.number" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>
	
    <#if readOnly=="true">
		<@s.textfield tabindex="2" 
	    	label="%{getText('breakType.create.label.name')}" 
	    	name="translatedBreakTypeName" 
	    	id="breakType.name" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	required="true" 
	    	readonly="${readOnly}"/>	
	<#else>
		<@s.textfield tabindex="2" 
	    	label="%{getText('breakType.create.label.name')}" 
	    	name="breakType.name" 
	    	id="breakType.name" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	required="true" 
	    	readonly="${readOnly}"/>	
    	<#if translatedBreakTypeNameOrNull?has_content>	
        	<@s.label label="" name="translatedBreakTypeNameMessage" id="breakType.name.translation"/>
     	</#if>	
	</#if>
</div>
<div class="formAction">
	
	<#if breakTypeId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='breakType.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('breakType.create.label.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('breakType.create.label.name')}" name="modifiedEntity.name"/>
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>


</body>
</html>

