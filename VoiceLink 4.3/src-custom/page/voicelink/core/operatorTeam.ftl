<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='operatorTeam.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='operatorTeam.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='operatorTeam.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='operatorTeam.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='operatorTeam.view.single.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>
<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/operatorTeam" action="${formName}.action" method="POST">
<#if operatorTeamId?has_content>
    <@s.hidden name="operatorTeamId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
	<@s.textfield tabindex="2" 
	    label="%{getText('operatorTeam.label.name')}" 
	    name="operatorTeam.name" 
	    id="operatorTeam.name" 
	    size="25" 
	    maxlength="50" 
	    show="true" 
	    required="true" 
	    readonly="${readOnly}"/>	
    <@s.select tabindex="3"
        size="10"
        theme="css_xhtml"
        label="%{getText('operatorTeam.label.operators')}" 
	    name="operators" 
		id="operators" 
		list="operatorMap" 
		listKey="value" 
		listValue="key"
		emptyOption="false"
		readonly="${readOnly}"
		cssClass="operatorTeamSelect" 
		multiple="true" 
		width="150" />
</div>
<div class="formAction">
	
	<#if operatorTeamId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='operatorTeam.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('operatorTeam.label.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('operatorTeam.label.name')}" name="modifiedEntity.name"/>
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>


</body>
</html>

