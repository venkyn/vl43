<#if readOnly>
  <#assign readOnly="true">
<#else>
  <#assign readOnly="false">
</#if>
<#if reportType?has_content>
    <#assign iter=1>
    <#assign defaultValTxt = ""/>
    <#list reportType.reportTypeParameters as param>
              <#if  param.isRequired = true >
                <#assign required="true">
              <#else>
                <#assign required="false">
              </#if>
              <#if param.defaultValue?has_content &&  param.fieldType != "ListField">
                <#assign defaultVal = param.defaultValue >
              <#else>
                <#assign defaultVal = "">
              </#if>
              <#assign selectedValues = ""/>
              <#if report.reportParameters?has_content>
                  <#list report.reportParameters as param2>
                      <#if param.id = param2.reportTypeParameter.id>
                       <#assign fieldRequiredError = fieldErrors["reportTypeParam${param.id?c}"]?exists
                       && fieldErrors["reportTypeParam${param.id?c}"]?contains(action.getText('errors.required'))>

                          <#if param2.value?has_content && !fieldRequiredError>
                              <#assign defaultVal = param2.value>
                          <#else>
                              <#assign defaultVal = "">
                          </#if>
                      </#if>
                  </#list>
                  <#if param.dropDownData?has_content>
                      <#list param.dropDownData?values as paramDropVal>
                          <#if defaultVal == paramDropVal>
                              <#list param.dropDownData?keys as paramDropKey>
                                  <#if param.dropDownData[paramDropKey] == paramDropVal>
                                       <#assign defaultValTxt = paramDropKey>
                                  </#if>
                              </#list>
                          </#if>
                      </#list>
                      <#list action.getSelectedValues(param.defaultValue) as selvalue>
                         <#assign selectedValues = "${selectedValues},${selvalue}"/>
                      </#list>
                  </#if>
              </#if>
              <!-- Text Field -->
              <#if param.fieldType = "TextField">
                     <@s.textfield
                        tabindex="${iter}"
                        label="%{getText('${param.description}')}"
                        name="reportTypeParam${param.id?c}"
                        id="reportTypeParam${param.id?c}"
                        value="${defaultVal}"
                        size="25"
                        maxlength="50"
                        required="${required}"
                        readonly="${readOnly}"/>
              <!-- Drop down -->
              <#elseif param.fieldType = "DropDownWithAll" || param.fieldType ="DropDownWithoutAll">
                     <#if readOnly == "true">
                        <@s.label label="%{getText('${param.description}')}" name="reportTypeParam${param.id?c}" value="${defaultValTxt}"/>
                     <#else>
                     <@s.select tabindex="${iter}"
                        theme="css_xhtml"
                        label="%{getText('${param.description}')}"
                        name="reportTypeParam${param.id?c}"
                        id="reportTypeParam${param.id?c}"
                        list=param.dropDownData
                        listKey="value"
                        value="${defaultVal}"
                        defaultValue="${defaultVal}"
                        listValue="key"
                        onchange="javascript:runScript('reportTypeParam${param.id?c}')"
                        required="${required}"
                        readonly="${readOnly}"/>
                     </#if>
              <!-- List -->
              <#elseif param.fieldType = "ListField">
                     <#if readOnly == "true">
                        <@s.select label="%{getText('${param.description}')}"  name="${param.defaultValue}" list=param.dropDownData listKey="value" listValue="key" multiple="true" readonly="${readOnly}"  size="10" width="150" />
                     <#else>
                     <@s.select tabindex="${iter}"
                        size="10"
                        theme="css_xhtml"
                        label="%{getText('${param.description}')}"
                        name="reportTypeParam${param.id?c}"
                        id="reportTypeParam${param.id?c}"
                        list=param.dropDownData
                        listKey="value"
                        listValue="key"
                        emptyOption="false"
                        defaultValue="${selectedValues}"
                        readonly="${readOnly}"
                        cssClass="operatorTeamSelect"
                        multiple="true"
                        width="150" />
                     </#if>
              </#if>
              <#assign iter=iter + 1>
    </#list>
<#else>
</#if>

