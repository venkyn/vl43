
<title><@s.text name='report.view.title'/></title>
<#include "/include/common/tablecomponent.ftl">	
<#assign pageContext="${base}/${navigation.applicationMenu}">


 	<@tablecomponent
 		varName="reportObj"
		columns=reportColumns
		tableId="listReportsTable"
		url="${pageContext}/report"
		tableTitle='report.view.title' 
		viewId=reportViewId
    	type=""
    	extraParams=extraParamsHash?default("") />