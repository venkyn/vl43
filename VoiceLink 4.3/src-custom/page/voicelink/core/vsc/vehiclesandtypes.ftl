<title><@s.text name='vsc.vehicleType.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
    <#include "/include/common/tablecomponent.ftl">	
    <@tablecomponent
     		varName="vehicleTypeObj"
    		columns=vehicleTypeColumns
    		tableId="listVehicleTypesTable"
    		url="${pageContext}/vscvehicletype"
    		tableTitle='vsc.vehicleType.view.title' 
            viewId=vehicleTypeViewId
        	type=""
        	extraParams=extraParamsHash?default("") />      
 
    <#assign urlMethod="getVehicleData"/>    
    <@tablecomponent
            varName="vehicleObj"
            columns=vehicleColumns
            tableId="listVehiclesTable"
            url="${pageContext}/vscvehicles"
            tableTitle='vsc.vehicle.view.title' 
            viewId=vehicleViewId
            type=""
            extraParams={"publishers":[{"name" : "vehicleTypeObj", "selector": "typeId"}]} />