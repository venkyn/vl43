<title><@s.text name='vsc.response.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
    
	<#include "/include/common/tablecomponent.ftl">	
		
 	<@tablecomponent
 		varName="tableObj"
		columns=checkResponseColumns
		tableId="listCheckResponseTable"
		url="${pageContext}/vscresponse"
		tableTitle='vsc.response.view.title' 
		viewId=checkResponseViewId
    	type=""
    	extraParams=extraParamsHash?default("") />