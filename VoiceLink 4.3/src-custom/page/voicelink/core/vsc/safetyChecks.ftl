<title><@s.text name='vsc.safetyCheck.view.title'/></title>
	<#assign pageContext="${base}/${navigation.applicationMenu}">
	
	<#include "/include/common/tablecomponent.ftl">	
	
	<@tablecomponent
        varName="safetyChecksObj"
        columns=safetyCheckColumns
        tableId="listSafetyChecksTable"
        url="${pageContext}/vscsafetycheck"
        tableTitle='vsc.safetyCheck.view.title' 
        viewId=safetyChecksViewId
        type=""
        extraParams=extraParamsHash?default("") />