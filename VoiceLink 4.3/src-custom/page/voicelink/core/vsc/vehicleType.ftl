<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='vsc.vehicleType.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='vsc.vehicleType.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='vsc.vehicleType.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='vsc.vehicleType.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='vsc.vehicleType.view.title.single'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
    ${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/vscvehicletype" action="${formName}.action" method="POST">
<#if typeId?has_content>
    <@s.hidden name="typeId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@s.textfield tabindex="1" 
                   label="%{getText('vsc.vehicleType.label.number')}" 
                   name="vehicleType.number" 
                   id="vehicleType.number" 
                   size="15" 
                   maxlength="9" 
                   show="true" 
                   readonly="${readOnly}" 
                   disabled="${disabled}"
                   required="true"/>
    
    <@s.textfield tabindex="2" 
                  label="%{getText('vsc.vehicleType.label.description')}" 
                  name="vehicleType.description" 
                  id="vehicleType.description" 
                  size="25" 
                  maxlength="100" 
                  show="true" 
                  readonly="${readOnly}" 
                  disabled="${disabled}"
                  required="true"/>
    
    <@s.checkbox tabindex="3" 
                 theme="css_xhtml" 
                 name="vehicleType.isCaptureVehicleID" 
                 id="vehicleType.isCaptureVehicleID" 
                 show="true"  
                 stayleft="true"
                 trueValue="vehicleType.captureVehicleId.view.true"
                 falseValue="vehicleType.captureVehicleId.view.false"
                 cssClass="checkbox" 
                 label="%{getText('vsc.vehicleType.label.isCaptureVehicleID')}" 
                 readonly="${readOnly}"/>                 
</div>
<div class="formAction">
	
	<#if typeId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='vehicleType.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('vsc.vehicleType.label.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('vsc.vehicleType.label.description')}" name="modifiedEntity.description"/>
	    
	    <@s.label label="%{getText('vsc.vehicleType.label.isCaptureVehicleID')}" name="modifiedEntity.isCaptureVehicleID"/>
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>


</body>
</html>

