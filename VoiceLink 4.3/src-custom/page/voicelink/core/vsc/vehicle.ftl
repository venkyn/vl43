<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='vsc.vehicle.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='vsc.vehicle.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='vsc.vehicle.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='vsc.vehicle.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='vsc.vehicle.view.title.single'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
    ${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/vscvehicles" action="${formName}.action" method="POST">
<#if vehicleId?has_content>
    <@s.hidden name="vehicleId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<#if typeId?has_content>
    <@s.hidden name="typeId"/>
</#if>
<div class="formTop">
    <@s.textfield tabindex="1" 
                   label="%{getText('vsc.vehicle.label.vehicleType')}" 
                   name="vehicle.vehicleType.description" 
                   id="vehicle.vehicleType.description" 
                   size="15" 
                   maxlength="10" 
                   show="true" 
                   readonly="true" 
                   required="false"/>
    <@s.textfield tabindex="2" 
                   label="%{getText('vsc.vehicle.label.vehicleCategory')}" 
                   name="vehicle.vehicleCategory" 
                   id="vehicle.vehicleType.description" 
                   size="15" 
                   maxlength="10" 
                   show="true" 
                   readonly="true" 
                   required="false"/> 
</div>
<div class="formMiddle">                   
    <@s.textfield tabindex="3" 
                   label="%{getText('vsc.vehicle.label.number')}" 
                   name="vehicle.vehicleNumber" 
                   id="vehicle.vehicleNumber" 
                   size="15" 
                   maxlength="10" 
                   show="true" 
                   readonly="${readOnly}" 
                   disabled="${disabled}"
                   required="true"/>      
     <@s.textfield tabindex="3" 
                   label="%{getText('vsc.vehicle.label.spokenNumber')}" 
                   name="vehicle.spokenVehicleNumber" 
                   id="vehicle.spokenVehicleNumber" 
                   size="15" 
                   maxlength="5" 
                   show="true" 
                   readonly="${readOnly}" 
                   required="true"/>            
</div>
<div class="formAction">
    
    <#if vehicleId?has_content>
        <#if !actionErrors?has_content>
            <#assign submitOnChangeForm="form1">
        </#if>
    </#if>

    <#assign submitIndex=199>
    <#assign cancelIndex=200>

    <#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
    <div id="modEntity" style="visibility: hidden;">
        <div class="modifiedTitle">
            <@s.text name='vsc.vehicle.edit.modifiedEntity.title'/>
        </div>
        <div class="formTop">
            <@s.label label="%{getText('vsc.vehicle.label.vehicleType')}" name="modifiedEntity.vehicleType.description" />
            <@s.label label="%{getText('vsc.vehicle.label.vehicleCategory')}" name="modifiedEntity.vehicleCategory" />
            <@s.label label="%{getText('vsc.vehicle.label.number')}" name="modifiedEntity.vehicleNumber" />
            <@s.label label="%{getText('vsc.vehicle.label.spokenNumber')}" name="modifiedEntity.spokenVehicleNumber" />
        </div>
    </div>
    <script language="javascript">
    YAHOO.namespace("example.resize");

    function init() {
        YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
            {
              width:"450",
              left: 300,
              top: 200,
              constraintoviewport: true,
              fixedcenter: true,
              underlay:"shadow",
              close:true,
              visible:false,
              draggable:true,
              modal:false } );
        YAHOO.example.resize.panel.render();
    }

    YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>


</body>
</html>