<title><@s.text name='operator.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
	<#include "/include/common/tablecomponent.ftl">

 	<@tablecomponent
 		varName="operatorObj"
		columns=operatorColumns
		tableId="listOperatorsTable"
		url="${pageContext}/operator"
		tableTitle='operator.view.title' 
		viewId=operatorViewId
    	type=""
    	extraParams=extraParamsHash?default("") />     			
