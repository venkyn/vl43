<#assign readOnly="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='region.edit.title'/> <@s.text name='region.step.part1'/> ${pageNumber?default(1)} <@s.text name='region.step.part2'/>
  </#assign>
  <#assign formName="edit">
<#elseif navigation.pageName="create" || navigation.pageName="create!duplicate">
  <#assign title>
    <@s.text name='region.create.title'/> <@s.text name='region.step.part1'/> ${pageNumber?default(1)} <@s.text name='region.step.part2'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='region.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='region.view.single.title'/> <@s.text name='region.step.part1'/> ${pageNumber?default(1)} <@s.text name='region.step.part2'/>
  </#assign>
  <#assign formName="viewRegion">
  <#assign readOnly="true">
</#if>

<html>
<head>
 <title>${title}</title>

</head>
<body >

<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/selection/region" action="${formName}.action" method="POST">

<input type="hidden" id="pageNumber" name="pageNumber" value="${pageNumber?default(1)}">
<input type="hidden" id="changeAction" name="changeAction" value="next">
<input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>

<@s.hidden name="savedEntityKey"/>
<#if regionId?has_content>
    <@s.hidden name="regionId"/>
</#if>
<#if navigation.pageName="create" || navigation.pageName="edit" || navigation.pageName="create!duplicate" ||  navigation.pageName="viewRegion">
  
  <#if !submitValue?has_content>
	  <#assign submitValue>
		<@s.text name="form.button.submit"/>
	  </#assign>
  </#if>

  <#if !cancelValue?has_content>
	<#assign cancelValue>
		<@s.text name="form.button.cancel"/>
	</#assign>
  </#if>

  <#if !pageNumber?has_content>
	  <#assign pageNumber=1>
  </#if>

<script type="text/javascript">
	var formObject = document.forms['${formName}'];
	
    function handleFunc (response) {
        $('profileDiv').innerHTML = response.responseText;
    }
  
    function handleFunc2 () {
        log('error');
    }    
    
    function nextForm() {

        formObject.changeAction.value = "next";
        
        var divs = $(formObject).getElementsByTagName("div");
        var hasAnyError = new Array();

        for(var i = 0; i < divs.length; i++) {
        var p = divs[i];
        if (p.getAttribute("errorFor")) {
            hasAnyError.push(p);
        }
       }
      
      if( hasAnyError.length <1 ) {
        $(formObject).submit();
      } 
    }  

    function prevForm() {
        $(formObject).changeAction.value = "prev";
        $(formObject).submit();
    }  
   
    function saveForm() {
        $(formObject).changeAction.value = "save";
        $(formObject).submit();
    }  
    
    function changeProfile(id) {
        var pid = $(id).options[$(id).selectedIndex].value;
    <#if regionId?has_content>
        var time= new Date();
        var params = {selectionChaseProfileId: pid,
                      regionId: ${regionId?c},
                      savedEntityKey: '${savedEntityKey}',
                      pageNumber: ${pageNumber?default(-1)},
                      formName: '${formName}',
                      date: time};
        if (id=="selectionNormalProfileId") {
            params = {selectionNormalProfileId: pid,
                      regionId: ${regionId?c},
                      pageNumber: ${pageNumber?default(-1)},
                      formName: '${formName}',
                      date: time};
        }
      <#else>
        var time= new Date();
        var params = {selectionChaseProfileId: pid,
                      savedEntityKey: '${savedEntityKey}',
                      pageNumber: ${pageNumber?default(-1)},
                      formName: '${formName}',
                      date: time};
        if (id=="selectionNormalProfileId") {
            params = {selectionNormalProfileId: pid,
                      savedEntityKey: '${savedEntityKey}',
                      pageNumber: ${pageNumber?default(-1)},
                      formName: '${formName}',
                      date: time};
                      
        }
       </#if>
        var url = "${base}/selection/region/getProfile.action";
        simpleXMLHttpRequestOutput=null;
	
        simpleXMLHttpRequestOutput=doSimpleXMLHttpRequest(url, params).addCallbacks(handleFunc,handleFunc2);           
    }

 </script>
<div id='pageDiv'>
    <#include "regionChange.ftl">
</div>

</#if>
</@s.form>
  
  
