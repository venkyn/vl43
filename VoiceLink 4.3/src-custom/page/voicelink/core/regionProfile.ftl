<#assign readOnly="false">
  <#assign title>
    <@s.text name='regionProfile.view.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/selection/breakType" action="${formName}.action" method="POST">
<#if breakTypeId?has_content>
    <@s.hidden name="regionProfileId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@s.textfield tabindex="1" 
        label="%{getText('breakType.create.label.number')}" 
        name="breakType.number" 
        id="breakType.number" 
        size="25" 
        maxlength="50" 
        required="true" 
        readonly="${readOnly}"/>
	
	<@s.textfield tabindex="2" 
	    label="%{getText('breakType.create.label.name')}" 
	    name="breakType.name" 
	    id="breakType.name" 
	    size="25" 
	    maxlength="50" 
	    show="true" 
	    required="true" 
	    readonly="${readOnly}"/>	
</div>
<div class="formAction">
	
	<#if breakTypeId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modifiedEntity" style="display:none;">
	<div class="modifiedTitle">
		<@s.text name='modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('breakType.create.label.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('breakType.create.label.name')}" name="modifiedEntity.name"/>
	</div>	
</div>
</#if>
</@s.form>
</div>

</body>
</html>