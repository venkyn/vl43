<#assign readOnly="false">
<#assign disabled="false">
<#assign title>
  <@s.text name='report.schedule.title'/>
</#assign>
<#assign formName="schedule">
<#assign submitValue>
  <@s.text name='report.schedule.button.submit'/>
</#assign>
<#assign semicolon><@s.text name='semicolon'/></#assign>
<#assign intervalformat><@s.text name='schedule.edit.daily.timeformat'/></#assign>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title} 
</div>

<script language="javascript">
function enableSchedule() {
    if (document.getElementById('frequency').value == 0) {
        disableForm(true);
    } else {
        disableForm(false);
    }
}

function disableForm(disabled) {
        document.getElementById('report.emails').disabled = disabled;
        document.getElementById('timeHours').disabled = disabled;
        document.getElementById('timeMinutes').disabled = disabled;
}
</script>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/report" action="${formName}.action" method="POST">
<#if reportId?has_content>
    <@s.hidden name="reportId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
		<@s.textfield tabindex="1" 
	    	label="%{getText('report.label.name')}" 
	    	name="report.name" 
	    	id="report.name" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	required="true" 
	    	readonly="true"/>

	    <@s.select tabindex="2"
            theme="css_xhtml"
            label="%{getText('report.label.frequency')}" 
	        name="frequency" 
		    id="frequency" 
		    list="frequencyMap" 
		    listKey="key" 
		    listValue="value"
		    emptyOption="false"
		    readonly="${readOnly}"
		    cssClass="reportTypeSelect" 
		    multiple="false" 
		    onchange="enableSchedule();" />     
		
		<#-- This was added to support the locales of the report scheduler -->   		
		<@s.select tabindex="2"
            theme="css_xhtml"
            label="%{getText('report.label.locale')}" 
	        name="report.language" 
		    id="report.language" 
		    list="languageMap" 
		    listKey="key" 
		    listValue="value"
		    emptyOption="false"
		    readonly="${readOnly}"
		    cssClass="reportTypeSelect" 
		    multiple="false" 
		    onchange="enableSchedule();" />   
		    
		      
<table class="wwctrlgrp_table"><tr><td>		                             		      		               
         <@s.select tabindex="4" 
            cssStyle="width: 40px"		
		    theme="css_xhtml" 
		    label="%{getText('report.schedule.type.time')}"
		    name="timeHours" 
            id="timeHours"
            list="scheduleHoursMap"
            listKey="key"
            listValue="value"
            emptyOption="false"
            size="1" 
            length="50"
            show="true"
            required="false"
            readonly="${readOnly}"
            grpId="time" />
</td><td class="scheduleTime">
	
		<@s.select tabindex="4" 
            cssStyle="width: 40px"		
		    theme="css_xhtml" 
		    label="&nbsp %{getText('semicolon')}"
		    name="timeMinutes" 
            id="timeMinutes"
            list="scheduleMinutesMap"
            listKey="value"
            listValue="key"
            emptyOption="false"
            size="1" 
            length="50"
            show="true"
            required="false"
            readonly="${readOnly}"
            grpId="time" />
</td><td>
            <@s.label tabindex="3"
  		    theme="css_xhtml" 
  		    label="&nbsp %{getText('schedule.edit.daily.timeformat.24')}" 
  		    name="timeformat" 
  		    id="timeformat"
  		    size="0"
  		    maxlength="0"
  		    show="true"
  		    required="false"
  		    readonly="${readOnly}" />
</td></tr></table>    
		<@s.textfield tabindex="5" 
	    	label="%{getText('report.label.subject')}" 
	    	name="report.subject" 
	    	id="report.subject" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	required="true" 
	    	readonly="${readOnly}"/>
	    	        
		<@s.textfield tabindex="5" 
	    	label="%{getText('report.label.email')}" 
	    	name="report.emails" 
	    	id="report.emails" 
	    	size="25" 
	    	maxlength="255" 
	    	show="true" 
	    	required="true" 
	    	readonly="${readOnly}"/>		    		    		    
</div>
<div class="formAction">
	
	<#if reportId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='report.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('report.create.label.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('report.create.label.name')}" name="modifiedEntity.name"/>
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>


</body>
</html>

