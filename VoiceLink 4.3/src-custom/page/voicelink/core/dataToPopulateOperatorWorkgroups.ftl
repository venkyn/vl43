<table border="0" class="content" width="260px">
<tr>
<td colspan="2">
<div id="textContainer">
<@s.text name='operator.assign.prompt.body' />
</div>
</td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
<td>
<div id="textContainer" style="font-weight: bold;">
<@s.text name="operator.label.workgroupAssignment" /><@s.text name='semicolon'/>
</div>
</td>
<td align="right">
<@s.select tabindex="1" 
		    name="defaultWorkgroup" 
		    id="assignWorkgroupsDropdown" 
		    list="workgroupMap" 
			listKey="value" 
			listValue="key"
		    readonly="false"
		    emptyOption="true"
		    cssClass="workgroupSelect"
		    theme="css_nonbreaking"/>
</td>
</tr>
</table>
