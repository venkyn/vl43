<html>
<head>
</head>
<body>

<#include "/include/common/columnformatting.ftl" />
<#include "/include/common/controlgrouping.ftl" />

<#if formName?has_content>
  <#if formName="viewRegion">
    <#assign readOnly="true">
  <#else>
  <#assign readOnly="false">
  </#if>
 </#if>


    <#if pageNumber != 3>  
    <div class="formMiddle">
    <BR>
    <@columnlayout header="region.create.caption.profileConfig">
    <div  class="yui-u-thinner first-thinner">

        <#if selectionRegionProfile.autoIssuance>
    	    <#assign promptType>
                <@s.text name='region.create.label.assignment.issuance.automatic'/>
            </#assign> 
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.assignment.issuance.manual'/>
            </#assign>
        </#if>
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.assignment.issuance')}" 
            name="selectionRegionProfile.autoIssuance" 
            id="selectionRegionProfile.autoIssuance"
            value="${promptType}"/>
    
        <#if selectionRegionProfile.allowMultipleAssignments>
    	    <#assign promptType>
                <@s.text name='region.create.label.allow.multiple.assignments.yes'/>
            </#assign> 
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.allow.multiple.assignments.no'/>
            </#assign>
        </#if>               
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.allow.multiple.assignments')}" 
            name="selectionRegionProfile.allowMultipleAssignments" 
            id="selectionRegionProfile.allowMultipleAssignments"
            value="${promptType}"/>            

        <#if selectionRegionProfile.allowPassingAssignment>
    	    <#assign promptType>
                <@s.text name='region.create.label.allow.pass.assignments.yes'/>
            </#assign> 
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.allow.pass.assignments.no'/>
            </#assign>
        </#if>                           
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.allow.pass.assignments')}" 
            name="selectionRegionProfile.allowPassingAssignment" 
            id="selectionRegionProfile.allowPassingAssignment"
            value="${promptType}"/>                        

        <#if selectionRegionProfile.allowBaseItems>
    	    <#assign promptType>
                <@s.text name='region.create.label.allow.base.items.yes'/>
            </#assign> 
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.allow.base.items.no'/>
            </#assign>
        </#if>             
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.allow.base.items')}" 
            name="selectionRegionProfile.allowBaseItems" 
            id="selectionRegionProfile.allowBaseItems"
            value="${promptType}"/>                                    

        <#if selectionRegionProfile.requireCaseLabelCheckDigits>
    	    <#assign promptType>
                <@s.text name='region.create.label.case.label.check.digits.yes'/>
            </#assign> 
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.case.label.check.digits.no'/>
            </#assign>
        </#if>                         
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.case.label.check.digits')}" 
            name="selectionRegionProfile.requireCaseLabelCheckDigits" 
            id="selectionRegionProfile.requireCaseLabelCheckDigits"
            value="${promptType}"/>                                            
                                             
        <@controlgroup groupId="pickpromptTop" caption="region.create.caption.pickpromptTop" disabled="false">

        <#if selectionRegionProfile.multiplePickPrompt>
    	    <#assign promptType>
                <@s.text name='region.create.label.prompt.type.multiple'/>          
            </#assign>
                
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.prompt.type.single'/>
            </#assign>
        </#if>   
                 
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.prompt.type')}" 
            name="selectionRegionProfile.promptType" 
            id="selectionRegionProfile.promptType"
            value="${promptType}" />

        <#if selectionRegionProfile.requireQuantityVerification>
    	    <#assign promptType>
                <@s.text name='region.create.label.quantity.verification.yes'/>          
            </#assign>
                
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.quantity.verification.no'/>
            </#assign>
        </#if>
    
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.quantity.verification')}" 
            name="selectionRegionProfile.requireQuantityVerification" 
            id="selectionRegionProfile.requireQuantityVerification"
            value="${promptType}" />
            
        </@controlgroup>
     </div>
     <div class="yui-u-thinner">

        <#if selectionRegionProfile.requireDeliveryPrompt>
    	    <#assign promptType>
                <@s.text name='region.create.label.allow.delivery.yes'/>          
            </#assign>
                
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.allow.delivery.no'/>
            </#assign>
        </#if>
        
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.allow.delivery')}" 
            name="selectionRegionProfile.requireDeliveryPrompt" 
            id="selectionRegionProfile.requireDeliveryPrompt"
            value="${promptType}" />

        <#if selectionRegionProfile.printContainerLabelsIndicator="DoNotPrint">
    	    <#assign promptType>
                <@s.text name='region.create.label.print.labels.doNotPrint'/>          
            </#assign>
                
        <#elseif selectionRegionProfile.printContainerLabelsIndicator="WhenContainerOpened">
            <#assign promptType>           
                <@s.text name='region.create.label.print.labels.whenContainerOpened'/>
            </#assign>
        <#else>
            <#assign promptType>           
                <@s.text name='region.create.label.print.labels.whenContainerClosed'/>
            </#assign>
        </#if>

        <#if pageNumber == 1 >  
         <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.print.labels')}" 
            name="selectionRegionProfile.printContainerLabelsIndicator" 
            id="selectionRegionProfile.printContainerLabelsIndicator"
            value="${promptType}" />            
        </#if>    
        
        <#if pageNumber == 2 >  
        
          <#if selectionRegionProfile.chasePrintLabelsOn == true>
    	    <#assign chasePromptType>
                <@s.text name='region.create.label.chase.print.labels.yes'/>          
            </#assign>
            <#else>
             <#assign chasePromptType>
                <@s.text name='region.create.label.chase.print.labels.no'/>          
            </#assign>
            </#if>
                
        
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.chase.print.labels')}" 
            name="selectionRegionProfile.chasePrintLabelsOn" 
            id="selectionRegionProfile.chasePrintLabelsOn"
            value="${chasePromptType}" />         
        </#if>   
            
         <#if selectionRegionProfile.containerType?has_content>
	          <#assign groupDisabled="false">
	     <#else>
	          <#assign groupDisabled="true">
	          <#assign promptType="---"/>
	     </#if>	
        
  		<@controlgroup groupId="containersTop" caption="region.create.caption.containersTop" disabled="${groupDisabled}"> 

        <#if selectionRegionProfile.containerType?has_content>
            <#if selectionRegionProfile.containerType.allowPreCreationOfContainers>
        	    <#assign promptType>
                    <@s.text name='region.create.label.precreate.containers.yes'/>          
                </#assign>
                
            <#else>
                <#assign promptType>           
                    <@s.text name='region.create.label.precreate.containers.no'/>
                </#assign>
            </#if>
        </#if>
  		 
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.precreate.containers')}" 
            name="selectionRegionProfile.containerType.allowPreCreationOfContainers" 
            id="selectionRegionProfile.containerType.allowPreCreationOfContainers"
            disabled="${groupDisabled}"
            value="${promptType}" />

        <#if selectionRegionProfile.containerType?has_content>
            <#if selectionRegionProfile.containerType.promptOperatorForContainerID>
        	    <#assign promptType>
                    <@s.text name='region.create.label.prompt.id.yes'/>          
                </#assign>
                
            <#else>
                <#assign promptType>           
                    <@s.text name='region.create.label.prompt.id.no'/>
                </#assign>
            </#if>
        </#if>
            
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.prompt.id')}" 
            name="selectionRegionProfile.containerType.promptOperatorForContainerID" 
            id="selectionRegionProfile.containerType.promptOperatorForContainerID"
            disabled="${groupDisabled}"
            value="${promptType}" />            
            
        <#if selectionRegionProfile.containerType?has_content>
            <#if selectionRegionProfile.containerType.requireDeliveryAfterClosed>
        	    <#assign promptType>
                    <@s.text name='region.create.label.prompt.delivery.on.container.close.yes'/>          
                </#assign>
                
            <#else>
                <#assign promptType>           
                    <@s.text name='region.create.label.prompt.delivery.on.container.close.no'/>
                </#assign>
            </#if>
        </#if>

        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.prompt.delivery.on.container.close')}" 
            name="selectionRegionProfile.containerType.requireDeliveryAfterClosed" 
            id="selectionRegionProfile.containerType.requireDeliveryAfterClosed"
            disabled="${groupDisabled}"
            value="${promptType}" />            

        <#if selectionRegionProfile.containerType?has_content>
            <#if selectionRegionProfile.containerType.allowMultipleOpenContainers>
        	    <#assign promptType>
                    <@s.text name='region.create.label.allow.multiple.open.yes'/>          
                </#assign>
                
            <#else>
                <#assign promptType>           
                    <@s.text name='region.create.label.allow.multiple.open.no'/>
                </#assign>
            </#if>
        </#if>
            
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.allow.multiple.open')}" 
            name="selectionRegionProfile.containerType.allowMultipleOpenContainers" 
            id="selectionRegionProfile.containerType.allowMultipleOpenContainers"
            disabled="${groupDisabled}"
            value="${promptType}" />            

        <#if selectionRegionProfile.containerType?has_content>
            <#if selectionRegionProfile.containerType.requireTargetContainers>
        	    <#assign promptType>
                    <@s.text name='region.create.label.requires.target.containers.yes'/>          
                </#assign>
                
            <#else>
                <#assign promptType>           
                    <@s.text name='region.create.label.requires.target.containers.no'/>
                </#assign>
            </#if>
        </#if>
            
        <@s.label 
            theme="css_xhtml" 
            tabindex="1" 
            label="%{getText('region.create.label.requires.target.containers')}" 
            name="selectionRegionProfile.containerType.requireTargetContainers" 
            id="selectionRegionProfile.containerType.requireTargetContainers"
            disabled="${groupDisabled}"
            value="${promptType}" /><BR>          
            
        </@controlgroup>
    </div>
    </@columnlayout>
    </div>
    <br><br>
    </#if>
    <#if pageNumber <= 1>      
    <div class="formBottom regionPage">
    <br>
    <@columnlayout header="region.create.caption.configSettings"> 
    <div class="yui-u-thinner first-thinner">   
        <#if selectionRegionProfile.autoIssuance>
	          <#assign groupDisabled="true">
	     <#else>
	          <#assign groupDisabled="false">
	     </#if>

         <@controlgroup groupId="assignIssuance" caption="region.create.caption.assignIssuance" disabled="${groupDisabled}">
         <@s.select 
		     tabindex="4" 
		     label="%{getText('region.create.label.number.spoken.digits')}" 
		     name="selectionRegion.userSettingsNormal.workIdentifierRequestLength" 
		     id="workIdentifierRequestLength" 
		     list="spokenDigitsMap"
		     readonly="${readOnly}"
		     cssClass="statusSelect"
		     required="true"
		     disabled="${groupDisabled}"/>
   		</@controlgroup>  
	        

	     <#if selectionRegionProfile.allowMultipleAssignments>
	          <#assign groupDisabled="false">
	     <#else>
	          <#assign groupDisabled="true">
	     </#if>
	     	<@controlgroup groupId="multiAssigns" caption="region.create.caption.multiAssigns" disabled="${groupDisabled}">	     	     
	         <@s.textfield tabindex="5" 
	            label="%{getText('region.create.label.number.assignments')}" 
	            name="selectionRegion.userSettingsNormal.maximumAssignmentsAllowed" 
	            id="maximumAssignmentsAllowed" 
	            size="5" 
	            maxlength="2"
	            required="true"  
	            readonly="${readOnly}"
	            disabled="${groupDisabled}"/>
	        </@controlgroup>	            

	     
	     <#if selectionRegionProfile.allowBaseItems>
	          <#assign groupDisabled="false">
	          <#assign groupRequired="true">
	     <#else>
	          <#assign groupDisabled="true">
	          <#assign groupRequired="false">	          
	     </#if>
	     	<@controlgroup groupId="baseItems" caption="region.create.caption.baseItems" disabled="${groupDisabled}">
		    <@s.select 
		        tabindex="6" 
		        label="%{getText('region.create.label.picking.type')}"
		    	name="normalPickingType"
		    	id="pickBaseItemsBy"
		    	list="pickTypeMap"
		    	readonly="${readOnly}"
		    	cssClass="statusSelect"
		    	disabled="${groupDisabled}"/>

	        <@s.textfield 
	            tabindex="7" 
	            label="%{getText('region.create.label.base.quantity.threshold')}" 
	            name="selectionRegion.userSettingsNormal.baseQuantityThreshold" 
	            id="baseQuantityThreshold" 
	            size="5" 
	            maxlength="6"
	            onblur="validate(this);"
	            readonly="${readOnly}"
	            required="${groupRequired}"
		    	disabled="${groupDisabled}"/>	            
	            
	        <@s.textfield 
	            tabindex="8" 
	            label="%{getText('region.create.label.base.weight.threshold')}" 
	            name="selectionRegion.userSettingsNormal.baseWeightThreshold" 
	            id="baseWeightThreshold" 
	            size="5" 
	            maxlength="9" 
	            onblur="validate(this);" 
	            readonly="${readOnly}"
	            required="${groupRequired}"	            
		    	disabled="${groupDisabled}"/>	            
	        </@controlgroup>	            

        <@controlgroup groupId="skips" caption="region.create.caption.skips" disabled="false">
	    <@s.select 
	        tabindex="9" 
	        label="%{getText('region.create.label.allow.skips')}"
	    	name="normalAllowSkips"
	    	id="allowSkips"
	    	list="allowSkipsMap"
	    	readonly="${readOnly}"
	    	cssClass="skipSelect"/>

	    <@s.checkbox 
	        tabindex="10" 
            theme="css_xhtml" 
            label="%{getText('region.create.label.repick.skips')}" 
            name="selectionRegion.userSettingsNormal.allowRepickSkips" 
            id="allowRepickSkips" 
            trueValue="checkbox.readonly.yes"
            falseValue="checkbox.readonly.no"            
            cssClass="checkbox" 
            stayleft="true"
            readonly="${readOnly}"/><BR>
	    </@controlgroup> 

     	 <#if selectionRegionProfile.multiplePickPrompt>
	          <#assign groupDisabled="true">
	     <#else>
	          <#assign groupDisabled="false">
	     </#if>	    
	        
	    <@controlgroup groupId="pickPrompt" caption="region.create.caption.pickPrompt" disabled="${groupDisabled}"> 	    
	    <@s.checkbox 
	        tabindex="10" 
            theme="css_xhtml" 
            label="%{getText('region.create.label.suppress.speaking.quantity')}" 
            name="selectionRegion.userSettingsNormal.suppressQuantityOfOne"
            id="suppressQuantityOfOne" 
            cssClass="checkbox" 
            trueValue="checkbox.readonly.yes"
            falseValue="checkbox.readonly.no"            
            readonly="${readOnly}"
            stayleft="true"
            disabled="${groupDisabled}"/>   
	    
	    </@controlgroup>
	    </div>    
     	<div class="yui-u-thinner">  
     	
     	 <#if selectionRegionProfile.requireDeliveryPrompt>
	          <#assign groupDisabled="false">
	     <#else>
	          <#assign groupDisabled="true">
	     </#if>
     	
		<@controlgroup groupId="allowDelivery" caption="region.create.caption.allowDelivery" disabled="${groupDisabled}">     	
	    <@s.checkbox 
	        tabindex="11" 
	        theme="css_xhtml" 
	        label="%{getText('region.create.label.confirm.delivery')}" 
	        name="selectionRegion.userSettingsNormal.requireDeliveryConfirmation" 
	        id="requireDeliveryConfirmation" 
            trueValue="checkbox.readonly.yes"
            falseValue="checkbox.readonly.no"	        
	        cssClass="checkbox" 
	        readonly="${readOnly}"
	        stayleft="true"
        	disabled="${groupDisabled}"/>	            
	    </@controlgroup>
	    
     	 <#if selectionRegionProfile.containerType?has_content>
	          <#assign groupDisabled="false">
	     <#else>
	          <#assign groupDisabled="true">
	     </#if>	    
	    
		<@controlgroup groupId="containersBottom" caption="region.create.caption.containersBottom" disabled="${groupDisabled}">
	    <@s.textfield 
	        tabindex="13" 
	        label="%{getText('region.create.label.spoken.container.digits')}" 
	        name="selectionRegion.userSettingsNormal.numberOfContainerDigitsSpeak" 
	        id="numberOfContainerDigitsSpeak" 
	        size="5" 
	        maxlength="2" 
	        required="true"
	        onblur="validate(this);"
	        readonly="${readOnly}"
	    	disabled="${groupDisabled}"/>	            
	    </@controlgroup>
	    <@controlgroup groupId="shorts" caption="region.create.caption.shorts" disabled="false" addpadding="true">

	    <@s.select 
	        tabindex="14" 
	        label="%{getText('region.create.label.allow.go.back.for.shorts')}"
	    	name="normalAllowGoBackForShorts"
	    	id="goBackForShortsIndicator"
	    	list="allowGoBackForShortsMap"
	    	readonly="${readOnly}"
	    	cssClass="shortsSelect"/>

	    <@s.select
	        tabindex="15" 
	        label="%{getText('region.create.label.auto.mark.out.shorts')}" 
	        name="normalAutoMarkOutShorts" 
	        id="normalAutoMarkOutShorts"  
            list="autoMarkOutShortsMap"
	    	readonly="${readOnly}" 
	    	cssClass="shortsSelect"/>

	    <@s.checkbox 
	        tabindex="16" 
	        theme="css_xhtml" 
	        label="%{getText('region.create.label.verify.replenishments.host')}" 
	        name="selectionRegion.userSettingsNormal.verifyReplenishment" 
	        id="verifyReplenishment"  
            trueValue="checkbox.readonly.yes"
            falseValue="checkbox.readonly.no"	        
	        cssClass="checkbox"  
	        stayleft="true"
	        readonly="${readOnly}"/>
  	    </@controlgroup>		        
	    <@s.checkbox 
	        tabindex="17" 
	        theme="css_xhtml" 
	        label="%{getText('region.create.label.allow.signoff.during.selection')}" 
	        name="selectionRegion.userSettingsNormal.allowSignOffAtAnyPoint" 
	        id="allowSignOffAtAnyPoint"  
            trueValue="checkbox.readonly.yes"
            falseValue="checkbox.readonly.no"	        
	        cssClass="checkbox"  
	        stayleft="true"
	        readonly="${readOnly}"/>
	        
	    <@s.checkbox 
	        tabindex="18"  
	        theme="css_xhtml" 
	        label="%{getText('region.create.label.allow.reverse.picking')}" 
	        name="selectionRegion.userSettingsNormal.allowReversePicking" 
	        id="allowReversePicking" 
            trueValue="checkbox.readonly.yes"
            falseValue="checkbox.readonly.no"	        
	        cssClass="checkbox"  
	        stayleft="true"
	        readonly="${readOnly}"/>
       
	    <@s.select 
	        tabindex="19" 
	        label="%{getText('region.create.label.assignment.summary.prompt')}"
	    	name="normalSummaryPrompt"
	    	id="normalSummaryPrompt"
	    	list="assignmentSummaryPromptMap"
	    	readonly="${readOnly}" 
	    	cssClass="regionSummaryPromptSelect"/>
        </div>
    </@columnlayout>        
    </div>
    
    <#elseif pageNumber = 2>
    <div class="formMiddle">
    <BR>
    <@columnlayout header="region.create.caption.configSettings">
    <div class="yui-u-thinner first-thinner">   
    
         <#if selectionRegionProfile.autoIssuance>
	          <#assign groupDisabled="true">
	     <#else>
	          <#assign groupDisabled="false">
	     </#if>
    
         <@controlgroup groupId="assignIssuance" caption="region.create.caption.assignIssuance" disabled="${groupDisabled}">
         <@s.select 
	        tabindex="4" 
	        label="%{getText('region.create.label.number.spoken.digits')}" 
	        name="selectionRegion.userSettingsChase.workIdentifierRequestLength" 
	        id="workIdentifierRequestLength" 
            list="spokenDigitsMap"
            readonly="${readOnly}"
            cssClass="statusSelect"
            required="true"
            disabled="${groupDisabled}"/>
   		</@controlgroup>  
	                
	     <#if selectionRegionProfile.allowMultipleAssignments>
	          <#assign groupDisabled="false">
	     <#else>
	          <#assign groupDisabled="true">
	     </#if>
	     	<@controlgroup groupId="multiAssigns" caption="region.create.caption.multiAssigns" disabled="${groupDisabled}">	     	     
	         <@s.textfield tabindex="5" 
	            label="%{getText('region.create.label.number.assignments')}" 
	            name="selectionRegion.userSettingsChase.maximumAssignmentsAllowed" 
	            id="maximumAssignmentsAllowed" 
	            size="5" 
	            maxlength="2"  
	            required="true"
	            readonly="${readOnly}"
	            disabled="${groupDisabled}"/>
	        </@controlgroup>	            

	     
	     <#if selectionRegionProfile.allowBaseItems>
	          <#assign groupDisabled="false">
	          <#assign groupRequired="true">
	     <#else>
	          <#assign groupDisabled="true">
	          <#assign groupRequired="false">	          
	     </#if>
	     	<@controlgroup groupId="baseItems" caption="region.create.caption.baseItems" disabled="${groupDisabled}">
		    <@s.select 
		        tabindex="6" 
		        label="%{getText('region.create.label.picking.type')}"
		    	name="chasePickingType"
		    	id="pickBaseItemsBy"
		    	list="pickTypeMap"
		    	readonly="${readOnly}"
		    	cssClass="statusSelect"
		    	disabled="${groupDisabled}"/>

	        <@s.textfield 
	            tabindex="7" 
	            label="%{getText('region.create.label.base.quantity.threshold')}" 
	            name="selectionRegion.userSettingsChase.baseQuantityThreshold" 
	            id="baseQuantityThreshold" 
	            size="5" 
	            maxlength="128"
                onblur="validate(this);"
	            readonly="${readOnly}"
	            required="${groupRequired}"
		    	disabled="${groupDisabled}"/>	            
	            
	        <@s.textfield 
	            tabindex="8" 
	            label="%{getText('region.create.label.base.weight.threshold')}" 
	            name="selectionRegion.userSettingsChase.baseWeightThreshold" 
	            id="baseWeightThreshold" 
	            size="5" 
	            maxlength="128"  
                onblur="validate(this);"
	            readonly="${readOnly}"
	            required="${groupRequired}"
		    	disabled="${groupDisabled}"/>	            
	        </@controlgroup>	            

        <@controlgroup groupId="skips" caption="region.create.caption.skips" disabled="false">
	    <@s.select 
	        tabindex="9" 
	        label="%{getText('region.create.label.allow.skips')}"
	    	name="chaseAllowSkips"
	    	id="allowSkips"
	    	list="allowSkipsMap"
	    	readonly="${readOnly}" 
	    	cssClass="statusSelect"/>

	    <@s.checkbox 
	        tabindex="10" 
            theme="css_xhtml" 
            label="%{getText('region.create.label.repick.skips')}" 
            name="selectionRegion.userSettingsChase.allowRepickSkips" 
            id="allowRepickSkips" 
            trueValue="checkbox.readonly.yes"
            falseValue="checkbox.readonly.no"            
            cssClass="checkbox" 
            stayleft="true"
            readonly="${readOnly}"/>
	    </@controlgroup>     
	    
	     <#if selectionRegionProfile.multiplePickPrompt>
	          <#assign groupDisabled="true">
	     <#else>
	          <#assign groupDisabled="false">
	     </#if>	 
	     
	    <@controlgroup groupId="pickPrompt" caption="region.create.caption.pickPrompt" disabled="${groupDisabled}"> 
	    
	    <@s.checkbox 
	        tabindex="10" 
            theme="css_xhtml" 
            label="%{getText('region.create.label.suppress.speaking.quantity')}" 
            name="selectionRegion.userSettingsChase.suppressQuantityOfOne"
            id="suppressQuantityOfOne" 
            cssClass="checkbox" 
            trueValue="checkbox.readonly.yes"
            falseValue="checkbox.readonly.no"            
            readonly="${readOnly}"
            stayleft="true"
            disabled="${groupDisabled}"/>	            	    
	    
	    </@controlgroup>
	    </div>    
     	<div class="yui-u-thinner">  
     	
     	 <#if selectionRegionProfile.requireDeliveryPrompt>
	          <#assign groupDisabled="false">
	     <#else>
	          <#assign groupDisabled="true">
	     </#if>
     	
		<@controlgroup groupId="allowDelivery" caption="region.create.caption.allowDelivery" disabled="${groupDisabled}">     	
	    <@s.checkbox 
	        tabindex="11" 
	        theme="css_xhtml" 
	        label="%{getText('region.create.label.confirm.delivery')}" 
	        name="selectionRegion.userSettingsChase.requireDeliveryConfirmation" 
	        id="requireDeliveryConfirmation" 
            trueValue="checkbox.readonly.yes"
            falseValue="checkbox.readonly.no"	        
	        cssClass="checkbox" 
	        readonly="${readOnly}"
	        stayleft="true"
        	disabled="${groupDisabled}"/>	            
	    </@controlgroup>
	    
     	 <#if selectionRegionProfile.containerType?has_content>
	          <#assign groupDisabled="false">
	     <#else>
	          <#assign groupDisabled="true">
	     </#if>	    
	    
		<@controlgroup groupId="containersBottom" caption="region.create.caption.containersBottom" disabled="${groupDisabled}">
	    <@s.textfield 
	        tabindex="13" 
	        label="%{getText('region.create.label.spoken.container.digits')}" 
	        name="selectionRegion.userSettingsChase.numberOfContainerDigitsSpeak" 
	        id="numberOfContainerDigitsSpeak" 
	        size="5" 
	        maxlength="2" 
	        required="true"
	        readonly="${readOnly}"
	    	disabled="${groupDisabled}"/>	            
	    </@controlgroup>
	    <@controlgroup groupId="shorts" caption="region.create.caption.shorts" disabled="false">

	    <@s.select 
	        tabindex="14" 
	        label="%{getText('region.create.label.allow.go.back.for.shorts')}"
	    	name="chaseAllowGoBackForShorts"
	    	id="goBackForShortsIndicator"
	    	list="allowGoBackForShortsMap"
	    	readonly="${readOnly}" 
	    	cssClass="shortsSelect"/>

	    <@s.select
	        tabindex="15" 
	        label="%{getText('region.create.label.auto.mark.out.shorts')}" 
	        name="chaseAutoMarkOutShorts" 
	        id="chaseAutoMarkOutShorts"  
            list="autoMarkOutShortsMap"
	    	readonly="${readOnly}" 
	    	cssClass="shortsSelect"/>

	    <@s.checkbox 
	        tabindex="16" 
	        theme="css_xhtml" 
	        label="%{getText('region.create.label.verify.replenishments.host')}" 
	        name="selectionRegion.userSettingsChase.verifyReplenishment" 
	        id="verifyReplenishment"  
            trueValue="checkbox.readonly.yes"
            falseValue="checkbox.readonly.no"	        
	        cssClass="checkbox"  
	        stayleft="true"
            readonly="${readOnly}"/>
  	    </@controlgroup>		        
	    <@s.checkbox 
	        tabindex="17" 
	        theme="css_xhtml" 
	        label="%{getText('region.create.label.allow.signoff.during.selection')}" 
	        name="selectionRegion.userSettingsChase.allowSignOffAtAnyPoint" 
	        id="allowSignOffAtAnyPoint"  
	        cssClass="checkbox" 
            trueValue="checkbox.readonly.yes"
            falseValue="checkbox.readonly.no"	        
	        stayleft="true" 
	        readonly="${readOnly}"/>
	        
	    <@s.checkbox 
	        tabindex="18"  
	        theme="css_xhtml" 
	        label="%{getText('region.create.label.allow.reverse.picking')}" 
	        name="selectionRegion.userSettingsChase.allowReversePicking" 
	        id="allowReversePicking" 
	        cssClass="checkbox"
            trueValue="checkbox.readonly.yes"
            falseValue="checkbox.readonly.no"	          
	        stayleft="true"
	        readonly="${readOnly}"/>
       
	    <@s.select 
	        tabindex="19" 
	        label="%{getText('region.create.label.assignment.summary.prompt')}"
	    	name="chaseSummaryPrompt"
	    	id="chaseSummaryPrompt"
	    	list="assignmentSummaryPromptMap"
	    	readonly="${readOnly}" 
	    	cssClass="regionSummaryPromptSelect"/>
        </div>
    </@columnlayout>        
    </div>
    </#if>

</body>
</html>
