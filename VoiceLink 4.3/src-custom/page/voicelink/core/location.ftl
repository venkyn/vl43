
<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='location.edit.title'/>    
  </#assign>
  <#assign formName="edit">
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='location.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='location.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='location.view.title.readOnly'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/location" action="${formName}.action" method="POST">
<#if locationId?has_content>
    <@s.hidden name="locationId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@s.textfield tabindex="1" label="%{getText('location.create.label.spokenVerification')}" name="location.spokenVerification" id="location.spokenVerification" 
    	size="25" maxlength="50" disabled="${disabled}" required="true" readonly="${readOnly}"/>
	
	<@s.textfield tabindex="2" label="%{getText('location.create.label.scannedVerification')}" name="location.scannedVerification" id="location.scannedVerification" 
		size="25" maxlength="50" show="true" disabled="${disabled}" required="true" readonly="${readOnly}"/>	
</div>
<div class="formMiddle">
    <@s.textfield tabindex="3" label="%{getText('location.create.label.aisle')}" name="location.aisle" id="location.aisle" size="25" 
    	disabled="${disabled}" maxlength="50" readonly="${readOnly}"/>

    <@s.textfield tabindex="4" label="%{getText('location.create.label.slot')}" name="location.slot" id="location.slot" size="25" maxlength="50" 
    	disabled="${disabled}" readonly="${readOnly}"/>

    <@s.textfield tabindex="5" label="%{getText('location.create.label.checkDigits')}" name="location.checkDigits" id="location.checkDigits" 
    	size="25" maxlength="5" readonly="${readOnly}"/>
    
    <#if readOnly=="true">
    	<@s.textfield tabindex="6" label="%{getText('location.create.label.preAisleDirection')}" name="translatedPreAisle" id="location.preAisle" 
    		size="25" maxlength="50" readonly="${readOnly}"/>
               
    	<@s.textfield tabindex="7" label="%{getText('location.create.label.postAisleDirection')}" name="translatedPostAisle" id="location.postAisle" 
    		size="25" maxlength="50" readonly="${readOnly}"/>
	<#else>
    	<@s.textfield tabindex="6" label="%{getText('location.create.label.preAisleDirection')}" name="location.preAisle" id="location.preAisle" 
    		size="25" maxlength="50" readonly="${readOnly}"/>               	
    	<#if translatedPreAisleOrNull?has_content>	
        	<@s.label label="" name="translatedPreAisleMessage" id="location.preAisle.translation"/>
     	</#if>
     		
    	<@s.textfield tabindex="7" label="%{getText('location.create.label.postAisleDirection')}" name="location.postAisle" id="location.postAisle" 
    		size="25" maxlength="50" readonly="${readOnly}"/>
    	<#if translatedPostAisleOrNull?has_content>	
        	<@s.label label="" name="translatedPostAisleMessage" id="location.postAisle.translation"/>
     	</#if>	
	</#if>
</div>
<div class="formAction">
	
	<#if locationId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='location.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('location.create.label.spokenVerification')}" name="modifiedEntity.spokenVerification"/>
	    
	    <@s.label label="%{getText('location.create.label.scannedVerification')}" name="modifiedEntity.scannedVerification"/>
	</div>
	<div class="formMiddle">
	    <@s.label label="%{getText('location.create.label.aisle')}" name="modifiedEntity.aisle"/>
	
	    <@s.label label="%{getText('location.create.label.slot')}" name="modifiedEntity.slot"/>
	    
	    <@s.label label="%{getText('location.create.label.checkDigits')}" name="modifiedEntity.checkDigits"/>
	    
	    <@s.label label="%{getText('location.create.label.preAisleDirection')}" name="modifiedEntity.preAisle"/>
	    
	    <@s.label label="%{getText('location.create.label.postAisleDirection')}" name="modifiedEntity.postAisle"/>	    
	    
	</div>	
</div>

<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>

</body>
</html>

