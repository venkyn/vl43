<title><@s.text name='workgroup.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
	<#include "/include/common/tablecomponent.ftl">	

 	<@tablecomponent
 		varName="tableObj"
		columns=workgroupColumns
		tableId="listWorkgroupsTable"
		url="${pageContext}/workgroup"
		tableTitle='workgroup.view.title' 
		viewId=workgroupViewId
    	type=""
    	extraParams=extraParamsHash?default("") />     					
