<table border="0" class="content" width="220px"> 
<tr>
<td colspan="2">
<div id="textContainer">
   <@s.text name="assignment.resequence.body.selectRegionText" /><@s.text name='semicolon'/>
</div> 
</td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
<td>
<div id="textLabel" style="font-weight: bold;">
<@s.text name="entity.Region" /><@s.text name='semicolon'/>
</div>
</td>
<td align="right">
<div id="regionSelect">
<@s.select tabindex="1"
            label="%{getText('entity.Region')}" 
            name="regionName" 
	        id="regionName" 
	        list="resequenceRegionMap" 
	        cssClass="regionSelect"  
	        listValue="value" 
	        listKey="key" 
	        value="key" 
	        readonly="false"
	        theme="css_nonbreaking"/>
</div>
</td>
</tr>
</table>