<table border="0" class="content" width="280px">
<tr><td colspan="2"> 
<div id="textContainer">
   <@s.text name="print.label.message" /><@s.text name='semicolon'/>
</div> 
</td>
</tr>
<tr>
<td>
<div id="textContainer" style="font-weight: bold;">
<@s.text name="print.create.label.printer" /><@s.text name='semicolon'/> &nbsp;
</div>
</td>
<td align="right">
<@s.select tabindex="1"
            name="printerName" 
	        id="printerName" 
	        list="printerNamesMap" 
	        cssClass="printerSelect"  
	        listValue="value" 
	        listKey="key" 
	        value="key " 
	        readonly="false" 
	        theme="css_nonbreaking"
	        class="inputElement"/>
</td>
</tr>
</table>
