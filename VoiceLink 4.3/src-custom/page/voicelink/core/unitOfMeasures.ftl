<title><@s.text name='unitofmeasure.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
    <#include "/include/common/tablecomponent.ftl">

 	<@tablecomponent
 		varName="unitOfMeasureObj"
		columns=uomColumns
		tableId="unitOfMeasureTable"
		url="${pageContext}/unitOfMeasure"
		tableTitle='unitofmeasure.view.title'
		viewId=unitOfMeasureViewId
    	type=""
    	extraParams=extraParamsHash?default("") />
