<#include "/include/common/controlgrouping.ftl" />
<#assign readOnly="false">
<#assign disabled="false">
<#assign calendarButton>
    <@s.text name='report.button.showcalendar'/>
</#assign>
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='report.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='report.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='report.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='report.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='report.view.single.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>

<style type="text/css">

.reportForm table.wwctrlgrp_table tr td.wwlbl_td{
	width:125px;
}

.reportTypeSelect {
    width:200px;
}

.reportFormatSelect{
    width:60px;
}

.reportIntervalSelect{
    width:100px;
}

</style>

<body>
<div class="formBody">
<div class="titleBar">
	${title} 
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/report" action="${formName}.action" method="POST">
<#assign formReportId="">
<#if report.type?has_content>
    <#assign pid=report.type.id>
</#if>
<#if reportId?has_content>
    <@s.hidden name="reportId"/>
    <@s.hidden name="savedEntityKey"/>
    <#assign formReportId=reportId/>
</#if>

<script language="javascript">
	 connect(currentWindow(), 'onload', partial(changeParameters));
	//This is not a clean way to do this
	function changeParameters() {
	    var pid;
	    if ($('reportTypeId') == null) {
	    <#if pid?has_content>
	        pid = ${pid?c};
	    <#else>
	        pid = null;
	    </#if>
	        
	    } else {
	    	<#if formName = 'view'>
	    		pid = ${reportTypeId?c}
	    	<#else>
	    		pid = $('reportTypeId').options[$('reportTypeId').selectedIndex].value;
	    	</#if>
	    } 
	        
        <#if reportId?has_content>
            var reportId = ${reportId?c};
        <#else>
            var reportId = null;
        </#if>
        
<#-- Attempt at passing the error list to the parameters page --> 
        <#if fieldErrors?has_content>
        var fieldErr = '${fieldErrors}';
        </#if>   
        var params = {reportId: reportId,
                      reportTypeId: pid,
                      readOnly: ${readOnly}<#if fieldErrors?has_content>,
                      fieldErr: fieldErr</#if>};

        var url = "${base}/selection/report/getParameters.action";
        simpleXMLHttpRequestOutput=null;
        simpleXMLHttpRequestOutput=doSimpleXMLHttpRequest(url, params).addCallbacks(handleFunc,handleFunc2);           
    }

    
    function handleFunc (response) {
        $('parameterDiv').innerHTML = response.responseText;
        <#if formName != 'view'>
        connectSubmit();
        </#if>
        fixupParameterControls();
    }
  
    function handleFunc2 () {
        log('error');
    }

	<#-- For edits; allow control enable/disabling to be done as needed. -->
	function fixupParameterControls() {
        var reportTypeId;
        <#if formName != 'view'>
        reportTypeId = $('reportTypeId').options[$('reportTypeId').selectedIndex].value;
        <#else>
        reportTypeId = '${reportTypeId?c}';
        </#if>
        switch(reportTypeId) 
        {
        	<#-- Labor summary report -->
        	case '-700':
        		runScript('reportTypeParam-706');
        		break;
        	<#-- Labor detail report -->
        	case '-701':
        		runScript('reportTypeParam-716');
        		break;
        	case '-1200':
        	    runScript('reportTypeParam-1205');
        	    break;
        	default:
        		break;
        }

	}	
	
	<#-- All reportParameter drop down controls will call back to this     -->
	<#-- when onchange occurs.                                             -->
	<#-- reportParamId - id of control that generated the call.            -->
	function runScript(reportParamId) {
		var element = document.getElementById(reportParamId);
		switch(reportParamId) {
			<#-- Labor Summary Report On drop down -->
			case 'reportTypeParam-706':
		    	<#-- if Replenishment or Replenishment w/Selection then disable
		    		 Selection Region dropdown. -->
				if (element.value == 'SELECTION_ONLY') {
					document.getElementById('reportTypeParam-707').disabled = false;
				} else {
					document.getElementById('reportTypeParam-707').selectedIndex = 0;
					document.getElementById('reportTypeParam-707').disabled = true;
				}
				break;
			<#-- Labor Detail Report On drop down -->
			case 'reportTypeParam-716':
		    	<#-- if Replenishment or Replenishment w/Selection then disable
		    		 Selection Region dropdown. -->
				if (element.value == 'SELECTION_ONLY') {
					document.getElementById('reportTypeParam-717').disabled = false;
				} else {
					document.getElementById('reportTypeParam-717').selectedIndex = 0;
					document.getElementById('reportTypeParam-717').disabled = true;
				}
				break;
		    case 'reportTypeParam-1205':
		        <#if formName != 'view'>
		        <#-- make an async call to get the regions for this action type-->	
		        <#if reportId?has_content>
                    var reportId = ${reportId?c};
                <#else>
                    var reportId = null;
                </#if>	       
		        var params = {actionId: element.value,
		                      reportId: reportId};
                var url = "${base}/selection/report/getPerfReportRegions.action";
                simpleXMLHttpRequestOutput=null;
                simpleXMLHttpRequestOutput=doSimpleXMLHttpRequest(url, params).addCallbacks(changeOpPerfRegions,handleFunc2);
                <#else>
                
                </#if>
		        break;
			default:
				break;
		}
	}
	
	function changeOpPerfRegions(request) {
	    var json = eval('(' + request.responseText + ')');
        var regionSelect = document.getElementById('reportTypeParam-1206');
        regionSelect.options.length=0;
        regionSelect.options[0] = new Option('--All--', '_ALL_', true, true);
        var i = 1;
        var defaultValue = null;
        for (obj in json) {
           if (obj == "DEFAULT") {
               defaultValue = json[obj];
           }        
        }
        for (obj in json) {
           if (obj != "DEFAULT") {
               if (defaultValue == obj) {               
                   regionSelect.options[i] = new Option(json[obj],obj,true,true);
               } else {
                   regionSelect.options[i] = new Option(json[obj],obj,false,false);               
               }
               i++;
           }        
        }
	}
		
</script>

<div class="formTop reportForm">
		<@s.textfield tabindex="1" 
	    	label="%{getText('report.label.name')}" 
	    	name="report.name" 
	    	id="report.name" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	required="true" 
	    	readonly="${readOnly}"/>

	    <@s.select tabindex="2"
            theme="css_xhtml"
            label="%{getText('report.label.type')}" 
	        name="reportTypeId" 
		    id="reportTypeId" 
		    list="typeMap" 
		    listKey="value" 
		    listValue="key"
		    emptyOption="false"
		    readonly="${readOnly}"
		    cssClass="reportTypeSelect" 
		    multiple="false" 
		    onchange="changeParameters();"
		    width="500"/>

	    <@s.select tabindex="3"
            theme="css_xhtml"
            label="%{getText('report.label.format')}" 
	        name="format" 
		    id="format" 
		    list="formatMap" 
		    listKey="value" 
		    listValue="key"
		    emptyOption="false"
		    readonly="${readOnly}"
		    cssClass="reportFormatSelect" 
		    multiple="false" 
		    width="150" />
	    
	    <@s.select tabindex="2"
            theme="css_xhtml"
            label="%{getText('report.label.interval')}" 
	        name="interval" 
		    id="interval" 
		    list="intervalMap" 
		    listKey="key" 
		    listValue="value"
		    emptyOption="false"
		    readonly="${readOnly}"
		    cssClass="reportIntervalSelect" 
		    multiple="false" 
		    width="150"/>
		    
    
</div>
<div class="formMiddle">
  		<@controlgroup groupId="reportParameters" caption="report.caption.parameters" disabled="false">
  		<div id="parameterDiv" class="reportForm">
        </div>
  		</@controlgroup>		    
</div>
<div class="formAction">
	
	<#if reportId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='report.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('report.create.label.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('report.create.label.name')}" name="modifiedEntity.name"/>
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>


</body>
</html>

