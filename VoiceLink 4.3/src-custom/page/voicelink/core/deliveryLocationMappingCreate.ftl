		

<#assign formName="create">
<#assign submitValue>
	<@s.text name='deliveryLocationMapping.create.button.submit'/>
</#assign>	
<#assign title>
	<@s.text name='deliveryLocationMapping.create.title'/>
</#assign>	
<#assign addAnotherMapping>
	<@s.text name="deliveryLocationMapping.create.button.addAnotherMapping"/>
</#assign>
<#assign remove>
     <@s.text name="deliveryLocationMapping.create.button.remove"/>
</#assign>
<#assign pageContext="${base}/${navigation.applicationMenu}">
<title>${title}</title>
<@s.form name="${formName}" id="form1" validate="true" namespaceVal="${pageContext}/deliveryLocationMapping" action="${formName}.action" method="GET">
<@s.hidden name="numberOfMappings" id="numberOfMappings" />
<@s.hidden name="deliveryLocationMappingTypeId" id="deliveryLocationMappingTypeId" />


<script language="javascript">
	var count = ${numberOfMappings};

	function addAddButton() 
	{
		var linkAdd = A({'id':'mappingAddButton','class':'anchorButton', 'href':'javascript:addMappingRule()'},'${addAnotherMapping}');
		var rows = [
		   ['','', linkAdd]
		];		
		row_display = function (row) {
		    return TR({'id':'mappingRowAddButton','height':'25'}, map(partial(TD, {'valign':'middle','class':'td'}), row));
		}			
		var addButtonRow = map(row_display, rows);
		var mappingTable = document.getElementById('mappingTable');	
	    appendChildNodes(mappingTable,  TBODY(null,addButtonRow));
     
	}
	
	function removeAddButton()
	{
		var linkAdd = document.getElementById('mappingRowAddButton');
		if(linkAdd != null) { 
			removeElement(linkAdd);
		}
	}
	
	function addMappingRule()
	{
		removeAddButton();
		var lblCustomer = LABEL({'class':'label'}, "Customer Number:");
		var lblDeliveryLocation = LABEL({'class':'label'}, "Delivery Location:");
		var txtCustomer = INPUT({'id':'deliveryLocationMappings[' + count + '].mappingValue',
		                  'name':'deliveryLocationMappings[' + count + '].mappingValue',
		                  'type':'text','tabindex':+(count + 1),'size':'5','maxlength':'50'});
		var txtDeliveryLocation = INPUT({'id':'deliveryLocationMappings[' + count + '].deliveryLocation',
		                        'name':'deliveryLocationMappings[' + count + '].deliveryLocation',
		                        'type':'text','tabindex':+(count + 1),'size':'5','maxlength':'9'});
		var linkRemove = A({'class':'anchorButton','href':'javascript:removeMappingRule(' + count + ')'}, '${remove}');
		var rows = [
		    [txtCustomer, txtDeliveryLocation, linkRemove]
		];		
		row_display = function (row) {
		    return TR({'height':'25','id':'mappingRow' + count}, map(partial(TD, {'valign':'middle','class':'td'}), row));
		}		
		var container = document.getElementById('mappingContainer');
		var mappingTable = document.getElementById('mappingTable');
		
		var mappingRow = map(row_display, rows);
		appendChildNodes(mappingTable, TBODY(null,mappingRow));

		var numberOfMappings = document.getElementById('numberOfMappings');
		
		addAddButton();
		count++;			
		numberOfMappings.value = count;
		
	}
	
	function removeMappingRule(id)
	{
	
	  var mappingRow = document.getElementById('mappingRow' + id);
	    if (count > 1){
		 count--;
		 reArrangeIndexes(id);
		 }
	}
	
	function reArrangeIndexes(id) {
		     
     for (i = id+1; i <= count; i++ ) {
       document.getElementById('deliveryLocationMappings[' + (i-1) + '].deliveryLocation').value = document.getElementById('deliveryLocationMappings[' + i + '].deliveryLocation').value;
       document.getElementById('deliveryLocationMappings[' + (i-1) + '].mappingValue').value = document.getElementById('deliveryLocationMappings[' + i + '].mappingValue').value;
       // delete rows..
     }
     var mappingRow = document.getElementById('mappingRow' + count);
     removeElement(mappingRow);
	 
	 var numberOfMappings = document.getElementById('numberOfMappings');
	 numberOfMappings.value = count;
	}
	
	
	
  function addMappingRuleReCreate(i,mapvalue,delLoc){

	var txtCustomer;
	var txtDeliveryLocation;
	removeAddButton();
		var newIndx = i+1;
		if (mapvalue.length == 0){
		  
			 txtCustomer = INPUT({'id':'deliveryLocationMappings[' + i + '].mappingValue',
                     'name':'deliveryLocationMappings[' + i + '].mappingValue',
                     'type':'text','tabindex':newIndx,'size':'5','maxlength':'50'});
		
		}else {
				 txtCustomer = INPUT({'id':'deliveryLocationMappings[' + i + '].mappingValue',
		                         'name':'deliveryLocationMappings[' + i + '].mappingValue',
		                         'value':mapvalue,'tabindex':newIndx,
		                         'type':'text','size':'5','maxlength':'50'});
		}

       if (delLoc.length == 0){
   		      txtDeliveryLocation = INPUT({'id':'deliveryLocationMappings[' + i + '].deliveryLocation',
	                          'name':'deliveryLocationMappings[' + i + '].deliveryLocation',
	                           'type':'text','tabindex':newIndx,'size':'5','maxlength':'9'});
       }else {
          		  txtDeliveryLocation = INPUT({'id':'deliveryLocationMappings[' + i + '].deliveryLocation',
	                                 'name':'deliveryLocationMappings[' + i + '].deliveryLocation',
	                                 'value':delLoc,'tabindex':newIndx,
	                                 'type':'text','size':'5','maxlength':'9'});
       }
		                                 
		var linkRemove = A({'class':'anchorButton','href':'javascript:removeMappingRule(' + i + ')'}, '${remove}');
		var rows = [
		    [txtCustomer, txtDeliveryLocation, linkRemove]
		];		
		row_display = function (row) {
		    return TR({'height':'25','id':'mappingRow' + i}, map(partial(TD, {'valign':'middle','class':'td'}), row));
		}		
		var container = document.getElementById('mappingContainer');
		var mappingTable = document.getElementById('mappingTable');
		
		var mappingRow = map(row_display, rows);
		appendChildNodes(mappingTable, TBODY(null,mappingRow));

		addAddButton();

	
	}
</script>
<div class="formTop">
<table id="mappingTable"  >
	<tr height="25">
		<td class="labelRelative">
			<br>
		
			<@s.text name="%{getText('${currentMappingTypeResourceKey}')}" />
			&nbsp;&nbsp;&nbsp;&nbsp;

		</td>
		<td class="labelRelative">
			<br>
			<@s.text name="%{getText('deliveryLocationMapping.label.deliveryLocation')}" />						
		</td>
	</tr>
<#list 0..numberOfMappings - 1 as x>

	<tr id="mappingRow${x}" height="25">
		<td>
		       <@s.textfield tabindex="1" 
    			   label="Customer Number" 
    			   name="deliveryLocationMappings[${x}].mappingValue" 
    			   id="deliveryLocationMappings[${x}].mappingValue" 
    			   size="5" maxlength="50" 
    			   show="true" 
    			   theme="simple"
    			    />	
    			    &nbsp;&nbsp;&nbsp;&nbsp;
    	</td>
    	<td>
    	       <@s.textfield tabindex="1" 
    			   label="Delivery Location" 
    			   name="deliveryLocationMappings[${x}].deliveryLocation" 
    			   id="deliveryLocationMappings[${x}].deliveryLocation" 
    			   size="5" maxlength="9" 
    			   show="true" 
    			   theme="simple"
    			    />	    			 
    	</td>
		<td style="width:200";>
			<a class="anchorButton" href="javascript:removeMappingRule(${x})">${remove}</a>
		</td>
		<td class="listErrorMessage">
			<#if fieldErrors['deliveryLocationMappings[${x}]']?exists>
			<#assign loopCount = 0>
			<#list fieldErrors['deliveryLocationMappings[${x}]'] as error ><#if 0 < loopCount  >, </#if>			
			${error}<#assign loopCount = (loopCount + 1)></#list>		
			</#if>
		</td>
	</tr>

</#list>
</table>

</div>
<div class="formAction">
</div>
<#include "/include/form/buttonbar.ftl">
<script for="window" event="onload" language="javascript">
	var numberOfMappings = document.getElementById('numberOfMappings');
	addAddButton();
</script>
</@s.form>
