<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='reasoncode.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='reasoncode.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='reasoncode.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='reasoncode.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='reasoncode.view.single.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>
<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/selection/reasonCodes" action="${formName}.action" method="POST">
<#if reasonCodeId?has_content>
    <@s.hidden name="reasonCodeId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>

<div class="formTop">

                <#if readOnly="true" >
                  <#if reasonCode.taskFunctionType=="NormalAndChaseAssignments">
                    <#assign reasonCodeTaskFunction>
                      <@s.text name='com.vocollect.voicelink.core.model.TaskFunctionType.alternative.6'/>
                    </#assign> 
                   <#else>
                      <#assign reasonCodeTaskFunction=taskFunction>
                   </#if>
                   <@s.label label="%{getText('reasoncode.create.label.function')}" name="taskFunction" id="reasonCode.taskFunctionType" value="${reasonCodeTaskFunction}" size="25" maxlength="128" />
	            <#else>
	          
	              <@s.select
	               tabindex="1" 
	               label="%{getText('reasoncode.create.label.function')}" 
	               name="taskApplicationStatus" 
	               id="taskApplicationStatus" 
	               list="functionMap" 
	               disabled="${disabled}" 
                   cssClass="functionSelect" 
	               readonly="${readOnly}"/>
	              
	            </#if> 
	               
               <@s.textfield
                   tabindex="2"
                   label="%{getText('reasoncode.create.label.number')}" 
                   name="reasonCode.reasonNumber" 
                   id="reasonCode.reasonNumber" 
    	           size="25" 
    	           maxlength="9" 
    	           disabled="${disabled}" 
    	           required="true" 
    	           readonly="${readOnly}"/>
	
    			<#if readOnly=="true">
	          		<@s.textfield 
	               		tabindex="3" 
	               		label="%{getText('reasoncode.create.label.description')}" 
	               		name="translatedReasonDescription" 
	               		id="reasonCode.reasonDescription" 
		           		size="25"
		           		maxlength="50" 
		           		show="true" 
		           		required="true"
		           		readonly="${readOnly}"/>	
				<#else>
	          		<@s.textfield 
	               		tabindex="3" 
	               		label="%{getText('reasoncode.create.label.description')}" 
	               		name="reasonCode.reasonDescription" 
	               		id="reasonCode.reasonDescription" 
		           		size="25"
		           		maxlength="50" 
		           		show="true" 
		           		required="true"
		           		readonly="${readOnly}"/>	
    				<#if translatedReasonDescriptionOrNull?has_content>	
        				<@s.label label="" name="translatedReasonDescriptionMessage" id="reasonConde.reasonDescription.translation"/>
     				</#if>	
				</#if>
	    	 						
</div>
<div class="formAction">
	
	<#if reasonCodeId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>

<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="display:hidden;">
	<div class="modifiedTitle">
		<@s.text name='reasoncode.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('reasoncode.create.label.number')}" name="modifiedEntity.reasonNumber"/>
	    <@s.label label="%{getText('reasoncode.create.label.function')}" name="modifiedEntity.taskFunctionType"/>
	    <@s.label label="%{getText('reasoncode.create.label.description')}" name="modifiedEntity.reasonDescription"/>
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>

