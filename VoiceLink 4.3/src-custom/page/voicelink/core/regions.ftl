<title><@s.text name='region.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">
 	<@tablecomponent
 		varName="regionObj"
		columns=regionColumns
		tableId="listRegionTable"
		url="${base}/selection/region"
		tableTitle='region.view.title' 
		viewId=regionViewId
    	type=""
    	extraParams=extraParamsHash?default("") />     			
		
