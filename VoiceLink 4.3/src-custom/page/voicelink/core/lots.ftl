<title><@s.text name='lot.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
	<#assign pageContext="${base}/${navigation.applicationMenu}">

 	<@tablecomponent
 		varName="lotObj"
		columns=lotColumns
		tableId="listLotsTable"
		url="${pageContext}/lot"
		tableTitle='lot.view.title' 
		viewId=lotViewId
    	type=""
    	extraParams=extraParamsHash?default("") />