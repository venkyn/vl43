<title><@s.text name='shift.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
	<#include "/include/common/tablecomponent.ftl">	
	
 	<@tablecomponent
 		varName="shiftObj"
		columns=shiftColumns
		tableId="listShiftsTable"
		url="${pageContext}/shift"
		tableTitle='shift.view.title' 
		viewId=shiftViewId
    	type=""
    	extraParams=extraParamsHash?default("") />     					
