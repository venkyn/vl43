<title><@s.text name='assignment.resequence.view.title'/></title>
    <#include "/include/common/tablecomponent.ftl">
    <#assign pageContext = "${base}/${navigation.applicationMenu}">
    <@s.hidden name="savedEntityKey"/>
    <@s.hidden name="regionId"/>
    <#assign urlMethod="getResequenceAssignmentData"/>
    <@tablecomponent
        varName="assignmentObj"
        columns=resequenceAssignmentColumns
        tableId="listAssignmentsTable"
        url="${pageContext}/assignment"
        tableTitle='assignment.resequence.view.title'
        viewId=resequenceViewId
        type=""
        extraParams={"extraURLParams":[{"name":"regionId","value":"${regionId?c}"},{"name":"savedEntityKey","value":"${savedEntityKey}"}]} />