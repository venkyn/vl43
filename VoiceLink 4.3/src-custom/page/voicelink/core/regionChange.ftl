<HTML>
<HEAD>
</HEAD>
<BODY>

  <#if !pageNumber?has_content>
	  <#assign pageNumber=1>
  </#if>

<#if pageNumber = 1>
    <div class="formTop" style="width: 50%;">
    <@s.select tabindex="3" 
        label="%{getText('region.label.normalProfile')}"
    	name="selectionNormalProfileId"
    	id="selectionNormalProfileId"
    	list="profileMap"
    	readonly="false" 
    	cssClass="profileSelect"
    	onchange="changeProfile('selectionNormalProfileId');"/>
    </div>

<#if readOnly == "true">
<script type="text/javascript">
        $('selectionNormalProfileId').disabled = "true";
</script>
    <@s.hidden name="selectionNormalProfileId" value="${selectionNormalProfileId}"/>
</#if>
    
    <@s.hidden name="selectionChaseProfileId"/>
    <@s.hidden name="chaseSummaryPromptId"/>
    
    <script type="text/javascript">
    connect(currentWindow(), 'onload', partial(changeProfile,'selectionNormalProfileId'));   
    </script>
    
  <div id="profileDiv">
  </div>

<#elseif pageNumber = 2>      

    <div class="formTop" style="width: 50%;">
    <@s.select tabindex="3" 
        label="%{getText('region.label.chaseProfile')}"
    	name="selectionChaseProfileId"
    	id="selectionChaseProfileId"
    	list="profileMap"
    	readonly="false" 
    	cssClass="profileSelect"
    	onchange="changeProfile('selectionChaseProfileId');"/>
    </div>
    
<#if readOnly == "true">
<script type="text/javascript">
           $('selectionChaseProfileId').disabled = "true";
</script>
    <@s.hidden name="selectionChaseProfileId" value="${selectionChaseProfileId}"/>
</#if>

    <@s.hidden name="selectionNormalProfileId"/>
    <@s.hidden name="normalSummaryPromptId"/>
    
    <script type="text/javascript">
    connect(currentWindow(), 'onload', partial(changeProfile,'selectionChaseProfileId'));
    </script>
    
  <div id="profileDiv">
  </div>
<#else>

<#assign editDisabled="false">
<#if navigation.pageName="edit">
    <#assign editDisabled="true">
</#if>

      <div class="formTop">
    	<#if readOnly=="true">
         	<@s.textfield 
              tabindex="1" 
              label="%{getText('region.create.label.name')}" 
              name="translatedRegionName" 
              id="name" 
              size="25" 
              maxlength="50" 
              required="true" 
              readonly="${readOnly}"/>
		<#else>
         	<@s.textfield 
              tabindex="1" 
              label="%{getText('region.create.label.name')}" 
              name="selectionRegion.name" 
              id="name" 
              size="25" 
              maxlength="50" 
              required="true" 
              readonly="${readOnly}"/>
    		<#if translatedRegionNameOrNull?has_content>	
        		<@s.label label="" name="translatedRegionNameMessage" id="selectionRegion.name.translation"/>
     		</#if>	
		</#if>
              
	      <@s.textfield 
	          tabindex="2" 
	          label="%{getText('region.create.label.number')}" 
	          name="selectionRegion.number" 
	          id="number" 
	          size="5" 
	          maxlength="9" 
	          required="true" 
	          disabled="${editDisabled}"
	          readonly="${readOnly}"/>
	          
    	<#if readOnly=="true">
	      	<@s.textfield 
	          tabindex="3" 
	          label="%{getText('region.create.label.description')}" 
	          name="translatedRegionDescription" 
	          id="description" 
	          size="25" 
	          maxlength="50" 
	          required="false" 
	          readonly="${readOnly}"/>
		<#else>
	      	<@s.textfield 
	          tabindex="3" 
	          label="%{getText('region.create.label.description')}" 
	          name="selectionRegion.description" 
	          id="description" 
	          size="25" 
	          maxlength="50" 
	          required="false" 
	          readonly="${readOnly}"/>
    		<#if translatedRegionDescriptionOrNull?has_content>	
        		<@s.label label="" name="translatedRegionDescriptionMessage" id="selectionRegion.description.translation"/>
     		</#if>	
		</#if>
	          
	      <@s.textfield 
    	      tabindex="4" 
    	      label="%{getText('region.create.label.goalRate')}" 
    	      name="selectionRegion.goalRate" 
    	      id="goalRate" 
    	      size="5" 
    	      maxlength="5" 
    	      required="true" 
    	      readonly="${readOnly}"/>
	</div>
      <div class="formMiddle">
      <br>
      
          <@s.textfield
              tabindex="1" 
              label="%{getText('region.label.normalProfile')}" 
              name="NormalProfileId" 
              value ="%{getText('profile.id.${selectionRegion.profileNormalAssignment.number}')}"
              id="name" 
              size="25" 
              maxlength="128" 
              required="false" 
              readonly="true"/>
              
          <@s.textfield
              tabindex="1" 
              label="%{getText('region.label.chaseProfile')}" 
              name="ChaseProfileId" 
              id="name" 
              value="%{getText('profile.id.${selectionRegion.profileChaseAssignment.number}')}"
              size="25" 
              maxlength="128" 
              required="false" 
              readonly="true"/>
        </div>
   	
      
      <@s.hidden name="selectionChaseProfileId"/>
      <@s.hidden name="selectionNormalProfileId"/>
      <@s.hidden name="chaseSummaryPromptId"/>      
      <@s.hidden name="normalSummaryPromptId"/>      

</#if>

    
  <#if !nextValue?has_content>
	<#assign nextValue>
		<@s.text name="form.button.next"/>
	</#assign>
  </#if>
  
  <#if !previousValue?has_content>
	<#assign previousValue>
		<@s.text name="form.button.previous"/>
	</#assign>
  </#if>
  
  <#if !cancelValue?has_content>
	<#assign cancelValue>
		<@s.text name="form.button.cancel"/>
	</#assign>
  </#if>
  
  <#if navigation.pageName="edit">
    <#if !saveValue?has_content>
	  <#assign saveValue>
		  <@s.text name="region.edit.button.submit"/>
	  </#assign>  
    </#if>
  <#else>
    <#if !saveValue?has_content>
	  <#assign saveValue>
		  <@s.text name="region.create.button.submit"/>
	  </#assign>  
    </#if>
   </#if>  
  
  <br>
  

<#if !(pageNumber = 1)>  
	<a
	href="javascript:prevForm();"
	id="${pageNumber}.prev"
	class="${cancelCssClass?default('anchorButton')}"
	tabindex="-1}"
	>${previousValue}</a>
</#if>

<#if !(pageNumber = 3)>  
    <a
	href="javascript:nextForm();"
	id="${pageNumber}.next"
	class="${cancelCssClass?default('anchorButton')}"
	tabindex="-1"
	>${nextValue}</a>
</#if>
<#if (pageNumber = 3) && navigation.pageName!="viewRegion">  
    <a
	href="javascript:saveForm();"
	id="${pageNumber}.save"
	class="${cancelCssClass?default('anchorButton')}"
	tabindex="-1"
	>${saveValue}</a>
</#if>
    <a
	href="javascript:modifySubmitVariable('form1','submitTypeElement','method:cancel');"
	id="${pageNumber}.cancel"
	class="${cancelCssClass?default('anchorButton')}"
	tabindex="-1"
	>${cancelValue}</a>


<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='region.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	      <@s.label label="%{getText('region.create.label.name')}" name="modifiedEntity.name"/>
   	      <@s.label label="%{getText('region.create.label.number')}" name="modifiedEntity.number"/>
	      <@s.label label="%{getText('region.create.label.description')}" name="modifiedEntity.description"/>
	      <@s.label label="%{getText('region.create.label.goalRate')}" name="modifiedEntity.goalRate"/>
	</div>	
</div>

<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
	
	
</script>
</#if>
</BODY>
</HTML>
