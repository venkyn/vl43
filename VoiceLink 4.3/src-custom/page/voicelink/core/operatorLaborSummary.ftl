<title><@s.text name='operatorLaborSummary.view.title' /></title>
	<#include "/include/common/tablecomponent.ftl">
	<#assign pageContext="${base}/${navigation.applicationMenu}">

    <#assign timeFilterSaved = timeWindow/>

      <#assign urlMethod="getOperatorLaborSummaryData"/>
 	  <@tablecomponent
 		varName="operatorLaborObj"
		columns=operatorSummaryColumns
		tableId="operatorLaborTable"
		url="${pageContext}/labor"
		tableTitle='operatorLaborSummary.view.title' 
    	viewId=operatorLaborSummaryViewId
    	type="table_time"
    	extraParams=extraParamsHash?default("") />
    	
    <script type="text/javascript">
        function buildGetDataParameters(operatorLaborTableObj) {
        
            var operators = new Array();
            var regions = new Array();
            var filterTypes = new Array();
        
            var timeWindow = operatorLaborTableObj.time_window_module.getTimeFilter();
            var tableSelection = operatorLaborTableObj.getSelectedObjects();
        
            for (i in tableSelection) {
                var obj = tableSelection[i];
        
                // Operator ID required
                operators.push(obj.operator.id);
            
                // Include the region of the selected row(s)
                if (obj.region.id == null || obj.region.id == "") {
                    regions.push("null");
                } else {
                    regions.push(obj.region.id);
                }
            
                // Include the filtertype of the selected row(s)
                filterTypes.push(obj.filterTypeAsInt);
            }
            return {
                'operatorIds' : operators,
                'regionIds' : regions,
                'filterTypes' : filterTypes,
                'timeWindow' : timeWindow
            };
        }
    </script>
    
    <#assign urlMethod="getOperatorLaborHistoryData"/>
    <@tablecomponent
 		varName="laborHistoryObj"
		columns=operatorHistoryColumns
		tableId="laborHistoryTable"
		url="${pageContext}/labor"
		tableTitle='laborDetailSummary.view.title'
    	viewId=operatorLaborHistoryViewId
    	type=""
    	extraParams={"publishers":[{"name" : "operatorLaborObj", "selectorFunction": "buildGetDataParameters"}]} />
 
