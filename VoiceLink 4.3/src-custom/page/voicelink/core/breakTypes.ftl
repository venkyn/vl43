<title><@s.text name='breakType.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
	<#assign pageContext="${base}/${navigation.applicationMenu}">

 	<@tablecomponent
 		varName="breakTypeObj"
		columns=breakTypeColumns
		tableId="listBreakTypesTable"
		url="${pageContext}/breakType"
		tableTitle='breakType.view.title' 
		viewId=breakTypeViewId
    	type=""
    	extraParams=extraParamsHash?default("") />
		
