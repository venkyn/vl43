<#if navigation.pageName="edit">
	<#assign title>
    	<@s.text name='print.edit.title'/>
	</#assign>
	<#assign formName="edit">
	<#assign readOnly="false">
	<#assign disabled="false">
	<#assign submitValue>
	    <@s.text name='printer.edit.button.submit'/>
	</#assign>
<#elseif navigation.pageName="create">
	<#assign title>
    	<@s.text name='print.create.title'/>
	</#assign>
	<#assign formName="create">
	<#assign readOnly="false">
	<#assign disabled="false">
	<#assign submitValue>
	    <@s.text name='printer.create.button.submit'/>
	</#assign>
<#elseif navigation.pageName="view">
	<#assign title>
    	<@s.text name='print.view.title.readOnly'/>
	</#assign>
	<#assign formName="view">
	<#assign readOnly="true">
	<#assign disabled="false">
	<#assign submitValue>
	    <@s.text name='printer.view.button.submit'/>
	</#assign>
</#if>
<#assign pageContext="${base}/${navigation.applicationMenu}">
 <html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	   ${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/print" action="${formName}.action" method="POST">
<#if printerId?has_content>
    <@s.hidden name="printerId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>

<div class="formTop">
			   <!-- printer.number start -->
               <@s.textfield
                   tabindex="1"
                   label="%{getText('print.create.label.printer.number')}" 
                   name="printer.number" 
                   id="printer.number" 
    	           size="25" 
    	           maxlength="2" 
    	           disabled="${disabled}" 
    	           required="true" 
    	           readonly="${readOnly}"/><!-- printer.number end -->
    	    <!-- printer.name start -->
			<#if (formName=="edit") || (formName=="view")>
	          <@s.textfield 
	               tabindex="2" 
	               label="%{getText('print.create.label.printer.name')}" 
	               name="printer.name" 
	               id="printer.name" 
		           size="25"
		           maxlength="50" 
		           show="true" 
		           disabled="${disabled}" 
		           readonly="true"/>	
	  		<#elseif (formName=="create")>
	  			<@s.select 
	  			   tabindex="2"
	  			   label="%{getText('print.create.label.printer.name')}"
	  			   id="printer.name" 
	  			   name="printer.name" 
	  			   list="printerList" 
	  			   listKey="name" 
	  			   listValue="name" 
				   multiple="false" 
				   readonly="${readOnly}" 
				   cssClass="site" />
			</#if>
			<!-- printer.name end -->
			<!-- printer.status start -->
	          <@s.select
	              tabindex="3" 
	              label="%{getText('print.create.label.printer.status')}" 
	              name="printer.isEnabled" 
	              id="printer.status" 
	              list="statusMap" 
	              cssClass="site" 
	              readonly="${readOnly}"
	              disabled="${disabled}" />
	        <!-- printer.status end -->		
	          
	     			
</div>
<div class="formAction">
	<#assign submitIndex=199>
	<#assign cancelIndex=200>
	<#if formName!="view">
		<#include "/include/form/buttonbar.ftl">
	</#if>
</div>

<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('print.create.label.printer.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('print.create.label.printer.name')}" name="modifiedEntity.name"/>
	    
	    <@s.label label="%{getText('print.create.label.printer.status')}" name="modifiedEntity.isEnabled"
	     trueValue="print.view.column.status.true" falseValue="print.view.column.status.false" />
  
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
