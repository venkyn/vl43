

<title><@s.text name='deliveryLocationMapping.view.title'/></title>
<#include "/include/common/tablecomponent.ftl">	
<#assign pageContext="${base}/${navigation.applicationMenu}">


 	<@tablecomponent
 		varName="deliveryLocationMappingObj"
		columns=deliveryLocationMappingColumns
		tableId="listDeliveryLocationMappingsTable"
		url="${pageContext}/deliveryLocationMapping"
		tableTitle='deliveryLocationMapping.view.title' 
		viewId=deliveryLocationMappingViewId
    	type=""
    	extraParams=extraParamsHash?default("") />
