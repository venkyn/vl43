<#assign readOnly="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='workgroup.edit.title'/>    
  </#assign>
  <#assign formName="edit">
  <#assign functions=allWorkgroupFunctions>
  <#assign existingFunctions=existingWorkgroupFunctions>
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='workgroup.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='workgroup.create.button.submit'/>
  </#assign>
  <#assign functions=allWorkgroupFunctions>
  <#assign existingFunctions=existingWorkgroupFunctions>
<#else>  
  <#assign title>
    <@s.text name='workgroup.view.title.readOnly'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
  <#assign functions=sortedWorkgroupFunctions>
</#if>

<#assign collapsibleIdPrefix="fn" />

<html>
<head>
  <title>${title}</title>
</head>
<body>

  <#-- This stuff should go in the <head> once sitemesh is fixed -->
  <style type="text/css">
  div.collapsibleGroup {
    padding: 2px;
  }
  div.collapsibleHeading {
    font-size: 12px;
    font-weight: normal;
    margin-bottom: 5px;
	display: inline;
  }
  div.collapsibleHeading img {
    cursor: pointer;
  }
  div.collapsibleList {
    margin-left: 30px;;
	display: none;
  }
  div.collapsibleList table tr {
    height: 16px;
  }
  div.collapsibleList table tr td {
    padding: 0px;
  }
  div.collapsibleLabel {
    display: inline;
    margin-left: 11px;
    font-size: 11px;
  }
  div.collapsibleGroup input.checkbox {
    margin: 0px;
  }
  div.collapsibleHeading input.checkbox {
    margin-right: 5px;
  }
  </style>
  
  <script type="text/javascript">
  <#-- This should also go in the <head> -->
  var selectedAmounts = new Object();
  var groupTotals = new Object();
  <#list functions as function>
    <#assign currentId="${collapsibleIdPrefix}-${function_index}" />
    <#if function.regions?has_content>
      selectedAmounts['${currentId}'] = 0;
      groupTotals['${currentId}'] = ${function.regions?size};
    <#else>
      selectedAmounts['${currentId}'] = 0;
      groupTotals['${currentId}'] = 0;
    </#if>
  </#list>
  
  /**
   * toggle display of the groupings based on a user clicking the arrow
   * @param id The ID of the div containing the grouping.
   *
   * NOTE: no style is associated with the class "shown". this is just 
   * to keep the state of the collapsible group.
   */
  function toggleCollapsibleGroup(id) {
    var element = $(id);
	var icon = $(id+"-icon");
	var heading = $(id+"-heading");
    if (!hasElementClass(element, "shown")) {
      addElementClass(element, "shown");
      showElement(element);
	  icon.src = "${base}/images/minus.gif";
    } else {
      removeElementClass(element, "shown");
      hideElement(element);
      icon.src = "${base}/images/plus.gif";
	}
  }

  /**
   * Updates the amount indicated in the heading of a group.
   * @param checked The state of the box that was checked.
   * @param id The ID of the div containing the grouping.
   */
  function updateSelectedAmount(checked, id) {
    var allBox = $(id+"-all");
    var heading = $(id+"-heading");
    if (checked) {
      selectedAmounts[id]++;
      if (selectedAmounts[id] >= 1) {
        updateHeading(true, id);
      }
      if (selectedAmounts[id] == groupTotals[id]) {
        allBox.checked = true;
      }
    } else {
      selectedAmounts[id]--;
      if (selectedAmounts[id] == 0) {
        updateHeading(false, id);
      }
      allBox.checked = false;
    }
    replaceChildNodes($(id+"-count"), selectedAmounts[id]);
  }
  
  /**
   * Updater for the check-all checkbox.
   * @param checked The state of the box that was checked.
   * @param id The ID of the div containing the grouping.
   */
  function updateSelectedAmountAll(checked, id) {
    var boxValue = false;
    if (checked) {
      boxValue = true;
      selectedAmounts[id] = groupTotals[id];
    } else {
      selectedAmounts[id] = 0;
    }
    updateHeading(checked, id);
    var i = 0;
    var subBox = $(id+"-"+i);
    while (subBox != null) {
      subBox.checked = boxValue;
      var subBox = $(id+"-"+(++i));
    }
    replaceChildNodes($(id+"-count"), selectedAmounts[id]);
  }
  
  /**
   * Basically just swaps the boldness of the heading.
   * @param active If true, bold the heading, else, make the heading normal;
   * @param id The ID of the grouping.
   */
  function updateHeading(active, id) {
    var heading = $(id+"-heading");
    if (active) {
      heading.style.fontWeight = "bold";
    } else {
      heading.style.fontWeight = "normal";
    }
  }

  /**
   * Initializes the selections. Freemarker just marks the boxes as checked or 
   * not, so we have to check the state of all the boxes and use their onclick 
   * handlers to update the CSS styles and check-all box.
   */
  function initCheckboxes() {
    for (key in selectedAmounts) {
      if (groupTotals[key] == 0) {
        updateHeading($(key + "-all").checked, key);
      } else {
        for (var i = 0; i < groupTotals[key]; i++) {
          var box = $(key + "-" + i);
          if (box.checked) {
            selectedAmounts[key]++;
          }
        }
        selectedAmounts[key]--;
        updateSelectedAmount(true, key);
        if (selectedAmounts[key] == groupTotals[key]) {
          $(key + "-all").checked = true;
          updateHeading(true, key);
        }
      }
    }
  }
  </script>

  <div class="formBody">
  <div class="titleBar">${title}</div>

  <@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/workgroup" action="${formName}.action" method="POST">
  <#if workgroupId?has_content>
    <@s.hidden name="workgroupId" />
    <@s.hidden name="savedEntityKey" />
  </#if>
  <#assign tabIndex=1>
  <div class="formTop">
    <#if readOnly=="true">
    	<@s.textfield tabindex="${tabIndex}" label="%{getText('workgroup.create.label.name')}" 
      		name="translatedWorkgroupName" id="workgroup.groupName" size="25" 
      		maxlength="50" disabled="false" required="true" readonly="${readOnly}" />
	<#else>
    	<@s.textfield tabindex="${tabIndex}" label="%{getText('workgroup.create.label.name')}" 
      		name="workgroup.groupName" id="workgroup.groupName" size="25" 
      		maxlength="50" disabled="false" required="true" readonly="${readOnly}" />
    	<#if translatedWorkgroupNameOrNull?has_content>	
        	<@s.label label="" name="translatedWorkgroupNameMessage" id="workgroup.groupName.translation"/>
     	</#if>	
	</#if>
    <#if formName="edit" || formName="view">
      <@s.textfield label="%{getText('workgroup.view.label.operators')}"
        name="workgroup.operatorCount" id="workgroup.operatorCount" size="25"
        maxlength="50" disabled="true" required="false" readonly="true" />
    </#if>
  </div>
  <div class="formMiddle">
  
	      <div style="height: 17px;"></div>
          <div class="wwctrlgrp">
            <div class="wwlbl">
              <label class="label">
                <@s.text name="workgroup.create.label.regions" />
              </label>
            </div>
            <div class="wwctrl">
            <#assign tabIndex=tabIndex+1>
            <#if !functions?has_content>
              <@s.text name="workgroup.create.label.regions.none" />
            </#if>
            <#list functions as function>
              <#assign currentId="${collapsibleIdPrefix}-${function_index}" />
              <#if function.regions?has_content>
			      <div class="collapsibleGroup" tid="groupheader">
		            <div class="collapsibleHeading" id="${currentId}-heading">
	                  <img src="${base}/images/plus.gif" alt="open" id="${currentId}-icon" onclick="toggleCollapsibleGroup('${currentId}');" />
	                  <#if readOnly="false">
	                    <input tabIndex="${tabIndex}" class="checkbox" type="checkbox" id="${currentId}-all" onclick="updateSelectedAmountAll(this.checked, '${currentId}');" />
	                  </#if>
	                  <@s.text name="${function.taskFunction.functionType.resourceKey}"/>&nbsp;-<@s.text name="workgroup.regions.heading" />
	                  <#if readOnly="true">
	                    (${function.regions?size})
	                  <#else>
	                    (<span id="${currentId}-count">0</span>&nbsp;<@s.text name="workgroup.regions.heading.of" />&nbsp;${function.regions?size})
	                  </#if>
		            </div>
			        <div class="collapsibleList wwctrl" id="${currentId}">
			          <table>
	                  <#list function.regions as region>
	                    <#assign functionAndRegionIds="${function.taskFunction.id?c}_${region.id?c}" />
	                      <#-- This ID is used for testability -->
	                      <tr id="${functionAndRegionIds}_pair">
	                      <#if readOnly="false">
	                        <#assign tabIndex=tabIndex+1>
	                        <td><input tabIndex="${tabIndex}" class="checkbox" type="checkbox" name="functionsAndRegions" 
	                             value="${functionAndRegionIds}" id="${currentId}-${region_index}" onclick="updateSelectedAmount(this.checked, '${currentId}');" 
	                        <#if existingFunctions?has_content && existingFunctions?seq_contains(functionAndRegionIds)>
	                          checked="true"
	                        </#if>
	                        /></td>
	                      </#if>
				          <td><div class="collapsibleLabel">${region.name}</div></td>
				        </tr>
	                  </#list>
	                  </table>
			        </div>
			      </div>
			  <#else>
			      <div class="collapsibleGroup" tid="groupheader">
		            <div class="collapsibleHeading" id="${currentId}-heading">
		              <img src="${base}/images/group_blank.jpg" alt="none" />
		              <#if readOnly="false">
		                <#assign functionAndRegionIds="${function.taskFunction.id?c}_" />
	                    <input tabIndex="${tabIndex}" class="checkbox" type="checkbox" name="functionsAndRegions" 
	                           value="${functionAndRegionIds}" id="${currentId}-all" onclick="updateHeading(this.checked, '${currentId}');"
				          <#if existingFunctions?has_content && existingFunctions?seq_contains(functionAndRegionIds)>
	                        checked="true"
	                      </#if>
	                    />
	                  </#if>
	                  <@s.text name="${function.taskFunction.functionType.resourceKey}"/>
		            </div>
		          </div>
			  </#if>
		    <#assign tabIndex=tabIndex+1>
            </#list>
            </div>
          </div>
		  		  
  </div>
  <div class="formBottom">

    <@s.checkbox 
      theme="css_xhtml" 
      tabindex="${tabIndex}" 
      label="%{getText('workgroup.create.label.isDefault')}" 
      name="options" 
      id="workgroup.isDefault" 
      fieldValue="1" 
      value="workgroup.isDefault" 
      disabled="false" 
      trueValue="checkbox.readonly.yes"
      falseValue="checkbox.readonly.no"
      readonly="${readOnly}"
      stayleft="true" />
    <#assign tabIndex=tabIndex+1>
    <@s.checkbox 
      theme="css_xhtml" 
      tabindex="${tabIndex}" 
      label="%{getText('workgroup.create.label.autoAddRegions')}"
      name="options" 
      id="autoAddRegions" 
      fieldValue="2" 
      value="autoAddRegions" 
      disabled="false" 
      trueValue="checkbox.readonly.yes"
      falseValue="checkbox.readonly.no"
      readonly="${readOnly}"
      stayleft="true" />
    <#-- This hidden checkbox insures that the setOptions method on the action is always called -->
    <input type="checkbox" name="options" value="dummy" checked="true" style="display: none;" />
  </div>
  <div class="formAction">
	
	<#if workgroupId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
  </div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='modifiedEntity.title' />
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('workgroup.create.label.name')}" name="modifiedEntity.groupName" />
	</div>
	<div class="formMiddle">
  
	      <div style="height: 17px;"></div>
          <div class="wwctrlgrp">
            <div class="wwlbl">
              <label class="label">
                <@s.text name="workgroup.create.label.regions" />
              </label>
            </div>
            <div class="wwctrl">
            <#if !modifiedEntity.workgroupFunctions?has_content>
              <@s.text name="workgroup.create.label.regions.none" />
            </#if>
            <#list modifiedEntity.workgroupFunctions as function>
              <#assign currentId="modified-${collapsibleIdPrefix}-${function_index}" />
              <#if function.regions?has_content>
			      <div class="collapsibleGroup">
		            <div class="collapsibleHeading" id="${currentId}-heading">
	                  <img src="${base}/images/plus.gif" alt="open" id="${currentId}-icon"  />
	                  <@s.text name="${function.taskFunction.functionType.resourceKey}"/>&nbsp;-<@s.text name="workgroup.regions.heading" />
	                  (${function.regions?size})
		            </div>
			        <div class="collapsibleList wwctrl" id="${currentId}">
			          <table>
	                  <#list function.regions as region>
	                    <#-- This ID is used for testability -->
	                    <tr id="${function.taskFunction.id?c}_${region.id?c}_pair_modified">
				          <td><div class="collapsibleLabel">${region.name}</div></td>
				        </tr>
	                  </#list>
	                  </table>
			        </div>
			      </div>
			  <#else>
			      <div class="collapsibleGroup">
		            <div class="collapsibleHeading">
		              <img src="${base}/images/group_blank.jpg" alt="none" />
	                  <@s.text name="${function.taskFunction.functionType.resourceKey}"/>
		            </div>
		          </div>
			  </#if>
            </#list>
            </div>
          </div>
	</div>
	<div class="formBottom">
	    <@s.label label="%{getText('workgroup.create.label.isDefault')}" name="modifiedEntity.isDefault"/>
	    <@s.label label="%{getText('workgroup.create.label.autoAddRegions')}" name="modifiedEntity.autoAddRegions"/>
	</div>
</div>

<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>
<script type="text/JavaScript">
    <#if formName!="view">
    initCheckboxes();
    </#if>
</script>

</body>
</html>

