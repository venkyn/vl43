<title><@s.text name='location.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">
	<#assign pageContext="${base}/${navigation.applicationMenu}">

 	<@tablecomponent
		varName="locationObj"
		columns=locationColumns
		tableId="listLocationsTable"
		url="${pageContext}/location"
		tableTitle='location.view.title' 
		viewId=locationViewId
    	type=""
    	extraParams=extraParamsHash?default("") />     	

