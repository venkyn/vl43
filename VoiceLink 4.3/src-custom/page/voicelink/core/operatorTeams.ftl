<title><@s.text name='operatorTeam.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
	<#assign pageContext="${base}/${navigation.applicationMenu}">

 	<@tablecomponent
 		varName="operatorTeamObj"
		columns=operatorTeamColumns
		tableId="listOperatorTeamsTable"
		url="${pageContext}/operatorTeam"
		tableTitle='operatorTeam.view.title' 
		viewId=operatorTeamViewId
    	type=""
    	extraParams=extraParamsHash?default("") />