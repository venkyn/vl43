<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='operator.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='operator.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='operator.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='operator.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='operator.view.title.single'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/operator" action="${formName}.action" method="POST">
<#if operatorId?has_content>
    <@s.hidden name="operatorId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@s.textfield tabindex="1" 
    			   label="%{getText('operator.label.operatorIdentifier')}" 
    			   name="operator.operatorIdentifier" 
    			   id="operator.operatorIdentifier" 
    			   size="25" maxlength="40" 
    			   show="true" 
    			   readonly="${readOnly}" 
    			   disabled="${disabled}"
    			   required="true"/>
	
	<@s.textfield tabindex="2" 
				  label="%{getText('operator.label.password')}" 
				  name="operator.password" 
				  id="operator.password" 
				  size="25" 
				  maxlength="10" 
				  show="true" 
				  readonly="${readOnly}" 
				  required="true"/>
    
    <@s.textfield tabindex="3" 
    			   label="%{getText('operator.label.employeeName')}" 
    			   name="operator.name" 
    			   id="operator.name" 
    			   size="25" 
    			   maxlength="128" 
    			   show="true" 
    			   readonly="${readOnly}" 
    			   required="false"/>

	<#if formName="edit" || formName="view">
		<@s.select tabindex="4" 
					label="%{getText('operator.label.operatorStatus')}" 
					name="operatorStatus" 
					id="operatorStatus" 
					list="operatorStatus" 
					readonly="${readOnly}" 
					disabled="true"
					cssClass="statusSelect"/>
		
		<@s.label label="%{getText('operator.label.signOnTime')}" name="signOnTime"/>
		
		<@s.label label="%{getText('operator.label.signOffTime')}" name="signOffTime"/>
		
		<@s.label label="%{getText('operator.label.currentWorkType')}" name="operator.currentWorkType.functionType" />
	</#if>
    <#if formName="create">
	    <#-- This uses a different name attribute so it maps to a getter that 
	         will always return the default work group. the setter just sets
	         this operator's workgroup.
	    -->
	   	<@s.select tabindex="5" 
	   	            theme="css_xhtml"
	    			label="%{getText('operator.label.workgroup')}" 
	    			name="defaultWorkgroup" 
	    			id="operator.workgroup" 
					list="workgroupMap" 
					listKey="value" 
					listValue="key"
	    			emptyOption="true"
	    			readonly="${readOnly}"
	    			cssClass="workgroupSelect" />
    <#else>
	   	<@s.select tabindex="5" 
	   	            theme="css_xhtml"
	    			label="%{getText('operator.label.workgroup')}" 
	    			name="workgroup" 
	    			id="operator.workgroup" 
					list="workgroupMap" 
					listKey="value" 
					listValue="key"
	    			emptyOption="true"
	    			readonly="${readOnly}"
	    			cssClass="workgroupSelect" />
    </#if>
       <@s.select tabindex="6"
                 theme="css_xhtml"
                 label="%{getText('operator.label.operatorTeams')}" 
	    		 name="operatorTeams" 
	    		 id="operatorTeams" 
				 list="operatorTeamMap" 
				 listKey="value" 
				 listValue="key"
	    		 emptyOption="false"
	    		 readonly="${readOnly}"
	    		 cssClass="operatorTeamSelect" 
	    		 multiple="true" 
	    		 size="10" 
	    		 width="150"/>
                 
</div>
<div class="formAction">
	
	<#if operatorIdentifier?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
	<div id="modEntity" style="visibility: hidden;">
		<div class="modifiedTitle">
			<@s.text name='operator.edit.modifiedEntity.title'/>
		</div>
		<div class="formTop">
			<@s.label label="%{getText('operator.label.operatorIdentifier')}" name="modifiedEntity.operatorIdentifier" />
			<@s.label label="%{getText('operator.label.password')}" name="modifiedEntity.password" />
			<@s.label label="%{getText('operator.label.operatorStatus')}" name="modifiedEntity.status" />
			<@s.label label="%{getText('operator.label.signOnTime')}" name="modifiedEntity.signOnTime"/>
			<@s.label label="%{getText('operator.label.signOffTime')}" name="modifiedEntity.signOffTime"/>
			<@s.label label="%{getText('operator.label.currentWorkType')}" name="modifiedEntity.currentWorkType" />
		    <@s.label label="%{getText('operator.label.workgroup')}"  name="modifiedWorkgroup"/>
		    <@s.label label="%{getText('operator.label.employeeName')}" name="modifiedEntity.name" />
		    <@s.label label="%{getText('operator.label.spokenName')}" name="modifiedEntity.spokenName" />
		    <@s.label label="%{getText('operator.label.operatorTeams')}" name="modifiedEntity.operatorTeams" />
		</div>
	</div>
	<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>


</body>
</html>

