<title><@s.text name='loading.routes.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
	<#assign pageContext="${base}/${navigation.applicationMenu}">
	
    <script type="text/javascript"> 
        function getFilter(viewId, columnId, val, operand) {
            return "submittedFilterCriterion={'viewId':'"+viewId+"','columnId':'"+columnId+"','operandId':'"+operand+"','value1':'" + val+"','value2':'','locked':false}";
        }
        function getLockedFilter(viewId, columnId, val, operand) {
            return "submittedFilterCriterion={'viewId':'"+viewId+"','columnId':'"+columnId+"','operandId':'"+operand+"','value1':'"+val+"','value2':'','locked':true}";
        }
        function displayImportedContainerCount(obj) {
            if (obj.numberOfImportedContainers > 0) {
                var filter1 = getFilter(-1214, '-22903', obj.route.number, -4);
                var filter2 = getFilter(-1214, '-22904', obj.number, -4);
                var filter3 = getFilter(-1214, '-22906', 2, -31);
                return A({"href":"${base}/loading/container/list.action?"+filter1+"&"+filter2+"&"+filter3}, obj.numberOfImportedContainers);
                                                                                     
            } else {
                return obj.numberOfImportedContainers;
           }
        }
        function displayActualContainerCount(obj) {
            if (obj.actualNumberOfContainers > 0) {
                var filter1 = getFilter(-1214, '-22903', obj.route.number, -4);
                var filter2 = getFilter(-1214, '-22904', obj.number, -4);
                return A({"href":"${base}/loading/container/list.action?"+filter1+"&"+filter2}, obj.actualNumberOfContainers);
                                                                                     
            } else {
                return obj.actualNumberOfContainers;
           }
        }
        
    </script type="text/javascript">


	<#assign urlMethod="getRouteData"/>
 	<@tablecomponent
 		varName="routeObj"
		columns=routeColumns
		tableId="listRoutesTable"
		url="${pageContext}/route"
		tableTitle='loading.routes.view.title' 
        viewId=routeViewId
    	type=""
    	extraParams=extraParamsHash?default("") />    		
		
    <#assign urlMethod="getStopData"/>
	<@tablecomponent
 		varName="stopObj"
		columns=stopColumns
		tableId="listStopsTable"
		url="${pageContext}/stop"
		tableTitle='loading.stops.view.title' 
        viewId=stopViewId
    	type=""
    	extraParams={"publishers":[{"name" : "routeObj", "selector": "routeId"}]} />    

    <script type="text/javascript">
        function getNewData(e) {
            stopObj.clearSelectedRows();        
    	    stopObj.refresh( {'routeId': routeObj.getSelectedIds()} );
        }
   	    connect($('listRoutesTable'), 'onSelectedRowsChanged', this, getNewData);   		
   	    connect(currentWindow(), 'onload', this, getNewData);
   	    
    </script type="text/javascript">
