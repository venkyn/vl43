<#include "/include/common/controlgrouping.ftl" />
<#include "/include/common/columnformatting.ftl" />

<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='loading.region.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='loading.region.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='loading.region.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='loading.region.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='loading.region.view.single.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/loading/region" action="${formName}.action?actionValue='save'" method="POST">
<#if regionId?has_content>
    <@s.hidden name="regionId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
  <@columnlayout header=""> 
    <div class="yui-u first">
    <#if readOnly=="true">
    	<@s.textfield 
        	tabindex="1" 
        	label="%{getText('loading.region.create.label.name')}" 
        	name="translatedRegionName" 
        	id="loadingRegion.name" 
        	size="25" 
        	maxlength="50" 
        	required="true" 
        	readonly="${readOnly}"/>
	<#else>
    	<@s.textfield 
        	tabindex="1" 
        	label="%{getText('loading.region.create.label.name')}" 
        	name="loadingRegion.name" 
        	id="loadingRegion.name" 
        	size="25" 
        	maxlength="50" 
        	required="true"
        	readonly="${readOnly}"/>
    		<#if translatedRegionNameOrNull?has_content>	
            	<@s.label 
    		   		label="" 
    				name="translatedRegionNameMessage" 
    				id="loadingRegion.name.translation"/>
     		</#if>	
   	</#if>
	    
	<@s.textfield 
	    tabindex="2" 
	    label="%{getText('loading.region.create.label.number')}" 
	    name="loadingRegion.number" 
	    id="loadingRegion.number" 
	    size="25" 
	    maxlength="9" 
	    show="true" 
	    required="true" 
	    readonly="${readOnly}"
        disabled="${disabled}"/>
        
    <#if readOnly=="true">
		<@s.textfield 
	    	tabindex="3" 
	    	label="%{getText('loading.region.create.label.description')}" 
	    	name="translatedRegionDescription" 
	    	id="loadingRegion.description" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	readonly="${readOnly}"/>	    
	<#else>
		<@s.textfield 
	    	tabindex="3" 
	    	label="%{getText('loading.region.create.label.description')}" 
	   	 	name="loadingRegion.description" 
	    	id="loadingRegion.description" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	readonly="${readOnly}"/>	    
    		<#if translatedRegionDescriptionOrNull?has_content>	
            	<@s.label 
    		   		label="" 
    				name="translatedRegionDescriptionMessage" 
    				id="loadingRegion.description.translation"/>
     		</#if>	
   	</#if>

	<@s.textfield 
	    tabindex="4" 
	    label="%{getText('loading.region.create.label.goalRate')}" 
	    name="loadingRegion.goalRate" 
	    id="loadingRegion.goalRate" 
	    size="25" 
	    maxlength="5" 
	    show="true" 
	    required="true" 
	    readonly="${readOnly}"/>	    
	    
	 </div>    	
	</@columnlayout>   
</div>

<div class="formMiddle">
     <@columnlayout header=""> 
       <div class="yui-u first">	   
         <@controlgroup groupId="operatorSummary" caption="loading.group.caption.general" disabled="false">


		<#-- Uncomment this code for showing a drop down with both Automatic and Manual
			 and comment the textfield code below.   
		<@s.select 
			    tabindex="5"
			    label="%{getText('loading.region.create.label.loadingRouteIssuance')}" 
			    name="loadingRouteIssuance" 
			    id="loadingRouteIssuance" 
			    list="loadingRouteIssuanceMap"
			    readonly="${readOnly}" 
			    disabled="false"/> -->
			    
         <@s.textfield 
	            tabindex="5" 
        	    label="%{getText('loading.region.create.label.loadingRouteIssuance')}" 
	    		name="loadingRouteIssuance" 
	    		id="loadingRouteIssuance" 
	    		size="25" 
	   		 	maxlength="5" 
	   		 	show="true" 
	    		required="false" 
	    		readonly="true"/>
			    
		    <@s.textfield 
			    tabindex="6" 
			    label="%{getText('loading.region.create.label.containerIDDigitsOperSpeaks')}" 
			    name="loadingRegion.containerIDDigitsOperSpeaks" 
			    id="loadingRegion.containerIDDigitsOperSpeaks" 
			    size="25" 
			    maxlength="1" 
			    show="true" 
			    required="true" 
			    readonly="${readOnly}"/> 
			    
		    <@s.textfield 
			    tabindex="7" 
			    label="%{getText('loading.region.create.label.trailerIDDigitsOpSpeaks')}" 
			    name="loadingRegion.trailerIDDigitsOperSpeaks" 
			    id="loadingRegion.trailerIDDigitsOperSpeaks" 
			    size="25" 
			    maxlength="2" 
			    show="true" 
			    required="true" 
			    readonly="${readOnly}"/> 
			    
		    <@s.textfield 
			    tabindex="8" 
			    label="%{getText('loading.region.create.label.maxConsolidationOfContainers')}" 
			    name="loadingRegion.maxConsolidationOfContainers" 
			    id="loadingRegion.maxConsolidationOfContainers" 
			    size="25" 
			    maxlength="2" 
			    show="true" 
			    required="false"
			    readonly="${readOnly}"/> 			    			    			    
		  <br>
         </@controlgroup>                    
       </div>
     </@columnlayout>                   
</div>
        
<div class="formAction">
	
	<#if regionId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>

<div id="modEntity" style="visibility: hidden;">
		<div class="modifiedTitle">
			<@s.text name='modifiedEntity.title'/>
		</div>
		<div class="formTop">
		    <@s.label label="%{getText('loading.region.create.label.number')}" name="modifiedEntity.number"/>
		    <@s.label label="%{getText('loading.region.create.label.name')}" name="modifiedEntity.name"/>
	        <@s.label label="%{getText('loading.region.create.label.goalRate')}" name="modifiedEntity.goalRate" />
	        <@s.label label="%{getText('loading.region.create.label.description')}" name="modifiedEntity.description" />			    
		</div>
		<div class="formMiddle">
	        <#-- General Task Settings -->
	        <@s.label label="%{getText('loading.region.create.label.loadingRouteIssuance')}" name="getText(modifiedEntity.loadingRouteIssuance.resourceKey)" />
	        <@s.label label="%{getText('loading.region.create.label.containerIDDigitsOperSpeaks')}" name="modifiedEntity.containerIDDigitsOperSpeaks" />
	        <@s.label label="%{getText('loading.region.create.label.trailerIDDigitsOpSpeaks')}" name="modifiedEntity.trailerIDDigitsOperSpeaks" />
	        <@s.label label="%{getText('loading.region.create.label.maxConsolidationOfContainers')}" name="modifiedEntity.maxConsolidationOfContainers" />
		</div>	
</div>
<script language="javascript">
    YAHOO.namespace("example.resize");

    function init() {
    
        YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
            {
              width:"450",
              left: 100,
              top: 100,
              constraintoviewport: true,
              fixedcenter: true,
              underlay:"shadow",
              close:true,
              visible:false,
              draggable:true,
              modal:false } );
        YAHOO.example.resize.panel.render();
    }

    YAHOO.util.Event.addListener(window, "load", init);
    
    
</script>
</#if>
</@s.form>
</div>

</body>
</html>

