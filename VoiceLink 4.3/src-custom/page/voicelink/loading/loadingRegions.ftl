<title><@s.text name='loadingRegion.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">

	<#include "/include/common/tablecomponent.ftl">	
		
 	<@tablecomponent
 		varName="tableObj"
		columns=regionColumns
		tableId="listRegionTable"
		url="${pageContext}/region"
		tableTitle='loadingRegion.view.title' 
		viewId=loadingRegionViewId
    	type=""
    	extraParams=extraParamsHash?default("") /> 