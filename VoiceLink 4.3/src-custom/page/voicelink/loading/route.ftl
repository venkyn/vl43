<#include "/include/common/customdialogdate.ftl"/>
<#assign readOnly="false">

<#assign calendarButton>
    <@s.text name='report.button.showcalendar'/>
</#assign>

<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='loading.route.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='loading.route.edit.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='loading.route.view.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<#assign saveValue>
  <@s.text name="form.button.submit"/>
</#assign>
<#assign cancelValue>
  <@s.text name="form.button.cancel"/>
</#assign>
  
  
<script language="Javascript">
	function changeDateLabel()
	{ 
	   $('departureDateTime').value = convertToDate($('departureDateTime').value);
	   setEnableSubmit(true);
	}

	function convertToDate(dateString)
	{   
		// Split the date into year part[0], month part[1], and date part[2]
		
		var parts=dateString.split("-");
		var d=new Date(parts[0], parts[1], parts[2]);
		
		// Store the month based on the numerical value of the month into a var
		var dmonth=d.getMonth();
		
		// Store the date from the orginal parts array
		var ddate=parts[2];
		
		// Store the year from the original parts array
		var dyear=parts[0];
		
		// Finally put all of the parts together in one string and return it to the caller
		var localizedDate = dmonth + "-" + ddate + "-" + dyear;
		
		return localizedDate;
	}
	


// -->
    </script>
<style type="text/css">
table.dateDialog td{
    padding-bottom: 13px;
    padding-left: 5px;
    vertical-align: bottom;
    font-family: Arial, Helvetica,  San-Serif;
    font-size: 11px;
}
</style>

<script type="text/javascript">
$j(function() {
	$j('#dockDoor').keyup(function() {
		  var message = "dockDoor="+$j('#dockDoor').val();
		  $j.ajax({
		 	type:"POST",
		 	url: "/VoiceLink/loading/route/asyncGetDockDoorData.action",
		 	data: message,
		 	processData: "false",
		 	dataType: "json",
		 	success: function(data) {
		 		$j("#dockDoor").autocomplete({
					source: data.ResultSet.Result
				});
		 		}
		   });
	});
});
</script>

<div>

</div>
<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/loading/route" action="${formName}.action" method="POST">
<input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>
<#if routeId?has_content>
    <@s.hidden name="routeId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<script type="text/javascript">

</script>

<div class="formTop">

       <@s.textfield 
        	tabindex="1" 
        	label="%{getText('loading.route.label.routeId')}" 
        	name="loadingRoute.number" 
        	id="loadingRoute.route" 
        	size="25" 
        	maxlength="50" 
        	required="false" 
        	readonly="true"/>	
        	
       <@s.textfield 
        	tabindex="2" 
        	label="%{getText('loading.route.label.dockDoor')}" 
        	name="loadingRoute.dockDoor.scannedVerification" 
        	id="dockDoor" 
        	size="25" 
        	maxlength="50" 
        	required="true" 
        	readonly="${readOnly}"/>        	   

		<@s.select 
		    tabindex="3"
		    label="%{getText('loading.route.label.status')}" 
		    name="loadingRoute.status" 
		    id="loadingRoute.status" 
		    list="loadingRouteStatusMap"
		    readonly="true" 
		    disabled="false"/>       

<#if navigation.pageName="edit">  
	   <b>
       <@s.text name="%{getText('loading.route.label.departureDate')}"/>
       </b>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     
       <@customdialogdate
	      dialogId="departure_date_time"
	      header="loading.route.date.title"
	      dateInputFieldId="departureDateTime"
	      callingButtonId="departureDateTime"
	      multiPage="false"/>
	   <@s.textfield tabindex="4"
			name="departureDateTime"
			id="departureDateTime"
			size="10" 
			maxlength="10"
			show="true"
			theme="simple"
			readonly="false"
			required="true"
			disabled="routeIntegrated"
			onchange="javascript:changeDateLabel();"/>	   

	   <@s.select tabindex="5"
            cssStyle="width: 40px"
		    theme="simple"
		    label=""
		    name="hours"
            id="hours"
            list="scheduleHoursMap"
            listKey="key"
            listValue="value"
            emptyOption="false"
            size="1"
            length="50"
            show="true"
            required="false"
            disabled="routeIntegrated"
            readonly="${readOnly}"/>
	
	   <@s.text name="%{getText('semicolon')}"/>
	       
	   <@s.select tabindex="6"
			cssStyle="width: 40px"
		    theme="simple"
		    label="%{getText('semicolon')}"
		    name="minutes"
            id="minutes"
            list="scheduleMinutesMap"
            listKey="value"
            listValue="key"
            emptyOption="false"
            size="1"
            length="50"
            show="true"
            required="false"
            disabled="routeIntegrated"
            readonly="${readOnly}"/>
            
 	   <@s.text name="%{getText('semicolon')}"/>            
		
	   <@s.select tabindex="7"
			cssStyle="width: 40px"
		    theme="simple"
		    label="%{getText('semicolon')}"
		    name="seconds"
            id="seconds"
            list="scheduleSecondsMap"
            listKey="value"
            listValue="key"
            emptyOption="false"
            size="1"
            length="50"
            show="true"
            required="false"
            disabled="routeIntegrated"
            readonly="${readOnly}"/>            
		
       <@s.text name="%{getText('schedule.edit.interval.timeformat')}" />
<#else>
   	   <@s.textfield 
	   	    tabindex="8"
	   	    label="%{getText('loading.route.label.departureDate')}"
			name="formattedDepartureDateTime"
			id="loadingRoute.departureDateTime"
			size="10" 
			maxlength="10"
			readonly="true"
			required="true"/>
</#if>
       
       <@s.textfield 
        	tabindex="12" 
        	label="%{getText('loading.route.label.trailerNumber')}" 
        	name="loadingRoute.trailer" 
        	id="loadingRoute.trailer" 
        	size="25" 
        	maxlength="9" 
        	required="false" 
        	readonly="${readOnly}"/>			

		<@s.select 
		    tabindex="13"
		    label="%{getText('loading.route.label.dedicatedMultiStop')}" 
		    name="loadingRoute.type" 
		    id="loadingRoute.type" 
		    list="loadingRouteTypeMap"
		    readonly="true" 
		    disabled="false"/>        	     
       
       <@s.textfield 
        	tabindex="14"
        	label="%{getText('loading.route.label.expectedContainers')}" 
        	name="loadingRoute.expectedNumberOfContainers" 
        	id="loadingRoute.expectedNumberOfContainers" 
        	size="25" 
        	maxlength="50" 
        	required="false" 
        	readonly="true"/>
        	
       <@s.textfield 
        	tabindex="15" 
        	label="%{getText('loading.route.label.region')}" 
        	name="loadingRoute.region.name" 
        	id="loadingRoute.region.name" 
        	size="25" 
        	maxlength="50" 
        	required="false" 
        	readonly="true"/>        	      

       <@s.textfield 
        	tabindex="16" 
        	label="%{getText('loading.route.label.expectedCube')}" 
        	name="loadingRoute.expectedCube" 
        	id="loadingRoute.expectedCube" 
        	size="25" 
        	maxlength="50" 
        	required="false" 
        	readonly="true"/>
        	
       <@s.textfield 
        	tabindex="17" 
        	label="%{getText('loading.route.label.expectedWeight')}" 
        	name="loadingRoute.expectedWeight" 
        	id="loadingRoute.expectedWeight" 
        	size="25" 
        	maxlength="50" 
        	required="false" 
        	readonly="true"/>
        	
       <@s.textfield 
        	tabindex="18" 
        	label="%{getText('loading.route.label.startTime')}" 
        	name="loadingRoute.startTime" 
        	id="loadingRoute.startTime" 
        	size="25" 
        	maxlength="50" 
        	required="false" 
        	readonly="true"/>
        	
       <@s.textfield 
        	tabindex="19" 
        	label="%{getText('loading.route.label.endTime')}" 
        	name="loadingRoute.endTime" 
        	id="loadingRoute.endTime" 
        	size="25" 
        	maxlength="50" 
        	required="false" 
        	readonly="true"/>
        	
		<@s.select 
		    tabindex="20"
		    label="%{getText('loading.route.label.captureLoad')}" 
		    name="loadingRoute.captureLoadPosition" 
		    id="loadingRoute.captureLoadPosition" 
		    list="captureLoadPositionMap"
		    readonly="${readOnly}" 
		    disabled="false"/>        	
        	
		<@s.textarea 
			 tabindex="21" 
			 theme="css_xhtml" 
			 label="%{getText('loading.route.label.Note')}" 
			 name="loadingRoute.note" 
			 id="loadingRoute.note"
			 rows="2"
			 cols="50" 
			 readonly="${readOnly}"/>        	        	        	        	       	
       
</div>

<div class="formAction">
	<#if routeId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
	
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='loading.route.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	   <@s.label label="%{getText('loading.route.label.routeId')}" name="modifiedEntity.number"/>

       <@s.label label="%{getText('loading.route.label.dockDoor')}" name="modifiedEntity.dockDoor.scannedVerification"/>

       <@s.label label="%{getText('loading.route.label.status')}" name="modifiedEntity.status" />
	   
	   <@s.label label="%{getText('loading.route.label.departureDate')}" name="modifiedFormattedDepartureDateTime" />
	   
	   <@s.label label="%{getText('loading.route.label.trailerNumber')}" name="modifiedEntity.trailer" />
	   
	   <@s.label label="%{getText('loading.route.label.dedicatedMultiStop')}" name="modifiedEntity.type"/>
       
       <@s.label label="%{getText('loading.route.label.expectedContainers')}" name="modifiedEntity.expectedNumberOfContainers"/>
       
       <@s.label label="%{getText('loading.route.label.region')}" name="modifiedEntity.region.name"/>
       
       <@s.label label="%{getText('loading.route.label.expectedCube')}" name="modifiedEntity.expectedCube"/>
       
       <@s.label label="%{getText('loading.route.label.expectedWeight')}" name="modifiedEntity.expectedWeight"/>
       
       <@s.label label="%{getText('loading.route.label.startTime')}" name="modifiedEntity.startTime"/>
       
       <@s.label label="%{getText('loading.route.label.endTime')}" name="modifiedEntity.endTime"/>
       
       <@s.label label="%{getText('loading.route.label.captureLoad')}" name="modifiedEntity.captureLoadPositionStringValue"/>
       
       <@s.label label="%{getText('loading.route.label.Note')}" name="modifiedEntity.note"/>
	</div>	
</div>

<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
              width:"450",
              left: 100,
              top: 100,
              constraintoviewport: true,
              fixedcenter: true,
              underlay:"shadow",
              close:true,
              visible:false,
              draggable:true,
              modal:false } );
        YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>

</body>
</html>

