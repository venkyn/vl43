<#assign readOnly="true">
<#assign disabled="true">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='loading.container.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='loading.container.edit.button.submit'/>
  </#assign>
  <#assign readOnly="false">
<#else>  
  <#assign title>
    <@s.text name='loading.container.view.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

  <#assign saveValue>
    <@s.text name="form.button.submit"/>
  </#assign>
  <#assign cancelValue>
    <@s.text name="form.button.cancel"/>
  </#assign>
<html>
<head>
<title>${title}</title>
</head>

<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/loading/container" action="${formName}.action" method="POST">
<input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>
<#if containerId?has_content>
    <@s.hidden name="containerId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>


<div class="formTop">

       <@s.label 
	   label="%{getText('loading.container.label.number')}" 
	   name="loadingContainer.containerNumber" 
	   id="loadingContainer.containerNumber"/>

	  <@s.select 
	    tabindex="1"
	    label="%{getText('loading.container.label.status')}" 
	    name="containerStatus" 
	    id="containerStatus" 
	    list="statusMap"
	    readonly="${readOnly}" 
	    disabled="false"/>        	
		
       <@s.label  
	   label="%{getText('loading.container.label.routeNumber')}" 
	   name="loadingContainer.stop.route.number" 
	   id="loadingContainer.stop.route.number"/>
	   
	   <@s.label  
	   label="%{getText('loading.container.label.departureDateTime')}" 
	   name="formattedDepartureDateTime"
	   id="loadingContainer.stop.route.departureDateTime"/>
	   
	   <@s.label  
	   label="%{getText('loading.container.label.regionName')}" 
	   name="loadingContainer.stop.route.region.name" 
	   id="loadingContainer.stop.route.region.name"/>
	   
	   
       <@s.label 
	   label="%{getText('loading.container.label.stopNumber')}" 
	   name="loadingContainer.stop.number" 
	   id="loadingContainer.stop.number"/>

       <@s.label 
	   label="%{getText('loading.container.label.operator')}" 
	   name="loadingContainer.operator.common.name" 
	   id="loadingContainer.operator.common.name"/>
	   
       <@s.label  
	   label="%{getText('loading.container.label.loadTime')}" 
	   name="formattedLoadTime" 
	   id="loadingContainer.loadTime"/>
       
	<@s.select 
	    tabindex="3"
	    label="%{getText('loading.container.label.type')}" 
	    name="loadingContainer.type" 
	    id="loadingContainer.type" 
	    list="LoadingContainerTypeMap"
	    readonly="true" 
	    disabled="false"/>	   
	   
	   <@s.label  
	   label="%{getText('loading.container.label.masterContainer')}" 
	   name="loadingContainer.masterContainerNumber" 
	   id="loadingContainer.masterContainerNumber"/>

       <@s.label 
	   label="%{getText('loading.container.label.expectedCube')}" 
	   name="loadingContainer.expectedCube" 
	   id="loadingContainer.expectedCube"/>
       
       <@s.label 
	   label="%{getText('loading.container.label.expectedWeight')}" 
	   name="loadingContainer.expectedWeight" 
	   id="loadingContainer.expectedWeight"/>

	   <@s.label  
	   label="%{getText('loading.container.label.regionNumber')}" 
	   name="loadingContainer.stop.route.region.number" 
	   id="loadingContainer.stop.route.region.number"/>
</div>
<div class="formAction">

	<#if containerId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
	
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modifiedEntity" style="display:none;">
	<div class="modifiedTitle">
		<@s.text name='loading.container.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	   <@s.label label="%{getText('loading.container.label.number')}" name="modifiedEntity.containerNumber"/>
	   
	   <@s.label label="%{getText('loading.container.label.status')}" name="modifiedEntity.containerStatus"/>
	   
	   <@s.label label="%{getText('loading.container.label.routeNumber')}" name="modifiedEntity.route.number"/>
	   
	   <@s.label label="%{getText('loading.route.label.departureDate')}" name="modifiedEntity.route.departureDateTime"/>
	   
	   <@s.label label="%{getText('loading.route.label.region')}" name="modifiedEntity.route.region.name"/>
	   
       <@s.label label="%{getText('loading.container.label.stopNumber')}" name="modifiedEntity.stop.number"/>

       <@s.label label="%{getText('loading.container.label.operator')}" name="modifiedEntity.operator.common.name"/>
       
       <@s.label label="%{getText('loading.container.label.loadTime')}" name="modifiedEntity.loadTime"/>
       
       <@s.label label="%{getText('loading.container.label.type')}" name="modifiedEntity.type"/>
       
       <@s.label label="%{getText('loading.container.label.masterContainer')}" name="modifiedEntity.masterContainer"/>
       
       <@s.label label="%{getText('loading.container.label.expectedCube')}" name="modifiedEntity.expectedCube"/>
       
       <@s.label label="%{getText('loading.container.label.expectedWeight')}" name="modifiedEntity.expectedWeight"/>

	</div>
</div>

<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 100,
			  top: 100,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>

</body>
</html>

