<title><@s.text name='routeLabor.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">
    <#assign pageContext="${base}/${navigation.applicationMenu}">
    
      <#assign urlMethod="getroutedata"/>
 	  <@tablecomponent
 		varName="routeLaborObj"
		columns=routeLaborColumns
		tableId="routeLaborTable"
		url="${pageContext}/labor"
		tableTitle="routeLabor.view.title" 
    	viewId=routeLaborViewId
    	type=""
    	extraParams={"extraURLParams":[{"name":"operatorLaborId","value":"${operatorLaborId}"}]} />
