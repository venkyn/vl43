<#include "/include/common/controlgrouping.ftl" />
<#include "/include/common/columnformatting.ftl" />

<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='loading.stop.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='loading.stop.edit.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='loading.stop.view.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

  <#assign saveValue>
    <@s.text name="form.button.submit"/>
  </#assign>
  <#assign cancelValue>
    <@s.text name="form.button.cancel"/>
  </#assign>
  
<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/loading/stop" action="${formName}.action?actionValue='save'" method="POST">
<#if stopId?has_content>
    <@s.hidden name="stopId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
  <@columnlayout header=""> 
    <div class="yui-u first">
    	<@s.textfield 
        	tabindex="1" 
        	label="%{getText('loading.stop.stopnumber')}" 
        	name="loadingStop.number" 
        	id="loadingStop.stop" 
        	size="25" 
        	maxlength="50" 
        	required="false" 
        	readonly="true"/>

    	<@s.textfield 
        	tabindex="2" 
        	label="%{getText('loading.stop.customer.number')}" 
        	name="loadingStop.customerNumber" 
        	id="loadingStop.customerNumber" 
        	size="25" 
        	maxlength="50" 
        	required="false" 
        	readonly="true"/>
        	
		<@s.select 
		    tabindex="3"
		    label="%{getText('loading.stop.status')}" 
		    name="loadingStop.status" 
		    id="loadingStop.status" 
		    list="loadingStopStatusMap"
		    readonly="true" 
		    disabled="false"/>        	

    	<@s.textfield 
        	tabindex="4" 
        	label="%{getText('loading.stop.route.number')}" 
        	name="loadingStop.route.number" 
        	id="loadingStop.route.number" 
        	size="25" 
        	maxlength="50" 
        	required="false" 
        	readonly="true"/>        	

    	<@s.textfield 
        	tabindex="5" 
        	label="%{getText('loading.stop.region.number')}" 
        	name="loadingStop.route.region.number" 
        	id="loadingStop.region.number" 
        	size="25" 
        	maxlength="50" 
        	required="false" 
        	readonly="true"/>
        	
    	<@s.textfield 
        	tabindex="6" 
        	label="%{getText('loading.stop.region.name')}" 
        	name="loadingStop.route.region.name" 
        	id="loadingStop.region.name" 
        	size="25" 
        	maxlength="50" 
        	required="false" 
        	readonly="true"/>
        	
		<@s.textarea 
			 tabindex="7" 
			 theme="css_xhtml" 
			 label="%{getText('loading.stop.note')}" 
			 name="loadingStop.note" 
			 id="loadingStop.note"
			 rows="2"
			 cols="50" 
			 readonly="${readOnly}"/>
	 </div>    	
	</@columnlayout>   
</div>

<div class="formAction">

	<#if stopId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
	
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='loading.stop.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	   <@s.label label="%{getText('loading.stop.stopnumber')}" name="modifiedEntity.number"/>

       <@s.label label="%{getText('loading.stop.customer.number')}" name="modifiedEntity.customerNumber"/>

       <@s.label label="%{getText('loading.stop.status')}" name="modifiedEntity.status" />
	   
	   <@s.label label="%{getText('loading.stop.route.number')}" name="modifiedEntity.route.number" />
	   
	   <@s.label label="%{getText('loading.stop.region.number')}" name="modifiedEntity.route.region.number"/>
       
       <@s.label label="%{getText('loading.stop.region.name')}" name="modifiedEntity.route.region.name"/>
       
       <@s.label label="%{getText('loading.stop.note')}" name="modifiedEntity.note"/>
	</div>	
</div>

<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
              width:"450",
              left: 100,
              top: 100,
              constraintoviewport: true,
              fixedcenter: true,
              underlay:"shadow",
              close:true,
              visible:false,
              draggable:true,
              modal:false } );
        YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>

</body>
</html>

