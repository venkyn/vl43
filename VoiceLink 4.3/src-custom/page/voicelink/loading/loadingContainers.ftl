<title><@s.text name='loading.container.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">

    <#include "/include/common/tablecomponent.ftl"> 
    <script type="text/javascript">

        function displayLoadingContainer(obj) {
            return A({'href':base+'/loading/container/view.action?containerId='+obj.id}, obj.name); 
        }   
    </script>
        
    <@tablecomponent
        varName="containerObj"
        columns=containerColumns
        tableId="listLoadingContainerTable"
        url="${pageContext}/container"
        tableTitle='loading.container.view.title' 
        viewId=ViewId
        type=""
        extraParams=extraParamsHash?default("") /> 