<title><@s.text name='cyclecounting.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	

      
    <#assign urlMethod="getCycleCountingData"/>
 	<@tablecomponent
 		varName="cyclecountingObj"
		columns=cyclecountingColumns
		tableId="listCycleCountsTable"
		url="${base}/cyclecounting/assignment"
		tableTitle='cyclecounting.view.title' 
    	viewId=cyclecountingViewId
    	type=""
    	extraParams=extraParamsHash?default("") />

	<#assign urlMethod="getCycleCountingDetailData"/>
 	<@tablecomponent
 		varName="detailObj"
		columns=detailColumns
		tableId="listDetailTable"
		url="${base}/cyclecounting/assignmentDetail"
		tableTitle='cyclecounting.detail.view.title' 
        viewId=detailViewId
    	type=""
		extraParams={"publishers":[{"name" : "cyclecountingObj", "selector": "cycleCountId"}]} />


 	<script type="text/javascript"> 	
   	    if( $("menuBlade_listCycleCountsTable") != null ) {
	   	    //set up and register handlers
	   	    cyclecountingObj.menuBladeHeight = $("menuBlade_listCycleCountsTable").clientHeight;
	   	    detailObj.menuBladeHeight = $("menuBlade_listDetailTable").clientHeight;
	   	    
	   	    function openCycleCountBlade(evt) {
	   	    	showActionsMenu(cyclecountingObj);
	   	    	hideActionsMenu(detailObj);
	   	    }
	   	    
	   	    function openDetailBlade(evt) {
	   	    	showActionsMenu(detailObj);
	   	    	hideActionsMenu(cyclecountingObj);
	   	    }
   	    
   	    	//cyclecountingObj.registerHandlers(openCycleCountBlade, null);
   	    	//detailObj.registerHandlers(openDetailBlade, null); 
   	    }
 	</script>

