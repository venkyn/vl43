<title><@s.text name='region.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">		
  	<script type="text/javascript">
  		function getBooleanTranslation_cc(val) {
  		    if (val === true) {
				return "<@s.text name='cyclecounting.region.view.true' />";
			} else {
				return "<@s.text name='cyclecounting.region.view.false' />";
			}
  		}
  	</script>
 	<@tablecomponent
 		varName="regionObj"
		columns=regionColumns
		tableId="listRegionTable"
		url="${base}/cyclecounting/region"
		tableTitle='region.view.title' 
		viewId=regionViewId
    	type=""
    	extraParams=extraParamsHash?default("") />     			
		
