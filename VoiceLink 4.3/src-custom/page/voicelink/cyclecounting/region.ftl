<#include "/include/common/columnformatting.ftl" />
<#include "/include/common/controlgrouping.ftl" />
<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='cyclecounting.region.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='cyclecounting.region.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='cyclecounting.region.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='cyclecounting.region.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='cyclecounting.region.view.single.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/cyclecounting/region" action="${formName}.action?actionValue='save'" method="POST">
<#if regionId?has_content>
    <@s.hidden name="regionId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@columnlayout header="">
        <div class="yui-u first">
        	<@s.textfield 
            	tabindex="1" 
            	label="%{getText('cyclecounting.region.label.name')}" 
            	name="cycleCountingRegion.name" 
            	id="cycleCountingRegion.name" 
            	size="25" 
            	maxlength="50" 
            	show="true" 
            	required="true" 
            	readonly="${readOnly}"/>
            <@s.textfield 
                tabindex="2" 
                label="%{getText('cyclecounting.region.label.number')}" 
                name="cycleCountingRegion.number" 
                id="cycleCountingRegion.number" 
                size="10" 
                maxlength="9" 
                required="true" 
                readonly="${readOnly}"
                disabled="${disabled}"/>
    	 	<@s.textfield 
    	    	tabindex="3" 
    	    	label="%{getText('cyclecounting.region.label.description')}" 
    	    	name="cycleCountingRegion.description" 
    	    	id="cycleCountingRegion.description" 
    	    	size="25" 
    	    	maxlength="50" 
    	    	show="true" 
    	    	readonly="${readOnly}"/> 
        	<@s.textfield 
        	    tabindex="4" 
        	    label="%{getText('cyclecounting.region.label.goalRate')}" 
        	    name="cycleCountingRegion.goalRate" 
        	    id="cycleCountingRegion.goalRate" 
        	    size="25" 
        	    maxlength="5" 
        	    show="true" 
        	    required="true" 
        	    readonly="${readOnly}"/>
    	</div>
    </@columnlayout>
</div>
<div class="formMiddle">
    <@columnlayout header="">
        <div class="yui-u first">
        	 <@s.select 
        		tabindex="5" 
        	    label="%{getText('cyclecounting.region.label.countType')}" 
        	    name="countType" 
        	    id="countType" 
        	    list="countTypeOptions" 
        	    readonly="${readOnly}" 
        		disabled="false" />
        	 <@s.select 
        	    tabindex="6" 
        	    label="%{getText('cyclecounting.region.label.countMode')}" 
        	    name="countMode" 
        	    id="countMode"
        	    list="countModeOptions" 
        		readonly="${readOnly}" 
        		disabled="false" />	
        </div>
    </@columnlayout>
</div>
<div class="formAction">
	
	<#if regionId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>

<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='modifiedEntity.title'/>
	</div>
		<div class="formTop">
		    <@s.label label="%{getText('cyclecounting.region.label.name')}" name="modifiedEntity.name" />
		    <@s.label label="%{getText('cyclecounting.region.label.number')}" name="modifiedEntity.number" />
		    <@s.label label="%{getText('cyclecounting.region.label.description')}" name="modifiedEntity.description" />
		    <@s.label label="%{getText('cyclecounting.region.label.goalRate')}" name="modifiedEntity.goalRate" />
		</div>
		<div class="formMiddle">
		    <@s.label label="%{getText('cyclecounting.region.label.countType')}" name="getText(modifiedEntity.countType.resourceKey)" />
		    <@s.label label="%{getText('cyclecounting.region.label.countMode')}" name="getText(modifiedEntity.countMode.resourceKey)" />
		</div>
</div>
<script language="javascript">
    YAHOO.namespace("example.resize");

    function init() {
    
        YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
            {
              width:"450",
              left: 300,
              top: 200,
              constraintoviewport: true,
              fixedcenter: true,
              underlay:"shadow",
              close:true,
              visible:false,
              draggable:true,
              modal:false } );
        YAHOO.example.resize.panel.render();
    }

    YAHOO.util.Event.addListener(window, "load", init);
    
    
</script>
</#if>
</@s.form>
</div>

</body>
</html>

