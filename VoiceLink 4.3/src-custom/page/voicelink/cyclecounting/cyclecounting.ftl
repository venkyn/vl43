<#assign readOnly="false">
<#assign yesreadOnly="true">

 <#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='cyclecounting.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='cyclecounting.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#else>  
  <#assign title>
    <@s.text name='cyclecounting.view.single.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
 </#if>

<#assign saveValue>
   <@s.text name="form.button.submit"/>
  </#assign>
  <#assign cancelValue>
    <@s.text name="form.button.cancel"/>
</#assign>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/cyclecounting/assignment" action="${formName}.action" method="POST">
<input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>
<#if cycleCountId?has_content>
    <@s.hidden name="cycleCountId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>

<div class="formTop">

                   <@s.textfield tabindex="2" 
    			   label="%{getText('cyclecounting.assignment.label.sequenceNumber')}" 
    			   name="cycleCountingAssignment.sequence" 
    			   id="cycleCountingAssignment.sequence" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
    			   
                   <@s.select tabindex="3" 
                     label="%{getText('cyclecounting.assignment.label.status')}" 
                     name="cycleCountStatus" 
                     id="cycleCountStatus" 
                     list="cycleCountStatusMap" 
                     cssClass="statusSelect" 
                     readonly="${readOnly}" />
                                      
                                     
    			   <@s.textfield tabindex="4" 
    			   label="%{getText('cyclecounting.assignment.label.regionName')}" 
    			   name="cycleCountingAssignment.region.name" 
    			   id="cycleCountingAssignment.region.name" 
    			   size="25" 
    			   maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="5" 
    			   label="%{getText('common.view.column.locationId')}" 
    			   name="cycleCountingAssignment.location.scannedVerification" 
    			   id="cycleCountingAssignment.location.scannedVerification" 
    			   size="25" 
    			   maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <#if navigation.pageName="edit">
	                   <@s.select
	                     tabindex="6" 
	                     label="%{getText('cyclecounting.assignment.label.operatorIdentifier')}" 
	                     name="cycleCountOperatorId" 
	                     id="cycleCountOperatorId" 
	                     list="cycleCountOperatorMap" 
	                     cssClass="operatorSelect" 
	                     readonly="${readOnly}" />
                   <#else>
	                     <@s.label 
	                     label="%{getText('cyclecounting.assignment.label.operatorIdentifier')}" 
	                     name="cycleCountingAssignment.operator.common.operatorIdentifier" 
	                     id="cycleCountingAssignment.operator.common.operatorIdentifier" 
	                     size="25" 
	                     maxlength="80" />
                   </#if>                   
                                     
                   <@s.textfield tabindex="7" 
    			   label="%{getText('cyclecounting.assignment.label.startTime')}" 
    			   name="cycleCountingAssignment.startTime" 
    			   id="cycleCountingAssignment.startTime" 
    			   size="25" 
    			   maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="8" 
    			   label="%{getText('cyclecounting.assignment.label.endTime')}" 
    			   name="cycleCountingAssignment.endTime" 
    			   id="cycleCountingAssignment.endTime" 
    			   size="25" 
    			   maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                                                                                            
                   <@s.textfield tabindex="9" 
    			   label="%{getText('cyclecounting.assignment.label.id')}" 
    			   name="cycleCountId" 
    			   id="cycleCountId" 
    			   size="25" 
    			   maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                                                         
                   <@s.textfield tabindex="10" 
    			   label="%{getText('cyclecounting.assignment.label.regionNumber')}" 
    			   name="cycleCountingAssignment.region.number" 
    			   id="cycleCountingAssignment.region.number" 
    			   size="25" 
    			   maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="11" 
    			   label="%{getText('cyclecounting.assignment.label.dateReceived')}" 
    			   name="cycleCountingAssignment.createdDate" 
    			   id="cycleCountingAssignment.createdDate" 
    			   size="25" 
    			   maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
            
</div>

<div class="formAction">
	
	<#if cycleCountId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='assignment.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('cyclecounting.assignment.label.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('cyclecounting.assignment.label.status')}" name="modifiedEntity.status"/>
	    
	    <@s.label label="%{getText('cyclecounting.assignment.label.operatorIdentifier')}" name="modifiedEntity.operatorId"/>
	    
	    <@s.label label="%{getText('cyclecounting.assignment.label.edit.sequenceNumber')}" name="modifiedEntity.sequence.sequenceNumber"/>
	
    
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</body>
</html>

