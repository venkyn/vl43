
<div id="modifyOperator">
<@s.select tabindex="1"
            label="%{getText('')}" 
            name="operatorId" 
	        id="operatorId" 
	        list="cycleCountOperatorMap" 
	        listValue="value" 
	        listKey="key" 
	        readonly="false" 
	        cssClass="operatorSelect"  
	        theme="css_nonbreaking"/>
</div>