<title><@s.text name='assignmentLabor.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">
    <#assign pageContext="${base}/${navigation.applicationMenu}">
    
      <#assign urlMethod="getAssignmentData"/>
 	  <@tablecomponent
 		varName="assignmentLaborObj"
		columns=assignmentLaborColumns
		tableId="assignmentLaborTable"
		url="${pageContext}/labor"
		tableTitle="assignmentLabor.view.title" 
    	viewId=assignmentLaborViewId
    	type=""
    	extraParams={"extraURLParams":[{"name":"operatorLaborId","value":"${operatorLaborId}"}]} />
