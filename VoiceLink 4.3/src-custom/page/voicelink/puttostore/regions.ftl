<title><@s.text name='putaway.region.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">
	<script type="text/javascript">
	       function getBooleanTranslation_pts(value) {
            if (value === true) {
                return "<@s.text name='puttostore.region.view.true' />";
            } else {
                return "<@s.text name='puttostore.region.view.false' />";
            }
        }
    </script>
 	<@tablecomponent
 		varName="regionObj"
		columns=regionColumns
		tableId="listRegionTable"
		url="${base}/puttostore/region"
		tableTitle="puttostore.regions.view.title"
		viewId=regionViewId
    	type=""
    	extraParams=extraParamsHash?default("") />     			
		
