<#include "/include/common/columnformatting.ftl" />
<#include "/include/common/controlgrouping.ftl" />

<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title='puttostore.region.edit.title'/>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='puttostore.region.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title='puttostore.region.create.title'/>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='puttostore.region.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title='puttostore.region.view.title' />
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
    <title><@s.text name='${title}'/></title>
</head>
<body>
    <style type="text/css">
    select.digitsDropdown {
        width: 80px;
    }
    </style>
    <div class="formBody">
        <div class="titleBar"><@s.text name="${title}" /></div>
        <@s.form name="${formName}" id="form1" validate="true" namespaceVal="/puttostore/region" action="${formName}.action?actionValue='save'" method="POST">
        <#if regionId?has_content>
            <@s.hidden name="regionId"/>
            <@s.hidden name="savedEntityKey"/>
        </#if>
        <div class="formTop">
          <@columnlayout header="">
          <div class="yui-u first">
    	    <#if readOnly=="true">
              <@s.textfield 
                tabindex="1" 
    		    label="%{getText('puttostore.region.label.name')}" 
    			name="translatedRegionName" 
    			id="ptsRegion.name" 
    			size="25" 
    			maxlength="50" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"
    			required="true"/>
			<#else>
              <@s.textfield 
                tabindex="1" 
    		    label="%{getText('puttostore.region.label.name')}" 
    			name="ptsRegion.name" 
    			id="ptsRegion.name" 
    			size="25" 
    			maxlength="50" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"
    			required="true"/>
    		    <#if translatedRegionNameOrNull?has_content>	
              		<@s.label 
    		    		label="" 
    					name="translatedRegionNameMessage" 
    					id="puttostoreRegion.name.translation"/>
     		    </#if>	
   		  	</#if>
   		  	
            <@s.textfield 
                tabindex="2" 
    		    label="%{getText('puttostore.region.label.number')}" 
    			name="ptsRegion.number" 
    			id="ptsRegion.number" 
    			size="25" 
    			maxlength="9" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="${disabled}"
    			required="true"/>
    			
    		<#if readOnly=="true">
				<@s.textfield 
	    			tabindex="3" 
	    			label="%{getText('puttostore.region.label.description')}" 
	    			name="translatedRegionDescription" 
	    		id="ptsRegion.description" 
	    		size="25" 
	    		maxlength="50" 
	    		show="true" 
	    		readonly="${readOnly}"	    
    			disabled="false"
    			required="false"/>
			<#else>
				<@s.textfield 
	    		tabindex="3" 
	    		label="%{getText('puttostore.region.label.description')}" 
	   	 		name="ptsRegion.description" 
	    		id="ptsRegion.description" 
	    		size="25" 
	    		maxlength="50" 
	    		show="true" 
	    		readonly="${readOnly}"	    
    			disabled="false"
    			required="false"/>
    			<#if translatedRegionDescriptionOrNull?has_content>	
            		<@s.label 
    		   			label="" 
    					name="translatedRegionDescriptionMessage" 
    					id="puttostoreRegion.description.translation"/>
     			</#if>	
   			</#if>
            <@s.textfield 
                tabindex="4" 
    		    label="%{getText('puttostore.region.label.goalRate')}" 
    			name="ptsRegion.goalRate" 
    			id="ptsRegion.goalRate" 
    			size="5" 
    			maxlength="5"
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"
    			required="true"/>
          </div>
   	      <div class="yui-u">  
   	      </div>
   	      </@columnlayout>
        </div>
        <div class="formMiddle">
          <@columnlayout header="">
          <div class="yui-u first">
          <#-- General Task Settings -->
          <@controlgroup groupId="general" caption="puttostore.region.create.caption.general" disabled="false">
    		 <@s.checkbox
                cssClass="checkbox"  
                theme="css_xhtml" 
                tabindex="6" 
                label="%{getText('puttostore.region.label.allowRepickSkips')}" 
                name="ptsRegion.allowRepickSkips" 
                id="allowRepickSkips" 
                trueValue="puttostore.region.view.true"
                falseValue="puttostore.region.view.false"                
                disabled="false" 
                readonly="${readOnly}"
                stayleft="true"/>
                
              <@s.checkbox
                cssClass="checkbox"  
                theme="css_xhtml" 
                tabindex="8" 
                label="%{getText('puttostore.region.label.allowPassAssignment')}" 
                name="ptsRegion.allowPassAssignment" 
                id="allowPassAssignment" 
                trueValue="puttostore.region.view.true"
                falseValue="puttostore.region.view.false"                
                disabled="false" 
                readonly="${readOnly}"
                stayleft="true"/>
                
               <@s.checkbox
                cssClass="checkbox"  
                theme="css_xhtml" 
                tabindex="9" 
                label="%{getText('puttostore.region.label.allowSignOff')}" 
                name="ptsRegion.allowSignOff" 
                id="allowSignOff" 
                trueValue="puttostore.region.view.true"
                falseValue="puttostore.region.view.false"                
                disabled="false" 
                readonly="${readOnly}"
                stayleft="true"/>
                
             <@s.select 
		        tabindex="11" 
				label="%{getText('puttostore.region.label.allowSkipping')}" 
				name="allowSkips" 
				id="allowSkips"
				list="allowSkipsMap" 
				readonly="${readOnly}" 
				disabled="false"/>
		
		    
		      </@controlgroup> 
   		     
            <@controlgroup groupId="location" caption="puttostore.region.create.caption.location" disabled="false">
   		
               <@s.checkbox
                cssClass="checkbox"  
                theme="css_xhtml" 
                tabindex="10" 
                label="%{getText('puttostore.region.label.confirmSpokenLocation')}" 
                name="ptsRegion.confirmSpokenLocation" 
                id="confirmSpokenLocation" 
                trueValue="puttostore.region.view.true"
                falseValue="puttostore.region.view.false"                
                disabled="false" 
                readonly="${readOnly}"
                stayleft="true"/>  
                   
	        <@s.select 
		        tabindex="13" 
				label="%{getText('puttostore.region.label.spokenLocationLength')}" 
				name="ptsRegion.spokenLocationLength" 
				id="spokenLocationLength"
				list="spokenLocationLengthMap" 
				readonly="${readOnly}" 
				disabled="false"
				/>
				
			 <@s.select 
		        tabindex="14" 
				label="%{getText('puttostore.region.label.locationCheckDigitLength')}" 
				name="ptsRegion.locationCheckDigitLength" 
				id="locationCheckDigitLength"
				list="locationCheckDigitLengthMap" 
				readonly="${readOnly}" 
				disabled="false"/>
		     </@controlgroup> 
   	
   	       <@controlgroup groupId="quantity" caption="puttostore.region.create.caption.license" disabled="false">
   	       
   	       
           <@s.checkbox
                cssClass="checkbox"  
                theme="css_xhtml" 
                tabindex="10" 
                label="%{getText('puttostore.region.label.confirmSpokenLicense')}" 
                name="ptsRegion.confirmSpokenLicense" 
                id="confirmSpokenLocation" 
                trueValue="puttostore.region.view.true"
                falseValue="puttostore.region.view.false"                
                disabled="false" 
                readonly="${readOnly}"
                stayleft="true"/>  
   	       
   		   <@s.select 
		        tabindex="12" 
				label="%{getText('puttostore.region.label.spokenLicenseLength')}" 
				name="ptsRegion.spokenLicenseLength" 
				id="spokenLicenseLength"
				list="spokenLicenseLengthMap" 
				readonly="${readOnly}" 
				disabled="false"/>	
				
			  <@s.select 
		        tabindex="16" 
				label="%{getText('puttostore.region.label.maxLicensesInGroup')}" 
				name="ptsRegion.maxLicensesInGroup" 
				id="maxLicensesInGroup"
				list="maxLicensesLengthMap" 
				readonly="${readOnly}" 
				disabled="false"
				/>
			
             </@controlgroup> 
             
             </div>
             <div class="yui-u"> 
             
         
              <@controlgroup groupId="container" caption="puttostore.region.create.caption.containers" disabled="false">
              
              <@s.checkbox
                cssClass="checkbox"  
                theme="css_xhtml" 
                tabindex="17" 
                label="%{getText('puttostore.region.label.promptForContainerID')}" 
                name="ptsRegion.systemGeneratesContainerID" 
                id="systemGeneratesContainerID" 
                trueValue="puttostore.region.view.true"
                falseValue="puttostore.region.view.false"                
                disabled="false" 
                readonly="${readOnly}"
                stayleft="true"/>
          		
			  <@s.checkbox
                cssClass="checkbox"  
                theme="css_xhtml" 
                tabindex="18" 
                label="%{getText('puttostore.region.label.allowMultipleOpenContainers')}" 
                name="ptsRegion.allowMultipleOpenContainers" 
                id="allowMultipleOpenContainers" 
                trueValue="puttostore.region.view.true"
                falseValue="puttostore.region.view.false"                
                disabled="false" 
                readonly="${readOnly}"
                stayleft="true"/>	
    
              <@s.select 
    	        tabindex="19" 
				label="%{getText('puttostore.region.label.validateContainerId')}" 
				name="validateContainerId" 
				id="validateContainerId"
				list="validateContainerMap" 
				readonly="${readOnly}" 
				disabled="false"/>	
						
			   <@s.select 
		        tabindex="20" 
				label="%{getText('puttostore.region.label.validateContainerLength')}" 
				name="ptsRegion.validateContainerLength" 
				id="validateContainerLength"
				list="validateContainerLengthMap" 
				readonly="${readOnly}" 
				disabled="false"/>
                
   		    </@controlgroup> 
   		  </div>
   	      <div class="yui-u">
   	        <#-- Quantity Prompts -->
   	        <@controlgroup groupId="quantity" caption="puttostore.region.create.caption.flowThruLocations" disabled="false">
   	        
              <@s.textfield 
                tabindex="21" 
    		    label="%{getText('puttostore.region.label.flowThruLocation')}" 
    			name="ptsRegion.flowThruLocation" 
    			id="ptsRegion.flowThruLocation" 
    			size="25" 
    			maxlength="50" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"/>
                
              <@s.textfield 
                tabindex="22" 
    		    label="%{getText('puttostore.region.label.residualReturnLocation')}" 
    			name="ptsRegion.residualLocation" 
    			id="ptsRegion.residualLocation" 
    			size="25" 
    			maxlength="50" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"/>
                
              <@s.textfield 
                tabindex="23" 
    		    label="%{getText('puttostore.region.label.residualReturnLocationCheckDigit')}" 
    			name="ptsRegion.residualLocationCD" 
    			id="ptsRegion.residualLocationCD" 
    			size="25" 
    			maxlength="5" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"/>   
    			
    		 <@s.textfield 
                tabindex="24" 
    		    label="%{getText('puttostore.region.label.unExpectedResidualLocation')}" 
    			name="ptsRegion.unExpResidualLocation" 
    			id="ptsRegion.unExpResidualLocation" 
    			size="25" 
    			maxlength="50" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"/>
    			
    	   <@s.textfield 
                tabindex="25" 
    		    label="%{getText('puttostore.region.label.unExpectedResidualLocationCD')}" 
    			name="ptsRegion.unExpResidualLocationCD" 
    			id="ptsRegion.unExpResidualLocationCD" 
    			size="25" 
    			maxlength="5" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"/>                 
                
   	        </@controlgroup>
   	      </div>
   	      </@columnlayout>
        </div>
		<div class="formAction">
			<#if regionId?has_content>
				<#if !actionErrors?has_content>
					<#assign submitOnChangeForm="form1">
				</#if>
			</#if>
			<#assign submitIndex=199>
			<#assign cancelIndex=200>
			<#include "/include/form/buttonbar.ftl">
		</div>
        <#-- Include a modified data div if there is modified data to display -->
        <#if modifiedEntity?has_content>
            <div id="modEntity" style="visibility: hidden;">
		        <div class="modifiedTitle">
			        <@s.text name='puttostore.region.edit.modifiedEntity.title'/>
		        </div>
		        <div class="formTop">
		            <@s.label label="%{getText('puttostore.region.label.number')}" name="modifiedEntity.number" />
		            <@s.label label="%{getText('puttostore.region.label.name')}" name="modifiedEntity.name" />
		            <@s.label label="%{getText('puttostore.region.label.goalRate')}" name="modifiedEntity.goalRate" />
		            <@s.label label="%{getText('puttostore.region.label.description')}" name="modifiedEntity.description" />
		        </div>
		        <div class="formMiddle">
		            <#-- General Task Settings -->
		            <@s.label label="%{getText('puttostore.region.label.allowRepickSkips')}" name="modifiedEntity.allowRepickSkips" />
		            <@s.label label="%{getText('puttostore.region.label.allowCancelTrip')}" name="modifiedEntity.allowCancelTrip" />
		            <@s.label label="%{getText('puttostore.region.label.allowSignOff')}" name="modifiedEntity.allowSignOff" />
		            <@s.label label="%{getText('puttostore.region.label.speakForMultipleSKU')}" name="modifiedEntity.speakForMultipleSKU" />
		             <#-- container Prompts -->
		            <@s.label label="%{getText('puttostore.region.label.promptForContainerID')}" name="modifiedEntity.promptForContainerID" />
		            <@s.label label="%{getText('puttostore.region.label.allowMultipleContainers')}" name="modifiedEntity.allowMultipleContainers" />
		            <#-- Task Data Entry -->
		            <@s.label label="%{getText('puttostore.region.label.flowThruLocation')}" name="modifiedEntity.flowThruLocation" />
		            <@s.label label="%{getText('puttostore.region.label.flowThruReturnLocation')}" name="modifiedEntity.flowThruReturnLocation" />
		             </div>
	        </div>
            <script language="javascript">
            	YAHOO.namespace("example.resize");

            	function init() {
            		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
            			{
            			  width:"450",
            			  left: 300,
            			  top: 200,
            			  constraintoviewport: true,
            			  fixedcenter: true,
	                	  underlay:"shadow",
            			  close:true,
	                	  visible:false,
	                	  draggable:true,
                		  modal:false } );
                	YAHOO.example.resize.panel.render();
            	}

            	YAHOO.util.Event.addListener(window, "load", init);
            </script>
         </#if>
        </@s.form>
    </div>
    <script type="text/javascript">
	    connect(document.getElementsByTagName('body')[0], 'onclick', hideMoreErrors);
    </script>
</body>
</html>
