<title><@s.text name='customerLocation.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
	<#assign pageContext="${base}/${navigation.applicationMenu}">
    <#assign multipleText>
      <@s.text name='puttostore.container.multiple.link'/>
    </#assign>	

 	<@tablecomponent
 		varName="customerLocationObj"
		columns=customerLocationColumns
		tableId="listCustomerLocationsTable"
		url="${pageContext}/customerLocation"
		tableTitle='customerLocation.view.title' 
		viewId=customerLocationViewId
    	type=""
    	extraParams=extraParamsHash?default("") />

    <#assign urlMethod="getData" />
    		
 	<@tablecomponent
 		varName="customerOrderObj"
		columns=customerOrderColumns
		tableId="listCustomerOrdersTable"
		url="${pageContext}/customerOrder"
		tableTitle='customerOrder.view.title' 
        viewId=customerOrderViewId
    	type=""
    	extraParams={"publishers":[{"name" : "customerLocationObj", "selector": "customerLocationID"}]} />		

    <script type="text/javascript">
        function getNewData(e) {
    	    customerOrderObj.refresh( {'customerLocationID': customerLocationObj.getSelectedIds()} );
        }
   	    connect($('listCustomerLocationsTable'), 'onSelectedRowsChanged', this, getNewData);   		
   	    connect(currentWindow(), 'onload', this, getNewData);
   	    
    </script type="text/javascript">