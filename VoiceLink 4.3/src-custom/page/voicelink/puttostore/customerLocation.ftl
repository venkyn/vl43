<#assign readOnly="true">
<#assign disabled="false">
<#assign title>
  <@s.text name='puttostore.customerLocation.view.single.title'/>
</#assign>
<#assign formName="view">

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/customerLocation" action="${formName}.action" method="POST">
<#if customerLocationId?has_content>
    <@s.hidden name="customerLocationId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">

    <@s.textfield tabindex="1" 
        label="%{getText('puttostore.view.column.route')}" 
        name="customerLocation.route" 
        id="customerLocation.route" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>
        
    <@s.textfield tabindex="2" 
        label="%{getText('puttostore.view.column.customer')}" 
        name="customerLocation.customerInfo.customerNumber" 
        id="customerLocation.customerInfo.customerNumber" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>
        
    <@s.textfield tabindex="3" 
        label="%{getText('puttostore.view.column.percentComplete')}" 
        name="percentComplete" 
        id="percentComplete" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/> 
                       
    <@s.textfield tabindex="4" 
        label="%{getText('puttostore.view.column.aisle')}" 
        name="customerLocation.location.description.aisle" 
        id="customerLocation.location.description.aisle" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>	

    <@s.textfield tabindex="5" 
        label="%{getText('puttostore.view.column.slot')}" 
        name="customerLocation.location.description.slot" 
        id="customerLocation.location.description.slot" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>

    <@s.textfield tabindex="6" 
        label="%{getText('puttostore.view.column.checkDigits')}" 
        name="customerLocation.location.checkDigits" 
        id="customerLocation.location.checkDigits" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>

    <@s.textfield tabindex="7" 
        label="%{getText('puttostore.view.column.deliveryLocation')}" 
        name="customerLocation.deliveryLocation" 
        id="customerLocation.deliveryLocation" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>
        
    <@s.textfield tabindex="8" 
        label="%{getText('puttostore.view.column.customerName')}" 
        name="customerLocation.customerInfo.customerAddress" 
        id="customerLocation.customerInfo.customerAddress" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>     
        
    <@s.textfield tabindex="9" 
        label="%{getText('puttostore.view.column.customerAddress')}" 
        name="customerLocation.customerInfo.customerAddress" 
        id="customerLocation.customerInfo.customerAddress" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>  
        
    <@s.textfield tabindex="10" 
        label="%{getText('puttostore.view.column.preAisle')}" 
        name="customerLocation.location.description.preAisle" 
        id="customerLocation.location.description.preAisle" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>  
        
    <@s.textfield tabindex="11" 
        label="%{getText('puttostore.view.column.postAisle')}" 
        name="customerLocation.location.description.postAisle" 
        id="customerLocation.location.description.postAisle" 
        size="10" 
        maxlength="2" 
        required="true" 
        readonly="${readOnly}"
        disabled="${disabled}"/>                                                     
</div>
<div class="formAction">
	
	<#if customerLocationId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='customerLocation.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('customerLocation.create.label.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('customerLocation.create.label.name')}" name="modifiedEntity.name"/>
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>
<script type="text/JavaScript">
	connect(document.getElementsByTagName('body')[0], 'onclick', hideMoreErrors);
</script>

</body>
</html>

