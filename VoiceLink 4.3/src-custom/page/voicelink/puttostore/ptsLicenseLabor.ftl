<title><@s.text name='voicelink.puttostore.labor.license.title'/></title>
	<#include "/include/common/tablecomponent.ftl">
	<#assign pageContext="${base}/${navigation.applicationMenu}">

      <#assign urlMethod="getLicenseData"/>
 	  <@tablecomponent
 		varName="licenseLaborTableObj"
		columns=licenseLaborColumns
		tableId="licenseLaborTable"
		url="${pageContext}/labor"
		tableTitle="ptsLicenseLaborLabor.view.title" 
    	viewId=licenseLaborViewId
    	type=""
    	extraParams={"extraURLParams":[{"name":"operatorLaborId","value":"${operatorLaborId}"}]} />
