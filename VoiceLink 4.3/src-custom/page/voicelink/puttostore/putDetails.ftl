<title><@s.text name='putDetail.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
	<#assign pageContext="${base}/${navigation.applicationMenu}">

    <#assign urlMethod="getPutDetailData"/>
 	<@tablecomponent
 		varName="putDetailObj"
		columns=putDetailColumns
		tableId="listPutDetailsTable"
		url="${pageContext}/license/putDetail"
		tableTitle='putDetail.view.title' 
		viewId=putDetailViewId
    	type=""
    	extraParams={"extraURLParams":[{"name":"licenseId","value":"${licenseId?c}"},{"name":"putId","value":"${putId?c}"}]} />        
