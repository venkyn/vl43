<title><@s.text name='container.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">
	<#assign pageContext="${base}/${navigation.applicationMenu}">
    
 	<@tablecomponent
 		varName="ptsContainerObj"
		columns=ptsContainerColumns
		tableId="listPtsContainersTable"
		url="${base}/puttostore/container"
		tableTitle='ptsContainer.view.title' 
		viewId=ptsContainerViewId
    	type=""
    	extraParams=extraParamsHash?default("") />
	
    <#assign urlMethod="getContainerDetailData"/> 
 	    
    <@tablecomponent
 		varName="ptsContainerDetailsObj"
		columns=containerDetailColumns
		tableId="listContainerDetailsTable"
		url="${base}/puttostore/containerDetail"
		tableTitle='containerDetail.view.title' 
		viewId=containerDetailViewId
    	type=""
    	extraParams={"publishers":[{"name" : "ptsContainerObj", "selector": "containerID"}]}/>