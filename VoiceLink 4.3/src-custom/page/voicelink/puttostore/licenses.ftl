<title><@s.text name='puttostore.license.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
	<#assign pageContext="${base}/${navigation.applicationMenu}">

 	<@tablecomponent
 		varName="licenseObj"
		columns=licenseColumns
		tableId="listLicensesTable"
		url="${pageContext}/license"
		tableTitle='puttostore.license.view.title' 
		viewId=licenseViewId
    	type=""
    	extraParams=extraParamsHash?default("") />
    	
    <#assign urlMethod="getPutData" />
    		
 	<@tablecomponent
 		varName="putObj"
		columns=putColumns
		tableId="listPutsTable"
		url="${pageContext}/put"
		tableTitle='put.view.title' 
        viewId=putViewId
    	type=""
    	extraParams={"publishers":[{"name" : "licenseObj", "selector": "licenseID"}]} />     		
    
