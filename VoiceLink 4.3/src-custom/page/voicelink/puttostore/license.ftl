<#assign readOnly="false">
<#assign yesreadOnly="true">

  <#assign formName="edit">
  
  <#assign saveValue>
    <@s.text name="form.button.submit"/>
  </#assign>
  <#assign cancelValue>
    <@s.text name="form.button.cancel"/>
  </#assign>
<html>
<head>
<title><@s.text name='puttostore.license.edit.title'/></title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	<@s.text name='puttostore.license.edit.title'/>
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/puttostore/license" action="${formName}.action" method="POST">
<input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>
<#if licenseId?has_content>
    <@s.hidden name="licenseId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<script type="text/javascript">
	function showEditLicenseConfirmDialog() {
		var dialogProperties = {
			    title: "<@s.text name='license.editStatus.confirmation.header'/>",			    
				body: "<@s.text name='license.editStatus.confirmation.body'/>",
				button1: "<@s.text name='license.editStatus.confirmation.ok.button'/>",
				button2: "<@s.text name='license.editStatus.confirmation.cancel.button'/>"}
		buildCustomDialog(dialogProperties,submitForm, "");
	}
	
	function submitForm() {
		$(${formName}).submit();
	}
</script>

<div class="formTop">

                   <@s.textfield tabindex="1" 
    			   label="%{getText('puttostore.view.column.license')}" 
    			   name="license.number" 
    			   id="license.number" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="2" 
    			   label="%{getText('puttostore.view.column.regionName')}" 
    			   name="license.region.name" 
    			   id="license.region.name" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
           
                   <@s.textfield tabindex="3" 
    			   label="%{getText('puttostore.view.column.regionNumber')}" 
    			   name="license.region.number" 
    			   id="license.region.number" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
           
    			   <@s.select
	               tabindex="4" 
	               label="%{getText('puttostore.view.column.licenseStatus')}" 
    			   name="licenseStatus" 
    			   id="licenseStatus" 
    			   list="license.GUITransitionStatuses"
    			   readonly="${readOnly}"/>			    			 

	               <@s.textfield tabindex="4" 
    				label="%{getText('puttostore.view.column.operatorName')}" 
    				name="license.operator.common.operatorIdentifier" 
		    		id="license.operator.common.operatorIdentifier" 
    				size="25" maxlength="40" 
    				show="true"
		            readonly="${yesreadOnly}" />
    			    
                   <@s.textfield tabindex="6" 
    			   label="%{getText('puttostore.view.column.startTime')}" 
    			   name="licenseStartTime" 
    			   id="licenseStartTime" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="7" 
    			   label="%{getText('puttostore.view.column.endTime')}" 
    			   name="licenseEndTime"
    			   id="licenseEndTime"
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="8" 
    			   label="%{getText('puttostore.view.column.residualItems')}" 
    			   name="license.expectedResiduals" 
    			   id="license.expectedResiduals" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="9" 
    			   label="%{getText('puttostore.view.column.residualLocations')}" 
    			   name="license.region.residualLocation" 
    			   id="license.region.residualLocation" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="10" 
    			   label="%{getText('common.view.column.createdDate')}" 
    			   name="dateImported" 
    			   id="dateImported" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
                   
                   <@s.textfield tabindex="11" 
    			   label="%{getText('puttostore.view.column.putDetailCount')}" 
    			   name="license.putDetailCount" 
    			   id="license.putDetailCount" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />            
                   
                   <@s.textfield tabindex="12" 
    			   label="%{getText('puttostore.view.column.exportStatus')}" 
    			   name="exportStatus" 
    			   id="exportStatus" 
    			   size="25" maxlength="40" 
    			   show="true"
                   readonly="${yesreadOnly}" />
               
</div>
<div class="formAction">

    <a
	href="javascript:saveForm();"
	id="${formName}.save"
	class="${cancelCssClass?default('anchorButton')}"
	tabindex="199"
	>${saveValue}</a>
	
    <a
	href="javascript:modifySubmitVariable('form1','submitTypeElement','method:cancel');"
	id="${formName}.cancel"
	class="${cancelCssClass?default('anchorButton')}"
	tabindex="200"
	>${cancelValue}</a>	
	
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='license.edit.modifiedEntity.title'/>
	</div>
	<div class="formTop">
	     
	</div>	
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
	
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>

<script type="text/JavaScript">
    
    function saveForm() {
        showEditLicenseConfirmDialog();
    }  

</script>

</body>
</html>

