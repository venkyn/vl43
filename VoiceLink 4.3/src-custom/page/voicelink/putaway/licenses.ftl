<title><@s.text name='putaway.license.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
	
 	<@tablecomponent
 		varName="licenseObj"
		columns=licenseColumns
		tableId="listLicenseTable"
		url="${base}/putaway/license"
		tableTitle="putaway.license.view.title" 
		viewId=licenseViewId
    	type=""
    	extraParams=extraParamsHash?default("") />     
