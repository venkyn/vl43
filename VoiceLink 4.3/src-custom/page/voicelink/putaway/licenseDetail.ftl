<title><@s.text name='putaway.licenseDetail.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
 	<@tablecomponent
 		varName="licenseDetailObj"
		columns=licenseDetailColumns
		tableId="listLicenseDetailTable"
		url="${base}/putaway/licenseDetail"
		tableTitle="putaway.licenseDetail.view.title" 
		viewId=licenseDetailViewId
    	type=""
    	extraParams=extraParamsHash?default("") />

