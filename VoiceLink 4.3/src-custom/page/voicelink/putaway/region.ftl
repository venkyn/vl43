<#include "/include/common/columnformatting.ftl" />
<#include "/include/common/controlgrouping.ftl" />

<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title='putaway.region.edit.title'/>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='putaway.region.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title='putaway.region.create.title'/>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='putaway.region.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title='putaway.region.view.title.single' />
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
    <title><@s.text name='${title}'/></title>
</head>
<body>
    <style type="text/css">
    select.digitsDropdown {
        width: 80px;
    }
    </style>
    <div class="formBody">
        <div class="titleBar"><@s.text name="${title}" /></div>
        <@s.form name="${formName}" id="form1" validate="true" namespaceVal="/putaway/region" action="${formName}.action?actionValue='save'" method="POST">
        <#if regionId?has_content>
            <@s.hidden name="regionId"/>
            <@s.hidden name="savedEntityKey"/>
        </#if>
        <div class="formTop">
          <@columnlayout header="">
          <div class="yui-u first">
    	    <#if readOnly=="true">
              <@s.textfield 
                tabindex="1" 
    		    label="%{getText('putaway.region.label.name')}" 
    			name="translatedRegionName" 
    			id="putawayRegion.name" 
    			size="25" 
    			maxlength="50" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"
    			required="true"/>
			<#else>
              <@s.textfield 
                tabindex="1" 
    		    label="%{getText('putaway.region.label.name')}" 
    			name="putawayRegion.name" 
    			id="putawayRegion.name" 
    			size="25" 
    			maxlength="50" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"
    			required="true"/>
    		    <#if translatedRegionNameOrNull?has_content>	
              		<@s.label 
    		    		label="" 
    					name="translatedRegionNameMessage" 
    					id="putawayRegion.name.translation"/>
     		    </#if>	
   		  	</#if>
   		  	
            <@s.textfield 
                tabindex="2" 
    		    label="%{getText('putaway.region.label.number')}" 
    			name="putawayRegion.number" 
    			id="putawayRegion.number" 
    			size="25" 
    			maxlength="9" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="${disabled}"
    			required="true"/>
    			
    		<#if readOnly=="true">
				<@s.textfield 
	    			tabindex="3" 
	    			label="%{getText('putaway.region.label.description')}" 
	    			name="translatedRegionDescription" 
	    		id="putawayRegion.description" 
	    		size="25" 
	    		maxlength="50" 
	    		show="true" 
	    		readonly="${readOnly}"	    
    			disabled="false"
    			required="false"/>
			<#else>
				<@s.textfield 
	    		tabindex="3" 
	    		label="%{getText('putaway.region.label.description')}" 
	   	 		name="putawayRegion.description" 
	    		id="putawayRegion.description" 
	    		size="25" 
	    		maxlength="50" 
	    		show="true" 
	    		readonly="${readOnly}"	    
    			disabled="false"
    			required="false"/>
    			<#if translatedRegionDescriptionOrNull?has_content>	
            		<@s.label 
    		   			label="" 
    					name="translatedRegionDescriptionMessage" 
    					id="putawayRegion.description.translation"/>
     			</#if>	
   			</#if>
            <@s.textfield 
                tabindex="4" 
    		    label="%{getText('putaway.region.label.goalRate')}" 
    			name="putawayRegion.goalRate" 
    			id="putawayRegion.goalRate" 
    			size="5" 
    			maxlength="5"
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"
    			required="true"/>
          </div>
   	      <div class="yui-u">  
   	      </div>
   	      </@columnlayout>
        </div>
        <div class="formMiddle">
          <@columnlayout header="">
          <div class="yui-u first">
          <#-- General Task Settings -->
          <@controlgroup groupId="general" caption="putaway.region.create.caption.general" disabled="false">
              <#-- This hidden checkbox makes sure that the setBooleanRegionProperty
                   method on the action is always called -->
              <@s.checkbox
                cssClass="checkbox"
                theme="css_xhtml"
		        tabindex="5"
		        label="%{getText('putaway.region.label.allowSkipLicense')}" 
		        name="putawayRegion.allowSkipLicense" 
		        id="allowSkipLicense"
                disabled="false" 
                trueValue="putaway.region.view.true"
                falseValue="putaway.region.view.false"                
                readonly="${readOnly}"
                stayleft="true"/>
                
              <@s.checkbox
                cssClass="checkbox" 
                theme="css_xhtml" 
                tabindex="6" 
                label="%{getText('putaway.region.label.allowCancelLicense')}" 
                name="putawayRegion.allowCancelLicense" 
                id="allowCancelLicense" 
                disabled="false" 
                trueValue="putaway.region.view.true"
                falseValue="putaway.region.view.false"                
                readonly="${readOnly}"
                stayleft="true"/>
                
              <@s.checkbox
                cssClass="checkbox"  
                theme="css_xhtml" 
                tabindex="7" 
                label="%{getText('putaway.region.label.verifyLicense')}" 
                name="putawayRegion.verifyLicense" 
                id="verifyLicense" 
                trueValue="putaway.region.view.true"
                falseValue="putaway.region.view.false"                
                disabled="false" 
                readonly="${readOnly}"
                stayleft="true"/>
                
		      <@s.select 
		        tabindex="8" 
				label="%{getText('putaway.region.label.captureStartLocation')}" 
				name="captureStartLocation" 
				id="captureStartLocation"
				list="captureStartLocationOptions" 
				readonly="${readOnly}" 
				disabled="false"
				/>

              <@s.checkbox
                cssClass="checkbox"  
                theme="css_xhtml" 
                tabindex="9" 
                label="%{getText('putaway.region.label.allowOverrideLocation')}" 
                name="putawayRegion.allowOverrideLocation" 
                trueValue="putaway.region.view.true"
                falseValue="putaway.region.view.false"                
                id="allowOverrideLocation" 
                disabled="false" 
                readonly="${readOnly}"
                stayleft="true"/>
                
              <@s.checkbox
                cssClass="checkbox"  
                theme="css_xhtml" 
                tabindex="10" 
                label="%{getText('putaway.region.label.verifySpokenLicOrLoc')}" 
                name="putawayRegion.verifySpokenLicOrLoc" 
                id="verifySpokenLicOrLoc" 
                disabled="false" 
                trueValue="putaway.region.view.true"
                falseValue="putaway.region.view.false"                
                readonly="${readOnly}"
                stayleft="true"/>
                
              <@s.textfield 
                tabindex="11" 
    		    label="%{getText('putaway.region.label.numberOfLicensesAllowed')}" 
    			name="putawayRegion.numberOfLicensesAllowed" 
    			id="putawayRegion.numberOfLicensesAllowed" 
    			size="2" 
    			maxlength="2" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"
    			required="true" />

            <@s.textfield 
                tabindex="12" 
    		    label="%{getText('putaway.region.label.exceptionLocation')}" 
    			name="putawayRegion.exceptionLocation" 
    			id="putawayRegion.exceptionLocation" 
    			size="25" 
    			maxlength="50" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"
    			required="false" />
   		    </@controlgroup> 
   		  </div>
   	      <div class="yui-u">
   	        <#-- Quantity Prompts -->
   	        <@controlgroup groupId="quantity" caption="putaway.region.create.caption.quantityPrompts" disabled="false">
   	        
              <@s.checkbox
                cssClass="checkbox"
                theme="css_xhtml" 
                tabindex="13" 
                label="%{getText('putaway.region.label.allowOverrideQuantity')}" 
                name="putawayRegion.allowOverrideQuantity" 
                id="allowOverrideQuantity" 
                disabled="false" 
                trueValue="putaway.region.view.true"
                falseValue="putaway.region.view.false"                
                readonly="${readOnly}"
                stayleft="true"/>
                
              <@s.checkbox
                cssClass="checkbox"
                theme="css_xhtml" 
                tabindex="14" 
                label="%{getText('putaway.region.label.allowPartialPut')}" 
                name="putawayRegion.allowPartialPut" 
                id="allowPartialPut" 
                disabled="false" 
                trueValue="putaway.region.view.true"
                falseValue="putaway.region.view.false"                
                readonly="${readOnly}"
                stayleft="true"/>
                
              <@s.checkbox
                cssClass="checkbox"
                theme="css_xhtml" 
                tabindex="15" 
                label="%{getText('putaway.region.label.capturePickupQuantity')}" 
                name="putawayRegion.capturePickupQuantity" 
                id="capturePickupQuantity" 
                disabled="false" 
                trueValue="putaway.region.view.true"
                falseValue="putaway.region.view.false"                
                readonly="${readOnly}"
                stayleft="true"/>
                
              <@s.checkbox
                cssClass="checkbox"
                theme="css_xhtml" 
                tabindex="16" 
                label="%{getText('putaway.region.label.capturePutQuantity')}" 
                name="putawayRegion.capturePutQuantity" 
                id="capturePutQuantity" 
                disabled="false" 
                trueValue="putaway.region.view.true"
                falseValue="putaway.region.view.false"                
                readonly="${readOnly}"
                stayleft="true"/>
                
   	        </@controlgroup>
   	        <#-- Task Data Entry -->
   	        <@controlgroup groupId="task" caption="putaway.region.create.caption.taskDataEntry" disabled="false">
   	        
   	          <@s.textfield 
                tabindex="17" 
    		    label="%{getText('putaway.region.label.licDigitsTaskSpeaks')}" 
    			name="putawayRegion.licDigitsTaskSpeaks" 
    			id="putawayRegion.licDigitsTaskSpeaks" 
    			size="2" 
    			maxlength="2" 
    			show="true" 
    			readonly="${readOnly}" 
    			disabled="false"
    			required="true" />
    			
		      <@s.select 
		        tabindex="18" 
				label="%{getText('putaway.region.label.licDigitsOperatorSpeaks')}" 
				name="putawayRegion.licDigitsOperatorSpeaks" 
				id="licDigitsOperatorSpeaks"
				list="licDigitsOperatorSpeaksOptions" 
				readonly="${readOnly}" 
				cssClass="digitsDropdown"
				disabled="false" />
				
		      <@s.select 
		        tabindex="19" 
				label="%{getText('putaway.region.label.locDigitsOperatorSpeaks')}" 
				name="putawayRegion.locDigitsOperatorSpeaks" 
				id="locDigitsOperatorSpeaks"
				list="locDigitsOperatorSpeaksOptions" 
				readonly="${readOnly}" 
				cssClass="digitsDropdown"
				disabled="false" />
				
		      <@s.select 
		        tabindex="20" 
				label="%{getText('putaway.region.label.checkDigitsOperatorSpeaks')}" 
				name="putawayRegion.checkDigitsOperatorSpeaks" 
				id="checkDigitsOperatorSpeaks"
				list="checkDigitsOperatorSpeaksOptions" 
				readonly="${readOnly}" 
				cssClass="digitsDropdown"
				disabled="false" />
				
   	        </@controlgroup>
   	      </div>
   	      </@columnlayout>
        </div>
		<div class="formAction">
			<#if operatorIdentifier?has_content>
				<#if !actionErrors?has_content>
					<#assign submitOnChangeForm="form1">
				</#if>
			</#if>
			<#assign submitIndex=199>
			<#assign cancelIndex=200>
			<#include "/include/form/buttonbar.ftl">
		</div>
        <#-- Include a modified data div if there is modified data to display -->
        <#if modifiedEntity?has_content>
            <div id="modEntity" style="visibility: hidden;">
		        <div class="modifiedTitle">
			        <@s.text name='putaway.region.edit.modifiedEntity.title'/>
		        </div>
		        <div class="formTop">
		            <@s.label label="%{getText('putaway.region.label.number')}" name="modifiedEntity.number" />
		            <@s.label label="%{getText('putaway.region.label.name')}" name="modifiedEntity.name" />
		            <@s.label label="%{getText('putaway.region.label.goalRate')}" name="modifiedEntity.goalRate" />
		            <@s.label label="%{getText('putaway.region.label.description')}" name="modifiedEntity.description" />
		        </div>
		        <div class="formMiddle">
		            <#-- General Task Settings -->
		            <@sw.label label="%{getText('putaway.region.label.allowSkipLicense')}" name="modifiedEntity.allowSkipLicense" />
		            <@sw.label label="%{getText('putaway.region.label.allowCancelLicense')}" name="modifiedEntity.allowCancelLicense" />
		            <@sw.label label="%{getText('putaway.region.label.verifyLicense')}" name="modifiedEntity.verifyLicense" />
		            <@sw.label label="%{getText('putaway.region.label.captureStartLocation')}" name="modifiedEntity.captureStartLocation" />
		            <@sw.label label="%{getText('putaway.region.label.allowOverrideLocation')}" name="modifiedEntity.allowOverrideLocation" />
		            <@sw.label label="%{getText('putaway.region.label.verifySpokenLicOrLoc')}" name="modifiedEntity.verifySpokenLicOrLoc" />
		            <@sw.label label="%{getText('putaway.region.label.numberOfLicensesAllowed')}" name="modifiedEntity.numberOfLicensesAllowed" />
		            <@sw.label label="%{getText('putaway.region.label.exceptionLocation')}" name="modifiedEntity.exceptionLocation" />
		            <#-- Quantity Prompts -->
		            <@sw.label label="%{getText('putaway.region.label.allowOverrideQuantity')}" name="modifiedEntity.allowOverrideQuantity" />
		            <@sw.label label="%{getText('putaway.region.label.allowPartialPut')}" name="modifiedEntity.allowPartialPut" />
		            <@sw.label label="%{getText('putaway.region.label.capturePickupQuantity')}" name="modifiedEntity.capturePickupQuantity" />
		            <@sw.label label="%{getText('putaway.region.label.capturePutQuantity')}" name="modifiedEntity.capturePutQuantity" />
		            <#-- Task Data Entry -->
		            <@sw.label label="%{getText('putaway.region.label.licDigitsTaskSpeaks')}" name="modifiedEntity.licDigitsTaskSpeaks" />
		            <@sw.label label="%{getText('putaway.region.label.licDigitsOperatorSpeaks')}" name="modifiedEntity.licDigitsOperatorSpeaks" />
		            <@sw.label label="%{getText('putaway.region.label.locDigitsOperatorSpeaks')}" name="modifiedEntity.locDigitsOperatorSpeaks" />
		            <@sw.label label="%{getText('putaway.region.label.checkDigitsOperatorSpeaks')}" name="modifiedEntity.checkDigitsOperatorSpeaks" />
		        </div>
	        </div>
            <script language="javascript">
            	YAHOO.namespace("example.resize");

            	function init() {
            		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
            			{
            			  width:"450",
            			  left: 300,
            			  top: 200,
            			  constraintoviewport: true,
            			  fixedcenter: true,
	                	  underlay:"shadow",
            			  close:true,
	                	  visible:false,
	                	  draggable:true,
                		  modal:false } );
                	YAHOO.example.resize.panel.render();
            	}

            	YAHOO.util.Event.addListener(window, "load", init);
            </script>
         </#if>
        </@s.form>
    </div>
</body>
</html>
