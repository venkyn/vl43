<title><@s.text name='putaway.region.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">
    <script type="text/javascript">
        function getBooleanTranslation_putaway(value) {
            if (value === true) {
                return "<@s.text name='putaway.region.view.true' />";
            } else {
                return "<@s.text name='putaway.region.view.false' />";
            }
        }
    </script>
 	<@tablecomponent
 		varName="regionObj"
		columns=regionColumns
		tableId="listRegionTable"
		url="${base}/putaway/region"
		tableTitle="putaway.region.view.title"
		viewId=regionViewId
    	type=""
    	extraParams=extraParamsHash?default("") />     			
		
