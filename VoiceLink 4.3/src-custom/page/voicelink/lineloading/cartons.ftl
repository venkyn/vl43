<title><@s.text name='carton.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
	<#assign pageContext="${base}/${navigation.applicationMenu}">

 	 	<@tablecomponent
 		varName="cartonObj"
		columns=cartonColumns
		tableId="listCartonsTable"
		url="${base}/lineloading/carton"
		tableTitle='carton.view.title' 
        viewId=cartonViewId
    	type=""
    	extraParams=extraParamsHash?default("") />

 	
 	
 	
