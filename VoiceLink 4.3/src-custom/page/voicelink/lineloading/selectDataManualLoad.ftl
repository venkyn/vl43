<table border="0" class="content" width="290px"> 
<tr>
<td colspan="2"><div id="textContainer" ><@s.text name="lineloading.carton.manualload.message" /></div></td>
</tr>
<tr>
<td>
<div id="textContainer" style="font-weight: bold;"><@s.text name="common.view.column.operator" /><@s.text name='semicolon'/></div>
</td>
<td align="right">
				<@s.select tabindex="3" 
    			   name="operatorID" 
    			   id="operatorID" 
    			   list="allOperatorsInRegion"
    			   readonly="false"
    			   theme="css_nonbreaking"
    			   class="inputElement"/>
</td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr>
<tr>
<td>
<div id="textContainer" style="font-weight: bold;"><@s.text name="lineloading.pallet.view.column.palletNumber" /><@s.text name='semicolon'/></div>
</td>
<td align="right">
				<@s.select tabindex="3" 
    			   name="palletID" 
    			   id="palletID" 
    			   list="openPalletsForRoutStop"
    			   readonly="false"
    			   theme="css_nonbreaking"
    			   class="inputElement"/>
</td></tr>
</table>