<title><@s.text name='pallet.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
 <#assign pageContext="${base}/${navigation.applicationMenu}">
   
 <@tablecomponent
		varName="palletObj"
		columns=palletColumns
		tableId="listPalletsTable"
		url="${base}/lineloading/pallet"
		tableTitle='pallet.view.title' 
    	viewId=palletViewId
    	type=""
    	extraParams=extraParamsHash?default("") />

      <#assign urlMethod="getPalletCartonData" />
      <@tablecomponent
		varName="cartonObj"
		columns=cartonColumns
		tableId="listCartonsTable"
		url="${base}/lineloading/carton"
		tableTitle='carton.view.title' 
    	viewId=cartonViewId
    	type=""
    	extraParams={"publishers":[{"name" : "palletObj", "selector": "palletID"}]} />
    	
    
    