<title><@s.text name='routeStops.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">

	<#include "/include/common/tablecomponent.ftl">	

 	<@tablecomponent
 		varName="tableObj"
		columns=routeStopColumns
		tableId="listRouteStopsTable"
		url="${pageContext}/routeStop"
		tableTitle='routeStops.view.title' 
		viewId=routeStopViewId
    	type=""
    	extraParams=extraParamsHash?default("") /> 