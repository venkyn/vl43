
<#include "/include/common/controlgrouping.ftl" />
<#include "/include/common/columnformatting.ftl" />

<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='lineloading.region.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='lineloading.region.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='lineloading.region.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='lineloading.region.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='lineloading.region.view.single.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/lineloading/region" action="${formName}.action?actionValue='save'" method="POST">
<#if regionId?has_content>
    <@s.hidden name="regionId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
  <@columnlayout header=""> 
    <div class="yui-u first">
    <#if readOnly=="true">
    	<@s.textfield 
        	tabindex="1" 
        	label="%{getText('lineloading.region.create.label.name')}" 
        	name="translatedRegionName" 
        	id="lineLoadingRegion.name" 
        	size="25" 
        	maxlength="50" 
        	required="true" 
        	readonly="${readOnly}"/>
	<#else>
    	<@s.textfield 
        	tabindex="1" 
        	label="%{getText('lineloading.region.create.label.name')}" 
        	name="lineLoadingRegion.name" 
        	id="lineLoadingRegion.name" 
        	size="25" 
        	maxlength="50" 
        	required="true" 
        	readonly="${readOnly}"/>
    		<#if translatedRegionNameOrNull?has_content>	
            	<@s.label 
    		   		label="" 
    				name="translatedRegionNameMessage" 
    				id="lineloadingRegion.name.translation"/>
     		</#if>	
   	</#if>
	    
	<@s.textfield 
	    tabindex="2" 
	    label="%{getText('lineloading.region.create.label.number')}" 
	    name="lineLoadingRegion.number" 
	    id="lineLoadingRegion.number" 
	    size="25" 
	    maxlength="10" 
	    show="true" 
	    required="true" 
	    readonly="${readOnly}"
        disabled="${disabled}"/>
        
    <#if readOnly=="true">
		<@s.textfield 
	    	tabindex="3" 
	    	label="%{getText('lineloading.region.create.label.description')}" 
	    	name="translatedRegionDescription" 
	    	id="lineLoadingRegion.description" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	readonly="${readOnly}"/>	    
	<#else>
		<@s.textfield 
	    	tabindex="3" 
	    	label="%{getText('lineloading.region.create.label.description')}" 
	   	 	name="lineLoadingRegion.description" 
	    	id="lineLoadingRegion.description" 
	    	size="25" 
	    	maxlength="50" 
	    	show="true" 
	    	readonly="${readOnly}"/>	    
    		<#if translatedRegionDescriptionOrNull?has_content>	
            	<@s.label 
    		   		label="" 
    				name="translatedRegionDescriptionMessage" 
    				id="lineloadingRegion.description.translation"/>
     		</#if>	
   	</#if>

	<@s.textfield 
	    tabindex="4" 
	    label="%{getText('lineloading.region.create.label.goalRate')}" 
	    name="lineLoadingRegion.goalRate" 
	    id="lineLoadingRegion.goalRate" 
	    size="25" 
	    maxlength="5" 
	    show="true" 
	    required="true" 
	    readonly="${readOnly}"/>	    
	    
	 </div>    	
	</@columnlayout>   
</div>

<div class="formMiddle">
     <@columnlayout header=""> 
       <div class="yui-u first">	   
         <@controlgroup groupId="operatorSummary" caption="lineloading.group.caption.general" disabled="false">
              
		<#--	<@s.select 
			    tabindex="5" 
			    label="%{getText('lineloading.region.create.label.palletIdentificationMode')}" 
			    name="lineLoadingRegion.palletIdentificationMode" 
			    id="lineLoadingRegion.palletIdentificationMode" 
			    list="lineLoadingRegion.palletIdentificationMode"
			    readonly="true" 
			    required="${readOnly}"
			    cssClass="statusSelect"
			    disabled="false"/>-->
			    
		  	<@s.textfield 
	            tabindex="5" 
        	    label="%{getText('lineloading.region.create.label.palletIdentificationMode')}" 
	    		name="palletIdentificationMode" 
	    		id="palletIdentificationMode" 
	    		size="25" 
	   		 	maxlength="5" 
	   		 	show="true" 
	    		required="true" 
	    		readonly="true"/>	
			    
			    
			    
			<@s.checkbox 
		        tabindex="6" 
		        theme="css_xhtml" 
		        label="%{getText('lineloading.region.create.label.allowCloseStopFromTask')}" 
		        name="lineLoadingRegion.allowCloseStopFromTask" 
		        id="lineLoadingRegion.allowCloseStopFromTask" 
		        cssClass="checkbox" 
                trueValue="lineloadregion.allowCloseStopFromTask.true"
                falseValue="lineloadregion.allowCloseStopFromTask.false"		        
		        readonly="${readOnly}"
	        	disabled="false"
	        	stayleft="true"/>	
		    
		    <@s.checkbox 
		        tabindex="7" 
		        theme="css_xhtml" 
		        label="%{getText('lineloading.region.create.label.autoPrintManifestReport')}" 
		        name="lineLoadingRegion.autoPrintManifestReport" 
		        id="lineLoadingRegion.autoPrintManifestReport" 
                trueValue="lineloadregion.autoPrintManifestReport.true"
                falseValue="lineloadregion.autoPrintManifestReport.false"		        
		        cssClass="checkbox" 
		        readonly="${readOnly}"
	        	disabled="false"
	        	stayleft="true"/>	
						    
		  <br>
         </@controlgroup>                    

         <@controlgroup groupId="operatorSummary" caption="lineloading.group.caption.pallet" disabled="false">
                  
		    <@s.textfield 
			    tabindex="8" 
			    label="%{getText('lineloading.region.create.label.palletIDStart')}" 
			    name="lineLoadingRegion.palletLabelLayout.palletIDStart" 
			    id="lineLoadingRegion.palletLabelLayout.palletIDStart" 
			    size="25" 
			    maxlength="3" 
			    show="true" 
			    required="true" 
			    readonly="${readOnly}"/>   
			    
			<@s.textfield 
			    tabindex="9" 
			    label="%{getText('lineloading.region.create.label.palletIDLength')}" 
			    name="lineLoadingRegion.palletLabelLayout.palletIDLength" 
			    id="lineLoadingRegion.palletLabelLayout.palletIDLength" 
			    size="25" 
			    maxlength="3" 
			    show="true" 
			    required="true" 
			    readonly="${readOnly}"/>	
		    
		    <@s.textfield 
			    tabindex="10" 
			    label="%{getText('lineloading.region.create.label.routeStart')}" 
			    name="lineLoadingRegion.palletLabelLayout.routeStart" 
			    id="lineLoadingRegion.palletLabelLayout.routeStart" 
			    size="25" 
			    maxlength="3" 
			    show="true" 
			    required="true" 
			    readonly="${readOnly}"/>
			    
		    <@s.textfield 
			    tabindex="11" 
			    label="%{getText('lineloading.region.create.label.routeLength')}" 
			    name="lineLoadingRegion.palletLabelLayout.routeLength" 
			    id="lineLoadingRegion.palletLabelLayout.routeLength" 
			    size="25" 
			    maxlength="3" 
			    show="true" 
			    required="true" 
			    readonly="${readOnly}"/>	
			    
			<@s.textfield 
			    tabindex="12" 
			    label="%{getText('lineloading.region.create.label.stopStart')}" 
			    name="lineLoadingRegion.palletLabelLayout.stopStart" 
			    id="lineLoadingRegion.palletLabelLayout.stopStart" 
			    size="25" 
			    maxlength="3" 
			    show="true" 
			    required="true" 
			    readonly="${readOnly}"/>
			    
			<@s.textfield 
			    tabindex="13" 
			    label="%{getText('lineloading.region.create.label.stopLength')}" 
			    name="lineLoadingRegion.palletLabelLayout.stopLength" 
			    id="lineLoadingRegion.palletLabelLayout.stopLength" 
			    size="25" 
			    maxlength="3" 
			    show="true" 
			    required="true" 
			    readonly="${readOnly}"/>
						    
         </@controlgroup>   
       </div>
     </@columnlayout>                   
</div>
        
<div class="formAction">
	
	<#if regionId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modifiedEntity" style="display:none;">
	<div class="modifiedTitle">
		<@s.text name='modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('lineloading.region.create.label.number')}" name="modifiedEntity.number"/>
	    
	    <@s.label label="%{getText('lineloading.region.create.label.name')}" name="modifiedEntity.name"/>
	</div>	
</div>
</#if>
</@s.form>
</div>

</body>
</html>

