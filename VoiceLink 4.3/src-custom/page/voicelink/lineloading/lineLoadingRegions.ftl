<title><@s.text name='lineLoadingRegion.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
	<#include "/include/common/tablecomponent.ftl">		
 	<@tablecomponent
 		varName="tableObj"
		columns=regionColumns
		tableId="listLineLoadingRegionTable"
		url="${pageContext}/region"
		tableTitle='lineLoadingRegion.view.title' 
		viewId=lineLoadingRegionViewId
    	type=""
    	extraParams=extraParamsHash?default("") /> 