<title><@s.text name='carton.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">	
	    <#assign pageContext="${base}/${navigation.applicationMenu}">
	

	<#assign urlMethod="getCartonDetailsTableData"/>
 	<@tablecomponent
 		varName="cartonDetailsObj"
		columns=cartonDetailsColumns
		tableId="listCartonDetailsTable"
		url="${pageContext}/carton/cartonDetail"
		tableTitle='lineloading.carton.view.column.cartonDetails' 
        viewId=cartonDetailsViewId
    	type=""
    	extraParams={"extraURLParams":[{"name":"cartonId","value":"${cartonId?c}"}]} />
