<html>
  <head>
    <title><@s.text name='print.table'/></title>
    <link rel="stylesheet" type="text/css" href="${base}/css/print.css"/>
    <script type="text/javascript" src="${base}/scripts/MochiKit/MochiKit.js"></script>
    <script type="text/javascript">
        function loaded() {
            window.loaded = true;
        }
        
        function setTable(tableStruct) {
            printContent.innerHTML = "<table>" + tableStruct.innerHTML + "</table>";
        }
        currentWindow().setupData = setTable;
    </script>
  </head>
  <body onload="javascript:loaded()">
    <div id="printContent">
      <@s.text name='wait.message'/>
    </div>
  </body>
</html> 