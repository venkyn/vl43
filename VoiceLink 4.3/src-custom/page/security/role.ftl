<#assign readOnly="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='role.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign formAction="edit.action">  
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='role.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign formAction="create.action">  
  <#assign submitValue>
    <@s.text name='role.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='role.view.currentRole.title'/>
  </#assign>
  <#assign formName="view">
  <#assign formAction="view.action">
  <#assign readOnly="true">
</#if>
<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/admin/role" action="${formAction}" method="POST">
  <!-- Hidden id of existing role when editing -->
  <#if roleId?has_content>
    <@s.hidden name="roleId"/>
    <@s.hidden name="savedEntityKey"/>
  </#if>
	<div class="formTop">
	    <@s.textfield tabindex="1" label="%{getText('role.name')}" name="role.name" id="role.name" size="25" maxlength="128" required="true" readonly="${readOnly}"/>
	    <@s.textfield tabindex="2" label="%{getText('roles.column.description')}" name="role.description" id="role.description" size="50" maxlength="128" readonly="${readOnly}"/>
    </div> 
    <#assign currentTabIndex=2>
    <#assign featureCount=0>
    <div class="formMiddle formMiddlePadded">
		<div id="wwgrp_CreateRole_features" class="wwgrp">
			<div class="wwctrlgrp">
				<div id="wwlbl_CreateRole_features" class="wwlbl">
					<label for="CreateRole_features" class="label"><@s.text name="role.create.label.features"/></label>
				</div><br>
				<div class="wwctrl">
				  <table class="features">
		 		  <#list featureGroups as group>
		 		  <#assign currentTabIndex=currentTabIndex + 1>
		 		  <#if "${readOnly}"=="true">
		 		      <#list group.features as feature>
		                  <#if features?has_content && features?seq_contains(feature.id)>
		 		  <tr>
		 		    <td>
		              <label for="featureGroups-${group_index}" class="checkboxGroupLabel">${stack.findValue("getText('${group.name}')")} (${stack.findValue("getText('${group.description}')")})</label>
		            </td>
		          </tr>
		          <tr>
		                    <#break>
		                  </#if>
		              </#list>
		            <td class="featureRow">
		              <div class="features">
		 		        <#list group.features as feature>
		 		        <#assign currentTabIndex=currentTabIndex + 1>
		 		        <#assign featureCount=featureCount + 1>
		                       <#if features?has_content && features?seq_contains(feature.id)>
		 		          <span id="feature${featureCount}">		  
		                    <label for="features-g${group_index}-f${feature_index}" class="checkboxLabel">${stack.findValue("getText('${feature.name}')")}</label> 
		                    <br />
		                  </span>
		                       </#if>
		                </#list>
		              <div>
		            </td>
		          </tr>
		 		  <#else>	
		 		  <tr>
		 		    <td>
		              <input tabindex="${currentTabIndex}" class="checkbox" type="checkbox" name="featureGroups" value="${group.id?c}" id="featureGroups-${group_index}" onclick="javascript:toggleFeatureGroup(this.id);"/>
						<script type="text/javascript">
				 			MochiKit.Signal.connect("featureGroups-${group_index}","onfocus",showFocusOnCheckbox);
				 			MochiKit.Signal.connect("featureGroups-${group_index}","onblur",loseFocusOnCheckbox);
			 			</script>
		              <label for="featureGroups-${group_index}" class="checkboxGroupLabel">${stack.findValue("getText('${group.name}')")} (${stack.findValue("getText('${group.description}')")})</label>
		            </td>
		          </tr>
		          <tr>
		            <td class="featureRow">
		              <div class="features">
		 		        <#list group.features as feature>
		 		        <#assign currentTabIndex=currentTabIndex + 1>
		 		        <#assign featureCount=featureCount + 1>
		 		          <span id="feature${featureCount}">		  
		                    <input tabindex="${currentTabIndex}" class="checkbox" type="checkbox" name="features" value="${feature.id?c}" id="features-g${group_index}-f${feature_index}" onclick="javascript:checkedFeatureInGroup(this.id);"
		                       <#if features?has_content && features?seq_contains(feature.id)>
		                    	checked="true"
		                       </#if>
		                    />
							<script type="text/javascript">
					 			MochiKit.Signal.connect("features-g${group_index}-f${feature_index}","onfocus",showFocusOnCheckbox);
					 			MochiKit.Signal.connect("features-g${group_index}-f${feature_index}","onblur",loseFocusOnCheckbox);
				 			</script>
		                    <label for="features-g${group_index}-f${feature_index}" class="checkboxLabel">${stack.findValue("getText('${feature.name}')")}</label> 
		                    <br />
		                  </span>
		                </#list>
		              <div>
		            </td>
		          </tr>
		          </#if>
		          </#list>
				  </table>
				</div>
			</div>
		</div>
	</div>
    <div class="formAction">
		<#assign submitIndex=currentTabIndex + 1>
		<#assign cancelIndex=currentTabIndex + 2>
		
		<#if roleId?has_content>
			<#if !actionErrors?has_content>
				<#assign submitOnChangeForm="form1">
			</#if>
		</#if>
		
		<#include "/include/form/buttonbar.ftl">
    </div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility:hidden;overflow:auto">
	<div class="modifiedTitle">
		<@s.text name='modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('role.name')}" value="${(modifiedEntity.name)?default('')}" />
	    <@s.label label="%{getText('roles.column.description')}" name="modifiedEntity.description" value="${(modifiedEntity.description)?default('')}" />
	</div>
	<div class="formMiddle formMiddlePadded">
		<div class="wwgrp">
			<div class="wwctrlgrp">
				<div class="wwlbl">
					<label for="CreateRole_features" class="label"><@s.text name="role.create.label.features"/></label>
				</div>
				<div class="wwctrl">
				  <table class="features">
		 		  <#list featureGroups as group>
		 		  <tr>
		 		    <td>
		              <span class="checkboxGroupLabel">${stack.findValue("getText('${group.name}')")}</span>
		            </td>
		          </tr>
		          <tr>
		            <td class="featureRow">
		              <div class="features">
		 		        <#list group.features as feature>
		                	<#if modifiedFeatures?has_content && modifiedFeatures?seq_contains(feature.id)>
								${stack.findValue("getText('${feature.name}')")}
		                    	<br />
		                	</#if>
		                </#list>
		              <div>
		            </td>
		          </tr>
		          </#list>
				  </table>
				</div>
			</div>
		</div>
	</div>
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  height:"500",
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>

</@s.form>
</div>
</body>
</html>

