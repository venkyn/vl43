<title><@s.text name='role.view.title'/></title>
<#include "/include/common/tablecomponent.ftl">
  	<#assign extraParamsHash = {}>
 	<@tablecomponent
  		varName="roleObj"
		columns=roleColumns
		tableId="listRolesTable"
		url="${base}/admin/role"
		tableTitle='role.view.title'
    	viewId=view_Id
    	type=""
    	extraParams=extraParamsHash?default("") />
