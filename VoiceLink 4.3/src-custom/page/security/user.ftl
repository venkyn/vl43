<#assign readOnly="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='user.edit.title'/>
  </#assign>
  <#assign formName="edit">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='user.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='user.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='user.view.currentUser.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<#if authenticateAgainstDirectoryService == true>
	<#assign disabledPassword="true"/>
<#else>
	<#assign disabledPassword="false"/>
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>
<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/admin/user" action="${formName}.action" method="POST">
<#if userId?has_content>
    <@s.hidden name="userId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@s.textfield tabindex="1" label="%{getText('user.name')}" name="user.name" id="user.name" size="25" maxlength="128" required="true" readonly="${readOnly}"/>
	<#if readOnly="false">
		<#if authenticateAgainstDirectoryServiceAvailable == true>
			<@s.checkbox tabindex="2" onclick="changeAuthenicationValue(this); " theme="css_xhtml" name="authenticateAgainstDirectoryService" id="authenticateAgainstDirectoryService" cssClass="checkbox" label="%{getText('user.authenticate.directoryservice')}" readonly="${readOnly}"/>
			<@s.hidden id="hiddenAuthenticateAgainstDirectoryService" name="hiddenAuthenticateAgainstDirectoryService" value="${disabledPassword}"/>
		    <@s.password tabindex="3" label="%{getText('user.create.label.password')}" name="password" id="user.password" size="25" maxlength="25" showPassword="true" required="true" disabled="${disabledPassword}"/>	
		    <@s.password tabindex="4" label="%{getText('user.create.label.confirmPassword')}" name="confirmPassword" id="user.confirmPassword" size="25" maxlength="25" showPassword="true" required="true" disabled="${disabledPassword}"/>
		<#else>
		    <@s.password tabindex="3" label="%{getText('user.create.label.password')}" name="password" id="user.password" size="25" maxlength="25" showPassword="true" required="true"/>	
		    <@s.password tabindex="4" label="%{getText('user.create.label.confirmPassword')}" name="confirmPassword" id="user.confirmPassword" size="25" maxlength="25" showPassword="true" required="true"/>
		</#if>
	<#else>
		<#if authenticateAgainstDirectoryServiceAvailable == true>
			<@s.label label="%{getText('user.authenticate.directoryservice')}" value="${disabledPassword}" />
		</#if>		
	</#if>
</div>
<div class="formMiddle">

    <@s.checkboxlist tabindex="5" theme="css_xhtml" label="%{getText('user.column.roles')}" name="roleIds" list="roleMap" listKey="value" listValue="key" cssClass="checkbox" required="true" readonly="${readOnly}"/>  
    <#assign ALL_SITE_ACCESS=1>
	<#if numberOfSites != 1 >
	    <div id="siteSelection" >
	    
	    <#if readOnly="false">
	        <@s.label for="siteSelection" required="true" label="%{getText('user.create.label.sites')}" />
	        <div style="position:relative;top:-40px;">
		    <#if loggedOnUserSiteAccess==ALL_SITE_ACCESS>
		       <@s.radio tabindex="50" theme="css_xhtml" label="" name="userSiteAccessString" id="siteAccess" list="siteRadioMap" cssClass="radio" value="userSiteAccessString"/>	       
	           <script type="text/JavaScript">
	               connect('siteAccess1', 'onclick', setAllSiteAccess);
	               connect('siteAccess0', 'onclick', setSelectiveSiteAccess);
	           </script>		             
		    </#if>
		    <#if loggedOnUserSiteAccess==ALL_SITE_ACCESS>
		      <#if "${formName}" == "create">
		        <#if userSiteAccess==ALL_SITE_ACCESS>
		           <@s.checkboxlist readonly="false" tabindex="100" theme="css_xhtml"  label="" name="siteIds" list="siteMap" listKey="value" listValue="key" cssClass="checkbox" disabled="true"/>	         		  			      
		        <#else>
		           <@s.checkboxlist readonly="false" tabindex="100" theme="css_xhtml"  label="" name="siteIds" list="siteMap" listKey="value" listValue="key" cssClass="checkbox"/>	         		  			         
		        </#if>   
		      <#else>
		        <#if userSiteAccess==ALL_SITE_ACCESS>
		           <@s.checkboxlist tabindex="100" theme="css_xhtml"  label="" name="siteIds" list="siteMap" listKey="value" listValue="key" cssClass="checkbox" disabled="true"/>	         		  			      
		        <#else>
		           <@s.checkboxlist tabindex="100" theme="css_xhtml"  label="" name="siteIds" list="siteMap" listKey="value" listValue="key" cssClass="checkbox"/>	         		  			         
		        </#if>   
		      </#if>
		    <#else>
		      <#if "${formName}" == "create">
		        <#if userSiteAccess==ALL_SITE_ACCESS>
		           <@s.checkboxlist tabindex="100" theme="css_xhtml"  label="" name="siteIds" list="siteMap" listKey="value" listValue="key" cssClass="checkbox" disabled="true"/>	         		  			      
		        <#else>
		           <@s.checkboxlist tabindex="100" theme="css_xhtml"  label="" name="siteIds" list="siteMap" listKey="value" listValue="key" cssClass="checkbox"/>	         		  			         
		        </#if>   
		      <#else>
		        <#if userSiteAccess==ALL_SITE_ACCESS>
		            <@s.label label="" value="%{getText('user.edit.label.allSiteAccess')}" />
		        <#else>
		           <@s.checkboxlist tabindex="100" theme="css_xhtml"  label="" name="siteIds" list="siteMap" listKey="value" listValue="key" cssClass="checkbox"/>	         		  			         
		        </#if>   
		      </#if>
		    </#if>  
		    </div>
	    <#else>
	        <#if userSiteAccess==ALL_SITE_ACCESS>
		        <@s.label label="%{getText('user.create.label.sites')}" value="%{getText('site.context.all')}" required="true"/>
	        <#else>
	            <@s.checkboxlist readonly="true" tabindex="100" theme="css_xhtml"  label="%{getText('user.create.label.sites')}" name="siteIds" list="siteMap" listKey="value" listValue="key" cssClass="checkbox" required="true"/>	         		  			             
	        </#if>
	    </#if>	       
	    </div>
	<#else>
		<@s.iterator value="siteMap">
			<#if userSiteAccess==ALL_SITE_ACCESS>
				<#assign itemKey = stack.findValue("value")/>
			    <input type="hidden" value="1" name="userSiteAccess"/>
			<#else>
				<#assign itemKey = stack.findValue("value")/>
			    <input type="hidden" value="${itemKey?c}" name="siteIds"/>
			    <input type="hidden" value="0" name="userSiteAccess"/>
			</#if>
		</@s.iterator>
	</#if>
    <@s.select tabindex="196" label="%{getText('user.column.status')}" name="user.enabled" id="user.status" list="statusMap" cssClass="userStatusSelect" readonly="${readOnly}"/>
</div>
<div class="formBottom">
    <@s.textfield tabindex="197" label="%{getText('user.emailAddress')}" name="user.emailAddress" id="user.emailAddress" size="25" maxlength="256" readonly="${readOnly}"/>

    <@s.textarea tabindex="198" theme="css_xhtml" label="%{getText('user.notes')}" name="user.notes" id="user.notes" rows="2" cols="50" readonly="${readOnly}"/>
<#if userId?has_content>
    <@s.label theme="css_xhtml" label="%{getText('user.create.label.lastLogin')}" name="lastLoggedIn" id="user.lastLoggedIn"/>
</#if>
</div>
<div class="formAction">
	
	<#if userId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('user.name')}" name="modifiedEntity.name"/>
	    <#if authenticateAgainstDirectoryServiceAvailable == true>
	    	<@s.label label="%{getText('user.authenticate.directoryservice')}" value="${disabledPassword}" />
	    </#if>
	</div>
	<div class="formMiddle">
	    <@s.label label="%{getText('user.column.roles')}" name="modifiedUserRoleNamesAsString"/>
	    <@s.label label="%{getText('user.create.label.sites')}" name="modifiedUserSiteNamesAsString"/>
	    <@s.label label="%{getText('user.column.status')}" name="modifiedStatus"/>
	</div>
	<div class="formBottom">
	    <@s.label label="%{getText('user.emailAddress')}" name="modifiedEntity.emailAddress"/>
	
	    <@s.textarea theme="css_xhtml" label="%{getText('user.notes')}" name="modifiedEntity.notes" rows="2" cols="40" disabled="true"/>
	    <@s.label theme="css_xhtml" label="%{getText('user.create.label.lastLogin')}" name="lastLoggedIn" />
	</div>
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  zIndex: 500,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render(document.body);
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>

<script type="text/JavaScript">
	if( $("form1.submit1") ) {
		connect('form1.submit1', 'onclick', enablePasswordFields)
	}
</script>

</body>
</html>

