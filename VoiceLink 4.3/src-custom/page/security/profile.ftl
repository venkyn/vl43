<#assign formName="profile">
<html>
<head>
<title><@s.text name="user.profile.title"/></title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	<@s.text name="user.profile.title"/>
	
</div>

<@s.form id="form1" name="${formName}" validate="true" namespaceVal="/admin/user" action="${formName}.action" method="POST">
<@s.hidden name="profileEdit"/>
<#if userId?has_content>
    <@s.hidden name="userId"/>
</#if>
<#if savedEntityKey?has_content>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@s.label label="%{getText('user.name')}" name="user.name"/>

	<#if authenticateAgainstDirectoryService == false>
    	<@s.password tabindex="1" label="%{getText('user.create.label.password')}" name="password" size="25" maxlength="20" show="true" required="true"/>
    	<@s.password tabindex="2" label="%{getText('user.create.label.confirmPassword')}" name="confirmPassword" size="25" maxlength="20" show="true" required="true"/>
    </#if>
</div>
<div class="formMiddle">
        <@s.label label="%{getText('user.column.roles')}" value="${userRoleNamesAsString}"/>
</div>
<div class="formBottom">
    <@s.textfield tabindex="3" label="%{getText('user.emailAddress')}" name="user.emailAddress" size="25" maxlength="256"/>
    <@s.label theme="css_xhtml" label="%{getText('user.create.label.lastLogin')}" name="lastLoggedIn"/>
</div>
<div class="formAction">

	<#assign submitOnChangeForm="form1">

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>

<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility:hidden;">
	<div class="modifiedTitle">
		<@s.text name='modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('user.name')}" name="modifiedEntity.name"/>
	</div>
	<div class="formMiddle">
	    <@s.label label="%{getText('user.column.roles')}" name="modifiedUserRoleNamesAsString"/>
	
	    <@s.label label="%{getText('user.column.status')}" name="modifiedStatus"/>
	</div>
	<div class="formBottom">
	    <@s.label label="%{getText('user.emailAddress')}" name="modifiedEntity.emailAddress"/>
	</div>
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>

</body>
</html>

