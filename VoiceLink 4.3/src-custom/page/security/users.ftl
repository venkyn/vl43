<title><@s.text name='user.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">
 	<#assign extraParamsHash = {}>
 	<@tablecomponent
  		varName="userObj"
		columns=userColumns
		tableId="listUsersTable"
		url="${base}/admin/user"
		tableTitle='user.view.title'
    	viewId=userViewId
    	type=""
    	extraParams={"extraURLParams":[{"name":"tempSiteID","value":"${currentSiteID?c}"}]} />
