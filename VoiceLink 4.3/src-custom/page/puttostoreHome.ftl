<title><@s.text name='voicelink.puttostore.home.title'/></title>
<#include "/include/common/configurablehomepage.ftl">

 <@configurablehomepage 
   summaries = summaries 
   summariesLocation = summariesLocation
   rows=3
   columns=2
   homepageId=homepageId
  />