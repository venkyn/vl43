<title><@s.text name='epp.home.title'/></title>

<#include "/include/common/configurablehomepage.ftl">
 
<#assign noAckMessage>
    <@s.text name='notification.summary.noUnacknowledged'/>
</#assign>
    	
 <@configurablehomepage 
   summaries = summaries 
   summariesLocation = summariesLocation
   rows=3
   columns=3
   homepageId=homepageId
  />

