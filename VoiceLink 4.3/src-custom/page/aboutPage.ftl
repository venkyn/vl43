<#assign title>
	<@s.text name='about.page'/>
</#assign>

<html>
<head>
<title>${title}</title>
</head>
 <body onLoad="javascript:test();">
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="viewLicenseCopyright" id="form1" validate="false" namespaceVal="/aboutPage" action="aboutPage.action" method="POST">
	<div class="formTop">
		<div id="innerContent" style="position:relative;margin-left: 0px;margin-top: 20px;">
			<b>Vocollect Applications</b></br></br>
			<#if applicationList?has_content>
				<#list applicationList as applicationDetails>
					<#list applicationDetails as applicationDetail>
						<#if applicationDetail_index == 0>
							<@s.text name='about.applicationName'/>
							<b><@s.text name='${applicationDetail}'/></b>
						</#if>
						<#if applicationDetail_index == 1>
							<@s.text name='about.version'/>
							<b><@s.text name='${applicationDetail}'/></b>
						</#if>
						<#if applicationDetail_index == 2>
							<@s.text name='about.buildNumber'/>
							<b><@s.text name='${applicationDetail}'/></b>
						</#if>
						</br>
					</#list>
					</br>
				</#list>
				<@s.text name='about.CSID'/><b>:    <@s.text name='about.CSID.value'/></b>
				</br>
				<@s.text name='about.SYS'/><b>:     <@s.text name='about.SYS.value'/></b>
				</br>                 
				</br>
			</#if>
            Copyright &copy; 2014 Vocollect, Inc., a subsidiary of Honeywell International Inc. All rights reserved.</b><br><br>
			The trademarks, logos and service marks (Marks) used on this product are the property of Vocollect or other third parties.  You are not permitted to use the Marks without the prior written consent of Vocollect or such third party that may own the Marks.  Vocollect, Vocollect Voice, and the Vocollect logo are trademarks of Vocollect.  All other trademarks not owned by Vocollect that appear on this product are the property of their respective owners, who may or may not be affiliated with, connected to, or sponsored by Vocollect.<br><br>
			Reference to any product, process, publication, service or offering of any third party by trade name, trademark, manufacturer or otherwise does not necessarily constitute or imply the endorsement or recommendation of such by Vocollect.<br><br><br>

			For patent information, see <a href="http://www.honeywellaidc.com/Pages/patents.aspx">http://www.honeywellaidc.com/Pages/patents.aspx</a><br><br>	
			<b>License Information regarding Bundled Third-Party Software</b><br><br>
			This product includes code licensed from RSA Data Security. It also includes software developed by the following:<br>
			<table id="thirdpartytable" cellspacing="0">
				<thead>
				<th align="left"><@s.text name='about.thirdpartytable.package.column'/></th>
				<th align="left"><@s.text name='about.thirdpartytable.license.column'/></th>
				<th align="left"><@s.text name='about.thirdpartytable.sourcecodeavailability.column'/></th>
				</thead>
	
				<tbody>
					<#if thirdPartyComponents?has_content>
						<#list thirdPartyComponents as thirdPartyComponentDetails>
							<#if thirdPartyComponentDetails_index % 2 == 0>
								<tr>
							<#else>
								<tr class="odd">
							</#if>
							<#list thirdPartyComponentDetails as thirdPartyComponentDetail>
								<td>${thirdPartyComponentDetail}</td>
							</#list>
							</tr>
						</#list>
						</br>
					</#if>
				</tbody>
			</table>
		</div>
	</div>
</@s.form>

</body>
</html>  
