<html>
  <head>
	<title><@s.text name='log.view.title'/></title>
	<#include "/include/common/formatfilesize.ftl">
  </head>
  <body>
	<div class="formBody">
	<div class="titleBar">
		<@s.text name='log.view.title'/>
	</div>
    <@s.form id="form1" name="readLog" action="/admin/log/zip.action" method="POST" theme="css_xhtml">
	  <input type="hidden" name="fileName" value="${requestedFile.fileName}">
	  <div class="logAttributes">
		<TABLE VALIGN="top" WIDTH="300px">
			<tr><td><B><@s.text name='log.view.attribute.name'/></B></td><td> ${requestedFile.displayName}</td></tr>
			<tr><td><B><@s.text name='logs.column.type'/></B></td><td> <@s.text name='log.type.${requestedFile.fileType?lower_case}'/></td></tr>
			<tr><td><B><@s.text name='logs.column.size'/></B></td><td> <@formatfilesize sizeInBytes="${requestedFile.actualFileSize?c}"/></td></tr>
			<tr><td><B><@s.text name='log.view.attribute.date'/></B></td><td> ${requestedFile.modificationDate?datetime?string.short_long}</td></tr>
		</TABLE>
		<br>
		<#list actionMessages as actionError>
		  <li><span class="errorMessage">${actionError}</span></li>
		</#list>
	  </div>	
	  <#if requestedFile.actualFileSize != 0 >

	    <div class="formMiddle" style="padding-top:10px;">
		</div>
		<#assign loglineCount = 0>
		
		<#if (requestedFile.actualFileSize >= maxFileSize)>
			<div id="codeDiv" class="logDisplayMessage" style="padding-left: 25px;margin-bottom: 13px">
				${fileTooLarge}
			</div>
		<#else>
			<div style="margin-top:10px;">
				<#list fileContents as logLine>
					<#assign loglineCount = loglineCount + 1>
					<div id="codeDiv-${logLine_index}"
						<#if loglineCount % 2 = 0>
							class="logDisplayOdd"
						<#else>
							class="logDisplayEven"
						</#if>
						>
						${logLine}
					</div>
				</#list>
			</div>
		</#if>
		
	  <#else>
	  	<div style="height:50px;">
	  	&nbsp;
	  	</div>
	  </#if>
    </@s.form>
  </body>
</html>
