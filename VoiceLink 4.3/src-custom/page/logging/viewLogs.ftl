<title><@s.text name='logs.view.title'/></title>
<#include "/include/common/tablecomponent.ftl">
  		<#assign extraParamsHash = {}>
	 
	 	<@tablecomponent
     		varName="logObj"	 	
			columns=logColumns
			tableId="listLogsTable"
			url="${base}/admin/log"
			tableTitle='logs.view.title'
	    	viewId=view_Id
	     	type=""
	     	extraParams=extraParamsHash?default("") />	
