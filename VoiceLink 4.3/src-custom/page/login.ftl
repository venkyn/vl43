<#assign title>
	<@s.text name="login.title"/>
</#assign>
<html>
<head>
<title>${title}</title>
</head>
<body>
<script type="text/javascript">
noTimeout = 1;
</script>
<div class="title">
	${title}
</div>
<div style="position:absolute;left:0px;top:50px;text-align:center;width:100%;font-size:12;font-family:Arial,Helvetica,San-Serif;">
	<#if (request.getParameter("error"))?exists>
     	<@s.text name="login.error.message"/>
    </#if>
	<#if (request.getParameter("logout"))?exists>
     	<@s.text name="login.logout.message"/>
    </#if>
</div>
<form name="loginForm" action="j_acegi_security_check" method="POST" id="form1">
<div class="formdata">
	<table>
	  <tr>
	    <td><label><@s.text name="login.username"/></label></td>
	    <td><input tabindex="1" class="inputElement" type="text" name="j_username" size="25" id="firstInput"></td>
	  </tr>
	  <tr>
	    <td><label><@s.text name="login.password"/></label></td>
	    <td><input tabindex="2" class="inputElement" type="password" name="j_password" size="25"></td>
	  </tr>
	</table>
</div>
<#assign submitText>
  <@s.text name="login.button.submit"/>
</#assign>

<div align="center" style="margin-top:10px;">

	<@s.submit type="button"
				tabindex="199" 
				theme="css_xhtml"
				value="${submitText}"
				name="form1.submit1"
				cssClass="loginButton"
				id="form1.submit1"/>
	<script type="text/javascript">
		connect("form1.submit1","onmouseover",addMouseOverClass);
		connect("form1.submit1","onmouseout",removeMouseOverClass);
		
		function setFocusOnFirstInput() {
			$("firstInput").focus();
		}
		
		connect(window, "onload", setFocusOnFirstInput);		
	</script>
</div>
</form>	
</body>
</html>  
