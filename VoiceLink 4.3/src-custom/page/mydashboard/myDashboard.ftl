<#assign title ="chart auto refresh in iframe"> 
<#include "/decorators/dashboard.ftl">
<script type="text/javascript" src="${base}/scripts/fusioncharts/fusioncharts.js"></script>
<script type="text/javascript" src="${base}/scripts/fusioncharts/fusioncharts.jqueryplugin.js"></script>
<script type="text/javascript" src="${base}/scripts/jquery/plugins/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_grid_chart.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/voc_chart.js"></script>
<script type="text/javascript" src="${base}/scripts/vocollect/chartUtilities.js"></script>
<#assign pageContext = "${base}/${navigation.applicationMenu}">

<#if currentDashboard?has_content>
	<script>
		var redErrorStatusMessage = "<@s.text name='status.stopped.updating'/>";
		var redErrorDefaultMessage =  "<@s.text name='lastUpdateError.defaultErrorMessage'/>";
		var noDataTextMessage = "<@s.text name='dashboard.noData.emptyData.message'/>";
		var noDataChangeInParentMessage =  "<@s.text name='dashboard.noData.parentChanged.message'/>";
		
		//disabling session tracking to avoid time out on my dashboard iFrame 
		//(if session times out on my dashboard page because of session time- out on some other open tab in the browser 
		//and session time out is not disabled separately for iFrame then iFrame shows session time- out 
		//because iFrame stops redrawing after session time out on my Dashboard parent page till the user logs back in through any page)
		disconnect(sessionHandler);
	
		//array to store chartComponent objects in order to reuse the objects instead of creating a new one every time a chart is drawn
		var chartComponentObjArray = [];
		var maxFrameHeight = 0;
		
		$j(function(){				
			
			handleDialogFocusBehavior();
			
			drawDashboardSelect();
			
			<#if currentDashboard.id?has_content>
		    	drawCharts(${currentDashboard.id?c});
		    </#if>
		    
		    //added this to show top of iFrame after all charts are drawn
			window.parent.scrollWindow(window.parent.scrollYVar);
		});
		
		function handleDialogFocusBehavior() {
			//Overiding the event handling function to empty to prevent dialog and scroll jumps if scrolled down and did some interaction with dialog
			//which were getting caused due to logic of jquery to identify an element to focus on.
			$j.ui.dialog.prototype._focusTabbable = function(){};
			
			//Overiding the event handling function to empty to prevent dialog and scroll jumps if foucused out of current dialog and clicked on another one
			//which were getting caused due to z- index handling by jQuery to display selected dialog on top.
			//jquery bug reference link: http://bugs.jqueryui.com/ticket/9166
			$j.ui.dialog.prototype._moveToTop = function( event, silent ) {
				var $jparent = this.uiDialog.parent();
				var $jelementsOnSameLevel = $jparent.children();
				
				var heighestZIndex = 0;
				$j.each($jelementsOnSameLevel, function(index, element) {
					var zIndexOfElement = $j(element).css('z-index');
					if (zIndexOfElement) {
						var zIndexOfElementAsNumber = parseInt(zIndexOfElement) || 0;
						if (zIndexOfElementAsNumber > heighestZIndex) {
							heighestZIndex = zIndexOfElementAsNumber;
						}
					}
				});
				var currentZIndex = this.uiDialog.css('z-index');
				
				var moved;
				if (currentZIndex >= heighestZIndex) {
					moved = false;
				} else {
					this.uiDialog.css('z-index', heighestZIndex + 1);
					moved = true;
				}
				
				if ( moved && !silent ) {
					this._trigger( "focus", event );
				}
				
				return moved;
			}
		}
	
		function adjustFrame() {
			var maxHeight = 0;
			$j("div[id^='chart-div']").each(function(index, chartDivObj) {
				var chartDivObjId = chartDivObj.id;
				var chartId = chartDivObjId.substring(10);				
				var chartComponentObj = chartComponentObjArray[chartId];
		
				var chartDiv = $j(chartDivObj);
				var divLowerY = 0;
				if (chartDiv.hasClass('ui-dialog-content')) {
					divLowerY = chartComponentObj.chartY +  chartComponentObj.dialogHeight;
				}
				
				maxHeight = maxHeight > divLowerY ? maxHeight : divLowerY;
			});
			
			maxFrameHeight = maxHeight;
			window.parent.adjustFrameSize(maxHeight);
		}
		
		//get ids of charts to draw
		function getChartsToDrawIds(dashboardId) {
			var output = null;
			$j
					.ajax({
						async : false,
						url : globals.BASE + '/dashboardalert/mydashboard/getChartIds.action',
						data : {
							dashboardId : dashboardId
						},
						datatype : 'json',
						success : function(d) {
							output = d;
						}
					});
		
			return output;
		}
	
		function drawCharts(dashboardId) {
		
			var chartsToDrawIds = getChartsToDrawIds(dashboardId);
			var x = 20;
			var y = 65;
			
			$j.each( chartsToDrawIds.chartIds, function(index, chartObj) {
				$j("<div></div>").attr('id','chart-div-' + chartObj.id).appendTo($j('#chart-parent')).css({'padding-top' : '0px', 'padding-bottom' : '1px', 'padding-left' : '0px', 'padding-right' : '0px'}); 
				
				var parentChartFilterParams = [];
				 
				var chart = new ChartComponent({
													'dashboardId' : dashboardId, 
													'chartId' : chartObj.id,
													'parentChartsFilterFields' : parentChartFilterParams, 
													'chartX' : x, 
													'chartY' : y
											});
											
				chartComponentObjArray[chartObj.id] = chart;
	            chart.getChartData();
				y=y+360;
			});
		}
		
		function drawDashboardSelect() {
		
			var dashboard_select_label = $j('<label/>');
			$j(dashboard_select_label).css({
											"font-size" : "15px",
											"font-weight" : "Bold",
											"padding-right" : "8px"
										});
			$j(dashboard_select_label).append("Dashboards");
			
			var dashboard_select = $j('<select/>', {
										'align' : 'left',
										'id' : 'dashboardSelectId'
									});										
			$j(dashboard_select).css("padding", "2px");
			
			var dashboardOptions = ${dashboards};			
			var properties = [];
			
			$j(dashboardOptions).each(function(index, d) {
				var option = $j('<option/>', {
					'value' : d.id
				});

				$j(option).text(d.name);

				$j(dashboard_select).append(option);
			});
			
			$j('#dashboardSelectDiv').append(dashboard_select_label);
			$j('#dashboardSelectDiv').append(dashboard_select);
			
			<#if currentDashboard.id?has_content>
				$j(dashboard_select).val(${currentDashboard.id?c}).attr("selected", "selected");
			</#if>
			
			var dashboardSelectPreVal = $j('#dashboardSelectId option:selected').val();
			
			$j(dashboard_select).change(function(){
			
				var selectedDashboardVal = $j('#dashboardSelectId option:selected').val();
				
				if (isNaN(selectedDashboardVal)) {
					$j(dashboard_select).val(dashboardSelectPreVal).attr("selected", "selected");
				}
				else {
					$j.ajax({
						async : false,
						url : globals.BASE
								+ '/dashboardalert/mydashboard/showDashboardForMyDashboard.action',
						data : {
							dashboardId : selectedDashboardVal
						},
						datatype : 'json',
						success : function(data) {
							if (data.errorCode != null && data.errorCode == 1) {
								window.parent.writeStatusMessage(data.message, 'actionMessage error');
								window.parent.$j('#messages').show();			
								dashboardSelectPreVal = selectedDashboardVal;
							} 
							else {
								window.parent.location= "${pageContext}/mydashboard/myDashboard.action?dashboardId=" + selectedDashboardVal;
							}
						}	
					});
				}
			});
		}
	</script>

	<style type="text/css">
	    div.ui-widget-header {
	        border: 1px solid #ffffff;
	        background: #ffffff;
	        color: #4c80ad;
	        font-weight: bold;
	        font-size: 12px
	    }
	    
	    div.ui-resizable-handle{
	        position: absolute;
	        background: transparent;
	    }
	
	    .ui-dialog .ui-dialog-titlebar-close span {
	        display: block;
	        margin: -8px;
	    }
	    
	    div.ui-widget-content div.ui-icon {
	        background-image: none;
	    }
	</style>

	<div align = "right" id="dashboardSelectDiv">
	</div>

    <div align="center">
        <h1>
            ${currentDashboard.name}
        </h1>
        <#if currentDashboard.description?has_content>
            <h4>
                ${currentDashboard.description}
            </h4>
        </#if>
    </div>
    
	<div align="left" id="chart-parent">
	</div>

</#if>