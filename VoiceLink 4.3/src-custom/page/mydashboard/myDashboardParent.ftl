<script>
	//disabling session tracking to avoid time out on my Dashboard parent page
	disconnect(sessionHandler);

	var windowNavigateAway = 0;
	var frameNavigateAway = 0;
	
	var scrollYVar = 0;
	
	$j(window).bind('beforeunload', function(){
		windowNavigateAway = 1;
	});
	
	$j(window).bind('resize', function(){
		var newWidth = $j(window).width() - 200;
		$j("#chartFrame").css({
			width: newWidth
		});
		$j('#chartFrame')[0].contentWindow.reAlignCharts(newWidth);
		
	});
	
	var framesrc = "myDashboardFrame.action";
	
	<#if currentDashboard.id?has_content>
		framesrc += "?dashboardId=${currentDashboard.id?c}";
	</#if>
	
	createiFrame(framesrc);
		
	function createiFrame(framesrc) {
		var target = document.getElementById("content");
		var newFrame = document.createElement("iframe");
		newFrame.setAttribute("src", framesrc);
		newFrame.setAttribute("id", "chartFrame");
		newFrame.setAttribute("frameborder", "0");
		newFrame.setAttribute("scrolling", 'no');
		newFrame.setAttribute("width", $j(window).width()  - 200);
		newFrame.setAttribute("height",$j(window).height()  - 50);
		target.appendChild(newFrame);
	}
	
	function adjustFrameSize(newSize) {
		if(newSize < ($j(window).height() - 100)) {
			return;
		}
		
		$j("#chartFrame").css({
			height: newSize + 25
		});
	}
	
	function scrollWindow(position) {
		$j(window).scrollTop(position);
	}	
</script>
