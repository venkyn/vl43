<#assign title>
   <@s.text name='licensing.view.title'/>
</#assign>
<#if navigation.pageName="enterLicenseFile">
	<#assign titlebar>
	   <@s.text name='licensing.import.titlebar'/>
	</#assign>
<#elseif navigation.pageName="showAgreement">
	<#assign titlebar>
	   <@s.text name='licensing.agreement.titlebar'/>
	</#assign>	
<#elseif navigation.pageName="agreement">
	<#assign titlebar>
	   <@s.text name='licensing.agreement.titlebar'/>
	</#assign>		
<#else>
	<#assign titlebar>
	   <@s.text name='licensing.view.titlebar'/>
	</#assign>
</#if>
<#assign labelMessage>
   <@s.text name='licensing.import.file.message'/>
</#assign>
<#assign throwException>
<@s.property value="throwException"/>
</#assign>
 <#assign formName="import">
 
<html>
   <head>
        <title>${title}</title>
    </head>
  <body>
  	<div class="formBody">
	<div class="titleBar">${titlebar}</div>
			<@s.form name="${formName}" id="form1" namespaceVal="/admin/licensing" validate="true" action="licenseFlow" method="POST" enctype="multipart/form-data">
			<div class="formTop">
      			
      		<@s.hidden name="eventId" value="submit"/>
      		
      		<@s.file id="doc" name="doc" label='${labelMessage}' required='true'></@s.file>

      		</div>
      		<div class="formAction" style="position:relative;margin-top: 20px">
				<#if !actionErrors?has_content>
					<#assign submitOnChangeForm="form1">
				</#if>
				<#assign cancelHref="licenseFlow.action?eventId=cancel">
				<#assign submitIndex=199>
				<#assign cancelIndex=200>
				<#assign enabled=false>
				<#if !submitValue?has_content>
	  				<#assign submitValue>
						<@s.text name="license.button.submit"/>
	  					</#assign>
  			    </#if>

				<#include "/include/form/buttonbar.ftl">
			</div>
  			</@s.form>
  </div>
  </body>
</html>
         
