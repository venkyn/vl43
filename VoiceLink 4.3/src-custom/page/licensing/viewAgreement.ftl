<#assign title>
   <@s.text name='licensing.view.title'/>
</#assign>
<#assign titlebar>
   <@s.text name='licensing.agreement.titlebar'/>
</#assign>
<#assign licenseNumber>
   <@s.text name='licensing.license.number'/>
</#assign>
 <#assign formName="agreement">
<html> 
<head> 
 <title>${title}</title>
 </head> 
 
 <body>
   <div class="formBody">
    <div class="titleBar">${titlebar}</div> 
    <@s.form name="${formName}" id="form1" validate="false" action="licenseFlow" method="POST" enctype="multipart/form-data" >
    <@s.hidden name="eventId" value="acceptAgreement"/>
    <div class="formTop">
        <br/>
	    <div class="link">
	      <a href="${base}/admin/licensing/agreement.action?printableview=true" target="_blank">
	        <@s.text name="nav.licensing.printableview"/>
	      </a>
	    </div>
        <br/>
        <#include "/include/common/licenseAgreement.ftl">
        <br/>
        <div class="emphasizeText">BY CLICKING "I ACCEPT THE LICENSE AGREEMENT" YOU ACCEPT ALL THE TERMS AND CONDITIONS IN THIS 
        AGREEMENT AND INTEND TO BE LEGALLY BOUND BY THEM.</div>
        <br/>
	    <div class="link">
	      <a href="${base}/admin/licensing/agreement.action?printableview=true" target="_blank">
	        <@s.text name="nav.licensing.printableview"/>
	      </a>
	    </div>
     </div>
     <div class="formAction" style="position:relative;margin-top: 20px">
        <!-- Currently there is no way to specify the alternate button text, so
             we can't reuse the button bar. -->
		<input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>
		<a href="javascript:submitForm($('form1'));"
			id="${submitId?default('form1.submit1')}"
			class="${submitCssClass?default('anchorButton')}"
			tabindex="${submitIndex?default('-1')}" >
			<@s.text name='license.agreement.button.submit'/>
		</a>
		<a href="licenseFlow.action?eventId=cancel"
			id="${cancelId?default('form1.cancel1')}"
			class="${cancelCssClass?default('anchorButton')}"
			tabindex="${cancelIndex?default('-1')}">
			<@s.text name='form.button.cancel'/>
		</a>
		<script type="text/javascript">
			MochiKit.Signal.connect("${cancelId?default('form1.cancel1')}","onfocus",showFocusOnLink);
			MochiKit.Signal.connect("${cancelId?default('form1.cancel1')}","onblur",hideFocusOnLink);
		</script>			
	</div>
  	</@s.form>
  </div>   
 </body></html>
           
