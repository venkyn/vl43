<#assign title>
   <@s.text name='licensing.view.title'/>
</#assign>
<#assign titlebar>
   <@s.text name='licensing.agreement.titlebar'/>
</#assign>
<#assign licenseNumber>
   <@s.text name='licensing.license.number'/>
</#assign>
 <#assign formName="agreement">
<html> 
<head> 
 <title>${title}</title>
 </head> 
 <body>
   <div class="formBody">
    <#if !parameters.printableview?exists>
      <div class="titleBar">${titlebar}</div> 
    </#if>
    <div class="formTop">
    	
    		<#if !parameters.printableview?exists>
    		  <br/>
    		  <p>
    		    <div class="link">
    		      <a href="${base}/admin/licensing/agreement.action?printableview=true" target="_blank">
    		        <@s.text name="nav.licensing.printableview"/>
    		      </a>
    		    </div>
    		  </p>
    		<#else>
    		  <a href="javascript:window.close();"><@s.text name="nav.licensing.closewindow"/></a>
    		</#if>
    		<br/>
            <#include "/include/common/licenseAgreement.ftl">
            <#if !parameters.printableview?exists>
              <br/>
    		  <p>
    		    <div class="link">
    		      <a href="${base}/admin/licensing/agreement.action?printableview=true" target="_blank">
    		        <@s.text name="nav.licensing.printableview"/>
    		      </a>
    		    </div>
    		  </p>
    		<#else>
    		  <a href="javascript:window.close();"><@s.text name="nav.licensing.closewindow"/></a>
    		</#if>
     </div>
  </div>   
 </body></html>
           
