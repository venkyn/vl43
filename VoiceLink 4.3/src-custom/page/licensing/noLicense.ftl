<#assign title>
   <@s.text name='licensing.view.title'/>
</#assign>
<#assign titlebar>
   <@s.text name='license.unlicensed.title'/>
</#assign>

 <#assign formName="view">
<html>
   <head>
        <title>${title}</title>
    </head>
  <body>
  <div class="formBody">
	<div class="titleBar">${titlebar}</div>
    <div class="formTop" style="margin-top: 10px;">
        <div class="emphasizeText"><@s.text name='license.unlicensed.comment'/></div>
        <div style="margin-top: 10px;margin-bottom: 15px;font-size: +12;">
        	  <@s.text name="license.would.you.like.to"/>
              <a href="<@s.url includeParams='none'  value='/admin/licensing/beginLicenseFlow.action'/>">
                  <@s.text name="license.unlicensed.options.import"/>
              </a>
              <@s.text name="license.question.mark"/>
        </div>
    </div>
  </body>
 </html>
