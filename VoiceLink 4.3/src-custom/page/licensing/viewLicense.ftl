<#assign title>
   <@s.text name='licensing.view.title'/>
</#assign>
<#assign titlebar>
   <@s.text name='licensing.view.titlebar'/>
</#assign>
<#assign licenseNumber>
   <@s.text name='licensing.license.number'/>
</#assign>
<#assign max_unit_size>
<@s.property value="MAX_LIMIT"/>
</#assign>
<#assign formName="view">
<html>
     <head>
         <title>${title}</title>
     </head>
     <body>
     <div class="formBody">     
		<div class="titleBar">
		    ${titlebar}
		</div>
		<div class="formTop">
		 <script type="text/javascript">
		 	function showExistingErrorsBubble() {
		 	}
		 </script>
			<@s.label label="%{getText('licensing.license.number')}" name="voiceLicense.licenseNumber"/>
			<@s.label label="%{getText('licensing.license.revision')}" name="voiceLicense.licenseRevision"/>
			<@s.label label="%{getText('licensing.creation.date')}" name="voiceLicense.creationDate"/>
			<@s.label label="%{getText('licensing.order.number')}" name="voiceLicense.orderNumber"/>
			<@s.label label="%{getText('licensing.customer.id')}" name="voiceLicense.customerID"/>
			<@s.label label="%{getText('licensing.licensee.name')}" name="voiceLicense.licenseeName"/>
			<@s.label label="%{getText('licensing.server.location')}" name="voiceLicense.licenseeSite"/>
			<@s.label label="%{getText('licensing.license.type')}" name="voiceLicense.licenseType.displayName"/>
			<@s.label label="%{getText('licensing.overdraft.allowance')}" name="voiceLicense.allowance"/>
			<@s.label label="%{getText('licensing.license.expiration')}" name="voiceLicense.expirationDate"/>
			<table class="viewLicenseTable">
				<tr STYLE="font-size : 20pt;">
					<td width=20%><@s.label label="%{getText('licensing.licensed.products')}"/></td>
					<td width=20%><@s.label label="%{getText('licensing.licensed.products.product')}"/></td>
					<td width=20%><@s.label label="%{getText('licensing.licensed.products.version')}"/></td>
					<td width=20%><@s.label label="%{getText('licensing.licensed.products.feature')}"/></td>
					<td width=20%><@s.label label="%{getText('licensing.licensed.products.limit')}"/></td>
			    </tr>
	    		<#list voiceLicense.licensedProducts as l>
	    		<tr>
	    			<td width=20% valign="top" class="regularTable"></td>
		    		<td width=20% valign="top" class="regularTable">
		    		    ${l.name}<#t/>
		    		    <#if l.platform?exists &&
		    		         l.platform?has_content>
		    		        ,&nbsp;<#t/>
		    		        <#assign platformText>
                                <@s.text name="${l.platformAsMsgKey}"/>
                            </#assign>
		    		        <#if !platformText?starts_with("device.type")>
		    		            ${platformText}
		    		        <#else>
		    		            ${l.platform}
		    		        </#if>
		    		    </#if>
		    		</td>
		    		<td width=20% valign="top" class="regularTable">${l.version}</td>    		
					<td width=20% valign="top" class="regularTable">
					<#if l.platform?exists &&
		    		         l.platform?has_content>
		    		         <#-- Display nothing -->
		    		<#else>
						<#list l.productFeatures as f>
							${f.name}<br>
						</#list>
					</#if>
					</td>				
					<td width=20% valign="top" class="regularTable">
					<#if l.platform?exists &&
							l.platform?has_content>
							
							<#assign totalUnits = 0 />
							
							<#list l.productFeatures as f>
							 	<#assign totalUnits = totalUnits + f.units />
							</#list>
							
							<#if totalUnits < 100000 >
						    	 ${totalUnits}<br>
						 	<#else >
						    	 Unlimited<br>
							</#if>
					<#else>
						<#list l.productFeatures as f>
						 	<#if f.units < 100000 >
						    	 ${f.units}<br>
						 	<#else >
						    	 Unlimited<br>
							</#if>
						</#list>
					</#if>
					</td>
				</tr></#list>
			</table>
		</div>
	</div>
   
  </body>
</html>
