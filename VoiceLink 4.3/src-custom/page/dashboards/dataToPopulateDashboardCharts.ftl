<table border="0" class="content" width="260px">
<tr>
<td colspan="2">
<div id="textContainer">
<@s.text name='dashboard.addChart.prompt.body' />
</div>
</td>
</tr>
<tr><td colspan="2">&nbsp;</td></tr> 
<tr>
<td>
<div id="textContainer" style="font-weight: bold;">
<@s.text name="dashboard.label.addChart" /><@s.text name='semicolon'/>
</div>
</td>
<td align="left">
<@s.select tabindex="1" 
		    name="defaultAddChartToDashboard" 
		    id="addChartsToDashboardDropdown" 
		    list="chartsToAddMap" 
			listKey="value" 
			listValue="key"
		    readonly="false"
		    cssClass="addChartToDashboardSelect"
		    theme="css_nonbreaking"/>
</td>
</tr>
</table>