<#assign title>
  <@s.text name='dashboardalert.dashboard.adddrilldownchart.title'/>
</#assign>
<#assign formName="addDrillDownChart">
<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
    ${title}
</div>

<script>
	function refreshDrillDownCharts() {
		var pid = $j("#parentChartId").val();
		var dashboardId = ${dashboardId?c};
		$j.ajax({
				async : false,
				url : globals.BASE
						+ '/dashboardalert/dashboards/getDrillDownChartsActionSupport.action',
				dataType: "json",
				data : {
					dashboardId : dashboardId,
					parentChartId : pid
				},
				cache: false,
				success : function(d) {
					$("wwctrl_linkingCategoryForDrillDown").innerHTML = d.category;
					drawSelect(getProperties($j(d.drillDownCharts)),'#drillDownChartId');
					drawSelect(getProperties($j(d.linkedDrillDownFields)),'#linkedDrillDownFieldId');
				}
			});		
	}
	
	function refreshLinkedDrillDownFields() {
		var pid = $j("#parentChartId").val();
		var drillId = $j("#drillDownChartId").val();
		$j.ajax({
				async : false,
				url : globals.BASE
						+ '/dashboardalert/dashboards/getLinkedDrillDownFieldsActionSupport.action',
				dataType: "json",
				data : {
					drillDownChartId : drillId,
					parentChartId : pid
				},
				cache: false,
				success : function(d) {
					drawSelect(getProperties($j(d.linkedDrillDownFields)),'#linkedDrillDownFieldId');
				}
			});		
	}
	
	function getProperties(data) {
		var properties = [];

		$j(data).each(function(index, value) {
			var property = {};
			property['key'] = value.id;
			property['value'] = value.name;
			properties.push(property);
		});	
		
		return properties;	
	}
  	

</script>
<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/dashboards" action="${formName}.action" method="POST">

	<div class="formTop">
		<@s.select tabindex="1" 
		        label="%{getText('drillDownChart.label.parentChart')}" 
		        name="parentChartId" 
		        id="parentChartId"
		        list="parentCharts"
		        listKey="value" 
				listValue="key"
		        disabled="false"
		        readonly="false" 
		        required="true" 
		        cssClass="parentChartIdField"
		        onchange="refreshDrillDownCharts();"/>
		        
		<@s.textfield tabindex="2" 
		       label="%{getText('drillDownChart.label.parent.linkingCategory')}" 
		       name="linkingCategoryForDrillDown" 
		       id="linkingCategoryForDrillDown" 
		       size="15" 
		       maxlength="255" 
		       show="true" 
		       readonly="true" 
		       required="false"/>	 
		        
		<@s.select tabindex="3" 
		        label="%{getText('drillDownChart.label.drillDownChart')}" 
		        name="drillDownChartId" 
		        id="drillDownChartId"
		        list="drillDownCharts"
		        listKey="value" 
				listValue="key"
		        disabled="false"
		        readonly="false" 
		        required="true" 
		        cssClass="drillDownChartIdField"
		        onchange="refreshLinkedDrillDownFields();"/>	
		
		<@s.select tabindex="4" 
		        label="%{getText('drillDownChart.label.drillDown.linkedField')}" 
		        name="linkedDrillDownFieldId" 
		        id="linkedDrillDownFieldId"
		        list="linkedDrillDownFields"
		        disabled="false"
		        readonly="false" 
		        required="true" 
		        cssClass="linkedDrillDownFieldIdField"/>
		        
		<@s.hidden id="${dashboardId}" name="dashboardId"/>		        	        
	</div>
		
<div class="formAction">

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
	<#if (parentCharts?size == 1) && (drillDownCharts?size == 1)>
		<script>
			<#assign submitOnChangeForm="form1">	
	    </script>
	 </#if>	
</div>

</@s.form>
</div>

</body>
</html>
