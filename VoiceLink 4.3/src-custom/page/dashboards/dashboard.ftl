<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='dashboard.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='dashboard.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='dashboard.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='dashboard.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='dashboard.view.title.single'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
    ${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/dashboards" action="${formName}.action" method="POST">
<#if dashboardId?has_content>
    <@s.hidden name="dashboardId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
	<div class="formTop">
	    <@s.textfield tabindex="1" 
	       label="%{getText('dashboard.label.name')}" 
	       name="dashboard.name" 
	       id="dashboard.name" 
	       size="15" 
	       maxlength="50" 
	       show="true" 
	       readonly="${readOnly}" 
	       required="true"/>
	    <@s.textfield tabindex="2" 
	       label="%{getText('dashboard.label.description')}" 
	       name="dashboard.description" 
	       id="dashboard.description" 
	       size="15" 
	       maxlength="255" 
	       show="true" 
	       readonly="${readOnly}" 
	       required="false"/>
	   <@s.select tabindex="3" 
	       label="%{getText('dashboard.label.timeFilter')}" 
	       name="timeFilter" 
	       id="timeFilter"
	       list="timeFilterMap"
	       readonly="${readOnly}"
	       required="true"
	       cssClass="timeFilterField"/>
	</div>
		
<div class="formAction">
	
	<#if dashboardId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>

<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
    <div id="modEntity" style="visibility: hidden;">
        <div class="modifiedTitle">
            <@s.text name='dashboard.edit.modifiedEntity.title'/>
        </div>
        <div class="formTop">
            <@s.label label="%{getText('dashboard.label.name')}" name="modifiedEntity.name" />
            <@s.label label="%{getText('dashboard.label.description')}" name="modifiedEntity.description" />
            <@s.label label="%{getText('dashboard.label.timeFilter')}" name="modifiedTimeFilter" />
        </div>
    </div>
    <script language="javascript">
    YAHOO.namespace("example.resize");

    function init() {
        YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
            {
              width:"450",
              left: 300,
              top: 200,
              constraintoviewport: true,
              fixedcenter: true,
              underlay:"shadow",
              close:true,
              visible:false,
              draggable:true,
              modal:false } );
        YAHOO.example.resize.panel.render();
    }

    YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>

</body>
</html>
