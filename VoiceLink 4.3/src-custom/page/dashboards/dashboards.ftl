<title><@s.text name='dashboardalert.dashboard.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
    <#include "/include/common/tablecomponent.ftl">
	<script type="text/javascript">
        function getFilter(viewId, columnId, val, operand) {
            return "submittedFilterCriterion={'viewId':'"+viewId+"','columnId':'"+columnId+"','operandId':'"+operand+"','value1':'" + val+"','value2':'','locked':false}";
        }
        function getLockedFilter(viewId, columnId, val, operand) {
            return "submittedFilterCriterion={'viewId':'"+viewId+"','columnId':'"+columnId+"','operandId':'"+operand+"','value1':'"+val+"','value2':'','locked':true}";
        }
    </script type="text/javascript">
    
    <#assign urlMethod="getDashboardData"/>
 	<@tablecomponent
 		varName="dashboardObj"
		columns=dashboardColumns
		tableId="listDashboardsTable"
		url="${pageContext}/dashboards"
		tableTitle='dashboardalert.dashboard.view.title' 
		viewId=dashboardViewId
    	type=""
    	extraParams=extraParamsHash?default("") />


    <#assign urlMethod="getDashboardDetailsData"/>
 	<@tablecomponent
 		varName="dashboardDetailsObj"
		columns=dashboardDetailsColumns
		tableId="listDashboardDetailsTable"
		url="${pageContext}/dashboards"
		tableTitle='dashboard.detail.view.title' 
		viewId=dashboardDetailsViewId
    	type=""
    	extraParams={"publishers":[{"name" : "dashboardObj", "selector": "dashboardIds"}]} />    	       
