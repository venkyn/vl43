<title><@s.text name='schedule.view.title'/></title>
<#include "/include/common/tablecomponent.ftl">

	<#assign extraParamsHash = {}>
 	
 	<@tablecomponent
  		varName="scheduleObj"
		columns=schedulerColumns
		tableId="listSchedulesTable"
		url="${base}/admin/schedule"
		tableTitle='schedule.view.title'
		viewId=schedulesViewId
		type=""
		extraParams=extraParamsHash?default("") />

	<@tablecomponent
		varName="historyObj"
		columns=historyColumns
		tableId="listHistoryTable"
		url="${base}/admin/schedulehistory"
		tableTitle='schedule.view.history.title'
    	viewId=historyViewId
    	type=""
		extraParams={"publishers":[{"name" : "scheduleObj", "selector": "jobId"}]} />

	<script type="text/javascript">
   	    
   	    //create the wrapper functions to handle opening/closing multiple blades
   	    function openHistory(evt) {
   	    	hideActionsMenu(scheduleObj);
   	    	//log("open history action menu");
   	    }
   	    function openSchedule(evt) {
   	    	showActionsMenu(scheduleObj);
   	    	//log("close history action menu");
   	    }
   	    
   	    //initialize the clientHeight of the table object
   	    scheduleObj.menuBladeHeight = $("menuBlade_listSchedulesTable").clientHeight;
   	    //now register onfocus and onblur handlers-- disabled because the functionality isn't necessary here
	    //scheduleObj.registerHandlers( openSchedule, null ); 
	    //historyObj.registerHandlers( openHistory, null );
    </script>
		
