<#assign readOnly="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='schedule.edit.label.title'/>
  </#assign>
  <#assign formName="edit">
<#else>  
  <#assign title>
    <@s.text name='schedule.view.label.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>
  
<#assign semicolon><@s.text name='semicolon'/></#assign>
<#assign intervalformat><@s.text name='schedule.edit.interval.timeformat'/></#assign>
<#assign dateformat><@s.text name='schedule.edit.daily.timeformat'/></#assign>
  
<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/admin/schedule" action="${formName}.action" method="POST">
<#if jobId?has_content>
    <@s.hidden name="jobId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>

<div class="formTop">
    <@s.label theme="css_xhtml" tabindex="1" label="%{getText('schedules.column.processName')}" name="jobDisplayName" id="processname"/>   

<#-- Disable Error Message -->
<#-- Note: this works for the current jobs if we want to disable this it needs to happen in the action class.-->
<div id="warningDiv" class="listErrorMessage sysHide" style="font-weight:normal">
	<@s.text name='schedule.exportpurgearchive.warning'/>
</div>
<#-- End Disable Error Message -->

    <@s.select tabindex="1" label="%{getText('schedules.column.status')}" name="job.enabled" id="job.status" list="statusMap" readonly="${readOnly}" onchange="displayDisableWarning(this);"/>
</div>

<div class="formMiddle">
  	<#if readOnly=="false">
	<#assign radioText>
		<@s.text name='schedule.view.title'/>
 	</#assign>
 	<#assign jobTypeKey>{ "interval", "daily" }</#assign>
 	<table border=0 width="100%" style="position:relative;">
 	    <tr>
 	      <td width="30%"></td>
 	      <td colspan="2"></td>
  		<tr>
			<td rowspan="3" style="position:relative;">
				<div style="position: relative;">
				<@s.radio id="jobType" theme="css_nonbreaking" tabindex="2" cssClass="radioSchedule" label="${radioText}"
                           list="jobTypeMap" name="jobType" readonly="${readOnly}"/>
                 </div>
  			</td>
  		</tr>
  		<tr>
  			<td style="position:relative;">
  				<div style="position: relative;">
 				<@s.component template="controlheader" id="intervalHours" name="intervalHours" theme="css_nonbreaking" 
                               label="%{getText('schedule.type.interval')}"/>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<@s.textfield tabindex="3" name="intervalHours" 
                               id="intervalHours" size="1" maxlength="2" readonly="${readOnly}" theme="css_nonbreaking"/>${semicolon}
				<@s.textfield tabindex="4" name="intervalMinutes" 
                               id="intervalMinutes" grpId="intervalHours" size="1" maxlength="2" readonly="${readOnly}" theme="css_nonbreaking"/>${semicolon}
				<@s.textfield tabindex="5" name="intervalSeconds" 
                               id="intervalSeconds" grpId="intervalHours" size="1" maxlength="2" readonly="${readOnly}" theme="css_nonbreaking"/>&nbsp;${intervalformat}
				<@s.component template="controlfooter" id="intervalHours" theme="css_nonbreaking"/>
				</div>
			</td>
		</tr>
  		<tr>
  			<td style="position:relative;">
  				<div style="position: relative;">
 				
 				<@s.component template="controlheader" id="cronHours" name="cronHours" theme="css_nonbreaking" 
 				 label="%{getText('schedule.type.daily')}"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
				<@s.textfield tabindex="6" name="cronHours" id="cronHours"
				 size="1" maxlength="2" readonly="${readOnly}" theme="css_nonbreaking"/>${semicolon}
				
				<@s.textfield tabindex="7" name="cronMinutes" id="cronMinutes" grpId="cronHours" size="1" 
				maxlength="2" readonly="${readOnly}" theme="css_nonbreaking"/>
				
				<@s.select tabindex="8" name="cronHourType" id="cronHourType" grpId="cronHours" list="hrTypeMap"
				 readonly="${readOnly}" theme="css_nonbreaking" class="inputElement"/>&nbsp;${dateformat}
				
				<@s.component template="controlfooter" id="cronHours" theme="css_nonbreaking"/>
				</div>
			</td>
		</tr>
	</table>
	<#else>
		<#assign interval><@s.property value="isInterval"/></#assign>
		<@s.textfield theme="css_xhtml" id="displayScheduleInfo" label="%{getText('schedule.edit.label.scheduleInformation')}" name="" value="" readonly="true"/>
		<@s.textfield theme="css_xhtml" label="%{getText('schedule.type')}" id="jobType" name="jobTypeDisplay" readonly="true"/>
		<@s.textfield theme="css_xhtml" label="%{getText('schedule.time')}" id="timeDisplay" name="timeDisplay" readonly="true"/>
		
	</#if>	
</div>


<div class="formBottom">
	 <@s.label theme="css_xhtml" tabindex="9" label="%{getText('schedules.column.nextExecution')}" name="displayNextRun"/>
	 
	 <@s.label theme="css_xhtml" tabindex="10" label="%{getText('schedules.column.lastStarted')}" name="displayLastStarted"/>

	 <@s.label theme="css_xhtml" tabindex="11" label="%{getText('schedule.edit.label.lastFinished')}" name="displayLastRun"/>

	 <@s.label theme="css_xhtml" tabindex="12" label="%{getText('schedule.edit.label.lastResult')}" name="jobLastRunResult" id="lastrunresult" />

	 <@s.label theme="css_xhtml" tabindex="13" label="%{getText('schedule.edit.label.lastDetails')}" name="job.lastRunMessage" id="lastdetails" />
</div>

<div class="formAction">
	
	<#if jobId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>

<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility: hidden;">
	<div class="modifiedTitle">
		<@s.text name='modifiedEntity.title'/>
	</div>
	<div class="formTop">
		<@s.label label="%{getText('schedules.column.processName')}" name="modifiedEntity.displayName"/>
		<@s.label label="%{getText('schedules.column.status')}" name="modifiedStatus"/>
    </div>
	<div class="formMiddle">
		<@s.textfield theme="css_xhtml" label="%{getText('schedule.edit.label.scheduleInformation')}" name="" value="" readonly="true"/>
		<@s.label label="%{getText('schedule.type')}" name="modifiedType"/>
		<@s.label label="%{getText('schedule.time')}" name="modifiedTimeDisplay"/>
		
	</div>
	<div class="formBottom">
	 <@s.label theme="css_xhtml" tabindex="9" label="%{getText('schedules.column.nextExecution')}" name="modifiedEntity.displayNextRun"/>
	 
	 <@s.label theme="css_xhtml" tabindex="10" label="%{getText('schedules.column.lastStarted')}" name="modifiedEntity.displayLastStarted"/>
	
	 <@s.label theme="css_xhtml" tabindex="11" label="%{getText('schedule.edit.label.lastFinished')}" name="modifiedEntity.displayLastRun"/>
	
	 <@s.label theme="css_xhtml" tabindex="12" label="%{getText('schedule.edit.label.lastResult')}" name="modifiedEntity.lastRunResult" />
	
	 <@s.label theme="css_xhtml" tabindex="13" label="%{getText('schedule.edit.label.lastDetails')}" name="modifiedEntity.lastRunMessage"/>
	</div>
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}

	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>	 
</@s.form>
</body>
</html>
