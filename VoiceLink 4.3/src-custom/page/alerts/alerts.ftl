<title><@s.text name='dashboardalert.alert.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
	<#include "/include/common/tablecomponent.ftl">
	<script type="text/javascript">
        function getFilter(viewId, columnId, val, operand) {
            return "submittedFilterCriterion={'viewId':'"+viewId+"','columnId':'"+columnId+"','operandId':'"+operand+"','value1':'" + val+"','value2':'','locked':false}";
        }
        function getLockedFilter(viewId, columnId, val, operand) {
            return "submittedFilterCriterion={'viewId':'"+viewId+"','columnId':'"+columnId+"','operandId':'"+operand+"','value1':'"+val+"','value2':'','locked':true}";
        }
    </script type="text/javascript">
    
    <#assign urlMethod="getAlertData"/>
 	<@tablecomponent
 		varName="alertObj"
		columns=alertColumns
		tableId="listAlertsTable"
		url="${pageContext}/alerts"
		tableTitle='dashboardalert.alert.view.title' 
		viewId=alertViewId
    	type=""
    	extraParams=extraParamsHash?default("") />     			

    	
    <#assign urlMethod="getAlertCriteriaData"/>
 	<@tablecomponent
 		varName="alertCriteriaObj"
		columns=alertCriteriaColumns
		tableId="listAlertCriteriaTable"
		url="${pageContext}/alerts"
		tableTitle='dashboardalert.alertCriteria.view.title' 
		viewId=alertCriteriaViewId
    	type=""
    	extraParams={"publishers":[{"name" : "alertObj", "selector": "alertIdValueForCriteria"}]} />
 		