<#include "/include/common/alertCriteria.ftl" />

<script type="text/javascript" src="${base}/scripts/vocollect/voc_alert_criteria.js"></script>
<link rel="stylesheet" type="text/css" href="${base}/css/miniTable.css">

<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='alert.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='alert.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='alert.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='alert.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='alert.view.title.single'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
    ${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/alerts" action="${formName}.action" method="POST">
<#if alertId?has_content>
    <@s.hidden name="alertId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
	<div class="formTop">
	    <@s.textfield tabindex="1" 
	       label="%{getText('alert.label.alertName')}" 
	       name="alert.alertName" 
	       id="alert.alertName" 
	       size="15" 
	       maxlength="50" 
	       show="true" 
	       readonly="${readOnly}" 
	       required="true"/>
		<@s.select tabindex="2" 
	        label="%{getText('alert.label.alertStatus')}" 
	        name="alertStatus" 
	        id="alertStatus"
	        list="alertStatusMap"
	        readonly="${readOnly}"
	        cssClass="alertStatusField"/> 	       
  		<#if navigation.pageName="view">
			<@s.textfield tabindex="3" 
		       label="%{getText('alert.label.alertOn')}" 
		       name="alertOnValue" 
		       id="alertOnValue" 
		       size="25" 
		       maxlength="50" 
		       show="true" 
		       readonly="${readOnly}" 
		       required="true"/>
	    <#else>
		    <@s.select tabindex="3" 
		        label="%{getText('alert.label.alertOn')}" 
		        name="daInformationID" 
		        id="daInformationID"
		        list="alertDAInformationMap"
                listKey="value" 
                listValue="key"
		        disabled="${disabled}"
		        readonly="${readOnly}" 
		        cssClass="daInformationIDField"/>
	    </#if>
	</div>
	<br>
	<div class="formMiddle">
	<span id="promptTitle" class="promptConfigTitle"><@s.text name='alert.criteria.create.caption.general'/></span>
	<br>
      	<#-- Data for criteria div populated in voc_alert_criteria.js file -->
      	<br>
      	<@alertCriteria id="criteria" name="criteria" alertId="alertId" alertOnId="daInformationID" readonly="${readOnly}"/>
	</div>
	<br>
	<div class="formBottom">
	<span id="promptTitle" class="promptConfigTitle"><@s.text name='alert.configurations.create.caption.general'/></span>
	<#if navigation.pageName="view">
		<br>
	</#if>
		<@s.select tabindex="5" 
	        label="%{getText('alert.label.alertNotificationPriority')}" 
	        name="notificationPriority" 
	        id="notificationPriority"
	        list="notificationPriorityMap"
	        readonly="${readOnly}"
	        cssClass="alertNotificationPriorityField"/>        
		<@s.textfield tabindex="6" 
	       label="%{getText('alert.label.emailAddress')}" 
	       name="alert.emailAddress" 
	       id="alert.emailAddress" 
	       size="25" 
	       maxlength="255" 
	       show="true" 
	       readonly="${readOnly}" 
	       required="false"/>
	    <@s.select tabindex="7" 
            label="%{getText('alert.label.dashboard')}" 
            name="dashboardId" 
            id="dashboardId"
            list="dashboardMap"
            listKey="value" 
            listValue="key"
            readonly="${readOnly}"
            cssClass="dashboard"
            emptyOption="true"/>
	    <@s.textfield tabindex="8" 
	       label="%{getText('alert.label.frequency')}" 
	       name="alertFrequency" 
	       id="alertFrequency"
	       size="5" 
	       maxlength="7" 
	       show="true" 
	       readonly="${readOnly}" 
	       required="true"/>  
		<#if formName="view" || formName="edit"> 
			<@s.label label="%{getText('alert.label.lastEvaluationTime')}" name="lastEvaluationTime"/>
		
			<@s.label label="%{getText('alert.label.lastAlertEventTime')}" name="lastAlertEventTime"/>
		</#if>
	</div>
	
<div class="formAction">
	
	<#if alertId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>

<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
    <div id="modEntity" style="visibility: hidden;">
        <div class="modifiedTitle">
            <@s.text name='alert.edit.modifiedEntity.title'/>
        </div>
        <div class="formTop">
            <@s.label label="%{getText('alert.label.alertName')}" name="modifiedEntity.alertName" />
            <@s.label label="%{getText('alert.label.alertStatus')}" name="modifiedAlertStatus" />
            <@s.label label="%{getText('alert.label.alertOn')}" name="modifiedEntity.daInformation.daInformationDisplayName" />
            <@s.label label="%{getText('alert.label.selected.criterias')}" name="modifiedCriterias[0]" />
            <#list modifiedCriterias as modifiedCriteria>
            	<#if modifiedCriteria_index != 0>
            		<@s.label label=" " name="modifiedCriterias[${modifiedCriteria_index}]" />
            	</#if>
            </#list>
            <@s.label label="%{getText('alert.label.alertNotificationPriority')}" name="modifiedAlertNotificationPriority" />
            <@s.label label="%{getText('alert.label.emailAddress')}" name="modifiedEntity.emailAddress" />
            <@s.label label="%{getText('alert.label.dashboard')}" name="modifiedEntity.dashboard.name" />
            <@s.label label="%{getText('alert.label.frequency')}" name="modifiedEntity.reNotificationFrequency.descriptiveText" />
            <@s.label label="%{getText('alert.label.lastEvaluationTime')}" name="modifiedEntity.lastEvaluationTime" />
            <@s.label label="%{getText('alert.label.lastAlertEventTime')}" name="modifiedEntity.lastAlertEventTime" />
        </div>
    </div>
    <script language="javascript">
    YAHOO.namespace("example.resize");

    function init() {
        YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
            {
              width:"450",
              left: 300,
              top: 200,
              constraintoviewport: true,
              fixedcenter: true,
              underlay:"shadow",
              close:true,
              visible:false,
              draggable:true,
              modal:false } );
        YAHOO.example.resize.panel.render();
    }

    YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>

</body>
</html>
