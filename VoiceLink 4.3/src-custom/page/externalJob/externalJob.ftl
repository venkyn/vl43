<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='externalJob.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='externalJob.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='externalJob.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='externalJob.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='externalJob.view.title.single'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
    ${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/externalJob" action="${formName}.action" method="POST">
<#if externalJobId?has_content>
    <@s.hidden name="externalJobId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
    <@s.textfield tabindex="1" 
       label="%{getText('externalJob.label.name')}" 
       name="externalJob.name" 
       id="externalJob.name" 
       size="15" 
       maxlength="50" 
       show="true" 
       readonly="${readOnly}" 
       disabled="${disabled}"
       required="true"/>   
    <@s.select tabindex="2" 
        label="%{getText('externalJob.label.type')}" 
        name="jobType" 
        id="jobType"
        list="jobTypeMap"
        readonly="true" 
        cssClass="jobType"/>
    <@s.textarea tabindex="3" 
       label="%{getText('externalJob.label.command')}" 
       name="externalJob.command" 
       id="externalJob.command"
       rows="2" cols="50" 
       maxlength="255" 
       show="true" 
       readonly="${readOnly}"
       required="true"/>         
    <@s.textarea tabindex="4" 
       label="%{getText('externalJob.label.workingDirectory')}" 
       name="externalJob.workingDirectory" 
       id="externalJob.workingDirectory" 
       rows="2" cols="50" 
       maxlength="255" 
       show="true" 
       readonly="${readOnly}"
       required="true"/>    
</div>
<div class="formAction">
	
	<#if externalJobId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>

<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
    <div id="modEntity" style="visibility: hidden;">
        <div class="modifiedTitle">
            <@s.text name='externalJob.edit.modifiedEntity.title'/>
        </div>
        <div class="formTop">
            <@s.label label="%{getText('externalJob.label.name')}" name="modifiedEntity.name" />
            <@s.label label="%{getText('externalJob.label.type')}" name="modifiedEntity.jobType" />
            <@s.label label="%{getText('externalJob.label.command')}" name="modifiedEntity.command" />
            <@s.label label="%{getText('externalJob.label.workingDirectory')}" name="modifiedEntity.workingDirectory" />
        </div>
    </div>
    <script language="javascript">
    YAHOO.namespace("example.resize");

    function init() {
        YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
            {
              width:"450",
              left: 300,
              top: 200,
              constraintoviewport: true,
              fixedcenter: true,
              underlay:"shadow",
              close:true,
              visible:false,
              draggable:true,
              modal:false } );
        YAHOO.example.resize.panel.render();
    }

    YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>


</body>
</html>