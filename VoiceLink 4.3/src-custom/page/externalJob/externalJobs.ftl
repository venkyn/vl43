<title><@s.text name='externaljob.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
	<#include "/include/common/tablecomponent.ftl">

 	<@tablecomponent
 		varName="externalJobObj"
		columns=externalColumns
		tableId="listExternalJobsTable"
		url="${pageContext}/externalJob"
		tableTitle='externaljob.view.title' 
		viewId=externalJobViewId
    	type=""
    	extraParams=extraParamsHash?default("") />     			
