<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<#-- Set title according to error. -->
<#global title>
  <@s.text name="errorPage.${error}.title"/>
</#global>
<#global messageText="errorPage.${error}.text">


<html>
        <#include "/decorators/head.ftl">
<body>

    <div id="container">
        <#include "/decorators/header.ftl">
	    <div class="titleBar">
            ${title}
	    </div>
        <div id="errorPageContent">
          <#include "/include/errors/${error}.ftl">
        </div>
    </div>
<!-- EXCEPTION MESSAGE
${exception?default("No exception message available")}
-->
<!-- EXCEPTION STACK TRACE
<#if exceptionStack?has_content>
${exceptionStack}
<#elseif exception?has_content>
<#assign traceElements=exception.stackTrace/>
<#list traceElements as element>
${element}
</#list>
<#else>
No exception stack trace available
</#if>
-->
        <#include "/decorators/footer.ftl">
</body>
</html>


