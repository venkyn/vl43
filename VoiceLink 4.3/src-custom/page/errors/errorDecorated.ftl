<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<#-- Sometimes error pages get decorated by SiteMesh, sometimes they don't,
     depending on where and when in the processing cycle the error occurred.
     This error page is called when we think it will be decorated.  -dday -->
     
<#-- Set title according to error. -->
<#assign title>
  <@s.text name="errorPage.${error}.title"/>
</#assign>
<#global messageText="errorPage.${error}.text">

<html>
<head>
<title>${title}</title>
</head>
<body>
	    <div class="titleBar">
            ${title}
	    </div>
        <div id="errorPageContent">
          <#include "/include/errors/${error}.ftl">
        </div>
<!-- EXCEPTION MESSAGE
${exception?default("No exception message available")}
-->
<!-- EXCEPTION STACK TRACE
<#if exceptionStack?has_content>
${exceptionStack}
<#elseif exception?has_content>
<#assign traceElements=exception.stackTrace/>
<#list traceElements as element>
${element}
</#list>
<#else>
No exception stack trace available
</#if>
-->
</body>
</html>


