<title><@s.text name='voicelink.selection.home.title'/></title>

<#include "/include/common/configurablehomepage.ftl">

 <@configurablehomepage 
   summaries = summaries 
   summariesLocation = summariesLocation
   rows=3
   columns=2
   homepageId=homepageId
  />

<script type="text/javascript">
   	    //set up and register handlers
   	    function openWorkBlade(evt) {
   	    	showActionsMenu(selectionCurrentWorkSummaryObj);
   	    	try {
   	    		hideActionsMenu(selectionLaborSummaryObj);
   	    	} catch(err) {
   	            // if the summary isn't there, this will throw an invalid reference exception.
	            // checking for undefined does not work for variables, only object properties.
	            // because these errors are expected behavior, logging is commented out.
   	    	    //log(err);
   	    	}
   	    }
   	    
   	    function openLaborBlade(evt) {
   	    	showActionsMenu(selectionLaborSummaryObj);
   	    	try {
   	    		hideActionsMenu(selectionCurrentWorkSummaryObj);
   	    	} catch(err) {
   	    	    //log(err);
   	    	}
   	    }
   	    
	   	try {
			selectionCurrentWorkSummaryObj.menuBladeHeight = $("menuBlade_listselectionCurrentWorkSummaryTable").clientHeight;
			selectionCurrentWorkSummaryObj.registerHandlers(openWorkBlade, null);
		} catch(err) { 
	        //log(err);
		}
	
		try {
			selectionLaborSummaryObj.menuBladeHeight = $("menuBlade_listselectionLaborSummaryTable").clientHeight;
			selectionLaborSummaryObj.registerHandlers(openLaborBlade, null);
	    } catch(err) {
	        //log(err);
	    }
</script>