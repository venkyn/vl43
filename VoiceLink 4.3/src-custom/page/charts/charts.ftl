<title><@s.text name='dashboardalert.chart.view.title'/></title>
    <#assign pageContext="${base}/${navigation.applicationMenu}">
	<#include "/include/common/tablecomponent.ftl">
	<script type="text/javascript">
        function getFilter(viewId, columnId, val, operand) {
            return "submittedFilterCriterion={'viewId':'"+viewId+"','columnId':'"+columnId+"','operandId':'"+operand+"','value1':'" + val+"','value2':'','locked':false}";
        }
        function getLockedFilter(viewId, columnId, val, operand) {
            return "submittedFilterCriterion={'viewId':'"+viewId+"','columnId':'"+columnId+"','operandId':'"+operand+"','value1':'"+val+"','value2':'','locked':true}";
        }
    </script type="text/javascript">
    
    <#assign urlMethod="getChartData"/>
 	<@tablecomponent
 		varName="chartObj"
		columns=chartColumns
		tableId="listChartsTable"
		url="${pageContext}/charts"
		tableTitle='dashboardalert.chart.view.title' 
		viewId=chartViewId
    	type=""
    	extraParams=extraParamsHash?default("") />        