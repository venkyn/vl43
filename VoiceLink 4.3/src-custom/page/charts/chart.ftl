<#include "/include/common/chartDataFields.ftl" />

<script type="text/javascript" src="${base}/scripts/vocollect/voc_chart_datafields.js"></script>
<link rel="stylesheet" type="text/css" href="${base}/css/miniTable.css">

<#assign readOnly="false">
<#assign disabled="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='chart.edit.title'/>
  </#assign>
  <#assign formName="edit">
  <#assign submitValue>
    <@s.text name='chart.edit.button.submit'/>
  </#assign>
  <#assign disabled="true">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='chart.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='chart.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='chart.view.title.single'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
    ${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/${navigation.applicationMenu}/charts" action="${formName}.action" method="POST">
<script>
	var pivotOnValue = [];
	var pivotForValue = [];
	var columnIds = [];

	$j(function() {
		
		setPivotFieldsState();
		handleChartTypeChange();
		handleChartDataProviderChange();
		handleChartDataFieldDraw();	
	
		//disabling pivot fields on page load if chart type is anything other than grid chart
		if ($j('#type option:selected').val() != 2) {
			setPivotFieldsSelected();
		}
		
		//disabling sort fields on page load if there is any pivot value selected
		var isPivotOnNone = $j('#pivotOnId option:selected').val() != 'NONE'; 
		var isPivotForNone = $j('#pivotForId option:selected').val() != 'NONE';		
		var sortFieldEnabled = isPivotOnNone || isPivotForNone;		
		setSortByFieldState(sortFieldEnabled);		
		setSortOrderFieldState(sortFieldEnabled);
		
		//disabling sort order field on page load if selected sort by field is none
		var isSortByNone = $j('#sortById option:selected').val() == 'NONE';	
		setSortOrderFieldState(sortFieldEnabled);
	});
	
	/**
	* Handles chart type changes
	*/
	function handleChartTypeChange() {
		$j('#type').change(function() {
			setPivotFieldsState();
			setPivotFieldsSelected();
			adjustSortFieldsStateAndValues();
		});
	}
	
	/**
	* function to set state of pivot fields
	*/
	function setPivotFieldsState() {
		var result = $j('#type option:selected').val() != 2; 
		
		$j("#pivotOnId").prop('disabled', result ); 
		$j("#pivotForId").prop('disabled', result ); 
	}
	
	/**
	* function to set selected value of pivot fields
	*/
	function setPivotFieldsSelected() {
		$j('#pivotOnId').val('NONE').attr("selected", "selected");
		$j('#pivotForId').val('NONE').attr("selected", "selected");
	}
	
	/**
	* function to set state of sort by field
	*/
	function setSortByFieldState(result) {		
		$j("#sortById").prop('disabled', result ); 
	}
	
	/**
	* function to set state of sort order field
	*/
	function setSortOrderFieldState(result) {		 
		$j("#sortOrderId").prop('disabled', result ); 
	}
	
	/**
	* function to set selected value of sort fields
	*/
	function setSortByFieldSelected() {
		$j('#sortById').val('NONE').attr("selected", "selected");
		setSortOrderFieldState(true);
	}
	
	/**
	* Handles chart data provider changes
	*/
	function handleChartDataProviderChange() {
		$j('#daInformationID').change(function() {
			$j.ajax({
				async : false,
				url : globals.BASE
						+ '/dashboardalert/charts/getChartConfigFieldsActionSupport.action',
				data : {
					daInformationID : this.value
				},
				datatype : 'json',
				success : function(data) {
					drawSelect(getProperties($j(data.chartDataProperties)), "#categoryId");
					drawSelect(getProperties($j(data.chartPivotFields)), "#pivotOnId");
					drawSelect(getProperties($j(data.chartPivotFields)), "#pivotForId");
					drawSelect(getProperties($j(data.sortByFields)), "#sortById");
				}
		
			});
		});
	}
	
	/**
	* Builds the property list from server sent json
	*/
	function getProperties(data) {
		var properties = [];

		$j(data).each(function(index, value) {
			var property = {};
			property['key'] = value.columnId;
			property['value'] = value.displayName;
			properties.push(property);
		});

		return properties;
	}
	
	/**
	* Handle chart data field draw
	*/
	function handleChartDataFieldDraw() {
		$j('#dataFieldDiv').on('datafielddrawcomplete',function() {
			adjustDropdowns();
		});
	}
	
	/**
	*Creates the array of column ids of all the selected checkboxes
	*/
	function populateColumnIds() {
		columnIds = [];
		
		// Get all ids of checked fields
		$j("input[id^='fieldChecked']").each(function(){
			if (this.checked){
				columnIds.push(this.columnId);
			}
		});
	}
	
	/**
	* redraw pivot on dropdown on category change or data field checkbox change
	*/
	function drawPivotOnDropdown() {
		var selectedPivotOn = $j('#pivotOnId option:selected').val();
		
		$j('#pivotOnId').empty();
		
		var selectedCategory = $j('#categoryId option:selected').val();
		
		$j(pivotOnValue).each(function(index, d) {
			if (selectedCategory != d.value && $j.inArray(parseInt(d.value),columnIds) == -1){
				var option = $j('<option/>', {
					'value' : d.value
				});
		
				$j(option).text(d.text);
				$j('#pivotOnId').append(option);
			}
		});
		
		$j('#pivotOnId').val(selectedPivotOn).attr("selected", "selected");		
	}
	
	/**
	*redraw pivot for dropdown on category change or data field checkbox change
	*/
	function drawPivotForDropdown() {
		var selectedPivotFor = $j('#pivotForId option:selected').val();
	
		$j('#pivotForId').empty();
		
		var selectedCategory = $j('#categoryId option:selected').val();
		
		var selectedPivotOn = $j('#pivotOnId option:selected').val();

		$j(pivotForValue).each(function(index, d) {
			if (selectedCategory != d.value && (selectedPivotOn != d.value || selectedPivotOn == 'NONE') && $j.inArray(parseInt(this.value),columnIds) == -1){
				var option = $j('<option/>', {
					'value' : d.value
				});
		
				$j(option).text(d.text);
				$j('#pivotForId').append(option);
			}
		});	
		
		$j('#pivotForId').val(selectedPivotFor).attr("selected", "selected");
	}

	/**
	* Update the pivot on and pivot for list when category changes
	*/
	function handleCategoryChange() {
		$j('#categoryId').change(function(){
			drawPivotOnDropdown();
			drawPivotForDropdown();
			
			adjustSortFieldsStateAndValues();		
		});
	}

	/**
	*Update the pivot for list when pivot on changes and handle state of sort fields
	*/
	function handlePivotOnChange() {
		$j('#pivotOnId').change(function(){
			drawPivotForDropdown();
						
			adjustSortFieldsStateAndValues();
		});	
	}

	/**
	*handle state of sort fields when pivot for changes
	*/
	function handlePivotForChange() {
		$j('#pivotForId').change(function(){						
			adjustSortFieldsStateAndValues();
		});	
	}

	/**
	*handle state of sort order field when sort by changes
	*/
	function handleSortByChange() {
		$j('#sortById').change(function(){						
			var isSortByNone = $j('#sortById option:selected').val() == 'NONE';	
			setSortOrderFieldState(isSortByNone);
		});	
	}
	
	/**
	*handles the event of clicking any data field check box by 
	*refreshing the pivot on and pivot for drop downs accordingly
	*/
	function handleDataFieldCheckboxesChange() {
		$j("input[id^='fieldChecked']").each(function(){
			$j(this).on('change',function(){
				//Populate the array of column ids of all the selected checkboxes
				populateColumnIds(); 
				
				drawPivotOnDropdown();
				drawPivotForDropdown();	
				
				adjustSortFieldsStateAndValues();	
			});
		});
	}
	
	/**
	*method to handle state and selected value of sort fields
	*/
	function adjustSortFieldsStateAndValues() {
		var isPivotOnNone = $j('#pivotOnId option:selected').val() != 'NONE'; 
		var isPivotForNone = $j('#pivotForId option:selected').val() != 'NONE';
		
		if(isPivotOnNone || isPivotForNone) {
			setSortByFieldState(true);
			setSortOrderFieldState(true);
			setSortByFieldSelected();
		} else {
			setSortByFieldState(false);
			var isSortByNone = $j('#sortById option:selected').val() == 'NONE';	
			setSortOrderFieldState(isSortByNone);
		} 
	}
	
	/**
	* Adjusts the dropdowns after data data provider change and first time load
	*/
	function adjustDropdowns(){
		pivotOnValue = [];
		pivotForValue = [];
		
		//Populate the array of column ids of all the selected checkboxes
		populateColumnIds(); 
		
		// Get selected category ID
		var selectedCategory = $j('#categoryId option:selected').val();
		
		// Save all value of pivot on and remove category field
		$j('#pivotOnId > option').each(function(){
			var option = {'value': this.value, 'text': this.text};
			pivotOnValue.push(option);
						
			if (this.value == selectedCategory || $j.inArray(parseInt(this.value),columnIds) > -1 ){
				$j('#pivotOnId option[value="'+this.value+'"]').remove();
			}
		});

		var selectedPivotOn = $j('#pivotOnId option:selected').val();

		// Save all value of pivot for and remove category and pivot on fields
		$j('#pivotForId > option').each(function(){
			var option = {'value': this.value, 'text': this.text};
			pivotForValue.push(option);
			
			if (this.value == selectedCategory || (this.value == selectedPivotOn && selectedPivotOn != 'NONE') || $j.inArray(parseInt(this.value),columnIds) > -1 ){
				$j('#pivotForId option[value="'+this.value+'"]').remove();
			}
		});

		adjustSortFieldsStateAndValues();	
		
		handleCategoryChange();
		handleDataFieldCheckboxesChange();
		handlePivotOnChange();
		handlePivotForChange();
		handleSortByChange();
		
	}
		
</script>

<#if chartId?has_content>
    <@s.hidden name="chartId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
	<div class="formTop">
	    <@s.textfield tabindex="1" 
	       label="%{getText('chart.label.name')}" 
	       name="chart.name" 
	       id="chart.name" 
	       size="15" 
	       maxlength="50" 
	       show="true" 
	       readonly="${readOnly}" 
	       required="true"/>
	    <@s.textfield tabindex="2" 
	       label="%{getText('chart.label.description')}" 
	       name="chart.description" 
	       id="chart.description" 
	       size="15" 
	       maxlength="255" 
	       show="true" 
	       readonly="${readOnly}" 
	       required="false"/>
		<#if navigation.pageName="view">
			<@s.textfield tabindex="3" 
			       label="%{getText('chart.label.dataProvider')}" 
			       name="chartDataSourceValue" 
			       id="chartDataSourceValue" 
			       size="25" 
			       maxlength="50" 
			       show="true" 
			       readonly="${readOnly}" 
			       required="true"/>
		<#else>
			<@s.select tabindex="3" 
			        label="%{getText('chart.label.dataProvider')}" 
			        name="daInformationID" 
			        id="daInformationID"
			        list="chartDAInformationMap"
			        listKey="value" 
					listValue="key"
			        disabled="${disabled}"
			        readonly="${readOnly}" 
			        required="true" 
			        cssClass="daInformationIDField"/>	       
		</#if>      
	    <@s.select tabindex="4" 
	        label="%{getText('chart.label.category')}" 
	        name="categoryId" 
	        id="categoryId"
	        list="categoriesMap"
	        readonly="${readOnly}"  
		    required="true"
	        cssClass="categoryIdField"/>	       
	</div>
	<br>
	<div class="formMiddle">
	<span id="promptTitle" class="promptConfigTitle"><@s.text name='chart.datafields.create.caption'/></span>
	<br>
      	<#-- Data for chartdatafield div populated in voc_chart_datafields.js file -->
      	<br>
      	<@chartDataFields id="dataField" name="dataField" chartId="chartId" chartDataSourceId="daInformationID" readonly="${readOnly}"/>
	</div>
	<br>	
	
    <div class="formBottom">
	<span id="promptTitle" class="promptConfigTitle"><@s.text name='chart.general.create.caption'/></span>
	
		<@s.select tabindex="6" 
	        label="%{getText('chart.label.type')}" 
	        name="type" 
	        id="type"
	        list="typeMap"
	        readonly="${readOnly}"
	        required="true"
	        cssClass="chartTypeField"/>
	    	 	
	</div>   
	    <div class="formBottom">
		    <@s.select tabindex="7" 
		        label="%{getText('chart.label.pivotOn')}" 
		        name="pivotOnId" 
		        id="pivotOnId"
		        list="pivotOnFieldsMap"
		        readonly="${readOnly}"
		        required="false"
		        cssClass="pivotOnIdField"/> 	
		    <@s.select tabindex="8" 
		        label="%{getText('chart.label.pivotFor')}" 
		        name="pivotForId" 
		        id="pivotForId"
		        list="pivotForFieldsMap"
		        readonly="${readOnly}"
		        required="false"
		        cssClass="pivotForIdField"/>	
		</div>	
	    
	    <div class="formBottom">  	
		    <@s.select tabindex="9" 
		        label="%{getText('chart.label.sortBy')}" 
		        name="sortById" 
		        id="sortById"
		        list="sortByFieldsMap"
		        readonly="${readOnly}"
		        required="false"
		        cssClass="sortByIdField"/>
		    <#if navigation.pageName="view" && "${sortById}" = "NONE">
		    	<@s.label label="%{getText('chart.label.sortOrder')}" value=" " />
		    <#else>
		     <@s.select tabindex="10" 
		        label="%{getText('chart.label.sortOrder')}" 
		        name="sortOrderId" 
		        id="sortOrderId"
		        list="sortOrderMap"
		        readonly="${readOnly}"
		        required="false"
		        cssClass="sortOrderField"/>	
		    </#if>  	
		   
		</div>	
		
<div class="formAction">
	
	<#if chartId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=199>
	<#assign cancelIndex=200>

	<#include "/include/form/buttonbar.ftl">
</div>

<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
    <div id="modEntity" style="visibility: hidden;">
        <div class="modifiedTitle">
            <@s.text name='chart.edit.modifiedEntity.title'/>
        </div>
        <div class="formTop">
            <@s.label label="%{getText('chart.label.name')}" name="modifiedEntity.name" />
            <@s.label label="%{getText('chart.label.description')}" name="modifiedEntity.description" />
            <@s.label label="%{getText('chart.label.dataProvider')}" name="modifiedEntity.daInformation.daInformationDisplayName" />
            <@s.label label="%{getText('chart.label.category')}" name="modifiedEntity.category.daColumnDisplayName" />          
            <@s.label label="%{getText('chart.label.selected.dataFields')}" name="modifiedDataFields[0]" />  
            <#list modifiedDataFields as modifiedField>
            	<#if modifiedField_index != 0>
            		<@s.label label=" " name="modifiedDataFields[${modifiedField_index}]" />
            	</#if>
            </#list>            
            <@s.label label="%{getText('chart.label.type')}" name="modifiedChartType" />
            <@s.label label="%{getText('chart.label.pivotOn')}" name="modifiedEntity.pivotOnDisplayName" />
            <@s.label label="%{getText('chart.label.pivotFor')}" name="modifiedEntity.pivotForDisplayName" />
            <@s.label label="%{getText('chart.label.sortBy')}" name="modifiedEntity.sortByDisplayName" />
            <@s.label label="%{getText('chart.label.sortOrder')}" name="modifiedSortOrder" />
        </div>
    </div>
    <script language="javascript">
    YAHOO.namespace("example.resize");

    function init() {
        YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
            {
              width:"450",
              left: 300,
              top: 200,
              constraintoviewport: true,
              fixedcenter: true,
              underlay:"shadow",
              close:true,
              visible:false,
              draggable:true,
              modal:false } );
        YAHOO.example.resize.panel.render();
    }

    YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>

</body>
</html>
