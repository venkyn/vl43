<#include "/include/common/formatdays.ftl">
<#assign testDirectoryServer>
    <@s.text name="systemConfiguration.dirAuth.button.testDirectoryServerConnectionInformation"/>
</#assign>
<#assign readOnly="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='systemConfiguration.edit.title'/>
  </#assign>
  <#assign formName="edit">
<#else>  
  <#assign title>
    <@s.text name='systemConfiguration.view.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<#if smtpAuthenticationRequired == true>
	<#assign divSmtpAuthValue="sysVisible"/>
	<#assign smtpAuthChecked="true"/>		    
<#else>
	<#assign divSmtpAuthValue="sysHide"/>
	<#assign smtpAuthChecked="false"/>		    
</#if>

<#if directoryServerAuthenticationRequired == true>
	<#assign divDirServAuthValue="sysVisible"/>
	<#assign dirAuthChecked="true"/>		    
<#else>
	<#assign divDirServAuthValue="sysHide"/>
	<#assign dirAuthChecked="false"/>		    
</#if>
<html>
<head>
<title>${title}</title>
</head>
 <body onLoad="javascript:test();">
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/admin/systemconfiguration" action="${formName}.action" method="POST">
<@s.hidden name="savedEntityKey"/>
<div class="formTop">
	<div style="position:relative;margin-left: 0px;margin-top: 20px;">
		<@s.label theme="simple" value="%{getText('systemConfiguration.smtp.title')}" cssClass="configurationHeading"/>
	</div>
	<div id="smtpConfig" style="position:relative;margin-left:20px;padding-bottom: 10px;">
		<#if smtpHost?has_content || readOnly="false">
			<@s.textfield tabindex="1" label="%{getText('systemConfiguration.smtp.host')}" name="smtpHost" id="smtpHost" size="25" maxlength="256" required="true" readonly="${readOnly}"/>
		<#else>
			<@s.label label="%{getText('systemConfiguration.smtp.host')}" name="smtpHost" id="smtpHost" size="25" maxlength="256" required="true" value="%{getText('systemConfiguration.smtp.host.none')}"/>
		</#if>
 
		<#if readOnly="false">
	     	<@s.checkbox tabindex="2" onclick="changeValue(this); displayObject(this, 'ndetails');" theme="css_xhtml" name="smtpAuthenticationRequired" id="smtpAuthenticationRequired" cssClass="checkbox" label="%{getText('systemConfiguration.smtp.requires.authentication')}" trueValue="checkbox.readonly.true"  falseValue="checkbox.readonly.false" stayleft="true" readonly="${readOnly}"/>
			<@s.hidden id="hiddenSmtpAuthenticationRequired" name="hiddenSmtpAuthenticationRequired" value="${smtpAuthChecked}"/>
		<#else>
			<@s.checkbox tabindex="2" disabled="true" onclick="displayObject(this, 'ndetails');" theme="css_xhtml" name="smtpAuthenticationRequired" id ="smtpAuthenticationRequired" cssClass="sysText" label="%{getText('systemConfiguration.smtp.requires.authentication')}" trueValue="checkbox.readonly.true"  falseValue="checkbox.readonly.false" stayleft="true" readonly="${readOnly}"/>
		</#if>
		<div id="ndetails" class="formMiddleNoLine ${divSmtpAuthValue}">
		  	<@s.textfield tabindex="3" label="%{getText('systemConfiguration.smtp.user.name')}" name="smtpUserName" id="smtpUserName" size="25" maxlength="256" required="true" readonly="${readOnly}"/>
			<#if readOnly="false">
		  		<@s.password tabindex="4" label="%{getText('systemConfiguration.smtp.password')}" name="smtpPassword" id="smtpPassword" show="true" size="25" maxlength="256" required="true"/>
		  		<@s.password tabindex="5" label="%{getText('systemConfiguration.smtp.password.verified')}" name="smtpPasswordVerified" id="smtpPasswordVerified" show="true" size="25" maxlength="256" required="true"/>
		  	</#if>
		</div>
	</div>
</div>
<div class="formMiddle">
	<div style="position:relative;margin-left: 0px;margin-top: 10px;">
		<@s.label theme="simple" value="%{getText('systemConfiguration.dirauth.title')}" cssClass="configurationHeading"/>
	</div>
	<div id="directoryServerConfig" style="position:relative;padding-left: 20px;padding-top: 10px;padding-bottom: 20px;">
		<#if readOnly="false">
			<@s.checkbox tabindex="6" onclick="changeValue(this); displayObject(this, 'directoryServerConfigDetails');" theme="css_xhtml" name="directoryServerAuthenticationRequired" trueValue="checkbox.readonly.true"  falseValue="checkbox.readonly.false" stayleft="true" id="directoryServerAuthenticationRequired" cssClass="checkbox" label="%{getText('systemConfiguration.dirauth.requires.authentication')}" readonly="${readOnly}"/>
			<@s.hidden id="hiddenDirectoryServerAuthenticationRequired" name="hiddenDirectoryServerAuthenticationRequired" value="${dirAuthChecked}"/>
		<#else>
			<@s.checkbox tabindex="6" disabled="true" onclick="displayObject(this, 'directoryServerConfigDetails');" theme="css_xhtml" name="directoryServerAuthenticationRequired" id ="directoryServerAuthenticationRequired" trueValue="checkbox.readonly.true"  falseValue="checkbox.readonly.false" stayleft="true" cssClass="sysText" label="%{getText('systemConfiguration.dirauth.requires.authentication')}" readonly="${readOnly}"/>
		</#if>
		<div id="directoryServerConfigDetails" style="position:relative;margin-top: 20px" class="formMiddleNoLine ${divDirServAuthValue}">
			<#if readOnly="false">
				<@s.textfield tabindex="7" label="%{getText('systemConfiguration.dirauth.host')}" name="directoryServerHost" id="directoryServerHost" size="25" maxlength="256" required="true" readonly="${readOnly}"/>
				<@s.textfield tabindex="8" label="%{getText('systemConfiguration.dirauth.port')}" name="directoryServerPort" id="directoryServerPort" size="25" maxlength="256" required="true" readonly="${readOnly}"/>
				<@s.textfield tabindex="9" label="%{getText('systemConfiguration.dirauth.searchUserName')}" name="directoryServerSearchUserName" id="directoryServerSearchUserName" size="25" maxlength="256" required="false" readonly="${readOnly}"/>
				<@s.password tabindex="10" label="%{getText('systemConfiguration.dirauth.searchUserPassword')}" name="directoryServerSearchPassword" id="directoryServerSearchPassword" show="true" size="25" maxlength="256" required="false"/>
				<@s.password tabindex="11" label="%{getText('systemConfiguration.dirauth.verifyUserPassword')}" name="directoryServerSearchPasswordVerified" id="directoryServerSearchPasswordVerified" show="true" size="25" maxlength="256" required="false"/>
				<@s.textfield tabindex="12" label="%{getText('systemConfiguration.dirauth.searchBase')}" name="directoryServerSearchBase" id="directoryServerSearchBase" size="25" maxlength="256" required="true" readonly="${readOnly}"/>
				<@s.textfield tabindex="13" label="%{getText('systemConfiguration.dirauth.searchableAttribute')}" name="directoryServerSearchableAttribute" id="directoryServerSearchableAttribute" size="25" maxlength="256" required="true" readonly="${readOnly}"/>
				<br />
				<br />
				<a
				href="javascript:testConnectionToDirectoryServer();"
				id="testLDAPConnection"
				class="${submitCssClass?default('anchorButtonNoMargin')}"
				tabindex="14"
				>${testDirectoryServer}</a>
				<div id="testArea">
					<@s.textfield tabindex="15" label="%{getText('systemConfiguration.dirauth.testuser')}" name="directoryServerTestUser" id="directoryServerTestUser" size="25" maxlength="256" required="false" readonly="${readOnly}"/>
				</div>
			<#else>
				<@s.label label="%{getText('systemConfiguration.dirauth.host')}" name="directoryServerHost" id="directoryServerHost" size="25" maxlength="256" required="true"/>
				<@s.label label="%{getText('systemConfiguration.dirauth.port')}" name="directoryServerPort" id="directoryServerPort" size="25" maxlength="256" required="true"/>
				<@s.label label="%{getText('systemConfiguration.dirauth.searchUserName')}" name="directoryServerSearchUserName" id="directoryServerSearchUserName" size="25" maxlength="256" required="false"/>
				<@s.label label="%{getText('systemConfiguration.dirauth.searchBase')}" name="directoryServerSearchBase" id="directoryServerSearchBase" size="25" maxlength="256" required="true"/>
				<@s.label label="%{getText('systemConfiguration.dirauth.searchableAttribute')}" name="directoryServerSearchableAttribute" id="directoryServerSearchableAttribute" size="25" maxlength="256" required="true"/>
			</#if>
		</div>
	</div>
</div>
<#-- PurgeArchive Start-->

<div class="formMiddle">
	<br/>
	<div class="label" style="width: 240px;">
		<@s.text name='systemConfiguration.purgearchive.title'/>
	</div>
	
	<#if readOnly="false">
		<#-- Edit Page -->
		<#-- Strategy here is parallel arrays passed back to the action.
			purgeKeys - contains an array of all purge-checked item names
			archiveKeys - contains an array of all archive-checked item names
			purgeValueTextboxes - contains an array of all purged-checked item values
			archiveValueTextboxes - contains an array of all archive-checked item values
		-->
		<br />
		<#list purgeArchiveItems as item>
			<#if item.purgeValue?exists>
					<#assign purgeHasValue="true" />
					<#assign purgeEmpty="false"/>
			<#else>
				<#assign purgeHasValue="false" />
				<#assign purgeEmpty="true"/>
			</#if>
			<#if item.archiveValue?exists>
					<#assign archiveHasValue="true"/>
					<#assign archiveExists="true"/>
			<#else>
				<#assign archiveHasValue="false" />
				<#assign archiveExists="false"/>
			</#if>
			<p>
				<@s.text name='systemConfiguration.purgearchive.${item.name}'/>
			</p>
			<div class="systemConfigIndent">
					<div id="purgeDayWarning_${item_index}" class="listErrorMessage sysHide" style="font-weight:normal">
	  					<@s.text name='systemConfiguration.purgearchive.warning'/>
					</div>
					<div id="purgeLimitError_${item_index}" class="listErrorMessage sysHide" style="font-weight:normal">
	  					<@s.text name='systemConfiguration.purgearchive.purgeLimitError'/>
					</div>
					<#if purgeHasValue = "true">
						<#-- Xwork validation doesn't play nice with our arrays so we provide
							the validation error message -->
						<#if item.purgeErrorMessages?exists>
							<#list item.purgeErrorMessages as message>
								<div class="listErrorMessage" style="font-weight:normal">
									${message}
								</div>
								
							</#list>
						</#if>
						<@s.hidden id="purgeKey_${item_index}" name="purgeKeys" value="${item.name}"/>
						<@s.text name='systemConfiguration.purge.transactional.description'/>&nbsp;
						<@s.textfield id="purgeValue_${item_index}" name="purgeValueTextboxes" onchange="displayDayWarning(this,'purge','${item_index}'),displayErrors('${item_index}');" onkeyup="displayDayWarning(this,'purge','${item_index}'),displayErrors('${item_index}');" theme="simple" label="" tabindex="100" size="2" maxlength="4" value="${item.purgeValue}"/>
					<#else>
						<@s.hidden id="purgeKey_${item_index}" name="purgeKeys" value="${item.name}" disabled="true"/>
						<@s.text name='systemConfiguration.purge.transactional.description'/>&nbsp;
						<@s.textfield id="purgeValue_${item_index}" name="purgeValueTextboxes" onchange="displayDayWarning(this,'purge','${item_index}'),displayErrors('${item_index}');" onkeyup="displayDayWarning(this,'purge','${item_index}'),displayErrors('${item_index}');" theme="simple" label="" tabindex="100" size="2" maxlength="4" disabled="true"/>
					</#if>
					&nbsp;<@s.text name='unit.days'/>
					<script language="javascript">
						hiddenEnable('purgeKey_${item_index}','${purgeHasValue}');
						undoValidationFieldPopulation('purge', '${item_index}');
					</script>
				<br/>
				<div id="archiveDayWarning_${item_index}" class="listErrorMessage sysHide" style="font-weight:normal">
					<p>
	  					<@s.text name='systemConfiguration.purgearchive.warning'/>
	  				</p>
				</div>
				<#if archiveHasValue = "true">
					<#if item.archiveErrorMessages?exists>
						<#list item.archiveErrorMessages as message>
							<div class="listErrorMessage" style="font-weight:normal">
								${message}
							</div>
						</#list>
					</#if>
					<@s.hidden id="archiveKey_${item_index}" name="archiveKeys" value="${item.name}"/>
					<@s.text name='systemConfiguration.purge.archive.description'/>&nbsp;
					<@s.textfield id="archiveValue_${item_index}" name="archiveValueTextboxes" onchange="displayDayWarning(this,'archive','${item_index}'),displayErrors('${item_index}');" onkeyup="displayDayWarning(this,'archive','${item_index}'),displayErrors('${item_index}');" theme="simple" label="" tabindex="100" size="2" maxlength="4" value="${item.archiveValue}"/>
					&nbsp;<@s.text name='unit.days'/><p></p>
					<script language="javascript">
						hiddenEnable('archiveKey_${item_index}','${archiveHasValue}');
						undoValidationFieldPopulation('archive', '${item_index}');
					</script>
				<#else>
					<#if archiveExists != "false">
						<@s.hidden id="archiveKey_${item_index}" name="archiveKeys" value="${item.name}" disabled="true"/>
						<@s.text name='systemConfiguration.purge.archive.description'/>&nbsp;
						<@s.textfield id="archiveValue_${item_index}" name="archiveValueTextboxes" onchange="displayDayWarning(this,'archive','${item_index}'),displayErrors('${item_index}');" onkeyup="displayDayWarning(this,'archive','${item_index}'),displayErrors('${item_index}');" theme="simple" label="" tabindex="100" size="2" maxlength="4" disabled="true"/>
						&nbsp;<@s.text name='unit.days'/><p></p>
						<script language="javascript">
							hiddenEnable('archiveKey_${item_index}','${archiveHasValue}');
							undoValidationFieldPopulation('archive', '${item_index}');
						</script>
					</#if>
				</#if>
				<br/>
			</div>
		</#list>
	<#else>
		<#-- View Page -->
		<div class="systemConfig">
		<TABLE VALIGN="top" BORDER="0" WIDTH="89%">
			<#-- Column Headers -->
			<tr>
				<td><B><@s.text name='systemConfiguration.purgearchive.datadescription.title'/></B></td>
				<td><B><@s.text name='systemConfiguration.purgearchive.purge.title'/></B></td>
				<td><B><@s.text name='systemConfiguration.purgearchive.archive.title'/></B></td>
			</tr>
			<#list purgeArchiveItems as item>
				<#-- For each item, list the name, purge value, and archive value -->
				<#-- We may be able to clean this area up some being that there is validation for null entry
				     and since there is no null entry there is no reason to check this again.-->
				<tr>
					<td><@s.text name='systemConfiguration.purgearchive.${item.name}'/></td>
					<#if item.purgeValue?exists>
						<td><@formatdays days=item.purgeValue?number/></td>
					<#else>
						<td><@formatdays days=-1/></td>
					</#if>
					<#if item.archiveValue?exists>
					<#--Checks if archive text value is >= 0 -->
						<#if item.archiveValue?number &gt;= 0>
							<td><@formatdays days=item.archiveValue?number/></td>
						<#else>
							<td><@formatdays days=-1/></td>
						</#if>
					<#else>
						<td><@formatdays days=-1/></td>
					</#if>
				</tr>
			</#list>
		</TABLE>
		</div>
		
	</#if>
</div>
<#if showOtherConfiguration>
	<div class="formMiddle">
		<div style="position:relative;margin-left: 0px;margin-top: 10px;">
			<@s.label theme="simple" value="%{getText('systemConfiguration.otherConfiguration.title')}" cssClass="configurationHeading"/>
		</div>
		<div id="OtherConfig" style="position:relative;padding-left: 20px;padding-top: 10px;padding-bottom: 20px;">
			<#if showBatteryConfig>
				<#if readOnly="false">
					<@s.textfield tabindex="200" label="%{getText('systemConfiguration.otherConfiguration.batteryLastUsedLimit')}" name="batteryLastUsedLimit" id="batteryLastUsedLimit" size="3" maxlength="3" required="true" readonly="${readOnly}"/>
				<#else>
					<@s.label label="%{getText('systemConfiguration.otherConfiguration.batteryLastUsedLimit')}" name="batteryLastUsedLimit" id="batteryLastUsedLimit" size="3" maxlength="3" required="true"/>
				</#if>
			</#if>
			
			<#if showVLOtherConfig>
				<#if readOnly="false">
					<@s.textfield tabindex="200" label="%{getText('systemConfiguration.otherConfiguration.regionIntervals')}" name="regionIntervals" id="regionIntervals" size="50" maxlength="100" readonly="${readOnly}"/>
				<#else>
					<@s.label label="%{getText('systemConfiguration.otherConfiguration.regionIntervals')}" name="regionIntervals" id="regionIntervals" size="50" maxlength="100" />
				</#if>
			</#if>
			
		</div>
	</div>
</#if>
<#if !readOnly?has_content || readOnly=="false">
<div class="formAction" style="position:relative;margin-top: 20px">
	<#if !actionErrors?has_content>
		<#assign submitOnChangeForm="form1">
	</#if>
	
	<#assign submitIndex=199>
	<#assign cancelIndex=200>
	<#assign enabled=false>
	<#include "/include/form/buttonbar.ftl">
</div>
<script>
	connect($('form1.submit1'), 'onclick', checkIfErrors);
</script>
</#if>

</@s.form>
</div>

</body>
</html>
  
