 <div id="addsummaries">
 
<#if allSummaries?has_content>
 <@s.select 
    tabindex="1"
    label=""   
    name="summaries" 
    id="summaries"  
    list="allSummaries" 
    cssClass="summarySelect"  
    listValue="key" 
    listKey="value" value="value" theme="css_nonbreaking" tid="select.addsummaries" />
<@s.hidden id="showAvailability" name="showAvailability" value="true"/>
<@s.hidden id="location" name="location" />
<@s.hidden id="addSummary" name="addSummary" value="%{getText('add.this.summary.ok.button')}"/>
<@s.hidden id="cancelText" name="cancelText" value="%{getText('delete.noText')}"/>

<#else>
<@s.text name='summary.none.available'/>
<@s.hidden id="showAvailability" name="showAvailability" value="false"/>
<@s.hidden id="cancelText" name="cancelText" value="%{getText('delete.noText')}"/>
</#if>
</div>


