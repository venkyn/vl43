
<#assign readOnly="false">
<#if navigation.pageName="edit">
  <#assign title>
    <@s.text name='site.edit.title'/>
  </#assign>
  <#assign formName="edit">
<#elseif navigation.pageName="create">
  <#assign title>
    <@s.text name='site.create.title'/>
  </#assign>
  <#assign formName="create">
  <#assign submitValue>
    <@s.text name='site.create.button.submit'/>
  </#assign>
<#else>  
  <#assign title>
    <@s.text name='site.view.currentSite.title'/>
  </#assign>
  <#assign formName="view">
  <#assign readOnly="true">
</#if>

<#assign warningMessage>
	<@s.text name='site.modify.name.warning'/>
</#assign>

<script language="javascript">
	function getObject(obj) {
	    if (document.getElementById) {
	        obj = document.getElementById(obj);
	    } else if (document.all) {
	        obj = document.all.item(obj);
	    } else {
	        obj = null;
	    }
	    return obj;
	}

	function showWarning() {
	<#if formName == "edit">
		var labelDiv = getObject('wwctrl_site.name');
	   	var existingErrorLabel = getObject('changeNameErrorLabel');
	   	var newErrorLabel = 
	   		LABEL({'id':'changeNameErrorLabel',
	   				'name':'changeNameErrorLabel',
	   				'class':'listErrorMessage'}, '${warningMessage}');
		if ( existingErrorLabel == null ) {
			// We haven't had this before - so we need to create it
	   		appendChildNodes(labelDiv, newErrorLabel);
	   	} else {
			// The label exists - replace it with the new one
			labelDiv.replaceChild(existingErrorLabel, newErrorLabel);
		}	
	</#if>
	}
</script>

<html>
<head>
<title>${title}</title>
</head>
<body>
<div class="formBody">
<div class="titleBar">
	${title}
</div>

<@s.form name="${formName}" id="form1" validate="true" namespaceVal="/admin/site" action="${formName}.action" method="POST">
<#if siteId?has_content>
    <@s.hidden name="siteId"/>
    <@s.hidden name="savedEntityKey"/>
</#if>
<div class="formTop">
		<@s.textfield tabindex="1" label="%{getText('site.column.siteName')}" name="site.name" id="site.name" size="25" maxlength="256" required="true" readonly="${readOnly}" onchange="showWarning();"/> 
	    <@s.textfield tabindex="2" label="%{getText('site.description')}" name="site.description" id="site.description" size="50" maxlength="2048" readonly="${readOnly}"/>	
</div>
<div class="formMiddle">
    <!-- This looks like a hack, but the @select tag was not performing a lookup on the time zone when the form was readonly.-->
    <!-- Looks like this map is being treated differently than a list-->
    <#if readOnly == 'true'>
		<@s.textfield tabindex="3" label="%{getText('site.create.label.timezone')}" name="siteTimeZone" id="timeZone" readonly="${readOnly}"/>
    <#else>
		<@s.select tabindex="3" label="%{getText('site.create.label.timezone')}" name="site.timeZoneId" id="timeZone" list="timeZoneList" listKey="value" listValue="label" readonly="${readOnly}"/>
    </#if>
	<@s.textarea tabindex="5" theme="css_xhtml" label="%{getText('site.notes')}" name="site.notes" id="site.notes" rows="2" cols="50" readonly="${readOnly}"/>
	<@s.textfield tabindex="6" label="%{getText('site.shiftStartTime')}" name="site.shiftStartTime" id="site.shiftStartTime" size="5" maxlength="5" required="true" readonly="${readOnly}"/>
</div>
<div class="formAction">
	
	<#if siteId?has_content>
		<#if !actionErrors?has_content>
			<#assign submitOnChangeForm="form1">
		</#if>
	</#if>

	<#assign submitIndex=6>
	<#assign cancelIndex=7>

	<#include "/include/form/buttonbar.ftl">
</div>
<#-- Include a modified data div if there is modified data to display -->
<#if modifiedEntity?has_content>
<div id="modEntity" style="visibility:hidden;">
	<div class="modifiedTitle">
		<@s.text name='modifiedEntity.title'/>
	</div>
	<div class="formTop">
	    <@s.label label="%{getText('site.column.siteName')}" name="modifiedEntity.name"/>
	    <@s.label label="%{getText('site.description')}" name="modifiedEntity.description"/>	    
	</div>
	<div class="formMiddle">
	    <@s.label label="%{getText('site.create.label.timezone')}" name="modifiedEntity.timeZoneId"/>
	    <@s.textarea theme="css_xhtml" label="%{getText('site.notes')}" name="modifiedEntity.notes" rows="2" cols="50"/>
	    <@s.label label="%{getText('site.shiftStartTime')}" name="modifiedEntity.shiftStartTime"/>
	</div>
</div>
<script language="javascript">
	YAHOO.namespace("example.resize");

	function init() {
		YAHOO.example.resize.panel = new YAHOO.widget.Panel("modEntity",
			{
			  width:"450",
			  left: 300,
			  top: 200,
			  constraintoviewport: true,
			  fixedcenter: true,
			  underlay:"shadow",
			  close:true,
			  visible:false,
			  draggable:true,
			  modal:false } );
		YAHOO.example.resize.panel.render();
	}
		
	YAHOO.util.Event.addListener(window, "load", init);
</script>
</#if>
</@s.form>
</div>
</body>
</html>

