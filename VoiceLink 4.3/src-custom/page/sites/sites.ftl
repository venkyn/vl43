<title><@s.text name='site.view.title'/></title>
	<#include "/include/common/tablecomponent.ftl">
    <#assign extraParamsHash = {}>

 	<@tablecomponent
 		varName="siteObj"
		columns=siteColumns
		tableId="listSitesTable"
		url="${base}/admin/site"
		tableTitle='site.view.title'
		type=""
		extraParams=extraParamsHash?default("")
        viewId=view_Id />
		
