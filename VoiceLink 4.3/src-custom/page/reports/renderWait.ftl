<html>
<head>
</head>

<body onLoad="doload()">
   <script language="JavaScript">

        var sURL = unescape("render.action");

        function doLoad() {
            setTimeout( "refresh()", 1000 );
        }

        function refresh() { 
           window.location = "render.action";
        }

</script>

    <script>
    /**
     * Shows the wait panel while the log file is loading.
     */ 
    function showProgressBar() {
        waitPanel = new YAHOO.widget.Panel("wait", 
                  { width:"240px", 
                    fixedcenter:true, 
                    underlay:"shadow", 
                    close:false, 
                    draggable:false, 
                    modal:false, 
                    effect:{effect:YAHOO.widget.ContainerEffect.FADE, duration:0.5}
                  } 
               );                  
      waitPanel.setHeader("<@s.text name='wait.message'/>");
      waitPanel.setBody(IMG({'src':'/epp/images/waiting.gif'}));
      waitPanel.render(document.body);    // centers correctly if use document.body
      waitPanel.show();
    }
    </script>
    <script for="window" event="onload" language="javascript">
      showProgressBar();
      doLoad();
    </script>

	<div class="formBody">
		<div class="titleBar">
			<@s.text name='report.wait.title'/>
		</div>
		<div class="logAttributes">
			<@s.text name='report.wait.message'/>
		</div>
	  	<div style="height:116px;">
	  	&nbsp;
	  	</div>
	</div>

</body>
</html>
