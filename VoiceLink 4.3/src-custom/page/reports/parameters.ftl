<#include "/include/common/customdialogdate.ftl">
<#assign title>
    <@s.text name='${reportNameKey}'/>
</#assign>
<#assign calendarButton>
    <@s.text name='report.button.showcalendar'/>
</#assign>
<#assign formId>
    <@s.text name='form1'/>
</#assign>
<title>${title}</title>
<script type="text/javascript">
	var calId;
    // This validation function will contain all field-level validation function calls,
    // based on the validation function specified in the database.
    function formValidation() {
        var valid = '';
        var errorsExist = false;
        var mandatoryCount1 = 0;
        var mandatoryCount2 = 0;
        var err_startdate = "";
        var err_enddate = "";
        var err_mandatory = "";
        var max_length = "";
        var startDate = "";
        var endDate = "";
        
        <#assign cnt=1>
        <#list processedParameters as validators>
            if("${cnt}" ==  1) {
                err_mandatory = "${validators.parameterName}";
            }
                     
            <#if (validators.isRequired = true )>
            	    mandatoryCount1++;
                    mandatoryCount2++;
            </#if> 
            <#if validators.validationFunction?exists && validators.validationFunction != "">
                var err_id = "";
                <#if validators.parameterType ="class java.lang.Boolean">
                    var checkBox${cnt} = $("${validators.parameterName}");
                    valid = ${validators.validationFunction}(checkBox${cnt}.checked);
                <#elseif validators.parameterType ="class java.util.Date">
                    // The date fields have a fixed ID for the calendar
                    var text${cnt} = $("dateTextField${cnt}");
                    valid = ${validators.validationFunction}(text${cnt}.value);
                    if("${validators.parameterName}" == "START_DATE") {
                        err_startdate = "dateTextField${cnt}";
                        startDate = text${cnt}.value;
                    } else {
                    	err_enddate = "dateTextField${cnt}";
                        endDate = text${cnt}.value;
                    }
                    var err_id = "dateTextField${cnt}";
                <#else>
                    // Use the parameter name
                    var text${cnt} = $("${validators.parameterName}");
                    if("${validators.maxLength}" != '' &&  ("${validators.maxLength}" != undefined) )
                    {
                    	
                    	
                    	valid = ${validators.validationFunction}(text${cnt}.value, "${validators.maxLength}"); 
                    }else{
                    	valid = ${validators.validationFunction}(text${cnt}.value, 0); 
                    }
                   <#if validators.isRequired = true>
                    if( (text${cnt}.value) != '') {
                        mandatoryCount2--;
                    }
                </#if>
                </#if>
                if ((valid != '') && (valid != undefined)) {
                    if ( err_id == "" ) {
                        replaceChildNodes("wwerr_${validators.parameterName}", DIV({"class":"errorMessage"}, valid));
                    } else {
                        replaceChildNodes("wwerr_" + err_id, DIV({"class":"errorMessage"}, valid));                     
                    }
                    errorsExist = true;
                } else {
                    if ( err_id == "" ) {
                        replaceChildNodes("wwerr_${validators.parameterName}", null);
                    } else {
                        replaceChildNodes("wwerr_" + err_id, null);
                    }
                }
                

            </#if>
            
            <#if ( validators.isRequired = true ) && (validators.isFromList = true)>
                var text${cnt} = $("${validators.parameterName}");
                if((text${cnt}.value) != '_ALL_') {
                    mandatoryCount2--;
                }
            </#if>
            <#assign cnt=cnt + 1>
        </#list>
        if ( (mandatoryCount1 != 0) && ((mandatoryCount1 - mandatoryCount2)  == 0) ){
          replaceChildNodes("wwerr_" + err_mandatory, DIV({"class":"errorMessage"}, mandatoryCheckFailed()));                     
          return;
        }
        valid = validateDates(startDate, endDate);
        if((valid != '') && (valid != undefined) ){
            if( (startDate != '') && (endDate == '')){
                replaceChildNodes("wwerr_" + err_enddate, DIV({"class":"errorMessage"}, valid));                     
            } else {
                replaceChildNodes("wwerr_" + err_startdate, DIV({"class":"errorMessage"}, valid));                      
            }
            errorsExist =  true;
        }
        if (!errorsExist) {
            submitForm($('${formId}'));
        }
    }

</script>

  
<div class="formBody">
    <div class="titleBar">
        ${title}
    </div>  


    <#assign formName="parameters">

    <@s.form name="${formId}" id="${formId}" validate="false" namespaceVal="${reportNamespace}" action="render.action" method="POST">

        <#if userId?has_content>
            <@s.hidden name="userId"/>
            <@s.hidden name="savedEntityKey"/>
        </#if>
        <!-- A couple parameters are stored in HTML to pass into the render action. -->
        <@s.hidden name="userName"/>
        <@s.hidden name="siteName"/>
        <@s.hidden name="sessionBeanId"/>
        
        <div class="formTop">
        <#-- POTENTIAL LIST VALUES
             *  String
             *  Integer
             *  Long
             *  Double
             *  Boolean
             *  Date -->
            <#assign iter=1>
            
            <#list processedParameters as param>            
              <#if  param.isRequired = true >
                <#assign required="true">
              <#else>  
                <#assign required="false">
              </#if> 
                <#if param.parameterType = "class java.lang.String" && param.isFromList = false >
                    <#if param.parameterName = "SESSION_VALUE" || param.parameterName = "SITE">
                    	<#assign readonlyvalue="true">
                    	<#assign max = 50>
                    <#else>
                    	<#assign readonlyvalue="false">
                    	<#assign max = "${param.maxLength}">
                    </#if>
                    <@s.textfield
                        tabindex="${iter}"
                        label="${param.description}"
                        name="${param.parameterName}"
                        id="${param.parameterName}"
                        value="${param.defaultValue}"
                        size="25"
                        maxlength="${max}"
                        required="${required}"
                        readonly="${readonlyvalue}"/>
                <#elseif param.parameterType = "class java.lang.Boolean">
                    <#-- The css_xhtml checkbox does not work properly; it creates
                    two div tags but tries to close three of them in the generic
                    footer.  -->
                    <br />
                    <br />&nbsp;&nbsp;&nbsp;&nbsp;
                    <@s.checkbox tabindex="${iter}" theme="simple" name="${param.parameterName}"
                        id="${param.parameterName}" cssClass="checkbox"
                        label="${param.description}" readonly="false"
                        value="${param.defaultValue}" />
                    <br />
                <#elseif param.parameterType = "class java.util.Date">
                    <@s.textfield tabindex="${iter}" label="${param.description}"
                        name="${param.parameterName}" id="dateTextField${iter}" size="25"
                        maxlength="10" required="${required}" readonly="false"
                        value="${param.defaultValue}" />

                    <style type="text/css">
                        div#cal${iter} {
                            z-index: 10;
                            position: relative;
                        }
                    </style>

                    <@customdialogdate
                      dialogId="div_report_dateParam${iter}"
                      header="report.selectdate"
                      dateInputFieldId="dateTextField${iter}"
                      callingButtonId="calendarLink${iter}"
                      multiPage="true"/>
                    <div style="margin-left: 140px;">
                        <a id="calendarLink${iter}" href="#">${calendarButton}</a>
                    </div>
                    

                <#elseif param.parameterType = "class java.lang.Integer" && param.isFromList = false >
                    <@s.textfield tabindex="${iter}" label="${param.description}" name="${param.parameterName}" id="${param.parameterName}" size="25" maxlength="${param.maxLength}" required="${required}" readonly="false"/>
                <#elseif param.parameterType = "class java.lang.Double" && param.isFromList = false >
                    <@s.textfield tabindex="${iter}" label="${param.description}"
                        name="${param.parameterName}" id="${param.parameterName}" size="25"
                        maxlength="${param.maxLength}" required="${required}" readonly="false"
                        value="${param.defaultValue}"/>
                <#elseif param.parameterType = "class java.lang.Long" && param.isFromList = false >
                    <@s.textfield tabindex="${iter}" label="${param.description}"
                        name="${param.parameterName}" id="${param.parameterName}" size="25"
                        maxlength="${param.maxLength}" required="${required}" readonly="false"
                        value="${param.defaultValue}"/>
                <#elseif param.isFromList = true>
                    <@s.select tabindex="${iter}" label="${param.description}" name="${param.parameterName}"
                        id="${param.parameterName}" list=param.dropDownData listKey="value"
                        value="${param.defaultValue}"
                        listValue="label" required="${required}" readonly="false"/>
                        <script type="text/javascript">
                                // Get a reference to the drop-down
                                function selectValue${param.parameterName}() {
                                    var text${param.parameterName} = document.getElementById('${param.parameterName}');
                                    // Loop through all the items
                                    if (text${param.parameterName}.options == undefined) {
                                        return;
                                    }
                                    for (iLoop = 0; iLoop< text${param.parameterName}.options.length; iLoop++) {    
                                        if (text${param.parameterName}.options[iLoop].value == '${param.defaultValue}') {
                                            // Item is found. Set its selected property, and exit the loop
                                            text${param.parameterName}.options[iLoop].selected = true;
                                            text${param.parameterName}.options[iLoop].value = '${param.defaultValue}';
                                            break;
                                        }
                                    }
                                }
                                selectValue${param.parameterName}();
                        </script>
                <#else>
                    ${param.parameterType}:UNRECOGNIZED PARAMETER TYPE..  SKIPPED PROMPT
                </#if>
                <#assign iter=iter + 1>
            </#list>
            <div id="cal1Container" position=""></div> 
        </div>
        <div class="formAction">
            <#assign submitOnChangeForm="${formId}">
            <#assign submitIndex=199>
            <#assign cancelIndex=200>
            <#assign submitValue>
                <@s.text name='report.generate'/>
            </#assign>
            <#assign cancelValue>
                <@s.text name="form.button.cancel"/>
            </#assign>

            <input type="hidden" name="submitTypeElement" value="" id="submitTypeElement"/>

            <#if !formId?has_content>
                <#assign formId="${formId}">
            </#if>

            <#--href="javascript:formvalidation();" -->
            <a href="javascript:formValidation();"
                id="${submitId?default('${formId}.submit1')}"
                class="${submitCssClass?default('anchorButton')}"
                tabindex="${submitIndex?default('-1')}"
            >${submitValue}</a>
                    
            <script type="text/javascript">
                MochiKit.Signal.connect("${submitId?default('${formId}.submit1')}","onfocus",showFocusOnLink);
                MochiKit.Signal.connect("${submitId?default('${formId}.submit1')}","onblur",hideFocusOnLink);
            </script>

            <a href="javascript:window.close();"
                id="${cancelId?default('${formId}.cancel1')}"
                class="${cancelCssClass?default('anchorButton')}"
                tabindex="${cancelIndex?default('-1')}"
            >${cancelValue}</a>
    
            <script type="text/javascript">
                MochiKit.Signal.connect("${cancelId?default('${formId}.cancel1')}","onfocus",showFocusOnLink);
                MochiKit.Signal.connect("${cancelId?default('${formId}.cancel1')}","onblur",hideFocusOnLink);
            </script>

        </div>
    </@s.form>
</div>
