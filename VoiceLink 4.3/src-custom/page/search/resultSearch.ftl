<#assign siteMinusImg = "../../images/minusblue.png"/>
<#assign sitePlusImg = "../../images/plusblue.png"/>
<#assign areaMinusImg = "../../images/minus.gif"/>
<#assign areaPlusImg = "../../images/plus.gif"/>
<#assign none = "\'none\'"/>
<#assign block = "\'block\'"/>
<title><@s.text name='feature.search.name'/></title>
<body>

	<div class= "Container" align= "left">
	</div>

	<div id= "SearchContent">
		<div id= "listUsersTableDiv"><div id="SearchContentTitle"><@s.text name='feature.search.name'/></div>
			<div id= "results">
			<table><tr><td>
			  <#if resultCollection?exists && resultCollection.siteList?has_content>
					${searchSuccessText}
				<div class="features">
					<div class="area">
						<#list resultCollection.siteList as siteList>
							<div class="hd">
							<#if (resultCollection.getIndex("${siteList.name}") == 0)>
								<#assign hide = "false"/>
								<#assign siteImg = siteMinusImg/>
								<#assign areaImg = areaMinusImg/>
							<#else>
								<#assign hide = "true"/>
								<#assign siteImg = sitePlusImg/>
								<#assign areaImg = areaMinusImg/>
							</#if>
							<img id="img_${siteList.name}" 
									src="${siteImg}" 
									onClick="javascript:toggleComponent('${siteList.name}', '${sitePlusImg}', '${siteMinusImg}');">
							${action.getResultSizeText(siteList.name, siteList.size?c, siteList.hiddenCount?c)}
						</div>			
						<div class="bd" id="${siteList.name}">
							<#if hide == "true">
								<script type="text/javascript"/>
									toggleComponent('${siteList.name}', '${sitePlusImg}', '${siteMinusImg}');
								</script>	
							</#if>
																
							<#list siteList.data as areaList>
								<img id="img_${siteList.name}_${areaList.name}" 
										onClick="javascript:toggleComponent('${siteList.name}_${areaList.name}', '${areaPlusImg}', '${areaMinusImg}');" 
										src="${areaImg}">
								
								<b>${action.getResultSizeText(areaList.name, areaList.size?c, areaList.hiddenCount?c)}</b><br>
								
					      		<div id="${siteList.name}_${areaList.name}" class="results">
									<#list areaList.data as results>
										<#if results.isReadOnly = true>
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											${results.displayName}<br>
										<#else>
											<#if siteNameToId?has_content>
												<#if siteNameToId["${siteList.name}"]?has_content>
													<#assign siteTagId = siteNameToId["${siteList.name}"]>
												</#if>
											</#if>
											<#if siteTagId?has_content>
												<@s.url includeParams='none'  value='${results.targetUrl}&currentSiteID=${siteTagId}' id='url'/>
											<#else>
												<@s.url includeParams='none'  value='${results.targetUrl}' id='url'/>
											</#if>
											<a href="${url}">${results.displayName}</a><br>
										</#if>
									</#list>
								</div>
							</#list>
						</div>
					</#list>
					</div>
					</div>
				<#else>
					${searchNoResultsText}
				</#if>
			</td>
			<td width="20"></td>
			<td valign="top"></td>
			</tr></table>
			</div>
		</div>
	</div>
	<div style="clear: both;">&nbsp;</div>
    </div>
</body>
