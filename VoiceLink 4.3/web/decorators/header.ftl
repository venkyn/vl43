<#-- You must use caution to ensure that no Freemarker expresssions can fail
     here, because the header is used on the error pages, so an error here can 
     cause a cyclic failure. -->
<div id="header">
    <div id="logo">
    </div>
    <div id="leftHeaderPatterns">
        <span class="title">
          <@s.text name='user.profile.header' />
        </span>
        <#if currentUser?has_content> 
        <a tabindex="351" href="<@s.url includeParams='none'  value='/viewProfile.action'/>">${currentUser.name?html}</a> |
        <#else>  
        &nbsp; |  
        </#if>
        <a tabindex="353" href="<@s.url includeParams='none'  value='/logout.action'/>">
        	<@s.text name='nav.userInfo.logout'/>
		</a>
		&nbsp;&nbsp; &nbsp; 
        <span class="title">
          <@s.text name='licensing.header' />
        </span>
		 <a tabindex="353" href="<@s.url includeParams='none'  value='/admin/licensing/view.action'/>">
			<#if currentLicenseCompany?has_content>
	        	${currentLicenseCompany}:${currentLicenseSite}
	        <#else>
	        	<span class="unlicensed"><@s.text name='licensing.unlicensed'/></span>
			</#if>
         </a>
    </div>
    
    <#include "/include/common/appButton.ftl">
    
	<div id="apps">
		<ul>

	    <#list action.availablePluginModules as module>
			<#if navigation?has_content && module.getIsCurrent(navigation.applicationMenu)>
				<#assign class="select">
				<#assign currentModule=module>
			<#else>
				<#assign class="">
			</#if>
			<li>
				<@appButton url="${module.getUrl(request)}" text="${module.name}" id="${module.name}" class="${class}" />
			</li>
		</#list>
	    
		</ul>
	</div>

</div>
<#if currentModule?has_content && currentModule.hasComponents>
	<div id="tabs">
		<ul>
	    <#list currentModule.components as component>
	    	<#if currentSiteID==-927>
            	<@appTab text="${component.name}"
                	     name="${component.navMenuName}" 
                    	 uri="${component.uri}?currentSiteID=-927"
	                     accessible=action.getIsPluginAccessible(component)/>
	        <#else>
            	<@appTab text="${component.name}"
                	     name="${component.navMenuName}" 
                    	 uri="${component.uri}"
	                     accessible=action.getIsPluginAccessible(component)/>
	        </#if>
		</#list>
		</ul>
	</div>
</#if>
