<!--
	# Vocollect HTML Application Information
	# <@s.text name='label.appDisplayName'/> - <@s.text name='label.appVersion'/> 
	# (<@s.text name='label.buildIdentifier'/>)
	# <@s.text name='label.copyrightMessage'/>
-->
<head>
    <@s.head theme="ajax">
    	<meta http-equiv="X-UA-Compatible" content="IE=9" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">        
		
        <#-- Include the minified JS and CSS Files -->
		<link rel="stylesheet" type="text/css" href="${base}/css/VoiceLink-201410160200.css" />
		
		<script type="text/javascript">	
	
    		<#-- Set up the timeout overlay buttons and a messages -->	
    		<#assign timeoutWindowTitle=stack.findValue("getText('sessionTimeout.window.title')")>
    		<#assign timeoutPromptText=stack.findValue("getText('sessionTimeout.message.prompt')")>
    		<#assign timeoutPromptButton=stack.findValue("getText('sessionTimeout.button.prompt')")>
    		<#assign timeoutExpiredText=stack.findValue("getText('sessionTimeout.message.expired')")>
    		<#assign timeoutExpiredButton=stack.findValue("getText('sessionTimeout.button.expired')")>
    		<#assign timeoutErrorText=stack.findValue("getText('sessionTimeout.message.error')")>
    		<#assign timeoutErrorButton=stack.findValue("getText('sessionTimeout.button.error')")>
    		<#assign filterEqualTo=stack.findValue("getText('operand.0.equals')")>
    	
    		<#-- these variables must be set when using timeout.js -->
    		var SessionTimeout = ${session.maxInactiveInterval?c};
           	var Notice = 30;
           	var timeoutWindowTitle = "${timeoutWindowTitle}";
           	var timeoutPromptText = "${timeoutPromptText}";
           	var timeoutPromptButton = "${timeoutPromptButton}";
           	var timeoutExpiredText = "${timeoutExpiredText}";
           	var timeoutExpiredButton = "${timeoutExpiredButton}";
           	var timeoutErrorText = "${timeoutErrorText}";
           	var timeoutErrorButton = "${timeoutErrorButton}";      	
           	var filterEqualTo = "${filterEqualTo}";
           	
           	
           	var globals = {
           		 BASE : "${base}"
           		<#if navigation?has_content>
           		,	PAGE_CONTEXT : "${base}/${navigation.applicationMenu}"
           		</#if>
           		, TITLE : "<@s.text name='label.appDisplayName'/> - ${title}"
           		<#if currentUser?has_content> 
           	    , USER_NAME : "${currentUser.name?html}"
           		</#if>
           		};   
       	         		
       	</script>
		
       	<script type="text/javascript" src="${base}/scripts/VoiceLink-201410160200.js"></script>
       	
		<#-- Include additional app-specific CSS files -->
		<#include "/include/common/appcss.ftl">
		<#include "/include/common/inlineJsCss.ftl">

		<#-- Include additional app-specific and dwr scripts -->
		<#include "/include/common/appjs.ftl">		
       	<script type="text/javascript" src='${base}/dwr/engine.js'></script>
       	
       	<#-- would like to remove this -->
		<script type="text/javascript" src="${base}/scripts/vocollect/dwrfix.js"></script>	
		
		<script type="text/javascript">
			connect( window, "onload", watchForUnackNotifications );
			connect(currentWindow(), 'onload', dwrFix);
			var logToServer = new LogToServer({'url': "${base}/javaScriptLog.action", 'loggingOn': false});
			logToServer.setLogLevel(logToServer.ERROR);
			setLocalizedVariables("<@s.text name='status.more.information'/>",
	                              "<@s.text name='status.view.details'/>",
	                              "<@s.text name='status.stopped.updating'/>",
	                              "<@s.text name="lastUpdateError.defaultErrorMessage"/>",
	                              "<@s.text name="wait.message"/>");		
		</script>
        
        <#-- Set Vocollect application icon -->
        <link rel="shortcut icon" href="${base}/favicon.ico">
        
        <#-- Page Title -->
        <title><@s.text name='label.appDisplayName'/> - ${title}</title>
        
    </@s.head>
</head>
