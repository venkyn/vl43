<#--
    This is the help menu.

    -John Zeitler, 02 Nov 2006
-->
<#include "/include/common/action.ftl" />
<div class="spacer"></div>
    <div class="menuHeader" id="menuHeaderId">
        <@s.text name='nav.help.menu' />
    </div>
    <ul class="NavList">
        <@action
                name="nav.help.context"
                tableId=""
                id="a_contexthelp"
                url="javascript:openContextHelp();"
                tabIndex="291"
                actionEnablers={"checkForHelp":"nav.help.notfound"}
                featureName="always" />
        <@action
                name="nav.help.global"
                tableId=""
                id="a_globalhelp"
                url="javascript:openGlobalHelp();"
                tabIndex="292"
                actionEnablers={}
                featureName="always" />
        <#-- This inclusion will break in VoiceLink space and must exist in VoiceConsole space -->
        <#--
        <@action
                name="nav.hardware.help.global"
                tableId=""
                id="a_globalhardwarehelp"
                url="javascript:openGlobalHardwareHelp();"
                tabIndex="293"
                actionEnablers={}
                featureName="always" />
        -->
        <@action
                name="about.page"
                tableId=""
                id="a_aboutPage"
                url="/admin/aboutPage.action"
                tabIndex="294"
                actionEnablers={}
                featureName="always" />
    </ul>
