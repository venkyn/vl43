<!DOCTYPE html>

<html>
	<head>
    <#include "/decorators/head.ftl">
	</head>
	<body>
	    <div id="container">
	        <#include "/decorators/header.ftl">
		    <#include "/include/common/messages.ftl">
		    <div style="padding: 10px;">
				${body}
			</div>
	    </div>
	</body>
</html>