<#if (navigation.applicationMenu)?has_content>
    <#-- Check if the user object has side nav menu open-->
    <script type="text/javascript">
    <#-- Create a new actions menu for use on the entire page. -->
    var actionsMenuObj = new ActionsMenu();
    function getActionsMenu() { return actionsMenuObj; }
    
    /**
    * All row selections alert the actions menu object.
    */
    // Once the page is ready
    $j(document).ready(function() {
        // Wait until all tables have fully loaded
        $j(document).bind(events.ALL_TABLES_LOADED, function() {
            $j(document).bind(events.SELECTION_CHANGE, function(event, tableObj) {
                actionsMenuObj.updateActions(tableObj);
            });
        });
    });
    
    </script>
    <#if currentUser.navMenuOpen>
    <div id="sidenav">
        <div id="sidenav_inside">
          <img src="${base}/images/sideNav_top_divider.gif" class="top" />
          <div id="sidenav_handle">
          <#-- show open_arrow as the menu nav is open -->
         <#include "/include/common/collapsableMenu.ftl" />
          </div>
          <#if numberOfSites != 1 >
              <div class="menuHeader"><@s.text name='nav.site.information'/></div>
              <div class="siteInfo">
                  <#if navigation.actionsMenu?has_content && navigation.actionsMenu == "search">
                        <@s.text name='nav.sites.AllSites' />
                  <#elseif textAction>
                        <#assign escapedSiteName=singleSiteName?replace("\'", "\\\'") />
                        <script type="text/javascript">
                            name = '${escapedSiteName}';
                            if(name.length>25){
                                name = name.substr(0,25) + '...';
                            }
                            document.write(name);
                        </script>
                  <#else>
                  <@s.select id="selectedSite" name="currentSiteID" theme="simple" list="siteList" listKey="id" listValue="value" 
                    multiple="false" readonly="false" cssClass="site" tid="select.site"/>
                    
                  <script type="text/javascript">
                    connect($('selectedSite'), 'onchange', handleSiteChange);
                  </script>
                  </#if>
              </div>
              <div class="spacer"></div>
          </#if>
          <!-- 
            Put the site switching element here based off action data
          -->
          <#include "/include/navigation/${navigation.applicationMenu}.ftl">
            <#if navigation.actionsMenu?has_content 
             && navigation.actionsMenu != "search"
             && navigation.actionsMenu != "home" >
                <#include "/include/navigation/actionmenus/menu.ftl" >
            </#if>
          <div class="spacer"></div>
            <#include "/decorators/searchMenu.ftl">
            <#include "/decorators/helpMenu.ftl">
        </div>
    </div>
    <!--[if IE]>
        <script type="text/javascript">
            adjustBubbleHeightForIE();
        </script>
    <![endif]-->
    <#else>
    <#-- if side nav menu is closed show empty nav menu div -->
    <div id="sidenav" style= "height:${currentUser.sideNavHeight};left:-135px;">
        <div id="sidenav_inside" >
        
          <#-- remove this image to make the top divider flush to the tab bar -->
          <img src="${base}/images/sideNav_top_divider.gif" class="top" />
          <div id="sidenav_handle">
          <#-- Show close_arrow as the nav menu is closed -->
         <#include "/include/common/collapsableMenu.ftl"/>
          </div>
            
              <div class="menuHeader">&nbsp;</div>
              <div class="siteInfo">
                  &nbsp;
              </div>
              <div class="spacer"></div>
              
          <!-- 
            Put the site switching element here based off action data
          -->
        
            
        </div>
    </div>
    </#if>      
</#if>

<script type="text/javascript">
if(navigator.appName=='Netscape'){
    wrapLongHeaders();
}

<#if dropDownId?exists>
if(navigator.appName=='Microsoft Internet Explorer'){
    ${dropDownId}_selectfix=new dropdownFix(<#if dropDownNamespaces?seq_contains(navigation.actionsMenu)>true<#else>false</#if>,'${dropDownId}');
    YAHOO.util.Event.addListener('${dropDownId}','mousedown',${dropDownId}_selectfix.expandDrop,'${dropDownId}');
}
</#if>

</script>     
