<!--
	# Vocollect HTML Application Information
	# <@s.text name='label.appDisplayName'/> - <@s.text name='label.appVersion'/> 
	# (<@s.text name='label.buildIdentifier'/>)
	# <@s.text name='label.copyrightMessage'/>
-->
<head>
    <@s.head theme="ajax">
    
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">        
		
        <#-- Include the minified JS and CSS Files -->
		<link rel="stylesheet" type="text/css" href="${base}/css/VoiceLink-201410160200.css" />
		
		<script type="text/javascript">	
	
    		<#-- Set up the timeout overlay buttons and a messages -->	
    		<#assign timeoutWindowTitle=stack.findValue("getText('sessionTimeout.window.title')")>
    		<#assign timeoutPromptText=stack.findValue("getText('sessionTimeout.message.prompt')")>
    		<#assign timeoutPromptButton=stack.findValue("getText('sessionTimeout.button.prompt')")>
    		<#assign timeoutExpiredText=stack.findValue("getText('sessionTimeout.message.expired')")>
    		<#assign timeoutExpiredButton=stack.findValue("getText('sessionTimeout.button.expired')")>
    		<#assign timeoutErrorText=stack.findValue("getText('sessionTimeout.message.error')")>
    		<#assign timeoutErrorButton=stack.findValue("getText('sessionTimeout.button.error')")>
    		<#assign filterEqualTo=stack.findValue("getText('operand.0.equals')")>
    	
    		<#-- these variables must be set when using timeout.js -->
    		var SessionTimeout = ${session.maxInactiveInterval?c};
           	var Notice = 30;
           	var timeoutWindowTitle = "${timeoutWindowTitle}";
           	var timeoutPromptText = "${timeoutPromptText}";
           	var timeoutPromptButton = "${timeoutPromptButton}";
           	var timeoutExpiredText = "${timeoutExpiredText}";
           	var timeoutExpiredButton = "${timeoutExpiredButton}";
           	var timeoutErrorText = "${timeoutErrorText}";
           	var timeoutErrorButton = "${timeoutErrorButton}";      	
           	var filterEqualTo = "${filterEqualTo}";
           	
           	
           	var globals = {
           		 BASE : "${base}"
           		<#if navigation?has_content>
           		,	PAGE_CONTEXT : "${base}/${navigation.applicationMenu}"
           		</#if>
           		, TITLE : "<@s.text name='label.appDisplayName'/> - ${title}"
           		<#if currentUser?has_content> 
           	    , USER_NAME : "${currentUser.name?html}"
           		</#if>
           		};   
       	         		
       	</script>
		
       	<script type="text/javascript" src="${base}/scripts/VoiceLink-201410160200.js"></script>
       	
    </@s.head>
</head>
