<!DOCTYPE html>
<html>
        <#include "/decorators/head.ftl">
  <body>
    <script type="text/javascript">
		//default contextHelpUrl; indicates no help is available
		var contextHelpUrl = -1;
	</script>
    <div id="container" >
		<#include "/include/common/messages.ftl">
    	<#include "/decorators/header.ftl">
    		<div id="middle">
	        <#include "/decorators/nav.ftl">
	        <#if navigation?exists>
	        <#-- Check if the user object has side nav menu open-->
	        <#if currentUser.navMenuOpen>
	        <div id="topstatus">
	        <#else>
	        <div id="topstatus"  style="margin-left: 40px;">
	        </#if>
	        	<#assign bcTabIndex=376>
	        	<div id="breadcrumbs">
				    <#if breadCrumbTrail?has_content>
						<#list breadCrumbTrail as crumb>
							<#if crumb.source?has_content>
								<a tabindex="${bcTabIndex}" href="<@s.url includeParams='none'  value='${crumb.source}'/>"
								<#if crumb.id?has_content>
									id="${crumb.id}"
								</#if>
								><@s.text name="${crumb.text}"/></a><span class="arrows"> >> </span>
							<#else>
								<@s.text name="${crumb.text}"/>
							</#if>
							<#assign bcTabIndex= bcTabIndex + 1>
				    	</#list>
				    </#if>
		       	</div>
		    </div>
			<div style="clear:both;">
			    
			</div>
		    </#if>
		    <#-- Check if the user object has side nav menu open-->
		    <#if currentUser.navMenuOpen>
	        <div id="content">
	        <#else>
	        <div id="content" style="margin-left: 40px;">
	        </#if>
				${body}
			</div>
			<div style="clear:both;">&nbsp;</div>
		</div>
    </div>
	<script type="text/javascript">
        <#-- force action enablers to run if no tables are present. -->
        var am = getActionsMenu();
        if (am != undefined && getElementsByTagAndClassName("div", "tableDiv").length == 0) {
            am.updateActionsNoTable();
        }
    </script>
    <div id="timeStamp">
        <@s.text name="label.lastUpdated"/>&nbsp;<span id="timeStampValue">${currentSiteDateTime}</span>
    </div>
  </body>
</html>
