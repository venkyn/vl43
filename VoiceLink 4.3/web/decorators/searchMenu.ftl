<@s.form theme="css_xhtml" name="searchForm" id="searchForm" validate="false" action="/${navigation.applicationMenu}/search/result.action" method="POST">
<div class="menuHeader" id="searchDiv">
	<@s.text name='nav.search.menu'/>
	<@s.textfield size="15" theme="css_nonbreaking" id="searchText" name="searchText" tid="textfield.search"/>
	<@s.submit type="button" id="searchButton" name="searchButton" value="%{getText('search.button')}" onclick="javascript:checkText($(searchForm).searchText.value);return false"/>
</div>
</@s.form>
