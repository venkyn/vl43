<!DOCTYPE html>
<html>

        <#assign title>
            <@s.text name='nav.dashboardalert.myDashboard'/>
        </#assign>
        
        <#include "/decorators/head.ftl">
  <body>
    <script type="text/javascript">
        //default contextHelpUrl; indicates no help is available
        var contextHelpUrl = -1;
    </script>
    <div id="container" >
        <#include "/include/common/messages.ftl">
        <#include "/decorators/header.ftl">
            <div id="middle">
            <#include "/decorators/nav.ftl">
            <#if navigation?exists>
            <#-- Check if the user object has side nav menu open-->
            <#if currentUser.navMenuOpen>
            <div id="topstatus">
            <#else>
            <div id="topstatus"  style="margin-left: 40px;">
            </#if>
                <#assign bcTabIndex=376>
            </div>
            <div style="clear:both;">
                
            </div>
            </#if>
            <#-- Check if the user object has side nav menu open-->
            <#if currentUser.navMenuOpen>
            <div id="content">
            <#else>
            <div id="content" style="margin-left: 40px;">
            </#if>
                ${body}
            </div>
            <div style="clear:both;">&nbsp;</div>
        </div>
    </div>
    <script type="text/javascript">
        <#-- force action enablers to run if no tables are present. -->
        var am = getActionsMenu();
        if (am != undefined && getElementsByTagAndClassName("div", "tableDiv").length == 0) {
            am.updateActionsNoTable();
        }
    </script>
  </body>
</html>
