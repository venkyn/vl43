<!DOCTYPE html>

<html>
	<#include "/decorators/head.ftl">
	<body>
	    <script type="text/javascript">
	    <#-- define this function to return null so the table component macro
	         can handle not having an actions menu object. -->
	        function getActionsMenu() { return null; }
	    </script>
	    <div id="container">
	        <#include "/decorators/header.ftl">
		    <#include "/include/common/messages.ftl">
		    <div id="contentWrap">
				${body}
			</div>
	    </div>
	</body>
</html>
