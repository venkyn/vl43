<!DOCTYPE html>
<html>
        <#include "/decorators/head.ftl">
<body>

    <div id="containment">
		<div id="header">
		    <div id="logo">
		    </div>
		</div>
        <div id="contentWrap" align="center">
	        <div id="login" align="left">
				${body}
			</div>
		</div>
    </div>
    <#include "/decorators/footer.ftl">
</body>
</html>