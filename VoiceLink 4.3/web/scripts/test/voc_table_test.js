/**
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 *
 * Table Component 2.0 Test cases.
 *
 */

/**
 * This tests the handlePublisher() function
 */
test('handlePublisher(event, tableObj)', function() {

});

/**
 * This tests the getSelectedIds() function
 */
test('getSelectedIds()', function() {

});

/**
 * This tests the clearSelectedRows() function
 */
test('clearSelectedRows()', function() {

});

/**
 * This tests the determineInitialSort() function
 */
test('determineInitialSort()', function() {

});

/**
 * This tests the setup_loader() function
 */
test('setup_loader()', function() {

});

/**
 * This tests the setup_grid() function
 */
test('setup_grid()', function() {

});

/**
 * This tests the displayRowCount(any)() function
 */
test('displayRowCount(count)', function() {

});

/**
 * This tests the clickFilterLink() function
 */
test('clickFilterLink()', function() {

});

/**
 * This tests the getColumnById() function
 */
test('getColumnById(id)', function() {

})

/**
 * This tests the refresh() function
 */
test('refresh()', function() {

});

/**
 * This tests the updateFilterCriteria() function
 */
test('updateFilterCriteria()', function() {

});


/**
 * This tests the getVisibleColumns() function
 */
test('getVisibleColumns()', function() {

});

/**
 * This tests the update_columns() function
 */
test('update_columns()', function() {

});

/**
 * This tests the add_column() function
 */
test('add_column(col, idx)', function() {

});

/**
 * This tests the remove_column() function
 */
test('remove_column(name)', function() {

});

/**
 * This tests the setHeight() function
 */
test('setHeight(height, bool)', function() {

});

/**
 * This tests the initialize_constants() function
 */
test('initialize_constants()', function() {

});
