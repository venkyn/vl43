/* This prevents a known issue with DWR when the conn is broken. */
function dwrFix() {
    DWREngine._stateChange = function(batch) {
	if (!batch.completed && batch.req.readyState == 4) {
	    try {
		var reply = batch.req.responseText;
		if (reply == null || reply == "") {
		    //DWREngine._handleMetaDataError(null, "No data received from server");
		    return;
		}
		var contentType = batch.req.getResponseHeader('Content-Type');
		if (!contentType.match(/^text\/plain/)
			&& !contentType.match(/^text\/javascript/)) {
		    DWREngine._handleMetaDataError(null,
			    "Invalid content from server");
		}
		// Skip checking the xhr.status because the above will do for most errors
		// and because it causes Mozilla to error

		// This should get us out of 404s etc. 
		if (reply.search("DWREngine._handle") == -1) {
		    DWREngine._handleMetaDataError(null,
			    "Invalid reply from server");
		    return;
		}

		eval(reply);

		// We're done. Clear up
		DWREngine._clearUp(batch);
	    } catch (ex) {
		if (ex == null)
		    ex = "Unknown error occured";
		DWREngine._handleMetaDataError(null, ex);
	    } finally {
		// If there is anything on the queue waiting to go out, then send.
		// We don't need to check for ordered mode, here because when ordered mode
		// gets turned off, we still process *waiting* batches in an ordered way. 
		if (DWREngine._batchQueue.length != 0) {
		    var sendbatch = DWREngine._batchQueue.shift();
		    DWREngine._sendData(sendbatch);
		    DWREngine._batches[DWREngine._batches.length] = sendbatch;
		}
	    }
	}
    };
}
