/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

painters.displayLocationId_cc = function displayLocationId(obj) {
	return painters.builders.link({
		"href" : globals.BASE
				+ "/cyclecounting/location/view.action?locationId="
				+ obj.location.id
	}, obj.location.scannedVerification);
};

painters.displayItemNumber_cc = function displayItemNumber(obj) {
	return painters.builders.link({
		"href" : globals.BASE + "/cyclecounting/item/view.action?itemId="
				+ obj.item.id
	}, obj.item.number);
};

painters.displayOperator_cc = function displayOperator(obj) {
	return painters.builders.link({
		"href" : globals.BASE
				+ "/cyclecounting/operator/view.action?operatorId="
				+ obj.operator.id
	}, obj.operator.common.operatorIdentifier);
};

painters.displayRegionNumber_cc = function displayRegionNumber(obj) {
	return painters.builders.link({
		"href" : globals.BASE + "/cyclecounting/region/view.action?regionId="
				+ obj.region.id
	}, obj.region.number);
};

painters.displayAssignByRegion_cc = function(obj) {
	if (obj.totalAssignments > 0) {
		var regionIdFilter = getFilter(-1209, '-23011', obj.region.name,
				operandHelper.stringEqualsOperand);
		var createdDateFilter = getFilter(-1209, '-23016', cyclecountingAssignmentSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/cyclecounting/assignment/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&"
					+ createdDateFilter
		}, obj.totalAssignments);
	} else {
		return obj.totalAssignments;
	}
};

painters.displayAssignByRegionAndInProgress_cc = function(obj) {
	if (obj.inProgress > 0) {
		var regionIdFilter = getFilter(-1209, '-23011', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1209, '-23006', '3',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1209, '-23016', cyclecountingAssignmentSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/cyclecounting/assignment/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.inProgress);
	} else {
		return obj.inProgress;
	}
};

painters.displayAssignByRegionAndAvailable_cc = function(obj) {
	if (obj.available > 0) {
		var regionIdFilter = getFilter(-1209, '-23011', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1209, '-23006', '2',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1209, '-23016', cyclecountingAssignmentSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/cyclecounting/assignment/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.available);
	} else {
		return obj.available;
	}
};

painters.displayAssignByRegionAndComplete_cc = function(obj) {
	if (obj.complete > 0) {
		var regionIdFilter = getFilter(-1209, '-23011', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1209, '-23006', '4',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1209, '-23016', cyclecountingAssignmentSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/cyclecounting/assignment/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.complete);
	} else {
		return obj.complete;
	}

};

painters.displayAssignByRegionAndNotComplete_cc = function(obj) {
	if (obj.nonComplete > 0) {
		var regionIdFilter = getFilter(-1209, '-23011', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1209, '-23006', '4',
				operandHelper.enumNotEqualsOperand);
		var createdDateFilter = getFilter(-1209, '-23016', cyclecountingAssignmentSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/cyclecounting/assignment/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.nonComplete);
	} else {
		return obj.nonComplete;
	}
};

painters.displayOperatorsByRegionAndSignedOn_cc = function(obj) {
	if (obj.operatorsWorkingIn > 0) {
		var regionIdFilter = getFilter(-1005, '-10406', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1005, '-10403', '1',
				operandHelper.enumEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/cyclecounting/operator/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
		}, obj.operatorsWorkingIn);
	} else {
		return obj.operatorsWorkingIn;
	}
};

painters.displayOperatorsByRegion_cc = function(obj) {
	if (obj.operatorsAssigned > 0) {
		var regionIdFilter = getFilter(-1005, '-10408', obj.region.name,
				operandHelper.stringEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/cyclecountingR/operator/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter
		}, obj.operatorsAssigned);
	} else {
		return obj.operatorsAssigned;
	}
};


