/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

painters.displayTotal_ll = function(obj) {
	if (obj.totalCartons > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
//		var routeFilter = getFilter(-1101, '-13506', obj.routeStop.route,
//				operandHelper.stringEqualsOperand);
//		var stopFilter = getFilter(-1101, '-13507', obj.routeStop.stop,
//				operandHelper.stringEqualsOperand);
		// and status=1 or status=2
//		var statusFilter = getFilter(-1101, '-13509', '1',
//				operandHelper.enumEqualsOperand)
//				+ "&"
//				+ getFilter(-1101, '-13509', '2',
//						operandHelper.enumEqualsOperand);
		var dateFilter = getFilter(-1101, '-13501', lineloadingWaveSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/lineloading/carton/list.action?summarySiteID="
					+ obj.site.id + "&" + waveFilter + "&" 
//					+ routeFilter + "&" + stopFilter + "&" + statusFilter + "&" 
					+ dateFilter
		}, obj.totalCartons);
	} else {
		return obj.totalCartons;
	}
};

painters.displayAvailable_ll = function(obj) {
	if (obj.available > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
//		var routeFilter = getFilter(-1101, '-13506', obj.routeStop.route,
//				operandHelper.stringEqualsOperand);
//		var stopFilter = getFilter(-1101, '-13507', obj.routeStop.stop,
//				operandHelper.stringEqualsOperand);
		// and status=1 or status=2
		var statusFilter = getFilter(-1101, '-13509', '1',
				operandHelper.enumEqualsOperand)
				+ "&"
				+ getFilter(-1101, '-13509', '2',
						operandHelper.enumEqualsOperand);
		var dateFilter = getFilter(-1101, '-13501', lineloadingWaveSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/lineloading/carton/list.action?summarySiteID="
					+ obj.site.id + "&" + waveFilter + "&" 
//					+ routeFilter + "&" + stopFilter + "&" 
					+ statusFilter + "&" + dateFilter
		}, obj.available);
	} else {
		return obj.available;
	}
};

painters.displayLoaded_ll = function(obj) {
	if (obj.loaded > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
//		var routeFilter = getFilter(-1101, '-13506', obj.routeStop.route,
//				operandHelper.stringEqualsOperand);
//		var stopFilter = getFilter(-1101, '-13507', obj.routeStop.stop,
//				operandHelper.stringEqualsOperand);
		// and status=3
		var statusFilter = getFilter(-1101, '-13509', '3',
				operandHelper.enumEqualsOperand);
		var dateFilter = getFilter(-1101, '-13501', lineloadingWaveSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/lineloading/carton/list.action?summarySiteID="
					+ obj.site.id + "&" + waveFilter + "&" 
//					+ routeFilter + "&" + stopFilter + "&"
					+ statusFilter + "&" + dateFilter
		}, obj.loaded);
	} else {
		return obj.loaded;
	}
};

painters.displayPicked_ll = function(obj) {
	if (obj.picked > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
//		var routeFilter = getFilter(-1101, '-13506', obj.routeStop.route,
//				operandHelper.stringEqualsOperand);
//		var stopFilter = getFilter(-1101, '-13507', obj.routeStop.stop,
//				operandHelper.stringEqualsOperand);
//		// and pickStatus=1
		var statusFilter = getFilter(-1101, '-13508', '2',
				operandHelper.enumEqualsOperand);
		var dateFilter = getFilter(-1101, '-13501', lineloadingWaveSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/lineloading/carton/list.action?summarySiteID="
					+ obj.site.id + "&" + waveFilter + "&"
//					+ routeFilter + "&" + stopFilter "&" 
					+ statusFilter + "&" + dateFilter
		}, obj.picked);
	} else {
		return obj.picked;
	}
};

painters.displayMissing_ll = function(obj) {
	if (obj.missing > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
//		var routeFilter = getFilter(-1101, '-13506', obj.routeStop.route,
//				operandHelper.stringEqualsOperand);
//		var stopFilter = getFilter(-1101, '-13507', obj.routeStop.stop,
//				operandHelper.stringEqualsOperand);
//		// and pickStatus=1 and status<>3
		var statusFilter = getFilter(-1101, '-13508', '1',
				operandHelper.enumEqualsOperand)
				+ "&"
				+ getFilter(-1101, '-13509', '3',
						operandHelper.enumNotEqualsOperand);
		var dateFilter = getFilter(-1101, '-13501', lineloadingWaveSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/lineloading/carton/list.action?summarySiteID="
					+ obj.site.id + "&" + waveFilter + "&" 
//					+ routeFilter + "&" + stopFilter + "&" 
					+ statusFilter + "&" + dateFilter
		}, obj.missing);
	} else {
		return obj.missing;
	}
};

painters.displaySetAsideShorted_ll = function(obj) {
	if (obj.setAsideOrShorted > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
//		var routeFilter = getFilter(-1101, '-13506', obj.routeStop.route,
//				operandHelper.stringEqualsOperand);
//		var stopFilter = getFilter(-1101, '-13507', obj.routeStop.stop,
//				operandHelper.stringEqualsOperand);
//		// and status=4 or status=5
		var statusFilter = getFilter(-1101, '-13509', '4',
				operandHelper.enumEqualsOperand)
				+ "&"
				+ getFilter(-1101, '-13509', '5',
						operandHelper.enumEqualsOperand);
		var dateFilter = getFilter(-1101, '-13501', lineloadingWaveSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/lineloading/carton/list.action?summarySiteID="
					+ obj.site.id + "&" + waveFilter + "&" 
//					+ routeFilter + "&" + stopFilter + "&" 
					+ statusFilter + "&" + dateFilter
		}, obj.setAsideOrShorted);
	} else {
		return obj.setAsideOrShorted;
	}
};

painters.displayPickShorted_ll = function(obj) {
	if (obj.pickShorted > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
//		var routeFilter = getFilter(-1101, '-13506', obj.routeStop.route,
//				operandHelper.stringEqualsOperand);
//		var stopFilter = getFilter(-1101, '-13507', obj.routeStop.stop,
//				operandHelper.stringEqualsOperand);
//		// and pickStatus=3
		var statusFilter = getFilter(-1101, '-13508', '3',
				operandHelper.enumEqualsOperand);
		var dateFilter = getFilter(-1101, '-13501', lineloadingWaveSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/lineloading/carton/list.action?summarySiteID="
					+ obj.site.id + "&" + waveFilter + "&" 
//					+ routeFilter + "&" + stopFilter + "&" 
					+ statusFilter + "&" + dateFilter
		}, obj.pickShorted);
	} else {
		return obj.pickShorted;
	}
};

painters.displayRegionLink_ll = function(obj) {
	return painters.builders.link({
		'regionId' : obj.routeStop.region.name,
		'href' : globals.PAGE_CONTEXT + '/region/view.action?regionId='
				+ obj.routeStop.region.id
	}, obj.routeStop.region.name);
};

painters.displayDetailsCount_ll = function(obj) {
	if (obj.detailCount > 0) {
		var cartonDetailFilter = getFilter('-1102', '-13601', obj.number, '-4');
		return painters.builders.link({
			'id' : obj.detailCount,
			'href' : globals.BASE
					+ '/lineloading/carton/cartonDetail/list.action?'
					+ cartonDetailFilter
		}, obj.detailCount);
	} else {
		return obj.detailCount;
	}
};

painters.displayAllowToCloseStop_ll = function(obj) {
	if (obj.allowCloseStopFromTask == true) {
		return itext('lineloadregion.allowCloseStopFromTask.true');
	} else {
		return itext('lineloadregion.allowCloseStopFromTask.false');
	}
};

painters.displayAutomaticallyPrintManifest_ll = function(obj) {
	if (obj.autoPrintManifestReport == true) {
		return itext('lineloadregion.autoPrintManifestReport.true');
	} else {
		return itext('lineloadregion.autoPrintManifestReport.false');
	}
};

painters.displayLineLoadingRegion_ll = function(obj) {
	return painters.builders.link({
		'href' : globals.BASE + '/lineloading/region/view.action?regionId='
				+ obj.id
	}, obj.name);
};

painters.displayTotalCartons_ll = function(obj) {
	if (obj.totalCartons > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
		var routeFilter = getFilter(-1101, '-13506', obj.route,
				operandHelper.stringEqualsOperand);
		var stopFilter = getFilter(-1101, '-13507', obj.stop,
				operandHelper.stringEqualsOperand);
		// var dateFilter = getFilter(-1101, '-13500', obj.dateOpened,
		// stringEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE + "/lineloading/carton/list.action?"
					+ waveFilter + "&" + routeFilter + "&" + stopFilter
		}, obj.totalCartons);
	} else {
		return obj.totalCartons;
	}
};

painters.displayAvailableCartons_ll = function(obj) {
	if (obj.availableCartons > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
		var routeFilter = getFilter(-1101, '-13506', obj.route,
				operandHelper.stringEqualsOperand);
		var stopFilter = getFilter(-1101, '-13507', obj.stop,
				operandHelper.stringEqualsOperand);
		// and status=1 or status=2
		var statusFilter = getFilter(-1101, '-13509', '1',
				operandHelper.enumEqualsOperand)
				+ "&"
				+ getFilter(-1101, '-13509', '2',
						operandHelper.enumEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE + "/lineloading/carton/list.action?"
					+ waveFilter + "&" + routeFilter + "&" + stopFilter + "&"
					+ statusFilter
		}, obj.availableCartons);
	} else {
		return obj.availableCartons;
	}
};

painters.displayLoadedCartons_ll = function(obj) {
	if (obj.loadedCartons > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
		var routeFilter = getFilter(-1101, '-13506', obj.route,
				operandHelper.stringEqualsOperand);
		var stopFilter = getFilter(-1101, '-13507', obj.stop,
				operandHelper.stringEqualsOperand);
		// and status=3
		var statusFilter = getFilter(-1101, '-13509', '3',
				operandHelper.enumEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE + "/lineloading/carton/list.action?"
					+ waveFilter + "&" + routeFilter + "&" + stopFilter + "&"
					+ statusFilter
		}, obj.loadedCartons);
	} else {
		return obj.loadedCartons;
	}
};

painters.displayPickedCartons_ll = function(obj) {
	if (obj.pickedCartons > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
		var routeFilter = getFilter(-1101, '-13506', obj.route,
				operandHelper.stringEqualsOperand);
		var stopFilter = getFilter(-1101, '-13507', obj.stop,
				operandHelper.stringEqualsOperand);
		// and pickStatus=1
		var statusFilter = getFilter(-1101, '-13508', '2',
				operandHelper.enumEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE + "/lineloading/carton/list.action?"
					+ waveFilter + "&" + routeFilter + "&" + stopFilter + "&"
					+ statusFilter
		}, obj.pickedCartons);
	} else {
		return obj.pickedCartons;
	}
};

painters.displayMissingCartons_ll = function(obj) {
	if (obj.missingCartons > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
		var routeFilter = getFilter(-1101, '-13506', obj.route,
				operandHelper.stringEqualsOperand);
		var stopFilter = getFilter(-1101, '-13507', obj.stop,
				operandHelper.stringEqualsOperand);
		// and pickStatus=1 and status<>3
		var statusFilter = getFilter(-1101, '-13508', '1',
				operandHelper.enumEqualsOperand)
				+ "&"
				+ getFilter(-1101, '-13509', '3',
						operandHelper.enumNotEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE + "/lineloading/carton/list.action?"
					+ waveFilter + "&" + routeFilter + "&" + stopFilter + "&"
					+ statusFilter
		}, obj.missingCartons);
	} else {
		return obj.missingCartons;
	}
};

painters.displaySetAsideOrShortedCartons_ll = function(obj) {
	if (obj.setAsideOrShortedCartons > 0) {
		var waveFilter = getFilter(-1101, '-13505', obj.wave,
				operandHelper.stringEqualsOperand);
		var routeFilter = getFilter(-1101, '-13506', obj.route,
				operandHelper.stringEqualsOperand);
		var stopFilter = getFilter(-1101, '-13507', obj.stop,
				operandHelper.stringEqualsOperand);
		// and status=4 or status=5
		var statusFilter = getFilter(-1101, '-13509', '4',
				operandHelper.enumEqualsOperand)
				+ "&"
				+ getFilter(-1101, '-13509', '5',
						operandHelper.enumEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/lineloading/carton/list.action?waveFilter&"
					+ routeFilter + "&" + stopFilter + "&" + statusFilter
		}, obj.setAsideOrShortedCartons);
	} else {
		return obj.setAsideOrShortedCartons;
	}
};
