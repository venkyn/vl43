/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Table Component 2.0
 */

/** @constructor */
TableComponent = function(_init_args) {

    // public values for the table
    this.REFRESH_RATE = 5000; // How fast the table is refreshing
    this.ROW_COUNT = 5000; // How many rows of data to get per page
    this.PAGE_SIZE = 24; // How many rows are seen in the UI
    this.MAX_HEIGHT = 800; // How tall can the table be stretched
    this.MIN_HEIGHT = 210; // How small can the table shrink
    this.FIRST_ROW_ID = 0; // This should alway be 0
    this.FIRST_TIME_RUN = "true"; // Is the the first time the table is
    // rendering
    this.REFRESH_REQUEST = "true"; // Enable the refresh requests

    // Used to set post-format data for configurable home pages.
    this.post_format = function() {
	return;
    };

    // Set variables based on initial table arguments
    this.id = _init_args["id"];
    this.columns = _init_args["columns"]["all"];
    this.viewId = _init_args["viewId"];
    this.url = _init_args["url"];
    this.name = _init_args["name"];
    this.type = _init_args["type"];
    this.title = _init_args["title"];
    this.summaryLoc = _init_args["summaryLoc"];
    this.initial_filters = _init_args["filters"];
    this.publishers = _init_args["publishers"];
    this.extra_url_params = _init_args["extraURLParams"];
    this.rowsToHighlight = _init_args["rowsToHighlight"];
    this.time_filter = _init_args["timeFilter"];
    this.autoCompleteUrl = _init_args["autoCompleteUrl"];
    this.divName = this.name + "Div";
    
    // Additional variables
    this.grid;
    this.loader;
    this.params;
    this.no_data;
    this.dataRowCount;
    this.visible_columns = this.getVisibleColumns();

    // List of events that this table component calls
    this.events = {
	REFRESH : "refresh",
	SAVE_PREFERENCES : "save_preferences",
	FILTER_LINK : "filter_link",
	FILTER_UPDATE : "filter_update",
	SCROLL_VIEWPORT : "scroll_viewport"
    };
};

TableComponent.prototype = {

    /**
     * Initialize and render table component. Args: callback - called after
     * table is initialized
     */
    init : function(callback) {

	// This appends '<table_id>-' to the beginning.
	var UIConstants = function(_id) {
	    var table_id = _id;
	    var get = function(name) {
		return table_id + '-' + this[name];
	    };
	    return {
		"get" : get
	    };
	};
	this.ui_constants = new UIConstants(this.id);
	this.initialize_constants();

	// Push all of the elements to the end and return the length for book
	// marking
	pageTableComponents.push(this);

	// Set up the modules and pass their localized strings
	this.filter_module = new FilterModule(this, this.initial_filters);
	this.columns_module = new ColumnsModule(this);
	this.time_window_module = new TimeWindowModule(this);
	this.printable_view_module = new PrintableViewModule(this);
	this.copy_selection_module = new CopySelectionModule(this);
	// Pass each module its international text (itext)
	this.filter_module.setLocalizedStrings({
	    noCriteriaSet : itext('filter.no.criteria'),
	    addFilterCrit : itext('filter.add.criteria'),
	    remove : itext('filter.remove.criteria'),
	    saveChanges : itext('filter.save.changes'),
	    filteredBy : itext('filter.filtered.by'),
	    clearCriteria : itext('filter.clear.all'),
	    selectAColumn : itext('filter.select.a.column'),
	    selectAnOperand : itext('filter.select.an.operand'),
	    bookmarkPage : itext('filter.bookmark.page'),
	    manageFilter : itext('filter.manage.filter'),
	    filterTableTitle : itext('filter.validate.required'),
	    filterRequiredString : itext('filter.table.title'),
	    cancel : itext('autocomplete.cancel'),
	    selectAValue : itext('filter.select.a.value'),
	    addToFilter : itext('filter.add.to.filter'),
	    removeFromFilter : itext('filter.remove.criteria'),
	    trueValue : itext('true'),
	    falseValue : itext('false'),
	    close : itext('addremove.close')
	});
	this.columns_module.setLocalizedStrings({
	    addRemoveColumns : itext('addremove.addremovecolumns'),
	    restore : itext('addremove.restore'),
	    close : itext('addremove.close'),
	    restorePromptFirst : itext('addremove.restore.prompt.first'),
	    restorePromptSecond : itext('addremove.restore.prompt.second'),
	    yes : itext('addremove.restore.prompt.yes'),
	    no : itext('addremove.restore.prompt.no'),
	    ensureOneColumn : itext('addremove.close.ensureOneColumn'),
	    okText : itext('addremove.close.prompt.ok')
	});
	this.time_window_module.setLocalizedStrings({
	    _8hrs : itext('table.dropdown.8hours'),
	    _10hrs : itext('table.dropdown.10hours'),
	    _24hrs : itext('table.dropdown.24hours'),
	    _48hrs : itext('table.dropdown.48hours'),
	    _72hrs : itext('table.dropdown.72hours'),
	    _168hrs : itext('table.dropdown.168hours')
	});
	this.printable_view_module.setLocalizedStrings({
	    printable_error_title : itext('printable.error.title'),
	    printable_error_message : itext('printable.error.message')
	});
	this.copy_selection_module.setLocalizedStrings({
	    copy_warning : itext('copy.warning'),
	    copy_add_instructions : itext('copy.addInstructions'),
	    copy_ok_button : itext('copy.ok.button'),
	    copy_browser_not_supported : itext('copy.browserNotSupported')
	});

	// Set the Slick.Grid options
	this.grid_options = {
	    rowHeight : 20,
	    defaultColumnWidth : 150
	};

	// Additional parameters for the table values are above
	this.params = {
	    rowCount : this.ROW_COUNT,
	    viewId : this.viewId,
	    firstTimeRun : this.FIRST_TIME_RUN,
	    rowsPerPage : this.PAGE_SIZE,
	    refreshRequest : this.REFRESH_REQUEST,
	    firstRowId : this.FIRST_ROW_ID
	};
	var initialSortInfo = this.determineInitialSort();
	this.params['sortColumn'] = initialSortInfo['sortColumn'];
	this.params['sortAsc'] = initialSortInfo['sortAsc'];

	// If we have extra queries lets add them to the params
	for ( var i = 0, j = this.extra_url_params.length; i < j; i++) {
	    this.params[extraURLParams[i].name] = extraURLParams[i].value;
	}
	this.setup_loader();
	this.setup_grid();
	this.grid.onViewportChanged();
	var self = this;
	// When Page Ready and all tables have loaded bind the events
	$j(document).ready(
		function() {
		    $j(document).bind(
			    events.ALL_TABLES_LOADED,
			    function() {
				$j(document).bind(
					events.SELECTION_CHANGE,
					function(event, tableObj) {
					    self.handlePublisher(event,
						    tableObj);
					    self.displayRowCount();
					});
				
					var am = getActionsMenu();
					
			        if (am == null) {
			        	am = new ActionsMenu();
			            am.updateActionsNoTable();
			        } else {
			        	am.updateActions(self);
			        }

			    });
		});
	$j(this).bind(this.events.REFRESH, this.refresh);
	$j(this).bind(this.events.SAVE_PREFERENCES,
		this.loader.saveUserPreferences);
	$j(this).bind(this.events.FILTER_LINK, this.clickFilterLink);
	$j(this).bind(this.events.FILTER_UPDATE, this.updateFilterCriteria);

	this.refreshId = setInterval(function() {
	    $j(self).trigger(self.events.REFRESH);
	}, this.REFRESH_RATE);
	this.post_format();
	callback();
    },
   // a table has changed selection
    handlePublisher : function(event, tableObj) {

	// debug.log(tableObj.name + ' has changed selection');

	// check to see if it's in the publisher list
	var subscribed = false;
	for (idx in this.publishers) {
	    subscribed |= (tableObj.name == this.publishers[idx].name);

	    // The selection occurred in a table we care about.
	    if (subscribed) {
		// debug.log(this.name + " is subscribed to this publisher.");

		// Pass it in with selected IDs
		if (this.publishers[idx].selector != null) {
		    // Grab the selected ids from the table that fired the
		    // event.
		    var selected_ids = tableObj.getSelectedIds();

		    // Refresh, and pass in the selected ids
		    var params = {};
		    params[this.publishers[idx].selector] = selected_ids;
		    this.refresh(params);

		    // Otherwise, if there's a selector function, let's call it.
		} else if (this.publishers[idx].selectorFunction != null) {
		    this.refresh(this.publishers[idx]
			    .selectorFunction(tableObj));
		}
	    }
	}
    },
    // Get the actual obj.id from the selected id and return it
    getSelectedIds : function() {
	var selectedIds = [];
	var rows = this.grid.getSelectedRows();
	for ( var i = 0, l = rows.length; i < l; i++) {
	    var item = this.loader.data[rows[i]];
	    if (item)
		selectedIds.push(item.id);
	}
	return selectedIds;
    },
    // Get the selected row objects
    getSelectedObjects : function() {
	var formattedObjects = [];
	var rows = this.grid.getSelectedRows();

	for ( var i = 0, l = rows.length; i < l; i++) {
	    var item = this.loader.data[rows[i]];
	    if (item)
		formattedObjects.push(item);
	}
	return formattedObjects;
    },
    // Convenience function to clear a grid of all selected rows
    clearSelectedRows : function() {
    	this.grid.setSelectedRows([]);
		$j(document).trigger(events.SELECTION_CHANGE, this);
    },
    // Determine if a column is sorted and its direction
    determineInitialSort : function() {
	for ( var i = 0; i < this.columns.length; i++) {
	    if (this.columns[i].sorted) {
		return {
		    sortColumn : this.columns[i].field,
		    sortAsc : this.columns[i].sortDirection,
		    id : this.columns[i].id
		};
	    }
	}
	return {};
    },
    // Setup the Remote Model to go retrieve data
    setup_loader : function() {
	this.loader = new Slick.Data.RemoteModel();
	this.loader.setupPrefs(this.url, this.visible_columns, this.params,
	this.print_url);
	this.loader.setFilterCriteria(this.filter_module.getFilterCriterion());
    },
    setup_grid : function() {

	// Draw a containing div if it's not a summary page
	var div_class = "table_component_summary";
	var self = this;
	// If not the summary table make the DIV table_component
	if (!this.type.match('summary')) {
	    div_class = "table_component";
	    $j('#' + this.id + 'Div').addClass('regular');
	}

	// Close the DIV tag
	$j('#' + this.id + 'Div').append($j('<div />', {
	    'id' : this.ui_constants.get('TableDiv')
	}));

	// Attach the CSS for a table
	$j('#' + this.ui_constants.get('TableDiv')).addClass('tableDiv');

	// Table Title DIV
	$j('#' + this.ui_constants.get('TableDiv')).append(
		'<div id="' + this.ui_constants.get('TitleBarDiv')
			+ '" class="titleBar">' + this.title + '</div>');

	// Time Window DIV
	if (this.type == 'summary_time' || this.type == "table_time") {
	    $j('#' + this.ui_constants.get('TableDiv')).append(
		    this.time_window_module.buildTitleDropdown());
	}

	// Table links DIV
	$j('#' + this.ui_constants.get('TableDiv'))
		.append(
			'<div class="internalMenuBar">'
				+ '<a class="intMenuLink" href="javascript:'
				+ this.name
				+ '.filter_module.toggleMenu();" id="manageFilterDivLink">'
				+ itext('filter.manage.filter')
				+ '</a>'
				+ '<a class="intMenuLink" href="javascript:'
				+ this.name
				+ '.columns_module.toggleMenu();" id="addRemoveColumnsDivLink">'
				+ itext('addremove.addremovecolumns')
				+ '</a>'
				+ '<a class="intMenuLink" href="javascript:'
				+ this.name
				+ '.copy_selection_module.copyRowSelection();" id="copySelectionLink"">'
				+ itext('copy.linkText')
				+ '</a>'
				+ '<a class="intMenuLink" href="javascript:'
				+ this.name
				+ '.printable_view_module.showPrintWindow();" id="printableViewLink">'
				+ itext('print.table') + '</a>' + '</div>');

	// Render links container DIV
	$j('#' + this.ui_constants.get('TableDiv')).append(
		'<div id="' + this.ui_constants.get('menuContainer')
			+ '" class="menuContainer" />');

	// Render the slick grid DIV
	$j('#' + this.ui_constants.get('TableDiv')).append(
		'<div id="' + this.divName + '"  class="' + div_class
			+ '"></div>');

	// Footer DIV
	$j('#' + this.ui_constants.get('TableDiv')).append(
		'<div id="' + this.ui_constants.get('RowCountDiv')
			+ '" class="rowCount regular rowCountContent"></div>');

	this.determineTableResize();
	// Resizes the table component up and down (min is 4 rows max is
	// 35 rows) This only provides a handle on the s edge of the TC.
	if (!this.type.match("summary")) {
	    var resized = false;
		$j('#' + this.id + 'Div').resizable(
		    {
			minHeight : this.MIN_HEIGHT,
			maxHeight : this.MAX_HEIGHT,
			'handles' : 's',
			'alsoResize' : $j('#' + this.id
				+ 'Div .table_component, #' + this.id
				+ 'Div .table_component_summary'),
			'resize' : function(e, ui){
			    	resized = true;
			    },
			'stop' : function(e, ui) {
			    if( resized == true) {
			    		var cookieName = Math.abs(self.viewId) + globals.USER_NAME;
			    		var userTablesizePref = {};
			    		userTablesizePref['height'] = ui.size.height;
			    		self.grid.resizeCanvas();
			    		$j.cookies.set(cookieName, userTablesizePref);
			    } 
			    
			 }
		    
		    });
	    // TODO save the south edge position to user preferences
	}
	// Create the grid
	this.grid = new Slick.Grid("#" + this.divName, this.loader.data,
		this.visible_columns, this.grid_options);

	// Bind the double click resize event
	this.handleDblClickResize();

	// Bind the keydown event
	$j('#' + this.divName).keydown(this.grid.handleKeyDown);

	// Render No Records To Display DIV in case we have no data
	$j('#' + this.ui_constants.get('TableDiv') + ' .slick-viewport')
		.append($j('<div />', {
		    'id' : this.ui_constants.get('InitialLoad'),
		    'class' : 'emptyDiv'
		}).text(itext('tablecomponent.norows')));

	this.filter_module.init();
	this.columns_module.init();
	if (this.time_filter) {
	    this.time_window_module.init(this.time_filter);
	}
	this.printable_view_module.init();
	this.copy_selection_module.init();

	// Setup initial sort header
	this.grid.setSortColumn(this.determineInitialSort()['id'],
		this.params['sortAsc']);

	// Reload Data on scroll out of range
	this.grid.onViewportChanged = function(e, args) {
	    var vp = self.grid.getViewport();
	    self.loader.ensureData(vp.top, vp.bottom, false);
	};
	// Sort data and save it to the user preference
	this.grid.onSort = function(sortCol, sortAsc) {
	    self.loader.setSort(sortCol.field, sortAsc ? 1 : -1);
	    var vp = self.grid.getViewport();
	    $j(self).trigger(self.events.SAVE_PREFERENCES);
	    self.loader.ensureData(vp.top, vp.bottom, false);
	};
	// Reorder the columns and save to user preferences
	this.grid.onColumnsReordered = function() {
	    self.loader.setColumns(self.grid.getColumns());
	    self.visible_columns = self.grid.getColumns();
	    $j(self).trigger(self.events.SAVE_PREFERENCES);
	};
	// Resize columns and save to user preferences
	this.grid.onColumnsResized = function() {
	    self.loader.setColumns(self.grid.getColumns());
	    self.visible_columns = self.grid.getColumns();
	    $j(self).trigger(self.events.SAVE_PREFERENCES);
	};
	// On row selection, alert the document with an event
	this.grid.onSelectedRowsChanged = function(e) {
	    // TODO: persist the row selection to the user preferences
	    $j(document).trigger(events.SELECTION_CHANGE, self);
	};

	this.grid.onScroll = function(e, args) {
	    $j(self).trigger(self.events.SCROLL_VIEWPORT);
	};
	// Show the loading icon in rare cases when we have to
	this.loader.onDataLoading.subscribe(function() {
	    if (!self.loadingIndicator) {
		self.loadingIndicator = $j(
			"<span class='loading-indicator'><label>"
				+ itext("tablecomponent.loading")
				+ "</label></span>").appendTo(
			$j('#' + self.ui_constants.get('TableDiv')));
		var $g = $j("#" + self.divName);

		self.loadingIndicator.css("position", "absolute").css(
			"top",
			$g.position().top + $g.height() / 2
				- self.loadingIndicator.height() / 2).css(
			"left",
			$g.position().left + $g.width() / 2
				- self.loadingIndicator.width() / 2);
	    }
	    // Show the loading indicator
	    self.loadingIndicator.show();
	});
	// Events to trigger when data is loaded
	this.loader.onDataLoaded.subscribe(function(e, args) {
	    self.dataRowCount = args.count;
	    for ( var i = args.from; i <= args.to; i++) {
		self.grid.removeRow(i);
	    }
	    self.displayRowCount();
	    self.grid.updateRowCount();
	    self.grid.render();
	    self.hightlightRow();
	    if (args.count == undefined || args.count == 0) {
		$j('#' + self.ui_constants.get('InitialLoad')).show();
		self.no_data = true;
	    } else {
		if (self.no_data) {
		    $j('#' + self.ui_constants.get('InitialLoad')).hide();
		    self.no_data = false;
		}
	    }
	    $j('#timeStampValue').text(self.loader.getCurrentTimestamp());
	    self.loadingIndicator.fadeOut();
	});
    },
    determineTableResize : function() {
    	var cookieName = Math.abs(this.viewId) + globals.USER_NAME;
    	var cookieVal = $j.cookies.get(cookieName);
    	if(cookieVal){
    		// setting the new height of the table component and its container
    		$j('#' + this.ui_constants.get('TableDiv')).height(cookieVal.height);
    		$j('#' + this.id + 'Div').height(cookieVal.height + 14);
    		$j('#' + this.divName).height(cookieVal.height - 60);
    	}
    },
    // Bind the double click resizable handle event
    handleDblClickResize : function() {
	$j('#' + this.ui_constants.get('TableDiv') + ' .slick-resizable-handle')
		.live(
			"dblclick",
			{
			    self : this
			},
			dblBind = function(e) {
			    var col = $j(e.target).parents(
				    ".slick-header-column");
			    var col_index = col.index();
			    var cell_padding = 12; // Normally 12
			    var colElements = $j('#'
				    + e.data.self.ui_constants.get('TableDiv')
				    + ' c' + col_index);
			    var max = col[0];
			    for ( var i = 0; i < colElements.length; i++) {
				if (colElements[i].textContent.length > max.textContent.length) {
				    max = colElements[i];
				}
			    }
			    $j(max).css({
				"overflow" : "visible",
				"width" : "auto"
			    });
			    var width = $j(max).width();

			    if (width > .75 * $j(e.target).parents(".tableDiv")
				    .width()) {
				width = .75 * $j(e.target).parents(".tableDiv")
					.width();
			    }
			    $j(
				    '#'
					    + e.data.self.ui_constants
						    .get('TableDiv') + ' c'
					    + col_index).css({
				'width' : width + cell_padding
			    });
			    $j(max).css({
				"overflow" : "hidden",
				"width" : width + cell_padding
			    });
			    for ( var i = 0; i < e.data.self.visible_columns.length; i++) {
				if (e.data.self.visible_columns[i].name == col
					.text()) {
				    e.data.self.visible_columns[i].width = width
					    + cell_padding;
				    e.data.self.visible_columns[i].currentWidth = width
					    + cell_padding;
				    break;
				}
			    }
			    e.data.self.update_columns();
			    e.stopImmediatePropagation();
			});
    },
    saveSelectionToSession : function() {
	if (_selectionSaveURL != null) {
	    var params = "";
	    for ( var i = 0; i < _selectedRows.length; i++) {
		params += "&ids=" + _selectedRows[i];
	    }
	    params += "&viewId=" + _viewId;
	    // makeAsynchronousPostCall(params,_selectionSaveURL,handleFilterAction);
	    req = getXMLHttpRequest();
	    req.open("POST", url);
	    req.setRequestHeader('Content-type',
		    'application/x-www-form-urlencoded');
	    d_POST = sendXMLHttpRequest(req, params);
	    d_POST.addCallback(handler);
	    return d_POST;
	}

    },
    // Display row count in the footer
    displayRowCount : function() {
	var count = this.loader.data.length;
	var selectedRows = this.getSelectedIds().length;
	if (selectedRows > 0) {
	    if (count > 1) {
		$j("#" + this.ui_constants.get('RowCountDiv')).html(
			'<span>'
				+ itext('rowcount.rowsSelected', [
					selectedRows, count ]) + '</span>');
	    } else {
		$j("#" + this.ui_constants.get('RowCountDiv')).html(
			itext('rowcount.rowSelected', [ selectedRows, count ])
				+ '</span>');
	    }
	} else {
	    if (count == 1) {
		$j("#" + this.ui_constants.get('RowCountDiv')).html(
			itext('rowcount.rowCountOnly', [ count ]) + '</span>');
	    } else {
		$j("#" + this.ui_constants.get('RowCountDiv')).html(
			itext('rowcount.rowsCountOnly', [ count ]) + '</span>');
	    }
	}
    },
    // Click the filter link
    clickFilterLink : function() {
	this.filter_module;
    },
    hightlightRow : function() {
    	for ( var i = 0; i < this.rowsToHighlight.length; i++) {
    		for (var vari = 0; vari< this.loader.data.length; vari++) {
    			r = this.loader.data[vari];
    			if(r[this.rowsToHighlight[i].field] == itext(this.rowsToHighlight[i].value) && 
    					r[this.rowsToHighlight[i].field1] == this.rowsToHighlight[i].value1) {
    				$j('.slick-row[row=' + vari + ']').addClass('unread');
    			}
    		}
    	}
    },
    // Return column by ID
    getColumnById : function(id) {
	for ( var i = 0; i < this.columns.length; i++) {
	    if (this.columns[i].id == id) {
		return this.columns[i];
	    }
	}
    },
    // Return 0 if selected column is 'admin'
    isAdministrative : function() {
	var rows = this.grid.getSelectedRows();
	for ( var i = 0, l = rows.length; i < l; i++) {
	    var item = this.loader.data[rows[i]];
	    if (item.isAdministrative) {
		return 0;
	    } else {
		return 1;
	    }
	}
    },
    // Trigger refresh of the grid
    refresh : function(parentRowSelection) {
	var vp = this.grid.getViewport();
	this.loader.ensureData(vp.top, vp.bottom, true, parentRowSelection);
    },
    // Update the filter criteria and trigger a refresh
    updateFilterCriteria : function() {
	this.loader.setFilterCriteria(this.filter_module.getFilterCriterion());
	$j(this).trigger(this.events.REFRESH);
    },
    
    // Returns the url for the auto completion of filter values
    getAutoCompleteUrl : function() {
    	return this.autoCompleteUrl;
    },
    
    getColumnFieldNameByid : function(id){
    	for ( var i = 0; i < this.columns.length; i++) {
    	    if (this.columns[i].id == id) {
    		return this.columns[i].field;
    	    }
    	}
    },
    // Get the visible columns
    getVisibleColumns : function() {
	var visibleColumns = [];

	for ( var i = 0; i < this.columns.length; i++) {
	    if (this.columns[i].visible) {
		visibleColumns.push(this.columns[i]);
	    }
	}
	return visibleColumns;
    },
    // Update the visible columns and save to user preferences
    update_columns : function() {
	this.grid.setColumns(this.visible_columns);
	this.loader.setColumns(this.visible_columns);
	this.handleDblClickResize();
	$j(this).trigger(this.events.SAVE_PREFERENCES);
    },
    // Add a column to the grid and call update columns
    add_column : function(col, idx) {
	this.visible_columns.splice(idx, 0, col);

	for ( var i = 0; i < this.columns.length; i++) {
	    if (this.columns[i].name === col.name) {
		this.columns[i].visible = true;
		if (this.columns[i].width === 0) {
		    this.columns[i].width = this.columns[i].defaultWidth;
		}
	    }
	}
	this.update_columns();
    },
    // Remove a column and call update columns
    remove_column : function(name) {
	var idx = null;
	for ( var i = 0; i < this.columns.length; i++) {
	    if (this.columns[i].name === name) {
		this.columns[i].visible = false;
		break;
	    }
	}
	for ( var i = 0; i < this.visible_columns.length; i++) {
	    if (this.visible_columns[i].name === name) {
		idx = i;
		break;
	    }
	}
	this.visible_columns.splice(idx, 1);
	this.grid.setColumns(this.visible_columns);
	this.loader.setColumns(this.columns);
	$j(this).trigger(this.events.SAVE_PREFERENCES);
    },
    // Convenience function of setting the height of a table component
    setHeight : function(height, bool) {
	$j('#' + this.ui_constants.get('TableDiv')).height(height);
	$j('#' + this.id + 'Div').height(height);
	$j('#' + this.divName).height(height - 60);
	this.grid.resizeCanvas();
	},
    // Initialize the UI constants
    initialize_constants : function() {
	this.ui_constants['TableDiv'] = 'tableDiv';
	this.ui_constants['HeaderDiv'] = 'headerDiv';
	this.ui_constants['TableWrapDiv'] = 'tableWrapDiv';
	this.ui_constants['TitleBarDiv'] = 'titleBarDiv';
	this.ui_constants['RowCountDiv'] = 'rowCountDiv';
	this.ui_constants['RowCountContentDiv'] = 'rowCountContentDiv';
	this.ui_constants['TitleContentDiv'] = 'titleContentDiv';
	this.ui_constants['ManageFilterContent'] = 'manageFilterContent';
	this.ui_constants['AddRemoveColumnsLink'] = 'addRemoveColumnsLink';
	this.ui_constants['ManageFilterLink'] = 'manageFilterLink';
	this.ui_constants['ManageFilterDivClose'] = 'manageFilterDivClose';
	this.ui_constants['FilterColumn'] = 'filterColumn';
	this.ui_constants['FilterOperand'] = 'filterOperand';
	this.ui_constants['FilterCriteria'] = 'filterCriteria';
	this.ui_constants['FilterValueOne'] = 'filterValueOne';
	this.ui_constants['FilterValueTwo'] = 'filterValueTwo';
	this.ui_constants['FilterCritAddLink'] = 'filterCritAddLink';
	this.ui_constants['FilterCritRemoveLink'] = 'filterCritRemoveLink';
	this.ui_constants['FilterCritSaveLink'] = 'filterCritSaveLink';
	this.ui_constants['FilterSpacer'] = 'filterSpacer';
	this.ui_constants['TitleFilterInfo'] = 'titleFilterInfo';
	this.ui_constants['AddFilterCriteria'] = 'addFilterCriteria';
	this.ui_constants['RemoveFilterImg'] = 'removeFilterImg';
	this.ui_constants['ConfirmLinkA'] = 'confirmLinkA';
	this.ui_constants['CancelLinkA'] = 'cancelLinkA';
	this.ui_constants['ColumnNamesDiv'] = 'columnNamesDiv';
	this.ui_constants['RestoreMessageDiv'] = 'restoreMessageDiv';
	this.ui_constants['CloseRestoreDiv'] = 'closeRestoreDiv';
	this.ui_constants['ManageFilterDiv'] = 'manageFilterDiv';
	this.ui_constants['CloseRestoreLink'] = 'closeRestoreLink';
	this.ui_constants['CloseRestoreSpan'] = 'closeRestoreSpan';
	this.ui_constants['CheckBoxId'] = 'checkBoxId';
	this.ui_constants['ColId'] = 'colId';
	this.ui_constants['TitleDropdown'] = 'titleDropdown';
	this.ui_constants['TitleDropdownSelected'] = 'titleDropdownSelected';
	this.ui_constants['TitleDropdownOptions'] = 'titleDropdownOptions';
	this.ui_constants['InitialLoad'] = 'initialLoad';
	this.ui_constants['manageFilterDivLink'] = 'manageFilterDivLink';
	this.ui_constants['addRemoveColumnsDivLink'] = 'addRemoveColumnsDivLink';
	this.ui_constants['menuContainer'] = 'menuContainer';

    }
};
