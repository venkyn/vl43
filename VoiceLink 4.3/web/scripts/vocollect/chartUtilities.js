/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

/**
 * This file containers all the common chart related function which can be used from
 * both voc_chart.js and voc_chart_preview.js files.
 *  
 */
function getSSChartData(configData, chartData, parentChartsFilterCategoryFields, chartId, dashboardId) {
	var dashboardIdValue = dashboardId;
	var chartIdValue = chartId;
	var category = configData.category;
	var chartFields = configData.fields;
	var childChartIds = configData.childChartIds;
	var config = getChartConfig(configData);
	var data = [];
	
	var maxDataValue = 0;
	
	if (configData.type == 'PIE') {
		$j(chartData).each(function(index, d) {
			if (chartData.length > 0) {
				$j.each(chartFields, function(idx, field) {
					if (childChartIds.length > 0) {
						var filterCategoryFields = getCopyOfArray(parentChartsFilterCategoryFields);
						filterCategoryFields.push({
							                'chartid' : chartIdValue,
							                'fieldid' : category.fieldId,
							                'fieldname' : category.name,
											'fieldvalue' : d[category.fieldId]
												});

						var linkVar = 'j-drillDownCharts-' + dashboardIdValue + '####' + JSON.stringify(childChartIds) + '####' + JSON.stringify(filterCategoryFields);
														
						data.push({
					        'label' : d[category.fieldId],
							'value' : Math.abs(d[field.fieldId]),
							'displayValue' : d[field.fieldId],
							'link' : linkVar
								});
					} else {
						data.push({
							'label' : d[category.fieldId],
							'value' : Math.abs(d[field.fieldId]),
							'displayValue' : d[field.fieldId]
								});
					}
				});
			}
		});
	} else {
		$j(chartData).each(function(index, d) {
			if (chartData.length > 0) {
				$j.each(chartFields, function(idx, field) {
					
					var fieldIdValue = d[field.fieldId];
					
					if (fieldIdValue > maxDataValue) {
						maxDataValue = fieldIdValue;
					}
					
					if (childChartIds.length > 0) {
						var filterCategoryFields = getCopyOfArray(parentChartsFilterCategoryFields);
						filterCategoryFields.push({
							                'chartid' : chartIdValue,
							                'fieldid' : category.fieldId,
							                'fieldname' : category.name,
											'fieldvalue' : d[category.fieldId]
												});

						var linkVar = 'j-drillDownCharts-' + dashboardIdValue + '####' + JSON.stringify(childChartIds) + '####' + JSON.stringify(filterCategoryFields);
								
						data.push({
							'label' : d[category.fieldId],
							'value' : fieldIdValue,
							'link' : linkVar
								});
					} else {
						data.push({
							'label' : d[category.fieldId],
							'value' : fieldIdValue
								});
					}
				});
			}
		});
		
		config.yAxisMaxValue = maxDataValue;
	}

	var combinedData = new Object();
	combinedData.chart = config;
	combinedData.data = data;
	return JSON.stringify(combinedData);
};

function getMSChartData(configData, chartData, parentChartsFilterCategoryFields, chartId, dashboardId) {
	var dashboardIdValue = dashboardId;
	var chartIdValue = chartId;
	var chartFields = configData.fields;
	var cat = configData.category;
	var childChartIds = configData.childChartIds;
	var config = getChartConfig(configData);
	var dataset = [];
	var categories = [];
	var category = [];
	var fields = [];
	var dataArray = [];
	
	var linkVar = [];
	
	var maxDataValue = 0;

	$j(chartData).each(
			function(index, d) {
				if (chartFields.length > 0) {
					category.push({
						"label" : d[cat.fieldId]
					});
					if (childChartIds.length > 0) {
						var filterCategoryFields = getCopyOfArray(parentChartsFilterCategoryFields);
						filterCategoryFields.push({
										'chartid' : chartIdValue,
										'fieldid' : cat.fieldId,
						                'fieldname' : cat.name,
										'fieldvalue' : d[cat.fieldId]
											});
						
						linkVar[index] = 'j-drillDownCharts-' + dashboardIdValue + '####'
								+ JSON.stringify(childChartIds) + '####' + JSON.stringify(filterCategoryFields);
					}
				}

				fields = [];
				$j.each(chartFields, function(idx, dataField) {
					fields.push(dataField.fieldDisplayname);
					if (index == 0) {
						dataArray[idx] = [];
					}
					dataArray[idx][index] = d[dataField.fieldId];
				});
			});

	categories.push({
		'category' : category
	});

	// create dataset to draw chart
	$j(dataArray).each(function(index, d) {
		data = [];
		$j(d).each(function(idx, v) {
			
			if (v > maxDataValue) {
				maxDataValue = v;
			}
			
			if (childChartIds.length > 0) {
				data.push({
					"value" : v,
					"link" : linkVar[idx]
				});
			} else {
				data.push({
					"value" : v
				});
			}
		});
		dataset.push({
			'seriesname' : fields[index] + '',
			'data' : data
		});
	});

	config.yAxisMaxValue = maxDataValue;
	
	var combinedData = new Object();
	combinedData.chart = config;
	combinedData.categories = categories;
	combinedData.dataset = dataset;
	return JSON.stringify(combinedData);
};


function getGridChartData(configData, chartData, parentChartsFilterCategoryFields, chartId, dashboardId) {
	var dashboardIdValue = dashboardId;
	var chartIdValue = chartId;
	var chartFields = configData.fields;
	var childChartIds = configData.childChartIds;
	var category = configData.category;
	var columns = [];
	var data = [];
	
	if (chartFields.length > 0) {
		columns.push(configData.category.name);

		$j(chartFields).each(function(index, dataField) {
			columns.push(dataField.fieldDisplayname);
		});

		$j(chartData).each(function(index, d){
			var row = [];
			if (childChartIds.length > 0) {

			    var filterCategoryFields = getCopyOfArray(parentChartsFilterCategoryFields);
				filterCategoryFields.push({
					                'chartid' : chartIdValue,
					                'fieldid' : category.fieldId,
					                'fieldname' : category.name,
									'fieldvalue' : d[category.fieldId]
										});
				

				drillDownParam = dashboardIdValue + '####' + JSON.stringify(childChartIds) + '####' + JSON.stringify(filterCategoryFields);
						
				// &#39; is the HTML code for single quote
				row.push("<a href=\'javascript:drillDownCharts(&#39;" + drillDownParam + "&#39;)\'>" + d[category.fieldId] + "</a>");
			} else {
				row.push(d[category.fieldId]);
			}

			$j(chartFields).each(function(idx, dataField) {
				row.push(d[dataField.fieldId]);
			});

			data.push(row);
		});
	}

	var combinedData = new Object();
	combinedData.columns = columns;
	combinedData.data = data;
	return JSON.stringify(combinedData);
};


function getCopyOfArray(parentFilterFieldsArray) {
	var parentFilterFieldsArrayCopy = [];
    for(var i=0; i<parentFilterFieldsArray.length; i++) {
    	parentFilterFieldsArrayCopy[i] = parentFilterFieldsArray[i];
    }
    return parentFilterFieldsArrayCopy;
};

function getChartConfig(configData) {
	var yAxisName = '';
	var xAxisName = '';
	var showLegend = '1';
	var legendCaption = '';
	var showValues = '1';
	var showLabels = '1';
	var enableSmartLabels = '0';
	var enableRotation = '0';
	var showBorder = '0';
	var formatNumber = '0';
	var formatNumberScale = '0';
	var showZeroPies = '0';
	
	var isPie = false;

	switch (jQuery(configData).attr('type')) {
	case "COLUMN":
		xAxisName = configData.category.name;
		if (configData.fields.length == 1) {
			yAxisName = configData.fields[0].fieldDisplayname;
			if(configData.fields[0].fieldUom != " ") {
				yAxisName += '(' + configData.fields[0].fieldUom + ')';
			}
			
			showLegend = '0';
		}

		break;

	case "PIE":
		if (configData.fields.length == 1) {
			legendCaption = configData.fields[0].fieldDisplayname;
			if(configData.fields[0].fieldUom != " ") {
				legendCaption += '(' + configData.fields[0].fieldUom + ')';
			}
		}

		enableSmartLabels = '1';
		showLabels = '0';
		enableRotation = '1';
		isPie = true;
		
		break;

	case "SCROLLCOLUMN":
		xAxisName = configData.category.name;
		if (configData.fields.length == 1) {
			yAxisName = configData.fields[0].fieldDisplayname;
			if(configData.fields[0].fieldUom != " ") {
				yAxisName += '(' + configData.fields[0].fieldUom + ')';
			}
			
			showLegend = '0';
		}

		break;
	}

	var chartConfig = new Object();
	chartConfig.caption = configData.name;
	chartConfig.subCaption = configData.description;
	chartConfig.chartLeftMargin = '0';
	chartConfig.chartRightMargin = '0';
	chartConfig.chartTopMargin = '0';
	chartConfig.chartBottomMargin = '2';
	chartConfig.xaxisname = xAxisName;
	chartConfig.yaxisname = yAxisName;
	chartConfig.showLegend = showLegend;
	chartConfig.legendCaption = legendCaption;
	chartConfig.showLabels = showLabels;
	chartConfig.showValues = showValues;
	chartConfig.showBorder = showBorder;
	chartConfig.formatNumber = formatNumber;
	chartConfig.formatNumberScale = formatNumberScale;
	chartConfig.enableSmartLabels = enableSmartLabels;
	chartConfig.showZeroPies = showZeroPies;
	chartConfig.enableRotation = enableRotation;
	
	if (!isPie) {
		chartConfig.adjustDiv = '0';
		chartConfig.setAdaptiveYMin = '0';
	}

	return chartConfig;
};


function drawGrid(configData, data, divId, noDataTextMessage, chartWidth, chartHeight) {
	
	divId.css({
		'width' : 'auto',
		'height' : chartHeight
	});
	
	var gridData = JSON.parse(data);	
	
	divId.gridChart({
		dataSource : {
			"chart" : {
				'caption' : configData.name,
				'subCaption' : configData.description,
				'noDataTextMessage' : noDataTextMessage,
				'columns' : gridData.columns
			},
			data : gridData.data
		}
	});
};