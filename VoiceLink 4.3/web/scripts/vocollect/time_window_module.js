/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 * 
 * Table Component 2.0 Time Window Module
 * 
 */

/**
 * Time Filter module for the table component
 */
TimeWindowModule = function(table) {
    this.titleDropdownOpen = false;
    this.table = table;
    this.tableName = table.name;
};

TimeWindowModule.prototype = new MenuModule();

TimeWindowModule.prototype.init = function(timeFilter) {
    this.currentTimeFilter = timeFilter;
    this.selectDropdownValue(timeFilter);
};

TimeWindowModule.prototype.buildTitleDisplay = function() {
    var selectedTimeFilter = "";

    switch (this.currentTimeFilter) {
    default:
	selectedTimeFilter = A({
	    'href' : '#'
	}, this.localizedStrings._8hrs);
    case 1:
	selectedTimeFilter = A({
	    'href' : '#'
	}, this.localizedStrings._8hrs);
	break;
    case 2:
	selectedTimeFilter = A({
	    'href' : '#'
	}, this.localizedStrings._10hrs);
	break;
    case 3:
	selectedTimeFilter = A({
	    'href' : '#'
	}, this.localizedStrings._24hrs);
	break;
    case 4:
	selectedTimeFilter = A({
	    'href' : '#'
	}, this.localizedStrings._48hrs);
	break;
    case 5:
	selectedTimeFilter = A({
	    'href' : '#'
	}, this.localizedStrings._72hrs);
	break;
    case 6:
	selectedTimeFilter = A({
	    'href' : '#'
	}, this.localizedStrings._168hrs);
	break;
    }
    var downArrow = A({
	'href' : '#'
    }, IMG({
	'src' : globals.BASE + '/images/transparent_group_opened.gif',
	'class' : 'downArrow'
    }));

    return DIV({
	'id' : this.ui_get('TitleDropdownSelected'),
	'class' : 'titleDropdownSelected',
	'onclick' : this.tableName + '.time_window_module.openTitleDropdown()'
    }, selectedTimeFilter, downArrow);
};

TimeWindowModule.prototype.buildTitleDropdown = function() {
    var self = this;
    var timeFilterOption8 = A({
	'href' : 'javascript:' + this.tableName
		+ '.time_window_module.selectDropdownValue(1);'
    }, this.localizedStrings._8hrs);
    var timeFilterOption10 = A({
	'href' : 'javascript:' + this.tableName
		+ '.time_window_module.selectDropdownValue(2);'
    }, this.localizedStrings._10hrs);
    var timeFilterOption24 = A({
	'href' : 'javascript:' + this.tableName
		+ '.time_window_module.selectDropdownValue(3);'
    }, this.localizedStrings._24hrs);
    var timeFilterOption48 = A({
	'href' : 'javascript:' + this.tableName
		+ '.time_window_module.selectDropdownValue(4);'
    }, this.localizedStrings._48hrs);
    var timeFilterOption72 = A({
	'href' : 'javascript:' + this.tableName
		+ '.time_window_module.selectDropdownValue(5);'
    }, this.localizedStrings._72hrs);
    var timeFilterOption168 = A({
	'href' : 'javascript:' + this.tableName
		+ '.time_window_module.selectDropdownValue(6);'
    }, this.localizedStrings._168hrs);

    var titleDropdownOptions = DIV({
	'id' : this.ui_get('TitleDropdownOptions'),
	'class' : 'titleDropdownOptions'
    }, timeFilterOption8, timeFilterOption10, timeFilterOption24,
	    timeFilterOption48, timeFilterOption72, timeFilterOption168);
    return DIV({
	'id' : this.ui_get('TitleDropdown'),
	'class' : 'titleDropdown'
    }, self.buildTitleDisplay(), titleDropdownOptions);
};

TimeWindowModule.prototype.openTitleDropdown = function() {
    var self = this;
    if (this.titleDropdownOpen == true) {
	self.closeTitleDropdown();
    } else {
	$j('#' + this.ui_get('TitleDropdownOptions')).css('visibility',
		'visible');
	$j('#' + this.ui_get('TitleDropdown')).css("zIndex", "100");
	this.titleDropdownOpen = true;
    }
};

TimeWindowModule.prototype.closeTitleDropdown = function() {
    $j('#' + this.ui_get('TitleDropdownSelected')).css('visibility', 'visible');
    $j('#' + this.ui_get('TitleDropdownOptions')).css('visibility', 'hidden');
    this.titleDropdownOpen = false;
};

TimeWindowModule.prototype.selectDropdownValue = function(inputValue) {
    this.closeTitleDropdown();
    this.currentTimeFilter = inputValue;
    this.time_filter = inputValue;
    this.table.time_filter = inputValue;
    
    $j('#' + this.ui_get('TitleDropdownSelected')).remove();
    $j('#' + this.ui_get('TitleDropdown')).append(this.buildTitleDisplay());
    this.saveTimeFilter(this.table.viewId, this.currentTimeFilter);
    this.table.refresh();
};

TimeWindowModule.prototype.saveTimeFilter = function(viewId, timeWindow) {
    var VIEW_ID_DELIMITER = "viewId=";
    var GENERAL_DELIMITER = "&";
    var TIME_WINDOW_DELIMITER = "timeWindowValue=";
    var parameterList = VIEW_ID_DELIMITER + viewId;
    parameterList += GENERAL_DELIMITER + TIME_WINDOW_DELIMITER + timeWindow;
    
    this.currentTimeFilter = timeWindow;

    $j.post(globals.BASE + "/saveTimeWindow.action", parameterList,
	    function() {
		debug.log("Timewindow value of " + timeWindow
			+ " successfully saved.");
	    });
};

TimeWindowModule.prototype.getTimeFilter = function() {
    return this.currentTimeFilter;
};

