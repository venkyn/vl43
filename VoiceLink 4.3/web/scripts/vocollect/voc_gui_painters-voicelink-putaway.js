/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

function getSummaryFilterLink_putaway(obj, prop, val, operand) {
	var filter = getFilter(-1202, -20106, val, operand)
			+ "&"
			+ getFilter(-1202, -20103, obj.region.number,
					operandHelper.numberEqualsOperand);
	filter += "&"
			+ getFilter(-1202, '-20121', licenseSummaryObj.time_filter,
					operandHelper.dateWithin);
	return painters.builders.link({
		"href" : globals.BASE + "/putaway/license/list.action?summarySiteID="
				+ obj.site.id + "&" + filter
	}, prop);
};

painters.displayTotalLicenses_putaway = function(obj) {
	if (obj.totalLicenses > 0) {
		var filter = getFilter(-1202, -20103, obj.region.number,
				operandHelper.numberEqualsOperand);
		filter += "&"
				+ getFilter(-1202, '-20121', licenseSummaryObj.time_filter,
						operandHelper.dateWithin);
		return painters.builders.link({
			'href' : globals.BASE
					+ '/putaway/license/list.action?summarySiteID='
					+ obj.site.id + '&' + filter
		}, obj.totalLicenses);
	} else {
		return obj.totalLicenses;
	}
};

painters.displayInProgressLicenses_putaway = function(obj) {
	if (obj.inProgressLicenses > 0) {
		return getSummaryFilterLink_putaway(obj, obj.inProgressLicenses, '2',
				operandHelper.enumEqualsOperand);
	} else {
		return obj.inProgressLicenses;
	}
};

painters.displayAvailableLicenses_putaway = function(obj) {
	if (obj.availableLicenses > 0) {
		return getSummaryFilterLink_putaway(obj, obj.availableLicenses, '1',
				operandHelper.enumEqualsOperand);
	} else {
		return obj.availableLicenses;
	}
};

painters.displayCompletedLicenses_putaway = function(obj) {
	if (obj.completedLicenses > 0) {
		return getSummaryFilterLink_putaway(obj, obj.completedLicenses, '3',
				operandHelper.enumEqualsOperand);
	} else {
		return obj.completedLicenses;
	}
};

painters.displayNonCompletedLicenses_putaway = function(obj) {
	if (obj.nonCompletedLicenses > 0) {
		return getSummaryFilterLink_putaway(obj, obj.nonCompletedLicenses, '3',
				operandHelper.enumNotEqualsOperand);
	} else {
		return obj.nonCompletedLicenses;
	}
};

painters.displayLicDigitsOperatorSpeaks_putaway = function(obj) {
	if (obj.licDigitsOperatorSpeaks == 0) {
		return itext('putaway.region.option.variable');
	} else {
		return obj.licDigitsOperatorSpeaks;
	}
};

painters.displayLocDigitsOperatorSpeaks_putaway = function(obj) {
	if (obj.locDigitsOperatorSpeaks == 0) {
		return itext('putaway.region.option.all');
	} else {
		return obj.locDigitsOperatorSpeaks;
	}
};

painters.displayCheckDigitsOperatorSpeaks_putaway = function(obj) {
	if (obj.checkDigitsOperatorSpeaks == 0) {
		return itext('putaway.region.option.all');
	} else {
		return obj.checkDigitsOperatorSpeaks;
	}
};

painters.displayExpirationFlag_putaway = function(obj) {
	if (obj.expirationFlag === true) {
		return itext('putaway.license.label.expirationFlag.true');
	} else {
		return itext('putaway.license.label.expirationFlag.false');
	}
};

painters.displayItemNumber_putaway = function(obj) {
	return painters.builders.link({
		'href' : globals.BASE + '/putaway/item/view.action?itemId='
				+ obj.item.id
	}, obj.item.number);
};

painters.displayItemNumber_putawayDetail = function(obj) {
	return painters.builders.link({
		'href' : globals.BASE + '/putaway/item/view.action?itemId='
				+ obj.license.item.id
	}, obj.license.item.number);
};

painters.displayLicenseDetails_putaway = function(obj) {
	if (obj.licenseDetailsCount > 0) {
		var filter = "?submittedFilterCriterion={'viewId':'-1203','columnId':'-20204','operandId':'-4','value1':'"
				+ obj.number + "','value2':'','critNum':1,'locked':false}";
		return painters.builders.link({
			'href' : globals.BASE + '/putaway/licenseDetail/list.action'
					+ filter
		}, obj.licenseDetailsCount);
	} else {
		return obj.licenseDetailsCount;
	}
};

painters.displayImported_putaway = function(obj) {
	if (obj.imported) {
		return itext('putaway.license.label.imported.true');
	} else {
		return itext('putaway.license.label.imported.false');
	}
};

painters.displayLicenseRegionName_putaway = function(obj) {
	return painters.builders.link({
		'href' : globals.BASE + '/putaway/region/view.action?regionId='
				+ obj.license.region.id
	}, obj.license.region.name);
};

painters.displayStartLocation_putaway = function(obj) {
	return painters.builders.link({
		'href' : globals.BASE + '/putaway/location/view.action?locationId='
				+ obj.startLocation.id
	}, obj.startLocation.scannedVerification);
};

painters.displayPutLocation_putaway = function(obj) {
	return painters.builders.link({
		'href' : globals.BASE + '/putaway/location/view.action?locationId='
				+ obj.putLocation.id
	}, obj.putLocation.scannedVerification);
};

painters.displayAllowSkipLicense_putaway = function(obj) {
	return getBooleanTranslation_putaway(obj.allowSkipLicense);
};

painters.displayAllowCancelLicense_putaway = function(obj) {
	return getBooleanTranslation_putaway(obj.allowCancelLicense);
};

painters.displayVerifyLicense_putaway = function(obj) {
	return getBooleanTranslation_putaway(obj.verifyLicense);
};

painters.displayAllowOverrideLocation_putaway = function(obj) {
	return getBooleanTranslation_putaway(obj.allowOverrideLocation);
};

painters.displayVerifySpokenLicOrLoc_putaway = function(obj) {
	return getBooleanTranslation_putaway(obj.verifySpokenLicOrLoc);
};

painters.displayAllowOverrideQuantity_putaway = function(obj) {
	return getBooleanTranslation_putaway(obj.allowOverrideQuantity);
};

painters.displayAllowPartialPut_putaway = function(obj) {
	return getBooleanTranslation_putaway(obj.allowPartialPut);
};

painters.displayCapturePickupQuantity_putaway = function(obj) {
	return getBooleanTranslation_putaway(obj.capturePickupQuantity);
};

painters.displayCapturePutQuantity_putaway = function(obj) {
	return getBooleanTranslation_putaway(obj.capturePutQuantity);
};
