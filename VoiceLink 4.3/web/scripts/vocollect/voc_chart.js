/**
 * Copyright (c) 2013 Vocollect, Inc. Pittsburgh, PA 15235 All rights reserved.
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Table Component 2.0
 */

/** @constructor */
ChartComponent = function(_init_args) {
	this.dashboardId = _init_args['dashboardId'];
	this.chartId = _init_args['chartId'];
	this.parentChartsFilterFields = _init_args['parentChartsFilterFields'];
	this.chartX = _init_args['chartX']; 
	this.chartY = _init_args['chartY'];
	this.id = this.chartId;
	this.type = '';
	this.dialogWidth = 750; 
	this.dialogHeight = 350;
	this.chartWidth = 750; 
	this.chartHeight = 350;
	
	this.chartDataVar;
	this.noDataText;
	
	this.chartXRatio = this.chartX/ $j(window).width(); 				//ratio of X co- ordinate position value to window width
	this.dialogWidthRatio = this.dialogWidth/ $j(window).width(); 	//ratio of dialog width to window width
	
	this.isChartDrawCycleComplete = false;				//variable to indicate whether last chart draw cycle has been completed or not, 
														//to use before initiating a new chart draw call
	
	this.autoRefreshVar;	
};

ChartComponent.prototype.startAutoRefresh = function() {
	var self = this;
	
	//value of autoRefresh interval in milliseconds
	var refreshInterval = 60000;
	this.autoRefreshVar = setInterval(function() {
											if(self.isChartDrawCycleComplete) {
												self.isChartDrawCycleComplete = false;
		                                        self.getChartData();
											}
	                                  }, refreshInterval);
};

ChartComponent.prototype.stopAutoRefresh = function() {
	clearInterval(this.autoRefreshVar);
};

ChartComponent.prototype.draw = function(data) {

	var self = this;
	
	self.chartDataVar = data.data;
	
	var configData = data.config;
	var chartData = data.data;
	
	if (chartData.length == 0) {
		self.setNoTextMessage(self.parentChartsFilterFields);
	}
		
	var dataToDrawChart = '';
	
	var divId = $j('#chart-div-'+self.chartId);
	
	var dialogTitle = configData.name;

	$j(self.parentChartsFilterFields).each(function(index, d) {
		if(index != 0) {
			dialogTitle += " >> ";
		} else {
			dialogTitle = '';
		}
		dialogTitle += d.fieldname + ": " + d.fieldvalue;
	});

	if(configData.timeFilter) {
		dialogTitle += ' [Time Filter: ' + configData.timeFilter + ']';
	}
	
	if (configData.type == 'GRID') {
		dataToDrawChart = getGridChartData(configData, chartData, self.parentChartsFilterFields, self.chartId, self.dashboardId);
		
		divId.css({'overflow' : 'auto'});
		
		self.createDialogBox(dialogTitle);
		
		drawGrid(configData, dataToDrawChart, divId, self.noDataText, self.chartWidth, self.chartHeight);
		
		self.createDialogBox(dialogTitle);
	} else {
		if(configData.type == 'SCROLLCOLUMN') {
			dataToDrawChart = getMSChartData(configData, chartData, self.parentChartsFilterFields, self.chartId, self.dashboardId);
			self.type = "ScrollColumn2D";
		} else if (configData.fields.length > 1) {
			dataToDrawChart = getMSChartData(configData, chartData, self.parentChartsFilterFields, self.chartId, self.dashboardId);
			self.type = "MSColumn3D";
		} else {
			dataToDrawChart = getSSChartData(configData, chartData, self.parentChartsFilterFields, self.chartId, this.dashboardId);
			if (configData.type == 'PIE') {
				self.type = "Pie3d";
			} else {
				self.type = "Column3D";
			}
		}

		divId.css({'overflow' : 'hidden'});
		
		self.createDialogBox(dialogTitle);
		
		if  ( FusionCharts( "chart-" + self.chartId ) ) { 
		    FusionCharts( "chart-" + self.chartId ).dispose();
		    $j("div._SmartLabel_Container").remove();
		}
		
		var chart = new FusionCharts(self.type, "chart-" + self.chartId, self.chartWidth, self.chartHeight, "0", "0");
		chart.setChartData(dataToDrawChart, "json");
		chart.configure("ChartNoDataText", self.noDataText);
		chart.render("chart-div-" + self.chartId);
	}
	
	self.isChartDrawCycleComplete = true;
	
	adjustFrame();
};


ChartComponent.prototype.getChartPreferences = function(data) {
	var self = this;
	
	if (data.config.type == 'COLUMN') {
		self.dialogWidth = 450;
		self.dialogHeight = 350;
	} else if (data.config.type == 'PIE') {
		self.dialogWidth = 350;
		self.dialogHeight = 250;
	} 
	
	var frameWidth = $j(window).width();
	
	self.dialogWidthRatio = self.dialogWidth/ frameWidth;
	
	var dWidthRatio = self.dialogWidthRatio;
	var dHeight = self.dialogHeight;
	var dLeftRatio = self.chartXRatio;
	var dTop = self.chartY;

	var request = $j.ajax({
		type : 'POST',
		async : true,
		url : globals.BASE + '/getChartPreferences.action',
		data : {
			chartId : self.chartId,
			dashboardId : self.dashboardId
		}
	});

	request.done(function(preferences) {
		if (preferences) {
			if (preferences.chartWidth) {
				dWidthRatio = preferences.chartWidth;
			}

			if (preferences.chartHeight) {
				dHeight = preferences.chartHeight;
			}

			if (preferences.chartY) {
				dTop = preferences.chartY;
			}

			if (preferences.chartX) {
				dLeftRatio = preferences.chartX;
			}
		}
	
		self.dialogWidthRatio = dWidthRatio;
		self.chartXRatio = dLeftRatio;
		
		var dLeft = self.chartXRatio * frameWidth;
		var dWidth = self.dialogWidthRatio * frameWidth;
		
		var rightMarginRequired = (dLeft + dWidth) - (frameWidth - 7);
		if(rightMarginRequired > 0) {
			dLeft = dLeft - rightMarginRequired;
			if((dWidth > frameWidth) || (dLeft < 5)) {
				dLeft = 5;
			}
		}
		
		self.dialogWidth = dWidth;
		self.dialogHeight = dHeight;
		self.chartWidth = self.dialogWidth;
		self.chartHeight = self.dialogHeight;
		self.chartX = dLeft;
		self.chartY = dTop;
		
		self.draw(data);
	});

};

ChartComponent.prototype.setNoTextMessage = function(filterFields) {

	var self = this;
	var count = 0;

	if (filterFields.length == 0) {
		self.noDataText = noDataTextMessage;
		return;
	}
	
	$j(filterFields).each(function(index, filterField) {
		var chartData = chartComponentObjArray[filterField.chartid].chartDataVar;

		if (!filterField.fieldvalue) {
			count++;
			return;
		}

		var filterFieldId = filterField.fieldid;
		var filterFieldValue = filterField.fieldvalue;

		$j(chartData).each(function(idx, data) {
			var dataValue = data[filterFieldId];
			if ((dataValue != null)
					&& (dataValue == filterFieldValue)) {
				count++;
				return false;
			}
		});
	});

	self.noDataText = count == filterFields.length ? noDataTextMessage : noDataChangeInParentMessage;
};

ChartComponent.prototype.getChartData = function() {
	var self = this;
	var output = null;
	
	$j
			.ajax({
				async : true,
				url : globals.BASE
						+ '/dashboardalert/mydashboard/getChartData.action',
				cache : false,
				data : {
					dashboardId : self.dashboardId,
					chartId : self.chartId,
					parentChartsFilterFields : JSON.stringify(self.parentChartsFilterFields)
				},
				datatype : 'json',
				success : onSuccess,
				error : function(request, type, errorThrown) {
					if (!window.parent.windowNavigateAway && !window.parent.frameNavigateAway) {
					    onError();
					}
				}				
			});

	function onSuccess (resp) {
		//on successful completion of request check for any errors in response.
		
		if (checkForErrorMessageInJSON(resp)) {
			output = resp;
		}
		
		if (output == null) {
			return;
		}
		
		//when there is a recoverable or not to be reported error refresh the chart
		if (output.errorCode == 2) {
			self.isChartDrawCycleComplete = true;
			return;
		}

		//check if the data returned by the server is not in accordance with the data required to draw a chart
		//this will be true when the user has been logged out cause of session expiration due to inactivity on some page other than my dashboard.
		//then server will still return data on getChartData request but not the one required to draw the chart.
		if (output.config == null) {
			
			onError();
			
			return;
		}	
		
		self.getChartPreferences(output);
	}
	
	function onError() {
		//error happens when server fails to complete the request and throws back an error.
		
		self.stopAutoRefresh();
		
		var statusDate = new Date();
		
		var completeStatusMessage = statusDate.toLocaleString() + ' <br /> ' + redErrorStatusMessage + ' <br /> ' + redErrorDefaultMessage;

		window.parent.writeStatusMessage(completeStatusMessage,"actionMessage error");
		window.parent.$j('#messages').show();		
	}
};

ChartComponent.prototype.createDialogBox = function(dialogTitle) {

    var self = this;

	var chartDiv = $j('#chart-div-' + self.chartId);
	
	//the no. 7 represents the extra size required to
	// retain the previously being displayed height of dialog
	var dialogHeightDiff = 7;
	
	// check to see if dialog is already associated to div in order to avoid
	// multiple event associations to same dialog element.
	if (chartDiv.hasClass('ui-dialog-content')) {
		

		// if chart div has dialog associated to it, update the title,
		// width and height of dialog box
	    chartDiv.dialog('option', {
	    	'position' : { using : function(pos) {
										$j(this).css('top', self.chartY).css('left', self.chartX);
									}
	    				 },
	    	'title' : dialogTitle,
			'width' : self.dialogWidth,
			'height' : self.dialogHeight + dialogHeightDiff
		});
	    
	    if(!(chartDiv.dialog('isOpen'))) {
	    	//if dialog is close, open it
	    	chartDiv.dialog('open');
	    }
	    
	    self.chartHeight = self.dialogHeight - (chartDiv.dialog('widget').find('.ui-dialog-titlebar').height() + 13);
	    
	    return;
	} 	
		
	// Drawing the dialog; 
	
	chartDiv.dialog({
		title : dialogTitle,
		width : self.dialogWidth,
		height : self.dialogHeight + dialogHeightDiff,
		open: function (event, ui) {
		    self.startAutoRefresh();
		    self.chartHeight = self.dialogHeight - (chartDiv.dialog('widget').find('.ui-dialog-titlebar').height() + 13);
        },
		close: function (event, ui) {
        	self.stopAutoRefresh();
        },
		position : {
			using : function(pos) {
				$j(this).css('top', self.chartY).css('left', self.chartX);
			}
		}
	});
	
	var chartPreferenceVar = new Object();
	
	var sizePreferenceObj = new Object();
	sizePreferenceObj.width = self.dialogWidth;
	sizePreferenceObj.height = self.dialogHeight;	
	chartPreferenceVar.size = sizePreferenceObj;
	
	var positionPreferenceObj = new Object();	
	positionPreferenceObj.top = self.chartY;
	positionPreferenceObj.left = self.chartX;
	chartPreferenceVar.position = positionPreferenceObj;
	
	updateChartPreference(chartPreferenceVar, '/chartSizeChanged.action');

	var width = chartDiv.dialog('option', 'width');
	var xAxisMargin = 5;
	var yAxisMargin = 75; 
	
	//space to be left on right side of window as margin
	var windowRightSideMargin = 7;
	
	// Set dialog drag boundary
	chartDiv.parent().draggable({
		containment : [ xAxisMargin, yAxisMargin, $j(window).width() - (width + windowRightSideMargin), ]
	});

	chartDiv.on('dialogresize', function(event, ui) {
		
		width = chartDiv.dialog('option', 'width');

		chartDiv.parent()
				.draggable(
						'option',
						'containment',
						[
								xAxisMargin,
								yAxisMargin,
								$j(window).width()
										- (width + windowRightSideMargin), ]);
		
		$j(window.parent).mouseup(function() {
    		chartDiv.mouseup();
		});
	});

	chartDiv.on('dialogresizestop', function(event, ui) {
		// On resize stop calculating the amount of dialog area exceeding beyond
		// the visible frame limits in up and right direction and resetting it
		var titleBarHeight = chartDiv.dialog('widget').find('.ui-dialog-titlebar').height() + 13;
		
		var topDeviation = yAxisMargin - ui.position.top;
		var calculatedHeight = getCalculatedHeight(event, ui);

		if(topDeviation > 0) {
			chartDiv.dialog('option', { 
				'position' : { using : function(pos) {
											$j(this).css('top', yAxisMargin);
										}
							 },
				'height' : calculatedHeight - topDeviation + dialogHeightDiff
			});
		
			ui.position.top = yAxisMargin;
			ui.size.height = calculatedHeight - topDeviation;
		} else {
			ui.size.height = calculatedHeight;
			chartDiv.dialog('option', {
				'position' : { using : function(pos) {
											$j(this).css('top', ui.position.top);
										}
							 },
				'height' : calculatedHeight + dialogHeightDiff
			});
		}

		var rightDeviation = (ui.position.left + ui.size.width) - ($j(document).width() - windowRightSideMargin);
		if(rightDeviation > 0) {
			chartDiv.dialog('option', {
				'width' : ui.size.width - rightDeviation,
				'height' : ui.size.height + dialogHeightDiff
			});
			
			ui.size.width = ui.size.width - rightDeviation;
		}
		
		if (self.type != 'GRID') {
			chartDiv.updateFusionCharts({
				width : ui.size.width,
				height : ui.size.height - titleBarHeight
			});
			
			$j("div._SmartLabel_Container").remove();
		}

		width = chartDiv.dialog('option', 'width');

		chartDiv.parent()
				.draggable(
						'option',
						'containment',
						[
								xAxisMargin,
								yAxisMargin,
								$j(window).width()
										- (width + windowRightSideMargin), ]);

		updateChartPreference(ui, '/chartSizeChanged.action');
		
		adjustFrame();
	});
	
	var windowScrollYpos = 0;

	chartDiv.on('dialogdragstart', function(event, ui) {
		windowScrollYpos = $j(window.parent).scrollTop();
	});

	chartDiv.on('dialogdrag', function(event, ui) {
		
		ui.position.left = ui.offset.left;
		ui.position.top = ui.offset.top;
		
		var topPositionDiff = ui.position.top - self.chartY;
		
		$j(window.parent).mouseup(function() {
    		chartDiv.mouseup();
		});
		
		var newDialogFrameHeight = ui.position.top + self.dialogHeight;
		
		if (maxFrameHeight > newDialogFrameHeight) {
			newDialogFrameHeight = maxFrameHeight;
		}
		
		window.parent.adjustFrameSize(newDialogFrameHeight);
		window.parent.scrollWindow(windowScrollYpos + topPositionDiff);
	});

	chartDiv.on('dialogdragstop', function(event, ui){
		updateChartPreference(ui, '/chartPositionChanged.action');
		
		windowScrollYPos = 0;
		adjustFrame();
	});
	
	//Method to get changed height
	function getCalculatedHeight(event, ui) {
		
		//This is to handle a potential bug with FF where it returns -ve height of jquery dialog box at time.
		if(ui.size.height > -1) {
			return ui.size.height;
		}
		
		var calculatedHeight = ui.originalSize.height;
		
		var found = false;
		var widthChanged = false;

		if(ui.size.width !=  ui.originalSize.width) {
			widthChanged  = true;
		}
		
		if(ui.position.top < ui.originalPosition.top || ui.position.top > ui.originalPosition.top) {
			yChanged = true;
		}
		
		//identify resize in upward direction
		if(ui.position.top < ui.originalPosition.top && !found) {
			calculatedHeight = (ui.originalPosition.top + ui.originalSize.height) - event.pageY;
			found = true;
		}
		
		//identify squeeze from top
		if(ui.position.top > ui.originalPosition.top && !found) {
			calculatedHeight = (ui.originalPosition.top + ui.originalSize.height) - event.pageY;
			found = true;
		}
		
		//identify resize in downward direction
		if(event.pageY > (ui.position.top + ui.originalSize.height) && !found) {
			calculatedHeight = event.pageY - ui.position.top;
			found = true;
		}
		
		//identify squeeze from bottom
		if(event.pageY < (ui.position.top + ui.originalSize.height) && !found && !widthChanged) {
			calculatedHeight = event.pageY - ui.position.top;
			found = true;
		}
		
		return calculatedHeight; 
	}
	
	function updateChartPreference(ui, action) {
		var width = null; var height = null;
		if(ui.size) {
			width = ui.size.width;
			height = ui.size.height ;
			self.dialogWidth = width;
			self.dialogWidthRatio = width/ $j(window).width();
			self.dialogHeight = height;
		}
		
		self.chartY = ui.position.top;
		self.chartX = ui.position.left;
		self.chartXRatio = ui.position.left/ $j(window).width();
		
		$j.ajax({
			type : 'POST',
			async : true,
			url : globals.BASE + action,
			data : {
				chartId : self.chartId,
				dashboardId : self.dashboardId,
				chartWidth : self.dialogWidthRatio,
				chartHeight : self.dialogHeight,
				chartY : self.chartY,
				chartX : self.chartXRatio
			},
			datatype : 'json',
			success : onSuccess
		});
	}
	
	function onSuccess(resp) {
		checkForErrorMessageInJSON(resp);
	}
};


function checkForErrorMessageInJSON(request) {
	// method to check after successful response, if an error occurred at server
	// end during the ajax request.
	
	if (request.errorCode != null && request.errorCode == 1) {
		window.parent.writeStatusMessage(request.message,"actionMessage error");
		window.parent.$j('#messages').show();
		return false;
    } else {
		return true;
    }
}

// plot Drill Down Charts
function drillDownCharts(drillDownArguments) {
	
	var splitArguments = drillDownArguments.split("####");

	var dashboardId = splitArguments[0];
	var childChartIds = JSON.parse(splitArguments[1]);
	var parentChartsFilterFields = JSON.parse(splitArguments[2]);

	var x = 650;
	var y = 65;

	$j.each(childChartIds, function(index, chartObj) {
		if ($j('#chart-div-' + chartObj.id).length <= 0) {
			$j("<div></div>").attr('id', 'chart-div-' + chartObj.id).appendTo(
					$j('#chart-parent')).css({'padding-top' : 0, 'padding-bottom' : 1, 'padding-left' : 0, 'padding-right' : 0});
			
			var chart = new ChartComponent({
				'dashboardId' : dashboardId,
				'chartId' : chartObj.id,
				'parentChartsFilterFields' : parentChartsFilterFields,
				'chartX' : x,
				'chartY' : y,
				'isChartDrawnByUser' : true
			});
			
			chartComponentObjArray[chartObj.id] = chart;
			
			chart.getChartData();
		} else {			
			var chartComponentObj = chartComponentObjArray[chartObj.id];
			
			chartComponentObj.dashboardId = dashboardId;
			chartComponentObj.chartId = chartObj.id;
			chartComponentObj.parentChartsFilterFields = parentChartsFilterFields;
			chartComponentObj.chartX = x;
			chartComponentObj.chartY = y;
			chartComponentObj.isChartDrawCycleComplete = false;
			
			chartComponentObj.getChartData();
		}
		y = y + 360;
	});
}

function reAlignCharts(maxWidth) {
	$j("div[id^='chart-div']").each(function(index, chartDivObj) {
		var chartDivObjId = chartDivObj.id;
		var chartId = chartDivObjId.substring(10);
		
		var chartDiv = $j(chartDivObj);
		
		var chartComponentObj = chartComponentObjArray[chartId];
		
		var newWidth = chartComponentObj.dialogWidthRatio * maxWidth;
		var newLeftPos = chartComponentObj.chartXRatio * maxWidth;	
		var newHeight = chartComponentObj.dialogHeight + 7;
		
		var rightMarginRequired = (newLeftPos + newWidth) - (maxWidth - 7);
		if(rightMarginRequired > 0) {
			newLeftPos = newLeftPos - rightMarginRequired;
			if((newWidth > maxWidth) || (newLeftPos < 5)) {
				newLeftPos = 5;
			}
		}
		
		if (chartDiv.hasClass('ui-dialog-content')) {
			chartDiv.dialog('option', {
				'position' : { using : function(pos) {
											$j(this).css('left', newLeftPos);
										}
							 },
				'width' : newWidth,
				'height' : newHeight
			});
			
			var newTitleHeight = (chartDiv.dialog('widget').find('.ui-dialog-titlebar').height() + 13);
			
			chartDiv.updateFusionCharts({
				width : newWidth,
				height : newHeight - newTitleHeight - 7
			});
			
			$j("div._SmartLabel_Container").remove();
			
			//Apply draggable containment to all charts

			chartDiv.parent().draggable('option', 'containment',
					[ 5, 75, maxWidth - (newWidth + 7), ]);
		}
	});
}