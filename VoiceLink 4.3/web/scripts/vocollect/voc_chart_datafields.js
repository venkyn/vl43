/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Table Component 2.0
 */

/**
 * Chart Data Fields class
 */
ChartDataFields = function(_init_args) {
	this.id = _init_args['id'];
	this.container_id = _init_args['container_id'];
	this.chart_id = _init_args['chart_id'];
	this.chart_data_source_id = _init_args['chart_data_source_id'];
	this.readOnly = _init_args['read_only'];
	this.chart_data_fields = _init_args['chart_data_fields'];

	if (!this.readOnly) {
		this.readOnly = false;
	}
	
	this.datafields_table = null;
	this.data_field = null;
	this.data = null;
	this.chartdatafields = null;
	this.chartdataproperties = null;

	this.dataFieldCount = 0;

	this.init();
};

/**
 * Init method
 */
ChartDataFields.prototype.init = function() {
	if (this.chart_data_fields) {
		this.data = this.chart_data_fields;
	} else {
		this.data = this.getChartDataFieldsActionSupport();
	}

	this.chartdatafields = this.data.chartDataFields;
	this.chartdataproperties = this.data.chartDataProperties;
	this.data_field = $j("#" + this.id);

	$j('#' + this.chart_data_source_id).change($j.proxy(function() {
		this.data = this.getChartDataFieldsActionSupport();
		this.chartdatafields = this.data.chartDataFields;
		this.chartdataproperties = this.data.chartDataProperties;
		$j(this.container_id).empty();
		this.dataFieldCount = 0;
		this.draw();
	}, this));
};

/**
 * Gets the data fields list for aggregator
 */
ChartDataFields.prototype.getChartDataFieldsActionSupport = function() {
	var output = null;

	var chartId = $j('[name="' + this.chart_id + '"]').val();
	var daInformationID = $j('#' + this.chart_data_source_id).val();

	$j
			.ajax({
				async : false,
				url : globals.BASE
						+ '/dashboardalert/charts/getChartDataFieldsActionSupport.action',
				data : {
					chartId : chartId,
					daInformationID : daInformationID
				},
				datatype : 'json',
				success : function(d) {
					output = d;
				}
			});

	return output;
};

/**
 * The main draw function
 */
ChartDataFields.prototype.draw = function() {
	table = $j('<table/>', {
		'border' : '1'
	});
	$j(table).addClass('simpleData');
	$j(table).css("width", "50%");
	$j(table).css("border-color", "#AAAAAA");

	tr_controls = $j('<tr/>');

	th_datafieldName = $j('<th/>', {
		'align' : 'center'
	});
	$j(th_datafieldName).css("padding", "3px 5px");
	$j(tr_controls).append(th_datafieldName);
	$j(th_datafieldName).append("Field Name");

	th_plotfield = $j('<th/>', {
		'align' : 'center'
	});
	$j(th_plotfield).css("padding", "3px 5px");
	$j(tr_controls).append(th_plotfield);
	$j(th_plotfield).append("Plot");

	$j(table).append(tr_controls);

	if (this.chartdatafields) {
		$j(this.chartdatafields).each(
				$j.proxy(function(index, datafield) {
					tr_controls = this.drawGrid(index,
							this.chartdataproperties, datafield);
					$j(table).append(tr_controls);
					this.dataFieldCount++;
				}, this));
	} else {
		$j(this.chartdataproperties).each(
				$j.proxy(function(index, datafield) {
					tr_controls = this.drawGrid(index,
							this.chartdataproperties, datafield);
					$j(table).append(tr_controls);
					this.dataFieldCount++;
				}, this));
	}

	$j(this.container_id).append(table);

	this.datafields_table = table;

	// create a blank data set when page loads
	this.handleData();
	
	//fires an event when the the data grid is completely drawn
	$j(this.container_id).trigger('datafielddrawcomplete');
};

/**
 * Creates property key value from property list
 */
ChartDataFields.prototype.getProperties = function(data) {
	var properties = [];

	$j(data).each(function(index, value) {
		var property = {};
		property['key'] = value.fieldId;
		property['value'] = value.displayName;
		properties.push(property);
	});

	return properties;
};

/**
 * Draws the controls
 */
ChartDataFields.prototype.drawGrid = function(index, properties, datafield) {

	tr_controls = $j('<tr/>', {
		'id' : 'dataFieldRow-' + index
	});

	$j(table).append(tr_controls);

	td_datafieldName = $j('<td/>', {
		'align' : 'left',
		'id' : 'propertyFieldId-' + index + datafield.fieldId
	});
	$j(td_datafieldName).css("padding", "3px 5px");
	$j(tr_controls).append(td_datafieldName);

	var prop_options = this.getProperties(properties);
	$j.each(prop_options, function(index, prop) {
		if (prop['key'] === datafield.fieldId) {
			$j(td_datafieldName).append(prop['value']);
		}
	});

	td_datafieldChecked = $j('<td/>', {
		'align' : 'center'
	});
	$j(td_datafieldChecked).css("padding", "3px 5px");
	$j(tr_controls).append(td_datafieldChecked);

	input_fieldChecked = $j('<input/>');
	$j(input_fieldChecked).prop('id', 'fieldChecked-' + index);
	$j(input_fieldChecked).prop('columnId', datafield.columnId);
	$j(input_fieldChecked).prop('type', "checkbox");
	$j(input_fieldChecked).prop('checked', datafield.fieldChecked);
	$j(input_fieldChecked).prop('disabled', this.readOnly);
	$j(input_fieldChecked).change($j.proxy(this.handleData, this));
	$j(td_datafieldChecked).append(input_fieldChecked);

	return tr_controls;
};

/**
 * Data handler
 */
ChartDataFields.prototype.handleData = function() {

	var fields = [];

	$j('tr[id^=dataFieldRow-]')
			.each(
					function(i, row) {
						var index = $j(row).attr('id').replace('dataFieldRow-',
								'');
						var propertyIdArgument = 'propertyFieldId-' + index;
						$j('td[id^="' + propertyIdArgument + '"]').each(
								function(j, column) {
									fieldId = $j(column).attr('id').replace(
											'propertyFieldId-' + index, '');
								});
						fieldChecked = $j('#fieldChecked-' + index).attr(
								'checked') ? true : false;
						columnId = $j('#fieldChecked-' + index).prop('columnId');

						var field = {};
						field['fieldId'] = fieldId;
						field['columnId'] = columnId;
						field['sequence'] = i;
						field['fieldChecked'] = fieldChecked;

						fields.push(field);
					});

	fieldsJSON = JSON.stringify(fields);
	$j(this.data_field).val(fieldsJSON);

};