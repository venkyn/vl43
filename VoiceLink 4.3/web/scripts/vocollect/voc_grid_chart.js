/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Table Component 2.0
 */

(function($) {

	$.fn.gridChart = function(options) {

		var settings = $.extend({
			dataSource : {
				chart : {
					caption : 'Grid Chart',
					subCaption : 'This is a grid chart',
					noDataTextMessage : 'No data to display.',
					columns : [ 'Column1', 'Columnn2', 'Column3' ]
				},
				data : [ [ 'd1.1', 'd1.2', 'd1.3' ],
						[ 'd2.1', 'd2.2', 'd2.3' ], [ 'd3.1', 'd3.2', 'd3.3' ],
						[ 'd4.1', 'd4.2', 'd4.3' ] ]
			}
		}, options);

		return this.each(function() {

			if (settings.dataSource.data && settings.dataSource.data.length > 0){
				$(this).text('');
				$(this).append($('<div/>',{'align':'center'}).append($('<h3/>').text(settings.dataSource.chart.caption)));
				$(this).append($('<div/>',{'align':'center'}).append($('<h4/>').text(settings.dataSource.chart.subCaption)));
				$(this).append(grid);
			}else{
				$(this).text('');
				$(this).append($('<div/>',{'align':'center'}).append($('<h4/>').text(settings.dataSource.chart.noDataTextMessage)));
			}

		});

		function grid() {
			var t_Grid = $('<table/>', {
				width : '100%'
			});
			t_Grid.css({
				'border' : '1px solid #94ADCB'
			});

			var tr_Heading = $('<tr/>');
			$(settings.dataSource.chart.columns).each(function(index, value) {
				var th_Heading = $('<th/>');

				th_Heading.css({
					'background' : '#B5CAE2',
					'padding' : '5px',
					'font-size' : '10pt',
					'border' : '1px solid #94ADCB'
				});

				th_Heading.text(value);
				tr_Heading.append(th_Heading);
			});
			t_Grid.append(tr_Heading);

			$(settings.dataSource.data).each(function(rowIndex, row) {
				var tr_Data = $('<tr/>');
				$(row).each(function(colIndex, col) {
					var td_Data = $('<td/>');

					td_Data.css({
						'background' : '#ffffff',
						'padding' : '5px',
						'font-size' : '10pt',
						'border' : '1px solid #94ADCB'
					});

					td_Data.html(col);
					tr_Data.append(td_Data);
				});
				t_Grid.append(tr_Data);
			});

			return t_Grid;
		}

	};

}(jQuery));