/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

function createCycleCountEditStatusFTL(request) {
    if (checkForErrorMessage(request)) {
	showChangeCycleCountStatusDialog(request.responseText);
    }
}

function getDataOnSelectedForCycleCountEditStatus(tableObject, action) {
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createCycleCountEditStatusFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function performActionOnSelectedForCycleCountEditStatus(tableObject, action) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    endLine += "&status=" + $("status").value;

    doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	    handleUserMessages, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    
    tableObj = tableObject;
}

function createCycleCountEditOperatorFTL(request) {
    if (checkForErrorMessage(request)) {
	showChangeCycleCountOperatorDialog(request.responseText);
    }
}

function getDataOnSelectedForCycleCountEditOperator(tableObject, action) {
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createCycleCountEditOperatorFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function performActionOnSelectedForCycleCountEditOperator(tableObject, action) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    endLine += "&operatorId=" + $("operatorId").value;

    doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	    handleUserMessages, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    
    tableObj = tableObject;
}
