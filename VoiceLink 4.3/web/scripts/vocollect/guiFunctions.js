/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Vocollect GUI Functions
 * guiFunctions.js
 */

// boolean value to stop updating of window
var hasError = false;
var waitMessage = 'Loading, please wait...';
var waitImg = '../../images/waiting.gif';
var pageTableComponents = new Array();

function openWindow(url) {
    window.open(url);
}

function bookmarkPage() {
    var criteria = "";
    for ( var i = 0; i < pageTableComponents.length; i++) {
	if (i != 0) {
	    criteria += "&";
	}
	criteria += pageTableComponents[i].filter_module
		.getBookmarkParameters();
    }
    if (criteria != "") {
	if (window.location.search.indexOf("?") == -1) {
	    url = window.location + "?" + criteria;
	} else {
	    baseURL = window.location.toString();
	    arrBaseURL = baseURL.split("?");
	    url = arrBaseURL[0] + "?" + criteria;
	}
	// Google Chrome Exception
	if (window.chrome) {
	    alert('Automatic Bookmarking is not available when using Google Chrome. Please manually add this page to your bookmarks after you click OK.');
	} else {
	    if (window.sidebar) {
		window.sidebar.addPanel(getTitleForPage(), url, "");
	    } else if (window.external) {
		window.external.AddFavorite(url, getTitleForPage());
	    }
	}
    }
}

var waitPanel = null;
/**
 * Creates the System Response Indicator for an action that requires a wait; and
 * makes it modal depending on the boolean value passed into the isModal
 * parameter.
 */
function showWaitResponse(isModal) {
    writeStatusMessage(_waitingMessage, "waiting");
}

/**
 * Hides the wait panel and removes modal coverskin if isModal is set to true.
 */
function hideWaitResponse() {
    if (hasElementClass("messages", "waiting")) {
	writeStatusMessage(null);
    }
}

var _moreInformation;
var _defaultMessage;
var _stoppedUpdating;
var _viewDetailsLink;
var _waitingMessage;

function setLocalizedVariables(moreInformation, viewDetailsLink,
	stoppedUpdating, defaultMessage, waitingMessage) {
    _moreInformation = moreInformation;
    _stoppedUpdating = stoppedUpdating;
    _defaultMessage = defaultMessage;
    _viewDetailsLink = viewDetailsLink;
    _waitingMessage = waitingMessage;
}

/**
 * Alerts the user of an error
 * 
 * @param message -
 *                the message shown in the dropdown box
 * @param timestamp =
 *                the value to show in the timestamp pane
 */
function updateError(message, timestamp) {

    var midBubbleTextDiv = DIV({
	'class' : 'midBubbleTextDiv'
    }, null);
    if (message == null || message == "") {
	message = _defaultMessage;
    }
    appendChildNodes(midBubbleTextDiv, SPAN({
	'style' : 'display:block;'
    }, message));

    var bubbleDiv = buildDetailsDiv(midBubbleTextDiv);
    var theMessage = DIV({
	'align' : 'center'
    });
    var timestampText;
    var color = "#fff";
    if (timestamp == null || timestamp == "") {
	timestampText = "";
    } else {
	timestampText = " (" + timestamp + ")";
    }
    appendChildNodes(theMessage, SPAN({
	'class' : 'actionMessage'
    }, _stoppedUpdating + timestampText + " "), BR(null), SPAN({
	'class' : 'actionMessage'
    }, message));

    writeStatusMessage(theMessage, "error");

}

/**
 * Builds the details for partial success and updateError functionality.
 */
function buildDetailsDiv(middleArea) {
    midBubbleDiv = DIV({
	'class' : 'midBubble'
    }, SPAN({
	'class' : 'title'
    }, _moreInformation));
    appendChildNodes(midBubbleDiv, middleArea);
    bubbleDiv = DIV({
	'id' : 'viewDetailsDiv',
	'class' : 'viewDetailsDiv'
    }, null);
    appendChildNodes(bubbleDiv, DIV({
	'class' : 'topBubble'
    }, null), midBubbleDiv, DIV({
	'class' : 'botBubble'
    }, null));
    return bubbleDiv;
}

var enableSubmit = true;
function setEnableSubmit(boolVal) {
    enableSubmit = boolVal;
}

function enableSubmitButton(event) {
    setEnableSubmit(true);
    removeElementClass(anchorElement, "disabled");
}

function adjustContentHeight() {
    if ($("sidenav") && $("content")) {
	if ($("content").clientHeight < $("sidenav").clientHeight) {
	    $("content").style.height = $("sidenav").clientHeight;
	}
    }
}

/**
 * Functions for adding MouseOver events on buttons
 */

function addMouseOverClass(event) {
    element = event.target();
    addElementClass(element, "mouseover");
}

function removeMouseOverClass(event) {
    element = event.target();
    removeElementClass(element, "mouseover");
}

/**
 * This function is for submitting forms
 */
function modifySubmitVariable(formId, elementId, newName) {
    $(elementId).name = newName;
    $(formId).submit();
}

function submitForm(formelement) {
    if (enableSubmit) {
	formelement.submit();
    }
}

/**
 * Wrapper class for enablers in the actions menu.
 * 
 * @author BJR
 */
function ActionsMenuEnabler(func, msg) {
    this.execute = func;
    this.message = msg;
}

/**
 * The <code>ActionsMenu</code> object.
 * 
 * @author BJR
 */
function ActionsMenu() {

    var DISABLED_CLASS_NAME = "disabledAction";
    var ENABLED_CLASS_NAME = "enabledAction";

    /** Measured in milliseconds. */
    var BUBBLE_WAIT_TIME = 1000;

    /**
     * Hash of action id's to an ordered array of N
     * <code>ActionMenuEnabler</code>s that contain the check function and
     * disabled message for that function.
     * <p>
     * Here's a visual:<code>actionEnablers['tableId']['id', [N, ActionsMenuEnabler]]</code>
     */
    this.actionEnablers = new Object();

    /**
     * Hash of action ID's to their timeout ID.
     */
    this.timeouts = new Object();

    /**
     * Add an enabler function for a particular action element.
     * 
     * @param tableId
     *                The ID of the table component that this enabler is
     *                associated with.
     * @param actionId
     *                The ID of the action's list-item element.
     * @param func
     *                A function pointer that is used as a check.
     * @param msg
     *                The message that will be displayed if this enabler returns
     *                false.
     */
    this.addActionEnabler = function(tableId, actionId, func, msg) {
	log("addActionEnabler(" + tableId + ", " + actionId + ", func, msg");
	// Initialize the array for this action if need be.
	if (this.actionEnablers[tableId] == null) {
	    this.actionEnablers[tableId] = new Object();
	}
	if (this.actionEnablers[tableId][actionId] == null) {
	    this.actionEnablers[tableId][actionId] = new Array();
	}
	this.actionEnablers[tableId][actionId].push(new ActionsMenuEnabler(
		func, msg));

	if (tableId == 'alwaysRun') {
	    var bubbleMessage = actionId + "_msg";
	    replaceChildNodes(bubbleMessage, msg);
	}
    };
    /**
     * Callback function for the table component to tell us to update. This
     * should only happen when a selection has been made or changes in the
     * table.
     * 
     * @param event
     *                The event that caused this method to be invoked. Should be
     *                an onSelectedRowsChanged or onTableInitialized event.
     */

    // TODO something here
    this.updateActions = function(event) {
	var tableComp = event;

	// if there is no id we have a problem
	if (tableComp.id == undefined) {
	    log("error updating actions: no table component specified.");
	    return;
	}

	// Iterate over each action
	for (actionID in this.actionEnablers['alwaysRun']) {
	    this.runActionEnablers('alwaysRun', actionID, tableComp);
	}
	for (actionID in this.actionEnablers[tableComp.id]) {
	    this.runActionEnablers(tableComp.id, actionID, tableComp);
	}
    };

    this.updateActionsNoTable = function() {
	for (actionID in this.actionEnablers['alwaysRun']) {
	    this.runActionEnablers('alwaysRun', actionID, null);
	}
    };
    /**
     * Takes an action ID as an input, grabs the array of enablers to run on the
     * action, then runs them in the order they appear in the array.
     * 
     * @param tableID
     *                the table we care about.
     * @param actionID
     *                The ID of the action that also maps to our array of
     *                enablers.
     */
    this.runActionEnablers = function(tableID, actionID, tableComp) {
	var actionElement = $(actionID);
	var enablerArray = this.actionEnablers[tableID][actionID];
	var enabled = true;
	var message = "";
	// Iterate over each enabler for this action
	for ( var i = 0; i < enablerArray.length; i++) {
	    var enabler = enablerArray[i];
	    message = enabler.message;
	    enabled = enabler.execute(tableComp);
	    if (!enabled) {
		break;
	    }
	}

	// To set the message in the bubble by default(bubble shall be visible
	// only if the element is disabled).
	var bubbleMessage = $(actionID + "_msg");
	replaceChildNodes(bubbleMessage, message);

	this.setActionState(enabled, actionElement);
    };
    /**
     * Sets the state of an action, changing its CSS class to enabled or
     * disabled. This method can be used directly if an action is not sensitive
     * to the 'onSelectedRowsChanged' event.
     * 
     * @param enabled
     *                Boolean to show the element as enabled or disabled.
     * @param actionElement
     *                The action as a DOM object.
     */
    this.setActionState = function(enabled, actionElement) {
	if (enabled && hasElementClass(actionElement, DISABLED_CLASS_NAME)) {
	    swapElementClass(actionElement, DISABLED_CLASS_NAME,
		    ENABLED_CLASS_NAME);
	} else if (!enabled
		&& hasElementClass(actionElement, ENABLED_CLASS_NAME)) {
	    swapElementClass(actionElement, ENABLED_CLASS_NAME,
		    DISABLED_CLASS_NAME);
	}
    };
    /**
     * Displays the specified bubble after 1 second has passed.
     * 
     * @param event
     *                The event object from the DOM element we just
     *                mouseover'ed.
     */
    this.showActionBubble = function(event) {
	var element = (event) ? event.src() : null;
	if (element) {
	    if (this.timeouts[element.id] != null) {
		// clear the old timeout if we had one.
		clearTimeout(this.timeouts[element.id]);
	    }
	    this.timeouts[element.id] = setTimeout(
		    "getActionsMenu().showActionBubbleCallback(\'" + element.id
			    + "\')", BUBBLE_WAIT_TIME);
	}
    };
    /**
     * This method is only to be used by ActionsMenu.showActionBubble(). It is
     * purely to allow the setTimeout() method to manipulate the action menu's
     * bubble messages.
     * 
     * @param id
     *                The DOM element's ID we want to show.
     */
    this.showActionBubbleCallback = function(id) {
	var element = $(id);
	if (element && hasElementClass(element, DISABLED_CLASS_NAME)) {
	    var bubble = $(id + "_bubble");
	    bubble.style.visibility = "visible";
	}
	getActionsMenu().timeouts[id] = null;
    };
    /**
     * Hide the specified bubble.
     * 
     * @param event
     *                The event object from the DOM element we just mouseout'ed
     *                of.
     */
    this.hideActionBubble = function(event) {
	var element = (event) ? event.src() : null;
	// Should always run; in case state changes
	if (element /* && hasElementClass(element, DISABLED_CLASS_NAME) */) {
	    var timeoutNumber = this.timeouts[element.id];
	    if (!!timeoutNumber) {
		clearTimeout(timeoutNumber);
	    }
	    var bubble = $(element.id + "_bubble");
	    bubble.style.visibility = "hidden";
	}
    };
}

/**
 * Verifies if a URL in the actions menu may be navigated to. This ensures that
 * disabled actions will not be linked. This is not a security feature, but a
 * GUI feature to disallow user navigation. A crafty user could just cut&paste
 * the URL from the page source, so don't rely on this 'lil function.
 * 
 * @param anchorID
 *                The ID to the anchor HREF that was clicked.
 * @param url
 *                The destination URL in question. If the URL starts with
 *                'javascript:', it will be executed. If the URL starts with
 *                'http:\\', it will be taken as an absolute HREF. Else, the URL
 *                will be taken as a relative HREF.
 */
function verifyActionURL(anchorID, url) {
    var anchorElement = $(anchorID);
    if (hasElementClass(anchorElement, "enabledAction")) {
	window.document.location.href = url;
    }
}

/*
 * Common Action-Enablers. Custom action enablers should be written in the
 * actions FTL they are referenced in, unless they are shared by menus on
 * different pages.
 */

function enableOnOne(tableComponent) {
    return (tableComponent.getSelectedIds().length == 1);
}

function enableOnNone(tableComponent) {
    return (tableComponent.getSelectedIds().length == 0);
}

function enableOnMoreThanOne(tableComponent) {
    return (tableComponent.getSelectedIds().length > 1);
}

function enableOnMoreThanNone(tableComponent) {
    return (tableComponent.getSelectedIds().length > 0);
}

function enableOnOneAssignmentMoreThanNonePicks(tableComponent) {
    return (assignmentObj.getSelectedIds().length == 1)
	    && (tableComponent.getSelectedIds().length > 0);
}

function enableNoAdmins(tableComponent) {
    var isAdmin = tableComponent.isAdministrative();
	if (isAdmin == 0) {
	    return false;
	}
    return true;
}

function showFocusOnCheckbox(event) {
    event.src().style.background = "#999";
}

function loseFocusOnCheckbox(event) {
    event.src().style.background = "#FFF";
}

function showFocusOnLink(event) {
    addElementClass(event.src(), "IElinkFocus");
}

function hideFocusOnLink(event) {
    removeElementClass(event.src(), "IElinkFocus");
}

/**
 * Generic action helper function that grabs the ids of the selected table row
 * elements, appends them to the given url, and then invokes the url.
 * 
 * @param tableObject
 *                the table component whose selected row ids are to be retrieved
 * @param url
 *                the url to invoke
 * @param idParam
 *                the parameter name used to append ids to the url
 */
function gotoUrlWithSelected(tableObject, url, idParam) {
    idToken = idParam + "=";

	// When there are no rows present in TC the get selected id looses the
	// reference to the last row selected. This implementation is kind of hack
	// as we are assuming there will be no object with id 0. Ideally TC should
	// not loose the last row selected even if all the rows in the TC are
	// deleted.
	selectedId = tableObject.getSelectedIds();
	selectedId = (selectedId.length == '' ? [ '0' ] : selectedId);
    
    endLine = "?" + idToken + selectedId.join("&" + idToken);
    window.document.location.href = url + endLine;
}

/*
 * Generic action helper function that grabs the ids of the selected table row
 * elements and sends an asynchronous action to the server.
 */
var tableObj = null;
simpleXMLHttpRequestOutput = null;

function performActionOnSelected(tableObject, action, handler) {
    if (typeof (handler) == 'undefined') {
	handler = handleUserMessages;
    }
    if (tableObject.getSelectedIds() && tableObject.getSelectedIds().length > 0) {
	endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
	simpleXMLHttpRequestOutput = doSimpleXMLHttpRequest(action + endLine,
		{}).addCallbacks(handler, function(request) {
	    if (request.number == 403) {
		writeStatusMessage(error403Text, "error");
	    } else {
		reportError(request);
	    }
	});
	showWaitResponse(true);
	tableObj = tableObject;
    }
}

function performActionWithParams(tableObject, action, params) {
    simpleXMLHttpRequestOutput = doSimpleXMLHttpRequest(action, params)
	    .addCallbacks(handleUserMessages, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObject.clearSelectedRows();
    showWaitResponse(true);
    tableObj = tableObject;
}

/*
 * Action helper function that grabs the ids of the selected table row elements
 * and sends an asynchronous action to the server. It calls createFTL function
 * for callback handling as it returns HTML for customdialog drop down lists and
 * the like.
 */

function getDataOnSelected(tableObject, action) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    simpleXMLHttpRequestOutput = doSimpleXMLHttpRequest(action + endLine, {})
	    .addCallbacks(createFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, "error");
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function performAction(ids, action, handler) {
    if (typeof (handler) == 'undefined') {
	handler = handleUserMessages;
    }
    endLine = "?ids=" + ids;
    simpleXMLHttpRequestOutput = doSimpleXMLHttpRequest(action + endLine, {})
	    .addCallbacks(handler, function(request) {
		log("Request is ==> " + request.number);
		if (request.number == 403) {
		    writeStatusMessage(error403Text, "error");
		} else {
		    reportError(request);
		}
	    });
}

/*
 * Clears the message div
 */
function clearMessagesDiv() {
    replaceChildNodes("messages", null);
    disconnect(vdConnections["viewDetailsLink"]);
    disconnect(vdConnections["viewDetailsDiv"]);
    disconnect(vdConnections["body"]);
    disconnect(vdConnections["messageX"]);
    $("messages").style.visibility = "hidden";
}

/**
 * Handles writing error message
 */
function writeStatusMessage(message, className) {
	var messageDiv = $j("#messages");
	messageDiv.attr("class", "messageContainer");
	if(className)
		{
		messageDiv.addClass(className);
		}
	messageDiv.children().remove();
	messageDiv.append($j("<span />").html(message));
	if (className == "error") {
		imageColor = "white";
	    } else {
		imageColor = "black";
	    }
	var message_x = $j("<div />").attr(
			{
				"id": "messageX", 
				"class": imageColor
			}).click(clearMessagesDiv);
	messageDiv.append(message_x);
	resetMessagesDiv();
    //signal(currentWindow(), 'onPageChange');
}

var vdConnections = new Array();

/**
 * Handles the data returned from an asynchronous action (e.g. delete)
 */
function handleUserMessages(request) {
    // return codes from server requests
    var ERROR_SUCCESS = "0";
    var ERROR_FAILURE = "1";
    var ERROR_PARTIAL_SUCCESS = "2";
    var ERROR_REDIRECT = "3";
    var ERROR_SUCCESS_MORE_INFO = "4";

    var data = null;
    try {
	// If there is a JSON parse error...
	// FF: throws an exception
	// IE: data is undefined
	data = evalJSONRequest(request);
    } catch (error) {
	log(error);
    }

    if (!data) {
	if (waitPanel && waitPanel.isShowing == true) {
	    hideWaitResponse();
	}
	return;
    }

    if (data.errorCode == ERROR_SUCCESS) {
	writeStatusMessage(data.generalMessage);
    } else if (data.errorCode == ERROR_FAILURE) {
	writeStatusMessage(data.generalMessage, "error");
    } else if (data.errorCode == ERROR_PARTIAL_SUCCESS
	    || data.errorCode == ERROR_SUCCESS_MORE_INFO) {
	moreMessages = data.partialMessages;
	if (moreMessages.length > 0) {
	    var midBubbleTextDiv = DIV({
		'class' : 'midBubbleTextDiv'
	    }, null);
	    for ( var i = 0; i < moreMessages.length; i++) {
		appendChildNodes(midBubbleTextDiv, SPAN({
		    'style' : 'display:block;'
		}, LI({}, moreMessages[i])));
	    }

	    var bubbleDiv = buildDetailsDiv(midBubbleTextDiv);
	    var theMessage;
	    var color = "#fff";
	    if (ERROR_SUCCESS_MORE_INFO) {
		theMessage = SPAN({
		    'class' : 'actionMessage'
		}, data.generalMessage + " ");
		color = "#333";
	    } else {
		theMessage = SPAN({
		    'class' : 'actionMessage error'
		}, data.generalMessage + " ");
	    }
	    appendChildNodes(theMessage, A({
		'id' : 'viewDetailsLink',
		'style' : 'color:' + color
			+ ';text-decoration:underline;cursor:pointer;'
	    }, _viewDetailsLink), bubbleDiv);
	    writeStatusMessage(theMessage, "partial");

	    // opens and closes the View Details bubble
	    vdConnections["viewDetailsLink"] = connect($('viewDetailsLink'),
		    'onclick', toggleViewDetails);
	    vdConnections["viewDetailsDiv"] = connect($('viewDetailsDiv'),
		    'onclick', toggleViewDetails);
	    vdConnections["body"] = connect(document.body, 'onclick',
		    closeViewDetails);
	}
    } else if (data.errorCode == ERROR_REDIRECT) {
	window.location = data.generalMessage;
    }

    // moved here to prevent interference with showing the messages
    if (tableObj) {
	tableObj.refresh();
    }

    return request;
}

function setAllSiteAccess() {

    var newState = document.getElementById("siteAccess1").checked;
    var radioElement = document.getElementById("siteAccess1");
    radioElement.checked = newState;
    counter = 1;
    var element = document.getElementById("siteIds-" + counter);
    while (element != null) {
	element.disabled = true;
	counter++;
	element = document.getElementById("siteIds-" + counter);
    }

}

function setSelectiveSiteAccess() {

    var newState = document.getElementById("siteAccess0").checked;
    var radioElement = document.getElementById("siteAccess0");
    radioElement.checked = newState;
    counter = 1;
    var element = document.getElementById("siteIds-" + counter);
    while (element != null) {
	element.disabled = false;
	counter++;
	element = document.getElementById("siteIds-" + counter);
    }

}

/**
 * Handles the toggle of the View Details bubble for partial success messages
 */
function toggleViewDetails(e) {
    if (!e)
	var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation)
	e.stopPropagation();

    if ($('viewDetailsDiv').style.visibility == "visible") {
	$('viewDetailsDiv').style.visibility = "hidden";
    } else {
	$('viewDetailsDiv').style.visibility = "visible";
    }
}

/**
 * Close the ViewDetails bubble for partial success messages if it happens to be
 * open. Use this method for when an event should only close, and never open the
 * bubble (i.e. when clicking the body). Use toggleViewDetails() otherwise.
 */
function closeViewDetails(e) {
    $('viewDetailsDiv').style.visibility = "hidden";
}

/*
 * Handles the showing and hiding of regions for the search results page
 */
function hideRegion(region) {
    div = document.getElementById(region);
    divimg = document.getElementById('img_' + region);
    if (div.style.display != 'none') {
	div.style.display = 'none';
	divimg.src = '../../images/plusblue.png';
    } else {
	div.style.display = 'block';
	divimg.src = '../../images/minusblue.png';
    }
}

function hideArea(region, area) {
    id = region + '_' + area;
    div = document.getElementById(id);
    divimg = document.getElementById('img_' + region + '_' + area);
    if (div.style.display != 'none') {
	div.style.display = 'none';
	divimg.src = '../../images/plus.gif';
    } else {
	div.style.display = 'block';
	divimg.src = '../../images/minus.gif';
    }
}

function toggleComponent(elementId, openImg, closeImg) {
    // alert(elementId + " - toggle - " + openImg);
    div = document.getElementById(elementId);
    divimg = document.getElementById('img_' + elementId);
    // alert("Div display - " + div.style.display);
    if (div.style.display != 'none') {
	div.style.display = 'none';
	divimg.src = openImg;
    } else {
	div.style.display = 'block';
	divimg.src = closeImg;
    }
}

/**
 * This class logs JavaScript messages in the server log and the MochiKit
 * logWindow.
 */
function LogToServer(parameters) {
    // log level constants
    var TRACE = 1;
    var DEBUG = 2;
    var INFO = 3;
    var WARN = 4;
    var ERROR = 5;
    var FATAL = 6;

    var _url = parameters.url;
    var _loggingOn = parameters.loggingOn;
    var _logLevel = 1;

    // public functions for this class
    this.setUrl = setUrl;
    this.setLoggingOn = setLoggingOn;
    this.setLogLevel = setLogLevel;
    this.trace = trace;
    this.debug = debug;
    this.info = info;
    this.warn = warn;
    this.error = error;
    this.fatal = fatal;

    // make constants public
    this.TRACE = TRACE;
    this.DEBUG = DEBUG;
    this.INFO = INFO;
    this.WARN = WARN;
    this.ERROR = ERROR;
    this.FATAL = FATAL;

    /**
     * Set the url of the ajax call to the Action class that handles log
     * requests.
     */
    function setUrl(url) {
	_url = url;
    }

    /**
     * Set flag indicating whether we should log messages to the server and the
     * MochiKit logWindow.
     */
    function setLoggingOn(loggingOn) {
	_loggingOn = loggingOn;
    }

    /**
     * Set level of logging.
     */
    function setLogLevel(logLevel) {
	_logLevel = logLevel;
    }

    /**
     * Log messages to server at the trace level.
     */
    function trace(message) {
	logMessage(TRACE, message, MochiKit.Logging.log);
    }

    /**
     * Log messages to server at the debug level.
     */
    function debug(message) {
	logMessage(DEBUG, message, MochiKit.Logging.logDebug);
    }

    /**
     * Log messages to server at the info level.
     */
    function info(message) {
	logMessage(INFO, message, MochiKit.Logging.log);
    }

    /**
     * Log messages to server at the warn level.
     */
    function warn(message) {
	logMessage(WARN, message, MochiKit.Logging.logWarning);
    }

    /**
     * Log messages to server at the error level.
     */
    function error(message) {
	logMessage(ERROR, message, MochiKit.Logging.logError);
    }

    /**
     * Log messages to server at the fatal level.
     */
    function fatal(message) {
	logMessage(FATAL, message, MochiKit.Logging.logFatal);
    }

    /**
     * Log messages to the server. param level - Level to log messages at. param
     * message - message to log.
     */
    function logMessage(level, message, mochiKitFunction) {
	// log("_loggingOn = " + _loggingOn);
	log("level = " + level);
	log("_logLevel = " + _logLevel);
	// is logging turned on?
	// and is the logging level within the set _logLevel threshold?
	if ((_loggingOn) && (level >= _logLevel)) {
	    // call a moichiKit function to log to the logWindow
	    mochiKitFunction(message);
	    // call server to log this message
	    params = {
		logLevel : level,
		message : message
	    };
	    // ajax call to serva Action class to log message
	    doSimpleXMLHttpRequest(_url, params)
		    .addCallbacks(
			    handleLogReturn,
			    function(request) {
				MochiKit.Logging
					.logFatal("LogToServer.logMessage() - error logging to server");
			    });
	}
    }

    /**
     * Handles callback from ajax call to server. Does nothing with return
     * request. param request - Request object from server.
     */
    function handleLogReturn(request) {
	var data = evalJSONRequest(request);
	if (!data) {
	    MochiKit.Logging.logFatal("LogToServer.handleLogReturn - no data");
	    return;
	}

    }

}

/**
 * This function realigns all the CSS positioning for the action menu bubble
 * messages for IE6. The browser doesn't render CSS correctly so the best we can
 * do is calculate a new value for the 'bottom' rule.
 */
function adjustBubbleHeightForIE() {
    var height = elementDimensions($('sidenav')).h - 30;
    var bubbles = YAHOO.util.Dom.getElementsByClassName(
	    'actionBubbleContainer', 'div', 'sidenav');
    YAHOO.util.Dom.setStyle(bubbles, 'bottom', height);
}

/**
 * This function stores the lastURL accessed by the user to store it in the user
 * object for redirecting after the nav menu has been closed.
 */
function setOpenMenu(isOpen) {
    document.openCloseNavMenu.lastRequestedURL.value = location.href;
    document.openCloseNavMenu.navMenuOpen.value = isOpen;
    document.openCloseNavMenu.sideNavHeight.value = $("sidenav").clientHeight;
    document.openCloseNavMenu.submit();
}

function showOKDialog(title, htmlBody) {
    var mySimpleDialog = new YAHOO.widget.SimpleDialog("dlg", {
	effect : {
	    effect : YAHOO.widget.ContainerEffect.FADE,
	    duration : 0.25
	},
	fixedcenter : true,
	zIndex : 200,
	modal : true,
	draggable : false
    });
    var myButtons = [ {
	text : "OK",
	handler : function() {
	    this.hide();
	}
    } ];

    mySimpleDialog.cfg.queueProperty("buttons", myButtons);
    mySimpleDialog.setHeader(title);
    mySimpleDialog.setBody(htmlBody);
    mySimpleDialog.cfg.setProperty("icon", YAHOO.widget.SimpleDialog.ICON_WARN);

    mySimpleDialog.render(document.body);
    mySimpleDialog.show();
}

/**
 * Variables for menu blade animation.
 */
var menuHideAnim;
var menuShowAnim;
var currentHidingList = null;
var currentShowingList = null;
var slideDuration = 0.5;

/**
 * Hides a menu blade within the action menu.
 */
function hideActionsMenu(tableName) {
    // log( "hide " + tableName );
    var currentHidingMenu = "menuBlade_" + tableName.getTableId();
    currentHidingList = "bladeList_" + tableName.getTableId();

    // save the current height on first run
    if (tableName.menuBladeHeight == 0) {
	tableName.menuBladeHeight = $(currentHidingMenu).clientHeight;
    }
    // now hide the blade
    menuHideAnim = new YAHOO.util.Anim(currentHidingMenu, {
	height : {
	    to : 0
	}
    }, slideDuration);
    menuHideAnim.onComplete.subscribe(finishHideActionsMenu);
    menuHideAnim.animate();

}

/**
 * IE fix; hides the UL element so that IE will properly hide the action menu.
 */
function finishHideActionsMenu() {
    // log(currentHidingList);
    if (currentHidingList != null) {
	$(currentHidingList).style.display = "none";
	currentHidingList = null;
    }
}

/**
 * Shows a menu blade within the action menu.
 */
function showActionsMenu(tableName) {
    // log( "show " + tableName );
    var currentShowingMenu = "menuBlade_" + tableName.getTableId();
    currentShowingList = "bladeList_" + tableName.getTableId();
    // show the blade
    menuShowAnim = new YAHOO.util.Anim(currentShowingMenu, {
	height : {
	    to : tableName.menuBladeHeight
	}
    }, slideDuration);
    menuShowAnim.onComplete.subscribe(finishShowActionsMenu);
    menuShowAnim.animate();
}

/**
 * IE fix; shows the UL element so that IE will properly show the action menu.
 */
function finishShowActionsMenu() {
    // log(currentShowingList);
    if (currentShowingList != null) {
	$(currentShowingList).style.display = "";
	currentShowingList = null;
    }
}

// JavaScript related to appButton.ftl
var DISABLED_TAB_CLASS_NAME = "disabledTab";
var timeout;
function showNoAccessMessage(event) {
    var element = event.src();
    if (hasElementClass(element, DISABLED_TAB_CLASS_NAME)) {
	if (timeout != null) {
	    clearTimeout(timeout);
	}
	timeout = setTimeout("showNoAccessMessageCallback(\'" + element.id
		+ "\')", 1000);
    }
}

function showNoAccessMessageCallback(id) {
    var bubble = $(id + "-bubble");
    bubble.style.visibility = "visible";
    timeout = null;
}

function hideNoAccessMessage(event) {
    var element = event.src();
    if (hasElementClass(element, DISABLED_TAB_CLASS_NAME)) {
	if (timeout != null) {
	    clearTimeout(timeout);
	} else {
	    var bubble = $(element.id + "-bubble");
	    bubble.style.visibility = "hidden";
	}
    }
}

function getTextWidth(el) {

    var _hiddenResizeDiv = DIV({
	'id' : 'hiddenResizeDiv',
	'class' : 'hiddenResizeDiv'
    }, null);

    // Copy the contents of the cell
    var newCell = el.cloneNode(true);

    // Add the contents to the hidden DIV
    appendChildNodes(_hiddenResizeDiv, newCell);

    // Attach the hidden DIV to the DOM
    appendChildNodes(currentDocument().body, _hiddenResizeDiv);

    // Get the width of the DIV
    var width = _hiddenResizeDiv.clientWidth;

    // Remove the hidden DIV
    _hiddenResizeDiv.removeChild(newCell);
    currentDocument().body.removeChild(_hiddenResizeDiv);

    return (width);
}

/*
 * Firefox fix that wraps left nav headers that are too long
 */
function wrapLongHeaders() {
    // Get all left nav menu headers
    var elements = YAHOO.util.Dom.getElementsByClassName('menuHeader', 'div');
    for ( var i = 0; i <= elements.length - 1; i++) {
	// Don't include search as it contains more elements than just the
	// header
	if (elements[i].id != 'searchDiv') {
	    header = elements[i].innerHTML.split(' ');
	    // Use temporary hidden div to measure header length
	    tempDiv = DIV({
		'id' : 'tempDiv',
		'style' : 'position:absolute;visibility:hidden;'
	    });
	    $('sidenav_inside').appendChild(tempDiv);
	    header = elements[i].innerHTML.split(' ');
	    prevWidth = 0;
	    currentWidth = 0;
	    widthSinceSpace = 0;
	    addString = '';
	    // Loop through each word
	    for ( var j = 0; j <= header.length - 1; j++) {
		// Loop through each letter
		for ( var k = 0; k <= header[j].length - 1; k++) {
		    chr = header[j].toString().substring(k, k + 1);
		    tempDiv.innerHTML = tempDiv.innerHTML + chr;
		    currentWidth = parseInt(YAHOO.util.Dom.getStyle(tempDiv.id,
			    'width').toString().substr(
			    0,
			    YAHOO.util.Dom.getStyle(tempDiv.id, 'width')
				    .toString().length - 2));
		    widthSinceSpace = widthSinceSpace
			    + (currentWidth - prevWidth);
		    prevWidth = currentWidth;
		    // Add break if word is too long
		    if (widthSinceSpace > 110) {
			tempDiv.innerHTML += '<br>';
			addString += tempDiv.innerHTML;
			tempDiv.innerHTML = '';
			widthSinceSpace = 0;
		    }
		}
		tempDiv.innerHTML = tempDiv.innerHTML + ' ';
		widthSinceSpace = 0;
	    }
	    addString += tempDiv.innerHTML;
	    elements[i].innerHTML = addString;
	    // Remove temporary div
	    $('sidenav_inside').removeChild(tempDiv);
	}
    }
}

function dropdownFix(selected, ddId) {
    _firstClick = true;
    _id = ddId;

    if (selected) {
	originalWidth = 125;
    } else {
	originalWidth = 134;
    }

    var maxWidth = originalWidth;

    this.expandDrop = expandDrop;
    this.contractDrop = contractDrop;

    isNecessary();

    function isNecessary() {
	var necessary = false;
	sel = $(_id).options;
	numOptions = sel.length;

	for ( var i = 0; i <= numOptions - 1; i++) {
	    width = getTextWidth(DIV({}, sel[i].innerHTML));
	    if (width >= maxWidth) {
		necessary = true;
		maxWidth = width + 20;
	    }
	}

	if (necessary == false) {
	    YAHOO.util.Event.removeListener(document.body, 'click',
		    contractDrop);
	    YAHOO.util.Event.removeListener(_id, 'dblclick', contractDrop);
	    YAHOO.util.Event.removeListener(_id, 'blur', contractDrop);
	    YAHOO.util.Event.removeListener(_id, 'mousedown', expandDrop);
	}
    }

    function expandDrop(e, id) {

	// dropdown
	dd = $(id);

	// dropdown container div
	ddDiv = dd.parentNode;

	// outer dropdown container div
	ddDivDiv = ddDiv.parentNode;

	// nav list item <li>
	ddLi = ddDivDiv.parentNode;
	
	 
	 /*Selection drop down size is a problem for IE 8. To handle this the width
     is set to the original width for IE 8 and maxwidth for < IE 8 */	
	
	if (navigator.appName == "Microsoft Internet Explorer") {
		var ver = getInternetExplorerVersion();
		if(ver < 8.0) {
    		dd.style.width = maxWidth;
    	} else {
    		dd.style.width = originalWidth;
    	}
    }
	
	ddDiv.style.overflow = 'visible';
	ddDivDiv.style.position = 'absolute';
	spacerDiv = document.createElement('DIV');
	spacerDiv.style.height = '34px';
	spacerDiv.id = 'spacerDiv';

	ddLi.appendChild(spacerDiv);

	YAHOO.util.Event.addListener(document.body, 'click', contractDrop, id);
	YAHOO.util.Event.addListener(_id, 'blur', contractDrop, id);
	YAHOO.util.Event.addListener(_id, 'dblclick', contractDrop, id);
	YAHOO.util.Event.removeListener(_id, 'mousedown', expandDrop);
	}

    function contractDrop(e, id) {
	if (_firstClick == true) {
	    _firstClick = false;
	} else {
	    // dropdown
	    dd = $(id);

	    // dropdown container div
	    ddDiv = dd.parentNode;

	    // outer dropdown container div
	    ddDivDiv = ddDiv.parentNode;

	    // nav list item <li>
	    ddLi = ddDivDiv.parentNode;

	    dd.style.width = originalWidth;
	    ddDiv.style.overflow = 'hidden';
	    ddDivDiv.style.position = 'static';
	    spacerDiv = $('spacerDiv');
	    ddLi.removeChild(spacerDiv);

	    YAHOO.util.Event.removeListener(document.body, 'click',
		    contractDrop);
	 	YAHOO.util.Event.removeListener(_id, 'blur', contractDrop);
	    YAHOO.util.Event.removeListener(_id, 'dblclick', contractDrop);
	    YAHOO.util.Event.addListener(_id, 'mousedown', expandDrop, _id);
	    _firstClick = true;
	}
    }

}

function updateTimeStamp(timeStampString) {
    if (timeStampString !== null) {
	var timeStampDiv = document.getElementById("timeStampValue");
	if (timeStampDiv != null) {
	    timeStampDiv.innerHTML = timeStampString;
	}
    }
}

/* Piece of code to detect the version of Internet Explorer*/
function getInternetExplorerVersion() {
	var ua = navigator.userAgent;
    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
    if (re.exec(ua) != null)
      rv = parseFloat( RegExp.$1 );
    return (rv);
}

//Function to draw a drop down
function drawSelect(data, selectControlId) {i
	var select = $j(selectControlId);
	$j(select).empty();

	$j(data).each(function(index, d) {
		var option = $j('<option/>', {
			'value' : d.key
		});

		$j(option).text(d.value);
		$j(select).append(option);
	});

	return select;
}