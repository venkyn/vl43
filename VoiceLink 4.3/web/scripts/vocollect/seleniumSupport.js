/*
 * Used by Selenium to determine when all Ajax elements are completely loaded.
 */
var pageLoadStatus = new PageLoadStatus();
function PageLoadStatus() {

    var loaded = false;
    var numberOfTables = 0;
    var numberOfLoadedTables = 0;

    function pageLoadComplete() {
	loaded = true;
    }

    function tableLoadComplete() {
	numberOfLoadedTables++;
	if (numberOfTables <= numberOfLoadedTables) {
	    pageLoadComplete();
	}
    }

    function addTable() {
	numberOfTables++;
	numberOfLoadedTables++;
    }

    function isPageLoaded() {
	return loaded;
    }

    function preloadTable() {
	numberOfLoadedTables--;
	loaded = false;
    }

    function numOfTables() {
	return numberOfTables;
    }

}

// Selenium was having problems directly accessing the above object.
// This function accesses the object for it.
function isPageLoaded() {
    return pageLoadStatus.isPageLoaded();
}
