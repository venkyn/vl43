/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

/*
 * Action helper function that grabs the ids of the selected table row elements
 * for labor assignment modifying end time and sends an asynchronous action to
 * the server. It calls createLaborEndTimeFTL function for callback handling as
 * it returns HTML for customdialog drop down lists and the like.
 */

function getDataOnSelectedLaborAssignment(tableObject, action) {
    params = "?ids=" + tableObject.getSelectedIds().join("&ids=");

    doSimpleXMLHttpRequest(action + params, {}).addCallbacks(
	    createLaborEndTimeFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function createAssignOperatorFTL(request) {
    if (checkForErrorMessage(request)) {
	showAssignOperatorToWorkgroupDialog(request.responseText);
    }
}

function getAssignOperatorDataOnSelected(tableObject, action) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	    createAssignOperatorFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, "error");
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

/**
 * Create dialogbox with ftl returned by AJAX-Used for Split picks method TODO :
 * Make it generic to take the ID of the dialog box and polulate it accordingly
 */
function createFTL(request) {

    var ERROR_SUCCESS = "0";
    var ERROR_FAILURE = "1";
    var ERROR_PARTIAL = "2";

    var data;
    var isError = false;
    try {
	data = evalJSONRequest(request);
    } catch (err) {
	// evalJSONRequest throws an error in Firefox.
	isError = true;
    }
    if (data == undefined) {
	// evalJSONRequest will return undefined in IE.
	isError = true;
    }
    if (isError) {
	showSplitAssignmentDialog(request.responseText);
	return;
    }
    if (data.errorCode == ERROR_SUCCESS) {
	writeStatusMessage(data.generalMessage, 'actionMessage');
    } else if (data.errorCode == ERROR_FAILURE) {
	writeStatusMessage(data.generalMessage, 'actionMessage error');
    }
}

// function to check if the request object contains a JSON object with
// errorcode.
function checkForErrorMessage(request) {
    var ERROR_SUCCESS = "0";
    var ERROR_FAILURE = "1";
    var ERROR_PARTIAL = "2";
    if (request.responseText.indexOf("errorCode") != -1) {
	try {
	    var data = evalJSONRequest(request);
	} catch (err) {
	    log(err.toString());
	}
	if (data.errorCode == ERROR_SUCCESS) {
	    writeStatusMessage(data.generalMessage, 'actionMessage');
	} else if (data.errorCode == ERROR_FAILURE) {
	    writeStatusMessage(data.generalMessage, 'actionMessage error');
	}
	return false;
    } else {
	return true;
    }
}

function getDataOnSelectedForContainerPrintLabel(tableObject, action) {
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createContainerPrintLabelFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function createContainerPrintLabelFTL(request) {
    if (checkForErrorMessage(request)) {
	showContainerPrintLabelDialog(request.responseText);
    }
}

function performActionFromCustomDialog(tableObject, action) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    endLine += "&oldOperator=" + $("newOperatorsInRegion").value;
    endLine += "&newOperator=" + $("oldOperatorsInRegion").value;
    doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	    handleUserMessages, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    
    tableObj = tableObject;
}

function performActionOnSelectedOperatorAssignment(tableObject, action) {
    endLine = "?operatorLaborId=" + tableObject.getSelectedIds()[0];
    endLine += "&assignmentHours=" + $("assignmentHours").value;
    endLine += "&assignmentMinutes=" + $("assignmentMinutes").value;
    endLine += "&assignmentSeconds=" + $("assignmentSeconds").value;
    endLine += "&hourType=" + $("hourType").value;
    endLine += "&day="
	    + ($("hiddenDateField").value - $("timeZoneOffset").value);
    doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	    handleUserMessages, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    
    tableObj = tableObject;
}

function performActionOnPrintLabel(tableObject, action) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    endLine += "&printerId=" + $("printerName").value;
    doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	    handleUserMessages, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    
    tableObj = tableObject;
}

/**
 * sends request to server to assign multiple operators to the selected work
 * group
 */
function performAssignmentFromCustomDialog(tableObject, action) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    endLine = endLine + "&workgroup=" + $("assignWorkgroupsDropdown").value;
    doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	    handleUserMessages, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

/*
 * Drop down code for left-side navigation panel
 */
var selectObj;
var defaultDropDownOption;

function initializeDropDown(selectId, menuId) {
    // createLoggingPane(true);
    selectObj = $(selectId);
    if (menuId != null) {
	defaultDropDownOption = $(menuId);
    }
    if (defaultDropDownOption != null
	    && defaultDropDownOption.index != undefined) {
	selectObj.selectedIndex = defaultDropDownOption.index;
    } else {
	selectObj.selectedIndex = 0;
    }
    connect(selectId, 'onchange', verifyNavigationURL);
}

/**
 * Callback function which hides the 'you can't go there' bubble message.
 */
function resetDropDown() {
    $(selectObj.id + "_bubble").style.visibility = "hidden";
}

/**
 * Any attempts to navigate via the dropdown are passed through this function.
 * Displays a bubble if the selected option is not within the user's security
 * settings.
 * 
 * @param event
 *                The 'onchange' event object we are listening to.
 */
function verifyNavigationURL(event) {
    var navDropDown = event.src();
    var url = navDropDown.value;
    var item = navDropDown.options[navDropDown.selectedIndex];
    var bubble = $(selectObj.id + "_bubble");
    bubble.style.visibility = "hidden";
    if (url == "go" && defaultDropDownOption != null
	    && defaultDropDownOption.index != undefined) {

	navDropDown.selectedIndex = defaultDropDownOption.index;
    } else {
	if (hasElementClass(item, "enabledItem")) {
	    window.document.location.href = url;
	} else {
	    bubble.style.visibility = "visible";
	    callLater(3, resetDropDown);
	}
    }
}

function getLanguagesOnSelectedSummaryPrompt(tableObject, action) {
    var date = new Date();
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    paramList += "&someParam=" + date;
    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createLanguageFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function createLanguageFTL(request) {
    if (checkForErrorMessage(request)) {
	showViewSummaryPromptLanguageDialog(request.responseText);
    }
}

function getLanguagesOnSelectedSummaryPromptEdit(tableObject, action) {
    var date = new Date();
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    paramList += "&someParam=" + date;
    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createLanguageEditFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function createLanguageEditFTL(request) {
    if (checkForErrorMessage(request)) {
	showEditSummaryPromptLanguageDialog(request.responseText);
    }
}

function getLanguagesOnSelectedSummaryPromptDelete(tableObject, action) {
    var date = new Date();
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    paramList += "&someParam=" + date;
    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createLanguageDeleteFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function createLanguageDeleteFTL(request) {
    if (checkForErrorMessage(request)) {
	showDeleteSummaryPromptLanguageDialog(request.responseText);
    }
}

function performActionOnSelectedForLanguageSummaryPrompt(tableObject, action,
	redirectAction) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    endLine += "&summaryPromptId=" + tableObject.getSelectedIds()[0];
    endLine += "&language=" + $("language").value;

    doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	    handleUserMessages, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    
    tableObj = tableObject;
    window.location = redirectAction;

}

function performActionOnSelectedForLanguageView(tableObject, action) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    endLine += "&summaryPromptId=" + tableObject.getSelectedIds()[0];
    endLine += "&language=" + $("language").value;
    window.location = action + endLine;

}

function performActionOnSelectedForLanguageSummaryPromptForEdit(tableObject,
	action) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    endLine += "&summaryPromptId=" + tableObject.getSelectedIds()[0];
    endLine += "&language=" + $("language").value;
    window.location = action + endLine;
}

function getSummaryPromptForRegions(tableObject, action) {

    var date = new Date();
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    paramList += "&someParam=" + date;
    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createRegionSummaryPromptFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function createRegionSummaryPromptFTL(request) {
    if (checkForErrorMessage(request)) {
	showSummaryPromptRegionDialog(request.responseText);
    }
}

function performActionOnSummaryPromptForRegions(tableObject, action) {
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    paramList += "&normalSummaryPromptId=" + $("normalSummaryPromptId").value;
    paramList += "&chaseSummaryPromptId=" + $("chaseSummaryPromptId").value;

    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createRegionSummaryPromptFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

/**
 * actions menu link target for any labor summary.
 */
function viewLaborData(tableObj, url) {
    var obj = tableObj.getSelectedObjects()[0];
    var filter = "&"
	    + getFilter(-1022, '-12109', obj.region.number, operandHelper.numberEqualsOperand);
    window.document.location.href = url + "?summarySiteID=" + obj.site.id
	    + "&timeFilterSaved=" + tableObj.time_window_module.getTimeFilter()
	    + filter;
}

function getLockedFilter(viewId, columnId, val, operand) {
    return "submittedFilterCriterion={'viewId':'" + viewId + "','columnId':'"
	    + columnId + "','operandId':'" + operand + "','value1':'" + val
	    + "','value2':'','locked':true}";
}

function getFilter(viewId, columnId, val, operand) {
    return "submittedFilterCriterion={'viewId':'" + viewId + "','columnId':'"
	    + columnId + "','operandId':'" + operand + "','value1':'" + val
	    + "','value2':'','locked':false}";
}

function formatOperatorTeam(id, operatorTeam) {
    return painters.builders.link({
	'id' : 'id-' + id + '_' + operatorTeam.name,
	'href' : globals.BASE
		+ '/selection/operatorTeam/view.action?operatorTeamId='
		+ operatorTeam.id
    }, operatorTeam.name);
}

function formatOperator(id, operator) {
    if (operator.name != null && operator.name.length > 0) {
	return painters.builders.link({
	    'id' : 'id-' + id + '_' + operator.name,
	    'href' : globals.BASE
		    + '/selection/operator/view.action?operatorId='
		    + operator.id
	}, operator.name)
		+ " ";
    } else {
	return painters.builders.link({
	    'id' : 'id-' + id + '_' + operator.operatorId,
	    'href' : globals.BASE
		    + '/selection/operator/view.action?operatorId='
		    + operator.id
	}, operator.operatorId)
		+ " ";
    }
}
