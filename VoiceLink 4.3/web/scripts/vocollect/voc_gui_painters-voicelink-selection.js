/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

painters.displayOperatorsByRegionAndSignedOn_sel = function(obj) {
	if (obj.operatorsWorkingIn > 0) {
		var regionIdFilter = getFilter(-1005, '-10406', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1005, '-10403', '1',
				operandHelper.enumEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/operator/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
		}, obj.operatorsWorkingIn);
	} else {
		return obj.operatorsWorkingIn;
	}
};

painters.displayOperatorsByRegion_sel = function(obj) {
	if (obj.operatorsAssigned > 0) {
		var regionIdFilter = getFilter(-1005, '-10408', obj.region.name,
				operandHelper.stringEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/operator/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter
		}, obj.operatorsAssigned);
	} else {
		return obj.operatorsAssigned;
	}
};

painters.displayShortsByRegion_sel = function(obj) {
	if (obj.totalShorts > 0) {
		var regionIdFilter = getFilter(-1008, '-10703', obj.region.name,
				operandHelper.stringEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/short/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter
		}, obj.totalShorts);
	} else {
		return obj.totalShorts;
	}
};

painters.displayShortsByRegionAndShorted_sel = function(obj) {
	if (obj.shorted > 0) {
		var regionIdFilter = getFilter(-1008, '-10703', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1008, '-10704', '4',
				operandHelper.enumEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/short/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
		}, obj.shorted);
	} else {
		return obj.shorted;
	}
};

painters.displayShortsByRegionAndAssigned_sel = function(obj) {
	if (obj.assigned > 0) {
		var regionIdFilter = getFilter(-1008, '-10703', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1008, '-10704', '6',
				operandHelper.enumEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/short/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
		}, obj.assigned);
	} else {
		return obj.assigned;
	}
};

painters.displayShortsByRegionAndMarkout_sel = function(obj) {
	if (obj.markedout > 0) {
		var regionIdFilter = getFilter(-1008, '-10703', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1008, '-10704', '7',
				operandHelper.enumEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/short/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
		}, obj.markedout);
	} else {
		return obj.markedout;
	}
};

painters.displayRegion_sel = function(obj) {
	return painters.builders.link({
		"href" : globals.BASE
				+ "/selection/region/viewRegion!input.action?regionId="
				+ obj.region.id
	}, obj.region.name);
};

painters.displayAssignPicksByRegion_sel = function(obj) {
	if (obj.totalAssignments > 0) {
		var regionIdFilter = getFilter(-1009, '-10817', obj.region.name,
				operandHelper.stringEqualsOperand);
		var createdDateFilter = getFilter(-1009, '-10828', selectionAssignmentSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/assignment/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&"
					+ createdDateFilter
		}, obj.totalAssignments);
	} else {
		return obj.totalAssignments;
	}
};

painters.displayAssignPicksByRegionAndInProgress_sel = function(obj) {
	if (obj.inProgress > 0) {
		var regionIdFilter = getFilter(-1009, '-10817', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1009, '-10803', '1',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1009, '-10828', selectionAssignmentSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/assignment/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.inProgress);
	} else {
		return obj.inProgress;
	}
};

painters.displayAssignPicksByRegionAndAvailable_sel = function(obj) {
	if (obj.available > 0) {
		var regionIdFilter = getFilter(-1009, '-10817', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1009, '-10803', '3',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1009, '-10828', selectionAssignmentSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/assignment/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.available);
	} else {
		return obj.available;
	}
};

painters.displayAssignPicksByRegionAndComplete_sel = function(obj) {
	if (obj.complete > 0) {
		var regionIdFilter = getFilter(-1009, '-10817', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1009, '-10803', '5',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1009, '-10828', selectionAssignmentSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/assignment/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.complete);
	} else {
		return obj.complete;
	}

};

painters.displayAssignPicksByRegionAndNotComplete_sel = function(obj) {
	if (obj.nonComplete > 0) {
		var regionIdFilter = getFilter(-1009, '-10817', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1009, '-10803', '5',
				operandHelper.enumNotEqualsOperand);
		var createdDateFilter = getFilter(-1009, '-10828', selectionAssignmentSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/assignment/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.nonComplete);
	} else {
		return obj.nonComplete;
	}
};

painters.displayContainerCount_sel = function(obj) {
	if (obj.containerCount > 0) {
		var filter = getFilter(-1014, '-11302', obj.viewableAssignmentNumber,
				-4);
		return painters.builders.link({
			"href" : globals.BASE + "/selection/container/list.action?"
					+ filter
		}, obj.containerCount);
	} else {
		return obj.containerCount;
	}
};

painters.displayAssignmentPickDetail_sel = function(obj) {
	if (obj.pickDetailCount > 0) {
	
		/*var assignmentIdFilter = getFilter(-1015, '-11425', obj.id, -4);
		var filter = getFilter(-1015, '-11402',
				obj.viewableAssignmentNumber, -4);*/
	 // the new implementation was done for explicitly passing assignmentId and pickId in the url 
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/assignment/pickDetail/list.action?" + "assignmentId=" + obj.id + "&" + "pickId=" + "-1"
		}, obj.pickDetailCount);
	} else {
		return obj.pickDetailCount;
	}
};

painters.displayPickDetail_sel = function(obj) {
	if (obj.pickDetailCount > 0) {
		/*var assignmentIdFilter = getLockedFilter(-1015, '-11424', obj.id, -4);
		var assignmentFilter = getLockedFilter(-1015, '-11402',
				obj.assignment.viewableAssignmentNumber, -4);
		var filter = getLockedFilter(-1015, '-11404', obj.item.number, -4);*/
	 // the new implementation was done for explicitly passing assignmentId and pickId in the url
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/assignment/pickDetail/list.action?"
					+  "pickId=" + obj.id + "&" + "assignmentId=" + "-1"
		}, obj.pickDetailCount);
	} else {
		return obj.pickDetailCount;
	}
};

painters.displayPickLocationDescription_sel = function(obj) {
	return obj.pick.location.description.aggregate;
};

painters.displayGroupNumber_sel = function(obj) {
	if (obj.groupInfo.groupCount > 1) {
		return obj.groupInfo.groupNumber;
	} else {
		return "";
	}
};

painters.displayTriggerReplenish_sel = function(obj) {
	if (obj.triggerReplenish === true) {
		return itext("pick.view.true");
	} else {
		return itext("pick.view.false");
	}
};

painters.displayBaseItemOverride_sel = function(obj) {
	if (obj.baseItemOverride === true) {
		return itext("pick.view.true");
	} else {
		return itext("pick.view.false");
	}
};

painters.displayVariableWeightItem_sel = function(obj) {
	if (obj.item.isVariableWeightItem === true) {
		return itext("item.isVariableWeightItem.true");
	} else {
		return itext("item.isVariableWeightItem.false");
	}
};

painters.displayNormalProfile_sel = function(obj) {
	return painters.builders.link({
		'selectionNormalProfileId' : obj.profileNormalAssignment.id,
		'href' : globals.BASE
				+ '/selection/region/viewNormalProfile.action?regionId='
				+ obj.id
	}, obj.profileNormalAssignment.name);
};

painters.displayChaseProfile_sel = function(obj) {
	return painters.builders.link({
		'selectionChaseProfileId' : obj.profileChaseAssignment.id,
		'href' : globals.BASE
				+ '/selection/region/viewChaseProfile.action?regionId='
				+ obj.id
	}, obj.profileChaseAssignment.name);
};

painters.displayPickedWeightDecimal_sel = function(obj) {
	var variableWeight = "";
	if (obj.variableWeight) {
		variableWeight = obj.variableWeight.toFixed(4);
	}
	return variableWeight;
};

painters.displayAssignmentRoute_sel = function(obj) {
	return itext(obj.route);
};

painters.displayPickRoute_sel = function(obj) {
	return itext(obj.assignment.route);
};

painters.displayContainerRoute_sel = function(obj) {
	return itext(obj.firstAssignment.route);
};

painters.displayItemNumber_sel = function displayItemNumber(obj) {
	return painters.builders.link({
		"href" : globals.BASE + "/selection/item/view.action?itemId="
				+ obj.item.id
	}, obj.item.number);
};

painters.displayLocationId_sel = function (obj) {
	return painters.builders.link({
		"href" : globals.BASE + "/selection/location/view.action?locationId="
				+ obj.location.id
	}, obj.location.scannedVerification);
};

