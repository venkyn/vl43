/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

/* Put to store painter funtions */

/* License summary */

painters.displayTotalLicenseByRegion_pts = function(obj) {
	if (obj.totalLicenses > 0) {
		var regionIdFilter = getFilter(-1043, '-21001', obj.region.name,
				operandHelper.stringEqualsOperand);
		var createdDateFilter = getFilter(-1043, '-21010', puttostoreLicenseSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/license/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&"
					+ createdDateFilter
		}, obj.totalLicenses);
	} else {
		return obj.totalLicenses;
	}
};

painters.displayTotalLicenseByRegionAndInprogress_pts = function(obj) {
	if (obj.inProgress > 0) {
		var regionIdFilter = getFilter(-1043, '-21001', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1043, '-21004', '1',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1043, '-21010', puttostoreLicenseSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/license/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.inProgress);
	} else {
		return obj.inProgress;
	}
};

painters.displayTotalLicenseByRegionAndAvailable_pts = function(obj) {
	if (obj.available > 0) {
		var regionIdFilter = getFilter(-1043, '-21001', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1043, '-21004', '3',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1043, '-21010', puttostoreLicenseSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/license/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.available);
	} else {
		return obj.available;
	}
};

painters.displayTotalLicenseByRegionAndComplete_pts = function(obj) {
	if (obj.complete > 0) {
		var regionIdFilter = getFilter(-1043, '-21001', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1043, '-21004', '5',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1043, '-21010', puttostoreLicenseSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/license/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.complete);
	} else {
		return obj.complete;
	}
};

painters.displayTotalLicenseByRegionAndIncomplete_pts = function(obj) {
	if (obj.nonComplete > 0) {
		var regionIdFilter = getFilter(-1043, '-21001', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1043, '-21004', '5',
				operandHelper.enumNotEqualsOperand);
		var createdDateFilter = getFilter(-1043, '-21010', puttostoreLicenseSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/license/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.nonComplete);
	} else {
		return obj.nonComplete;
	}
};

painters.displayOperatorsByRegionAndSignedOn_pts = function(obj) {
	if (obj.operatorsWorkingIn > 0) {
		var regionIdFilter = getFilter(-1005, '-10406', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1005, '-10403', '1',
				operandHelper.enumEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/operator/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
		}, obj.operatorsWorkingIn);
	} else {
		return obj.operatorsWorkingIn;
	}
};

painters.displayOperatorsByRegion_pts = function(obj) {
	if (obj.operatorsAssigned > 0) {
		var regionIdFilter = getFilter(-1005, '-10408', obj.region.name,
				operandHelper.stringEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/operator/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter
		}, obj.operatorsAssigned);
	} else {
		return obj.operatorsAssigned;
	}
};

painters.displayLaborRegion_pts = function(obj) {
	return painters.builders.link({
		"href" : globals.BASE + "/puttostore/region/view.action?regionId="
				+ obj.region.id + "&summarySiteID=" + obj.site.id
	}, obj.region.name);
};

painters.displayRoute_pts = function(obj) {
	var routeFilter = getFilter(-1042, '-21301', obj.route,
			operandHelper.stringEqualsOperand);
	return painters.builders.link({
		"href" : globals.BASE
				+ "/puttostore/customerLocation/list.action?summarySiteID="
				+ obj.site.id + "&" + routeFilter
	}, obj.route);
};

painters.displayCustomer_pts = function(obj) {
	var customerFilter = getFilter(-1042, '-21301', obj.route,
			operandHelper.stringEqualsOperand);
	return painters.builders.link({
		"href" : globals.BASE
				+ "/puttostore/customerLocation/list.action?summarySiteID="
				+ obj.site.id + "&" + customerFilter
	}, obj.numberOfCustomers);
};

painters.displaySpokenLicenseLength_pts = function(obj) {
	if (obj.spokenLicenseLength == 0) {
		return itext('region.spokenDigits.all');
	} else {
		return obj.spokenLicenseLength;
	}
};


painters.displayLicensePutDetail_pts = function(obj) {
	if (obj.putDetailCount > 0) {
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/license/putDetail/list.action?licenseId=" + obj.id + "&putId=-1" 
		}, obj.putDetailCount);
	} else {
		return obj.putDetailCount;
	}
};

painters.displayPutDetail_pts = function(obj) {
	if (obj.putDetailCount > 0) {
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/license/putDetail/list.action?putId=" + obj.id + "&licenseId=-1"
		}, obj.putDetailCount);
	} else {
		return obj.putDetailCount;
	}
};


painters.displayContainerNumber_pts = function(obj) {
	if (obj.containerNumber == 'itext(multipleText)') {
		var licenseIdFilter = getLockedFilter(-1114, '-21917', obj.id, -4);
		var licenseFilter = getLockedFilter(-1114, '-21914',
				obj.license.number, -4);
		return painters.builders.link({
			"href" : "globals.BASE/puttostore/license/putDetail/list.action?"
					+ licenseIdFilter + "&" + licenseFilter
		}, obj.containerNumber);
	} else {
		return (obj.containerNumber);
	}
};

painters.displayTotalLicenseByRegion_pts = function(obj) {
	if (obj.totalLicenses > 0) {
		var regionIdFilter = getFilter(-1043, '-21001', obj.region.name,
				operandHelper.stringEqualsOperand);
		var createdDateFilter = getFilter(-1043, '-21010', puttostoreLicenseSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/license/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&"
					+ createdDateFilter
		}, obj.totalLicenses);
	} else {
		return obj.totalLicenses;
	}
};

painters.displayTotalLicenseByRegionAndAvailable_pts = function(obj) {
	if (obj.available > 0) {
		var regionIdFilter = getFilter(-1043, '-21001', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1043, '-21004', '3',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1043, '-21010', puttostoreLicenseSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/license/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.available);
	} else {
		return obj.available;
	}
};

painters.displayTotalLicenseByRegionAndComplete_pts = function(obj) {
	if (obj.complete > 0) {
		var regionIdFilter = getFilter(-1043, '-21001', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1043, '-21004', '5',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1043, '-21010', puttostoreLicenseSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/license/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.complete);
	} else {
		return obj.complete;
	}
};

painters.displayTotalLicenseByRegionAndIncomplete_pts = function(obj) {
	if (obj.nonComplete > 0) {
		var regionIdFilter = getFilter(-1043, '-21001', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1043, '-21004', '5',
				operandHelper.enumNotEqualsOperand);
		var createdDateFilter = getFilter(-1043, '-21010', puttostoreLicenseSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/license/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.nonComplete);
	} else {
		return obj.nonComplete;
	}
};

painters.displayOperatorsByRegionAndSignedOn_pts = function(obj) {
	if (obj.operatorsWorkingIn > 0) {
		var regionIdFilter = getFilter(-1005, '-10406', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1005, '-10403', '1',
				operandHelper.enumEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/puttostore/operator/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
		}, obj.operatorsWorkingIn);
	} else {
		return obj.operatorsWorkingIn;
	}
};

painters.displayLaborRegion_pts = function(obj) {
	return painters.builders.link({
		"href" : globals.BASE + "/puttostore/region/view.action?regionId="
				+ obj.region.id + "&summarySiteID=" + obj.site.id
	}, obj.region.name);
};

painters.displayRoute_pts = function(obj) {
	var routeFilter = getFilter(-1042, '-21301', obj.route,
			operandHelper.stringEqualsOperand);
	return painters.builders.link({
		"href" : globals.BASE
				+ "/puttostore/customerLocation/list.action?summarySiteID="
				+ obj.site.id + "&" + routeFilter
	}, obj.route);
};

painters.displayCustomer_pts = function(obj) {
	var customerFilter = getFilter(-1042, '-21301', obj.route,
			operandHelper.stringEqualsOperand);
	return painters.builders.link({
		"href" : globals.BASE
				+ "/puttostore/customerLocation/list.action?summarySiteID="
				+ obj.site.id + "&" + customerFilter
	}, obj.numberOfCustomers);
};

painters.displayItemNumber_pts = function displayItemNumber(obj) {
	return painters.builders.link({
		"href" : globals.BASE + "/puttostore/item/view.action?itemId="
				+ obj.item.id
	}, obj.item.number);
};

getBooleanTranslation_pts = function(value) {
	if (value === true) {
		return itext('puttostore.region.view.true');
	} else {
		return itext('puttostore.region.view.false');
	}
};

painters.displayPutLocationId_pts = function (obj) {
	return painters.builders.link({
		"href" : globals.BASE
				+ "/puttostore/location/view.action?locationId="
				+ obj.customer.location.id
	}, obj.customer.location.scannedVerification);
};

painters.displayAllowSkipping_pts = function(obj) {
	return obj.allowSkipAisle.skip;
};

painters.displayAllowOverPack_pts = function(obj) {
	return getBooleanTranslation_pts(obj.allowOverPack);
};

painters.displayAllowRepickSkips_pts = function(obj) {
	return getBooleanTranslation_pts(obj.allowRepickSkips);
};

painters.displayAllowSignOff_pts = function(obj) {
	return getBooleanTranslation_pts(obj.allowSignOff);
};

painters.displayAllowPassAssignment_pts = function(obj) {
	return getBooleanTranslation_pts(obj.allowPassAssignment);
};

painters.displayAllowMultipleOpenContainers_pts = function(obj) {
	return getBooleanTranslation_pts(obj.allowMultipleOpenContainers);
};

painters.displaySystemGeneratesContainerID_pts = function(obj) {
	return getBooleanTranslation_pts(obj.systemGeneratesContainerID);
};

painters.displayConfirmSpokenLocation_pts = function(obj) {
	return getBooleanTranslation_pts(obj.confirmSpokenLocation);
};

painters.displayConfirmSpokenLicense_pts = function(obj) {
	return getBooleanTranslation_pts(obj.confirmSpokenLicense);
};

painters.displayValidateContainerIds_pts = function(obj) {
	if (obj.validateContainerIds) {
		return itext('puttostore.region.validateContainer.true');
	} else {
		return itext('puttostore.region.validateContainer.false');
	}
};
