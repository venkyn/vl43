/**
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 *
 * eppValidation.js
 *  This file is for validating EPP reports.  Each function name corresponds
 *   to a string in the report parameter database table, validation_function.
 *   The function must take a string as an argument, representing the data
 *   to be validated, and it must return an empty string when valid, or a
 *   localized error message when invalid.
 *
 */

// Helper function
function isInteger(s) {
    var i;
    for (i = 0; i < s.length; i++) {
	// Check that current character is number.
	var c = s.charAt(i);
	if (((c < "0") || (c > "9")))
	    return false;
    }
    // All characters are numbers.
    return true;
}

function reportDate(dateStr) {
    if (dateStr == '') {
	// Accept a NULL date
	return '';
    }

    var array = dateStr.split("-");
    if (array.length != 3) {
	return "<@s.text name='report.validation.dateformat' />";
    }

    // Loop through all components to look for non-numerics
    for (var i = 0; i < array.length; i++) {
	if (!isInteger(array[i])) {
	    return "<@s.text name='report.validation.numeric' />";
	}
    }

    var year = parseInt(array[0]);
    if (year < 1950 || year > 2050) {
	return "<@s.text name='report.validation.year' />";
    }

    var month = parseInt(array[1]);
    if (month < 1 || month > 12) {
	return "<@s.text name='report.validation.month' />";
    }

    var day = parseInt(array[2]);
    if (day < 1 || day > 31) {
	return "<@s.text name='report.validation.day' />";
    }

    return '';
}

function validateDates(startDate, endDate) {

    var startYear = '';
    var startMonth = '';
    var startDay = '';
    var endYear = '';
    var endMonth = '';
    var endDay = '';

    if ((startDate == '') && (endDate != '')) {

	return "<@s.text name='report.validation.startdate'/>";

    }

    if ((startDate != '') && (endDate == '')) {

	return "<@s.text name='report.validation.enddate'/>";

    }

    var firstIndex = startDate.indexOf('-');
    var partDate = startDate.substr(startDate.indexOf('-'));
    var nextIndex = partDate.indexOf('-');
    startYear = startDate.substr(0, 4);
    //startMonth = startDate.substr(startMonth.indexOf('-'),2) - 1;
    startMonth = startDate.substr(firstIndex + 1, nextIndex + 2);
    startDay = startDate.substr(8, 2);
    if (startMonth.indexOf('-') != -1) {
	startMonth = startDate.substr(firstIndex + 1, nextIndex + 1);
	startDay = startDate.substr(7, 2);
    }
    startDate = new Date(startYear, startMonth, startDay);

    var firstIndex = endDate.indexOf('-');
    var partDate = endDate.substr(endDate.indexOf('-'));
    var nextIndex = partDate.indexOf('-');
    endYear = endDate.substr(0, 4);
    endMonth = endDate.substr(firstIndex + 1, nextIndex + 2);
    endDay = endDate.substr(8, 2);
    if (endMonth.indexOf('-') != -1) {
	endMonth = endDate.substr(firstIndex + 1, nextIndex + 1);
	endDay = endDate.substr(7, 2);
    }
    endDate = new Date(endYear, endMonth, endDay);

    if (startDate > endDate) {
	return "<@s.text name='report.validation.startbeforeenddate'/>";

    } else {
	return '';
    }

}

function email(email) {
    if (email == '') {
	// We allow an empty email to be submitted;
	// it will be treated as a wildcard.
	return '';
    }

    var emailstring = email;
    var ampIndex = emailstring.indexOf("@");
    var afterAmp = emailstring.substring((ampIndex + 1), emailstring.length);
    // find a dot in the portion of the string after the ampersand only
    var dotIndex = afterAmp.indexOf(".");
    // determine dot position in entire string (not just after amp portion)
    dotIndex = dotIndex + ampIndex + 1;
    // afterAmp will be portion of string from ampersand to dot
    afterAmp = emailstring.substring((ampIndex + 1), dotIndex);
    // afterDot will be portion of string from dot to end of string
    var afterDot = emailstring.substring((dotIndex + 1), emailstring.length);
    var beforeAmp = emailstring.substring(0, (ampIndex));
    //old regex did not allow subdomains and dots in names
    //var email_regex = /^[\w\d\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\|\}\~]+(\.[\w\d\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\|\}\~])*\@(((\w+[\w\d\-]*[\w\d]\.)+(\w+[\w\d\-]*[\w\d]))|((\d{1,3}\.){3}\d{1,3}))$/;
    var email_regex = /^\w(?:\w|-|\.(?!\.|@))*@\w(?:\w|-|\.(?!\.))*\.\w{2,3}/;
    // index of -1 means "not found"
    if ((emailstring.indexOf("@") != "-1") && (emailstring.length > 5)
	    && (afterAmp.length > 0) && (beforeAmp.length > 1)
	    && (afterDot.length > 1) && (email_regex.test(emailstring))) {
	return '';
    } else {
	return "<@s.text name='report.validation.email' />";
    }

}

var emptyValue;
function isNumeric(s, maxlength) {
    if (!isInteger(s)) {
	return "<@s.text name='report.validation.number'/>";
    }
    return checkMaxLength(s, maxlength);

}

function defCheck(s, maxlength) {
    return checkMaxLength(s, maxlength);
}

function mandatoryCheckFailed() {
    return "<@s.text name='report.validation.mandatory' />";

}

function checkMaxLength(s, maxlength) {
    if ((maxlength != 0) && (maxlength < s.length)) {
	return "<@s.text name='report.validation.maxlength' />";
    }
    return "";

}
