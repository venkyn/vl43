/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. This file contains the
 * JavaScript previously placed in the EPP decorator HEAD.
 */

/**
 * Override Jquery to $j to avoid conflicts
 */
var $j = jQuery.noConflict();

/**
 * Notification handling
 */
function handleUnackNotifications(request) {
    var ERROR_SUCCESS = "0";
    var data = evalJSONRequest(request);

    if (data) {
	if (data.errorCode == ERROR_SUCCESS) {
	    removeElementClass("plugin.module.administration", "goRed");
	} else {
	    addElementClass("plugin.module.administration", "goRed");
	}
    }
    callLater(60, watchForUnackNotifications);
}

/**
 * Notification listener
 */
function watchForUnackNotifications() {
    if ($("plugin.module.administration") != undefined) {
	doSimpleXMLHttpRequest(
		globals.BASE
			+ "/admin/notification/getUnAckNotifications.action",
		{}).addCallbacks(handleUnackNotifications, function(request) {
	    log(request);
	});
    }
}

/**
 * Dynamic Page Title
 */
function getTitleForPage() {
    return globals.TITLE;
}
