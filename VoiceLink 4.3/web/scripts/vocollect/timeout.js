/**
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 *
 * Handles the tomcat timeout.
 *
 */
// time constants
var updateUrl = globals.BASE + "/refresh.action";
var loginLocation = globals.BASE + "/logout.action";
var logoutLocation = globals.BASE + "/logout.action";

// dialog messages
// prompt to close messages
// These depend on localized global variables having been set in
// the page head before this file is included.
var windowTitle = timeoutWindowTitle;
var promptText = timeoutPromptText;
var promptButtonText = timeoutPromptButton;
// session has expired messages
var expiredText = timeoutExpiredText;
var expiredButtonText = timeoutExpiredButton;
// prompt on error messages
var errorText = timeoutErrorText;
var errorButtonText = timeoutErrorButton;

var timeTillPrompt = null;

// handle to timer objects
var deferredTimeout = null;
var deferredPrompt = null;

function startSessionTracking(e) {
    if (typeof (window['noTimeout']) == "undefined") {
	if (typeof (window['Notice']) == "undefined") {
	    Notice = 0;
	}
	if (typeof (window['SessionTimeout']) == "undefined") {
	    SessionTimeout = 20 * 60;
	}
	timeTillPrompt = SessionTimeout - Notice;

	createBaseDialogDiv();

	// start the timeout timers
	kickTimeoutTimers();
    }
}

//this variable refrence is required at my dashboard page to disable session tracking
var sessionHandler = connect(currentWindow(), 'onload', startSessionTracking);

function createBaseDialogDiv() {
    // create the divs containing the timeout message
    var overlaybase = DIV({
	'id' : 'overlaybase'
    }, null);
    var overlay = DIV({
	'id' : 'overlay'
    }, DIV({
	'id' : 'overlaytitle'
    }, windowTitle), DIV({
	'id' : 'overlaybody'
    }, null));

    // get the body to add the hidden divs to
    var body = getElementsByTagAndClassName('body', null, null)[0];
    // add to the body
    appendChildNodes(body, overlaybase, overlay);
}

function kickTimeoutTimers() {
    deferredPrompt = callLater(timeTillPrompt, promptToRefresh);
    deferredTimeout = callLater(SessionTimeout, handleTimeout);
}

function promptToRefresh() {
    showOverlay();
    buildDialog(promptText, promptButtonText, sendRefresh);
}

function buildDialog(message, label, callbackFunc) {
    var ob = DIV({
	'id' : 'overlaybody'
    }, null);
    var div = DIV({
	'id' : 'overlaytext'
    }, message);
    var btn = BUTTON({
	'id' : 'okButton'
    }, label);
    connect(btn, 'onclick', callbackFunc);
    appendChildNodes(ob, div, BR(), btn);
    swapDOM('overlaybody', ob);
}

function showOverlay() {
    // resize overlaybase height to match height of entire body,
    // not just the visible window height
    var overlaybase = document.getElementById("overlaybase");
    if (overlaybase && overlaybase.style) {
	overlaybase.style.height = document.body.scrollHeight;
    }
    // move overlay box to 200 pixels below where the user
    // is currently scrolled to
    var overlay = document.getElementById("overlay");
    if (overlay && overlay.style) {
	overlay.style.top = document.body.scrollTop + 200;
    }

    if ($('TillyApplet')) {
	$('TillyApplet').style.visibility = 'hidden';
    }

    //Hide dropdowns if IE
    if (navigator.appName == 'Microsoft Internet Explorer') {
	if ($('selectionSelect')) {
	    $('selectionSelect').style.display = 'none';
	}
	if ($('selectedSite')) {
	    $('selectedSite').style.display = 'none';
	}

	var selectBoxes = document.body.getElementsByTagName('select');
	for ( var i = 0; i < selectBoxes.length; i++) {
	    selectBoxes[i].style.display = 'none';
	}
    }

    showElement('overlaybase');
    showElement('overlay');
}

function hideOverlay() {
    if ($('TillyApplet')) {
	$('TillyApplet').style.visibility = 'visible';
    }

    //Re-display dropdowns if IE
    if (navigator.appName == 'Microsoft Internet Explorer') {
	if ($('selectionSelect')) {
	    $('selectionSelect').style.display = 'block';
	}
	if ($('selectedSite')) {
	    $('selectedSite').style.display = 'block';
	}

	var selectBoxes = document.body.getElementsByTagName('select');
	for ( var i = 0; i < selectBoxes.length; i++) {
	    selectBoxes[i].style.display = 'block';
	}
    }
    hideElement('overlaybase');
    hideElement('overlay');
}

function sendRefresh() {
    simpleXMLHttpRequestOutput = doSimpleXMLHttpRequest(updateUrl, {})
	    .addCallbacks(handleRefresh, onErrorMessage);
}

function sendTimeout() {
    simpleXMLHttpRequestOutput = doSimpleXMLHttpRequest(logoutLocation, {})
	    .addCallbacks(handleLogout, onErrorMessage);

}

function handleLogout() {

}

function handleRefresh(request) {
    if (deferredTimeout) {
	deferredTimeout.cancel();
    }
    hideOverlay();
    kickTimeoutTimers();
}

function gotoLogin() {
    document.location.href = loginLocation;
}

function handleTimeout() {
    buildDialog(expiredText, expiredButtonText, gotoLogin);
    sendTimeout();
}

function onErrorMessage(request) {
    buildDialog(errorText, errorButtonText, sendRefresh);
}
