/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

function createReplenishmentEditStatusFTL(request) {
    if (checkForErrorMessage(request)) {
	showChangeAssignmentStatusDialog(request.responseText);
    }
}

function getDataOnSelectedForReplenishmentEditStatus(tableObject, action) {
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createReplenishmentEditStatusFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function performActionOnSelectedForReplenishmentEditStatus(tableObject, action) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    endLine += "&status=" + $("status").value;

    doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	    handleUserMessages, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    
    tableObj = tableObject;
}
