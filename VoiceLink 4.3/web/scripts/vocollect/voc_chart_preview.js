/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Table Component 2.0
 */

/** @constructor */
ChartPreviewComponent = function(_init_args) {
	this.chartId = _init_args['chartId'];
	this.id = this.chartId;
	this.chartWidth = 900;
	this.chartHeight = 520;
	this.dialogWidth = 900;
	this.dialogHeight = 550;
	this.type = '';
};

ChartPreviewComponent.prototype.draw = function(data) {
	if (data == null) {
		return;
	}
	
	var configData = data.config;
	var chartData = data.data;

	var noDataText = noDataTextMessage;
	
	var dataToDrawChart = '';

	$j("<div></div>").attr('id', 'chart-preview-div').appendTo(
					$j('body')).css({'padding-top' : 0, 'padding-bottom' : 1, 'padding-left' : 0, 'padding-right' : 0});

	var divId = $j('#chart-preview-div');
	
	if (configData.type == 'GRID') {
		dataToDrawChart = getGridChartData(configData, chartData, null, null, null);
		
		divId.css({'overflow' : 'auto'});
		
		drawGrid(configData, dataToDrawChart, divId, noDataText);
	} else {
		if(configData.type == 'SCROLLCOLUMN') {
			dataToDrawChart = getMSChartData(configData, chartData, null, null, null);
			this.type = "ScrollColumn2D";
		} else if (configData.fields.length > 1) {
			dataToDrawChart = getMSChartData(configData, chartData, null, null, null);
			this.type = "MSColumn3D";
		} else {
			dataToDrawChart = getSSChartData(configData, chartData, null, null, null);
			if (configData.type == 'PIE') {
				this.type = "Pie3d";
			} else {
				this.type = "Column3D";
			}
		}

		if  ( FusionCharts( "chart-preview-" + this.chartId ) ) { 
		    FusionCharts( "chart-preview-" + this.chartId ).dispose();
		}

		divId.css({'overflow' : 'hidden'});
		
		var chart = new FusionCharts(this.type, "chart-preview-" + this.chartId, this.chartWidth, this.chartHeight, "0", "0");
		chart.setChartData(dataToDrawChart, "json");
		chart.configure("ChartNoDataText", noDataText);
		chart.render("chart-preview-div");
	}
	
	this.createDialogBox(configData.name);
};

ChartPreviewComponent.prototype.getChartData = function() {
	var self = this;
	var output = null;
	$j
			.ajax({
				async : true,
				url : globals.BASE
						+ '/dashboardalert/charts/getChartPreviewData.action',
				data : {
					chartId : this.chartId
				},
				datatype : 'json',
				success : onSuccess
			});

	function onSuccess (resp) {
		//on successful completion of request check for any errors in response.
	
		if (self.checkForErrorMessageInJSON(resp)) {
			output = resp;
		}
		
		self.draw(output);
	}
};

	// function to check if the request object contains a JSON object with
	// errorcode.
ChartPreviewComponent.prototype.checkForErrorMessageInJSON = function(request) {
	    if (request.errorCode != null && request.errorCode == 1) {
			writeStatusMessage(request.generalMessage, 'actionMessage error');
			return false;
	    } else {
			return true;
	    }
};

ChartPreviewComponent.prototype.createDialogBox = function(chartName) {

	var chartDiv = $j('#chart-preview-div');
	
	// Drawing the dialog
	chartDiv.dialog({
		autoOpen : true,
		title : chartName + " Preview",
		open: function (event, ui) {
		    //css for dialog box z-index
		    $j(".ui-dialog").css("z-index", "250");
		    //css for overlay z-index to provide modal behavior
		    $j(".ui-widget-overlay").css("z-index", "200");
		    //css for header/title bar of dialog box
		    $j("div.ui-widget-header").css({
		    	"border" : "1px solid #ffffff",
		        "background" : "#ffffff",
		        "color" : "#4c80ad",
		        "font-weight" : "bold"		    
		    });
		    //css for outer boundary of close symbol of dialog box
		    $j(".ui-dialog .ui-dialog-titlebar-close span").css({
		    	"display" : "block",
		        "margin" : "-8px"		    
		    });
		    //css for dialog close icon
		    $j("div.ui-widget-content div.ui-icon").css("background-image", "none");
        },
		close: function (event, ui) {
        	//destroy dialog functionality from the <div> block
			chartDiv.empty().dialog( "destroy" );
			//remove <div> block from DOM.
		    chartDiv.remove();	
		    //remove SmartLable_Container div created by Fusion Charts while rendering the chart using javascript
		    $j("div._SmartLabel_Container").remove();
        },
        width : this.dialogWidth,
		height : this.dialogHeight,
		resizable: false,
		draggable: false,
		modal: true,
		closeOnEscape: true
	});
};