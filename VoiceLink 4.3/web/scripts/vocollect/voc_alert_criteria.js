/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Table Component 2.0
 */

/**
 * Alert Criteria class
 */
AlertCriteria = function(_init_args) {
	this.id = _init_args['id'];
	this.container_id = _init_args['container_id'];
	this.alert_id = _init_args['alert_id'];
	this.alert_on_id = _init_args['alert_on_id'];
	this.readOnly = _init_args['read_only'];
	this.criteriaProperties = _init_args['criteria_properties'];
	this.errorTexts = _init_args['error_message'].errorTexts;

	if (!this.readOnly) {
		this.readOnly = false;
	}

	this.criteria_table = null;
	this.criteria_field = null;
	this.data = null;
	this.propertyList = null;
	this.criterias = null;

	this.criteriaCount = 0;

	this.init();
};

/**
 * Init method
 */
AlertCriteria.prototype.init = function() {
	if (this.criteriaProperties) {
		this.data = this.criteriaProperties;
	} else {
		this.data = this.getCriteriaPropertiesActionSupport();
	}

	this.propertyList = this.data.properties;
	this.criterias = this.data.criterias;
	this.criteria_field = $j("#" + this.id);

	$j('#' + this.alert_on_id).change($j.proxy(function() {
		this.data = this.getCriteriaPropertiesActionSupport();
		this.propertyList = this.data.properties;
		this.criterias = this.data.criterias;
		$j(this.container_id).empty();
		this.criteriaCount = 0;
		this.draw();
	}, this));
};

/**
 * Gets the property list for aggregator
 */
AlertCriteria.prototype.getCriteriaPropertiesActionSupport = function() {
	var output = null;

	var alertId = $j('[name="' + this.alert_id + '"]').val();
	var daInformationID = $j('#' + this.alert_on_id).val();

	$j
			.ajax({
				async : false,
				url : globals.BASE
						+ '/dashboardalert/alerts/getCriteriaPropertiesActionSupport.action',
				data : {
					alertId : alertId,
					daInformationID : daInformationID
				},
				datatype : 'json',
				success : function(d) {
					output = d;
				}
			});

	return output;
};

/**
 * Creates property key value from property list
 */
AlertCriteria.prototype.getProperties = function(data) {
	var properties = [];

	$j(data).each(function(index, value) {
		var property = {};
		property['key'] = value.fieldId;
		property['value'] = value.displayName;
		properties.push(property);
	});

	return properties;
};

/**
 * Creates property key value from property list
 */
AlertCriteria.prototype.getOperands = function(data, fieldId) {
	var operands = [];

	// Get operand list for the field
	var tagetProperty = data[0];
	$j(data).each(function(index, value) {
		if (value.fieldId === fieldId) {
			tagetProperty = value;
		}
	});

	$j(tagetProperty.operandType).each(function(index, value) {
		var operand = {};
		operand['key'] = value.id;
		operand['value'] = value.text;
		operands.push(operand);
	});

	return operands;
};

/**
 * Creates property key value from property list
 */
AlertCriteria.prototype.getUOM = function(data, fieldId) {

	// Get UOM for the field
	var tagetProperty = data[0];
	$j(data).each(function(index, value) {
		if (value.fieldId === fieldId) {
			tagetProperty = value;
		}
	});

	return tagetProperty.uom;
};

/**
 * method to sort property array to fill alert criteria rows.
 * 
 * @param propertiesDataVal1
 *            property data for 1st field to compare
 * @param propertiesDataVal2
 *            property data for 2nd field to compare
 * @returns sorted property data to draw alert criteria data dropdown.
 */
AlertCriteria.prototype.sortPropertiesData = function(propertiesDataVal1,
		propertiesDataVal2) {
	var propertyData1FieldDisplayName = propertiesDataVal1.displayName
			.toUpperCase();
	var propertyData2FieldDisplayName = propertiesDataVal2.displayName
			.toUpperCase();
	return (propertyData1FieldDisplayName < propertyData2FieldDisplayName) ? -1
			: (propertyData1FieldDisplayName > propertyData2FieldDisplayName) ? 1
					: 0;
};

/**
 * The main draw function
 */
AlertCriteria.prototype.draw = function() {
	table = $j('<table/>');
	$j(table).addClass('simpleData');

	var self = this;
	this.propertyList.sort(function(propertiesDataVal1, propertiesDataVal2) {
		return self.sortPropertiesData(propertiesDataVal1, propertiesDataVal2);
	});
	
	if (this.readOnly) {
		// Draw controls in read only mode
		$j(this.criterias).each(
				$j.proxy(function(index, criteria) {
					tr_controls = this.drawReadOnlyControls(index,
							this.propertyList, criteria);
					$j(table).append(tr_controls);
					this.criteriaCount++;
				}, this));
	} else {
		// Draw controls with edit mode
		$j(this.criterias).each(
				$j.proxy(function(index, criteria) {
					tr_controls = this.drawControls(index, this.propertyList,
							criteria);
					$j(table).append(tr_controls);
					this.criteriaCount++;
				}, this));
	}

	// If there is no criteria, just draw one criteria line
	if (!this.criterias) {
		tr_controls = this.drawControls(0, this.propertyList);
		$j(table).append(tr_controls);
		this.criteriaCount++;
	}

	// Add error message
	if (this.errorTexts && this.errorTexts.length > 0) {
		var div_errorMessage = this.drawErrorMessage(this.errorTexts);
		$j(this.container_id).append(div_errorMessage);
	}

	$j(this.container_id).append(table);

	if (!this.readOnly) {
		// The add criteria button
		var div_addCriteria = this.drawAddCriteriaLink();
		$j(this.container_id).append(div_addCriteria);
	}

	this.criteria_table = table;

	// create a blank data set when page loads
	this.handleData();
};

/**
 * Draws the controls
 */
AlertCriteria.prototype.drawControls = function(index, properties, criteria) {

	tr_controls = $j('<tr/>', {
		'id' : 'criteriaRow-' + index
	});

	$j(tr_controls).addClass('odd');

	$j(table).append(tr_controls);

	// The when and text
	var relationControl = this.drawRelationControl(index, criteria, false);
	td_relations = $j('<td/>');
	td_relations.append(relationControl);
	$j(tr_controls).append(td_relations);

	// The property
	td_properties = $j('<td/>', {
		'align' : 'center'
	});
	$j(tr_controls).append(td_properties);
	var prop_options = this.getProperties(properties);

	var fieldId = '';
	if (criteria) {
		fieldId = criteria.fieldId;
	}
	var select_property = this.drawSelect(prop_options, fieldId);
	$j(select_property).prop('id', 'property-' + index);
	$j(select_property).change($j.proxy(this.handleOperandChange, this));
	$j(select_property).change($j.proxy(this.handleData, this));
	$j(select_property).change($j.proxy(this.handleRemoveErrorMessage, this));
	$j(td_properties).append(select_property);

	// The operands
	td_operands = $j('<td/>', {
		'align' : 'left'
	});
	$j(tr_controls).append(td_operands);

	var operandType = '';
	if (criteria) {
		operandType = criteria.operandType;
	}

	var opernd_options = this.getOperands(properties, fieldId);
	var select_operand = this.drawSelect(opernd_options, operandType);
	$j(select_operand).prop('id', 'operand-' + index);
	$j(select_operand).change($j.proxy(this.handleData, this));
	$j(select_operand).change($j.proxy(this.handleRemoveErrorMessage, this));
	$j(td_operands).append(select_operand);

	// The threshold
	td_threshold = $j('<td/>', {
		'align' : 'left'
	});
	$j(tr_controls).append(td_threshold);
	input_threshold = $j('<input/>');
	$j(input_threshold).prop('id', 'threshold-' + index);

	if (criteria) {
		$j(input_threshold).val(criteria.threshold);
	}

	$j(input_threshold).change($j.proxy(this.handleData, this));
	$j(input_threshold).change($j.proxy(this.handleRemoveErrorMessage, this));

	text_uom = $j('<span/>');
	$j(text_uom).prop('id', 'uom-' + index);
	$j(text_uom).css('margin-left', '10px');
	$j(text_uom).text(this.getUOM(properties, fieldId));
	$j(td_threshold).append(input_threshold);
	$j(td_threshold).append(text_uom);

	// The remove criteria link
	td_removeCriteria = $j('<td/>', {
		'align' : 'center'
	});
	$j(tr_controls).append(td_removeCriteria);

	if (index != 0) {
		a_removeCriteria = $j('<a/>', {
			'id' : 'removeCriteria-' + index,
			'href' : '#'
		});

		$j(a_removeCriteria).text('Remove');
		$j(a_removeCriteria).on('click',
				$j.proxy(this.handleRemoveCriteria, this));
		$j(td_removeCriteria).append(a_removeCriteria);
	}

	return tr_controls;
};

/**
 * Draws the controls
 */
AlertCriteria.prototype.drawReadOnlyControls = function(index, properties,
		criteria) {
	tr_controls = $j('<tr/>');
	$j(tr_controls).addClass('odd');

	$j(table).append(tr_controls);

	// The when,and & or relation text
	var relationControl = this.drawRelationControl(index, criteria, true);
	td_relations = $j('<td/>');
	td_relations.append(relationControl);
	$j(tr_controls).append(td_relations);

	// The property
	td_properties = $j('<td/>', {
		'align' : 'left'
	});
	$j(tr_controls).append(td_properties);

	var fieldId = '';
	if (criteria) {
		fieldId = criteria.fieldId;
	}

	var prop_options = this.getProperties(properties);
	$j.each(prop_options, function(index, prop) {
		if (prop['key'] === fieldId) {
			$j(td_properties).append(prop['value']);
		}
	});

	// The operands
	td_operands = $j('<td/>', {
		'align' : 'left'
	});
	$j(tr_controls).append(td_operands);

	if (criteria) {
		operandType = criteria.operandType;
	}
	var opernd_options = this.getOperands(properties, fieldId);
	$j.each(opernd_options, function(index, prop) {
		if (prop['key'] === operandType) {
			$j(td_operands).append(prop['value']);
		}
	});

	// The threshold
	td_threshold = $j('<td/>', {
		'align' : 'left'
	});
	$j(tr_controls).append(td_threshold);

	if (criteria) {
		text_uom = $j('<span/>');
		$j(text_uom).css('margin-left', '10px');
		$j(text_uom).text(this.getUOM(properties, fieldId));
		$j(td_threshold).append(criteria.threshold);
		$j(td_threshold).append(text_uom);
	}

	return tr_controls;
};

/**
 * Draw relation control
 */
AlertCriteria.prototype.drawRelationControl = function(index, criteria,
		readOnly) {

	var relationOptions = [ {
		"key" : 1,
		"value" : "and"
	}, {
		"key" : 2,
		"value" : "or"
	} ];

	var relationControl = null;
	if (!readOnly) {
		if (index != 0) {
			relationControl = this.drawSelect(relationOptions,
					(criteria) ? criteria.relation : '');
			$j(relationControl).attr('id', 'relation-' + index);
			$j(relationControl).change($j.proxy(this.handleData, this));
			$j(relationControl).change(
					$j.proxy(this.handleRemoveErrorMessage, this));
		} else {
			relationControl = 'when';
		}
	} else {
		if (index != 0) {
			$j(relationOptions).each(function(index, value) {
				if (value.key == criteria.relation) {
					relationControl = value.value;
				}
			});
		} else {
			relationControl = 'when';
		}
	}

	return relationControl;
};

/**
 * Draws the select boxes
 */
AlertCriteria.prototype.drawSelect = function(data, selection) {

	var select = $j('<select/>');

	$j(data).each(function(index, d) {
		var option = $j('<option/>', {
			'value' : d.key
		});

		$j(option).text(d.value);

		if (d.key === selection) {
			$j(option).prop('selected', true);
		}

		$j(select).append(option);
	});

	return select;

};

/**
 * Draw add criteria link
 */
AlertCriteria.prototype.drawAddCriteriaLink = function() {
	var div_addCriteria = $j('<div/>');
	var a_addCriteria = $j('<a/>', {
		'id' : 'addCriteria',
		'href' : '#'
	});
	$j(div_addCriteria).css('margin-top', '8px');
	$j(a_addCriteria).text('Add criteria');
	$j(a_addCriteria).on('click', $j.proxy(this.handleAddCriteria, this));
	$j(div_addCriteria).append(a_addCriteria);

	return div_addCriteria;

};

/**
 * Draws the error message
 */
AlertCriteria.prototype.drawErrorMessage = function(errors) {
	var div_errorMessage = $j('<div/>', {
		'id' : 'errorCriteria'
	});

	// Didn't use the 'errorMessage' class, because the 'float' parameter was
	// changing the alignment of criteria table
	$j(div_errorMessage).css({
		'color' : '#CC3300',
		'font-size' : '11px',
		'padding' : '2px',
		'margin-top' : '2px',
		'margin-bottom' : '2px'
	});

	if (errors.length == 1) {
		// If only one message
		$j(div_errorMessage).text(errors[0].message);
	} else {
		// If there are multiple message
		$j(div_errorMessage).text("Criteria has multiple errors.");
		var a_moreInfo = $j('<a/>', {
			'id' : 'errorMoreInfo',
			'href' : '#',
			'title' : 'test'
		});

		var div_errorPopup = $j('<div/>', {
			'id' : 'moreInfo'
		});
		var span_messageHeading = $j('<span/>');
		$j(span_messageHeading).css({
			'color' : '#CC3300',
			'font-size' : '11px',
			'padding' : '2px',
			'margin-top' : '2px',
			'margin-bottom' : '2px'
		});
		$j(span_messageHeading).append(
				$j('<b><u>Criteria has the following errors:</u></b>'));
		$j(div_errorPopup).append(span_messageHeading).append($j('<br/>'));
		$j(errors).each(function(i, value) {
			var span_Message = $j('<span/>');
			$j(span_Message).text((i + 1) + ". " + value.message);
			$j(span_Message).css({
				'color' : '#CC3300',
				'font-size' : '11px',
				'padding' : '2px',
				'margin-top' : '2px',
				'margin-bottom' : '2px'
			});
			$j(div_errorPopup).append(span_Message).append($j('<br/>'));

		});

		$j(a_moreInfo).text('More Info');

		// Balloon functionality is provided by jquery.balloon plug-in
		$j(a_moreInfo).balloon({
			'css' : {
				border : "solid 1px #FF0000",
				borderRadius: "0px",
				backgroundColor : "#FAEBD7",
				boxShadow : "",
				'opacity' : '1.00'
			},
			'contents' : $j(div_errorPopup).clone()
		});
		$j(div_errorMessage).append(a_moreInfo);
	}

	return div_errorMessage;
};

/**
 * Data handler
 */
AlertCriteria.prototype.handleData = function() {

	var criterias = [];

	$j('tr[id^=criteriaRow-]').each(function(i, row) {
		var index = $j(row).attr('id').replace('criteriaRow-', '');
		fieldId = $j('#property-' + index).val();
		operandType = $j('#operand-' + index).val();
		threshold = $j('#threshold-' + index).val();

		relation = 0;
		if (index != 0) {
			relation = $j('#relation-' + index).val();
		}

		var criteria = {};
		criteria['fieldId'] = fieldId;
		criteria['sequence'] = i;
		criteria['operandType'] = Number(operandType);
		criteria['threshold'] = threshold;
		criteria['relation'] = Number(relation);

		criterias.push(criteria);
	});

	criteriasJSON = JSON.stringify(criterias);
	$j(this.criteria_field).val(criteriasJSON);

};

/**
 * Handler to change operand when property changes
 */
AlertCriteria.prototype.handleOperandChange = function(event) {
	target = event.target;
	targetId = $j(target).attr('id');
	selectedFieldId = $j(target).val();

	var opernd_options = this.getOperands(this.propertyList, selectedFieldId);

	var select_new_operand = this.drawSelect(opernd_options);

	var operandId = $j(target).attr('id').replace('property', 'operand');
	var uomId = $j(target).attr('id').replace('property', 'uom');

	var select_operand = $j('#' + operandId);

	var select_operand_parent = $j(select_operand).parent();
	$j(select_operand).remove();
	$j(select_new_operand).prop('id', operandId);
	$j(select_new_operand).change($j.proxy(this.handleData, this));
	$j(select_new_operand)
			.change($j.proxy(this.handleRemoveErrorMessage, this));
	$j(select_operand_parent).append(select_new_operand);

	$j("#" + uomId).text(this.getUOM(this.propertyList, selectedFieldId));

};

/**
 * Adds criteria
 */
AlertCriteria.prototype.handleAddCriteria = function(event) {
	var newCriteria = this
			.drawControls(this.criteriaCount++, this.propertyList);
	$j(this.criteria_table).append(newCriteria);

	this.handleData(event);
	this.handleRemoveErrorMessage(event);

	enableSubmitButton();
};

/**
 * Removes criteria
 */
AlertCriteria.prototype.handleRemoveCriteria = function(event) {
	target = event.target;
	targetId = $j(target).attr('id');

	var tr_criteriaRow = $j(target).attr('id').replace('removeCriteria',
			'criteriaRow');

	$j('#' + tr_criteriaRow).remove();

	this.handleData(event);
	this.handleRemoveErrorMessage(event);

	enableSubmitButton();
};

/**
 * Remove error message on demand
 */
AlertCriteria.prototype.handleRemoveErrorMessage = function(event) {
	// Remove error message as something changed in the control and we need to
	// re-eval error condition
	$j('#errorCriteria').remove();
};