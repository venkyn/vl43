/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Common EPP Utilities
 */

/*
 * Output method to log info to Firebug console
 */
function printfire() {
    if (document.createEvent) {
	printfire.args = arguments;
	var ev = document.createEvent("Events");
	ev.initEvent("printfire", false, true);
	dispatchEvent(ev);
    }
}

/*
 * Cross-browser event handling
 */
function addEvent(elm, evType, fn, useCapture) {
    if (elm.addEventListener) {

	elm.addEventListener(evType, fn, useCapture);
	return true;
    } else if (elm.attachEvent) {
	var r = elm.attachEvent('on' + evType, fn);
	return r;
    } else {
	elm['on' + evType] = fn;
    }
}

function toggleLogCheckBox(checkBoxName, boxId) {

    var elements = document.getElementsByName(checkBoxName);
    var newState = document.getElementById(boxId).checked;

    for ( var i = 0; i < elements.length; i++) {
	elements[i].checked = newState;
    }
}

function toggleFeatureGroup(groupName) {

    var newState = document.getElementById(groupName).checked;

    var reg = /featureGroups-(.)/;
    var results = reg.exec(groupName);
    var groupNumber = results[1];

    var counter = 0;
    var element = document.getElementById("features-g" + groupNumber + "-f"
	    + counter);
    while (element != null) {
	element.checked = newState;
	counter++;
	element = document.getElementById("features-g" + groupNumber + "-f"
		+ counter);
    }
}

function checkedFeatureInGroup(featureId) {

    var reg = /features-g(.)-f(.)/;
    var results = reg.exec(featureId);
    var groupNumber = results[1];
    var counter = 0;
    var falses = 0;
    var trues = 0;
    var element = document.getElementById("features-g" + groupNumber + "-f"
	    + counter);
    while (element != null) {
	if (element.checked) {
	    trues++;
	} else {
	    falses++;
	}
	counter++;
	element = document.getElementById("features-g" + groupNumber + "-f"
		+ counter);
    }
    if (trues == 0 && falses != 0) {
	document.getElementById("featureGroups-" + groupNumber).checked = false;
    } else if (falses == 0 && trues != 0) {
	document.getElementById("featureGroups-" + groupNumber).checked = true;
    }

}

function insertAfter(node, referenceNode) {
    referenceNode.parentNode.insertBefore(node, referenceNode.nextSibling);
}

function jscss(a, o, c1, c2) {
    switch (a) {
	case 'swap':
	    o.className = !jscss('check', o, c1)
		    ? o.className.replace(c2, c1)
			: o.className.replace(c1, c2);
	    break;
	case 'add':
	    if (!jscss('check', o, c1)) {
		o.className += o.className ? ' ' + c1 : c1;
	    }
	    break;
	case 'remove':
	    var rep = o.className.match(' ' + c1) ? ' ' + c1 : c1;
	    o.className = o.className.replace(rep, '');
	    break;
	case 'check':
	    return new RegExp('\\b' + c1 + '\\b').test(o.className);
	    break;
    }
}

function findPosX(obj) {
    var curLeft = 0;
    if (obj.offsetParent) {
	do {
	    curLeft += obj.offsetLeft;
	} while (obj = obj.offsetParent);
    } else if (obj.x) {
	curLeft += obj.x;
    }
    return curLeft;

}

function createXMLHttpRequest() {
    if (window.ActiveXObject) {
	return new ActiveXObject("Microsoft.XMLHTTP");
    } else if (window.XMLHttpRequest) {
	return new XMLHttpRequest();
    }
}
