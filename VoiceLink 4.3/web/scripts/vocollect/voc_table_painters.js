/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Painter functions for epp
 * tables.
 * 
 * @author - jgeisler
 */

/**
 * Main Painter object.
 */
var painters = {
    /**
     * All painter functions run through this code. If the function is defined,
     * it is called with the object and column data passed in.
     */
    run_me : function(functionName, obj, column) {
	if (this[functionName] == undefined) {
	    return undefined;
	} else {
	    return this[functionName](obj, column);
	}
    },
    /**
     * Helpful DOM object builders.
     */
    builders : {
	/**
	 * Builds an HTML link.
	 * 
	 * @param vars -
	 *                object with id and href
	 * @param text -
	 *                the link text
	 */
	link : function(vars, text) {
	    var linkText = "<a ";
	    for (element in vars) {
		linkText += element + "=\"" + vars[element] + "\" ";
	    }
	    linkText += ">" + text + "</a>";
	    return linkText;
	}
    },
    /*
     * 
     * PAINTER FUNCTIONS
     * 
     */

    displayFileName : function(obj) {
	if (!obj.displayName) {
	    return "";
	}
	var action = 'view.action?fileName=' + obj.fileName;
	var clazz = '';

	if ((obj.fileName.toUpperCase()).indexOf("VOCOLLECT.ERR") >= 0) {
	    clazz = 'error';
	}

	return "<a id='" + obj.displayName + "' href=\"javascript:viewLog('"
			+ action + "')\"" + " class='" + clazz + "'>" + obj.displayName
			+ "</a>";

    },
    displayLogSize : function(obj) {
	if (!obj.actualFileSize) {
	    return "";
	}

	var returnObj = null;
	if (Number(obj.actualFileSize) > 1048576) {
	    returnObj = Math.round(obj.actualFileSize / 1048576) + ' '
		    + itext('logs.view.column.size.suffix.MB');
	} else if (Number(obj.actualFileSize) >= 1024) {
	    returnObj = Math.round(obj.actualFileSize / 1024) + ' '
		    + itext('logs.view.column.size.suffix.KB');
	} else {
	    returnObj = obj.actualFileSize + ' '
		    + itext('logs.view.column.size.suffix.bytes');
	}
	return returnObj;
    },
    displayUser : function(obj) {
	if (obj.name) {
	    return this.builders.link({
		id : obj.name,
		href : globals.BASE + "/admin/user/view.action?userId="
			+ obj.id
	    }, obj.name);
	} else {
	    return "";
	}
    },
    displayLastLoginTime : function(obj) {
	if (obj.lastLoginTime == null)
	    return "";
	else
	    return obj.lastLoginTime;
    },
    displayStatus : function(val) {
	if (val.isEnabled)
	    return itext('user.status.true');
	else
	    return itext('user.status.false');
    },
    displayToolTips : function(val) {
	if (val.enabled)
	    return itext('user.status.true');
	else
	    return itext('user.status.false');
    },
    showUserCount : function(obj) {
	if (obj.userCount) {
	    return obj.userCount;
	} else {
	    return "0";
	}
    },
    displayEmail : function(obj) {
	return this.builders.link({
	    id : obj.name,
	    href : "mailto:" + obj.emailAddress
	}, obj.emailAddress);
    },
    displayRoles : function(obj) {
	if (obj.roles) {
	    return map(partial(this.formatRole, obj.id), obj.roles);
	} else {
	    return " ";
	}
    },
    formatRole : function(id, role) {
	if (role.isAdministrative) {
	    return role.name + " ";
	} else {
	    return painters.builders.link({
		id : "id-" + id + "_" + role.name,
		href : globals.BASE + "/admin/role/view.action?roleId="
			+ role.id
	    }, role.name);
	}
    },
    displayDescription : function(obj) {
	if (obj.description == "role.description.readOnly") {
	    return itext('role.description.readOnly');
	} else if (obj.description == "role.description.user") {
	    return itext('role.description.user');
	} else {
	    return obj.description;
	}
    },
    displayName : function(obj) {
	if (!obj.name) {
	    return "";
	} else if (!obj.isAdministrative) {
	    return this.builders.link({
		id : obj.name,
		href : globals.BASE + "/admin/role/view.action?roleId="
			+ obj.id
	    }, obj.name);
	} else {
	    return obj.name;
	}
    },
    displaySite : function(obj) {
	if (!obj.name) {
	    return "";
	} else if (obj.id)
	    return this.builders.link({
		id : obj.name,
		href : globals.BASE + "/admin/site/view.action?siteId="
			+ obj.id
	    }, obj.name);
    },
    displayNotificationSite : function(obj) {
	if (obj.site.id) {
	    return this.builders.link({
		'id' : 'id-' + obj.id + '_' + obj.site.name,
		'href' : globals.BASE + '/admin/site/view.action?siteId='
			+ obj.site.id
	    }, obj.site.name);
	} else {
	    return obj.site.name;
	}
    },
    displayScheduleName : function(obj) {
	if (!obj.displayName) {
	    return "";
	}
	return this.builders.link({
	    'id' : obj.displayName,
	    'href' : globals.BASE + '/admin/schedule/view.action?jobId='
		    + obj.id
	}, obj.displayName);
    },
    displayScheduleStatus : function(obj) {
	if (obj.enabled)
	    return itext('schedule.status.true');
	else
	    return itext('schedule.status.false');
    },
    displayDisableWarning : function(obj) {
	var enabledValue = obj.value;
	if (enabledValue != 'true') {
	    warningDiv.style.display = 'block';
	    warningDiv.style.visibility = 'visible';
	} else {
	    warningDiv.style.display = 'none';
	    warningDiv.style.visibility = 'hidden';
	}
    },
    displaySiteCount : function(obj) {
	if (obj.tagId) {
	    return this.builders.link({
		'id' : obj.tagId,
		'href' : globals.BASE
			+ '/admin/notification/list.action?currentSiteID='
			+ obj.tagId + '&highlightByDefault=true'
	    }, obj.message);
	} else {
	    return obj.message;
	}
    },
    displayExternalJob : function(obj) {
    	return painters.builders.link({
    		'href' : globals.PAGE_CONTEXT + '/externalJob/view.action?externalJobId='
    				+ obj.id
    	}, obj.name);
    },
    displayAlert : function(obj) {
    	return painters.builders.link({
    		'href' : globals.PAGE_CONTEXT + '/alerts/view.action?alertId='
    				+ obj.id
    	}, obj.alertName);
    },
    displayChart : function(obj) {
    	return painters.builders.link({
    		'href' : globals.PAGE_CONTEXT + '/charts/view.action?chartId='
    				+ obj.id
    	}, obj.name);
    },
    displayDashboard : function(obj) {
    	return painters.builders.link({
    		'href' : globals.PAGE_CONTEXT + '/dashboards/view.action?dashboardId='
    				+ obj.id
    	}, obj.name);
    },
    displayDashboardIsDefault : function(obj) {
    	if (obj.isDefault) {
    		return itext('message.true');
    	} else {
    		return itext('message.false');
    	}
    }
};
