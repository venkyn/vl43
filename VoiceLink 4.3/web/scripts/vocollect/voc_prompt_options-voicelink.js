/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

function populateOptions(index, newPrompt) {

    for ( var i = 0; i <= promptItemArray.length - 1; i++) {
	if (promptItemArray[i].id == _promptItemArray[index].promptId) {
	    Label1 = promptItemArray[i].optionTitle1;
	    Label2 = promptItemArray[i].optionTitle2;
	    Label3 = promptItemArray[i].optionTitle3;

	    // Replace brackets with curly brackets
	    Option1 = promptItemArray[i].optionDefault1.replaceAll('[]', '{}');
	    Option2 = promptItemArray[i].optionDefault2.replaceAll('[]', '{}');
	    Option3 = promptItemArray[i].optionDefault3.replaceAll('[]', '{}');
	}
    }
    LabelCheck = chk_title;
    LabelTotals = totals_title;
    LabelChars = chars_title;

    /* Items Remaining Div */
    _promptItemArray[index].optionDiv = DIV({
	'id' : 'ItemsRemaining' + index,
	'class' : 'promptItemOptions'
    });
    addElementClass(_promptItemArray[index].optionDiv, HIDDEN_OPTIONS_CLASS);

    itemsRemainingTitle = SPAN({
	'class' : 'optionsTitle2'
    });

    if (newPrompt) {
	itemsRemainingTitle.innerHTML = _promptItemArray[index].name;
    } else {
	itemsRemainingTitle.innerHTML = defaultPromptArray[index].name;
    }

    appendChildNodes(_promptItemArray[index].optionDiv, itemsRemainingTitle);
    appendChildNodes($('itemOptions'), _promptItemArray[index].optionDiv);

    _currentPosition++;

    // Get Object
    curObj = _promptItemArray[_currentPosition];

    // Content Div
    curObj.innerDiv = DIV({
	'style' : 'position:relative'
    });
    appendChildNodes(curObj.optionDiv, curObj.innerDiv);

    // First Textbox Div
    curObj.optionsDiv[0] = DIV({
	'id' : 'divzero' + _currentPosition,
	'class' : 'optionsInput'
    });
    appendChildNodes(curObj.innerDiv, curObj.optionsDiv[0]);

    Lbl = SPAN({
	'style' : 'font-weight:bold'
    });
    Lbl.innerHTML = Label1 + '<br>';
    appendChildNodes(curObj.optionsDiv[0], Lbl);

    // First Textbox
    curObj.options[0] = INPUT({
	'type' : 'text',
	'id' : 'txt_zero' + _currentPosition
    });
    curObj.options[0].name = 'definitions[' + _currentPosition
	    + '].promptValue1';
    curObj.options[0].style.width = 300;
    curObj.options[0].maxLength = 100;
    addButtonActions(curObj.options[0]);
    if (_readOnly) {
	curObj.options[0].disabled = true;
    }
    appendChildNodes(curObj.optionsDiv[0], curObj.options[0]);

    // Second Textbox Div
    curObj.optionsDiv[1] = DIV({
	'id' : 'divone' + _currentPosition,
	'class' : 'optionsInput'
    });
    appendChildNodes(curObj.innerDiv, curObj.optionsDiv[1]);

    Lbl = SPAN({
	'style' : 'font-weight:bold'
    });
    Lbl.innerHTML = Label2 + '<br>';
    appendChildNodes(curObj.optionsDiv[1], Lbl);

    // Second Textbox
    curObj.options[1] = INPUT({
	'type' : 'text',
	'id' : 'txt_one' + _currentPosition
    });
    curObj.options[1].name = 'definitions[' + _currentPosition
	    + '].promptValue2';
    curObj.options[1].style.width = 300;
    curObj.options[1].maxLength = 100;
    addButtonActions(curObj.options[1]);
    if (_readOnly) {
	curObj.options[1].disabled = true;
    }
    appendChildNodes(curObj.optionsDiv[1], curObj.options[1]);

    // Third Textbox Div
    curObj.optionsDiv[2] = DIV({
	'id' : 'divnotone' + _currentPosition,
	'class' : 'optionsInput'
    });
    appendChildNodes(curObj.innerDiv, curObj.optionsDiv[2]);

    Lbl = SPAN({
	'style' : 'font-weight:bold'
    });
    Lbl.innerHTML = Label3 + '<br>';
    appendChildNodes(curObj.optionsDiv[2], Lbl);

    // Third Textbox
    curObj.options[2] = INPUT({
	'type' : 'text',
	'id' : 'txt_notone' + _currentPosition
    });
    curObj.options[2].name = 'definitions[' + _currentPosition
	    + '].promptValue3';
    curObj.options[2].style.width = 300;
    curObj.options[2].maxLength = 100;
    addButtonActions(curObj.options[2]);
    if (_readOnly) {
	curObj.options[2].disabled = true;
    }
    appendChildNodes(curObj.optionsDiv[2], curObj.options[2]);

    // Right Options Div
    curObj.rightOptions = DIV({
	'id' : 'rightOptions' + _currentPosition,
	'class' : 'rightPromptOptions'
    });
    appendChildNodes(curObj.innerDiv, curObj.rightOptions);

    Lbl = SPAN({
	'style' : 'font-weight:bold'
    });
    Lbl.innerHTML = LabelCheck + '<br>';
    appendChildNodes(curObj.rightOptions, Lbl);

    // Speak check box
    curObj.chk_speak = INPUT({
	'type' : 'checkbox',
	'id' : 'chk_speak' + _currentPosition,
	'class' : 'checkbox',
	'style' : 'margin-top:5px;margin-bottom:10px;'
    });
    curObj.chk_speak.name = 'definitions[' + _currentPosition
	    + '].allowSpeakingPhonetically';
    curObj.chk_speak.value = 'True';
    addButtonActions(curObj.chk_speak);
    if (_readOnly) {
	curObj.chk_speak.disabled = true;
    }
    appendChildNodes(curObj.rightOptions, curObj.chk_speak);

    // Totals Dropdown div
    curObj.div_totals = DIV({
	'id' : 'divtotals' + _currentPosition,
	'class' : 'optionsInput'
    });
    appendChildNodes(curObj.rightOptions, curObj.div_totals);

    Lbl = SPAN({
	'style' : 'font-weight:bold'
    });
    Lbl.innerHTML = LabelTotals + '<br>';
    appendChildNodes(curObj.div_totals, Lbl);

    // Totals Dropdown
    curObj.select_totals = SELECT({
	'id' : 'select_totals' + _currentPosition
    });
    curObj.select_totals.name = 'definitions[' + _currentPosition
	    + '].speakTotalsIndicator';
    addButtonActions(curObj.select_totals);
    if (_readOnly) {
	curObj.select_totals.disabled = true;
    }
    appendChildNodes(curObj.div_totals, curObj.select_totals);

    // Chars Dropdown div
    curObj.div_chars = DIV({
	'id' : 'divchars' + _currentPosition,
	'class' : 'optionsInput'
    });
    appendChildNodes(curObj.rightOptions, curObj.div_chars);

    Lbl = SPAN({
	'style' : 'font-weight:bold'
    });
    Lbl.innerHTML = LabelChars + '<br>';
    appendChildNodes(curObj.div_chars, Lbl);

    // Chars dropdown
    curObj.select_chars = SELECT({
	'id' : 'select_chars' + _currentPosition
    });
    curObj.select_chars.name = 'definitions[' + _currentPosition
	    + '].speakCharactersIndicator';
    connect(curObj.select_chars, "onchange", partial(showNumChars, curObj));
    addButtonActions(curObj.select_chars);
    if (_readOnly) {
	curObj.select_chars.disabled = true;
    }
    appendChildNodes(curObj.div_chars, curObj.select_chars);

    // Num Chars dropdown
    curObj.select_numChars = SELECT({
	'id' : 'select_numChars' + _currentPosition,
	'style' : 'visibility:hidden;margin-left:10px'
    });
    curObj.select_numChars.name = 'definitions[' + _currentPosition
	    + '].numberOfCharacters';
    addButtonActions(curObj.select_numChars);
    if (_readOnly) {
	curObj.select_numChars.disabled = true;
    }
    appendChildNodes(curObj.div_chars, curObj.select_numChars);

    // Append Main Div to page
    appendChildNodes(curObj.optionDiv, curObj.mainDiv);

    connect(curObj.options[0].id, "onblur", updatePreview);
    connect(curObj.options[1].id, "onblur", updatePreview);
    connect(curObj.options[2].id, "onblur", updatePreview);

    if (!newPrompt) {
	if (defaultPromptArray[_currentPosition].promptValue1 != '') {
	    curObj.options[0].value = defaultPromptArray[_currentPosition].promptValue1;
	}
	if (defaultPromptArray[_currentPosition].promptValue2 != '') {
	    curObj.options[1].value = defaultPromptArray[_currentPosition].promptValue2;
	}
	if (defaultPromptArray[_currentPosition].promptValue3 != '') {
	    curObj.options[2].value = defaultPromptArray[_currentPosition].promptValue3;
	}
    }
    for (i = 0; i <= promptItemArray.length - 1; i++) {
	if (newPrompt) {
	    checkVal = $(PROMPT_SELECTOR).options[$(PROMPT_SELECTOR).selectedIndex].value;
	} else {
	    checkVal = defaultPromptArray[_currentPosition].promptId;
	}
	if (promptItemArray[i].id == checkVal) {
	    if (!(promptItemArray[i].showOptions) || lang) {
		removeElement(curObj.rightOptions.id);
	    } else {
		if (!newPrompt) {
		    if (defaultPromptArray[_currentPosition].allowSpeakingPhonetically) {
			curObj.chk_speak.checked = true;
		    }
		}
		if (promptItemArray[i].fieldType == 'Numeric') {
		    removeElement(curObj.div_chars.id);
		    for ( var j = 0; j <= selTotalArray.length - 1; j++) {
			curObj.select_totals.options[j] = new Option(
				selTotalArray[j], j);
			if (!newPrompt) {
			    if (defaultPromptArray[_currentPosition].speakTotalsIndicator == j) {
				curObj.select_totals.options[j].selected = true;
			    }
			}
		    }
		} else {
		    removeElement(curObj.div_totals.id);
		    for ( var k = 0; k <= selCharArray.length - 1; k++) {
			curObj.select_chars.options[k] = new Option(
				selCharArray[k], k);
			if (!newPrompt) {
			    if (defaultPromptArray[_currentPosition].speakCharactersIndicator == selCharArray[k]) {
				curObj.select_chars.options[k].selected = true;
			    }
			}
		    }
		    if (curObj.select_chars.selectedIndex != 0) {
			curObj.select_numChars.style.visibility = "visible";
		    }
		    for ( var m = 0; m <= selNumCharArray.length - 1; m++) {
			curObj.select_numChars.options[m] = new Option(
				selNumCharArray[m], m + 1);
			if (!newPrompt) {
			    if (defaultPromptArray[_currentPosition].numberOfCharacters == selNumCharArray[m]) {
				curObj.select_numChars.options[m].selected = true;
			    }
			}
		    }
		}
	    }

	    if (promptItemArray[i].editBoxCount < 3) {
		curObj.options[2].value = '';
		removeElement(curObj.optionsDiv[2].id);
	    }
	    if (promptItemArray[i].editBoxCount < 2) {
		curObj.options[1].value = '';
		removeElement(curObj.optionsDiv[1].id);
		if (promptItemArray[i].showOptions) {
		    var browser = navigator.appName;
		    if (browser == "Microsoft Internet Explorer") {
			curObj.optionDiv.style.height = 120;
		    } else {
			curObj.optionDiv.style.height = 80;
		    }
		}
	    }
	    if (newPrompt) {
		curObj.options[0].value = Option1;
		if (promptItemArray[i].editBoxCount > 1) {
		    curObj.options[1].value = Option2;
		    if (promptItemArray[i].editBoxCount > 2) {
			curObj.options[2].value = Option3;
		    }
		}
	    }
	}
    }

    updatePreview();
}

String.prototype.replaceAll = function(strTarget, strSubString) {
    var strText = this;
    var intIndexOfMatch = strText.indexOf(strTarget);

    while (intIndexOfMatch != -1) {
	strText = strText.replace(strTarget, strSubString);
	intIndexOfMatch = strText.indexOf(strTarget);
    }

    return (strText);
};

function updatePreview() {
    string_zero = '';
    string_one = '';
    string_notone = '';

    if (navigator.appName == 'Microsoft Internet Explorer') {
	wid = getTotalPromptWidth();

	wid2 = YAHOO.util.Dom.getStyle('promptSelectorDiv', 'width');
	wid2 = parseInt(wid2.substr(0, wid2.length - 2));

	if (wid > (wid2 - 28)) {
	    $('promptSelectorDiv').style.height = 50;
	} else {
	    $('promptSelectorDiv').style.height = 35;
	}
    }

    for ( var i = 0; i <= _promptItemArray.length - 1; i++) {
	for ( var j = 0; j <= promptItemArray.length - 1; j++) {
	    if (_promptItemArray[i].promptId == promptItemArray[j].id) {
		boxCount = promptItemArray[j].editBoxCount;
	    }
	}
	if (_promptItemArray[i].options[0].value != '') {
	    if (boxCount == 1) {
		string_one += _promptItemArray[i].options[0].value.replaceAll(
			'{}', '0');
		string_notone += _promptItemArray[i].options[0].value
			.replaceAll('{}', '0');
	    }
	    string_zero += _promptItemArray[i].options[0].value.replaceAll(
		    '{}', '0');
	}
	if (_promptItemArray[i].options[1].value != '') {
	    if (boxCount == 2) {
		string_notone += _promptItemArray[i].options[1].value
			.replaceAll('{}', '1');
	    }
	    string_one += _promptItemArray[i].options[1].value.replaceAll('{}',
		    '1');
	}
	if (_promptItemArray[i].options[2].value != '') {
	    string_notone += _promptItemArray[i].options[2].value.replaceAll(
		    '{}', '2');
	}
    }
    $("preview_zero").innerHTML = string_zero;
    $("preview_one").innerHTML = string_one;
    $("preview_notone").innerHTML = string_notone;
}

function addButtonActions(formElement) {
    var anchorElement = $("${submitId?default('${formId}.submit1')}");

    if (formElement.type != "hidden" && formElement.type != "submit"
	    && form_name != "create") {
	if (formElement.type == "checkbox" || formElement.type == "radio") {
	    connect(formElement, "onkeypress", anchorElement,
		    enableSubmitButton);
	    connect(formElement, "onclick", anchorElement, enableSubmitButton);
	} else {
	    connect(formElement, "onkeydown", anchorElement, enableSubmitButton);
	    connect(formElement, "onkeypress", anchorElement,
		    enableSubmitButton);
	    connect(formElement, "onchange", anchorElement, enableSubmitButton);
	}
    }
}

function showNumChars(promptObject) {
    if (promptObject.select_chars.selectedIndex != 0) {
	promptObject.select_numChars.style.visibility = "visible";
    } else {
	promptObject.select_numChars.style.visibility = "hidden";
    }
}

function convertChars() {
    for ( var i = 0; i <= _promptItemArray.length - 1; i++) {
	_promptItemArray[i].options[0].value = _promptItemArray[i].options[0].value
		.replaceAll('"', ' ');
	_promptItemArray[i].options[1].value = _promptItemArray[i].options[1].value
		.replaceAll('"', ' ');
	_promptItemArray[i].options[2].value = _promptItemArray[i].options[2].value
		.replaceAll('"', ' ');

	_promptItemArray[i].options[0].value = _promptItemArray[i].options[0].value
		.replaceAll('\\', ' ');
	_promptItemArray[i].options[1].value = _promptItemArray[i].options[1].value
		.replaceAll('\\', ' ');
	_promptItemArray[i].options[2].value = _promptItemArray[i].options[2].value
		.replaceAll('\\', ' ');
    }
}
