/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Printable View Module for
 * Table Component 2.0
 */

PrintableViewModule = function(table) {
    this.table = table;

};

PrintableViewModule.prototype = new MenuModule();

PrintableViewModule.prototype.init = function() {

};

// Open a print window for printable version
PrintableViewModule.prototype.showPrintWindow = function() {

    // Get the rows that are selected
    var rows = this.table.grid.getSelectedRows();

    // Set up the printable view error dialog
    var printErrorDialog = $j('<div></div>').html(
	    this.localizedStrings.printable_error_message).dialog({
	autoOpen : false,
	title : this.localizedStrings.printable_error_title,
	modal : true
    });

    // TODO this is a static 1000 for now.
    if (rows.length >= 1 && rows.length <= 1000) {
	// Open the print window
	printWindow = window
		.open(
			globals.BASE + '/showPrintWindow.action',
			'_blank',
			'channelmode=no,directories=no,fullscreen=no,height=400,left=20,'
				+ 'location=no,menubar=yes,resizable=yes,scrollbars=yes,status=no,'
				+ 'titlebar=yes,toolbar=no,top=20,width=600');
	var tableStructure = this.buildPrintTable();
	// Wait 1 second for the page to load the DOM and javascript
	$j(function() {
	    setTimeout(function() {
		printWindow.setTable(tableStructure);
	    }, 1000);
	});
    } else {
	// Show the dialog if we have 0 or more than 1000 rows.
	printErrorDialog.dialog('open');
    }
};
// Build the printable view table
PrintableViewModule.prototype.buildPrintTable = function() {
    // Define the variables that are used for the printable table
    var rows = this.table.getSelectedObjects().sort(function(a, b) {
	return a.index - b.index;
    });
    var numRows = rows.length;
    var columns = this.table.visible_columns;
    var numColumns = columns.length;
    var printTable = document.createElement('table');
    var thead = document.createElement('thead');
    printTable.appendChild(thead);
    // Build the header out of each visible column
    for ( var i = 0; i < numColumns; i++) {
	var column = columns[i];
	var th = document.createElement('th');
	thead.appendChild(th);
	addText(th, column.name);
    }
    var tbody = document.createElement('tbody');
    printTable.appendChild(tbody);

    // Build the row content of the table
    for ( var i = 0; i < numRows; i++) {
	var row = rows[i];
	var tr = document.createElement('tr');
	tbody.appendChild(tr);

	// Build the individual cells
	for ( var j = 0; j < numColumns; j++) {
	    var column = columns[j];
	    var painter = null;
	    var td = document.createElement('td');
	    tr.appendChild(td);

	    // Declare colText
	    var colText = null;

	    // Check on the painter
	    if (column.painterFunction) {
		painter = column.painterFunction;
	    }

	    // Text is painted (url, translation)
			if (painter) {
				var paintedText = new Array();
				paintedText = painters.run_me(painter, row, column);
				// This is in case there is a painter defined that doesn't exist
				if (paintedText && paintedText.toString().indexOf('<a ') != -1) {
					colText = $j(paintedText).text();
					if (!colText) {
						colText = eval("row." + column.field);
					}
				} else {
					// No links present just display the text
					colText = paintedText;
				}
			} else {
				// Text is un-painted
				colText = eval("row." + column.field);
			}
			
	    
	    if (!colText && colText !== 0) {
		colText = "\u00A0";
	    }
	    addText(td, colText);
	}
    }
    // Return the HTML value of the table
    return printTable;
};
