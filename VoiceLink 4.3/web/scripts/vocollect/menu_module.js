/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Menu Module for Table
 * Component 2.0
 */
MenuModule = function(tableId) {
    this.restoreMsgShowing = false;
    this.tableId = tableId;
    this.localizedStrings = {};
};

MenuModule.prototype = {
    showInternalMenu : function() {
	if (this.restoreMsgShowing) {
	    return;
	}

	/* TODO: Stop updating the table */

	if ((typeof menuId) === 'ColumnMenu') {
	    this.lockColHeaders();
	}

	// this.setMenuHeight();
	// $(this.menuDivId).style.visibility = 'visible';
	this.$j("Div").find('#' + this.menuDivId).show('blind', {}, 600);
	this.$j("Div").find('#' + this.menuDivId + "Link").addClass("intMenuLinkSelected");

    },

    lockColHeaders : function() {
	console.log("UNIMPLEMENTED menu_module.js METHOD: lockColHeaders");
    },

    hideMenu : function() {
	var returnVal = false;
	if (this.$j("Div").find('#' + this.menuDivId).css('display') == "block") {
	    returnVal = true;
	}

	if (this.$j('.fullMenu:visible').length > 0) {
	    this.$j('.fullMenu:visible').hide('blind', {}, 600);
	}
	this.$j('.intMenuLinkSelected').removeClass('intMenuLinkSelected');
	return returnVal;
    },

    toggleMenu : function() {
	if (this.hideMenu()) {
	    return;
	}
	this.showInternalMenu(this.menuDivId);
    },

    setLocalizedStrings : function(_strings) {
	this.localizedStrings = _strings;
    },

    $j : function(selector) {
	return $j('#' + this.table.id + 'Div ' + selector);
    },

    ui_get : function(name) {
	return this.table.ui_constants.get(name);
    }
};
