/**
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 *
 * File is responsible to completing all asychronous setup activities for the table components.
 * Once all setup is complete, the event 'ajaxSetupComplete' is fired.
 *
 * Retrieved data is available through the asyncDataHolder.
 *
 * @author - jgeisler
 *
 */

var events = {
    // Fired when a table component exists on a page
    TABLE_COMPONENT_PAGE : "TC_COMPONENT_PAGE",
    // Fired when the asynchronous items are finished downloading
    PRE_REQS_COMPLETE : "ajaxSetupComplete",
    // Fired when table components are safe to begin loading
    GO_TABLE_COMPONENT : "goTableComponent",
    // Fired when a table component finishes loading
    TABLE_COMPONENT_LOADED : 'tableComponentLoaded',
    // Fired when all tables have finished loading
    ALL_TABLES_LOADED : 'allTablesLoaded',
    // Fired when the selection changes in a table component
    SELECTION_CHANGE : 'selectionChange'
};

var master_control = {
    /**
     * Set up the initial document listeners:
     *  - whether a table component exists on the page
     *  - completion of page prerequisites
     *  - completion of a table component loading into the page
     */
    setup_listeners : function() {
	$j(document).bind(events.TABLE_COMPONENT_PAGE,
		this.handlers.table_components_exist);
	$j(document).bind(events.PRE_REQS_COMPLETE,
		this.handlers.pre_reqs_complete);
	$j(document).bind(events.TABLE_COMPONENT_LOADED,
		this.handlers.table_ready);
    },
    /**
     * Map of registered tables, and their loading status (false if not completed, true if loading completed)
     */
    tables : {},
    /**
     * Main page event handlers.
     */
    handlers : {
	registered_table : false,
	/**
	 * Handles when pre-reqs are setup.
	 * Fires the event to tell table components to load
	 */
	pre_reqs_complete : function() {
	    $j(document).trigger(events.GO_TABLE_COMPONENT);
	},
	/**
	 * Handles when a table components exist.
	 * Adds the table to the list of tables.
	 * Starts the asynchronous stuff just once.
	 */
	table_components_exist : function(e, tableName) {
	    master_control.tables[tableName] = false;

	    // Fire init stuff only once even if there are multiple tables
	    if (!master_control.handlers.registered_table) {
		master_control.handlers.registered_table = true;
		asyncSetup.init();
	    }
	},
	/**
	 * Handles when a table component finishes loading.
	 * Updates the table map; if all registered tables have completed loading
	 * then an event is fired on the document that all tables have loaded.
	 */
	table_ready : function(e, tableName) {
	    master_control.tables[tableName] = true;

	    if (master_control.helpers.are_all_tables_ready())
		$j(document).trigger(events.ALL_TABLES_LOADED);

	}
    },
    /**
     * Master Control helpers
     */
    helpers : {
	/**
	 * Determines if all registered tables have loaded.
	 */
	are_all_tables_ready : function() {
	    var tables = master_control.tables;
	    var result = true;

	    for (idx in tables)
		result &= tables[idx];

	    return result;
	}
    }
};

/**
 * Asynchronous setup. The event work and
 * asynchronous calls necessary to get the
 * page setup for table components.
 */
var asyncSetup = {
    events : [],
    /**
     * Binds the event for when the content is ready.
     * Begins the operand info retrieval.
     * Begins the localization text retrieval.
     */
    init : function() {
	$j(document).bind("contentReady", this.eventFired);
	this.retrieveOperands();
	this.retrieveLocalizationText();
    },
    /**
     * Fires off the asynchronous call for operand data.
     */
    retrieveOperands : function() {
	var OPERAND_EVENT = "operands";
	this.newEventTrigger(OPERAND_EVENT);
	asyncObj = this;
	$j.get(globals.BASE + "/getOperands.action").success(function(data) {
	    asyncDataHolder.operandManager.set(data);
	    $j(document).trigger("contentReady", [ OPERAND_EVENT, asyncObj ]);
	});
    },
    /**
     * Fires off the asynchronous call for localization data.
     */
    retrieveLocalizationText : function() {
	var LOCALIZATION_EVENT = "localizationText";
	this.newEventTrigger(LOCALIZATION_EVENT);
	asyncObj = this;
	$j.get(globals.BASE + "/getTableLocText.action").success(
		function(data) {
		    asyncDataHolder.localizationTextManager.set(data);
		    $j(document).trigger("contentReady",
			    [ LOCALIZATION_EVENT, asyncObj ]);
		});
    },
    /**
     * Keeps track of outstanding asynchronous calls.
     */
    newEventTrigger : function(eventName) {
	this.events.push(eventName);
    },
    /**
     * Fires the 'pre-req is complete' event.
     */
    setupComplete : function() {
	$j(document).trigger(events.PRE_REQS_COMPLETE);
    },
    /**
     * Listener for when the content is ready from each asynchronous call.
     */
    eventFired : function(event, name, asyncObj) {

	var tmpEvents = [];
	var events = asyncObj.events;
	for (eventIdx in events) {
	    if (events[eventIdx] != name) {
		tmpEvents.push(events[eventIdx]);
	    }
	}

	asyncObj.events = tmpEvents;

	if (asyncObj.events.length == 0) {
	    asyncObj.setupComplete();
	}
    }
};

/**
 * Holds the asynchronous data.
 */
var asyncDataHolder = {
    /**
     * Holds the operands for filtering.
     */
    operandManager : {
	data : null,
	set : function(data) {
	    this.data = data;
	}
    },
    /**
     * Holds the localization text for the table component.
     */
    localizationTextManager : {
	keyMap : null,
	set : function(data) {
	    this.keyMap = eval('(' + data.tableKeys + ')');
	},
	getText : function(key) {
	    if (this.keyMap == null || this.keyMap[key] == undefined || this.keyMap[key] == '') {
		return key;
	    } else {
		return this.keyMap[key];
	    }
	}
    }
};

/**
 * Localization function for text in JavaScript code.
 * @param key - the string key
 * @param args - the arguments to be filled in; ordered
 * @return - returns the localized text or the key (if not found)
 */
var itext = function(key, args) {
    var text = asyncDataHolder.localizationTextManager.getText(key);

    if (args) {
	for (idx in args) {
	    text = text.replace("{" + idx + "}", args[idx]);
	}
    }

    return text;
};
// Be ready for anything
// Begin listening for events.
master_control.setup_listeners();
