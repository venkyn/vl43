/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

var _selectedIndex = -1;
var _promptItemArray = new Array();
var _currentPosition = -1;
var _draggable = true;
var _readOnly = false;
var _divIndexCount = 0;
var newPrompt = false;

var ITEM_DIV = 'itemDiv';
var PROMPT_ID = 'promptItem';
var PROMPT_CLASS = 'promptItem';
var PROMPT_SELECTOR = 'promptTypeId';
var PROMPT_SELECTOR_DIV = 'promptSelectorDiv';
var PROMPT_CONTAINER_DIV = 'promptContainerDiv';
var SELECTED_ITEM_CLASS = 'selectedPromptItem';
var HIDDEN_OPTIONS_CLASS = 'optionsDiv_hidden';
var BUFFER_WIDTH = 5;

YAHOO.util.DragDropMgr.stopPropagation = false;

function setDraggable(val) {
    _draggable = val;
}

function PromptItem(div) {
    this.div = div;
    this.dd = null;
    this.name = null;
    this.promptIdInput = null;
    //this.idInput = null;
    this.id = null;
    this.promptId = null;
    this.innerDiv = null;
    this.optionDiv = null;
    this.options = new Array();
    this.optionsDiv = new Array();
    this.rightOptions = null;
    this.chk_speak = null;
    this.div_totals = null;
    this.div_chars = null;
    this.select_totals = null;
    this.select_chars = null;
}

function getTotalPromptWidth() {
    var totalWidth = 5;
    for ( var i = 0; i < _promptItemArray.length; i++) {
	totalWidth = totalWidth + (_promptItemArray[i].div.clientWidth + 5);
    }
    return (totalWidth);
}

function addNewPromptItem() {
    var newIndex = _promptItemArray.length;
    var promptValue = $(PROMPT_SELECTOR).options[$(PROMPT_SELECTOR).selectedIndex].text;
    var promptId = $(PROMPT_SELECTOR).options[$(PROMPT_SELECTOR).selectedIndex].value;
    newPrompt = true;
    addPromptItem(newIndex, promptValue, promptId, null);
    setItemPositions();
}

function addPromptItem(position, promptValue, promptId, id) {

    var itemId = INPUT({
	'name' : 'definitions[' + position + '].promptItemId',
	'type' : 'hidden',
	'value' : promptId
    });
    //	var defId = INPUT({'name':'definitions['+position+'].tempId','type':'hidden','value':id});
    _promptItemArray[position] = new PromptItem(DIV({
	'id' : PROMPT_ID + _divIndexCount,
	'class' : PROMPT_CLASS
    }, promptValue, itemId));//, defId));
    _promptItemArray[position].name = promptValue;

    _promptItemArray[position].id = id;
    _promptItemArray[position].promptId = promptId;

    if (_draggable) {
	_promptItemArray[position].dd = new YAHOO.util.DD(
		_promptItemArray[position].div.id);
    }

    _promptItemArray[position].promptIdInput = itemId;
    //_promptItemArray[position].idInput = defId;
    setItemEvents(position);
    appendChildNodes($(ITEM_DIV), _promptItemArray[position].div);

    if (_draggable) {
	_promptItemArray[position].dd.setYConstraint(0, 0);
    }

    _divIndexCount++;
    populateOptions(position, newPrompt);
}

function removePromptItem() {
    if (_selectedIndex >= 0) {
	//Remove the item div
	removeElement(_promptItemArray[_selectedIndex].div);

	//Remove its accompanying optionDiv
	removeElement(_promptItemArray[_selectedIndex].optionDiv);

	//Adjust element IDs
	removeElemId(_selectedIndex);

	_promptItemArray.splice(_selectedIndex, 1);

	setItemPositions();

	for ( var i = 0; i < _promptItemArray.length; i++) {
	    setItemEvents(i);
	}
    }

    _selectedIndex = -1;
    updatePreview();
}

function promptItemMouseDown(index) {
    //Change the mouse cursor
    if (_draggable) {
	_promptItemArray[index].div.style.cursor = "w-resize";
    }
    for ( var i = 0; i < _promptItemArray.length; i++) {
	_promptItemArray[i].div.style.zIndex = 1000 + i;
    }

    _promptItemArray[index].div.style.zIndex = 1000 + _promptItemArray.length;

    //Add the selected class to the newly selected item
    if (index != _selectedIndex) {
	addElementClass(_promptItemArray[index].div, SELECTED_ITEM_CLASS);
	removeElementClass(_promptItemArray[index].optionDiv,
		HIDDEN_OPTIONS_CLASS);
	//Remove the selected class from the previously selected item
	if (_selectedIndex >= 0) {
	    removeElementClass(_promptItemArray[_selectedIndex].div,
		    SELECTED_ITEM_CLASS);
	    addElementClass(_promptItemArray[_selectedIndex].optionDiv,
		    HIDDEN_OPTIONS_CLASS);
	}
	// change prompt item DIV
	_selectedIndex = index;
    }
    updatePreview();
}

function setItemPositions() {
    var totalWidth = getTotalPromptWidth();
    var leftVal = 5;

    $('itemDiv').style.width = totalWidth + 5;

    for ( var i = 0; i < _promptItemArray.length; i++) {
	_promptItemArray[i].div.style.left = leftVal + "px";
	if (_draggable) {
	    _promptItemArray[i].dd.setInitPosition(
		    _promptItemArray[i].div.style.left,
		    _promptItemArray[i].div.style.top);
	    _promptItemArray[i].dd.setXConstraint(leftVal - BUFFER_WIDTH,
		    totalWidth - leftVal - _promptItemArray[i].div.clientWidth
			    - BUFFER_WIDTH);
	    _promptItemArray[i].dd.resetConstraints();
	}
	leftVal = leftVal
		+ (_promptItemArray[i].div.clientWidth + BUFFER_WIDTH);
	//_promptItemArray[i].idInput.name = 'definitions['+i+'].tempId';
	_promptItemArray[i].promptIdInput.name = 'definitions[' + i
		+ '].promptItemId';
    }
}

function promptItemDragging(index) {
    var draggedLeft = parseInt(_promptItemArray[index].div.style.left);
    var draggedRight = parseInt(_promptItemArray[index].div.style.left)
	    + _promptItemArray[index].div.clientWidth;
    var leftMid = 0;
    var rightMid = getTotalPromptWidth();

    if (index - 1 >= 0) {
	leftMid = (parseInt(_promptItemArray[index - 1].div.style.left) + (_promptItemArray[index - 1].div.clientWidth / 2));
    }
    if (index + 1 < _promptItemArray.length) {
	rightMid = (parseInt(_promptItemArray[index + 1].div.style.left) + (_promptItemArray[index + 1].div.clientWidth / 2));
    }
    if (draggedLeft < leftMid) {

	//Switch the left item
	swapPromptItems(index, index - 1);

	//Set the position of the switched item
	_promptItemArray[index].div.style.left = parseInt(_promptItemArray[index].div.style.left)
		+ BUFFER_WIDTH + _promptItemArray[index - 1].div.clientWidth + "px";

	setItemEvents(index);
	setItemEvents(index - 1);
	_selectedIndex = _selectedIndex - 1;
    } else if (draggedRight > rightMid) {
	//Switch the right item
	swapPromptItems(index, index + 1);

	//Set the position of the switched item
	_promptItemArray[index].div.style.left = parseInt(_promptItemArray[index].div.style.left)
		- BUFFER_WIDTH - _promptItemArray[index + 1].div.clientWidth + "px";

	setItemEvents(index);
	setItemEvents(index + 1);
	_selectedIndex = _selectedIndex + 1;
    }
}

function swapPromptItems(index, index2) {
    if (form_name != "create") {
	var anchorElement = $("${submitId?default('${formId}.submit1')}");
	enableSubmitButton(anchorElement);
    }
    var tempItem = _promptItemArray[index];
    _promptItemArray[index] = _promptItemArray[index2];
    _promptItemArray[index2] = tempItem;

    var tempName = _promptItemArray[index].promptIdInput.name;
    _promptItemArray[index].promptIdInput.name = _promptItemArray[index2].promptIdInput.name;
    _promptItemArray[index2].promptIdInput.name = tempName;

    //swap element IDs
    obj1 = _promptItemArray[index];
    obj2 = _promptItemArray[index2];

    swapElemIds(obj1, obj2);

    updatePreview();
}

function removeElemId(removeIndex) {

    for ( var i = removeIndex; i <= _promptItemArray.length - 1; i++) {
	obj = _promptItemArray[i];
	obj.optionDiv.id = 'ItemsRemaining' + (i - 1);
	obj.options[0].id = 'txt_zero' + (i - 1);
	obj.options[1].id = 'txt_one' + (i - 1);
	obj.options[2].id = 'txt_notone' + (i - 1);
	obj.options[0].name = 'definitions[' + (i - 1) + '].promptValue1';
	obj.options[1].name = 'definitions[' + (i - 1) + '].promptValue2';
	obj.options[2].name = 'definitions[' + (i - 1) + '].promptValue3';
	obj.optionsDiv[0].id = 'divzero' + (i - 1);
	obj.optionsDiv[1].id = 'divone' + (i - 1);
	obj.optionsDiv[2].id = 'divnotone' + (i - 1);
	obj.rightOptions.id = 'rightOptions' + (i - 1);
	obj.chk_speak.id = 'chk_speak' + (i - 1);
	obj.chk_speak.name = 'definitions[' + (i - 1)
		+ '].allowSpeakingPhonetically';
	obj.div_totals.id = 'divTotals' + (i - 1);
	obj.div_chars.id = 'divChars' + (i - 1);
	obj.select_chars.id = 'select_chars' + (i - 1);
	obj.select_chars.name = 'definitions[' + (i - 1)
		+ '].speakCharactersIndicator';
	obj.select_numChars.id = 'select_numChars' + (i - 1);
	obj.select_numChars.name = 'definitions[' + (i - 1)
		+ '].numberOfCharacters';
	obj.select_totals.id = 'select_totals' + (i - 1);
	obj.select_totals.name = 'definitions[' + (i - 1)
		+ '].speakTotalsIndicator';
    }

    _currentPosition--;
}

function swapElemIds(obj1, obj2) {
    tmpoptionDiv = obj1.optionDiv.id;
    tmpoptions0 = obj1.options[0].id;
    tmpoptions1 = obj1.options[1].id;
    tmpoptions2 = obj1.options[2].id;
    tmpoptionsDiv0 = obj1.optionsDiv[0].id;
    tmpoptionsDiv1 = obj1.optionsDiv[1].id;
    tmpoptionsDiv2 = obj1.optionsDiv[2].id;
    tmpoptionsName1 = obj1.options[0].name;
    tmpoptionsName2 = obj1.options[1].name;
    tmpoptionsName3 = obj1.options[2].name;
    tmprightOptions = obj1.rightOptions.id;
    tmpchk_speak = obj1.chk_speak.id;
    tmpchk_speakName = obj1.chk_speak.name;
    tmpdiv_totals = obj1.div_totals.id;
    tmpdiv_chars = obj1.div_chars.id;
    tmpselect_totals = obj1.select_totals.id;
    tmpselect_totalsName = obj1.select_totals.name;
    tmpselect_chars = obj1.select_chars.id;
    tmpselect_charsName = obj1.select_chars.name;
    tmpselect_numChars = obj1.select_numChars.id;
    tmpselect_numCharsName = obj1.select_numChars.name;

    obj1.optionDiv.id = obj2.optionDiv.id;
    obj1.options[0].id = obj2.options[0].id;
    obj1.options[1].id = obj2.options[1].id;
    obj1.options[2].id = obj2.options[2].id;
    obj1.optionsDiv[0].id = obj2.optionsDiv[0].id;
    obj1.optionsDiv[1].id = obj2.optionsDiv[1].id;
    obj1.optionsDiv[2].id = obj2.optionsDiv[2].id;
    obj1.options[0].name = obj2.options[0].name;
    obj1.options[1].name = obj2.options[1].name;
    obj1.options[2].name = obj2.options[2].name;
    obj1.rightOptions.id = obj2.rightOptions.id;
    obj1.chk_speak.id = obj2.chk_speak.id;
    obj1.chk_speak.name = obj2.chk_speak.name;
    obj1.div_totals.id = obj2.div_totals.id;
    obj1.div_chars.id = obj2.div_chars.id;
    obj1.select_totals.id = obj2.select_totals.id;
    obj1.select_totals.name = obj2.select_totals.name;
    obj1.select_chars.id = obj2.select_chars.id;
    obj1.select_chars.name = obj2.select_chars.name;
    obj1.select_numChars.id = obj2.select_numChars.id;
    obj1.select_numChars.name = obj2.select_numChars.name;

    obj2.optionDiv.id = tmpoptionDiv;
    obj2.options[0].id = tmpoptions0;
    obj2.options[1].id = tmpoptions1;
    obj2.options[2].id = tmpoptions2;
    obj2.options[0].name = tmpoptionsName1;
    obj2.options[1].name = tmpoptionsName2;
    obj2.options[2].name = tmpoptionsName3;
    obj2.optionsDiv[0].id = tmpoptionsDiv0;
    obj2.optionsDiv[1].id = tmpoptionsDiv1;
    obj2.optionsDiv[2].id = tmpoptionsDiv2;
    obj2.rightOptions.id = tmprightOptions;
    obj2.chk_speak.id = tmpchk_speak;
    obj2.chk_speak.name = tmpchk_speakName;
    obj2.div_totals.id = tmpdiv_totals;
    obj2.div_chars.id = tmpdiv_chars;
    obj2.select_totals.id = tmpselect_totals;
    obj2.select_totals.name = tmpselect_totalsName;
    obj2.select_chars.id = tmpselect_chars;
    obj2.select_chars.name = tmpselect_charsName;
    obj2.select_numChars.id = tmpselect_numChars;
    obj2.select_numChars.name = tmpselect_numCharsName;
}

function setItemEvents(index) {
    if (_draggable) {
	_promptItemArray[index].dd.onDrag = partial(promptItemDragging, index);
	_promptItemArray[index].dd.onMouseUp = partial(promptItemMouseUp, index);
    }
    _promptItemArray[index].div.onmousedown = partial(promptItemMouseDown,
	    index);
}

function promptItemMouseUp(index) {
    var totalWidth = getTotalPromptWidth();
    var leftVal = BUFFER_WIDTH;
    if (index > 0) {
	leftVal = parseInt(_promptItemArray[index - 1].div.style.left)
		+ BUFFER_WIDTH + _promptItemArray[index - 1].div.clientWidth;
    }
    _promptItemArray[index].div.style.left = leftVal + "px";
    _promptItemArray[index].div.style.cursor = "pointer";
}
