/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 * 
 * Copy Selection Module for Table Component 2.0
 * 
 */

CopySelectionModule = function(table) {
    this.table = table;

};

CopySelectionModule.prototype = new MenuModule();

CopySelectionModule.prototype.init = function() {

};
CopySelectionModule.prototype.copyRowSelection = function() {
    // Define the variables that are used for the printable table
    var rows = this.table.getSelectedObjects().sort(function(a, b) {
	return a.index - b.index;
    });    var numRows = rows.length;
    var columns = this.table.visible_columns;
    var numColumns = columns.length;
    var copyText = '';

    // Build the row content
    for ( var i = 0; i < numRows; i++) {
	var row = rows[i];

	// Build the individual cells
	for ( var j = 0; j < numColumns; j++) {
	    var column = columns[j];
	    var painter = null;

	    // Declare elemText
	    var elemText = null;

	    // Check on the painter
	    if (column.painterFunction) {
		painter = column.painterFunction;
	    }

	    // Text is painted (url, translation)
	    if (painter) {
		var paintedText = new Array();
		paintedText = painters.run_me(painter, row, column);
		elemText = $j(paintedText).text();
		if (!elemText) {
		    elemText = eval("row." + column.field);
		}
	    } else {
		// Text is un-painted
		elemText = eval("row." + column.field);
	    }
		
	    if (!elemText && elemText !== 0) {
		elemText = "\u00A0";
	    }
	    elemText += '\t';
	    copyText += elemText;
	}
    // Since this is copy to clipboard add and return and a newline
    copyText += "\r\n";
    }

    // Call the clipboard code
    this.sendTextToClipboard(copyText);
};
// Take the rows we selected and push them to the clipboard
CopySelectionModule.prototype.sendTextToClipboard = function(text) {
    if (window.clipboardData) {
	bResult = window.clipboardData.setData("Text", text);
    } else if (window.netscape) {
	try {
	    netscape.security.PrivilegeManager
		    .enablePrivilege('UniversalXPConnect');
	} catch (e) {
	    var dialogProperties = {
		title : this.localizedStrings.copy_warning,
		body : this.localizedStrings.copy_add_instructions,
		button1 : this.localizedStrings.copy_ok_button
	    };
	    buildCustomDialog(dialogProperties, "", "");
	    return;
	}
	var clip = Components.classes['@mozilla.org/widget/clipboard;1']
		.createInstance(Components.interfaces.nsIClipboard);
	if (!clip)
	    return;
	var trans = Components.classes['@mozilla.org/widget/transferable;1']
		.createInstance(Components.interfaces.nsITransferable);
	if (!trans)
	    return;
	trans.addDataFlavor('text/unicode');
	var str = Components.classes["@mozilla.org/supports-string;1"]
		.createInstance(Components.interfaces.nsISupportsString);

	var copytext = text;
	str.data = copytext;
	trans.setTransferData("text/unicode", str, copytext.length * 2);
	var clipid = Components.interfaces.nsIClipboard;
	if (!clip)
	    return false;
	clip.setData(trans, null, clipid.kGlobalClipboard);
    } else {
	// browser does not support copying
	var dialogProperties = {
	    title : this.localizedStrings.copy_warning,
	    body : this.localizedStrings.copy_browser_not_supported,
	    button1 : this.localizedStrings.copy_ok_button
	};
	buildCustomDialog(dialogProperties, "", "");
	return;
    }
};
