/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

/**
 * Generates a progress bar DOM object.
 * @param id The identifier for the progress bar. This ends up being the id
 *           attribute of the highest-level DOM element returned.
 * @param startPercent The percentage value to initialize the bar at.
 * @param pixelWidth The width in pixels of the bar.
 * @param showText Optional boolean to indicate wether 'XX%' should be displayed
 *                 preceeding the actual bar.
 */
function getProgressBar(id, startPercent, pixelWidth, showText) {
    // Compute the widths necessary.
    var fillWidth = parseInt((pixelWidth / 100) * startPercent);
    var overallWidth = pixelWidth + 2;

    // Generate the DOM elements for the bar itself.
    var grayPart = SPAN({
	'id' : id + 'innerspan',
	'class' : 'innerSpan',
	'style' : 'left:' + fillWidth + 'px'
    }, null);
    var bluePart = SPAN({
	'id' : id + 'outerspan',
	'class' : 'outerSpan'
    }, grayPart);
    var bar = DIV({
	'class' : 'progressBar'
    }, bluePart);

    // Set the widths.
    bar.style.width = overallWidth;
    bluePart.style.width = pixelWidth;
    grayPart.style.widht = pixelWidth;

    // The display style is inline-block, except for IE where inline was working.
    if (navigator.appName == "Microsoft Internet Explorer") {
	bar.style.display = "inline";
    }

    // Place a span of text, the percent sign, and the progress bar into an array.
    var children = new Array();
    children[0] = SPAN({
	'id' : id + 'textpercentage'
    }, startPercent);
    children[1] = "%";
    children[2] = bar;

    if (showText === false) {
	children[0].style.display = "none";
	children[1] = "";
    }

    // Create a span that encapsulates everything, and assign its childen.
    return SPAN({
	'id' : id,
	'class' : 'progressBarContainer'
    }, children);
}

/**
 * Set the progress of a progress bar.
 * @param id The ID given when the DOM object was created to identify the bar.
 * @param percentage The percentage to set the bar to. Integers 0 to 100 are valid.
 */
function setProgress(id, percentage) {
    // Grab the necessary elements by their IDs.
    var innerSpan = document.getElementById(id + "innerspan");
    var outerSpan = document.getElementById(id + "outerspan");
    var textSpan = document.getElementById(id + "textpercentage");
    if (innerSpan == undefined) {
	// If we cannot find the progress bar, simply return so other scripts
	// may continue to run.
	return;
    } else {
	// Compute new width values.
	var pixelWidth = parseInt(elementDimensions(outerSpan).w);
	var newFillWidth = parseInt((pixelWidth / 100) * percentage);
	// Set the 'left' CSS attribute to move the gray area over, effectively
	// increasing the blue area.
	innerSpan.style.left = newFillWidth + 'px';
	if (textSpan != undefined) {
	    textSpan.innerHTML = percentage;
	}
    }
}

/**
 * Returns the percentage value of the indicated progress bar.
 * @param id The ID of the progress bar.
 * @return An integer between 0 and 100.
 */
function getProgress(id) {
    return parseInt(document.getElementById(id + "textpercentage").innerHTML);
}
