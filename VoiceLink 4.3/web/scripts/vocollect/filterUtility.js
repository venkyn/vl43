/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Filter Utility for Table
 * Component 2.0
 */
/*
 * Utilized by the filter_module to build the different input containers
 * depending on the filter type (boolean, enum, text input).
 */
var filterUtility = {

	drawDropDown : function(critNumber, tablecomponent, viewid, criterion, elemIds, localizedOptions) {
		var FilterValueSelect = 'select.filterValue';
		appendChildNodes(elemIds.FilterCriteria + critNumber,
			 SELECT({"id":elemIds.FilterValueOne + critNumber,"class":"filterSelect","tid":FilterValueSelect + critNumber},OPTION({"value":"-33didNotSelect22"},tablecomponent.filter_module.localizedStrings.selectAValue)));
		if (criterion != null && criterion.value1 == "true") {
			appendChildNodes(elemIds.FilterValueOne + critNumber, OPTION({"value":"true","selected":"true"},localizedOptions.trueValue));
		} else {
			appendChildNodes(elemIds.FilterValueOne + critNumber, OPTION({"value":"true"},localizedOptions.trueValue));
		}
		if (criterion != null && criterion.value1 == "false") {
			appendChildNodes(elemIds.FilterValueOne + critNumber, OPTION({"value":"false","selected":"true"},localizedOptions.falseValue));
		} else {
			appendChildNodes(elemIds.FilterValueOne + critNumber, OPTION({"value":"false"},localizedOptions.falseValue));
		}
		buildAddOrRemoveLink(critNumber, tablecomponent, criterion, elemIds);
	},

	drawEnums : function(critNumber, tablecomponent, criterion, elemIds, request) {
		var data = evalJSONRequest(request);
		var enums = evalJSON(data.enums);
		var FilterValueSelect = 'select.filterValue';
	
		var select = SELECT({"id":elemIds.FilterValueOne + critNumber,"tid":FilterValueSelect + critNumber},OPTION({"value":"-33didNotSelect22"},tablecomponent.filter_module.localizedStrings.selectAValue));
		
		for (var i=0; i < enums.length; i++) {
			if (criterion != null && criterion.value1 == enums[i].value) {
			    appendChildNodes(select, OPTION({"value":enums[i].value,'selected':'true'},enums[i].display));
			} else {
			    appendChildNodes(select, OPTION({"value":enums[i].value},enums[i].display));
			}
		}
		appendChildNodes(elemIds.FilterCriteria + critNumber,
						 select);
		buildAddOrRemoveLink(critNumber, tablecomponent, criterion, elemIds);
		if (criterion !=  null) {
			tablecomponent.filter_module.saveNewFilterCriteria(critNumber, true);
		}
	},

	drawEmptyControl : function(critNumber, tablecomponent, viewid, criterion, elemIds, localizedOptions) {
	    var valueOne = "";
	    var FilterValue = 'input.filterValue';
	    
	    if (criterion != null && criterion.value1 != null){
	        valueOne = criterion.value1;
	    }
		    
	    appendChildNodes(elemIds.FilterCriteria + critNumber,
	        INPUT({"id":elemIds.FilterValueOne + critNumber,"class":"filterInput","value":valueOne, "style": "display:none;", "tid":FilterValue + critNumber}));
		    
	    buildAddOrRemoveLink(critNumber, tablecomponent, criterion, elemIds);
		
	},

	drawTwoInputBoxes : function(critNumber, tablecomponent, viewid, criterion, elemIds, localizedOptions, lockdown, columnId) {
		var valueOne = "";
		var valueTwo = "";
		var FilterValueInputOne = 'input.firstFilterValue';
		var FilterValueInputTwo = 'input.secondFilterValue';	
			
		var col = tablecomponent.getColumnById(columnId);
		if (criterion != null && criterion.value1 != null){
			valueOne = criterion.value1;
		}
		if (criterion != null && criterion.value2 != null){
			valueTwo = criterion.value2;
		}
		if (!$(elemIds.FilterValueOne + critNumber)) {
			if (!col.sortable) {
				appendChildNodes(elemIds.FilterCriteria + critNumber,
						 INPUT({"id":elemIds.FilterValueOne + critNumber,"class":"filterInput","size":"10","value":valueOne,"tid":FilterValueInputOne + critNumber}),
						 INPUT({"id":elemIds.FilterValueTwo + critNumber,"class":"filterInput","size":"10","value":valueTwo,"tid":FilterValueInputTwo + critNumber}));
			}
			else {
				filterUtility.drawAutoCompleteControl(elemIds.FilterCriteria + critNumber, elemIds.FilterValueOne + critNumber, valueOne, tablecomponent, col);
				filterUtility.drawAutoCompleteControl(elemIds.FilterCriteria + critNumber, elemIds.FilterValueTwo + critNumber, valueTwo, tablecomponent, col);
			}
		}
		if (!valueOne) {
			$(elemIds.FilterValueOne + critNumber).focus();
		}
		else if (!valueTwo) {
			$(elemIds.FilterValueTwo + critNumber).focus();
		}
		buildAddOrRemoveLink(critNumber, tablecomponent, criterion, elemIds);
	},
	
	drawInputBox: function (critNumber, tablecomponent, viewid, criterion, elemIds, localizedOptions, lockdown, columnId) {
		var valueOne = "";
		var FilterValue = 'input.filterValue';
	
		var col = tablecomponent.getColumnById(columnId);
		if (criterion != null && criterion.value1 != null){
			valueOne = criterion.value1;
		}
		if (!$(elemIds.FilterValueOne + critNumber)) {
			if (lockdown != null && lockdown) {
				appendChildNodes(elemIds.FilterCriteria + critNumber,
					INPUT({"id":elemIds.FilterValueOne + critNumber,"class":"filterInput","value":valueOne,"disabled":"true","tid":FilterValue + critNumber}));
		} 
	        else if (!col.sortable || col.filterAutoCompleteDisable) {
	            appendChildNodes(elemIds.FilterCriteria + critNumber,
	        	    INPUT({"id":elemIds.FilterValueOne + critNumber,"class":"filterInput","value":valueOne,"tid":FilterValue + critNumber}));
		}
		else {
		    filterUtility.drawAutoCompleteControl(elemIds.FilterCriteria + critNumber, elemIds.FilterValueOne + critNumber, valueOne, tablecomponent, col, FilterValue + critNumber);
		}
		if (!valueOne) {
		    $(elemIds.FilterValueOne + critNumber).focus();
		}
	}
		buildAddOrRemoveLink(critNumber, tablecomponent, criterion, elemIds);
        },
	
    drawAutoCompleteControl: function(parentId, baseId, valueOne, tablecomponent, col, tidValue) {
        	appendChildNodes(parentId, 
        		SPAN({'id': 'ACContainer-'+baseId, 
        			'class': 'autoCompleteContainer'},
        		INPUT({"id": baseId, 
        			"class": "filterInput", "value": valueOne, "tid":tidValue}),
        		DIV({"id": "ACList-"+ baseId, 
        			"class": "autoCompleteList"})
        		)
        	);
        	$(baseId).style.visibility = "visible";
        	 /* Implementation of the auto complete feature using Jquery
        	  * instead of YUI as was being used in version VL 4.0.1. 
        	  */       	
        	
        	var url = tablecomponent.getAutoCompleteUrl();
			var filterField = tablecomponent.getColumnFieldNameByid(col.id);
        	$j( '#' + baseId ).autocomplete({
    			source: function( request, response ) {
    				$j.ajax({
    					url: url,
    					dataType: "json",
    					data: {
    						filterField:filterField,
    				 		query : request.term,
    				 		maxRows : 100,
    				 		style: "full"
    					},
    					success: function( data ) {
    						response(data.ResultSet.Result);
    					}
    				});
    			},
    			minLength: 1,
    			
    			open: function() {
    				$j( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
    			},
    			close: function() {
    				$j( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
    			}
    		});
        	        	
        },
	
	drawDropDownWithEnums: function (critNumber, tablecomponent, viewid, criterion, elemIds, localizedOptions) {
	    var url = globals.BASE + "/getEnumerationValues.action";
	    var params = {
		viewId: viewid,
		columnId: $(elemIds.FilterColumn + critNumber).value
	};
	    simpleXMLHttpRequestOutput=doSimpleXMLHttpRequest(url, params)
		.addCallbacks(partial(filterUtility.drawEnums, critNumber, tablecomponent, criterion, elemIds), 
			function (request) {
		                reportError(request.responseText);
		        }
		);
	},
	
	
	drawDropDownWithDates : function (critNumber, tablecomponent, viewid, criterion, elemIds, localizedOptions) {
		var url = globals.BASE + "/getDateValues.action";
		var params = {};
		simpleXMLHttpRequestOutput=doSimpleXMLHttpRequest(url, params)
		        .addCallbacks(partial(filterUtility.drawEnums, critNumber, tablecomponent, criterion, elemIds), 
		         function (request) {
		                  reportError(request.responseText);
		        }
		);
	}
};

/**
 * Helper method for the filter utility builders.
 * @param critNumber - number of active criterion
 * @param tablecomponent - table component to be appended to
 * @param criterion - the criterion object itself
 * @param elemIds - id of the link element
 * @return
 */
function buildAddOrRemoveLink(critNumber, tablecomponent, criterion, elemIds) {
	if (criterion == null) {
	appendChildNodes(elemIds.FilterCriteria + critNumber,
	     A({"href":"javascript: " + tablecomponent.name + ".filter_module.saveNewFilterCriteria(" + critNumber  + ");", "class":"filterLink", "id": elemIds.FilterCritAddLink + critNumber}, tablecomponent.filter_module.localizedStrings.addToFilter));
	} else if (!criterion.locked) {
	appendChildNodes(elemIds.FilterCriteria + critNumber,
					     A({"href":"javascript: " + tablecomponent.name + ".filter_module.removeFilterCriteria(\"" + critNumber + "\" );", "class":"filterLink","id": elemIds.FilterCritRemoveLink + critNumber},tablecomponent.filter_module.localizedStrings.removeFromFilter));
	}
}