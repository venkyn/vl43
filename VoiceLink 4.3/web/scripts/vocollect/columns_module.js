/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Columns Module for Table
 * Component 2.0
 */

/**
 * Add/Remove columns module for table component.
 */
ColumnsModule = function(table) {
    
    this.columns = table.columns;
    this.ColumnNamesDiv = "";
    this.table = table;
    this.varName = table.name;
    this.menuDivId = this.varName + '-addRemoveColumnsDiv';
    this.dragAddRemoveColumn = new Array();
    this.isResizeDragging = false;
    this.restoreMsgShowing = false;

};

ColumnsModule.prototype = new MenuModule();

/*
 * Initialize add/remove columns div.
 */
ColumnsModule.prototype.init = function() {
    var addRemoveColumnsDiv = this.addRemoveColumnsDiv();
    this.$j('#' + this.ui_get('menuContainer')).append(addRemoveColumnsDiv);
};
/*
 * Event handler for receiving a new column via drag.
 */
ColumnsModule.prototype.handleColumnDrop = function(drag, e) {
    var self = e.data.self;
    var same_table = $j(drag).closest(".tableDiv").attr("id") == $j(e.target)
	    .closest(".tableDiv").attr("id");
    
    // reference to ColumnsModule since we're in an event
    if (!same_table || $j(drag).hasClass("selected")) {
	return;
    }

    if ($j(e.target).is(".slick-header,.slick-viewport,.slick-header-columns")) {
	// if the column is dropped on the header, viewport or header bar
	self.makeColumnVisible($j(drag));
    } else {
	// If it is dropped relative to another element
	self.makeColumnVisible($j(drag), $j(e));
	// Correctly style column in menu
    }

    $j(drag).addClass('selected').unbind("drag");
    $j(drag).find('input').attr('checked', 'checked');
    e.stopImmediatePropagation();
};
/*
 * Create initial div.
 */
ColumnsModule.prototype.addRemoveColumnsDiv = function() {

    // Note: Much of this pulled as is from the old table component and elements
    // were just converted to jquery. Refactoring opportunities
    // are definitely available.

    var columnNamesContent = $j('<div />', {
	'class' : 'columnNamesContent'
    });
    var columnNamesContainer = $j('<div />', {
	'class' : 'columnNamesBar',
	'id' : this.varName + '-addremovecolumsnamebar'
    }).append(columnNamesContent);
    
    var restoreDiv = this.createRestoreDiv();
    
    var columnNamesDiv = $j('<div />', {
	'id' : this.menuDivId,
	'class' : 'fullMenu',
	'style' : 'display: none; width:100%;'
    }).append(columnNamesContainer).append(restoreDiv);

    var closeRestoreSpan = $j('<span />', {
	'id' : this.ui_get('CloseRestoreSpan')
    }).append(
	    $j(
		    '<a />',
		    {
			'id' : this.ui_get('CloseRestoreLink'),
			'href' : 'javascript: ' + this.varName
				+ '.columns_module.showColumnDefaultMsg();'
		    }).text(this.localizedStrings.restore)).append(' | ');

    columnNamesContent.append(this.writeColumnHeadersForInternalMenu()).append(
	    $j('<div />', {
		'style' : 'clear:both;'
	    })).append(
	    $j('<div />', {
		'class' : 'closeRestoreBar'
	    }).append(closeRestoreSpan).append(
		    $j(
			    '<a />',
			    {
				'id' : 'closeRestoreBar',
				'href' : 'javascript: ' + this.varName
					+ '.columns_module.toggleMenu();'
			    }).text(this.localizedStrings.close)));

   
    
    // Append to menuContainer div
    this.$j('#' + this.ui_get('menuContainer')).append(columnNamesDiv);

    
    
    return columnNamesDiv;
};
/*
 * Create div that appears when reverting columns to default settings.
 */
ColumnsModule.prototype.createRestoreDiv = function() {
    restoreMessageLink = $j(
	    '<a />',
	    {
		'id' : 'closeRestoreBar',
		'class' : 'anchorButton',
		'href' : 'javascript: ' + this.varName
			+ '.columns_module.restoreColumnDefaults();'
	    }).text(this.localizedStrings.yes);
    restoreMessageLink2 = $j(
	    '<a />',
	    {
		'id' : 'closeRestoreBar',
		'class' : 'anchorButton',
		'href' : 'javascript: ' + this.varName
			+ '.columns_module.doNotRestore();'
	    }).text(this.localizedStrings.no);
    restoreMessageDiv2 = $j('<div />', {
	'class' : 'restoreMessages',
	'style' : 'display: block;position:relative;'
    }).append(restoreMessageLink).append(restoreMessageLink2);
    restoreMessageDiv1 = $j('<div />', {
	'class' : 'restoreMessages',
	'style' : 'display: block;position:relative;'
    }).text(this.localizedStrings.restorePromptSecond);
    restoreMessageDiv = $j('<div />', {
	'class' : 'restoreMessages'
    }).text(this.localizedStrings.restorePromptFirst);
    restoreMessageLeft = $j('<div />', {
	'class' : 'columnNamesBar'
    });

    restoreMessageLeft.append(restoreMessageDiv).append(restoreMessageDiv1)
	    .append(restoreMessageDiv2);
    restoreMessageRight = $j('<div />', {
	'id' : this.ui_get('RestoreMessageDiv'),
	'class' : 'columnNamesBarRight',
	'style' : 'display: none;position:relative;'
    }).append(restoreMessageLeft);

    return restoreMessageRight;
};

/*
 * Adds selectable column divs to menu.
 */
ColumnsModule.prototype.writeColumnHeadersForInternalMenu = function() {
    // add all columns to the div
    var trs = this.getAllColumnElements();
    var outerDiv = $j('<div />', {
	'class' : 'addRemoveColumns'
    });
    for ( var i = 0; i < trs.length; i++) {
	outerDiv.append(trs[i]);
    }
    return outerDiv;
};
/*
 * Creates each selectable column div and adds drag events and correct styles
 * based on what is in the columns array.
 */
ColumnsModule.prototype.getAllColumnElements = function() {

    var divArray = new Array();

    var columns_sorted = this.columns.slice(0).sort(keyComparator("name"));
    for ( var i = 0; i < columns_sorted.length; i++) {
	var checkbox = $j("<input />", {
	    'id' : this.ui_get('CheckBoxId') + i,
	    'type' : 'checkbox',
	    'class' : 'checkbox',
	    'name' : columns_sorted[i].name,
	    'checked' : columns_sorted[i].visible
	});
	var innerSpan = $j("<span />", {
	    'id' : this.ColId + columns_sorted[i].id,
	    'class' : 'headerText'
	}).text(columns_sorted[i].name);
	var col_div = $j('<div />', {
	    'id' : this.ui_get('DivId') + columns_sorted[i].id
	}).append(checkbox, innerSpan);
	divArray[i] = col_div;

	if (columns_sorted[i].visible) {
	    col_div.addClass("selected");
	} else {
	    this.addDragEvents(col_div);
	}

	col_div.find('input').bind("click", {
	    self : this
	}, this.handleColumnCheck);

    }
    return divArray;
};

ColumnsModule.prototype.addDragEvents = function(column) {
    column
	    .bind('dragstart', function() {
		$j(".addRemoveProxy").remove(); // TEMPORARY FIX FOR ITEM
		// SHOWING UP IN BOTTOM LEFT
		return $j(this).clone().css({
		    opacity : .5,
		    position : 'absolute',
		    zIndex : 5000
		}).addClass('addRemoveProxy').appendTo(document.body);
	    })
	    .bind("drag", function(e, dd) {
		$j(dd.proxy).css({
		    top : e.pageY,
		    left : e.pageX
		});
	    })
	    .bind(
		    'dragend',
		    {
			self : this
		    },
		    function(e, dd) {
			if ($j(e.target)
				.is(
					".slick-header-column,.slick-header,.slick-viewport,.slick-header-columns")
				|| $j(e.target).parent().is(
					".slick-header-column"))
			    e.data.self.handleColumnDrop(this, e);
			$j(".addRemoveProxy").remove();
		    });
};

ColumnsModule.prototype.handleColumnCheck = function(e) {
    var self = e.data.self;
    if ($j(this).attr('checked')) {
	$j(this).parent().addClass('selected').unbind("drag");
	self.makeColumnVisible($j(this).parent());
    } else {
	$j(this).parent().removeClass('selected');
	self.removeColumnByName($j(this).attr('name'));
	self.addDragEvents($j(this).parent());
    }
};

ColumnsModule.prototype.makeColumnVisible = function(addedColumn, event) {
    var addedColumnName = addedColumn.text();
    var col = null;

    for ( var i = 0; i < this.columns.length; i++) {
	if (this.columns[i].name === addedColumnName) {
	    col = this.columns[i];
	    break;
	}
    }

    var tempColumns = this.table.columns;
    var position = tempColumns.length; // Start index at end incase it is added
    // by checkbox
    if (event) {
	// This was dragged to a particular position
	var parent_div = $j(event.attr("target")).closest(
		".slick-header-column");
	if (event.attr("pageX") > parent_div.offset().left + parent_div.width()
		/ 2) {
	    position = parent_div.index() + 1;
	} else {
	    position = parent_div.index();
	}
    }
    this.table.add_column(col, position);
    // event[0].stopImmediatePropagation();
};

ColumnsModule.prototype.removeColumnByName = function(name) {
    this.table.remove_column(name);
};

ColumnsModule.prototype.showColumnDefaultMsg = function() {
    this.restoreMsgShowing = true;
    $j('#' + this.varName + '-addremovecolumsnamebar').hide();
    this.$j('#' + this.ui_get('RestoreMessageDiv')).show();
};

ColumnsModule.prototype.restoreColumnDefaults = function() {
    this.table.loader.restoreColumnDefaults(this.table.viewId);
};

ColumnsModule.prototype.doNotRestore = function() {
    this.restoreMsgShowing = false;
    this.$j('#' + this.ui_get('RestoreMessageDiv')).hide();
    $j('#' + this.varName + '-addremovecolumsnamebar').show();
};
