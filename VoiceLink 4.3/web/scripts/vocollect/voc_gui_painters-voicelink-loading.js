/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

/**
 * Loading Regions
 */
painters.displayLoadingRegion = function(obj) {
    return painters.builders.link({
	'href' : globals.BASE + '/loading/region/view.action?regionId='
		+ obj.id
    }, obj.name);
};

painters.displayContainerForRoute_load = function(obj) {
	if (obj.actualNumberOfContainers > 0) {
		var filter = getFilter(-1219, '-22905',
				obj.number, -4);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/loading/container/list.action?"+ filter 
		}, obj.actualNumberOfContainers);
	} else {
		return obj.actualNumberOfContainers;
	}
};

painters.displayContainerForRoute_loaded = function(obj) {
	if (obj.loadedNumberOfContainers > 0) {
		var routeFilter = getFilter(-1219, '-22905',
				obj.number, -4);
		var statusFilter = getFilter(-1219, '-22902', '3',
				operandHelper.enumEqualsOperand);
		var statusFilterConsolidate = getFilter(-1219, '-22902', '4',
				operandHelper.enumEqualsOperand);		
		return painters.builders.link({
			"href" : globals.BASE
					+ "/loading/container/list.action?"+ routeFilter +"&" + statusFilter +"&" + statusFilterConsolidate
		}, obj.loadedNumberOfContainers);
	} else {
		return obj.loadedNumberOfContainers;
	}
};

painters.displayContainerForStop_load = function(obj) {
	if (obj.actualNumberOfContainers > 0) {
		var routeFilter = getFilter(-1219, '-22905',
				obj.route.number, -4);
		var stopFilter = getFilter(-1219, '-22906',
				obj.number, -4);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/loading/container/list.action?"+ routeFilter +"&" + stopFilter
		}, obj.actualNumberOfContainers);
	} else {
		return obj.actualNumberOfContainers;
	}
};

painters.displayAssignmentsForStop_load = function(obj) {
	if (obj.assignmentCount > 0) {
		var routeFilter = getFilter(-1009, '-10808',
				obj.route.number, -4);
		var customerFilter = getFilter(-1009, '-10807',
				obj.customerNumber, -4);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/selection/assignment/list.action?"+ routeFilter + "&" +  customerFilter
		}, obj.assignmentCount);
	} else {
		return obj.assignmentCount;
	}
};

painters.displayRouteByRegion_load = function(obj) {
	if (obj.totalAssignments > 0) {
		var regionIdFilter = getFilter(-1217, '-22714', obj.region.name,
				operandHelper.stringEqualsOperand);
		var createdDateFilter = getFilter(-1217, '-22718', loadingRouteSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/loading/route/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&"
					+ createdDateFilter
		}, obj.totalAssignments);
	} else {
		return obj.totalAssignments;
	}
};

painters.displayRouteByRegionAndInProgress_load = function(obj) {
	if (obj.inProgress > 0) {
		var regionIdFilter = getFilter(-1217, '-22714', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1217, '-22703', '3',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1217, '-22718', loadingRouteSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/loading/route/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.inProgress);
	} else {
		return obj.inProgress;
	}
};

painters.displayRouteByRegionAndAvailable_load = function(obj) {
	if (obj.available > 0) {
		var regionIdFilter = getFilter(-1217, '-22714', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1217, '-22703', '1',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1217, '-22718', loadingRouteSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/loading/route/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.available);
	} else {
		return obj.available;
	}
};

painters.displayRouteByRegionAndComplete_load = function(obj) {
	if (obj.complete > 0) {
		var regionIdFilter = getFilter(-1217, '-22714', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1217, '-22703', '4',
				operandHelper.enumEqualsOperand);
		var createdDateFilter = getFilter(-1217, '-22718', loadingRouteSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/loading/route/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.complete);
	} else {
		return obj.complete;
	}
};

painters.displayRouteByRegionAndNotComplete_load = function(obj) {
	if (obj.nonComplete > 0) {
		var regionIdFilter = getFilter(-1217, '-22714', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1217, '-22703', '4',
				operandHelper.enumNotEqualsOperand);
		var createdDateFilter = getFilter(-1217, '-22718', loadingRouteSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/loading/route/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
					+ "&" + createdDateFilter
		}, obj.nonComplete);
	} else {
		return obj.nonComplete;
	}
};

painters.displayOperatorsByRegionAndSignedOn_load = function(obj) {
	if (obj.operatorsWorkingIn > 0) {
		var regionIdFilter = getFilter(-1005, '-10406', obj.region.name,
				operandHelper.stringEqualsOperand);
		var statusFilter = getFilter(-1005, '-10403', '1',
				operandHelper.enumEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/loading/operator/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter + "&" + statusFilter
		}, obj.operatorsWorkingIn);
	} else {
		return obj.operatorsWorkingIn;
	}
};

painters.displayOperatorsByRegion_load = function(obj) {
	if (obj.operatorsAssigned > 0) {
		var regionIdFilter = getFilter(-1005, '-10408', obj.region.name,
				operandHelper.stringEqualsOperand);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/loading/operator/list.action?summarySiteID="
					+ obj.site.id + "&" + regionIdFilter
		}, obj.operatorsAssigned);
	} else {
		return obj.operatorsAssigned;
	}
};

painters.displayDepartureDateForRoute_load = function(obj) {
	return displayTruncatedDate(obj.departureDateTime);
};

painters.displayDepartureDateForContainer_load = function(obj) {
	return displayTruncatedDate(obj.stop.route.departureDateTime);
};

function displayTruncatedDate(app) {
	if (app != undefined) {
		var date = app.split('.')[0];
		return date;
	} else
		return app;

}
