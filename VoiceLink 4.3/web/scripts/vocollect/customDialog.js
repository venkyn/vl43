/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Builds Custom Dialogs For EPP
 * and Dependent Projects
 */

function buildCustomDialog(dialogProperties, result1, result2) {
    var oneButton = (dialogProperties.button2 == undefined);

    if (oneButton) {
	result2 = "";
    }

    var handle1 = function(e) {
	if (dialog.validate()) {
	    this.hide();
	    if (result1 != "") {
		result1();
	    }
	    delete dialog;
	}
    };
    var handle2 = function(e) {
	this.hide();
	if (result2 != "") {
	    result2();
	}
	delete dialog;
    };
    buttonWidth = getTextWidth(DIV({}, dialogProperties.button1
	    + dialogProperties.button2));
    buttonWidth = buttonWidth * 1.6;

    if (navigator.appName == "Microsoft Internet Explorer") {
	browserWidth = document.body.offsetWidth;
    } else if (navigator.appName == "Netscape") {
	browserWidth = window.innerWidth;
    } else {
	browserWidth = 800;
    }

    if (buttonWidth < 350) {
	buttonWidth = 350;
    } else if (buttonWidth > browserWidth) {
	buttonWidth = browserWidth;
    }

    var dialog;

    var windowWidth = buttonWidth;
    // Check for manual override of the dialog's width.
    if (dialogProperties.windowWidth != undefined) {
	windowWidth = dialogProperties.windowWidth;
    }

    if (dialogProperties.useDialog) {
	dialog = new YAHOO.widget.Dialog("dialog", {
	    visible : false,
	    zIndex : 200,
	    width : 350,
	    modal : true,
	    fixedcenter : true,
	    draggable : false
	});
    } else {
	dialog = new YAHOO.widget.SimpleDialog("dialog", {
	    visible : false,
	    zIndex : 200,
	    width : 410,
	    modal : true,
	    fixedcenter : true,
	    draggable : false
	});
    }
    headerDiv = DIV({
	'id' : "dialogHeader"
    }, dialogProperties.title);

    dialog.setHeader(headerDiv);
    dialog.setBody(dialogProperties.body);
    var buttonList = [];
    if (oneButton) {
	buttonList = [ {
	    text : dialogProperties.button1,
	    handler : handle1,
	    isDefault : true
	} ];
    } else {
	buttonList = [ {
	    text : dialogProperties.button1,
	    handler : handle1
	}, {
	    text : dialogProperties.button2,
	    handler : handle2,
	    isDefault : true
	} ];
    }

    dialog.cfg.queueProperty("buttons", buttonList);

    var listeners = new YAHOO.util.KeyListener(document, {
	keys : 27
    }, {
	fn : handle2,
	scope : dialog,
	correctScope : true
    });
    dialog.cfg.queueProperty("keylisteners", listeners);

    dialog.render(document.body);
    dialog.show();

    return dialog;

}
