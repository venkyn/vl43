/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

/**
 * Method to validate of a pick can be manually picked. Makes AJAX calls
 * TODO : Make it generic to take the ID of the dialog box and polulate it accordingly
 */
function validateSelectedPick(tableObject, action, idParam) {
    endLine = "?pickId=" + tableObject.getSelectedIds().join("&pickId=");
    doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	    manualPickValidated, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function getOperatorListOnSelectedShort(tableObject, action) {
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createOperatorListFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function getDataOnSelectedForAssignmentPrintLabel(tableObject, action) {
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createAssignmentPrintLabelFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function createAssignmentPrintLabelFTL(request) {
    if (checkForErrorMessage(request)) {
	showAssignmentPrintLabelDialog(request.responseText);
    }
}

function getDataOnSelectedForAssignmentResequence(tableObject, action) {
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createAssignmentResequenceFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function createAssignmentResequenceFTL(request) {
    if (checkForErrorMessage(request)) {
	showAssignmentResequenceDialog(request.responseText);
    }
}

function getDataOnSelectedForPickPrintLabel(tableObject, action) {
    paramList = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    doSimpleXMLHttpRequest(action + paramList, {}).addCallbacks(
	    createPickPrintLabelFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    tableObj = tableObject;
}

function createPickPrintLabelFTL(request) {
    if (checkForErrorMessage(request)) {
	showPickPrintLabelDialog(request.responseText);
    }
}

function performActionOnSelectedShort(tableObject, action) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    endLine += "&operatorID=" + $("operatorID").value;
    doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	    handleUserMessages, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    
    tableObj = tableObject;
}

function createCancelResequenceFTL(request) {
    var data;
    var ERROR_REDIRECT = "3";
    try {
	data = evalJSONRequest(request);
    } catch (err) {
	// evalJSONRequest throws an error in Firefox.
	showCancelResequenceDialog();
	return;
    }

    if (data == undefined || data.errorCode == 'error') {
	// evalJSONRequest will return undefined in IE.
	showCancelResequenceDialog();
    } else {
	if (data.errorCode == ERROR_REDIRECT) {
	    window.location = data.generalMessage;
	} else {
	}
    }
}

function performActionOnResequence(tableObject, action, direction) {
    var moveTop = '3';
    var moveBottom = '4';
    if (tableObject.getSelectedIds().length > 0) {
	endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
	endLine += "&savedEntityKey=" + $("savedEntityKey").value;
    } else {
	endLine = "?savedEntityKey=" + $("savedEntityKey").value;
    }
    endLine += "&regionId=" + $("regionId").value;
    endLine += "&moveDirection=" + direction;
    tableObj = tableObject;
    if (direction == cancel) {
	doSimpleXMLHttpRequest(action + endLine, {})
		.addCallbacks(
			createCancelResequenceFTL,
			function(request) {
			    if (request.number == 403) {
				writeStatusMessage(error403Text,
					'actionMessage error');
			    } else {
				reportError(request);
			    }
			});
    } else if (action == applyURL) {
	doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
		function(request) {
		    handleUserMessages(request);
		    if (tableObj) {
			tableObj.refresh(tableObj.getExtraParams(), tableObject
				.getSelectedIds()[0]);
		    }
		});
    } else {
	doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
		function(request) {
		    if (tableObj) {
			if (direction != 4 && direction != 2)
			    tableObj.refresh(tableObj.getExtraParams(),
				    tableObject.getSelectedIds()[0]);
		    }
		});
    }
}

function performActionOnResequenceRegion(tableObject, action) {
    if (tableObject.getSelectedIds().length > 0) {
	endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
	endLine += "&regionId=" + $("regionName").value;
    } else {
	endLine = "?regionId=" + $("regionName").value;
    }
    window.location = action + endLine;
}

/*
 *Callback function for validateSelectedPick
 */
function manualPickValidated(request) {
    if (checkForErrorMessage(request)) {
	showManualPickDropDownDialog(request.responseText);
    }
}

/*
 * Method for validating inputs in the manualpicks customdialog box
 * Also makes AJAX call if validations succeeds
 */
function manualPickCustomDialogValidation(tableObject, action) {

    var qtp = $("topickQuantity").value;
    var qp = $("pickedQuantity").value;
    var qe = $("quantityEntered").value;

    if (isNaN(qe) || qe.indexOf('.') != -1) {
	showManualPickRecheckNotNumberDialog();
	return;
    }
    maxquantity = parseInt($("pickedQuantity").value)
	    + parseInt($("quantityEntered").value);
    if (qtp != maxquantity) {
	if (qtp > maxquantity) {
	    showManualPickRecheckDialog(qe);
	}
	if (qtp < maxquantity) {
	    showManualPickRecheckMaxDialog();
	}
    } else {
	endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
	endLine = endLine + "&quantityEntered=" + $("quantityEntered").value;
	doSimpleXMLHttpRequest(action + endLine, {})
		.addCallbacks(
			handleUserMessages,
			function(request) {
			    if (request.number == 403) {
				writeStatusMessage(error403Text,
					'actionMessage error');
			    } else {
				reportError(request);
			    }
			});
	
	tableObj = tableObject;
    }
}

/**
 *
 */
function makeAjaxCallForManualPick(tableObject, action, qe) {
    endLine = "?ids=" + tableObject.getSelectedIds().join("&ids=");
    endLine = endLine + "&quantityEntered=" + qe;
    doSimpleXMLHttpRequest(action + endLine, {}).addCallbacks(
	    handleUserMessages, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
    
    tableObj = tableObject;
}

function createOperatorListFTL(request) {
    if (checkForErrorMessage(request)) {
	showCreateChaseAssignmentDialog(request.responseText);
    }
}

function getRouteWithDeliveryDateForDepTimeUpdate(action) {
    doSimpleXMLHttpRequest(action, {}).addCallbacks(
	    createRouteModificationFTL, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
}

function createRouteModificationFTL(request) {
    if (checkForErrorMessage(request)) {
    	showRouteModificationDialog();
    }
}

