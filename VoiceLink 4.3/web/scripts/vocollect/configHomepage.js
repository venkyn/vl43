/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Script for Dynamically
 * creating configurable homepage areas.
 */

/*
 * Constructor for Table class.
 */
function Table(id, rows, columns, homepageId, currentSiteID) {
    if (rows == null) {
	this.maxRows = 3;
	this.rows = 3;
    } else {
	this.maxRows = rows;
	this.rows = rows;
    }
    if (columns == null) {
	this.columns = 2;
    } else {
	this.columns = columns;
    }
    this.homepageId = homepageId;
    this.currentSiteID = currentSiteID;
    this.baseCellHeight = 200;
    this.cellHeightMargin = 10;

    /* Set div that the table will be placed in. */
    this.setDiv(id);

    /* Set up arrays that will store information for each cell */
    this.cell = new Array(this.rows);
    this.cellrowspan = new Array(this.rows);
    this.cellcolspan = new Array(this.rows);
    this.celltblcomponent = new Array(this.rows);
    this.cellsummaryid = new Array(this.rows);
    this.cellvarname = new Array(this.rows);

    for ( var row = 1; row <= this.rows; row++) {
	this.cell[row - 1] = new Array(this.columns);
	this.cellrowspan[row - 1] = new Array(this.columns);
	this.cellcolspan[row - 1] = new Array(this.columns);
	this.celltblcomponent[row - 1] = new Array(this.columns);
	this.cellsummaryid[row - 1] = new Array(this.columns);
	this.cellvarname[row - 1] = new Array(this.columns);
	this.cellvarname[row - 1][0] = '';
	this.cellvarname[row - 1][1] = '';
	this.cellvarname[row - 1][2] = '';
    }

    this.defaultSummaries();
    this.InitializeTable();
}

YAHOO.util.ResizeTable = function(id, sGroup, config) {
    if (arguments.length > 0) {
	YAHOO.util.ResizeTable.superclass.constructor.call(this, id, sGroup,
		config);
    }
};

YAHOO.extend(YAHOO.util.ResizeTable, YAHOO.util.DDProxy);

/*
 * Initially fills the cells with "blank" divs.
 */
Table.prototype.defaultSummaries = function() {
    count = 1;

    for ( var row = 1; row <= this.rows; row++) {
	for ( var cell = 1; cell <= this.columns; cell++) {
	    this.setArea('blank' + count,
		    new Array(String(row) + String(cell)), false, '999', true);
	    count++;
	}
    }
};
/*
 * Sets the div that will contain the table.
 */
Table.prototype.setDiv = function(id) {
    this.div = document.getElementById(id);
};
/*
 * Sets a new area on the homepage. Accepts the following parameters: area - Div
 * ID of the area cellArray - array containing top left cell coordinates and
 * bottom right cell coordinates, ie. {'11', '23'} tblComponent - set to true if
 * panel uses the table component summaryId - id of the hidden div that
 * accompanies each panel
 * 
 * Cell layout: ________ ________ ________ | | | | | 1,1 | 1,2 | 1,3 |
 * |________|________|________| | | | | | 2,1 | 2,2 | 2,3 |
 * |________|________|________| | | | | | 3,1 | 3,2 | 3,3 |
 * |________|________|________|
 */
Table.prototype.setArea = function(area, cellArray, tblComponent, summaryId,
	dataOnly) {

    if (!(area.match('Div'))) {
	area = area + 'Div';
    }
    firstParam = String(cellArray[0]);
    startrow = parseInt(firstParam.substr(0, 1));
    startcell = parseInt(firstParam.substr(1, 1));

    if (cellArray.length > 1) {
	secondParam = String(cellArray[cellArray.length - 1]);
	endrow = parseInt(secondParam.substr(0, 1));
	endcell = parseInt(secondParam.substr(1, 1));
    } else {
	endrow = startrow;
	endcell = startcell;
    }

    for ( var row = startrow; row <= endrow; row++) {
	for ( var cell = startcell; cell <= endcell; cell++) {
	    this.cell[row - 1][cell - 1] = area + '_null';
	}
    }

    this.cell[startrow - 1][startcell - 1] = area;
    this.cellrowspan[startrow - 1][startcell - 1] = ((endrow - startrow) + 1);
    this.cellcolspan[startrow - 1][startcell - 1] = ((endcell - startcell) + 1);
    this.celltblcomponent[startrow - 1][startcell - 1] = tblComponent;
    this.cellsummaryid[startrow - 1][startcell - 1] = summaryId;

    if (!dataOnly) {
	for ( var i = startrow; i <= endrow; i++) {
	    for ( var j = startcell; j <= endcell; j++) {
		if (this.cell[i - 1][j - 1].match('_null')) {
		    $('cell' + String(i) + String(j)).parentNode
			    .removeChild($('cell' + String(i) + String(j)));
		}
	    }
	}
	cell = $('blank' + startrow + startcell).parentNode;
	cell.removeChild($('blank' + startrow + startcell));
	cell.rowSpan = this.cellrowspan[startrow - 1][startcell - 1];
	cell.colSpan = this.cellcolspan[startrow - 1][startcell - 1];
	$j('#' + cell.id).width(
		this.setDivWidth(this.cell[startrow - 1][startcell - 1]));
	$j('#' + cell.id)
		.height(
			this.baseCellHeight
				+ ((cell.rowSpan - 1) * (this.baseCellHeight + this.cellHeightMargin)));
	newCell = this.createDiv(startrow, startcell);
	$j(newCell).width(this.setDivWidth(newCell.id, true));
	$j(newCell)
		.height(
			this.baseCellHeight
				+ ((cell.rowSpan - 1) * (this.baseCellHeight + this.cellHeightMargin)));
	cell.appendChild(newCell);
    }
};
/*
 * Switches two summary area positions.
 */
Table.prototype.switchAreas = function(area1, area2) {
    area1Div = document.getElementById(area1);
    area2Div = document.getElementById(area2);
    area1Cell = document.getElementById(area1).parentNode;
    area2Cell = document.getElementById(area2).parentNode;
    end1Row = 0;
    end1Cell = 0;
    end2Row = 0;
    end2Cell = 0;
    tableresize = false;
    noSwap = false;

    if (area2Cell.id.match('-headerDiv')) {
	noSwap = true;
    }

    if (!noSwap) {
	for ( var row = 1; row <= this.rows; row++) {
	    for ( var cell = 1; cell <= this.columns; cell++) {
		if (this.cell[row - 1][cell - 1].match(area1)) {
		    if (this.cell[row - 1][cell - 1].match('null')) {
			end2Row = row;
			end2Cell = cell;
		    } else {
			start2Row = row;
			start2Cell = cell;
		    }
		} else if (this.cell[row - 1][cell - 1].match(area2)) {
		    if (this.cell[row - 1][cell - 1].match('null')) {
			end1Row = row;
			end1Cell = cell;
		    } else {
			start1Row = row;
			start1Cell = cell;
		    }
		}
	    }
	}

	if (end1Row == 0) {
	    end1Row = start1Row;
	}
	if (end1Cell == 0) {
	    end1Cell = start1Cell;
	}
	if (end2Row == 0) {
	    end2Row = start2Row;
	}
	if (end2Cell == 0) {
	    end2Cell = start2Cell;
	}
	rows1 = document
		.getElementById(this.cell[start1Row - 1][start1Cell - 1]).parentNode.rowSpan;
	rows2 = document
		.getElementById(this.cell[start2Row - 1][start2Cell - 1]).parentNode.rowSpan;
	tempvarname = this.cellvarname[start1Row - 1][start1Cell - 1];
	this.cellvarname[start1Row - 1][start1Cell - 1] = this.cellvarname[start2Row - 1][start2Cell - 1];
	this.cellvarname[start2Row - 1][start2Cell - 1] = tempvarname;
	totalCells = ((end1Row - start1Row) + 1)
		* ((end1Cell - start1Cell) + 1);
	fromArray = new Array(totalCells);
	totalCells = ((end2Row - start2Row) + 1)
		* ((end2Cell - start2Cell) + 1);
	toArray = new Array(totalCells);
	count = 0;
	for (row = start1Row; row <= end1Row; row++) {
	    for (cell = start1Cell; cell <= end1Cell; cell++) {
		fromArray[count] = String(row) + String(cell);
		count++;
	    }
	}
	count = 0;
	for (row = start2Row; row <= end2Row; row++) {
	    for (cell = start2Cell; cell <= end2Cell; cell++) {
		toArray[count] = String(row) + String(cell);
		count++;
	    }
	}

	area1Cell.removeChild(area1Div);
	area2Cell.removeChild(area2Div);

	area1Cell.appendChild(area2Div);
	area2Cell.appendChild(area1Div);
	tempTblComp = this.celltblcomponent[start1Row - 1][start1Cell - 1];
	tempSummaryId = this.cellsummaryid[start1Row - 1][start1Cell - 1];

	this.setArea(area1Div.id, new Array(String(start1Row)
		+ String(start1Cell), String(end1Row) + String(end1Cell)),
		this.celltblcomponent[start2Row - 1][start2Cell - 1],
		this.cellsummaryid[start2Row - 1][start2Cell - 1], true);
	this.setArea(area2Div.id, new Array(String(start2Row)
		+ String(start2Cell), String(end2Row) + String(end2Cell)),
		tempTblComp, tempSummaryId, true);

	if (this.cellsummaryid[start2Row - 1][start2Cell - 1] != '999'
		&& this.cellsummaryid[start1Row - 1][start1Cell - 1] != '999') {
	    swapArea(toArray, fromArray);
	} else {
	    if (this.cellsummaryid[start2Row - 1][start2Cell - 1] != '999') {
		saveArea(this.cellsummaryid[start2Row - 1][start2Cell - 1],
			toArray, fromArray);
	    }

	    if (this.cellsummaryid[start1Row - 1][start1Cell - 1] != '999') {
		saveArea(this.cellsummaryid[start1Row - 1][start1Cell - 1],
			fromArray, toArray);
	    }
	}
	pnlClose = document.getElementById(area1Div.id.substr(0,
		area1Div.id.length - 3)
		+ '-closeLink');
	minRow = this.getCellDimensions(area1Div.id)[0];
	minCell = this.getCellDimensions(area1Div.id)[1];
	maxRow = this.getCellDimensions(area1Div.id)[2];
	maxCell = this.getCellDimensions(area1Div.id)[3];
	totalCells = ((maxRow - minRow) + 1) * ((maxCell - minCell) + 1);
	fromArray = new Array(totalCells);
	count = 0;
	for (row = minRow; row <= maxRow; row++) {
	    for (cell = minCell; cell <= maxCell; cell++) {
		fromArray[count] = '\'' + String(row) + String(cell) + '\'';
		count++;
	    }
	}

	pnlClose.href = 'javascript:removeArea(\''
		+ this.cell[minRow - 1][minCell - 1] + '\',new Array('
		+ fromArray + '),'
		+ this.celltblcomponent[minRow - 1][minCell - 1] + ');';

	if (!(area2Div.id.match('blank'))) {
	    pnlClose2 = document.getElementById(area2Div.id.substr(0,
		    area2Div.id.length - 3)
		    + '-closeLink');
	    minRow = this.getCellDimensions(area2Div.id)[0];
	    minCell = this.getCellDimensions(area2Div.id)[1];
	    maxRow = this.getCellDimensions(area2Div.id)[2];
	    maxCell = this.getCellDimensions(area2Div.id)[3];
	    totalCells = ((maxRow - minRow) + 1) * ((maxCell - minCell) + 1);
	    fromArray = new Array(totalCells);
	    count = 0;
	    for (row = minRow; row <= maxRow; row++) {
		for (cell = minCell; cell <= maxCell; cell++) {
		    toArray[count] = '\'' + String(row) + String(cell) + '\'';
		    count++;
		}
	    }

	    pnlClose2.href = 'javascript:removeArea(\''
		    + this.cell[minRow - 1][minCell - 1] + '\',new Array('
		    + toArray + '),'
		    + this.celltblcomponent[minRow - 1][minCell - 1] + ');';
	}

	if (area2Div.id.match('blank')) {
	    document.getElementById(area1Cell.id).colSpan = 1;
	    document.getElementById(area1Cell.id).rowSpan = 1;
	    this.cellcolspan[start1Row - 1][start1Cell - 1] = 1;
	    this.cellrowspan[start1Row - 1][start1Cell - 1] = 1;
	    document.getElementById(area1Div.id).style.height = this.baseCellHeight;
	    document.getElementById(area1Cell.id).style.height = this.baseCellHeight;
	    this.removeSummary(area2Div.id);
	}
	temp = area1Div.style.height;
	area1Div.style.height = area2Div.style.height;
	area2Div.style.height = temp;

	if (this.celltblcomponent[start1Row - 1][start1Cell - 1]) {
	    new_span = end1Row - start1Row;
	    $j(
		    document
			    .getElementById(this.cell[start1Row - 1][start1Cell - 1]).parentNode)
		    .height(
			    this.baseCellHeight
				    + (new_span * (this.baseCellHeight + this.cellHeightMargin)));
	    this.cellvarname[start1Row - 1][start1Cell - 1]
		    .setHeight(this.baseCellHeight
			    + (new_span * (this.baseCellHeight + this.cellHeightMargin)));
	}

	if (this.celltblcomponent[start2Row - 1][start2Cell - 1]) {
	    new_span = end2Row - start2Row;
	    $j(
		    document
			    .getElementById(this.cell[start2Row - 1][start2Cell - 1]).parentNode)
		    .height(
			    this.baseCellHeight
				    + (new_span * (this.baseCellHeight + this.cellHeightMargin)));
	    this.cellvarname[start2Row - 1][start2Cell - 1]
		    .setHeight(this.baseCellHeight
			    + (new_span * (this.baseCellHeight + this.cellHeightMargin)));

	}

	area1Div.parentNode.style.width = this.setDivWidth(area1Div.id);
	if (area2Div.id.match('blank')) {
	    // area2Div.parentNode.style.width=this.setDivWidth('override','33%');
	} else {
	    area2Div.parentNode.style.width = this.setDivWidth(area2Div.id);
	}
    }
};
/*
 * Removes a summary from the grid.
 */
Table.prototype.removeSummary = function(id) {
    summary = document.getElementById(id);
    if (summary != null) {
	cell = summary.parentNode;
	cell.removeChild(summary);
    }
    for ( var row = 1; row <= this.rows; row++) {
	for ( var cell = 1; cell <= this.columns; cell++) {
	    if (this.cell[row - 1][cell - 1].match(id)) {
		this.setArea('blank' + String(row) + String(cell), new Array(
			String(row) + String(cell)), false, '999', true);
		this.blankCell(row, cell);
	    }
	}
    }
};

Table.prototype.displayTable = function() {

    /* Look for blank areas and remove them */
    for ( var rowct = 1; rowct <= this.rows; rowct++) {
	for ( var cellct = 1; cellct <= this.columns; cellct++) {
	    if (this.cell[rowct - 1][cellct - 1].match('blank')) {
		this.removeSummary(this.cell[rowct - 1][cellct - 1]);
	    } else {
		if (this.celltblcomponent[rowct - 1][cellct - 1]) {
		    dimens = this
			    .getCellDimensions(this.cell[rowct - 1][cellct - 1]);
		    this.formatTblComponent(this.cell[rowct - 1][cellct - 1],
			    new Array(dimens[0], dimens[1], dimens[2],
				    dimens[3]));
		}
	    }
	}
    }
};
/*
 * Generates the table and attaches it to the div.
 */
Table.prototype.InitializeTable = function() {
    var pnlTable = document.createElement('table');
    pnlTable.className = 'homepageSummaries';

    var pnlTBody = document.createElement('tbody');
    pnlTBody.id = 'pnlTable';

    for ( var row = 1; row <= this.rows; row++) {
	var pnlRow = document.createElement('tr');
	pnlTBody.appendChild(pnlRow);
	for ( var cell = 1; cell <= this.columns; cell++) {
	    var pnlCell = document.createElement('td');
	    pnlCell.id = 'cell' + String(row) + String(cell);
	    pnlCell.className = 'summaryTD';
	    pnlCell.colSpan = this.cellcolspan[row - 1][cell - 1];
	    pnlCell.rowSpan = this.cellrowspan[row - 1][cell - 1];
	    pnlCell.style.height = this.baseCellHeight;
	    pnlRow.appendChild(pnlCell);
	    pnlDiv = DIV({
		'id' : 'blank' + String(row) + String(cell)
	    });
	    pnlCell.appendChild(pnlDiv);
	}
	var pnlCell = document.createElement('td');
	pnlCell.id = 'heightfix' + row;
	pnlCell.colSpan = 1;
	pnlCell.rowSpan = 1;
	pnlCell.style.height = this.baseCellHeight;
	pnlRow.appendChild(pnlCell);
    }

    pnlTable.appendChild(pnlTBody);
    this.div.appendChild(pnlTable);
};
/*
 * Resizes a summary area.
 */
Table.prototype.resizeArea = function(area, border, amount) {
    noResize = false;
    tableresize = false;
    dimensions = new Array(4);
    dimensions = this.getCellDimensions(area);
    minRow = dimensions[0];
    minCell = dimensions[1];
    maxRow = dimensions[2];
    maxCell = dimensions[3];

    if (border == 'top') {
	new_minRow = minRow + amount;

    } else {
	new_minRow = minRow;
    }

    if (border == 'right') {
	new_maxCell = maxCell + amount;
    } else {
	new_maxCell = maxCell;
    }

    if (border == 'bottom') {
	new_maxRow = maxRow + amount;
    } else {
	new_maxRow = maxRow;
    }

    if (border == 'left') {
	new_minCell = minCell + amount;
    } else {
	new_minCell = minCell;
    }

    if (new_minRow < 1 || new_maxRow > this.rows || new_minCell < 1
	    || new_maxCell > this.columns || new_minRow > new_maxRow
	    || new_minCell > new_maxCell) {
    } else {
	for ( var row = new_minRow; row <= new_maxRow; row++) {
	    for ( var cell = new_minCell; cell <= new_maxCell; cell++) {
		if (!(this.cell[row - 1][cell - 1].match('blank'))
			&& !(this.cell[row - 1][cell - 1].match(area))) {
		    noResize = true;
		}
	    }
	}
	o_minCell = minCell;
	o_maxCell = maxCell;
	o_minRow = minRow;
	o_maxRow = maxRow;

	if (!(noResize)) {
	    totalCells = ((maxRow - minRow) + 1) * ((maxCell - minCell) + 1);
	    fromArray = new Array(totalCells);
	    totalCells = ((new_maxRow - new_minRow) + 1)
		    * ((new_maxCell - new_minCell) + 1);
	    toArray = new Array(totalCells);
	    count = 0;
	    for (row = minRow; row <= maxRow; row++) {
		for (cell = minCell; cell <= maxCell; cell++) {
		    fromArray[count] = String(row) + String(cell);
		    count++;
		}
	    }
	    count = 0;
	    for (row = new_minRow; row <= new_maxRow; row++) {
		for (cell = new_minCell; cell <= new_maxCell; cell++) {
		    toArray[count] = String(row) + String(cell);
		    count++;
		}
	    }
	    tbl = document.getElementById('pnlTable');

	    /* Top border dragged */
	    if (new_minRow != minRow) {
		this.celltblcomponent[new_minRow - 1][new_minCell - 1] = this.celltblcomponent[minRow - 1][minCell - 1];
		this.celltblcomponent[minRow - 1][minCell - 1] = false;
		if (this.celltblcomponent[new_minRow - 1][new_minCell - 1]) {
		    tableresize = true;
		}
		/* Down */
		if (amount > 0) {
		    tempSummId = this.cellsummaryid[minRow - 1][minCell - 1];
		    this.cellsummaryid[new_minRow - 1][new_minCell - 1] = tempSummId;
		    resizeCell = document
			    .getElementById(this.cell[minRow - 1][minCell - 1]).parentNode;
		    resizeCell.rowSpan = (-amount) + (maxRow - minRow) + 1;
		    resizeCell.parentNode.removeChild(resizeCell);
		    resizeCell.id = 'cell' + String(new_minRow)
			    + String(new_minCell);
		    notdone = true;
		    for ( var count = new_maxCell; count <= this.columns; count++) {
			if (chkObject('cell' + String(new_minRow)
				+ String(count))
				&& notdone) {
			    tbl.childNodes[new_minRow - 1].insertBefore(
				    resizeCell, document.getElementById('cell'
					    + String(new_minRow)
					    + String(count)));
			    notdone = false;
			}
		    }
		    if (notdone) {
			tbl.childNodes[new_minRow - 1].insertBefore(resizeCell,
				document.getElementById('heightfix'
					+ String(new_minRow)));
		    }

		    for (row = minRow; row <= new_minRow - 1; row++) {
			add_to_tr = tbl.childNodes[row - 1];
			for (cell = new_maxCell; cell >= new_minCell; cell--) {
			    var add_td = document.createElement('td');
			    add_td.colSpan = 1;
			    add_td.rowSpan = 1;
			    add_td.className = 'summaryTD';
			    add_td.id = 'cell' + String(row) + String(cell);
			    this.setArea('blank' + (row) + (cell) + 'Div',
				    new Array(String(row) + String(cell)),
				    false, '999', true);
			    add_div = this.createBlankDiv(row, cell);
			    add_td.style.width = this.setDivWidth(add_div.id);
			    dt = new YAHOO.util.DDTarget(add_div.id);
			    add_td.appendChild(add_div);
			    notdone = true;
			    for (count = cell; count <= this.columns; count++) {
				if (chkObject('cell' + String(row)
					+ String(count))
					&& notdone) {
				    tbl.childNodes[row - 1].insertBefore(
					    add_td, document
						    .getElementById('cell'
							    + String(row)
							    + String(count)));
				    notdone = false;
				}
			    }
			    if (notdone) {
				tbl.childNodes[row - 1].insertBefore(add_td,
					document.getElementById('heightfix'
						+ String(row)));
			    }
			}
		    }
		    /* Up */
		} else {
		    tempSummId = this.cellsummaryid[minRow - 1][minCell - 1];
		    this.cellsummaryid[new_minRow - 1][new_minCell - 1] = tempSummId;
		    resizeCell = document
			    .getElementById(this.cell[minRow - 1][minCell - 1]).parentNode;
		    resizeCell.rowSpan = (-amount) + (maxRow - minRow) + 1;
		    resizeCell.parentNode.removeChild(resizeCell);
		    document
			    .getElementById(this.cell[new_minRow - 1][new_minCell - 1]).parentNode.parentNode
			    .insertBefore(
				    resizeCell,
				    document
					    .getElementById(this.cell[new_minRow - 1][new_minCell - 1]).parentNode);
		    resizeCell.id = 'cell' + String(new_minRow)
			    + String(new_minCell);
		    for (row = new_minRow; row <= minRow - 1; row++) {
			for (cell = minCell; cell <= maxCell; cell++) {
			    document
				    .getElementById(this.cell[row - 1][cell - 1]).parentNode.parentNode
				    .removeChild(document
					    .getElementById(this.cell[row - 1][cell - 1]).parentNode);
			}
		    }
		}
	    }

	    /* Bottom border is dragged */
	    if (new_maxRow != maxRow) {
		rowSpanChange = (new_maxRow - new_minRow) - (maxRow - minRow);
		document.getElementById(area).parentNode.rowSpan = document
			.getElementById(area).parentNode.rowSpan
			+ rowSpanChange;
		if (this.celltblcomponent[new_minRow - 1][new_minCell - 1]) {
		    tableresize = true;
		}
		/* Down */
		if (amount > 0) {
		    for ( var i = maxRow + 1; i <= new_maxRow; i++) {
			for ( var j = minCell; j <= maxCell; j++) {
			    $(this.cell[i - 1][j - 1]).parentNode.parentNode
				    .removeChild($(this.cell[i - 1][j - 1]).parentNode);
			}
		    }
		    if (!noResize) {
			document
				.getElementById(this.cell[minRow - 1][minCell - 1]).parentNode.rowSpan = (new_maxRow - new_minRow) + 1;
		    }
		    /* Up */
		} else {
		    document.getElementById(this.cell[minRow - 1][minCell - 1]).parentNode.rowSpan = (new_maxRow - new_minRow) + 1;
		    /* Full width - Add full row */
		    for (row = maxRow; row >= new_maxRow + 1; row--) {
			for (cell = minCell; cell <= maxCell; cell++) {
			    var add_td = document.createElement('td');
			    add_td.colSpan = 1;
			    add_td.rowSpan = 1;
			    add_td.className = 'summaryTD';
			    add_td.id = 'cell' + String(row) + String(cell);
			    add_div = this.createBlankDiv(row, cell);
			    add_div.id = 'blank' + String(row) + String(cell)
				    + 'Div';
			    this.setArea(add_div.id, new Array(String(row)
				    + String(cell)), false, '999', true);
			    dt = new YAHOO.util.DDTarget(add_div.id);
			    add_td.style.width = this.setDivWidth(add_div.id);
			    add_td.appendChild(add_div);
			    this.setArea('blank' + (row) + (cell) + 'Div',
				    new Array(String(row) + String(cell)),
				    false, '999', true);
			    add_to_tr = tbl.childNodes[row - 1];
			    notdone = true;
			    for (count = cell + 1; count <= this.columns; count++) {
				if (chkObject('cell' + String(row)
					+ String(count))
					&& notdone) {
				    tbl.childNodes[row - 1].insertBefore(
					    add_td, document
						    .getElementById('cell'
							    + String(row)
							    + String(count)));
				    notdone = false;
				}
			    }
			    if (notdone) {
				tbl.childNodes[row - 1].insertBefore(add_td,
					document.getElementById('heightfix'
						+ String(row)));
			    }
			}
		    }
		}
	    }

	    /* Left border is dragged */
	    if (new_minCell != minCell) {
		this.celltblcomponent[new_minRow - 1][new_minCell - 1] = this.celltblcomponent[minRow - 1][minCell - 1];
		this.celltblcomponent[minRow - 1][minCell - 1] = false;
		if (this.celltblcomponent[new_minRow - 1][new_minCell - 1]) {
		    tableresize = true;
		}
		/* Right */
		if (amount > 0) {
		    tempSummId = this.cellsummaryid[minRow - 1][minCell - 1];
		    this.cellsummaryid[new_minRow - 1][new_minCell - 1] = tempSummId;
		    resizeCell = document
			    .getElementById(this.cell[minRow - 1][minCell - 1]).parentNode;
		    resizeCell.colSpan = (-amount) + (maxCell - minCell) + 1;
		    resizeCell.id = 'cell' + String(new_minRow)
			    + String(new_minCell);
		    for (row = new_minRow; row <= new_maxRow; row++) {
			for (cell = minCell; cell <= new_minCell - 1; cell++) {
			    var add_td = document.createElement('td');
			    add_td.colSpan = 1;
			    add_td.rowSpan = 1;
			    add_td.className = 'summaryTD';
			    add_td.id = 'cell' + row + cell;
			    this.setArea('blank' + row + cell + 'Div',
				    new Array(String(row) + String(cell)),
				    false, '999', true);
			    add_div = this.createBlankDiv(row, cell);
			    add_td.style.width = this.setDivWidth(add_div.id);
			    dt = new YAHOO.util.DDTarget(add_div.id);
			    add_td.appendChild(add_div);
			    notdone = true;
			    for (count = cell + 1; count <= this.columns; count++) {
				if (chkObject('cell' + String(row)
					+ String(count))
					&& notdone) {
				    tbl.childNodes[row - 1].insertBefore(
					    add_td, document
						    .getElementById('cell'
							    + String(row)
							    + String(count)));
				    notdone = false;
				}
			    }
			    if (notdone) {
				tbl.childNodes[row - 1].insertBefore(add_td,
					document.getElementById('heightfix'
						+ String(row)));
			    }

			    this.celltblcomponent[row - 1][cell - 1] = false;
			    this.cell[row - 1][cell - 1] = 'blank'
				    + String(row) + String(cell) + 'Div';
			    this.cellcolspan[row - 1][cell - 1] = 1;
			    this.cellrowspan[row - 1][cell - 1] = 1;
			}
		    }
		    /* Left */
		} else {
		    tempSummId = this.cellsummaryid[minRow - 1][minCell - 1];
		    this.cellsummaryid[new_minRow - 1][new_minCell - 1] = tempSummId;
		    resizeCell = document
			    .getElementById(this.cell[minRow - 1][minCell - 1]).parentNode;
		    resizeCell.colSpan = (-amount) + (maxCell - minCell) + 1;
		    for (row = new_minRow; row <= new_maxRow; row++) {
			for (cell = new_minCell; cell <= minCell - 1; cell++) {
			    removeCell = document
				    .getElementById(this.cell[row - 1][cell - 1]).parentNode;
			    removeElement(removeCell.id);
			}
		    }
		    resizeCell.id = 'cell' + String(new_minRow)
			    + String(new_minCell);
		    rw = resizeCell.parentNode;
		    removeElement(resizeCell.id);
		    if (new_maxCell == this.columns) {
			rw.insertBefore(resizeCell, $('heightfix'
				+ String(new_minRow)));
		    } else {
			rw
				.insertBefore(resizeCell, $('cell'
					+ String(new_minRow)
					+ String(new_maxCell + 1)));
		    }

		}
	    }

	    /* Right border is dragged */
	    if (new_maxCell != maxCell) {
		colSpanChange = (new_maxCell - new_minCell)
			- (maxCell - minCell);
		document.getElementById(area).parentNode.colSpan = document
			.getElementById(area).parentNode.colSpan
			+ colSpanChange;
		/* Right */
		if (amount > 0) {
		    tbl.childNodes[minRow - 1].childNodes[minCell - 1].colSpan = (new_maxCell - new_minCell) + 1;
		    for (row = minRow; row <= maxRow; row++) {
			for (cell = maxCell + 1; cell <= new_maxCell; cell++) {
			    tbl.childNodes[row - 1].removeChild(document
				    .getElementById('cell' + String(row)
					    + String(cell)));
			}
		    }
		    /* Left */
		} else {
		    for (row = minRow; row <= maxRow; row++) {
			for (cell = new_maxCell; cell <= maxCell - 1; cell++) {

			    var add_td = document.createElement('td');
			    add_td.colSpan = 1;
			    add_td.rowSpan = 1;
			    add_td.className = 'summaryTD';
			    add_td.id = 'cell' + row + (cell + 1);
			    this.setArea('blank' + row + (cell + 1) + 'Div',
				    new Array(String(row) + String(cell + 1)),
				    false, '999', true);
			    add_div = this.createBlankDiv(row, cell + 1);
			    add_td.style.width = this.setDivWidth(add_div.id);
			    dt = new YAHOO.util.DDTarget(add_div.id);
			    add_td.appendChild(add_div);
			    if (((cell + 1) == this.columns)
				    || !(chkObject('cell' + String(row)
					    + String(cell + 2)))) {
				tbl.childNodes[row - 1].insertBefore(add_td,
					$('heightfix' + String(row)));
			    } else {
				tbl.childNodes[row - 1].insertBefore(add_td,
					$('cell' + String(row)
						+ String(cell + 2)));
			    }
			    this.cell[row - 1][cell] = 'blank' + row
				    + (cell + 1) + 'Div';
			    this.cellcolspan[row - 1][cell] = 1;
			    this.cellrowspan[row - 1][cell] = 1;
			}
		    }
		}
	    }

	    this.setArea(area, new Array(String(new_minRow)
		    + String(new_minCell), String(new_maxRow)
		    + String(new_maxCell)),
		    this.celltblcomponent[new_minRow - 1][new_minCell - 1],
		    this.cellsummaryid[new_minRow - 1][new_minCell - 1], true);
	    totalCells = ((new_maxRow - new_minRow) + 1)
		    * ((new_maxCell - new_minCell) + 1);
	    toArray2 = new Array(totalCells);
	    count = 0;
	    for (row = new_minRow; row <= new_maxRow; row++) {
		for (cell = new_minCell; cell <= new_maxCell; cell++) {
		    toArray2[count] = '\'' + String(row) + String(cell) + '\'';
		    count++;
		}
	    }
	    pnlClose = document.getElementById(area.substr(0, area.length - 3)
		    + '-closeLink');
	    pnlClose.href = 'javascript:removeArea(\''
		    + this.cell[new_minRow - 1][new_minCell - 1]
		    + '\',new Array(' + toArray2 + '),'
		    + this.celltblcomponent[new_minRow - 1][new_minCell - 1]
		    + ');';

	    saveArea(this.cellsummaryid[new_minRow - 1][new_minCell - 1],
		    toArray, fromArray);
	    document.getElementById(area).parentNode.style.width = this
		    .setDivWidth(area);
	    document.getElementById(area).style.height = (this
		    .getCellDimensions(area)[2]
		    - this.getCellDimensions(area)[0] + 1)
		    * this.baseCellHeight;

	}

	if (this.celltblcomponent[new_minRow - 1][new_minCell - 1]
		&& !(noResize)) {
	    tempvarname = this.cellvarname[o_minRow - 1][o_minCell - 1];
	    this.cellvarname[o_minRow - 1][o_minCell - 1] = '';
	    this.cellvarname[new_minRow - 1][new_minCell - 1] = tempvarname;

	    if (border == 'top' || border == 'bottom') {
		new_span = new_maxRow - new_minRow;
		$j(
			document
				.getElementById(this.cell[minRow - 1][minCell - 1]).parentNode)
			.height(
				this.baseCellHeight
					+ (new_span * (this.baseCellHeight + this.cellHeightMargin)));
		this.cellvarname[new_minRow - 1][new_minCell - 1]
			.setHeight(this.baseCellHeight
				+ (new_span * (this.baseCellHeight + this.cellHeightMargin)));
	    }
	}
    }
};
/*
 * Converts the panel at given row and cell to a standard blank 'add a summary'
 * cell
 */
Table.prototype.blankCell = function(row, cell) {
    tbl = document.getElementById('pnlTable');
    if (chkObject('cell' + String(row) + String(cell))) {
	document.getElementById('cell' + String(row) + String(cell)).rowSpan = 1;
	document.getElementById('cell' + String(row) + String(cell)).colSpan = 1;
	blankDiv = this.createBlankDiv(row, cell);
	document.getElementById('cell' + String(row) + String(cell))
		.appendChild(blankDiv);
	document.getElementById('cell' + String(row) + String(cell)).colSpan = 1;
	document.getElementById('cell' + String(row) + String(cell)).rowSpan = 1;
	pnlTable.setArea(blankDiv.id, new Array(String(row) + String(cell)),
		false, '999', true);
	document.getElementById('cell' + String(row) + String(cell)).style.width = this
		.setDivWidth(blankDiv.id);
	document.getElementById('cell' + String(row) + String(cell)).style.height = '200px';
    } else {
	var blankTD = document.createElement('td');
	blankTD.colSpan = 1;
	blankTD.rowSpan = 1;
	blankTD.className = 'summaryTD';
	blankTD.id = 'cell' + row + cell;
	blankDiv = this.createBlankDiv(row, cell);
	blankTD.appendChild(blankDiv);
	blankTD.style.width = this.setDivWidth(blankDiv.id);

	// append new cell at the end of the row or insert it
	// before another cell
	notdone = true;
	for ( var count = cell + 1; count <= this.columns; count++) {
	    if (chkObject('cell' + String(row) + String(count)) && notdone) {
		tbl.childNodes[row - 1].insertBefore(blankTD, document
			.getElementById('cell' + String(row) + String(count)));
		notdone = false;
	    }
	}
	if (notdone) {
	    tbl.childNodes[row - 1].insertBefore(blankTD, document
		    .getElementById('heightfix' + String(row)));
	}

	this.cellcolspan[row - 1][cell - 1] = 1;
	this.cellrowspan[row - 1][cell - 1] = 1;
    }
    dt = new YAHOO.util.DDTarget(blankDiv.id);
};
/*
 * Creates and returns a panel to be placed into an area in the grid.
 */
Table.prototype.createDiv = function(row, cell) {
    pnlDiv = document.createElement('div');
    pnlDiv.id = this.cell[row - 1][cell - 1];

    pnlDiv.style.position = 'relative';
    pnlDiv.style.zIndex = 50;
    pnlDiv.className = 'summary';
    pnlDiv.style.height = parseInt(this.cellrowspan[row - 1][cell - 1])
	    * this.baseCellHeight;
    pnlDiv.style.position = 'relative';
    summaryIdHidden = document.createElement('input');
    summaryIdHidden.type = 'hidden';
    summaryIdHidden.id = 'summaryId';
    summaryIdHidden.name = 'summaryId';
    summaryIdHidden.value = this.cellsummaryid[row - 1][cell - 1];
    pnlDiv.appendChild(summaryIdHidden);
    minRow = this.getCellDimensions(this.cell[row - 1][cell - 1])[0];
    minCell = this.getCellDimensions(this.cell[row - 1][cell - 1])[1];
    maxRow = this.getCellDimensions(this.cell[row - 1][cell - 1])[2];
    maxCell = this.getCellDimensions(this.cell[row - 1][cell - 1])[3];
    totalCells = ((maxRow - minRow) + 1) * ((maxCell - minCell) + 1);
    fromArray = new Array(totalCells);
    count = 0;
    for ( var countrow = minRow; countrow <= maxRow; countrow++) {
	for ( var countcell = minCell; countcell <= maxCell; countcell++) {
	    fromArray[count] = '\'' + String(countrow) + String(countcell)
		    + '\'';
	    count++;
	}
    }

    if (!(this.celltblcomponent[row - 1][cell - 1])) {
	pnlDiv.style.borderColor = '#AAAAAA';
	pnlDiv.style.borderWidth = '1';
	pnlDiv.style.borderStyle = 'solid';
	pnlDiv.style.backgroundColor = '#DDDDDD';
	titleBar = document.createElement('div');
	titleBar.id = pnlDiv.id.substr(0, pnlDiv.id.length - 3)
		+ '-titleBarDiv';
	titleBar.className = 'titleBar';
	titleBar.style.marginBottom = '10';
	close_x = document.createElement('img');
	close_x.src = basePath + '/images/close_x.gif';
	close_x.className = 'close';
	close_link = document.createElement('a');
	close_link.href = 'javascript:removeArea(\''
		+ this.cell[row - 1][cell - 1] + '\',new Array(' + fromArray
		+ '),' + this.celltblcomponent[row - 1][cell - 1] + ');';
	close_link.id = pnlDiv.id.substr(0, pnlDiv.id.length - 3)
		+ '-closeLink';
	close_link.appendChild(close_x);
	titleBar.appendChild(close_link);
	titleContent = document.createElement('div');
	titleContent.className = 'titleContent';
	titleContent.innerHTML = pnlDiv.id.substr(0, pnlDiv.id.length - 3);
	titleBar.appendChild(titleContent);
	pnlDiv.insertBefore(titleBar, pnlDiv.firstChild);
	contentDiv = document.createElement('div');
	contentDiv.id = pnlDiv.id.substr(0, pnlDiv.id.length - 3)
		+ '-contentDiv';
	pnlDiv.appendChild(contentDiv);
    } else {
	contentDiv = document.createElement('div');
	contentDiv.id = pnlDiv.id.substr(0, pnlDiv.id.length - 3) + '-tableDiv';
	contentDiv.style.height = '100%';
	contentDiv.className = 'tableDiv';
	pnlDiv.appendChild(contentDiv);
    }
    rightBorder = document.createElement('div');
    rightBorder.style.zIndex = 202;
    rightBorder.id = pnlDiv.id + '_right';
    rightBorder.style.width = '5px';
    rightBorder.style.cursor = 'e-resize';
    rightBorder.style.height = '100%';
    rightBorder.style.position = 'absolute';
    rightBorder.style.right = 0;
    rightBorder.style.top = 0;
    pnlDiv.appendChild(rightBorder);
    leftBorder = document.createElement('div');
    leftBorder.style.zIndex = 202;
    leftBorder.id = pnlDiv.id + '_left';
    leftBorder.style.width = '5px';
    leftBorder.style.cursor = 'e-resize';
    leftBorder.style.height = '100%';
    leftBorder.style.position = 'absolute';
    leftBorder.style.left = 0;
    leftBorder.style.top = 0;
    pnlDiv.appendChild(leftBorder);
    topBorder = document.createElement('img');
    topBorder.src = basePath + '/images/topBorder.gif';
    topBorder.style.cursor = 'n-resize';
    topBorder.style.zIndex = 300;
    topBorder.id = pnlDiv.id + '_top';
    topBorder.style.height = '3px';
    topBorder.style.width = '100%';
    topBorder.style.position = 'absolute';
    topBorder.style.top = 0;
    topBorder.style.left = 0;
    pnlDiv.appendChild(topBorder);
    bottomBorder = document.createElement('div');
    bottomBorder.style.zIndex = 203;
    bottomBorder.id = pnlDiv.id + '_bottom';
    bottomBorder.style.height = '5px';
    bottomBorder.style.cursor = 'n-resize';
    bottomBorder.style.width = '100%';
    bottomBorder.style.position = 'absolute';
    bottomBorder.style.bottom = 0;
    bottomBorder.style.left = 0;
    pnlDiv.appendChild(bottomBorder);
    dd = new YAHOO.util.ResizeTable(pnlDiv.id);
    tblName = pnlDiv.id.substr(0, pnlDiv.id.length - 3);
    dd.setHandleElId(tblName + '-titleBarDiv');
    dd.setHandleElId(tblName + '-rowCountDiv');
    dd.setHandleElId(pnlDiv.id + '_top');
    dd.setHandleElId(pnlDiv.id + '_right');
    dd.setHandleElId(pnlDiv.id + '_bottom');
    dd.setHandleElId(pnlDiv.id + '_left');

    return pnlDiv;
};

Table.prototype.formatTblComponent = function(div_id, cellarray) {
    pnlDiv = $(div_id);
    totalCells = ((cellarray[2] - cellarray[0]) + 1)
	    * ((cellarray[3] - cellarray[1]) + 1);
    fromArray = new Array(totalCells);
    count = 0;
    for ( var countrow = cellarray[0]; countrow <= cellarray[2]; countrow++) {
	for ( var countcell = cellarray[1]; countcell <= cellarray[3]; countcell++) {
	    fromArray[count] = '\'' + String(countrow) + String(countcell)
		    + '\'';
	    count++;
	}
    }
    title = document.getElementById(pnlDiv.id.substr(0, pnlDiv.id.length - 3)
	    + '-titleBarDiv');
    title.style.zIndex = 250;
    close_x = document.createElement('img');
    close_x.src = basePath + '/images/close_x.gif';
    close_x.className = 'close';
    close_link = document.createElement('a');
    close_link.href = 'javascript:removeArea(\''
	    + this.cell[cellarray[0] - 1][cellarray[1] - 1] + '\',new Array('
	    + fromArray + '),'
	    + this.celltblcomponent[cellarray[0] - 1][cellarray[1] - 1] + ');';
    close_link.id = pnlDiv.id.substr(0, pnlDiv.id.length - 3) + '-closeLink';
    close_link.appendChild(close_x);
    title.appendChild(close_link);

    var browser = navigator.appName;
    if (browser == "Microsoft Internet Explorer") {
	rowCount = $(div_id.substr(0, div_id.length - 3) + '-rowCountDiv');
	rowCount.style.cursor = 'n-resize';
    }
};
/*
 * Creates and returns a blank panel to be inserted into the grid.
 */
Table.prototype.createBlankDiv = function(row, cell) {
    blankDiv = document.createElement('div');
    blankDiv.className = 'blank';
    blankDiv.id = 'blank' + row + cell + 'Div';
    blankDiv.style.height = this.baseCellHeight;
    for ( var i = 0; i <= 5; i++) {
	var brk = document.createElement('br');
	blankDiv.appendChild(brk);
    }
    var blankDivLink = document.createElement('a');
    blankDivLink.setAttribute('href', 'javascript:addSummary(' + row + ','
	    + cell + ')');
    blankDivLink.style.color = '#5555AA';
    var txt = document.createTextNode(addASummary);
    blankDivLink.appendChild(txt);
    blankDiv.appendChild(blankDivLink);
    return blankDiv;
};
/*
 * Returns an array with 4 cell coordinates for given id: top-left, top-right,
 * bottom-left, bottom-right
 */
Table.prototype.getCellDimensions = function(id) {
    minCell = 0;
    minRow = 0;
    maxCell = 0;
    maxRow = 0;

    for ( var row = 1; row <= this.rows; row++) {
	for ( var cell = 1; cell <= this.columns; cell++) {
	    if (this.cell[row - 1][cell - 1].match(id)) {
		if (minCell == 0 || minRow == 0) {
		    minCell = cell;
		    minRow = row;
		} else {
		    maxRow = row;
		    maxCell = cell;
		}
	    }
	}
    }
    if (maxRow == 0) {
	maxRow = minRow;
    }
    if (maxCell == 0) {
	maxCell = minCell;
    }
    dimensions = new Array(4);
    dimensions[0] = minRow;
    dimensions[1] = minCell;
    dimensions[2] = maxRow;
    dimensions[3] = maxCell;

    return dimensions;
};
/*
 * Adds summary in database.
 */
function addSummary(row, col) {

    var date = new Date();
    action = "getAllSummaries.action";
    endLine = "?location=" + row + col;
    endLine += "&someParam=" + date;
    endLine += "&homepageId=" + pnlTable.homepageId;
    simpleXMLHttpRequestOutput = doSimpleXMLHttpRequest(action + endLine, {})
	    .addCallbacks(createDialog, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
}

/*
 * Saves panel location changes in database.
 */
function saveArea(id, toArray, fromArray) {
    action = "dragDropAndResize.action";
    endLine = "?draggedFromLocation=" + fromArray.join("&draggedFromLocation=");
    endLine += "&draggedToLocation=" + toArray.join("&draggedToLocation=");
    endLine += "&summaryId=" + id;
    endLine += "&homepageId=" + pnlTable.homepageId;
    simpleXMLHttpRequestOutput = doSimpleXMLHttpRequest(action + endLine, {})
	    .addCallbacks(handleRequest, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
}

/*
 * Removes summary from database.
 */
function removeSummary(action) {

    // pnlTable.removeSummary( $("summaryIds").value);

    var fromLocs = $("fromArray").value;
    var fromLocation = fromLocs.split(',');
    action = "removeSummary.action";
    endLine = "?locationsOfSummaryToBeRemoved="
	    + fromLocation.join("&locationsOfSummaryToBeRemoved=");
    endLine += "&summaryId=" + $("summaryIds").value;
    endLine += "&isTableComponent=" + $("refresh").value;
    endLine += "&homepageId=" + pnlTable.homepageId;
    simpleXMLHttpRequestOutput = doSimpleXMLHttpRequest(action + endLine, {})
	    .addCallbacks(handleRequestAddSummary, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
}

function handleRequestAddSummary(request) {
    var data = evalJSONRequest(request);
    if (data.isTableComponent) {
	window.location.reload(true);
    }

}

function removeArea(id, fromArray, refresh) {

    var _fromArray = fromArray;
    var _refresh = refresh;

    var data = DIV({
	'id' : 'removeDiv'
    }, null);
    var summaryId = INPUT({
	'id' : 'summaryIds',
	'name' : 'summaryIds',
	'type' : 'hidden',
	'value' : id
    }, null);
    var fromArray = INPUT({
	'id' : 'fromArray',
	'name' : 'fromArray',
	'type' : 'hidden',
	'value' : _fromArray
    }, null);
    var refresh = INPUT({
	'id' : 'refresh',
	'name' : 'refresh',
	'type' : 'hidden',
	'value' : _refresh
    }, null);

    appendChildNodes(data, summaryId);
    appendChildNodes(data, fromArray);
    appendChildNodes(data, refresh);

    // YAHOO.simpledialog.div_deleteSummary_confirm.setBody();

    var element = $("removeDiv");
    if (element == null) {
	appendChildNodes($("div_deleteSummary_confirm"), data);
    } else {
	swapDOM(element, data);
    }

    YAHOO.simpledialog.div_deleteSummary_confirm.show();

}

function handleRequest(errorObject) {
    reportError(errorObject.responseText);
}

function simplyHideAddDialog() {
    YAHOO.simpledialog.div_addSummary_confirm.hide();
}

var handleAddSummary = function(e) {
    this.hide();
    performActionOnSummarySelected('addSummary.action');
};
function createDialog(request) {
    YAHOO.simpledialog.div_addSummary_confirm.setBody(request.responseText);

    if ($("showAvailability").value == "false") {
	YAHOO.simpledialog.div_addSummary_confirm.cfg.queueProperty("buttons",
		[ {
		    text : $("cancelText").value,
		    handler : simplyHideAddDialog,
		    isDefault : true
		} ]);
	YAHOO.simpledialog.div_addSummary_confirm.render(document.body);
    } else {
	YAHOO.simpledialog.div_addSummary_confirm.cfg.queueProperty("buttons",
		[ {
		    text : $("addSummary").value,
		    handler : handleAddSummary
		}, {
		    text : $("cancelText").value,
		    handler : simplyHideAddDialog,
		    isDefault : true
		} ]);
	YAHOO.simpledialog.div_addSummary_confirm.render(document.body);

    }

    YAHOO.simpledialog.div_addSummary_confirm.show();

}

function performActionOnSummarySelected(action) {
    endLine = "?location=" + $("location").value;
    endLine += "&summaryId=" + $("summaries").value;
    endLine += "&homepageId=" + pnlTable.homepageId;
    endLine += "&redirectSite=" + pnlTable.currentSiteID;
    simpleXMLHttpRequestOutput = doSimpleXMLHttpRequest(action + endLine, {})
	    .addCallbacks(handleUserMessages, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
}

/*
 * Saves a summary swap to the database.
 */
function swapArea(toArray, fromArray) {
    action = "swapSummaries.action";
    endLine = "?draggedFromLocation=" + fromArray.join("&draggedFromLocation=");
    endLine += "&draggedToLocation=" + toArray.join("&draggedToLocation=");
    endLine += "&homepageId=" + pnlTable.homepageId;
    simpleXMLHttpRequestOutput = doSimpleXMLHttpRequest(action + endLine, {})
	    .addCallbacks(handleRequest, function(request) {
		if (request.number == 403) {
		    writeStatusMessage(error403Text, 'actionMessage error');
		} else {
		    reportError(request);
		}
	    });
}

YAHOO.util.ResizeTable.prototype.onMouseUp = function(e) {
    if (this.border != '' && this.border != null) {
	var dragEl = this.getDragEl();
	var el = this.getEl();
	this.originalTop = -1000;
	this.originalLeft = -1000;

	this.dragElWidth = dragEl.style.width;
	this.dragElWidth = parseInt(this.dragElWidth.substr(0,
		this.dragElWidth.length - 2));
	this.dragElHeight = dragEl.style.height;
	this.dragElHeight = parseInt(this.dragElHeight.substr(0,
		this.dragElHeight.length - 2));
	minRow = pnlTable.getCellDimensions(el.id)[0];
	minCell = pnlTable.getCellDimensions(el.id)[1];
	maxRow = pnlTable.getCellDimensions(el.id)[2];
	maxCell = pnlTable.getCellDimensions(el.id)[3];

	if (this.border == 'right') {

	    for ( var ct = 1; ct <= pnlTable.columns; ct++) {
		if (this.cellWidth * (ct - 1) <= this.dragElWidth
			&& this.dragElWidth <= this.cellWidth * ct) {
		    newCell = ct + (minCell - 1);
		}
	    }

	    if (newCell - maxCell != 0) {
		pnlTable.resizeArea(el.id, 'right', newCell - maxCell);
	    }

	}

	if (this.border == 'bottom') {
	    for (ct = 1; ct <= pnlTable.rows; ct++) {
		if (this.cellHeight * (ct - 1) <= this.dragElHeight
			&& this.dragElHeight <= this.cellHeight * ct) {
		    newRow = ct + (minRow - 1);
		}
	    }

	    if (newRow - maxRow != 0) {
		pnlTable.resizeArea(el.id, 'bottom', newRow - maxRow);
	    }
	}
	if (this.border == 'top') {
	    for (ct = 1; ct <= pnlTable.rows; ct++) {
		if (this.cellHeight * (ct - 1) <= this.dragElHeight
			&& this.dragElHeight <= this.cellHeight * ct) {
		    newRow = ct + (minRow - 1);
		}
	    }

	    if (newRow - maxRow != 0) {
		pnlTable.resizeArea(el.id, 'top', (maxRow - newRow));
	    }
	}

	if (this.border == 'left') {
	    for (ct = 1; ct <= pnlTable.columns; ct++) {
		if (this.cellWidth * (ct - 1) <= this.dragElWidth
			&& this.dragElWidth <= this.cellWidth * ct) {
		    newCell = ct + (minCell - 1);
		}
	    }

	    if (newCell - maxCell != 0) {
		pnlTable.resizeArea(el.id, 'left', (maxCell - newCell));
	    }
	}

    }

};
/*
 * Overrides Yahoo Drag Drop function.
 */

YAHOO.util.ResizeTable.prototype.onDragDrop = function(e, id) {
    cell1 = YAHOO.util.DragDropMgr.dragCurrent.id;
    cell2 = id;

    if (this.move) {
	pnlTable.switchAreas(cell1, cell2);
	this.originalTop = -1000;
	this.originalLeft = -1000;
    }
};
/*
 * Returns a calculated percent for a div, based on its location, colspan and
 * the browser width.
 */
Table.prototype.setDivWidth = function(id, notPercent) {

    if (id == 'override') {
	adjPercent = override;
    } else {

	for ( var wrow = 1; wrow <= this.rows; wrow++) {
	    for ( var wcell = 1; wcell <= this.columns; wcell++) {
		if (id == this.cell[wrow - 1][wcell - 1]) {
		    span = this.cellcolspan[wrow - 1][wcell - 1];
		}
	    }
	}

	var offset = 0;
	if (span == 1) {
	    offset = 12;
	} else if (span == 2) {
	    offset = 20;
	} else if (span == 3) {
	    offset = 28;
	}
	cellWidth = parseInt(((YAHOO.util.Dom.getViewportWidth()) * (span / pnlTable.columns))
		- offset);
	percent = (cellWidth / YAHOO.util.Dom.getViewportWidth()) * 100;

	if (this.columns == 3) {
	    if (percent < 40) {
		adjPercent = '33%';
	    } else if (percent < 85) {
		adjPercent = '66%';
	    } else {
		adjPercent = '100%';
	    }
	} else {
	    if (percent < 60) {
		adjPercent = '50%';
	    } else {
		adjPercent = '100%';
	    }
	}

    }
    if (notPercent) {
	return cellWidth;
    } else {
	return adjPercent;
    }
};

YAHOO.util.ResizeTable.prototype.showFrame = function(iPageX, iPageY) {
    var el = this.getEl();
    var dragEl = this.getDragEl();
    var s = dragEl.style;

    this._resizeProxy();

    if (this.centerFrame) {
	this.setDelta(Math.round(parseInt(s.width, 10) / 2), Math
		.round(parseInt(s.height, 10) / 2));
    }

    this.setDragElPos(iPageX, iPageY);

    YAHOO.util.Dom.setStyle(dragEl, "visibility", "visible");
    minRow = pnlTable.getCellDimensions(el.id)[0];
    minCell = pnlTable.getCellDimensions(el.id)[1];
    maxRow = pnlTable.getCellDimensions(el.id)[2];
    maxCell = pnlTable.getCellDimensions(el.id)[3];

    this.elWidth = parseInt(((YAHOO.util.Dom.getViewportWidth()) * (document
	    .getElementById(YAHOO.util.DragDropMgr.dragCurrent.id).parentNode.colSpan / pnlTable.columns)) - 12);
    this.elHeight = YAHOO.util.Dom.getStyle(el.id, 'height');
    this.elHeight = parseInt(this.elHeight.substr(0, this.elHeight.length - 2));

    this.elRowSpan = el.parentNode.rowSpan;
    this.elColSpan = el.parentNode.colSpan;

    this.cellWidth = this.elWidth / this.elColSpan;
    this.cellHeight = this.elHeight / this.elRowSpan;

    this.pnlXclick = YAHOO.util.DragDropMgr.startX;
    this.pnlYclick = YAHOO.util.DragDropMgr.startY;
    temppnlWidth = YAHOO.util.Dom.getStyle(
	    YAHOO.util.DragDropMgr.dragCurrent.id, 'width');
    this.pnlWidth = parseInt(((YAHOO.util.Dom.getViewportWidth()) * (document
	    .getElementById(YAHOO.util.DragDropMgr.dragCurrent.id).parentNode.colSpan / pnlTable.columns)) - 12);
    temppnlHeight = YAHOO.util.Dom.getStyle(
	    YAHOO.util.DragDropMgr.dragCurrent.id, 'height');
    this.pnlHeight = parseInt(temppnlHeight.substr(0, temppnlHeight.length - 2));

    this.move = false;
    mouseClick = new YAHOO.util.Region(this.pnlYclick, this.pnlXclick,
	    this.pnlYclick, this.pnlXclick);

    if (checkBorder(mouseClick, el.id + '_right')) {
	this.border = 'right';
	this.setYConstraint(0, 0);
    } else if (checkBorder(mouseClick, el.id + '_left')) {
	this.border = 'left';
	this.setYConstraint(0, 0);
    } else if (checkBorder(mouseClick, el.id + '_bottom')
	    || checkBorder(mouseClick, el.id.substr(0, el.id.length - 3)
		    + '-rowCountDiv')) {
	this.border = 'bottom';
	this.setXConstraint(0, 0);
    } else if (checkBorder(mouseClick, el.id + '_top')) {
	this.border = 'top';
	this.setXConstraint(0, 0);
    } else {
	this.move = true;
	this.clearConstraints();
	this.border = '';
    }

    for ( var row = 1; row <= this.rows; row++) {
	for ( var cell = 1; cell <= this.columns; cell++) {
	    if (YAHOO.util.DragDropMgr.dragCurrent.id == this.cell[row - 1][cell - 1]) {
		this.firstRow = row;
		this.firstCell = cell;
	    }
	}
    }
};
function checkBorder(mouseClick, border) {
    regionToCheck = YAHOO.util.Dom.getRegion(border);
    if (regionToCheck.contains(mouseClick)) {
	return true;
    } else {
	return false;
    }
}

YAHOO.util.ResizeTable.prototype.alignElWithMouse = function(el, iPageX, iPageY) {
    if (this.originalTop == -1000 || this.originalTop == null) {
	this.originalTop = 0;
    }

    if (this.originalLeft == -1000 || this.originalLeft == null) {
	this.originalLeft = 0;
    }

    if (this.move == true) {
	var oCoord = this.getTargetCoord(iPageX, iPageY);
	// this.logger.log("****alignElWithMouse : " + el.id + ", " + aCoord +
	// ", " + el.style.display);

	if (!this.deltaSetXY) {
	    var aCoord = [ oCoord.x, oCoord.y ];
	    YAHOO.util.Dom.setXY(el, aCoord);
	    var newLeft = parseInt(YAHOO.util.Dom.getStyle(el, "left"), 10);
	    var newTop = parseInt(YAHOO.util.Dom.getStyle(el, "top"), 10);

	    this.deltaSetXY = [ newLeft - oCoord.x, newTop - oCoord.y ];
	} else {
	    panelMove = YAHOO.util.Dom.getRegion(el);
	    panelTable = YAHOO.util.Dom.getRegion('panelContainer');
	    YAHOO.util.Dom.setStyle(el, "left", (oCoord.x + this.deltaSetXY[0])
		    + "px");
	    YAHOO.util.Dom.setStyle(el, "top", (oCoord.y + this.deltaSetXY[1])
		    + "px");
	}

	this.cachePosition(oCoord.x, oCoord.y);
    } else {
	var oCoord = this.getTargetCoord(iPageX, iPageY);
	// this.logger.log("****alignElWithMouse : " + el.id + ", " + aCoord +
	// ", " + el.style.display);

	if (!this.deltaSetXY) {
	    var aCoord = [ oCoord.x, oCoord.y ];
	    YAHOO.util.Dom.setXY(el, aCoord);
	    var newLeft = parseInt(YAHOO.util.Dom.getStyle(el, "left"), 10);
	    var newTop = parseInt(YAHOO.util.Dom.getStyle(el, "top"), 10);

	    this.deltaSetXY = [ newLeft - oCoord.x, newTop - oCoord.y ];
	} else {
	    minRow = pnlTable
		    .getCellDimensions(YAHOO.util.DragDropMgr.dragCurrent.id)[0];
	    minCell = pnlTable
		    .getCellDimensions(YAHOO.util.DragDropMgr.dragCurrent.id)[1];
	    maxRow = pnlTable
		    .getCellDimensions(YAHOO.util.DragDropMgr.dragCurrent.id)[2];
	    maxCell = pnlTable
		    .getCellDimensions(YAHOO.util.DragDropMgr.dragCurrent.id)[3];

	    if (this.originalTop == 0) {
		this.originalTop = parseInt(YAHOO.util.Dom.getStyle(el, "top")
			.substr(0,
				YAHOO.util.Dom.getStyle(el, "top").length - 2));
	    }

	    if (this.originalLeft == 0) {
		this.originalLeft = parseInt(YAHOO.util.Dom
			.getStyle(el, "left").substr(0,
				YAHOO.util.Dom.getStyle(el, "left").length - 2));
	    }
	    heightOffset = 0;
	    widthOffset = 0;
	    if (pnlTable.columns == 2) {
		heightOffset = 40;
		if (minCell == 1 && maxCell - minCell == 0) {
		    widthOffset = 260;
		} else if (minCell == 2 && maxCell - minCell == 0) {
		    widthOffset = 190;
		} else {
		    widthOffset = 360;
		}
	    }

	    if (this.border == 'bottom') {
		$j(el)
			.css(
				"height",
				(oCoord.y
					+ (this.pnlHeight - ((minRow - 1) * this.cellHeight)) - 45)
					- heightOffset + "px");
	    } else if (this.border == 'right') {
		$j(el)
			.css(
				"width",
				(oCoord.x + (this.pnlWidth - ((minCell - 1) * this.cellWidth)))
					- widthOffset + "px");
	    } else if (this.border == 'top') {
		$j(el)
			.css(
				"height",
				((this.originalTop - oCoord.y) + this.pnlHeight + "px"));
		$j(el).css('top', oCoord.y + 'px');
	    } else if (this.border == 'left') {
		$j(el).css(
			"width",
			((this.originalLeft - oCoord.x) + this.pnlWidth
				- (widthOffset / 2) + "px"));
		$j(el).css("left", oCoord.x);
	    }
	}

	this.cachePosition(oCoord.x, oCoord.y);
    }
};
/*
 * Overrides Yahoo End Drag event to prevent drag from occurring.
 */
YAHOO.util.ResizeTable.prototype.endDrag = function(e, id) {
    this.clearConstraints();
};
function chkObject(val) {
    if (document.getElementById(val) != null) {
	return true;
    } else {
	return false;
    }
}

function reportError(text, timestamp) {
    if (_errBack) {
	_errBack(text, timestamp);
    }
}

function getDataForUpdatingSummary(refresh, actionUrl) {
    var date = new Date();
    simpleXMLHttpRequestOutput = doSimpleXMLHttpRequest(
	    actionUrl + '?someParam=' + date).addCallbacks(getData,
	    function(request) {
		reportError(request.responseText);
	    });
    handleData(refresh, actionUrl);
}

function handleData(refresh, actionUrl) {

}

function parseArray(array) {
    minRow = pnlTable.rows;
    minCell = pnlTable.columns;
    tempRow = 0;
    tempCell = 0;
    for ( var ct = 0; ct <= array.length - 1; ct++) {
	if (tempRow < 1) {
	    strArray = array[ct].toString();
	    tempRow = parseInt(strArray.substr(0, 1));
	}
	if (tempCell < 1) {
	    strArray = array[ct].toString();
	    tempCell = parseInt(strArray.substr(1, 1));
	}
	if (tempRow < minRow) {
	    minRow = tempRow;
	}
	if (tempCell < minCell) {
	    minCell = tempCell;
	}
    }
    parsed = new Array(2);
    parsed[0] = minRow;
    parsed[1] = minCell;

    return parsed;
}
