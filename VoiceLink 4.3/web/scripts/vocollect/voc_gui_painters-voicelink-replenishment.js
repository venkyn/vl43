/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

function getSummaryFilterLinkForAssignment_replen(site, prop, val, operand,
		regionId, timeFilter) {

	var filter = getFilter(-1028, -12603, val, operand);
	var filterRegion = getFilter(-1028, -12624, regionId,
			operandHelper.numberEqualsOperand);
	var dateFilter = getFilter(-1028, '-12630', timeFilter,
			operandHelper.dateWithin);
	return painters.builders.link({
		"href" : globals.BASE
				+ "/replenishment/assignment/list.action?summarySiteID="
				+ site.id + "&" + filter + "&" + filterRegion + "&"
				+ dateFilter
	}, prop);
}

function getBooleanTranslation_replen(val) {
    if (val === true) {
        return itext('replenishment.region.view.true');
    } else {
        return itext('replenishment.region.view.false');
    }
}
painters.displayAssignmentsFilterByTotal_replen = function(obj) {
	if (obj.totalAssignments > 0) {
		var filterRegion = getFilter(-1028, -12624, obj.region.number,
				operandHelper.numberEqualsOperand);
		var dateFilter = getFilter(-1028, '-12630', replenishmentAssignmentSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			'href' : globals.BASE
					+ '/replenishment/assignment/list.action?summarySiteID='
					+ obj.site.id + '&' + filterRegion + '&' + dateFilter
		}, obj.totalAssignments);
	} else {
		return obj.totalAssignments;
	}
};

painters.displayAssignmentsFilterByComplete_replen = function(obj) {
	if (obj.completedAssignments > 0) {
		var filter = getFilter(-1028, -12603, '3',
				operandHelper.enumEqualsOperand);
		var filter2 = getFilter(-1028, -12603, '4',
				operandHelper.enumEqualsOperand);
		var filterRegion = getFilter(-1028, -12624, obj.region.number,
				operandHelper.numberEqualsOperand);
		var dateFilter = getFilter(-1028, '-12630', replenishmentAssignmentSummaryObj.time_filter,
				operandHelper.dateWithin);
		return painters.builders.link({
			"href" : globals.BASE
					+ "/replenishment/assignment/list.action?summarySiteID="
					+ obj.site.id + "&" + filter + "&" + filter2 + "&"
					+ filterRegion + "&" + dateFilter
		}, obj.completedAssignments);
	} else {
		return obj.completedAssignments;
	}
};

painters.displayAssignmentsFilterByInProgress_replen = function(obj) {
	if (obj.inProgressAssignments > 0) {
		return getSummaryFilterLinkForAssignment_replen(obj.site,
				obj.inProgressAssignments, '2',
				operandHelper.enumEqualsOperand, obj.region.number,
				replenishmentAssignmentSummaryObj.time_filter);
	} else {
		return obj.inProgressAssignments;
	}
};

painters.displayAssignmentsFilterByAvailable_replen = function(obj) {
	if (obj.availableAssignments > 0) {
		return getSummaryFilterLinkForAssignment_replen(obj.site,
				obj.availableAssignments, '1', operandHelper.enumEqualsOperand,
				obj.region.number, replenishmentAssignmentSummaryObj.time_filter);
	} else {
		return obj.availableAssignments;
	}
};

painters.displayAssignmentsFilterByInComplete_replen = function(obj) {
	if (obj.nonCompletedAssignments > 0) {
		return getSummaryFilterLinkForAssignment_replen(obj.site,
				obj.nonCompletedAssignments, '3',
				operandHelper.numberLessThanOperand, obj.region.number,
				replenishmentAssignmentSummaryObj.time_filter);
	} else {
		return obj.nonCompletedAssignments;
	}
};

painters.displayAssignmentDetails_replen = function(obj) {
	if (obj.detailCount > 0) {
		var filter = "?submittedFilterCriterion={'viewId':'-1029','columnId':'-12713','operandId':'-4','value1':'"
				+ obj.number + "','value2':'','critNum':1,'locked':false}";
		return painters.builders.link({
			'href' : globals.BASE
					+ '/replenishment/assignmentDetail/list.action' + filter
		}, obj.detailCount);
	} else {
		return obj.detailCount;
	}
};

painters.displayIssuanceOrder_replen = function(obj) {
	return obj.issuanceOrder.aggregate;
};

painters.displayItemNumber_replen = function(obj) {
	return painters.builders.link({
		'itemId' : obj.item.number,
		'href' : globals.BASE + '/replenishment/item/view.action?itemId='
				+ obj.item.id
	}, obj.item.number);
};

painters.displayItemNumber_replenDetail = function(obj) {
	return painters.builders.link({
		'itemId' : obj.replenishment.item.number,
		'href' : globals.BASE + '/replenishment/item/view.action?itemId='
				+ obj.replenishment.item.id
	}, obj.replenishment.item.number);
};

painters.displayReserveLocation_replen = function(obj) {
	return painters.builders.link({
		'locationId' : obj.replenishment.fromLocation.id,
		'href' : globals.BASE
				+ '/replenishment/location/view.action?locationId='
				+ obj.replenishment.fromLocation.id
	}, obj.replenishment.fromLocation.scannedVerification);
};

painters.displayDestinationLocation_replen = function(obj) {
	if (obj.replenishLocation) {
		return painters.builders.link({
			'locationId' : obj.replenishLocation.id,
			'href' : globals.BASE
					+ '/replenishment/location/view.action?locationId='
					+ obj.replenishLocation.id
		}, obj.replenishLocation.scannedVerification);
	}
};

painters.displayAllowCancelLicense_replen = function(obj) {
	return getBooleanTranslation_replen(obj.allowCancelLicense);
};

painters.displayAllowOverrideLocation_replen = function(obj) {
	return getBooleanTranslation_replen(obj.allowOverrideLocation);
};

painters.displayAllowPartialPut_replen = function(obj) {
	return getBooleanTranslation_replen(obj.allowPartialPut);
};

painters.displayCapturePickUpQty_replen = function(obj) {
	return getBooleanTranslation_replen(obj.capturePickUpQty);
};

painters.displayAllowOverridePickUpQty_replen = function(obj) {
	return getBooleanTranslation_replen(obj.allowOverridePickUpQty);
};

painters.displayCapturePutQty_replen = function(obj) {
	return getBooleanTranslation_replen(obj.capturePutQty);
};

painters.displayVerifySpokenLicenseLocation_replen = function(obj) {
	return getBooleanTranslation_replen(obj.verifySpokenLicenseLocation);
};

painters.displayNumberLocationDigits_replen = function(obj) {
	if (obj.locDigitsOperSpeaks == 0) {
		return "All";
	} else {
		return obj.locDigitsOperSpeaks;
	}
};

painters.displayNumberCheckDigits_replen = function(obj) {
	if (obj.checkDigitsOperSpeaks == 0) {
		return "All";
	} else {
		return obj.checkDigitsOperSpeaks;
	}
};
