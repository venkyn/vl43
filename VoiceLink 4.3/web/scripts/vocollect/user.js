/**
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 *
 * Client Side User Authentication
 *
 */
function changeAuthenicationValue(obj) {
    hiddenAuthenticateAgainstDirectoryService = $('hiddenAuthenticateAgainstDirectoryService');

    var passwordInput = document.getElementById('user.password');
    var confirmPasswordInput = document.getElementById('user.confirmPassword');

    var controlGroupDiv = passwordInput.parentNode.parentNode;
    var passwordLabel = controlGroupDiv.getElementsByTagName("label")[0];
    controlGroupDiv = confirmPasswordInput.parentNode.parentNode;
    var confirmPasswordLabel = controlGroupDiv.getElementsByTagName("label")[0];

    if (obj.checked == true) {
	hiddenAuthenticateAgainstDirectoryService.value = 'true';
	passwordInput.disabled = true;
	confirmPasswordInput.disabled = true;
	passwordLabel.className = "label disabled";
	confirmPasswordLabel.className = "label disabled";
	passwordInput.value = "********";
	confirmPasswordInput.value = "********";
	passwordInput.touched = false;
	confirmPasswordInput.touched = false;
    } else {
	hiddenAuthenticateAgainstDirectoryService.value = 'false';
	passwordInput.disabled = false;
	confirmPasswordInput.disabled = false;
	passwordLabel.className = "label";
	confirmPasswordLabel.className = "label";
	passwordInput.value = "";
	confirmPasswordInput.value = "";
	passwordInput.touched = true;
	confirmPasswordInput.touched = true;
    }
}

function enablePasswordFields() {
    // We are submitting the form.  If even if the checkbox for directory
    // service authentication is checked, the disabled password fields will
    // not pass validation
    var passwordInput = $('user.password');
    var confirmPasswordInput = $('user.confirmPassword');
    passwordInput.disabled = false;
    confirmPasswordInput.disabled = false;

    // If the checkbox is selected - then set the mask in these password fields
    hiddenAuthenticateAgainstDirectoryService = $('hiddenAuthenticateAgainstDirectoryService');
    if (hiddenAuthenticateAgainstDirectoryService != null
	    && hiddenAuthenticateAgainstDirectoryService.value == 'true') {
	passwordInput.value = "********";
	confirmPasswordInput.value = "********";
	passwordInput.touched = true;
	confirmPasswordInput.touched = true;
    }
}
