/**
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 *
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 *
 * A Dom Utility
 *
 */

function getX(ele) {
    return YAHOO.util.Dom.getX(ele);
}

function getY(ele) {
    return YAHOO.util.Dom.getY(ele);
}

function moveAbsolute(ele, x, y) {
    YAHOO.util.Dom.setXY(ele, [ x, y ]);
}

function moveRelative(ele, x, y) {
    var curPos = YAHOO.util.Dom.getXY(ele);
    YAHOO.util.Dom.setX(ele, curPos[0] + x);
    YAHOO.util.Dom.setY(ele, curPos[1] + y);
}

function addClass(oElement, className) {
    YAHOO.util.Dom.addClass(oElement, className);
}

function voc_createDOM(par, type, id, klass) {
    var ele = document.createElement(type);
    if (id) {
	ele.id = id;
    }
    if (klass) {
	addClass(klass);
    }

    par.appendChild(ele);

    return ele;
}

function setStyle(oElement, prop, val) {
    YAHOO.util.Dom.setStyle(oElement, prop, val);
}

function addText(oElement, text) {
    oElement.appendChild(document.createTextNode(text));
}

function appendChildren(oElement, children) {
    for ( var i = 0; i < children.length; i++) {
	oElement.appendChild(children[i]);
    }
}

function setAttributes(oElement, dict) {
    for (i in oElement) {
	oElement.setAttribute(i, dict[i]);
    }
}
