/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Filter Selection Module for
 * Table Component 2.0
 */
FilterModule = function(table, presetFilters) {
    this.menuDivId = 'manageFilterDiv';
    this.columns = table.columns;
    this._colDlg;
    this.table = table;
    this.varName = table.name;
    this.operands;
    this.filterCriteriaCounter = 1;
    this.inProgressFilter = null;
    this.filterCriterion = presetFilters;

    this._filterElementIds = {
	FilterCriteria : this.ui_get('FilterCriteria'),
	FilterColumn : this.ui_get('FilterColumn'),
	FilterOperand : this.ui_get('FilterOperand'),
	FilterValueOne : this.ui_get('FilterValueOne'),
	FilterValueTwo : this.ui_get('FilterValueTwo'),
	FilterCritAddLink : this.ui_get('FilterCritAddLink'),
	FilterCritRemoveLink : this.ui_get('FilterCritRemoveLink'),
	FilterCritSaveLink : this.ui_get('FilterCritSaveLink')
    };
    this.manageFilterDivRef;

    this.tableId = table.id;
};

FilterModule.prototype = new MenuModule();

FilterModule.prototype.init = function() {
    this.operands = $j.parseJSON(asyncDataHolder.operandManager.data.operands);
    var manageFilterDiv = this.createManageFilterDiv();
    this.$j('#' + this.ui_get('menuContainer')).append(manageFilterDiv);
    this.manageFilterDivRef = manageFilterDiv;

    if (this.filterCriterion.length > 0) {
	this.buildPresetCriteria();
    }

    this.updateTitleBar();
};

FilterModule.prototype.createManageFilterDiv = function() {
    /* TODO: Maybe use jquery templating for this */

    var manageFilterContentDiv = $j('<div />', {
	'id' : this.ui_get('ManageFilterContent'),
	'class' : 'columnNamesContent filter'
    });
    var addFilterCriteriaDiv = $j('<div />', {
	"id" : this.ui_get('AddFilterCriteria')
    }).text(this.localizedStrings.noCriteriaSet);
    var addFilterCriteriaLink = $j(
	    '<a />',
	    {
		'href' : 'javascript:' + this.varName
			+ '.filter_module.startNewFilter();',
		'class' : 'filterLink'
	    }).text(this.localizedStrings.addFilterCrit);
    var manageFilterContainerDiv = $j('<div />', {
	'class' : 'columnNamesBar'
    });
    var manageFilterDiv = $j('<div />', {
	'id' : this.menuDivId,
	'class' : 'fullMenu',
	'style' : 'display: none; width:100%;'
    });
    var manageFilterCloseDiv = $j('<div />', {
	'id' : this.ui_get('ManageFilterDivClose'),
	'class' : 'closeRestoreBar'
    });
    var hideLink = $j(
	    '<a />',
	    {
		'href' : 'javascript:this.' + this.varName
			+ '.filter_module.toggleMenu();'
	    }).text(this.localizedStrings.close);

    addFilterCriteriaDiv.append(addFilterCriteriaLink);
    manageFilterContentDiv.append(addFilterCriteriaDiv);
    manageFilterContainerDiv.append(manageFilterContentDiv);
    manageFilterDiv.append(manageFilterContainerDiv);
    manageFilterContainerDiv.append(manageFilterCloseDiv.append(hideLink));

    return manageFilterDiv;
};

FilterModule.prototype.startNewFilter = function() {
    var colTable = $j('<div />');
    colTable.append($j('<div />', {
	'class' : 'title'
    }).text(this.localizedStrings.filterTableTitle));

    var headerTable = $j('<table />', {
	'id' : 'atable',
	'class' : 'yahootable'
    });

    var filterColumnIndex = 0;
    var tr = null;
    var td = null;
    var self = this;
    var columns_sorted = this.columns.slice(0).sort(keyComparator("name"));
    for ( var i = 0; i < columns_sorted.length; i++) {
	if (columns_sorted[i].filterType != "NoFilter") {
	    if (filterColumnIndex % 4 == 0) {
		if (tr)
		    headerTable.append(tr);
		tr = $j('<tr />');
	    }
	    td = $j('<td />', {
		'class' : 'border'
	    });
	    tr.append(td);
	    var aLink = $j('<a />', {
		'href' : 'javascript:' + this.varName
			+ '.filter_module.addAFilter("' + columns_sorted[i].id
			+ '");'
	    });
	    aLink.text(columns_sorted[i].name);
	    td.append(aLink);
	    filterColumnIndex++;
	}
    }

    if (tr)
	headerTable.append(tr);
    colTable.append(headerTable);

    this._colDlg = new YAHOO.widget.Panel("_colDlg", {
	fixedcenter : true,
	constraintoviewport : true,
	underlay : "none",
	close : false,
	visible : false,
	draggable : false
    });

    var closeDiv = $j('<div />', {
	'class' : 'button'
    });
    var closeDivLink = $j('<a />', {
	'class' : 'anchorButton',
	'href' : 'javascript:' + this.varName
		+ '.filter_module.closeFilterDlg();'
    });
    closeDivLink.text(this.localizedStrings.cancel);
    colTable.append(closeDiv.append(closeDivLink));
    closeDivLink.click(function() {
	self.closeFilterDlg();
    });

    this._colDlg.setBody(colTable.html());
    this._colDlg.render(document.body);
    this._colDlg.show();
};

FilterModule.prototype.closeFilterDlg = function() {
    if (this._colDlg) {
	this._colDlg.hide();
    }
};

FilterModule.prototype.addAFilter = function(filterId) {
    this.closeFilterDlg();
    var selectedIndex = -1;
    var critNumber = this.filterCriteriaCounter;
    this.filterCriteriaCounter++;
    this.inProgressFilter = critNumber;

    var columnSelectBox = $j('<select />', {
	'selected' : filterId,
	'id' : this.ui_get('FilterColumn') + critNumber,
	'class' : 'filterDropDown',
	'tid' : this.ui_get('FilterColumnSelect') + critNumber
    });
    var ind = 0;
    var columns_sorted = this.columns.slice(0).sort(keyComparator("name"));
    for ( var i = 0; i < columns_sorted.length; i++) {
	if (columns_sorted[i].filterType != "NoFilter") {
	    if (filterId == columns_sorted[i].id + '') {
		selectedIndex = columns_sorted[i].id;
	    }
	    columnSelectBox.append($j('<option />', {
		'value' : columns_sorted[i].id
	    }).text(columns_sorted[i].name));
	    ind++;
	}
    }

    columnSelectBox.val(selectedIndex);

    if (this.$j('#' + this.ui_get('AddFilterCriteria'))) {
	this.$j('#' + this.ui_get('AddFilterCriteria')).remove();
    }

    this.manageFilterDivRef.find('#' + this.ui_get('ManageFilterContent'))
	    .append($j('<div />', {
		'id' : this.ui_get('FilterCriteria') + critNumber
	    }).append(columnSelectBox));
    var self = this;
    columnSelectBox.change(function(e) {
	self.handleFilterColumnSelection(critNumber, e);
    });
    this.handleFilterColumnSelection(critNumber);
};

FilterModule.prototype.handleFilterColumnSelection = function(critNumber, e) {
    var selectedValue = this.$j('#' + this.ui_get('FilterColumn') + critNumber)
	    .val();
    for ( var i = 0; i < this.columns.length; i++) {
	if (this.columns[i].id == selectedValue) {
	    var operandType = this.columns[i].operandType;
	    var operands = this.getOperandsByType(operandType);
	    var selectedIndex = -1;

	    this.$j('#' + this.ui_get('FilterOperand') + critNumber).remove();
	    this.$j('#' + this.ui_get('FilterValueOne') + critNumber).remove();
	    this.$j('#' + this.ui_get('FilterValueTwo') + critNumber).remove();
	    this.$j('#' + this.ui_get('FilterCritAddLink') + critNumber)
		    .remove();

	    var operandSelectBox = $j('<select />', {
		'id' : this.ui_get('FilterOperand') + critNumber,
		'class' : 'filterDropDown',
		'tid' : this.ui_get('FilterOperandSelect') + critNumber
	    });
	    this.$j('#' + this.ui_get('FilterCriteria') + critNumber).append(
		    operandSelectBox);

	    for ( var j = 0; j < operands.length; j++) {
		this.$j('#' + this.ui_get('FilterOperand') + critNumber)
			.append($j('<option />', {
			    'value' : operands[j].id
			}).text(itext(operands[j].label)));
		if (itext(operands[j].label) == filterEqualTo)
		    selectedIndex = j;
	    }

	    if (selectedIndex != -1)
		operandSelectBox.selectedIndex = selectedIndex;
	    var localizedOptions =  this.localizedStrings;
	    var self = this;
	    operandSelectBox.change(function(e) {
		self.handleFilterOperandSelection(critNumber, operandType,
			localizedOptions, selectedValue, e);
	    });
	    this.handleFilterOperandSelection(critNumber, operandType,
		    localizedOptions, selectedValue, null, null);
	}
    }
};

FilterModule.prototype.handleFilterOperandSelection = function(critNumber,
	operandType, localizedOptions, columnId, event, criterion) {
    this.$j('#' + this.ui_get('FilterValueOne') + critNumber).remove();
    this.$j('#' + this.ui_get('FilterValueTwo') + critNumber).remove();
    this.$j('#' + this.ui_get('FilterCritAddLink') + critNumber).remove();
    var selectedValue = this
	    .$j('#' + this.ui_get('FilterOperand') + critNumber).val();
    var operands = this.getOperandsByType(operandType);
    for ( var i = 0; i < operands.length; i++) {
	if (operands[i].id == selectedValue) {
	 // A lookup based on the operand's function for creating the input element.
		// The method is then executed with the appropriate arguments
		// Implementation in filterUtility.js
	    filterUtility[operands[i].inputFunction](critNumber, this.table,
		    this.table.viewId, criterion, this._filterElementIds,
		    localizedOptions, false, columnId);
	}
    }
};

FilterModule.prototype.getOperandsByType = function(type) {
    var operandsByType = [];
    var idx = 0;
    for ( var i = 0; i < this.operands.length; i++) {
	if (this.operands[i].type === type) {
	    operandsByType[idx] = this.operands[i];
	    idx++;
	}
    }

    return operandsByType;
};

FilterModule.prototype.getOperandById = function(id) {
    for ( var i = 0; i < this.operands.length; i++) {
	if (this.operands[i].id + '' === id) {
	    return this.operands[i];
	}
    }
    return null;
};

FilterModule.prototype.saveNewFilterCriteria = function(critNumber, presetCrit) {
    var control = this.$j('#' + this.ui_get('FilterValueOne') + critNumber);
    var control2 = this.$j('#' + this.ui_get('FilterValueTwo') + critNumber);
    var parent = this.$j('#' + this.ui_get('FilterCriteria') + critNumber);
    var operand = this.$j('#' + this.ui_get('FilterOperand') + critNumber);
    var column = this.$j('#' + this.ui_get('FilterColumn') + critNumber);
    var validateResult = this.validateFilterInputText(parent, column, operand,
	    control);

    if(control2 != null) {
		var validateResult2 = this.validateFilterInputText(parent, column, operand,
			    control2);
		if (validateResult2 != -1) {
			if (!validateResult2) {
			    return;
			}
		}	
	}
    if (validateResult != -1) {
	if (!validateResult) {
	    return;
	}
	
	
	if (presetCrit == null) {
	    this.buildFilterAndUpdate(critNumber);
	}
	control.addClass("savedCriteria");
	if (this.$j('#' + this.ui_get('FilterCritAddLink') + critNumber).length > 0) {
	    this.$j('#' + this.ui_get('FilterCritAddLink') + critNumber)
		    .remove();
	    this
		    .$j('#' + this.ui_get('FilterCriteria') + critNumber)
		    .append(
			    $j(
				    '<a />',
				    {
					'id' : this
						.ui_get('FilterCritRemoveLink')
						+ critNumber,
					'href' : 'javascript:'
						+ this.varName
						+ '.filter_module.removeFilterCriteria("'
						+ critNumber + '");',
					'class' : 'filterLink'
				    }).text(this.localizedStrings.remove));
	}
	column.attr('disabled', 'disabled');
	operand.attr('disabled', 'disabled');

	if (control) {
	    var self = this;
	    control.focus(function() {
		self.handleValueFocus(critNumber);
	    });
	}
	
	if (control2) {
	    var self = this;
	    control2.focus(function() {
		self.handleValueFocus(critNumber);
	    });
	}

	this.appendAddNewCriteriaLink();

	if (this.filterCriterion.length > 0) {
	    replaceChildNodes(this.ui_get('ManageFilterDivClose'), A({
		'href' : 'javascript: bookmarkPage();'
	    }, this.localizedStrings.bookmarkPage), " | ", A({
		'href' : 'javascript: ' + this.varName
			+ '.filter_module.clearAllCriteria();'
	    }, this.localizedStrings.clearCriteria), " | ", A({
		'href' : 'javascript: ' + this.varName
			+ '.filter_module.toggleMenu();'
	    }, this.localizedStrings.close));
	}
	inProgressFilter = null;
	this.updateTitleBar();
	$j(this.table).trigger(this.table.events.FILTER_UPDATE);
    }
};

FilterModule.prototype.validateFilterInputText = function(parent, column,
	operand, control) {
    if (!(parent && column && operand && control)) {
	return -1;
	// Fail to run test
    }
    var isSelect = control.is('select');
    var value = control.val();
    if (parent.hasClass('filterBoxError')) {
	return 0;
	// Already invalid
    }
    if ((isSelect && value == this.NO_VALUE_SELECTED)
	    || (!value && (operand.value != this.OPERAND_STRING_EQUALS && operand.value != OPERAND_NUM_EQUALS))) {
	parent.addClass('filterBoxError');
	var errorMsg = $j('<span />', {
	    'class' : 'errorText'
	}).text(this.localizedStrings.filterRequiredString);
	parent.append(errorMsg);

	/* TODO: Handle new events to remove errors. */
	control.focus();
	return 0;
    }
    return 1;
};

FilterModule.prototype.appendAddNewCriteriaLink = function() {
    if (this.$j('#' + this.ui_get('AddFilterCriteria')).length > 0) {
	this.$j('#' + this.ui_get('AddFilterCriteria')).remove();
    }
    if (this.$j('#' + this.ui_get('ManageFilterContent')).length > 0) {
	if (this.filterCriterion.length == 0) {
	    this
		    .$j('#' + this.ui_get('ManageFilterContent'))
		    .append(
			    $j('<div />', {
				'id' : this.ui_get('AddFilterCriteria')
			    })
				    .text(this.localizedStrings.noCriteriaSet)
				    .append(
					    $j(
						    '<a />',
						    {
							'href' : 'javascript:'
								+ this.varName
								+ '.filter_module.startNewFilter();',
							'class' : 'filterLink'
						    })
						    .text(
							    this.localizedStrings.addFilterCrit)));
	} else {
	    this
		    .$j('#' + this.ui_get('ManageFilterContent'))
		    .append(
			    $j('<div />', {
				'id' : this.ui_get('AddFilterCriteria')
			    })
				    .append(
					    $j(
						    '<a />',
						    {
							'href' : 'javascript:'
								+ this.varName
								+ '.filter_module.startNewFilter();',
							'class' : 'filterLink'
						    })
						    .text(
							    this.localizedStrings.addFilterCrit)));
	}
    }
};

FilterModule.prototype.buildFilterAndUpdate = function(critNumber) {
    if (this.$j('#' + this.ui_get('FilterValueTwo') + critNumber).length > 0) {
	valueTwo = this.$j('#' + this.ui_get('FilterValueTwo') + critNumber)
		.val();
    } else {
	valueTwo = "";
    }

    var vocFilterCriteria = {
	viewId : this.table.viewId,
	columnId : this.$j('#' + this.ui_get('FilterColumn') + critNumber)
		.val(),
	operandId : this.$j('#' + this.ui_get('FilterOperand') + critNumber)
		.val(),
	value1 : this.$j('#' + this.ui_get('FilterValueOne') + critNumber)
		.val(),
	value2 : valueTwo,
	critNum : critNumber,
	locked : false
    };

    this.filterCriterion.push(vocFilterCriteria);
    this.saveFilterCriteria(vocFilterCriteria);
    // this.table.updateTitleBar();
};

FilterModule.prototype.saveFilterCriteria = function(criteria) {
    this.table.loader.addFilter(criteria, function(crit, id) {
	crit.id = id;
    });
};

FilterModule.prototype.getFilterCriterion = function() {
    var jsonReturn = new Array();
    for ( var x = 0; x < this.filterCriterion.length; x++) {
	jsonReturn.push(MochiKit.Base.serializeJSON(this.filterCriterion[x]));
    }
    return jsonReturn;
};

FilterModule.prototype.buildPresetCriteria = function() {
    // Run through and do the locked ones first.
    for ( var i = 0; i < this.filterCriterion.length; i++) {
	if (this.filterCriterion[i].locked) {
	    var critNumber = this
		    .createFullLockedFilter(this.filterCriterion[i]);
	    this.filterCriterion[i].critNum = critNumber;
	}
    }
    // Now do the unlocked ones.
    for ( var i = 0; i < this.filterCriterion.length; i++) {
	if (!this.filterCriterion[i].locked) {
	    var critNumber = this.createFullFilter(this.filterCriterion[i]);
	    this.filterCriterion[i].critNum = critNumber;
	}
    }
    // updateTitleBar();
};

FilterModule.prototype.createFullLockedFilter = function(criterion) {
    var critNumber;
    if (criterion.critNum) {
	critNumber = criterion.critNum;
    } else {
	critNumber = this.filterCriteriaCounter;
    }
    this.filterCriteriaCounter++;
    var columnSpan = $j('<span />', {
	"id" : this.ui_get('FilterColumn') + critNumber,
	"class" : "filterLockedSpan"
    });

    for ( var i = 0; i < this.columns.length; i++) {
	if (criterion.columnId == this.columns[i].columnId) {
	    columnSpan.text(this.columns[i].name);
	}
    }
    if (this.$j('#' + this.ui_get('AddFilterCriteria'))) {
	this.$j('#' + this.ui_get('AddFilterCriteria')).remove();
    }
    this.$j('#' + this.ui_get('ManageFilterContent')).append($j('<div />', {
	"id" : this.ui_get('FilterCriteria') + critNumber
    }).append(columnSpan));

    var operand = this.getOperandById(criterion.operandId);
    var columnSpan = this.$j('<span />', {
	"id" : this.ui_get('FilterOperand') + critNumber,
	"class" : "filterLockedSpan"
    }).text(itext(operand.label));
    this.$j('#' + this.ui_get('FilterCriteria') + critNumber)
	    .append(columnSpan);

    var localizedOptions = [ this.localizedStrings.trueValue,
	    this.localizedStrings.falseValue ];

    window[operand.inputFunction](critNumber, this.table, this.table.viewId,
	    criterion, this._filterElementIds, localizedOptions, true,
	    criterion.columnId);
    this.$j('#' + this.ui_get('FilterCriteria') + critNumber).addClass(
	    "lockedCriteria");

    this.appendAddNewCriteriaLink();

    return critNumber;
};

FilterModule.prototype.createFullFilter = function(criterion) {
    var critNumber;
    if (criterion.critNum) {
	critNumber = criterion.critNum;
    } else {
	critNumber = this.filterCriteriaCounter;
    }
    this.filterCriteriaCounter++;
    var columnSelectBox = SELECT({
	"id" : this.ui_get('FilterColumn') + critNumber,
	"class" : "filterDropDown"
    }, OPTION({
	'value' : -427
    }, this.localizedStrings.selectAColumn));
    for ( var i = 0; i < this.columns.length; i++) {
	if (criterion.columnId == this.columns[i].id) {
	    appendChildNodes(columnSelectBox, OPTION({
		'value' : this.columns[i].id,
		'selected' : 'true'
	    }, this.columns[i].name));
	} else {
	    appendChildNodes(columnSelectBox, OPTION({
		'value' : this.columns[i].id
	    }, this.columns[i].name));
	}
    }
    if (this.$j('#' + this.ui_get('AddFilterCriteria'))) {
	this.$j('#' + this.ui_get('AddFilterCriteria')).remove();
    }
    appendChildNodes(this.ui_get('ManageFilterContent'), DIV({
	"id" : this.ui_get('FilterCriteria') + critNumber
    }, columnSelectBox));
    var self = this;
    this.$j('#' + this.ui_get('FilterColumn') + critNumber).change(function() {
	self.handleFilterColumnSelection.bind(self, critNumber);
    });
    var operand = this.getOperandById(criterion.operandId);
    var operands = this.getOperandsByType(operand.type);

    var operandSelectBox = SELECT({
	"id" : this.ui_get('FilterOperand') + critNumber,
	"class" : "filterDropDown"
    });
    appendChildNodes(this.ui_get('FilterCriteria') + critNumber,
	    operandSelectBox);

    appendChildNodes(this.ui_get('FilterOperand') + critNumber, OPTION({
	"value" : -427
    }, this.localizedStrings.selectAnOperand));
    for ( var j = 0; j < operands.length; j++) {
	if (operands[j].id == operand.id) {
	    appendChildNodes(this.ui_get('FilterOperand') + critNumber, OPTION(
		    {
			'value' : operands[j].id,
			'selected' : 'true'
		    }, itext(operands[j].label)));
	} else {
	    appendChildNodes(this.ui_get('FilterOperand') + critNumber, OPTION(
		    {
			'value' : operands[j].id
		    }, itext(operands[j].label)));
	}
    }
    var localizedOptions = [ this.localizedStrings.trueValue,
	    this.localizedStrings.falseValue ];

    var self = this;
    this.$j('#' + this.ui_get('FilterOperand') + critNumber).change(
	    function() {
		self.handleFilterOperandSelection.bind(self, critNumber,
			operand.type, localizedOptions, null);
	    });
    // filterConnects["OperandSelect" + critNumber] = connect(FilterOperand +
    // critNumber, "onchange",
    // partial(handleFilterOperandSelection,critNumber,operand.type,localizedOptions));
    this.handleFilterOperandSelection(critNumber, operand.type,
	    localizedOptions, criterion.columnId, null, criterion);
    this.saveNewFilterCriteria(critNumber, true);
    return critNumber;
};

FilterModule.prototype.removeFilterCriteria = function(critNumber) {
    this.deleteFilterCriterion(critNumber);
    this.removeUIFilterCriteria(critNumber);
    this.updateTitleBar();
    $j(this.table).trigger(this.table.events.FILTER_UPDATE);
};

FilterModule.prototype.deleteFilterCriterion = function(critNumber) {
    var critId = null;
    for ( var i = 0; i < this.filterCriterion.length; i++) {
	if (this.filterCriterion[i].critNum == critNumber) {
	    critId = this.filterCriterion[i].id;
	}
    }

    if (critId != null) {
	this.table.loader.deleteFilter(critId);
    }
};

FilterModule.prototype.removeUIFilterCriteria = function(critNumber) {
    if (this.$j('#' + this.ui_get('FilterCriteria') + critNumber).length > 0) {
	this.$j('#' + this.ui_get('FilterCriteria') + critNumber).remove();
    }
    this.removeFilterCriterionFromLocalVar(critNumber);
    if (this.$j('#' + this.ui_get('AddFilterCriteria')).length > 0) {
	this.$j('#' + this.ui_get('AddFilterCriteria')).remove();
    }
    if (this.$j('#' + this.ui_get('ManageFilterDivClose')).length > 0) {
	if (this.filterCriterion.length == 0) {
	    replaceChildNodes(this.ui_get('ManageFilterDivClose'), A({
		'href' : 'javascript: ' + this.varName
			+ '.filter_module.toggleMenu();'
	    }, this.localizedStrings.close));
	}
    }

    this.appendAddNewCriteriaLink();
};

FilterModule.prototype.removeFilterCriterionFromLocalVar = function(critNumber) {
    _newFilterCriterion = new Array();
    while (this.filterCriterion.length > 0) {
	var filtercrit = this.filterCriterion.pop();
	if (filtercrit.critNum != critNumber) {
	    _newFilterCriterion.push(filtercrit);
	}
    }
    this.filterCriterion = _newFilterCriterion;
};

FilterModule.prototype.handleValueFocus = function(critNumber) {
    if ($(this.ui_get('FilterCritSaveLink') + critNumber) == null) {
	removeElement(this.ui_get('FilterCritRemoveLink') + critNumber);
	appendChildNodes(this.ui_get('FilterCriteria') + critNumber, A({
	    "id" : this.ui_get('FilterCritSaveLink') + critNumber,
	    "href" : "javascript: " + this.varName
		    + ".filter_module.saveChangesFilterCriteria(\""
		    + critNumber + "\" );",
	    "class" : "filterLink"
	}, this.localizedStrings.saveChanges), SPAN({
	    "id" : this.ui_get('FilterSpacer') + critNumber
	}, " | "), A({
	    "id" : this.ui_get('FilterCritRemoveLink') + critNumber,
	    "href" : "javascript: " + this.varName
		    + ".filter_module.removeFilterCriteria(\"" + critNumber
		    + "\" );",
	    "class" : "filterLink"
	}, this.localizedStrings.remove));
    }
};

FilterModule.prototype.saveChangesFilterCriteria = function(critNumber) {
    var control = this.$j('#' + this.ui_get('FilterValueOne') + critNumber);
    var control2 = this.$j('#' + this.ui_get('FilterValueTwo') + critNumber);
    var parent = this.$j('#' + this.ui_get('FilterCriteria') + critNumber);
    var operand = this.$j('#' + this.ui_get('FilterOperand') + critNumber);
    var column = this.$j('#' + this.ui_get('FilterColumn') + critNumber);
    var validateResult = this.validateFilterInputText(parent, column, operand,
	    control);
    if (validateResult != -1) {
	if (!validateResult) {
	    return;
	}

	var crit = null;
	for ( var i = 0; i < this.filterCriterion.length; i++) {
	    if (this.filterCriterion[i].critNum == critNumber) {
		if ($(this.ui_get('FilterValueOne') + critNumber)) {
		    this.filterCriterion[i].value1 = this.$j(
			    '#' + this.ui_get('FilterValueOne') + critNumber)
			    .val();
		}
		if ($(this.ui_get('FilterValueTwo') + critNumber)) {
		    this.filterCriterion[i].value2 = this.$j(
			    '#' + this.ui_get('FilterValueTwo') + critNumber)
			    .val();
		}
		crit = this.filterCriterion[i];
		this.table.loader.editFilter(crit);
	    }
	}
	removeElement(this.ui_get('FilterSpacer') + critNumber);
	removeElement(this.ui_get('FilterCritSaveLink') + critNumber);

	this.updateTitleBar();
	$j(this.table).trigger(this.table.events.FILTER_UPDATE);
    }
};

FilterModule.prototype.clearAllCriteria = function() {
    var removableCritIds = new Array();
    var removableCritNums = new Array();
    for ( var i = 0; i < this.filterCriterion.length; i++) {
	if (this.filterCriterion[i].id) {
	    removableCritIds.push(this.filterCriterion[i].id);
	}
	if (!this.filterCriterion[i].locked) {
	    removableCritNums.push(this.filterCriterion[i].critNum);
	}
    }
    this.table.loader.deleteFilters(removableCritIds);
    for ( var i = 0; i < removableCritNums.length; i++) {
	this.removeUIFilterCriteria(removableCritNums[i]);
    }
    this.updateTitleBar();
    $j(this.table).trigger(this.table.events.FILTER_UPDATE);
};

FilterModule.prototype.updateTitleBar = function() {
    var columnNames = "";
    var lockedColumnNames = "";
    var removableSpan = null;

    for ( var i = 0; i < this.filterCriterion.length; i++) {
	for ( var j = 0; j < this.columns.length; j++) {
	    if (this.filterCriterion[i].columnId + "" === this.columns[j].id
		    + "") {
		if (this.filterCriterion[i].locked) {
		    if (lockedColumnNames.length > 0) {
			lockedColumnNames += ", ";
		    } else {
			lockedColumnNames += this.localizedStrings.filteredBy
				+ " ";
		    }
		    lockedColumnNames += this.columns[j].name;
		} else {
		    if (removableSpan != null) {
			columnNames += ", ";
		    } else {
			var imageSrc;
			if (!this.$j('#' + this.ui_get('TitleBarDiv'))
				.hasClass("unfocus")) {
			    imageSrc = globals.BASE
				    + '/images/removeFilters.gif';
			} else {
			    imageSrc = globals.BASE
				    + '/images/removeFilters_unfocus.gif';
			}
			removableSpan = $j('<span />', {
			    'class' : 'removeFilter'
			})
				.append(
					$j(
						'<a />',
						{
						    href : 'javascript:'
							    + this.varName
							    + '.filter_module.clearAllCriteria()',
						    tid : 'removeFilter'
						})
						.append(
							$j(
								'<img />',
								{
								    id : this
									    .ui_get('RemoveFilterImg'),
								    style : 'display:inline;border:none;left:-2px;top:3px;position:relative;',
								    src : imageSrc
								})));
		    }
		    columnNames += this.columns[j].name;
		}
	    }
	}
    }

    if ($(this.ui_get('TitleFilterInfo'))) {
	removeElement(this.ui_get('TitleFilterInfo'));
    }

    if (lockedColumnNames.length > 0) {
	this.$j('#' + this.ui_get('TitleBarDiv')).append($j('<span />', {
	    'id' : this.ui_get('TitleFilterInfo'),
	    'class' : 'titleBarColumnNames'
	}).text(lockedColumnNames));

	if (removableSpan != null) {
	    $j(removableSpan).append(document.createTextNode(columnNames));
	    this.$j(this.ui_get('TitleFilterInfo')).text(
		    " and (" + removableSpan + " )");
	}
    } else if (removableSpan != null) {
	$j(removableSpan).append(document.createTextNode(columnNames));
	this.$j('#' + this.ui_get('TitleBarDiv')).append($j('<span />', {
	    'id' : this.ui_get('TitleFilterInfo'),
	    'class' : 'titleBarColumnNames'
	}));
	this
		.$j('#' + this.ui_get('TitleFilterInfo'))
		.append(
			document
				.createTextNode(this.localizedStrings.filteredBy
					+ " ("));
	this.$j('#' + this.ui_get('TitleFilterInfo')).append(removableSpan);
	this.$j('#' + this.ui_get('TitleFilterInfo')).append(
		document.createTextNode(" )"));
    }
};

FilterModule.prototype.getBookmarkParameters = function() {
    var bookmark = "";
    for ( var i = 0; i < this.filterCriterion.length; i++) {
	if (i != 0) {
	    bookmark += "&";
	}
	bookmark += "submittedFilterCriterion="
		+ JSON.stringify(this.filterCriterion[i]);
    }
    return bookmark;
};
