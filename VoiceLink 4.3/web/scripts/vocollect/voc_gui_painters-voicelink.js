/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc.
 */

/* Globals */
var operandHelper = {
	stringStartsWithOperand : -1,
	stringEndsWithOperand : -2,
	stringContainsOperand : -3,
	stringEqualsOperand : -4,
	numberEqualsOperand : -11,
	numberGreaterThanOperand : -12,
	numberGreaterThanOrEqualOperand : -13,
	numberLessThanOperand : -14,
	numberLessThanOrEqualOperand : -15,
	numberBetweenOperand : -16,
    dateWithin : -23,
    dateNotWithin : -22,
    dateEmpty : -21,
	enumEqualsOperand : -31,
	enumNotEqualsOperand : -32,
	booleanEqualsOperand : -41,
	booleanNotEqualsOperand : -42
};

function displayNumberOfOperators(obj, app, filterType) {
	if (obj.numberOfOperators > 0) {
		var filter = "&"
				+ getFilter(-1022, '-12109', obj.region.number,
						operandHelper.numberEqualsOperand);
		/* Figure out the table component name. */
		var laborTableObj = null;
		if (app == "selection") {
			laborTableObj = selectionLaborSummaryObj;
		} else if (app == "putaway") {
			laborTableObj = putawayLaborSummaryObj;
		} else if (app == "replenishment") {
			laborTableObj = replenishmentLaborSummaryObj;
		} else if (app == "lineloading") {
			laborTableObj = lineloadingLaborSummaryObj;
		} else if (app == "puttostore") {
			laborTableObj = puttostoreLaborSummaryObj;
		} else if (app == "cyclecounting") {
			laborTableObj = cyclecountingLaborSummaryObj;
		} else if (app == "loading") {
			laborTableObj = loadingLaborSummaryObj;
		}
		return painters.builders.link({
			"href" : globals.BASE + "/" + app
					+ "/labor/list.action?summarySiteID=" + obj.site.id
					+ "&timeFilterSaved="
					+ laborTableObj.time_window_module.getTimeFilter() + filter
		}, obj.numberOfOperators);
	} else {
		return obj.numberOfOperators;
	}
}

painters.displayNumberOfOperators_sel = function(obj) {
	return displayNumberOfOperators(obj, "selection", 1);
};

painters.displayNumberOfOperators_put = function(obj) {
	return displayNumberOfOperators(obj, "putaway", 2);
};

painters.displayNumberOfOperators_rep = function(obj) {
	return displayNumberOfOperators(obj, "replenishment", 3);
};

painters.displayNumberOfOperators_lin = function(obj) {
	return displayNumberOfOperators(obj, "lineloading", 3);
};

painters.displayNumberOfOperators_pts = function(obj) {
	return displayNumberOfOperators(obj, "puttostore", 5);
};

painters.displayNumberOfOperators_cc = function(obj) {
	return displayNumberOfOperators(obj, "cyclecounting", 6);
};

painters.displayNumberOfOperators_load = function(obj) {
	return displayNumberOfOperators(obj, "loading", 7);
};

painters.displayLocationDescription = function(obj) {
	return obj.location.description.aggregate;
};

painters.displayCustomerLocationDescription = function(obj) {
	return obj.customer.location.description.aggregate;
};

painters.displayTotalWeight = function(obj) {
	// log("object.totalWeight: '" + obj.summaryInfo.totalWeight + "'");
	return (obj.summaryInfo.totalWeight).toFixed(2);
};

painters.displayWorkgroup = function(obj) {
	if (obj.workgroup != null) {
		return painters.builders.link({
			'href' : globals.PAGE_CONTEXT
					+ '/workgroup/view.action?workgroupId=' + obj.workgroup.id
		}, obj.workgroup.groupName);
	}
};

painters.displayWorkgroupFunctions = function(obj) {
	var containerDiv = '<div class="longline">';
	for ( var i = 0; i < obj.workgroupFunctions.length; i++) {
		var func = obj.workgroupFunctions[i];
		var link;
		if (func.regionType == 1) {
			link = globals.BASE + '/selection/region/list.action';
		} else if (func.regionType == 2) {
			link = globals.BASE + '/putaway/region/list.action';
		} else if (func.regionType == 3) {
			link = globals.BASE + '/replenishment/region/list.action';
		} else if (func.regionType == 4) {
			link = globals.BASE + '/lineloading/region/list.action';
		} else if (func.regionType == 5) {
			link = globals.BASE + '/puttostore/region/list.action';
		} else if (func.regionType == 6) {
			link = globals.BASE + '/cyclecounting/region/list.action';
		} else if (func.regionType == 7) {
			link = globals.BASE + '/loading/region/list.action';
		} else {
			log("unknown regionType: " + func.regionType);
		}
		containerDiv += painters.builders.link({
			'href' : link
		}, func.functionType) + " ";
	}
	containerDiv += '</div>';
	return containerDiv;
};

painters.displayWorkgroupName = function(obj) {
	var link = painters.builders.link({
		'href' : globals.PAGE_CONTEXT + '/workgroup/view.action?workgroupId='
				+ obj.id
	}, obj.groupName);
	if (obj.isDefault) {
		return "<span>" + link + " " + itext('workgroup.view.label.default')
				+ "</span>";
	} else {
		return link;
	}
};

painters.displayOperatorCount = function(obj) {
	if (obj.operatorCount > 0) {
		var filter = "?" + getFilter(-1005, -10409, obj.groupName, -4);
		return painters.builders.link({
			'href' : globals.PAGE_CONTEXT + '/operator/list.action' + filter
		}, obj.operatorCount);
	} else {
		return obj.operatorCount;
	}
};

painters.displayOperatorTeams = function(obj) {
	if (obj.operatorTeams) {
		return map(partial(formatOperatorTeam, obj.id), obj.operatorTeams);
	} else {
		return " ";
	}
};

painters.displayOperatorIdentifier = function(obj) {
	return painters.builders.link({
		'operatorId' : obj.common.operatorIdentifier,
		'href' : globals.PAGE_CONTEXT + '/operator/view.action?operatorId='
				+ obj.id
	}, obj.common.operatorIdentifier);
};

painters.displayRegions = function(obj) {
	var returnString = "";
	for ( var i = 0; i < obj.regions.length; i++) {
		if (i == obj.regions.length - 1) {
			returnString += obj.regions[i].name;
		} else {
			returnString += obj.regions[i].name + ", ";
		}
	}
	return returnString;
};

painters.displayItemNumber = function(obj) {
	return painters.builders.link({
		'id' : obj.number,
		'href' : globals.PAGE_CONTEXT + '/item/view.action?itemId=' + obj.id
	}, obj.number);
};

painters.displayVarWeightItem = function(obj) {
	if (obj.isVariableWeightItem) {
		return itext('item.isVariableWeightItem.true');
	} else {
		return itext('item.isVariableWeightItem.false');
	}
};

painters.displayVarCaptureSerialNumbers = function(obj) {
	if (obj.isSerialNumberItem) {
		return itext('item.isCaptureSerialNumbers.true');
	} else {
		return itext('item.isCaptureSerialNumbers.false');
	}
};

painters.displayLocationId = function(obj) {
	return painters.builders.link({
		'href' : globals.PAGE_CONTEXT + '/location/view.action?locationId='
				+ obj.id
	}, obj.scannedVerification);
};

painters.displayLocationIdChildObject = function(obj) {
	return painters.builders.link({
		'href' : globals.PAGE_CONTEXT + '/location/view.action?locationId='
				+ obj.location.id
	}, obj.location.scannedVerification);
};

painters.displayTotalLots = function(obj) {
	if (obj.lotCount > 0) {
		var filter = "?submittedFilterCriterion={'viewId':'-1041','columnId':'-11703','operandId':'-4','value1':'"
				+ obj.number + "','value2':'','locked':false}";
		return painters.painters.builders.link({
			'href' : globals.PAGE_CONTEXT + '/lot/list.action?' + filter
		}, obj.lotCount);
	} else {
		return obj.lotCount;
	}
};

painters.displayCheckResponse = function(obj) {
	if (obj.checkResponse == -1) {
		return itext('vsc.response.pass');
	} else if (obj.checkResponse == -2) {
		return itext('vsc.response.fail');
	} else if (obj.checkResponse == -4) {
		return itext('vsc.response.failButContinued');
	} else {
		return obj.checkResponse;
	}
};

painters.displayBreakType = function(obj) {
	return painters.builders.link({
		'href' : globals.PAGE_CONTEXT + '/breakType/view.action?breakTypeId='
				+ obj.id
	}, obj.name);
};

painters.displayCoreRegions = function(obj) {
	return painters.builders.link({
		'regionId' : obj.name,
		'href' : globals.PAGE_CONTEXT
				+ '/region/viewRegion!input.action?regionId=' + obj.id
	}, obj.name);
};

painters.displayNumberLocationDigits = function(obj) {
	if (obj.locDigitsOperSpeaks == 0) {
		return "All";
	} else {
		return obj.locDigitsOperSpeaks;
	}
};

painters.displayNumberCheckDigits = function(obj) {
	if (obj.checkDigitsOperSpeaks == 0) {
		return "All";
	} else {
		return obj.checkDigitsOperSpeaks;
	}
};

painters.displayActionType = function(obj) {
	if (obj.actionType == "Selection") {
		return painters.builders.link({
			'href' : globals.BASE
					+ '/selection/labor/assignmentlist.action?operatorLaborId='
					+ obj.id
		}, obj.actionType);
	} else if (obj.actionType == "Break") {
		return "<span>"
				+ obj.actionType
				+ " ("
				+ painters.builders.link({
					'href' : globals.PAGE_CONTEXT
							+ '/breakType/view.action?breakTypeId='
							+ obj.breakType.id
				}, obj.breakType.name) + ")" + "</span>";
	} else {
		return obj.actionType;
	}
};

painters.displayRegion = function(obj) {
	return painters.builders.link({
		'href' : globals.PAGE_CONTEXT + '/region/view.action?regionId='
				+ obj.region.id
	}, obj.region.name);
};

painters.displayRegionName = function(obj) {
	return painters.builders.link({
		'href' : globals.PAGE_CONTEXT + '/region/view.action?regionId='
				+ obj.id
	}, obj.name);
};

painters.displayLicenseRegionName = function(obj) {
	return painters.builders.link({
		'href' : globals.PAGE_CONTEXT + '/region/view.action?regionId='
				+ obj.license.region.id
	}, obj.license.region.name);
};

painters.displayPrinterStatus = function(val) {
	if (val.isEnabled) {
		return itext('printer.status.true');
	} else {
		return itext('printer.status.false');
	}
};

painters.displayPrinter = function(obj) {
	return painters.builders.link({
		'href' : globals.PAGE_CONTEXT + '/print/view.action?printerId='
				+ obj.id
	}, obj.name);
};

painters.displayReasonNumber = function(obj) {
	return painters.builders.link({
		'reasonNumber' : obj.reasonNumber,
		'href' : globals.PAGE_CONTEXT
				+ '/reasonCodes/view.action?reasonCodeId=' + obj.id
	}, obj.reasonNumber);
};

painters.displayReasonCode = function displayReasonCode(obj) {
    return painters.builders.link({
	"href" : globals.PAGE_CONTEXT
		+ "/reasonCodes/view.action?reasonCodeId="
		+ obj.reason.id
    }, obj.reason.reasonNumber);
};

painters.displayReasonNumber = function(obj) {
	return painters.builders.link({
		'reasonNumber' : obj.reasonNumber,
		'href' : globals.PAGE_CONTEXT
				+ '/reasonCodes/view.action?reasonCodeId=' + obj.id
	}, obj.reasonNumber);
};

painters.displayTaskFunctionType = function(obj) {
	if (obj.taskFunctionType == "Normal And Chase Assignments") {
		return itext('com.vocollect.voicelink.core.model.TaskFunctionType.alternative.6');
	} else {
		return obj.taskFunctionType;
	}
};

painters.displayLanguageLink = function(obj) {
	splitUpDisplay = obj.display.split['.'];

	// language code is the last section of the display name
	languageCode = splitUpDisplay[splitUpDisplay.length - 1];

	if (obj.definitions[languageCode] != null) {
		return painters.builders.link({
			'href' : globals.BASE
					+ '/selection/summaryPrompt/viewLanguage.action?language='
					+ languageCode + '&summaryPromptId=' + obj.id
		}, itext('summaryPrompt.language.true'));
	} else {
		return itext('summaryPrompt.language.false');
	}
};

// TODO these both shouldnt exist its one or the other personal preference to
// LINK
painters.displayOperator = function(obj) {
	return painters.builders.link({
		'operatorId' : obj.operator.common.operatorIdentifier,
		'href' : globals.PAGE_CONTEXT + '/operator/view.action?operatorId='
				+ obj.operator.id
	}, obj.operator.common.operatorIdentifier);
};

painters.displayOperatorLink = function(obj) {
	return painters.builders.link({
		'operatorId' : obj.operator.common.operatorIdentifier,
		'href' : globals.PAGE_CONTEXT + '/operator/view.action?operatorId='
				+ obj.operator.id
	}, obj.operator.common.operatorIdentifier);
};

painters.displayOperators = function(obj) {
	if (obj.operators) {
		return map(partial(formatOperator, obj.id), obj.operators);
	} else {
		return " ";
	}
};

painters.displayOperatorTeam = function(obj) {
	return painters.builders.link({
		'href' : globals.PAGE_CONTEXT
				+ '/operatorTeam/view.action?operatorTeamId=' + obj.id
	}, obj.name);
};

painters.displayLotNumber = function(obj) {
	return painters.builders.link({
		'href' : globals.PAGE_CONTEXT + '/lot/view.action?lotId=' + obj.id
	}, obj.number);
};

painters.displayChildItemNumber = function(obj) {
	return painters.builders.link({
		'itemId' : obj.item.number,
		'href' : globals.PAGE_CONTEXT + '/item/view.action?itemId='
				+ obj.item.id
	}, obj.item.number);
};

painters.displayContainerLocationDescription = function(obj) {
	return obj.container.customer.location.description.aggregate;
};

painters.displayLocationIdForDetails = function(obj) {
	return painters.builders.link({
		'href' : globals.PAGE_CONTEXT + '/location/view.action?locationId='
				+ obj.container.customer.location.id
	}, obj.container.customer.location.scannedVerification);
};

//TODO needs updated evey time a new language is added or removed
painters.display_en_US = function(obj) {
	return displayLanguageLink(obj, 'en_US');
};

painters.display_en_AU = function(obj) {
	return displayLanguageLink(obj, 'en_AU');
};

painters.display_da = function(obj) {
	return displayLanguageLink(obj, 'da');
};

painters.display_en_GB = function(obj) {
	return displayLanguageLink(obj, 'en_GB');
};

painters.display_es_MX = function(obj) {
	return displayLanguageLink(obj, 'es_MX');
};

painters.display_fi = function(obj) {
	return displayLanguageLink(obj, 'fi');
};

painters.display_fr = function(obj) {
	return displayLanguageLink(obj, 'fr');
};

painters.display_nl = function(obj) {
	return displayLanguageLink(obj, 'nl');
};

painters.display_pt_BR = function(obj) {
	return displayLanguageLink(obj, 'pt_BR');
};

painters.display_sv = function(obj) {
	return displayLanguageLink(obj, 'sv');
};

painters.display_ja = function(obj) {
	return displayLanguageLink(obj, 'ja');
};

painters.display_zh_CN = function(obj) {
	return displayLanguageLink(obj, 'zh_CN');
};

painters.display_ko = function(obj) {
	return displayLanguageLink(obj, 'ko');
};

// FIXME this is way too hard coded
painters.displayLocationStatus = function(obj) {
	if (obj.status == "Multiple") {
		var filter = "?submittedFilterCriterion={'viewId':'-1030','columnId':'-12801','operandId':'-4','value1':'"
				+ obj.scannedVerification + "','value2':'','locked':false}";
		return painters.builders.link({
			'href' : globals.BASE + '/selection/locItem/list.action' + filter
		}, obj.status);
	} else {
		return obj.status;
	}
};

//These two are for for the item-location mapping view
painters.displayLocId = function(obj) {
    return painters.builders.link({
        'locationId' : obj.location.scannedVerification,
        'href' : globals.PAGE_CONTEXT + '/location/view.action?locationId='
                + obj.location.id
    }, obj.location.scannedVerification);
};

painters.displayItemNumberMapped = function(obj) {
    return painters.builders.link({
        'id' : obj.item.number,
        'href' : globals.PAGE_CONTEXT + '/item/view.action?itemId=' + obj.item.id
    }, obj.item.number);
};

painters.displayCaptureVehicleId = function(obj) {
	if (obj.isCaptureVehicleID) {
		return itext('vsc.vehicleType.captureVehicleId.true');
	} else {
		return itext('vsc.vehicleType.captureVehicleId.false');
	}
};

painters.displayVehicleTypeNumber = function(obj) {
    return painters.builders.link({
        'id' : obj.number,
        'href' : globals.PAGE_CONTEXT + '/vscvehicletype/view.action?typeId=' + obj.id
    }, obj.number);
};

painters.displayVehicleNumber = function(obj) {
    return painters.builders.link({
        'id' : obj.vehicleNumber,
        'href' : globals.PAGE_CONTEXT + '/vscvehicles/view.action?vehicleId=' + obj.id
    }, obj.vehicleNumber);
};


painters.displayShift = function (obj) {
	return painters.builders.link({
		'id' : obj.id,
		'href' : globals.PAGE_CONTEXT + "/shift/view.action?shiftId="
				+ obj.id
	}, obj.name);
};