/*
 * Copyright (c) 2014 Vocollect, Inc., a subsidiary of Honeywell International
 * Inc. All rights reserved.
 * 
 * This source code contains confidential information that is owned by
 * Vocollect, Inc. and may not be copied, disclosed or otherwise used without
 * the express written consent of Vocollect, Inc. Table Component 2.0
 */

(function($j) {
	function RemoteModel() {
		// private
		var PAGESIZE = 5000;  // Should = the tables row count
		var data = {
			length : 0
		};
		var sortcol = null;
		var sortdir = 1;
		var h_request = null;
		var req = null; // ajax request
		var columns = null;
		var url = null;
		var params = null;
		var filterCriteria = null;
		var currentTimestamp = null;

		// events
		var onDataLoading = new Slick.Event();
		var onDataLoaded = new Slick.Event();

		function init() {
		}

		function isDataLoaded(from, to) {
			for ( var i = from; i <= to; i++) {
				if (data[i] == undefined || data[i] == null) {
					return false;
				}
			}
			return true;
		}

		function clear() {
			for ( var key in data) {
				delete data[key];
			}
			data.length = 0;
		}

		/**
		 * The data request from the server.
		 * 
		 * @param from -
		 *            data window start
		 * @param to -
		 *            data window end
		 * @param forceRefresh -
		 *            whether to force a refresh now
		 * @param rowSelectionParams -
		 *            object that contains the param name and array of selected
		 *            ids from a parent table
		 */
		function ensureData(from, to, forceRefresh, rowSelectionParams) {
			if (req){
				return;
			}
			if (from < 0)
				from = 0;

			var fromPage = Math.floor(from / PAGESIZE);
			var toPage = Math.floor(to / PAGESIZE);

			while (data[fromPage * PAGESIZE] !== undefined && fromPage < toPage)
				fromPage++;

			while (data[toPage * PAGESIZE] !== undefined && fromPage < toPage)
				toPage--;

			if (!forceRefresh
					&& (fromPage > toPage || ((fromPage == toPage) && data[fromPage
							* PAGESIZE] !== undefined))) {
				// TODO: look-ahead
				return;
			}

			if (h_request != null)
				clearTimeout(h_request);

			h_request = setTimeout(function() {
				// Commenting this out to fix VLINK-4880
			    // With larger data sets the first row of the table doesn't
			    // work correctly because it's null in the data object
				//for ( var i = fromPage; i <= toPage; i++)
					//data[i * PAGESIZE] = null; // null indicates a 'requested
												// but not available yet'

				if (!forceRefresh) {
					onDataLoading.notify({
						from : from,
						to : to
					});
				}

				// Make the starting index the first index of the next page
				params.startIndex = fromPage * PAGESIZE;
				if (sortcol != null && sortcol != '') {
					params['sortColumn'] = sortcol;
					params['sortAsc'] = sortdir;
				}

				// If a parent table has a driving selection, add it to the
				// params
				if (rowSelectionParams != null) {
					for (key in rowSelectionParams) {
						params[key] = rowSelectionParams[key];
					}
				}

				params.submittedFilterCriterion = encodeURI(filterCriteria);
				
				navigateAway = 0;
				$j(window).bind('beforeunload', function(){navigateAway = 1;});

				
				req = $j.ajax({
					url : getDataUrl(),
					timeout : 1800000,
					callbackParameter : "callback",
					cache : false,
					data : params,
					// this needs to be true to allow the arrays to be parsed
					// to match the previous API (meets jQuery 1.4 data parser)
					traditional : true,
					success : onSuccess,
					error : function(request, type, errorThrown) {
						if (!navigateAway) {
							onError(fromPage, toPage);
						}
					}
				});
				
				req.fromPage = fromPage;
				req.toPage = toPage;
			}, 50);
		}

		function addFilter(filterParam, fn) {
			paramString = '&filterCriterion=' + $j.toJSON(filterParam);
			$j.post(globals.BASE + "/addFilterCriteria.action", paramString,
					function(result) {
						fn(filterParam, result.critId);
						debug.log("Filter criteria successfully saved.");
					});
		}

		function deleteFilter(id) {
			paramString = '&filterCriterionId=' + id;
			$j.post(globals.BASE + "/deleteFilterCriteria.action", paramString,
					function() {
						debug.log("Filter criteria successfully deleted.");
					});
		}

		function deleteFilters(ids) {
			var params = "";
			for ( var i = 0; i < ids.length; i++) {
				params += "&filterCriterionId=" + ids[i];
			}
			$j.post(globals.BASE + "/deleteFilterCriteria.action", params,
					function() {
						debug.log("Filter criteria successfully deleted.");
					});
		}

		function editFilter(filterParam) {
			paramString = '&filterCriterion=' + $j.toJSON(filterParam);
			$j.post(globals.BASE + "/editFilterCriteria.action", paramString,
					function() {
						debug.log("Filter criteria successfully edited.");
					});
		}

		function restoreColumnDefaults(viewId) {
			paramString = '&viewId=' + viewId;
			$j.post(globals.BASE + "/restoreDefaultPreferences.action",
					paramString, function() {
						debug.log("Default columns restored.");
						window.location.reload(true);
					});
		}

		function saveUserPreferences() {
			// loop over all of the columns and grab the user pref data
			var SORT_ORDER_DELIMITER = "sortOrder=";
			var SORTED_COLUMN_DELIMITER = "sortedColumn=";
			var COLUMNS_DELIMITER = "columns=";
			var GENERAL_DELIMITER = "&";
			var parameterList = "";

			parameterList += SORT_ORDER_DELIMITER + sortdir;

		// build the parameter list
			for ( var i = 0; i < columns.length; i++) {
				vocColumnPrefs = {
					column_id : String(columns[i].id),
					width : columns[i].width,
					column_order : i,
					visible : columns[i].visible
				};
				parameterList += GENERAL_DELIMITER;
				// serialize the column information
				parameterList += COLUMNS_DELIMITER
						+ $j.toJSON(vocColumnPrefs);
				if (columns[i].field == sortcol) {
					parameterList += GENERAL_DELIMITER;
					parameterList += SORTED_COLUMN_DELIMITER + columns[i].id;
				}
			}

			$j.post(globals.BASE + "/saveUserPreferences.action",
					parameterList, function() {
						debug.log("User preferences successfully saved.");
					});
		}

		function onError(fromPage, toPage) {
			debug.log("error loading pages " + fromPage + " to " + toPage);
			var statusDate = new Date();
			var statusMessage = itext('status.stopped.updating');
			var defaultMessage = itext('lastUpdateError.defaultErrorMessage');
			
			var completeStatusMessage = statusDate.toLocaleString() + ' <br /> ' + statusMessage + ' <br /> ' + defaultMessage;

			writeStatusMessage(completeStatusMessage,"actionMessage error");
			$j('#messages').show();


		}

		function getDataUrl() {
			return url;
		}

		function onSuccess(resp) {
			var from = 0;
			var to = 1;
			var count = 0;
			data.length = 0;

			if (resp.objects != null) {
				from = req.fromPage * PAGESIZE;
				to = from + resp.objects.length;
				count = resp.objects.length;
				data.length = resp.count;
			}
			currentTimestamp = resp.currentTimeStamp;

			for ( var i = 0; i < count; i++) {
				data[from + i] = resp.objects[i];
				data[from + i].index = from + i;
			}

			req = null;

			onDataLoaded.notify({
				from : from,
				to : to,
				count : data.length
			});
			
		}

		function getCurrentTimestamp() {
			return currentTimestamp;
		}

		function reloadData(from, to) {
			for ( var i = from; i <= to; i++)
				delete data[i];

			ensureData(from, to);
		}

		function setSort(column, dir) {
			sortcol = column;
			(dir == 1) ? sortdir = "true" : sortdir = "false";
			clear();
		}

		function getColumns() {
			return columns;
		}

		function setupPrefs(_url, _columns, _params) {
			url = _url;
			setColumns(_columns);
			params = _params;
		}

		function setColumns(_columns) {
			columns = _columns;
		}

		function setFilterCriteria(_fc) {
			filterCriteria = _fc;
		}

		init();

		return {
			// properties
			"data" : data,

			// methods
			"clear" : clear,
			"isDataLoaded" : isDataLoaded,
			"ensureData" : ensureData,
			"reloadData" : reloadData,
			"setSort" : setSort,
			"setupPrefs" : setupPrefs,
			"getColumns" : getColumns,
			"saveUserPreferences" : saveUserPreferences,
			"addFilter" : addFilter,
			"deleteFilter" : deleteFilter,
			"deleteFilters" : deleteFilters,
			"editFilter" : editFilter,
			"setColumns" : setColumns,
			"restoreColumnDefaults" : restoreColumnDefaults,
			"setFilterCriteria" : setFilterCriteria,
			"getCurrentTimestamp" : getCurrentTimestamp,

			// events
			"onDataLoading" : onDataLoading,
			"onDataLoaded" : onDataLoaded
		};
	}

	// Slick.Data.RemoteModel
	$j.extend(true, window, {
		Slick : {
			Data : {
				RemoteModel : RemoteModel
			}
		}
	});
})(jQuery);