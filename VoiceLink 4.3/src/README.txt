*******************************************************************************
NOTE: This directory is purposely empty. It must remain here in order for the
Ant-based build to work correctly. The core product source files are
available (in non-compilable form) for your reference in the src-ref directory.

It is not recommended to modify the core product classes. In the event that
you must, copy the file from "src-ref" to "src-custom", remove the warning
message and recompile.
*******************************************************************************

 