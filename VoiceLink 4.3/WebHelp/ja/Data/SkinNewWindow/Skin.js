CMCXmlParser._FilePathToXmlStringMap.Add(
	'Skin',
	'<?xml version=\"1.0\" encoding=\"utf-8\"?>' +
	'<CatapultSkin Version=\"1\" Title=\"VoiceLink Online Help\" Top=\"281px\" Left=\"485px\" Width=\"775px\" Height=\"652px\" Tabs=\"TOC,Index,Search,Glossary\" Bottom=\"63px\" Right=\"20px\" UseDefaultBrowserSetup=\"true\" UseBrowserDefaultSize=\"True\" AutoSyncTOC=\"true\">' +
	'    <!-- saved from url=(0014)about:internet -->' +
	'    <Toc LinesBetweenItems=\"True\" LinesFromRoot=\"False\" SingleClick=\"False\" PlusMinusSquares=\"False\" AlwaysShowSelection=\"False\" UseFolderIcons=\"False\" ImageListWidth=\"16\" BinaryStorage=\"False\" />' +
	'    <Stylesheet Link=\"Stylesheet.xml\">' +
	'    </Stylesheet>' +
	'    <WebHelpOptions NavigationPaneWidth=\"200\" HideNavigationOnStartup=\"False\" AboutBox=\"About.png\" AboutBoxWidth=\"645\" AboutBoxHeight=\"279\">' +
	'    </WebHelpOptions>' +
	'    <Toolbar EnableCustomLayout=\"true\" Buttons=\"Home|Print|Back|Forward|ToggleNavigationPane\" />' +
	'</CatapultSkin>'
);
