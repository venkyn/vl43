var xmlSkinData = "";
xmlSkinData += '<?xml version=\"1.0\" encoding=\"utf-8\"?>';
xmlSkinData += '<CatapultSkin Version=\"1\" Title=\"VoiceLink Online Help\" Top=\"281px\" Left=\"485px\" Width=\"775px\" Height=\"652px\" Tabs=\"TOC,Index,Search,Glossary\" Bottom=\"63px\" Right=\"20px\" UseDefaultBrowserSetup=\"true\" UseBrowserDefaultSize=\"True\" AutoSyncTOC=\"true\">';
xmlSkinData += '    <!-- saved from url=(0016)http://localhost -->';
xmlSkinData += '    <Toc LinesBetweenItems=\"True\" LinesFromRoot=\"False\" SingleClick=\"False\" PlusMinusSquares=\"False\" AlwaysShowSelection=\"False\" UseFolderIcons=\"False\" ImageListWidth=\"16\" BinaryStorage=\"False\" />';
xmlSkinData += '    <Stylesheet Link=\"Stylesheet.xml\">';
xmlSkinData += '    </Stylesheet>';
xmlSkinData += '    <WebHelpOptions NavigationPaneWidth=\"200\" HideNavigationOnStartup=\"False\" AboutBox=\"About.png\" AboutBoxWidth=\"648\" AboutBoxHeight=\"333\">';
xmlSkinData += '    </WebHelpOptions>';
xmlSkinData += '    <Toolbar EnableCustomLayout=\"true\" Buttons=\"Home|Print|Back|Forward|ToggleNavigationPane\" />';
xmlSkinData += '</CatapultSkin>';
CMCXmlParser._FilePathToXmlStringMap.Add('Skin', xmlSkinData);
