--Created for the VL3.1.1 release
--Updates Oracle 10g Database

ALTER TABLE arch_pts_puts MODIFY itemDescription nvarchar2(255) NULL;
ALTER TABLE arch_sel_picks MODIFY itemDescription nvarchar2(255) NULL;
