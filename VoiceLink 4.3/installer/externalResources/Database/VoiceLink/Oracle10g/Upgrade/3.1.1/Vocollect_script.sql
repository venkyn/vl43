--Created for the VL3.1.1 release
--Updates Oracle 10g Database

--Changes sort types to support data translation tables
UPDATE voc_column SET SortType = 4 WHERE id IN (-22006,-22203,-21903,-21311,-21310,-21211,-21210,-21202,-21123,-21120,-21119,-21118,-21111,-21103,-21001);
UPDATE voc_column SET SortType = 4 WHERE id IN (-21916,-21912,-21910,-21905);
UPDATE voc_column SET SortType = 1 WHERE id IN (-21917);

--Removes unsupported locales FROM list of summary prompts that can be built
DELETE FROM voc_column WHERE id IN (-13003,-13004,-13006,-13008,-13011,-13012,-13014,-13015,-13016,-13021,-13022,-13026,-13027,-13028);

--Adds Korean (ko) and Russian (ru) to the list of summary prompts that can be built
INSERT INTO voc_column (id,view_id,field,display,sorted,sort_asc,width,visible,display_function,required,displayable,column_order,sortType,filterType,operand_type,painter_function,option_key_prefix)VALUES(-13020,-1033,'definitions','summaryPrompt.view.column.ru',0,1,110,1,'displayPromptLanguage',0,1,21,0,0,4,'display_ru','summaryPrompt.language');
INSERT INTO voc_column (id,view_id,field,display,sorted,sort_asc,width,visible,display_function,required,displayable,column_order,sortType,filterType,operand_type,painter_function,option_key_prefix)VALUES(-13023,-1033,'definitions','summaryPrompt.view.column.ko',0,1,110,1,'displayPromptLanguage',0,1,24,0,0,4,'display_ko','summaryPrompt.language');

--Fixes issue where foreign languages are not correctly parsed with JavaScript
INSERT INTO voc_homepage_access_summaries (id,summary_id) VALUES (-1,-16);
INSERT INTO voc_homepage_access_summaries (id,summary_id) VALUES (-1,-17);
INSERT INTO voc_homepage_access_summaries (id,summary_id) VALUES (-1,-18);
INSERT INTO voc_homepage_access_summaries (id,summary_id) VALUES (-1,-19);

--This will need to be added to all future upgrade scripts
UPDATE voc_system_properties set value = 1 WHERE systemPropertyId = -1005;

--Updates Selection Pick export for VLINK-2574
DROP VIEW vw_export_picked;
CREATE VIEW vw_export_picked (pickDetailId,RecordType,PickTime,OriginalAssignmentNumber,LocationID,Item,Operator,PickStatus,QuantityPicked,RecordedVariableWeight,RecordedSerialNumber,RecordedLotNumber,ExportStatus,Site,PickSequenceNumber) AS SELECT pd.pickDetailId AS pickDetailId,pd.detailType AS RecordType,pd.pickTime,a.assignmentNumber AS OriginalAssignmentNumber,l.scannedVerification AS LocationID,i.itemNumber AS Item,vo.operatorIdentifier AS Operator,obj.status AS PickStatus,pd.quantityPicked AS QuantityPicked,pd.variableWeight AS RecordedVariableWeight,pd.serialNumber AS RecordedSerialNumber,pd.lotNumber AS RecordedLotNumber,pd.exportStatus,site.name AS Site,obj.sequenceNumber AS PickSequenceNumber FROM sel_picks obj JOIN sel_pick_details pd ON obj.pickID = pd.pickID LEFT OUTER JOIN core_operators o ON pd.OperatorID = o.OperatorID LEFT OUTER JOIN voc_operators vo ON o.OperatorID = vo.OperatorID LEFT OUTER JOIN core_items i ON obj.ItemID = i.ItemID JOIN sel_assignments a ON a.AssignmentID = obj.AssignmentID LEFT OUTER JOIN core_locations l ON obj.locationID = l.locationID JOIN sel_pick_tag objtag ON objtag.tagged_id = obj.pickID JOIN voc_tag vt ON objtag.tag_ID=vt.ID and VT.Tag_Type=1 JOIN voc_site site ON vt.taggable_ID=site.siteID WHERE (pd.exportStatus = 0 or pd.exportStatus = 1);

--Updates VL version, EPP version and modules
DECLARE 
vlVersion VARCHAR2(32); 
vipVersion VARCHAR2(32); 
newId NUMBER(19,0); 
oldId NUMBER(19,0); 

BEGIN 
vlVersion := '3.1.1';
vipVersion := '1.6.1';
SELECT MAX(moduleId) INTO newId FROM voc_plugin_modules;
SELECT moduleId INTO oldId FROM voc_plugin_modules WHERE name = 'plugin.module.voicelink' AND isEnabled = 1;
UPDATE voc_plugin_modules SET isEnabled = 0 WHERE name = 'plugin.module.voicelink';
INSERT INTO voc_plugin_modules (moduleId, name, moduleVersion, frameworkVersion, sequenceNumber, uri, navMenuName, isEnabled, createdDate) VALUES (newId + 1, 'plugin.module.voicelink', vlVersion, vipVersion, 101, '/selection/home.action', 'voicelink', 1, SYSDATE);
SELECT moduleId INTO newId FROM voc_plugin_modules WHERE name = 'plugin.module.voicelink' AND isEnabled = 1;
UPDATE voc_plugin_components SET moduleId = newId WHERE moduleId = oldId;
UPDATE voc_plugin_modules_auth SET moduleId = newId WHERE moduleId = oldId;
UPDATE voc_plugin_modules 
SET moduleVersion = vipVersion 
WHERE moduleId = 1;
UPDATE voc_plugin_modules 
SET frameworkVersion = vipVersion 
WHERE moduleId = 1;
UPDATE voc_plugin_modules 
SET moduleVersion = vipVersion 
WHERE moduleId = 2;
UPDATE voc_plugin_modules 
SET frameworkVersion = vipVersion 
WHERE moduleId = 2;
END;
