
--  ********************************************************************
--		CREATE CYCLE COUNTING ARCHIVE TABLES
--  ********************************************************************
	
	IF OBJECT_ID('dbo.arch_cc_assignment_details') IS NULL
		BEGIN
			PRINT N'Creating [arch_cc_assignment_details]...';
			CREATE TABLE [arch_cc_assignment_details] (
				[cycleCountDetailId] NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
				[version]            INT            NOT NULL,
				[cycleCountId]       NUMERIC (19)   NOT NULL,
				[itemNumber]         NVARCHAR (50)  NULL,
				[itemDescription]    NVARCHAR (255) NULL,
				[reasonNumber]       INT            NULL,
				[reasonDescription]  NVARCHAR (50)  NULL,
				[expectedQuantity]   INT            NULL,
				[actualQuantity]     INT            NULL,
				[unitOfMeasure]      NVARCHAR (255) NULL,
				[countTime]          DATETIME       NULL,
				[status]             INT            NOT NULL,
				[createdDate]        DATETIME       NOT NULL,
				PRIMARY KEY CLUSTERED ([cycleCountDetailId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			PRINT N'Creating [arch_cc_assignment_details].[IX_arch_ccdetail_createdDate]...';
			CREATE NONCLUSTERED INDEX [IX_arch_ccdetail_createdDate]
				ON [arch_cc_assignment_details]([createdDate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - arch_cc_assignment_details already exists...';
		END
	GO
		
	IF OBJECT_ID('dbo.arch_cc_assignments') IS NULL
		BEGIN	
			PRINT N'Creating [arch_cc_assignments]...';
			CREATE TABLE [arch_cc_assignments] (
				[cycleCountId]       NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
				[version]            INT            NOT NULL,
				[cycleCountSequence] NUMERIC (19)   NOT NULL,
				[regionName]         NVARCHAR (255) NOT NULL,
				[regionNumber]       INT            NOT NULL,
				[status]             INT            NOT NULL,
				[operatorName]       NVARCHAR (128) NULL,
				[operatorIdentifier] NVARCHAR (64)  NULL,
				[startTime]          DATETIME       NULL,
				[endTime]            DATETIME       NULL,
				[preAisle]           NVARCHAR (50)  NULL,
				[aisle]              NVARCHAR (50)  NULL,
				[postAisle]          NVARCHAR (50)  NULL,
				[slot]               NVARCHAR (50)  NULL,
				[siteId]             NUMERIC (19)   NOT NULL,
				[siteName]           NVARCHAR (256) NOT NULL,
				[createdDate]        DATETIME       NOT NULL,
				PRIMARY KEY CLUSTERED ([cycleCountId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			PRINT N'Creating [arch_cc_assignments].[IX_arch_ccAssignment_Status]...';
			CREATE NONCLUSTERED INDEX [IX_arch_ccAssignment_Status]
				ON [arch_cc_assignments]([status] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [arch_cc_assignments].[IX_arch_ccAssign_createdDate]...';
			CREATE NONCLUSTERED INDEX [IX_arch_ccAssign_createdDate]
				ON [arch_cc_assignments]([createdDate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - arch_cc_assignments already exists...';
		END
	GO			
			
	IF OBJECT_ID('dbo.arch_cc_labor') IS NULL
		BEGIN			
		PRINT N'Creating [arch_cc_labor]...';
			CREATE TABLE [arch_cc_labor] (
				[ccLaborId]          NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
				[version]            INT            NOT NULL,
				[assignmentId]       NUMERIC (19)   NOT NULL,
				[operatorName]       NVARCHAR (128) NULL,
				[operatorIdentifier] NVARCHAR (64)  NULL,
				[operatorLaborId]    NUMERIC (19)   NULL,
				[breakLaborId]       NUMERIC (19)   NULL,
				[startTime]          DATETIME       NOT NULL,
				[endTime]            DATETIME       NULL,
				[duration]           NUMERIC (19)   NOT NULL,
				[exportStatus]       INT            NOT NULL,
				[locationsCounted]   INT            NOT NULL,
				[actualRate]         FLOAT          NOT NULL,
				[percentOfGoal]      FLOAT          NOT NULL,
				[createdDate]        DATETIME       NOT NULL,
				PRIMARY KEY CLUSTERED ([ccLaborId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);

			PRINT N'Creating CK__arch_cc_l__durat__2903B818...';
			ALTER TABLE [arch_cc_labor] WITH NOCHECK ADD CHECK ([duration]>=(0));
			PRINT N'Creating CK__arch_cc_l__locat__29F7DC51...';
			ALTER TABLE [arch_cc_labor] WITH NOCHECK ADD CHECK ([locationsCounted]>=(0));
			PRINT N'Creating CK__arch_cc_l__actua__2AEC008A...';
			ALTER TABLE [arch_cc_labor] WITH NOCHECK ADD CHECK ([actualRate]>=(0));
			PRINT N'Creating CK__arch_cc_l__perce__2BE024C3...';
			ALTER TABLE [arch_cc_labor] WITH NOCHECK ADD CHECK ([percentOfGoal]>=(0));
		END
	ELSE
		BEGIN
			print N'table - arch_cc_labor already exists...';
		END		
	GO
		
--  ********************************************************************
--		CREATE LOADING ARCHIVE TABLES
--  ********************************************************************
		
	IF OBJECT_ID('dbo.arch_loading_containers') IS NULL
		BEGIN		
			PRINT N'Creating [arch_loading_containers]...';
			CREATE TABLE [arch_loading_containers] (
				[containerId]           NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
				[version]               INT            NOT NULL,
				[containerNumber]       NVARCHAR (50)  NOT NULL,
				[stopId]                NUMERIC (19)   NOT NULL,
				[masterContainerNumber] NVARCHAR (50)  NULL,
				[type]                  INT            NOT NULL,
				[status]                INT            NOT NULL,
				[expectedCube]          FLOAT          NULL,
				[expectedWeight]        FLOAT          NULL,
				[loadPosition]          INT            NULL,
				[operatorIdentifier]    NVARCHAR (50)  NULL,
				[operatorName]          NVARCHAR (128) NULL,
				[loadTime]              DATETIME       NULL,
				[createdDate]           DATETIME       NOT NULL,
				[exportStatus]          INT            NOT NULL,
				[siteId]                NUMERIC (19)   NOT NULL,
				[siteName]              NVARCHAR (256) NOT NULL,
				PRIMARY KEY CLUSTERED ([containerId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);

			PRINT N'Creating [arch_loading_containers].[IX_Arch_Load_Cont_CreatedDate]...';
			CREATE NONCLUSTERED INDEX [IX_Arch_Load_Cont_CreatedDate]
				ON [arch_loading_containers]([createdDate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [arch_loading_containers].[IX_arch_loadCont_xport_status]...';
			CREATE NONCLUSTERED INDEX [IX_arch_loadCont_xport_status]
				ON [arch_loading_containers]([exportStatus] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - arch_loading_containers already exists...';
		END	
	GO	

	IF OBJECT_ID('dbo.arch_loading_labor') IS NULL
		BEGIN			
			PRINT N'Creating [arch_loading_labor]...';
			CREATE TABLE [arch_loading_labor] (
				[laborId]            NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
				[version]            INT            NOT NULL,
				[routeId]            NUMERIC (19)   NOT NULL,
				[operatorIdentifier] NVARCHAR (50)  NULL,
				[operatorName]       NVARCHAR (128) NULL,
				[operatorLaborId]    NUMERIC (19)   NULL,
				[breakLaborId]       NUMERIC (19)   NULL,
				[startTime]          DATETIME       NOT NULL,
				[endTime]            DATETIME       NULL,
				[duration]           NUMERIC (19)   NOT NULL,
				[exportStatus]       INT            NOT NULL,
				[containersLoaded]   INT            NOT NULL,
				[actualRate]         FLOAT          NOT NULL,
				[percentOfGoal]      FLOAT          NOT NULL,
				[createdDate]        DATETIME       NOT NULL,
				PRIMARY KEY CLUSTERED ([laborId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			PRINT N'Creating [arch_loading_labor].[IX_Arch_LoadLabor_CreatedDate]...';

			CREATE NONCLUSTERED INDEX [IX_Arch_LoadLabor_CreatedDate]
				ON [arch_loading_labor]([createdDate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - arch_loading_labor already exists...';
		END	
	GO		
			
	IF OBJECT_ID('dbo.arch_loading_routes') IS NULL
		BEGIN				
			PRINT N'Creating [arch_loading_routes]...';
			CREATE TABLE [arch_loading_routes] (
				[routeId]             NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
				[version]             INT            NOT NULL,
				[routeNumber]         NVARCHAR (50)  NULL,
				[type]                INT            NOT NULL,
				[status]              INT            NOT NULL,
				[scannedVerification] NVARCHAR (50)  NULL,
				[trailer]             NVARCHAR (9)   NULL,
				[regionName]          NVARCHAR (255) NOT NULL,
				[regionNumber]        INT            NOT NULL,
				[departureDateTime]   DATETIME       NOT NULL,
				[startTime]           DATETIME       NULL,
				[endTime]             DATETIME       NULL,
				[captureLoadPosition] TINYINT        NOT NULL,
				[note]                NVARCHAR (255) NULL,
				[createdDate]         DATETIME       NOT NULL,
				[exportStatus]        INT            NOT NULL,
				[siteId]              NUMERIC (19)   NOT NULL,
				[siteName]            NVARCHAR (256) NOT NULL,
				PRIMARY KEY CLUSTERED ([routeId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			PRINT N'Creating [arch_loading_stops]...';
		END
	ELSE
		BEGIN
			print N'table - arch_loading_routes already exists...';
		END	
	GO
			
	IF OBJECT_ID('dbo.arch_loading_stops') IS NULL
		BEGIN			
			PRINT N'Creating [arch_loading_stops]...';
			CREATE TABLE [arch_loading_stops] (
				[stopId]                     NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
				[version]                    INT            NOT NULL,
				[stopNumber]                 NUMERIC (19)   NOT NULL,
				[routeId]                    NUMERIC (19)   NOT NULL,
				[status]                     INT            NOT NULL,
				[expectedNumberOfContainers] INT            NOT NULL,
				[actualNumberOfContainers]   INT            NULL,
				[customerNumber]             NVARCHAR (50)  NOT NULL,
				[note]                       NVARCHAR (255) NULL,
				[startTime]                  DATETIME       NULL,
				[endTime]                    DATETIME       NULL,
				[createdDate]                DATETIME       NOT NULL,
				[exportStatus]               INT            NOT NULL,
				[siteId]                     NUMERIC (19)   NOT NULL,
				[siteName]                   NVARCHAR (256) NOT NULL,
				PRIMARY KEY CLUSTERED ([stopId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			PRINT N'Creating [arch_loading_stops].[IX_Arch_LoadStop_CreatedDate]...';
			CREATE NONCLUSTERED INDEX [IX_Arch_LoadStop_CreatedDate]
				ON [arch_loading_stops]([createdDate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - arch_loading_stops already exists...';
		END	
	GO
	
-- **********************************************************************************
--	Move operator labor data from source db
--  This needs to be done before the cycle counting transactional tables are created.
-- **********************************************************************************			
	IF OBJECT_ID('dbo.cc_assignment_import_data') IS NULL
		BEGIN	
			ALTER TABLE [core_operator_labor]
			ADD	[goalRate]  INT  NULL;
		END
	ELSE
		BEGIN
			print N'table - core_operator_labor was already altered...';
		END		
	GO	
	
		
--  ********************************************************************
--  CREATE CYCLE COUNTING TRANSACTIONAL TABLES
--  ********************************************************************
	IF OBJECT_ID('dbo.cc_assignment_import_data') IS NULL
		BEGIN	
			PRINT N'Creating [cc_assignment_import_data]...';
			CREATE TABLE [cc_assignment_import_data] (
				[importID]     NUMERIC (19)   NOT NULL,
				[regionNumber] NVARCHAR (255) NULL,
				[locationId]   NVARCHAR (255) NOT NULL,
				[importStatus] INT            DEFAULT ((0)) NOT NULL,
				[siteName]     NVARCHAR (255) NOT NULL,
				PRIMARY KEY CLUSTERED ([importID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);		
		END
	ELSE
		BEGIN
			print N'table - cc_assignment_import_data already exists...';
		END	
	GO	
		
		
	IF OBJECT_ID('dbo.cc_assignment_tag') IS NULL
		BEGIN	
			PRINT N'Creating [cc_assignment_tag]...';
			CREATE TABLE [cc_assignment_tag] (
				[tagged_id] NUMERIC (19) NOT NULL,
				[tag_id]    NUMERIC (19) NOT NULL,
				PRIMARY KEY CLUSTERED ([tagged_id] ASC, [tag_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);		
		END
	ELSE
		BEGIN
			print N'table - cc_assignment_tag already exists...';
		END	
	GO			
		
		
	IF OBJECT_ID('dbo.cc_assignments') IS NULL
		BEGIN		
			PRINT N'Creating [cc_assignments]...';
			CREATE TABLE [cc_assignments] (
				[cycleCountId]       NUMERIC (19) IDENTITY (1, 1) NOT NULL,
				[version]            INT          NOT NULL,
				[cycleCountSequence] NUMERIC (19) NOT NULL,
				[regionId]           NUMERIC (19) NULL,
				[status]             INT          NOT NULL,
				[operatorId]         NUMERIC (19) NULL,
				[startTime]          DATETIME     NULL,
				[endTime]            DATETIME     NULL,
				[exportStatus]       INT          NOT NULL,
				[locationId]         NUMERIC (19) NOT NULL,
				[createdDate]        DATETIME     NOT NULL,
				[purgeable]          INT          NOT NULL,
				PRIMARY KEY CLUSTERED ([cycleCountId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);			
			PRINT N'Creating DF__cc_assign__purge__1C68D709...';
			ALTER TABLE [cc_assignments]  ADD DEFAULT ((0)) FOR [purgeable];
			PRINT N'Creating [cc_assignments].[IX_ccAssignment_Operator]...';
			CREATE NONCLUSTERED INDEX [IX_ccAssignment_Operator]
				ON [cc_assignments]([operatorId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [cc_assignments].[IX_ccAssignment_createdDate]...';
			CREATE NONCLUSTERED INDEX [IX_ccAssignment_createdDate]
				ON [cc_assignments]([createdDate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [cc_assignments].[IX_cyclecount_ccStatus]...';
			CREATE NONCLUSTERED INDEX [IX_cyclecount_ccStatus]
				ON [cc_assignments]([status] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [cc_assignments].[IX_ccAssignment_Region]...';
			CREATE NONCLUSTERED INDEX [IX_ccAssignment_Region]
				ON [cc_assignments]([regionId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [cc_assignments].[IX_CCAssign_purgeable]...';
			CREATE NONCLUSTERED INDEX [IX_CCAssign_purgeable]
				ON [cc_assignments]([purgeable] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [cc_assignments].[IX_cc_assignment_export_status]...';
			CREATE NONCLUSTERED INDEX [IX_cc_assignment_export_status]
				ON [cc_assignments]([exportStatus] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			CREATE NONCLUSTERED INDEX [IX_ccAssignmentExport_complete]
				ON [dbo].[cc_assignments]([status] ASC,[exportStatus] ASC)
				WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - cc_assignments already exists...';
		END		
	GO	
		
	IF OBJECT_ID('dbo.cc_detail_import_data') IS NULL
		BEGIN
			PRINT N'Creating [cc_detail_import_data]...';
			CREATE TABLE [cc_detail_import_data] (
				[importID]         NUMERIC (19)   NOT NULL,
				[cycleCountId]     NUMERIC (19)   NOT NULL,
				[itemId]           NVARCHAR (255) NOT NULL,
				[expectedQuantity] INT            NOT NULL,
				[unitOfMeasure]    NVARCHAR (255) NOT NULL,
				[importStatus]     INT            DEFAULT ((0)) NOT NULL,
				[siteName]         NVARCHAR (255) NULL,
				PRIMARY KEY CLUSTERED ([importID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			PRINT N'Creating [cc_detail_import_data].[IX_cc_assignment_detail_import]...';
			CREATE NONCLUSTERED INDEX [IX_cc_assignment_detail_import]
				ON [cc_detail_import_data]([cycleCountId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [cc_details]...';
			CREATE TABLE [cc_details] (
				[cycleCountDetailId] NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
				[version]            INT            NOT NULL,
				[cycleCountId]       NUMERIC (19)   NULL,
				[itemId]             NUMERIC (19)   NULL,
				[reasonId]           NUMERIC (19)   NULL,
				[expectedQuantity]   INT            NOT NULL,
				[actualQuantity]     INT            NULL,
				[unitOfMeasure]      NVARCHAR (255) NULL,
				[countTime]          DATETIME       NULL,
				[status]             INT            NOT NULL,
				[exportStatus]       INT            NOT NULL,
				[createdDate]        DATETIME       NOT NULL,
				PRIMARY KEY CLUSTERED ([cycleCountDetailId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			PRINT N'Creating [cc_details].[IX_ccdetail_createdDate]...';
			CREATE NONCLUSTERED INDEX [IX_ccdetail_createdDate]
				ON [cc_details]([createdDate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [cc_details].[IX_ccDetail_exportStatus]...';
			CREATE NONCLUSTERED INDEX [IX_ccDetail_exportStatus]
				ON [cc_details]([exportStatus] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - cc_detail_import_data already exists...';
		END	
	GO

	IF OBJECT_ID('dbo.cc_labor') IS NULL
		BEGIN		
			PRINT N'Creating [cc_labor]...';
			CREATE TABLE [cc_labor] (
				[ccLaborId]        NUMERIC (19) IDENTITY (1, 1) NOT NULL,
				[version]          INT          NOT NULL,
				[assignmentId]     NUMERIC (19) NOT NULL,
				[operatorId]       NUMERIC (19) NOT NULL,
				[operatorLaborId]  NUMERIC (19) NULL,
				[breakLaborId]     NUMERIC (19) NULL,
				[startTime]        DATETIME     NOT NULL,
				[endTime]          DATETIME     NULL,
				[duration]         NUMERIC (19) NOT NULL,
				[exportStatus]     INT          NOT NULL,
				[locationsCounted] INT          NOT NULL,
				[actualRate]       FLOAT        NOT NULL,
				[percentOfGoal]    FLOAT        NOT NULL,
				[createdDate]      DATETIME     NOT NULL,
				PRIMARY KEY CLUSTERED ([ccLaborId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			PRINT N'Creating [cc_labor].[IX_cc_assignlabor_operator]...';
			CREATE NONCLUSTERED INDEX [IX_cc_assignlabor_operator]
				ON [cc_labor]([operatorId] ASC, [endTime] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [cc_labor].[IX_cc_assignlabor_operLabor]...';
			CREATE NONCLUSTERED INDEX [IX_cc_assignlabor_operLabor]
				ON [cc_labor]([operatorLaborId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [cc_labor].[IX_ccassignment_breakLabor]...';
			CREATE NONCLUSTERED INDEX [IX_ccassignment_breakLabor]
				ON [cc_labor]([breakLaborId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [cc_labor].[IX_cc_assignLabor_assignment]...';
			CREATE NONCLUSTERED INDEX [IX_cc_assignLabor_assignment]
				ON [cc_labor]([assignmentId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - cc_labor already exists...';
		END	
	GO

	IF OBJECT_ID('dbo.cc_regions') IS NULL
		BEGIN	
			PRINT N'Creating [cc_regions]...';
			CREATE TABLE [cc_regions] (
				[regionId]  NUMERIC (19) NOT NULL,
				[countType] INT          NOT NULL,
				[countMode] INT          NOT NULL,
				PRIMARY KEY CLUSTERED ([regionId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
		END
	ELSE
		BEGIN
			print N'table - cc_regions already exists...';
		END	
	GO
			


--  *************************************************************************************
--	Add the new columns to the following tables for the loading application.
--  This must happen before the loading transactional tables are created
--  because we are checking to see if the loading route table was already created.
--  ************************************************************************************
	IF OBJECT_ID('dbo.loading_routes') IS NULL
		BEGIN			
			PRINT N'Altering [sel_assignments]...';
			ALTER TABLE [sel_assignments]
				ADD [departureDateTime] DATETIME     NULL,
					[loadingRegionId]   NUMERIC (19) NULL;
			PRINT N'Creating [sel_assignments].[IX_Interleave_Assignment_Find]...';
			CREATE NONCLUSTERED INDEX [IX_Interleave_Assignment_Find]
				ON [sel_assignments]([route] ASC, [departureDateTime] ASC, [loadingRegionId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - sel_assignments was already altered...';
		END	
	GO


	IF OBJECT_ID('dbo.loading_routes') IS NULL
		BEGIN	
			ALTER TABLE [arch_sel_assignments]
			ADD [loadingRegionName]   NVARCHAR (255) NULL,
				[loadingRegionNumber] INT            NULL,
				[departureDateTime] DATETIME         NULL;
		END
	ELSE
		BEGIN
			print N'table - arch_sel_assignments was already altered...';
		END	
	GO		

	
	IF OBJECT_ID('dbo.loading_routes') IS NULL
		BEGIN	
			ALTER TABLE [sel_assignment_import_data]
			ADD	[departureDateTime]   DATETIME       NULL,
				[loadingRegionNumber] NUMERIC (19)   NULL;
		END
	ELSE
		BEGIN
			print N'table - sel_assignment_import_data was already altered...';
		END	
			
			
	-- *****************************************************************************
	-- Altering table [sel_pick_import_data].  
	-- Check if loading transactional tables have been created so we don't attempt 
	-- to modify this table twice. 
	-- *****************************************************************************
	IF OBJECT_ID('dbo.loading_routes') IS NULL
		BEGIN	
			PRINT N'Altering table table [sel_pick_import_data]...';		
			PRINT N'Dropping DF__sel_pick___baseI__2022C2A6...';
			ALTER TABLE [sel_pick_import_data] DROP CONSTRAINT [DF__sel_pick___baseI__2022C2A6];
			PRINT N'Dropping DF__sel_pick___isBas__7FF5EA36...';
			ALTER TABLE [sel_pick_import_data] DROP CONSTRAINT [DF__sel_pick___isBas__2116E6DF];
			PRINT N'Dropping isBaseItem Column...';
			ALTER TABLE [sel_pick_import_data] DROP COLUMN [isBaseItem];
			PRINT N'Setting default value for baseItemOverride...';
			ALTER TABLE [dbo].[sel_pick_import_data] ADD  DEFAULT ((0)) FOR [baseItemOverride]
		END
	ELSE
		BEGIN
			print N'table - sel_pick_import_data was already altered...';
		END		
	GO
			
--  ********************************************************************
--  CREATE LOADING TRANSACTIONAL TABLES
--  ********************************************************************
	IF OBJECT_ID('dbo.loading_container_import_data') IS NULL
		BEGIN	
			PRINT N'Creating [loading_container_import_data]...';
			CREATE TABLE [loading_container_import_data] (
				[importID]          NUMERIC (19)   NOT NULL,
				[containerNumber]   NVARCHAR (50)  NOT NULL,
				[stopNumber]        NUMERIC (19)   NOT NULL,
				[routeNumber]       NVARCHAR (50)  NOT NULL,
				[departureDateTime] DATETIME       NOT NULL,
				[regionNumber]      NUMERIC (19)   NOT NULL,
				[expectedCube]      FLOAT          NULL,
				[expectedWeight]    FLOAT          NULL,
				[importStatus]      INT            DEFAULT ((0)) NOT NULL,
				[siteName]          NVARCHAR (255) NOT NULL,
				PRIMARY KEY CLUSTERED ([importID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF),
				UNIQUE NONCLUSTERED ([containerNumber] ASC, [siteName] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF) ON [PRIMARY]
			);
			PRINT N'Creating [loading_container_import_data].[IX_loading_container_import]...';
			CREATE NONCLUSTERED INDEX [IX_loading_container_import]
				ON [loading_container_import_data]([siteName] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - loading_container_import_data already exists...';
		END			
	GO		
		
	IF OBJECT_ID('dbo.loading_container_tag') IS NULL
		BEGIN
			PRINT N'Creating [loading_container_tag]...';
			CREATE TABLE [loading_container_tag] (
				[tagged_id] NUMERIC (19) NOT NULL,
				[tag_id]    NUMERIC (19) NOT NULL,
				PRIMARY KEY CLUSTERED ([tagged_id] ASC, [tag_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
		END
	ELSE
		BEGIN
			print N'table - loading_container_tag already exists...';
		END			
	GO		
		
		
	IF OBJECT_ID('dbo.loading_containers') IS NULL
		BEGIN			
			PRINT N'Creating [loading_containers]...';
			CREATE TABLE [loading_containers] (
				[containerId]           NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
				[version]               INT           NOT NULL,
				[containerNumber]       NVARCHAR (50) NOT NULL,
				[masterContainerNumber] NVARCHAR (50) NULL,
				[type]                  INT           NOT NULL,
				[status]                INT           NOT NULL,
				[expectedCube]          FLOAT         NULL,
				[expectedWeight]        FLOAT         NULL,
				[loadPosition]          INT           NULL,
				[stopId]                NUMERIC (19)  NOT NULL,
				[operatorId]            NUMERIC (19)  NULL,
				[loadTime]              DATETIME      NULL,
				[createdDate]           DATETIME      NOT NULL,
				[exportStatus]          INT           NOT NULL,
				PRIMARY KEY CLUSTERED ([containerId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);

			PRINT N'Creating [loading_containers].[IX_load_find_stop]...';
			CREATE NONCLUSTERED INDEX [IX_load_find_stop]
				ON [loading_containers]([stopId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [loading_containers].[IX_load_cont_export_status]...';
			CREATE NONCLUSTERED INDEX [IX_load_cont_export_status]
				ON [loading_containers]([exportStatus] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [loading_containers].[IX_Container_CreatedDate]...';
			CREATE NONCLUSTERED INDEX [IX_Container_CreatedDate]
				ON [loading_containers]([createdDate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];		
		END
	ELSE
		BEGIN
			print N'table - loading_containers already exists...';
		END			
	GO		
		
	IF OBJECT_ID('dbo.loading_regions') IS NULL
		BEGIN	
			PRINT N'Creating [loading_regions]...';
			CREATE TABLE [loading_regions] (
				[regionId]                     NUMERIC (19) NOT NULL,
				[loadingRouteIssuance]         INT          NOT NULL,
				[containerIDDigitsOperSpeaks]  INT          NOT NULL,
				[trailerIDDigitsOperSpeaks]    INT          NOT NULL,
				[maxConsolidationOfContainers] INT          NULL,
				PRIMARY KEY CLUSTERED ([regionId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
		END
	ELSE
		BEGIN
			print N'table - loading_regions already exists...';
		END	
	GO		
		
	IF OBJECT_ID('dbo.loading_route_import_data') IS NULL
		BEGIN	
			PRINT N'Creating [loading_route_import_data]...';
			CREATE TABLE [loading_route_import_data] (
				[importID]            NUMERIC (19)   NOT NULL,
				[routeNumber]         NVARCHAR (50)  NOT NULL,
				[dockDoorLocationId]  NVARCHAR (50)  NOT NULL,
				[trailer]             NVARCHAR (50)  NULL,
				[departureDateTime]   DATETIME       NOT NULL,
				[regionNumber]        NUMERIC (19)   NOT NULL,
				[note]                NVARCHAR (255) NULL,
				[captureLoadPosition] NUMERIC (19)   NOT NULL,
				[importStatus]        INT            DEFAULT ((0)) NOT NULL,
				[siteName]            NVARCHAR (255) NOT NULL,
				PRIMARY KEY CLUSTERED ([importID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF),
				UNIQUE NONCLUSTERED ([routeNumber] ASC, [departureDateTime] ASC, [regionNumber] ASC, [siteName] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF) ON [PRIMARY]
			);
		END
	ELSE
		BEGIN
			print N'table - loading_route_import_data already exists...';
		END			
	GO
	
	IF OBJECT_ID('dbo.loading_route_operators') IS NULL
		BEGIN	
			PRINT N'Creating [loading_route_operators]...';
			CREATE TABLE [loading_route_operators] (
				[routeID]    NUMERIC (19) NOT NULL,
				[operatorID] NUMERIC (19) NOT NULL,
				PRIMARY KEY CLUSTERED ([routeID] ASC, [operatorID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
		
		END
	ELSE
		BEGIN
			print N'table - loading_route_operators already exists...';
		END			
	GO			
		
	IF OBJECT_ID('dbo.loading_route_tag') IS NULL
		BEGIN
			PRINT N'Creating [loading_route_tag]...';
			CREATE TABLE [loading_route_tag] (
				[tagged_id] NUMERIC (19) NOT NULL,
				[tag_id]    NUMERIC (19) NOT NULL,
				PRIMARY KEY CLUSTERED ([tagged_id] ASC, [tag_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
		END
	ELSE
		BEGIN
			print N'table - loading_route_tag already exists...';
		END		
	GO		
		
	IF OBJECT_ID('dbo.loading_routes') IS NULL
		BEGIN			
			PRINT N'Creating [loading_routes]...';
			CREATE TABLE [loading_routes] (
				[routeId]             NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
				[version]             INT            NOT NULL,
				[routeNumber]         NVARCHAR (50)  NULL,
				[type]                INT            NOT NULL,
				[status]              INT            NOT NULL,
				[dockDoorLocationID]  NUMERIC (19)   NOT NULL,
				[trailer]             NVARCHAR (9)   NULL,
				[departureDateTime]   DATETIME       NOT NULL,
				[startTime]           DATETIME       NULL,
				[endTime]             DATETIME       NULL,
				[captureLoadPosition] NUMERIC (19)   NOT NULL,
				[regionId]            NUMERIC (19)   NOT NULL,
				[note]                NVARCHAR (255) NULL,
				[createdDate]         DATETIME       NOT NULL,
				[exportStatus]        INT            NOT NULL,
				[purgeable]           INT            DEFAULT ((0)) NOT NULL,
				PRIMARY KEY CLUSTERED ([routeId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			PRINT N'Creating [loading_routes].[IX_Route_CreatedDate]...';
			CREATE NONCLUSTERED INDEX [IX_Route_CreatedDate]
				ON [loading_routes]([createdDate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [loading_routes].[IX_LoadRoute_purgeable]...';
			CREATE NONCLUSTERED INDEX [IX_LoadRoute_purgeable]
				ON [loading_routes]([purgeable] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [loading_routes].[IX_load_route_export_status]...';
			CREATE NONCLUSTERED INDEX [IX_load_route_export_status]
				ON [loading_routes]([exportStatus] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - loading_routes already exists...';
		END			
	GO		
				
	IF OBJECT_ID('dbo.loading_routes_labor') IS NULL
		BEGIN	
			PRINT N'Creating [loading_routes_labor]...';
			CREATE TABLE [loading_routes_labor] (
				[routeLaborId]     NUMERIC (19) IDENTITY (1, 1) NOT NULL,
				[version]          INT          NOT NULL,
				[routeId]          NUMERIC (19) NOT NULL,
				[operatorId]       NUMERIC (19) NOT NULL,
				[operatorLaborId]  NUMERIC (19) NULL,
				[breakLaborId]     NUMERIC (19) NULL,
				[startTime]        DATETIME     NOT NULL,
				[endTime]          DATETIME     NULL,
				[duration]         NUMERIC (19) NOT NULL,
				[exportStatus]     INT          NOT NULL,
				[containersLoaded] INT          NOT NULL,
				[actualRate]       FLOAT        NOT NULL,
				[percentOfGoal]    FLOAT        NOT NULL,
				[createdDate]      DATETIME     NOT NULL,
				PRIMARY KEY CLUSTERED ([routeLaborId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			PRINT N'Creating [loading_routes_labor].[IX_loadingRoute_breakLabor]...';
			CREATE NONCLUSTERED INDEX [IX_loadingRoute_breakLabor]
				ON [loading_routes_labor]([breakLaborId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [loading_routes_labor].[IX_loadingRouteLabor_operator]...';
			CREATE NONCLUSTERED INDEX [IX_loadingRouteLabor_operator]
				ON [loading_routes_labor]([operatorId] ASC, [endTime] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [loading_routes_labor].[IX_RouteLabor_CreatedDate]...';
			CREATE NONCLUSTERED INDEX [IX_RouteLabor_CreatedDate]
				ON [loading_routes_labor]([createdDate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [loading_routes_labor].[IX_loadRouteLabor_loadingRoute]...';
			CREATE NONCLUSTERED INDEX [IX_loadRouteLabor_loadingRoute]
				ON [loading_routes_labor]([routeId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [loading_routes_labor].[IX_loadingRouteLabor_operLabor]...';
			CREATE NONCLUSTERED INDEX [IX_loadingRouteLabor_operLabor]
				ON [loading_routes_labor]([operatorLaborId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - loading_routes_labor already exists...';
		END			
	GO		
		
		
	IF OBJECT_ID('dbo.loading_stop_import_data') IS NULL
		BEGIN	
			PRINT N'Creating [loading_stop_import_data]...';
			CREATE TABLE [loading_stop_import_data] (
				[importID]                   NUMERIC (19)   NOT NULL,
				[routeId]                    NUMERIC (19)   NOT NULL,
				[stopNumber]                 NUMERIC (19)   NOT NULL,
				[expectedNumberOfContainers] INT            NOT NULL,
				[customerNumber]             NVARCHAR (50)  NOT NULL,
				[note]                       NVARCHAR (255) NULL,
				[importStatus]               INT            DEFAULT ((0)) NOT NULL,
				[siteName]                   NVARCHAR (255) NOT NULL,
				PRIMARY KEY CLUSTERED ([importID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			PRINT N'Creating [loading_stop_import_data].[IX_loading_stop_route_import]...';
			CREATE NONCLUSTERED INDEX [IX_loading_stop_route_import]
				ON [loading_stop_import_data]([routeId] ASC, [siteName] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - loading_stop_import_data already exists...';
		END			
	GO		
		
		
	IF OBJECT_ID('dbo.loading_stop_tag') IS NULL
		BEGIN	
			PRINT N'Creating [loading_stop_tag]...';
			CREATE TABLE [loading_stop_tag] (
				[tagged_id] NUMERIC (19) NOT NULL,
				[tag_id]    NUMERIC (19) NOT NULL,
				PRIMARY KEY CLUSTERED ([tagged_id] ASC, [tag_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
		END
	ELSE
		BEGIN
			print N'table - loading_stop_tag already exists...';
		END			
	GO		
		
		
	IF OBJECT_ID('dbo.loading_stops') IS NULL
		BEGIN	
			PRINT N'Creating [loading_stops]...';
			CREATE TABLE [loading_stops] (
				[stopId]                     NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
				[version]                    INT            NOT NULL,
				[stopNumber]                 NUMERIC (19)   NOT NULL,
				[status]                     INT            NOT NULL,
				[expectedNumberOfContainers] INT            NOT NULL,
				[note]                       NVARCHAR (255) NULL,
				[routeId]                    NUMERIC (19)   NOT NULL,
				[customerNumber]             NVARCHAR (50)  NOT NULL,
				[operatorId]                 NUMERIC (19)   NULL,
				[startTime]                  DATETIME       NULL,
				[endTime]                    DATETIME       NULL,
				[createdDate]                DATETIME       NOT NULL,
				[exportStatus]               INT            NOT NULL,
				PRIMARY KEY CLUSTERED ([stopId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			PRINT N'Creating [loading_stops].[IX_load_stop_export_status]...';
			CREATE NONCLUSTERED INDEX [IX_load_stop_export_status]
				ON [loading_stops]([exportStatus] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [loading_stops].[IX_Stop_CreatedDate]...';
			CREATE NONCLUSTERED INDEX [IX_Stop_CreatedDate]
				ON [loading_stops]([createdDate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
			PRINT N'Creating [loading_stops].[IX_Load_Stop_Number]...';
			CREATE NONCLUSTERED INDEX [IX_Load_Stop_Number]
				ON [loading_stops]([stopNumber] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
				ON [PRIMARY];
		END
	ELSE
		BEGIN
			print N'table - loading_stops already exists...';
		END	
	GO
	
	
	
-- 
--	Add NEW UOM tables
--			
	IF OBJECT_ID('dbo.core_uom') IS NULL
		BEGIN
  			PRINT N'Creating [core_uom]...';
			CREATE TABLE [core_uom] (
				[uomId]       NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
				[version]     INT           NOT NULL,
				[name]        NVARCHAR (50) NOT NULL,
				[createdDate] DATETIME      NOT NULL,
				PRIMARY KEY CLUSTERED ([uomId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
		END
	ELSE
		BEGIN
			print N'table - core_uom already exists...';
		END	
	GO
		
		
	IF OBJECT_ID('dbo.core_uom_tag') IS NULL
		BEGIN
			PRINT N'Creating [core_uom_tag]...';
			CREATE TABLE [core_uom_tag] (
				[tagged_id] NUMERIC (19) NOT NULL,
				[tag_id]    NUMERIC (19) NOT NULL,
				PRIMARY KEY CLUSTERED ([tagged_id] ASC, [tag_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
		END
	ELSE
		BEGIN
			print N'table - core_uom_tag already exists...';
		END	
	GO
	

	-- ******************************************************************	
	--	Rename these tables to core tables because the delivery 
	--  location mapping feature was moved to core.
	-- ******************************************************************	
	IF OBJECT_ID('dbo.sel_del_loc_map_setting_tag') IS NOT NULL
		BEGIN
			ALTER TABLE [dbo].[sel_del_loc_map_setting_tag] DROP CONSTRAINT [FK13B18B0F99B85CC]
			ALTER TABLE [dbo].[sel_del_loc_map_setting_tag] DROP CONSTRAINT [FK13B18B03EF30942]
			PRINT N'Renaming table [sel_del_loc_map_setting_tag] to [core_del_loc_map_setting_tag]...';
			EXECUTE sp_rename N'[sel_del_loc_map_setting_tag]', N'core_del_loc_map_setting_tag';	
		END
	ELSE
		BEGIN
			print N'table - sel_del_loc_map_setting_tag already renamed...';
		END		
	GO
		
	IF OBJECT_ID('dbo.sel_del_loc_mapping_settings') IS NOT NULL
		BEGIN
			PRINT N'Renaming table  [sel_del_loc_mapping_settings] to [core_del_loc_mapping_settings]...';
			EXECUTE sp_rename N'[sel_del_loc_mapping_settings]', N'core_del_loc_mapping_settings';
		END
	ELSE
		BEGIN
			print N'table - sel_del_loc_mapping_settings already dropped...';
		END			
	GO
				
	IF OBJECT_ID('dbo.sel_del_loc_mappings_tag') IS NOT NULL
		BEGIN
			ALTER TABLE [dbo].[sel_del_loc_mappings_tag] DROP CONSTRAINT [FKDA7354D83EF30942]
			ALTER TABLE [dbo].[sel_del_loc_mappings_tag] DROP CONSTRAINT [FKDA7354D8158AFDC0]
			PRINT N'Renaming table  [sel_del_loc_mappings_tag] to [core_del_loc_mappings_tag]...';
			EXECUTE sp_rename N'[sel_del_loc_mappings_tag]', N'core_del_loc_mappings_tag';			
		END
	ELSE
		BEGIN
			print N'table - sel_del_loc_mappings_tag already dropped...';
		END			
	GO
	
	IF OBJECT_ID('dbo.sel_del_loc_mappings') IS NOT NULL
		BEGIN
			PRINT N'Renaming table from [sel_del_loc_mappings] to [core_del_loc_mappings]...';
			EXECUTE sp_rename N'[sel_del_loc_mappings]', N'core_del_loc_mappings';	
		END
	ELSE
		BEGIN
			print N'table - sel_del_loc_mappings already renamed...';
		END			
	GO
	

		
	-- ***************************************************************************
	--		This table has been removed from EPP
	-- ***************************************************************************
	IF OBJECT_ID('dbo.voc_operand') IS NOT NULL
		BEGIN
			PRINT N'Dropping FK120AE56F1C17A2E2...';
			ALTER TABLE [voc_filter_criterion] DROP CONSTRAINT [FK120AE56F1C17A2E2];
			PRINT N'Dropping FK120AE56F1D4606E2...';
			ALTER TABLE [voc_filter_criterion] DROP CONSTRAINT [FK120AE56F1D4606E2];
			PRINT N'Dropping FK120AE56F64367F32...';
			ALTER TABLE [voc_filter_criterion] DROP CONSTRAINT [FK120AE56F64367F32];
			PRINT N'Dropping [voc_operand]...';
			DROP TABLE [voc_operand];
			-- rename column in voc_filter_criterion
			PRINT N'Renaming column in voc_filter_criterion...';
			EXECUTE sp_rename N'voc_filter_criterion.operand_Id', N'operandId', 'COLUMN';
			ALTER TABLE [dbo].[voc_filter_criterion] ADD CONSTRAINT [FK120AE56F1D4606E2] FOREIGN KEY ([filterCriteriaId]) REFERENCES [dbo].[voc_filter] ([id]) 
			PRINT N'Creating FK120AE56F64367F32...';
			ALTER TABLE [dbo].[voc_filter_criterion] ADD CONSTRAINT [FK120AE56F64367F32] FOREIGN KEY ([column_id]) REFERENCES [dbo].[voc_column] ([id]) 
		END
	ELSE
		BEGIN
			print N'table - voc_operand has already been removed...';
		END			
	GO	
		
	--
	-- Delete bad system translations
	--
	PRINT N'Deleting unused keys from VOC_SYSTEM_TRANSLATIONS'
	DELETE FROM VOC_SYSTEM_TRANSLATIONS WHERE code = 1000 and (locale = 'da' or locale = 'en_AU')
	GO		

	--
	-- Delete selection delivery mappings 
	--	
	PRINT N'Deleting old delivery location mapping type keys from VOC_SYSTEM_TRANSLATIONS'
	DELETE FROM VOC_SYSTEM_TRANSLATIONS WHERE KEYVALUE = N'com.vocollect.voicelink.selection.model.DeliveryLocationMappingType';
	GO

		--
		-- Inserting data into table CORE_TASK_FUNCTIONS
		--
	IF (SELECT COUNT(*) from core_task_functions WHERE taskFunctionId = -10) = 1
		BEGIN
			PRINT N'New core_task_functions were already added'
		END
	ELSE
		BEGIN
			PRINT N'Inserting new core_task_functions'
			SET IDENTITY_INSERT [dbo].[core_task_functions] ON; 
			INSERT INTO [dbo].[core_task_functions] ([taskFunctionId], [functionType], [regionType], [isEnabled], [hasRegions]) VALUES (-10, 10, 7, 1, 1) 
			INSERT INTO [dbo].[core_task_functions] ([taskFunctionId], [functionType], [regionType], [isEnabled], [hasRegions]) VALUES (-9, 9, 6, 1, 1) 
			SET IDENTITY_INSERT [dbo].[core_task_functions] OFF; 
		END
	GO
		--
		-- Inserting data into table CORE_WORK_GROUP_FUNCTIONS
		--
	IF (SELECT COUNT(*) from core_work_group_functions WHERE workgroupFunctionId = -10) = 1
		BEGIN
			PRINT N'New core_task_functions were already added'
		END
	ELSE
		BEGIN
			PRINT N'Inserting new core_work_group_functions'
			SET IDENTITY_INSERT [dbo].[core_work_group_functions] ON; 
			INSERT INTO [dbo].[core_work_group_functions] ([workgroupFunctionId], [version], [workgroupId], [taskFunctionId], [createdDate]) VALUES (-10, 0, -1, -10, '20080304 09:16:39.753') 
			INSERT INTO [dbo].[core_work_group_functions] ([workgroupFunctionId], [version], [workgroupId], [taskFunctionId], [createdDate]) VALUES (-9, 0, -1, -9, '20080304 09:16:39.753') 
			SET IDENTITY_INSERT [dbo].[core_work_group_functions] OFF; 
		END
	GO

	--
	-- Updating data of table SEL_SUM_PROMPT_ITEM
	--
	PRINT N'Updating sel_sum_prompt_item'
	UPDATE [dbo].[sel_sum_prompt_item] SET [assignmentProperty]=N'localizedRoute' WHERE [promptItemId]=-4 
	GO
	--
	-- Updating data of table VOC_CHART_SETTINGS
	--
	PRINT N'Updating voc_chart_settings'
	UPDATE [dbo].[voc_chart_settings] SET [includeLegend]=1 WHERE [chartSettingsId]=1 
	UPDATE [dbo].[voc_chart_settings] SET [includeLegend]=1 WHERE [chartSettingsId]=2 
	UPDATE [dbo].[voc_chart_settings] SET [includeLegend]=1 WHERE [chartSettingsId]=3 
	GO
		
	--
	-- Inserting data into table VOC_COLUMN
	--
	IF (SELECT COUNT(*) from voc_column WHERE id = -23710) = 0
		BEGIN
			PRINT N'Dropping constraint FK6049706B16D0F6D2'
			ALTER TABLE [dbo].[voc_column] DROP CONSTRAINT [FK6049706B16D0F6D2]
			PRINT N'Inserting new voc columns'
			SET IDENTITY_INSERT [dbo].[VOC_COLUMN] ON;
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23710, -1223, N'numberOfOperators', N'laborSummaryByRegion.view.column.numberOfOperators', 0, 5, 0, 1, 1, 90, 1, 8, NULL, 1, NULL, NULL, N'displayNumberOfOperators_load', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23709, -1223, N'site.name', N'common.view.column.site', 0, 5, 1, 1, 1, 120, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, N'site.id', NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23708, -1223, N'region.number', N'common.view.column.regionNumber', 0, 5, 1, 1, 1, 80, 0, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23707, -1223, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23706, -1223, N'totalTime', N'common.view.column.laborTotalTime', 0, 5, 0, 1, 1, 90, 1, 7, NULL, 1, N'formatTimeDurationFromLong', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23705, -1223, N'percentOfGoal', N'common.view.column.laborPercentOfGoal', 0, 5, 0, 1, 1, 90, 1, 3, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23704, -1223, N'actualRate', N'common.view.column.laborActualRate', 0, 5, 0, 1, 1, 90, 1, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23703, -1223, N'goalRate', N'common.view.column.regionGoalRate', 0, 5, 0, 1, 1, 90, 1, 5, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23702, -1223, N'totalQuantity', N'common.view.column.laborTotalQuantity', 0, 5, 0, 1, 1, 90, 1, 6, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23701, -1223, N'region.name', N'common.view.column.regionName', 1, 8, 0, 1, 1, 150, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, N'region.id', NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23610, -1222, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23609, -1222, N'estimatedCompleted', N'loadingCurrentWorkSummary.view.column.estimatedRegionCompleted', 0, 5, 0, 1, 1, 110, 1, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23608, -1222, N'region.goalRate', N'common.view.column.regionGoalRate', 0, 5, 0, 1, 1, 90, 1, 8, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23607, -1222, N'totalRoutesRemaining', N'loadingCurrentWorkSummary.view.column.totalRoutesRemaining', 0, 5, 0, 1, 1, 110, 1, 7, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23606, -1222, N'totalRoutesCompleted', N'loadingCurrentWorkSummary.view.column.totalRoutesCompleted', 0, 5, 0, 1, 1, 110, 1, 6, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23605, -1222, N'totalRoutes', N'loadingCurrentWorkSummary.view.column.totalRoutes', 0, 5, 0, 1, 1, 110, 1, 5, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23604, -1222, N'operatorsWorkingIn', N'loadingCurrentWorkSummary.view.column.operatorsWorking', 0, 5, 0, 1, 1, 110, 1, 4, NULL, 1, NULL, NULL, N'displayOperatorsByRegionAndSignedOn_load', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23603, -1222, N'region.number', N'common.view.column.regionNumber', 0, 5, 0, 1, 1, 80, 0, 3, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23602, -1222, N'region.name', N'common.view.column.regionName', 1, 8, 0, 1, 1, 110, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, N'region.id', NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23601, -1222, N'site.name', N'common.view.column.site', 1, 5, 1, 1, 1, 120, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, N'site.id', NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23509, -1221, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23508, -1221, N'nonComplete', N'loadingRouteSummary.view.column.noncompleteAssignemnts', 0, 5, 0, 1, 1, 110, 1, 8, NULL, 1, NULL, NULL, N'displayRouteByRegionAndNotComplete_load', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23507, -1221, N'complete', N'loadingRouteSummary.view.column.completeAssignemnts', 0, 5, 0, 1, 1, 110, 1, 7, NULL, 1, NULL, NULL, N'displayRouteByRegionAndComplete_load', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23506, -1221, N'available', N'loadingRouteSummary.view.column.availableAssignments', 0, 5, 0, 1, 1, 110, 1, 6, NULL, 1, NULL, NULL, N'displayRouteByRegionAndAvailable_load', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23505, -1221, N'inProgress', N'loadingRouteSummary.view.column.inProgressAssignments', 0, 5, 0, 1, 1, 110, 1, 5, NULL, 1, NULL, NULL, N'displayRouteByRegionAndInProgress_load', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23504, -1221, N'totalAssignments', N'loadingRouteSummary.view.column.totalAssignments', 0, 5, 0, 1, 1, 110, 1, 4, NULL, 1, NULL, NULL, N'displayRouteByRegion_load', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23503, -1221, N'region.number', N'common.view.column.regionNumber', 0, 5, 0, 1, 1, 80, 0, 3, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23502, -1221, N'region.name', N'common.view.column.regionName', 1, 8, 0, 1, 1, 110, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, N'region.id', NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23501, -1221, N'site.name', N'common.view.column.site', 1, 5, 1, 1, 1, 120, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'site.id', NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23410, -1214, N'numberOfOperators', N'laborSummaryByRegion.view.column.numberOfOperators', 0, 5, 0, 1, 1, 90, 1, 8, NULL, 1, NULL, NULL, N'displayNumberOfOperators_cc', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23409, -1214, N'site.name', N'common.view.column.site', 0, 5, 1, 1, 1, 120, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, N'site.id', NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23408, -1214, N'region.number', N'common.view.column.regionNumber', 0, 5, 1, 1, 1, 80, 0, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23407, -1214, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23406, -1214, N'totalTime', N'common.view.column.laborTotalTime', 0, 5, 0, 1, 1, 90, 1, 7, NULL, 1, N'formatTimeDurationFromLong', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23405, -1214, N'percentOfGoal', N'common.view.column.laborPercentOfGoal', 0, 5, 0, 1, 1, 90, 1, 3, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23404, -1214, N'actualRate', N'common.view.column.laborActualRate', 0, 5, 0, 1, 1, 90, 1, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23403, -1214, N'goalRate', N'common.view.column.regionGoalRate', 0, 5, 0, 1, 1, 90, 1, 5, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23402, -1214, N'totalQuantity', N'common.view.column.laborTotalQuantity', 0, 5, 0, 1, 1, 90, 1, 6, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23401, -1214, N'region.name', N'common.view.column.regionName', 1, 8, 0, 1, 1, 150, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, N'region.id', NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23310, -1213, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23309, -1213, N'estimatedCompleted', N'ccCurrentWorkSummary.view.column.estimatedRegionCompleted', 0, 5, 0, 1, 1, 110, 1, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23308, -1213, N'region.goalRate', N'common.view.column.regionGoalRate', 0, 5, 0, 1, 1, 90, 1, 8, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23307, -1213, N'totalLocationsRemaining', N'ccCurrentWorkSummary.view.column.locationsRemaining', 0, 5, 0, 1, 1, 110, 1, 7, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23306, -1213, N'totalLocationsCounted', N'ccCurrentWorkSummary.view.column.locationsCounted', 0, 5, 0, 1, 1, 110, 1, 6, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23304, -1213, N'operatorsWorkingIn', N'ccCurrentWorkSummary.view.column.operatorsWorking', 0, 5, 0, 1, 1, 110, 1, 4, NULL, 1, NULL, NULL, N'displayOperatorsByRegionAndSignedOn_cc', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23303, -1213, N'region.number', N'common.view.column.regionNumber', 0, 5, 0, 1, 1, 80, 0, 3, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23302, -1213, N'region.name', N'common.view.column.regionName', 1, 8, 0, 1, 1, 110, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, N'region.id', NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23301, -1213, N'site.name', N'common.view.column.site', 1, 5, 1, 1, 1, 120, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, N'site.id', NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23209, -1212, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23208, -1212, N'nonComplete', N'ccAssignmentSummary.view.column.noncompleteAssignemnts', 0, 5, 0, 1, 1, 110, 1, 8, NULL, 1, NULL, NULL, N'displayAssignByRegionAndNotComplete_cc', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23207, -1212, N'complete', N'ccAssignmentSummary.view.column.completeAssignemnts', 0, 5, 0, 1, 1, 110, 1, 7, NULL, 1, NULL, NULL, N'displayAssignByRegionAndComplete_cc', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23206, -1212, N'available', N'ccAssignmentSummary.view.column.availableAssignments', 0, 5, 0, 1, 1, 110, 1, 6, NULL, 1, NULL, NULL, N'displayAssignByRegionAndAvailable_cc', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23205, -1212, N'inProgress', N'ccAssignmentSummary.view.column.inProgressAssignments', 0, 5, 0, 1, 1, 110, 1, 5, NULL, 1, NULL, NULL, N'displayAssignByRegionAndInProgress_cc', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23204, -1212, N'totalAssignments', N'ccAssignmentSummary.view.column.totalAssignments', 0, 5, 0, 1, 1, 110, 1, 4, NULL, 1, NULL, NULL, N'displayAssignByRegion_cc', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23203, -1212, N'region.number', N'common.view.column.regionNumber', 0, 5, 0, 1, 1, 80, 0, 3, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23202, -1212, N'region.name', N'common.view.column.regionName', 1, 8, 0, 1, 1, 110, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, N'region.id', NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23201, -1212, N'site.name', N'common.view.column.site', 1, 5, 1, 1, 1, 120, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, N'site.id', NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23113, -1210, N'item.upc', N'item.view.column.UPC', 0, 1, 0, 1, 1, 100, 0, 11, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23112, -1210, N'item.spokenVerificationCode', N'common.view.column.itemSpokenProductVerification', 0, 1, 0, 1, 1, 110, 0, 10, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23111, -1210, N'item.scanVerificationCode', N'common.view.column.itemScannedProductVerification', 0, 1, 0, 1, 1, 110, 0, 9, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23110, -1210, N'status', N'cyclecounting.view.column.status', 0, 1, 1, 1, 0, 100, 1, 8, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23109, -1210, N'countTime', N'cyclecounting.detail.view.column.countTime', 0, 1, 1, 1, 0, 100, 1, 7, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23108, -1210, N'unitOfMeasure', N'common.view.column.pickUnitOfMeasure', 0, 1, 1, 1, 0, 100, 1, 6, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23107, -1210, N'actualQuantity', N'cyclecounting.detail.view.column.actualQuantity', 0, 1, 1, 1, 0, 100, 1, 5, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23106, -1210, N'expectedQuantity', N'cyclecounting.detail.view.column.expectedQuantity', 0, 1, 1, 1, 0, 100, 1, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23105, -1210, N'reason.reasonNumber', N'reason.codes.view.column.reasonCode', 0, 6, 1, 1, 0, 120, 1, 9, NULL, 1, NULL, NULL, N'displayReasonCode', NULL, N'reason.id', NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23104, -1210, N'item.description', N'common.view.column.itemDescription', 0, 4, 1, 1, 0, 120, 1, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23103, -1210, N'item.number', N'common.view.column.itemNumber', 0, 1, 1, 1, 0, 120, 1, 2, NULL, 1, NULL, NULL, N'displayItemNumber_cc', NULL, N'item.id', NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23102, -1210, N'cycleCountingAssignment.id', N'cyclecounting.detail.view.column.assignmentNumber', 1, 1, 1, 1, 0, 120, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23101, -1210, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23016, -1209, N'createdDate', N'common.view.column.createdDate', 0, 1, 0, 1, 1, 110, 0, 101, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23015, -1209, N'location.description.postAisle', N'common.view.column.locationPostAisle', 0, 8, 0, 1, 1, 150, 0, 15, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23014, -1209, N'location.description.preAisle', N'common.view.column.locationPreAisle', 0, 8, 0, 1, 1, 150, 0, 14, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23013, -1209, N'location.spokenVerification', N'location.view.column.spokenLocation', 0, 1, 0, 1, 1, 150, 1, 13, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23012, -1209, N'location.scannedVerification', N'common.view.column.locationId', 0, 1, 0, 1, 0, 120, 1, 12, NULL, 0, NULL, NULL, N'displayLocationId_cc', NULL, N'location.id', NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23011, -1209, N'region.name', N'common.view.column.regionName', 0, 1, 1, 1, 0, 120, 1, 11, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23010, -1209, N'region.number', N'common.view.column.regionNumber', 0, 1, 1, 1, 0, 80, 1, 10, NULL, 1, NULL, NULL, N'displayRegionNumber_cc', NULL, N'region.id', NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23009, -1209, N'endTime', N'cyclecounting.view.column.endTime', 0, 1, 1, 1, 0, 100, 1, 9, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23008, -1209, N'startTime', N'cyclecounting.view.column.startTime', 0, 1, 1, 1, 0, 100, 1, 8, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23007, -1209, N'operator.common.operatorIdentifier', N'common.view.column.operator', 0, 7, 1, 1, 0, 100, 1, 7, NULL, 0, NULL, NULL, N'displayOperator_cc', NULL, N'operator.id', NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23006, -1209, N'status', N'cyclecounting.view.column.status', 0, 1, 1, 1, 0, 100, 1, 6, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23005, -1209, N'location.checkDigits', N'common.view.column.locationCheckDigits', 0, 1, 0, 1, 1, 110, 1, 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23004, -1209, N'location.description.slot', N'common.view.column.locationSlot', 0, 1, 0, 1, 1, 45, 1, 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23003, -1209, N'location.description.aisle', N'common.view.column.locationAisle', 0, 1, 0, 1, 1, 45, 1, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23002, -1209, N'sequence', N'cyclecounting.view.column.sequence', 1, 1, 1, 1, 1, 120, 1, 2, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-23001, -1209, N'id', N'cyclecounting.view.column.assignmentNumber', 0, 1, 1, 1, 0, 120, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22989, -1220, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22988, -1220, N'percentOfGoal', N'common.view.column.laborPercentOfGoal', 0, 1, 0, 1, 1, 90, 1, 7, NULL, 1, N'formatDecimalValue', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22987, -1220, N'actualRate', N'common.view.column.laborActualRate', 0, 1, 0, 1, 1, 90, 1, 8, NULL, 1, N'formatDecimalValue', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22986, -1220, N'route.region.goalRate', N'common.view.column.regionGoalRate', 0, 1, 0, 1, 1, 90, 1, 9, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22985, -1220, N'containersLoaded', N'loading.labor.view.column.containersLoaded', 0, 1, 0, 1, 1, 90, 1, 6, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22984, -1220, N'duration', N'common.view.column.laborTotalTime', 0, 1, 0, 1, 1, 90, 1, 5, NULL, 1, N'formatTimeDurationFromLong', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22983, -1220, N'endTime', N'common.view.column.laborEndTime', 0, 1, 0, 1, 1, 140, 1, 4, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22982, -1220, N'startTime', N'common.view.column.laborStartTime', 1, 1, 0, 1, 1, 140, 1, 3, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22981, -1220, N'route.region.name', N'loading.routes.column.region', 0, 1, 0, 1, 1, 100, 1, 11, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22980, -1220, N'route.number', N'loading.routes.column.route', 1, 1, 0, 1, 1, 100, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22954, -1211, N'duration', N'common.view.column.laborTotalTime', 1, 1, 0, 1, 1, 100, 1, 5, NULL, 0, N'formatTimeDurationFromLong', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22953, -1211, N'endTime', N'common.view.column.laborEndTime', 1, 1, 0, 1, 1, 100, 1, 4, NULL, 0, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22952, -1211, N'startTime', N'common.view.column.laborStartTime', 1, 1, 0, 1, 1, 100, 1, 3, NULL, 0, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22951, -1211, N'operator.common.operatorIdentifier', N'common.view.column.operator', 1, 1, 0, 1, 1, 100, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22950, -1211, N'assignment.id', N'cyclecounting.view.column.assignmentNumber', 1, 1, 0, 1, 1, 100, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22914, -1219, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22913, -1219, N'loadTime', N'loading.container.column.loadTime', 1, 1, 0, 1, 1, 100, 0, 1, NULL, 2,  N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22912, -1219, N'expectedWeight', N'loading.container.column.weight', 0, 1, 0, 1, 1, 100, 1, 8, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22911, -1219, N'expectedCube', N'loading.container.column.cube', 0, 1, 0, 1, 1, 100, 1, 7, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22910, -1219, N'type', N'loading.container.column.type', 0, 1, 0, 1, 1, 200, 1, 6, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22909, -1219, N'stop.route.departureDateTime', N'loading.container.column.departureDateTime', 1, 1, 0, 1, 1, 100, 1, 5, NULL, 2, NULL, NULL, N'displayDepartureDateForContainer_load', NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22908, -1219, N'stop.route.region.number', N'loading.container.column.regionNumber', 0, 1, 0, 1, 1, 100, 0, 14, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22907, -1219, N'stop.route.region.name', N'loading.container.column.regionName', 0, 1, 0, 1, 1, 100, 1, 13, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22906, -1219, N'stop.number', N'loading.container.column.stopNumber', 1, 1, 0, 1, 1, 100, 1, 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22905, -1219, N'stop.route.number', N'loading.container.column.routeNumber', 1, 1, 0, 1, 1, 100, 1, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22904, -1219, N'operator.common.operatorIdentifier', N'common.view.column.operator', 1, 7, 0, 1, 1, 100, 0, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22903, -1219, N'masterContainerNumber', N'loading.container.column.masterContainerNumber', 1, 1, 0, 1, 1, 100, 0, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22902, -1219, N'status', N'loading.container.column.status', 0, 3, 0, 1, 1, 100, 1, 2, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22901, -1219, N'containerNumber', N'loading.container.column.containerNumber', 1, 1, 0, 1, 1, 100, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22833, -1218, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22832, -1218, N'route.number', N'loading.stop.column.routeNumber', 0, 1, 1, 0, 1, 150, 0, 12, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22831, -1218, N'note', N'loading.stop.column.note', 0, 1, 0, 1, 1, 200, 0, 11, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22830, -1218, N'assignmentCount', N'loading.stop.column.assignmentCount', 0, 1, 0, 1, 1, 100, 1, 10, NULL, 1, NULL, NULL, N'displayAssignmentsForStop_load', NULL, NULL, NULL, 1, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22829, -1218, N'endTime', N'loading.stop.column.endTime', 0, 1, 0, 1, 1, 100, 1, 9, NULL, 2,  N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22828, -1218, N'startTime', N'loading.stop.column.startTime', 0, 1, 0, 1, 1, 100, 1, 8, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22827, -1218, N'route.region.number', N'loading.stop.column.regionNumber', 0, 1, 0, 1, 1, 100, 0, 7, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22826, -1218, N'route.region.name', N'loading.stop.column.regionName', 0, 1, 0, 1, 1, 100, 1, 6, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22825, -1218, N'actualNumberOfContainers', N'loading.stop.column.actualContainers', 0, 5, 0, 1, 1, 150, 1, 5, NULL, 1, NULL, NULL, N'displayContainerForStop_load', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22824, -1218, N'expectedNumberOfContainers', N'loading.stop.column.expectedContainers', 0, 1, 0, 1, 1, 150, 1, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22823, -1218, N'status', N'loading.stop.column.status', 0, 3, 0, 1, 1, 150, 1, 3, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22822, -1218, N'customerNumber', N'loading.stop.column.customerNumber', 5, 5, 0, 1, 1, 150, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22821, -1218, N'number', N'loading.stop.column.stop', 1, 1, 0, 1, 1, 100, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22719, -1217, N'loadedNumberOfContainers', N'loading.routes.column.loadedContainers', 0, 5, 0, 1, 1, 100, 1, 9, NULL, 1, NULL, NULL, N'displayContainerForRoute_loaded', NULL, NULL, NULL, 1, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22718, -1217, N'createdDate', N'common.view.column.createdDate', 0, 1, 0, 1, 1, 110, 0, 101, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22717, -1217, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22716, -1217, N'note', N'loading.routes.column.note', 0, 1, 0, 1, 1, 100, 0, 16, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22715, -1217, N'region.number', N'loading.routes.column.regionNumber', 0, 1, 0, 1, 1, 100, 0, 15, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22714, -1217, N'region.name', N'loading.routes.column.regionName', 0, 1, 0, 1, 1, 100, 1, 14, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22713, -1217, N'captureLoadPositionStringValue', N'loading.routes.column.captureLoadPosition', 0, 5, 0, 1, 1, 100, 0, 12, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22712, -1217, N'endTime', N'loading.routes.column.endTime', 0, 1, 0, 1, 1, 100, 1, 13, NULL, 2,  N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22711, -1217, N'startTime', N'loading.routes.column.startTime', 0, 1, 0, 1, 1, 100, 1, 12, NULL, 2,  N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22710, -1217, N'expectedWeight', N'loading.routes.column.expectedWeight', 0, 1, 0, 1, 1, 100, 1, 11, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22709, -1217, N'expectedCube', N'loading.routes.column.expectedCube', 0, 1, 0, 1, 1, 100, 1, 10, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22708, -1217, N'actualNumberOfContainers', N'loading.routes.column.actualNumberOfContainers', 0, 5, 0, 1, 1, 100, 1, 8, NULL, 1, NULL, NULL, N'displayContainerForRoute_load', NULL, NULL, NULL, 2, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22707, -1217, N'expectedNumberOfContainers', N'loading.routes.column.expectedNumberOfContainers', 0, 1, 0, 1, 1, 100, 1, 7, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 1);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22706, -1217, N'trailer', N'loading.routes.column.trailerNumber', 0, 1, 0, 1, 1, 100, 1, 6, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22705, -1217, N'type', N'loading.routes.column.dedicatedMultiStop', 0, 3, 0, 1, 1, 75, 1, 5, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22704, -1217, N'dockDoor.scannedVerification', N'loading.routes.column.dockDoor', 0, 1, 0, 1, 1, 75, 1, 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22703, -1217, N'status', N'loading.routes.column.status', 0, 3, 0, 1, 1, 100, 1, 3, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22702, -1217, N'departureDateTime', N'loading.routes.column.departureDate', 0, 1, 0, 1, 1, 200, 1, 2, NULL, 2, NULL, NULL, N'displayDepartureDateForRoute_load', NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22701, -1217, N'number', N'loading.routes.column.route', 1, 1, 0, 1, 1, 100, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22609, -1216, N'id', NULL, 0, 1, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22608, -1216, N'goalRate', N'common.view.column.regionGoalRate', 0, 1, 1, 1, 1, 90, 1, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22607, -1216, N'description', N'loading.region.view.column.description', 0, 4, 0, 1, 1, 120, 1, 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22606, -1216, N'maxConsolidationOfContainers', N'loading.region.view.column.maxConsolidationOfContainers', 0, 1, 0, 1, 1, 150, 1, 6, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22605, -1216, N'trailerIDDigitsOperSpeaks', N'loading.region.view.column.trailerIDDigitsOperSpeaks', 0, 1, 0, 1, 1, 150, 1, 5, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22604, -1216, N'containerIDDigitsOperSpeaks', N'loading.region.view.column.containerIDDigitsOperSpeaks', 0, 1, 0, 1, 1, 150, 1, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22603, -1216, N'loadingRouteIssuance', N'loading.region.view.column.loadingRouteIssuance', 0, 3, 0, 1, 1, 150, 1, 3, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22602, -1216, N'name', N'common.view.column.regionName', 1, 4, 0, 1, 1, 120, 1, 2, NULL, 0, NULL, NULL, N'displayLoadingRegion', NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22601, -1216, N'number', N'common.view.column.regionNumber', 0, 1, 0, 1, 1, 80, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22509, -1208, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22508, -1208, N'description', N'cyclecounting.region.view.column.description', 0, 1, 1, 1, 1, 160, 0, 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22505, -1208, N'goalRate', N'common.view.column.regionGoalRate', 0, 1, 1, 1, 1, 90, 1, 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22504, -1208, N'countType', N'cyclecounting.region.view.column.countType', 0, 1, 1, 1, 1, 160, 1, 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22503, -1208, N'countMode', N'cyclecounting.region.view.column.countMode', 0, 1, 1, 1, 1, 160, 1, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22502, -1208, N'name', N'common.view.column.regionName', 0, 1, 1, 1, 1, 160, 1, 2, NULL, 0, NULL, NULL, N'displayRegionName', NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-22501, -1208, N'number', N'common.view.column.regionNumber', 0, 1, 1, 1, 1, 80, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-11802, -1050, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-11801, -1050, N'name', N'unitofmeasure.view.column.name', 1, 4, 0, 1, 1, 120, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-10831, -1009, N'departureDateTime', N'common.view.column.loading.departureDateTime', 0, 1, 0, 1, 1, 110, 0, 31, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			INSERT INTO VOC_COLUMN(ID, VIEW_ID, FIELD, DISPLAY, SORTED, SORTTYPE, REQUIRED, DISPLAYABLE, SORT_ASC, WIDTH, VISIBLE, COLUMN_ORDER, DATA_TYPE_ID, OPERAND_TYPE, DISPLAY_FUNCTION, DISPLAY_PARAM, PAINTER_FUNCTION, TOOL_TIP_FUNCTION, EXTRA_FIELDS, EXTRA_FILTER_INFO, FILTERTYPE, FILTER_ENUM_TYPE, OPTION_KEY_PREFIX, FILTER_AUTO_COMPLETE_DISABLE) VALUES
			(-10830, -1009, N'loadingRegion.name', N'common.view.column.loading.regionName', 0, 5, 0, 1, 1, 110, 1, 30, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL);
			SET IDENTITY_INSERT [dbo].[voc_column] OFF;
		END
	ELSE
		BEGIN
			PRINT N'New voc_columns were  already added'
		END
		
	GO
		
	--
	-- Updating data of table VOC_COLUMN
	--
	PRINT N'Updating voc_columns'
	UPDATE VOC_COLUMN SET SORTTYPE = 5, FILTERTYPE = 2 WHERE ID = -22302;
	UPDATE VOC_COLUMN SET FILTERTYPE = 2 WHERE ID = -22301;
	UPDATE VOC_COLUMN SET DISPLAY = NULL, WIDTH = 0 WHERE ID = -22218;
	UPDATE VOC_COLUMN SET WIDTH = 80 WHERE ID = -22217;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -22216;
	UPDATE VOC_COLUMN SET FIELD = N'container.customer.location.description.direction', SORTTYPE = 8, DISPLAY_FUNCTION = NULL, PAINTER_FUNCTION = NULL, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -22203;
	UPDATE VOC_COLUMN SET FIELD = N'customer.location.description.direction', SORTTYPE = 8, DISPLAY_FUNCTION = NULL, PAINTER_FUNCTION = NULL, FILTERTYPE = 2, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -22006;
	UPDATE VOC_COLUMN SET WIDTH = 80 WHERE ID = -21917;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, FILTERTYPE = 2 WHERE ID = -21916;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayLocationId_pts' WHERE ID = -21914;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, FILTERTYPE = 2 WHERE ID = -21912;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, FILTERTYPE = 2 WHERE ID = -21910;
	UPDATE VOC_COLUMN SET FIELD = N'put.customer.location.description.direction', SORTTYPE = 8, DISPLAY_FUNCTION = NULL, PAINTER_FUNCTION = NULL WHERE ID = -21903;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayConfirmSpokenLocation_pts' WHERE ID = -21815;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayValidateContainerIds_pts' WHERE ID = -21813;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displaySpokenLicenseLength_pts' WHERE ID = -21812;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayConfirmSpokenLicense_pts' WHERE ID = -21811;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displaySystemGeneratesContainerID_pts' WHERE ID = -21810;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowMultipleOpenContainers_pts' WHERE ID = -21809;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowPassAssignment_pts' WHERE ID = -21808;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowSignOff_pts' WHERE ID = -21807;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowRepickSkips_pts' WHERE ID = -21806;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowSkipping_pts' WHERE ID = -21805;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayRegionName' WHERE ID = -21801;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21707;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21706;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21705;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21704;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21703;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21609;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21608;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21607;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21606;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21605;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21604;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21603;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21602;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21508;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21507;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21506;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21505;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21504;
	UPDATE VOC_COLUMN SET WIDTH = 80, FILTER_AUTO_COMPLETE_DISABLE = 1  WHERE ID = -21503;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21410;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21409;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21408;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21407;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21406;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21404;
	UPDATE VOC_COLUMN SET WIDTH = 80, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21403;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21402;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, FILTERTYPE = 2 WHERE ID = -21311;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, FILTERTYPE = 2 WHERE ID = -21310;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21303;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, FILTERTYPE = 2 WHERE ID = -21210;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayContainerNumber_pts', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21208;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayItemNumber_pts' WHERE ID = -21201;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowOverPack_pts' WHERE ID = -21127;
	UPDATE VOC_COLUMN SET FIELD = N'operator.common.operatorIdentifier' WHERE ID = -21124;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayPutLocationId_pts' WHERE ID = -21121;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayPutDetail_pts' WHERE ID = -21114;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayItemNumber_pts' WHERE ID = -21110;
	UPDATE VOC_COLUMN SET FIELD = N'customer.location.description.direction', SORTTYPE = 8, DISPLAY_FUNCTION = NULL, PAINTER_FUNCTION = NULL, FILTERTYPE = 2, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -21103;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayLicensePutDetail_pts' WHERE ID = -21011;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayNonCompletedLicenses_putaway' , FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -20307;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayCompletedLicenses_putaway' , FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -20306;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAvailableLicenses_putaway' , FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -20305;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayInProgressLicenses_putaway', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -20304;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayTotalLicenses_putaway', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -20303;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayItemNumber_putawayDetail' WHERE ID = -20212;
	UPDATE VOC_COLUMN SET DISPLAY_FUNCTION = N'formatTimeWithTimeZone' WHERE ID = -20211;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, PAINTER_FUNCTION = N'displayPutLocation_putaway' WHERE ID = -20207;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, PAINTER_FUNCTION = N'displayStartLocation_putaway' WHERE ID = -20206;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayLicenseRegionName_putaway', FILTERTYPE = 2 WHERE ID = -20202;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayLicenseDetails_putaway', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -20120;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayExpirationFlag_putaway', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -20119;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayImported_putaway', TOOL_TIP_FUNCTION = NULL WHERE ID = -20117;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayItemNumber_putaway' WHERE ID = -20115;
	UPDATE VOC_COLUMN SET SORTTYPE = 8 WHERE ID = -20111;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, FILTERTYPE = 2 WHERE ID = -20110;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayLocationIdChildObject' WHERE ID = -20109;
	UPDATE VOC_COLUMN SET SORTED = 1, PAINTER_FUNCTION = N'displayRegion' WHERE ID = -20102;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayCheckDigitsOperatorSpeaks_putaway' WHERE ID = -20020;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayLocDigitsOperatorSpeaks_putaway' WHERE ID = -20019;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayLicDigitsOperatorSpeaks_putaway' WHERE ID = -20018;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayCapturePutQuantity_putaway' WHERE ID = -20016;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayCapturePickupQuantity_putaway' WHERE ID = -20015;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowPartialPut_putaway' WHERE ID = -20014;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowOverrideQuantity_putaway' WHERE ID = -20013;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayVerifySpokenLicOrLoc_putaway' WHERE ID = -20011;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowOverrideLocation_putaway' WHERE ID = -20010;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayVerifyLicense_putaway' WHERE ID = -20008;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowCancelLicense_putaway' WHERE ID = -20007;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowSkipLicense_putaway' WHERE ID = -20006;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayRegionName' WHERE ID = -20001;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14210;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14208;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14206;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14205;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14204;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14203;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14202;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14201;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14110;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14108;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14106;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14105;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14104;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14103;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14102;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14101;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14010;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14009;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14008;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14006;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14005;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14004;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14003;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14002;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -14001;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13736;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13735;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13734;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13733;
	UPDATE VOC_COLUMN SET WIDTH = 80, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13732;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13731;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13728;
	UPDATE VOC_COLUMN SET WIDTH = 90, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13727;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13726;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13725;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13724;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13723;
	UPDATE VOC_COLUMN SET WIDTH = 80, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13722;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13721;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13717;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13716;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13715;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13714;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13713;
	UPDATE VOC_COLUMN SET WIDTH = 80, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13712;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAssignmentsFilterByInComplete_replen', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13708;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAssignmentsFilterByComplete_replen', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13707;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAssignmentsFilterByAvailable_replen', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13706;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAssignmentsFilterByInProgress_replen', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13705;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAssignmentsFilterByTotal_replen' , FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13704;
	UPDATE VOC_COLUMN SET WIDTH = 80, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13703;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13702;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13652;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13651;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13650;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13649;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13648;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13647;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13646;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13645;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13644;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13643;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displaySetAsideShorted_ll' , FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13630;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayMissing_ll', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13629;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayPickShorted_ll', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13628;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayPicked_ll', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13627;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayLoaded_ll', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13626;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAvailable_ll', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13625;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayTotal_ll', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13624;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13623;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, FILTERTYPE = 2 WHERE ID = -13602;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAutomaticallyPrintManifest_ll' WHERE ID = -13537;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowToCloseStop_ll' WHERE ID = -13536;
	UPDATE VOC_COLUMN SET WIDTH = 90 WHERE ID = -13534;
	UPDATE VOC_COLUMN SET WIDTH = 80 WHERE ID = -13532;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayLineLoadingRegion_ll' WHERE ID = -13531;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displaySetAsideOrShortedCartons_ll' , FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13529;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayMissingCartons_ll', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13528;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, PAINTER_FUNCTION = N'displayPickedCartons_ll', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13527;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, PAINTER_FUNCTION = N'displayLoadedCartons_ll', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13526;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAvailableCartons_ll', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13525;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayTotalCartons_ll', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -13524;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayRegionLink_ll' WHERE ID = -13515;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayDetailsCount_ll' WHERE ID = -13514;
	UPDATE VOC_COLUMN SET WIDTH = 80 WHERE ID = -13513;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13329;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13328;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13327;
	UPDATE VOC_COLUMN SET WIDTH = 80 WHERE ID = -13326;
	UPDATE VOC_COLUMN SET WIDTH = 110, PAINTER_FUNCTION = N'displayAssignmentDetails_replen' WHERE ID = -13325;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13324;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13323;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13322;
	UPDATE VOC_COLUMN SET WIDTH = 90 WHERE ID = -13321;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13320;
	UPDATE VOC_COLUMN SET WIDTH = 110, PAINTER_FUNCTION = N'displayItemNumber_replen' WHERE ID = -13319;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13318;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13317;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13316;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13315;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13314;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13313;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13312;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13311;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13310;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13309;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13308;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13307;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13306;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13305;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -13304;
	UPDATE VOC_COLUMN SET WIDTH = 80 WHERE ID = -13228;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAssignmentRoute_sel' WHERE ID = -13211;
	UPDATE VOC_COLUMN SET WIDTH = 90 WHERE ID = -12915;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayNumberCheckDigits_replen' WHERE ID = -12912;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayVerifySpokenLicenseLocation_replen' WHERE ID = -12911;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayNumberLocationDigits_replen' WHERE ID = -12910;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayCapturePutQty_replen' WHERE ID = -12908;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowOverridePickUpQty_replen' WHERE ID = -12907;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayCapturePickUpQty_replen' WHERE ID = -12906;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowPartialPut_replen' WHERE ID = -12905;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowOverrideLocation_replen' WHERE ID = -12904;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAllowCancelLicense_replen' WHERE ID = -12903;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayRegionName' WHERE ID = -12902;
	UPDATE VOC_COLUMN SET WIDTH = 80 WHERE ID = -12901;
	UPDATE VOC_COLUMN SET SORTTYPE = 8 WHERE ID = -12804;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12803;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayItemNumberMapped', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12802;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12801;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12715;
	UPDATE VOC_COLUMN SET WIDTH = 110, FILTER_ENUM_TYPE = N'com.vocollect.voicelink.replenishment.model.ReplenishType' WHERE ID = -12714;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12712;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12711;
	UPDATE VOC_COLUMN SET WIDTH = 110, PAINTER_FUNCTION = N'displayItemNumber_replenDetail' WHERE ID = -12710;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12709;
	UPDATE VOC_COLUMN SET WIDTH = 110, PAINTER_FUNCTION = N'displayReasonCode' WHERE ID = -12708;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12707;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12706;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12705;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12704;
	UPDATE VOC_COLUMN SET WIDTH = 110, PAINTER_FUNCTION = N'displayDestinationLocation_replen' WHERE ID = -12703;
	UPDATE VOC_COLUMN SET WIDTH = 110, PAINTER_FUNCTION = N'displayReserveLocation_replen' WHERE ID = -12702;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12701;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12629;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12628;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12627;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12626;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12625;
	UPDATE VOC_COLUMN SET WIDTH = 80 WHERE ID = -12624;
	UPDATE VOC_COLUMN SET WIDTH = 110, PAINTER_FUNCTION = N'displayAssignmentDetails_replen' WHERE ID = -12623;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12622;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12621;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12620;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12619;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12618;
	UPDATE VOC_COLUMN SET WIDTH = 110, PAINTER_FUNCTION = N'displayItemNumber_replen' WHERE ID = -12617;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12616;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12615;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12614;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12613;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12612;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12611;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12610;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12609;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12608;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12607;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12606;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12605;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12604;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12603;
	UPDATE VOC_COLUMN SET WIDTH = 110, FILTER_ENUM_TYPE = N'com.vocollect.voicelink.replenishment.model.ReplenishPriority' WHERE ID = -12602;
	UPDATE VOC_COLUMN SET WIDTH = 110, PAINTER_FUNCTION = N'displayIssuanceOrder_replen', TOOL_TIP_FUNCTION = NULL WHERE ID = -12601;
	UPDATE VOC_COLUMN SET WIDTH = 120 WHERE ID = -12504;
	UPDATE VOC_COLUMN SET SORTTYPE = 3, WIDTH = 120 WHERE ID = -12501;
	UPDATE VOC_COLUMN SET WIDTH = 90 WHERE ID = -12212;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12209;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12208;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12207;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12206;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12205;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12204;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12114;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -12113;
	UPDATE VOC_COLUMN SET WIDTH = 110, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12112;
	UPDATE VOC_COLUMN SET WIDTH = 80, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12109;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12107;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12106;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12105;
	UPDATE VOC_COLUMN SET WIDTH = 90, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12104;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12103;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12102;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -12101;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayLocationId_sel' WHERE ID = -11705;  
	UPDATE VOC_COLUMN SET FILTERTYPE = 1 WHERE ID = -11704;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayItemNumber_sel' WHERE ID = -11703;  
	UPDATE VOC_COLUMN SET WIDTH = 120 WHERE ID = -11612;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11608;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11510;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11508;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11506;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11505;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11504;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11503;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11502;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayPickedWeightDecimal_sel' WHERE ID = -11411;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayPickLocationDescription_sel' WHERE ID = -11403;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11309;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11308;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayContainerRoute_sel', FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11305;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11303;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11302;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayChaseProfile_sel', FILTERTYPE = 2, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11204;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayNormalProfile_sel', FILTERTYPE = 2, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11203;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayRegionName' WHERE ID = -11202;
	UPDATE VOC_COLUMN SET WIDTH = 80 WHERE ID = -11201;
	UPDATE VOC_COLUMN SET WIDTH = 80 WHERE ID = -11005;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -11003;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayLocationId_sel' WHERE ID = -10946;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayTriggerReplenish_sel' WHERE ID = -10940;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayBaseItemOverride_sel' WHERE ID = -10933;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayPickRoute_sel' WHERE ID = -10920;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayVariableWeightItem_sel' WHERE ID = -10916;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayPickDetail_sel' WHERE ID = -10912;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayItemNumber_sel' WHERE ID = -10911;
	UPDATE VOC_COLUMN SET FIELD = N'location.description.direction', SORTTYPE = 8, DISPLAY_FUNCTION = NULL, PAINTER_FUNCTION = NULL, FILTERTYPE = 2, FILTER_AUTO_COMPLETE_DISABLE = 1  WHERE ID = -10904;
	UPDATE VOC_COLUMN SET SORTED = 1 WHERE ID = -10901;
	UPDATE VOC_COLUMN SET WIDTH = 80 WHERE ID = -10827;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayGroupNumber_sel' WHERE ID = -10816;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAssignmentPickDetail_sel' WHERE ID = -10812;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayContainerCount_sel' WHERE ID = -10810;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayAssignmentRoute_sel' WHERE ID = -10808;
	UPDATE VOC_COLUMN SET FIELD = N'location.description.direction', SORTTYPE = 8, DISPLAY_FUNCTION = NULL, PAINTER_FUNCTION = NULL, FILTERTYPE = 2, FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -10736;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -10733;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -10732;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayVariableWeightItem_sel' WHERE ID = -10717;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayItemNumber_sel' WHERE ID = -10711;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayLocationId_sel' WHERE ID = -10708;
	UPDATE VOC_COLUMN SET FILTER_AUTO_COMPLETE_DISABLE = 1 WHERE ID = -10514;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -10512;
	UPDATE VOC_COLUMN SET WIDTH = 110 WHERE ID = -10511;
	UPDATE VOC_COLUMN SET WIDTH = 180 WHERE ID = -10413;
	UPDATE VOC_COLUMN SET WIDTH = 120 WHERE ID = -10412;
	UPDATE VOC_COLUMN SET SORTTYPE = 8, FILTERTYPE = 2 WHERE ID = -10407;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayNotificationSite' WHERE ID = -509;
	UPDATE VOC_COLUMN SET SORTED = 1 WHERE ID = -505;
	UPDATE VOC_COLUMN SET SORTED = 0 WHERE ID = -503;
	UPDATE VOC_COLUMN SET DISPLAY_FUNCTION = N'formatTimeWithTimeZone' WHERE ID = -305;
	UPDATE VOC_COLUMN SET DISPLAY_FUNCTION = N'formatTimeWithTimeZone' WHERE ID = -304;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayScheduleStatus' WHERE ID = -303;
	UPDATE VOC_COLUMN SET PAINTER_FUNCTION = N'displayScheduleName' WHERE ID = -301;
	UPDATE VOC_COLUMN SET DATA_TYPE_ID = -2 WHERE ID = -3;
	GO
	
	--
	-- Inserting data into table VOC_FEATURE
	--
	
	IF (SELECT count(*) FROM voc_feature WHERE id = -1412) = 0
		BEGIN
			--
			-- Updating data of table VOC_FEATURE
			--
			PRINT N'Updating voc_feature'		
			UPDATE [dbo].[voc_feature] SET [read_only]=1 WHERE [id]=-1132 
			UPDATE [dbo].[voc_feature] SET [name]=N'feature.voicelink.deliveryLocationMapping.changeMappingField', [group_id]=-1005 WHERE [id]=-1110 
			UPDATE [dbo].[voc_feature] SET [name]=N'feature.voicelink.deliveryLocationMapping.modifyDeliveryLocation', [group_id]=-1005 WHERE [id]=-1109 
			UPDATE [dbo].[voc_feature] SET [name]=N'feature.voicelink.deliveryLocationMapping.delete', [group_id]=-1005 WHERE [id]=-1108 
			UPDATE [dbo].[voc_feature] SET [name]=N'feature.voicelink.deliveryLocationMapping.create', [group_id]=-1005 WHERE [id]=-1107 
			UPDATE [dbo].[voc_feature] SET [name]=N'feature.voicelink.deliveryLocationMapping.view', [group_id]=-1005 WHERE [id]=-1106 
			UPDATE [dbo].[voc_feature] SET [name]=N'feature.voicelink.itemLocationMapping.view', [group_id]=-1005 WHERE [id]=-1020 
			UPDATE [dbo].[voc_feature] SET [name]=N'feature.voicelink.itemLocationMapping.create', [group_id]=-1005 WHERE [id]=-1019 
			UPDATE [dbo].[voc_feature] SET [name]=N'feature.voicelink.itemLocationMapping.delete', [group_id]=-1005 WHERE [id]=-1018 
			UPDATE [dbo].[voc_feature] SET [name]=N'feature.voicelink.itemLocationMapping.markAsReplenished', [group_id]=-1005 WHERE [id]=-1017 
			PRINT N'Dropping constraing Updating FK17DCDF26241ED7C2...'
			ALTER TABLE [dbo].[voc_role_to_feature] DROP CONSTRAINT [FK17DCDF26241ED7C2] 
			PRINT N'Dropping constraing Updating FK17DCDF264DCDD932...'
			ALTER TABLE [dbo].[voc_role_to_feature] DROP CONSTRAINT [FK17DCDF264DCDD932] 
			PRINT N'Dropping constraing Updating FK35EB80819FA773C...'
			ALTER TABLE [dbo].[voc_feature] DROP CONSTRAINT [FK35EB80819FA773C] 	
			PRINT N'Dropping constraing Updating FKBA3C05486A944441...'
			ALTER TABLE [dbo].[voc_homepage_summaries] DROP CONSTRAINT [FKBA3C05486A944441] 
			PRINT N'Dropping constraing FK789FEEA5E5EF59C2...'
			ALTER TABLE [dbo].[voc_homepage_access_summaries] DROP CONSTRAINT [FK789FEEA5E5EF59C2] 
			PRINT N'Inserting new rows into voc_feature'
			SET IDENTITY_INSERT [dbo].[voc_feature] ON; 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1412, N'feature.cyclecounting.currentwork.summary', NULL, 1, -3) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1411, N'feature.cyclecounting.assignment.summary', NULL, 1, -3) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1410, N'feature.cyclecounting.labor.view', NULL, 1, -1008) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1409, N'feature.cyclecounting.labor.editEndTime', NULL, 0, -1008) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1408, N'feature.cyclecounting.assignment.changeOperator', NULL, 0, -1008) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1407, N'feature.cyclecounting.assignment.modifyStatus', NULL, 0, -1008) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1406, N'feature.cyclecounting.assignment.edit', NULL, 0, -1008) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1405, N'feature.cyclecounting.assignment.view', NULL, 1, -1008) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1404, N'feature.cyclecounting.region.view', NULL, 1, -1008) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1403, N'feature.cyclecounting.region.delete', NULL, 0, -1008) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1402, N'feature.cyclecounting.region.edit', NULL, 0, -1008) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1401, N'feature.cyclecounting.region.create', NULL, 0, -1008) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1400, N'feature.cyclecounting.home', NULL, 1, -1008) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1314, N'feature.loading.currentwork.summary', NULL, 1, -3) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1313, N'feature.loading.route.summary', NULL, 1, -3) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1312, N'feature.loading.container.edit', NULL, 0, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1311, N'feature.loading.labor.view', NULL, 1, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1310, N'feature.loading.container.view', NULL, 1, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1309, N'feature.loading.stop.edit', NULL, 0, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1308, N'feature.loading.stop.view', NULL, 1, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1307, N'feature.loading.loadDiagram.print', NULL, 1, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1306, N'feature.loading.route.edit', NULL, 0, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1305, N'feature.loading.route.view', NULL, 1, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1304, N'feature.loading.region.delete', NULL, 0, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1303, N'feature.loading.region.edit', NULL, 0, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1302, N'feature.loading.region.create', NULL, 0, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1301, N'feature.loading.region.view', NULL, 1, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1300, N'feature.loading.home', NULL, 1, -1007) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1098, N'feature.voicelink.unitofmeasure.view', NULL, 1, -1005) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1097, N'feature.voicelink.unitofmeasure.delete', NULL, 0, -1005) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1096, N'feature.voicelink.unitofmeasure.edit', NULL, 0, -1005) 
			INSERT INTO [dbo].[voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1095, N'feature.voicelink.unitofmeasure.create', NULL, 0, -1005) 
			SET IDENTITY_INSERT [dbo].[voc_feature] OFF; 
			--
			-- Inserting data into table VOC_FEATURE_GROUP
			--
			SET IDENTITY_INSERT [dbo].[voc_feature_group] ON; 
			INSERT INTO [dbo].[voc_feature_group] ([id], [name], [description]) VALUES (-1008, N'featureGroup.voicelink.cyclecounting.name', N'featureGroup.voicelink.cyclecounting.description') 
			INSERT INTO [dbo].[voc_feature_group] ([id], [name], [description]) VALUES (-1007, N'featureGroup.voicelink.loading.name', N'featureGroup.voicelink.loading.description') 
			SET IDENTITY_INSERT [dbo].[voc_feature_group] OFF; 
		
			--
			-- Inserting data into table VOC_HOMEPAGE
			--
			SET IDENTITY_INSERT [dbo].[voc_homepage] ON; 
			INSERT INTO [dbo].[voc_homepage] ([id], [homepage_type], [homepageId], [user_id]) VALUES (-9, N'com.vocollect.epp.model.Homepage', -9, NULL) 
			INSERT INTO [dbo].[voc_homepage] ([id], [homepage_type], [homepageId], [user_id]) VALUES (-8, N'com.vocollect.epp.model.Homepage', -8, NULL) 
			SET IDENTITY_INSERT [dbo].[voc_homepage] OFF; 
			--
			-- add index to voc_homepage
			--
			PRINT N'creating UQ__voc_home__3351ECE078D67087]'
			ALTER TABLE [dbo].[voc_homepage] ADD UNIQUE NONCLUSTERED ([homepageId] ASC, [user_id] ASC) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			
			--
			-- Inserting data into table VOC_HOMEPAGE_ACCESS_SUMMARIES
			--
			Print N'Inserting New feature information into voc_homepage_access_summaries....'	
			INSERT INTO [dbo].[voc_homepage_access_summaries] ([id], [summary_id]) VALUES (-9, -22) 
			INSERT INTO [dbo].[voc_homepage_access_summaries] ([id], [summary_id]) VALUES (-9, -21) 
			INSERT INTO [dbo].[voc_homepage_access_summaries] ([id], [summary_id]) VALUES (-9, -20) 
			INSERT INTO [dbo].[voc_homepage_access_summaries] ([id], [summary_id]) VALUES (-8, -26) 
			INSERT INTO [dbo].[voc_homepage_access_summaries] ([id], [summary_id]) VALUES (-8, -25) 
			INSERT INTO [dbo].[voc_homepage_access_summaries] ([id], [summary_id]) VALUES (-8, -24) 
			INSERT INTO [dbo].[voc_homepage_access_summaries] ([id], [summary_id]) VALUES (-1, -26) 
			INSERT INTO [dbo].[voc_homepage_access_summaries] ([id], [summary_id]) VALUES (-1, -25) 
			INSERT INTO [dbo].[voc_homepage_access_summaries] ([id], [summary_id]) VALUES (-1, -24) 
			INSERT INTO [dbo].[voc_homepage_access_summaries] ([id], [summary_id]) VALUES (-1, -22) 
			INSERT INTO [dbo].[voc_homepage_access_summaries] ([id], [summary_id]) VALUES (-1, -21) 
			INSERT INTO [dbo].[voc_homepage_access_summaries] ([id], [summary_id]) VALUES (-1, -20) 
			
			--
			-- Inserting data into table VOC_HOMEPAGE_SUMMARIES
			--
			Print N'Inserting New feature information into voc_homepage_summaries....'			
			INSERT INTO [dbo].[voc_homepage_summaries] ([id], [location], [summaryId]) VALUES (-9, N'11', -20) 
			INSERT INTO [dbo].[voc_homepage_summaries] ([id], [location], [summaryId]) VALUES (-9, N'12', -20) 
			INSERT INTO [dbo].[voc_homepage_summaries] ([id], [location], [summaryId]) VALUES (-9, N'21', -21) 
			INSERT INTO [dbo].[voc_homepage_summaries] ([id], [location], [summaryId]) VALUES (-9, N'22', -21) 
			INSERT INTO [dbo].[voc_homepage_summaries] ([id], [location], [summaryId]) VALUES (-9, N'31', -22) 
			INSERT INTO [dbo].[voc_homepage_summaries] ([id], [location], [summaryId]) VALUES (-9, N'32', -22) 
			INSERT INTO [dbo].[voc_homepage_summaries] ([id], [location], [summaryId]) VALUES (-8, N'11', -24) 
			INSERT INTO [dbo].[voc_homepage_summaries] ([id], [location], [summaryId]) VALUES (-8, N'12', -24) 
			INSERT INTO [dbo].[voc_homepage_summaries] ([id], [location], [summaryId]) VALUES (-8, N'21', -25) 
			INSERT INTO [dbo].[voc_homepage_summaries] ([id], [location], [summaryId]) VALUES (-8, N'22', -25) 
			INSERT INTO [dbo].[voc_homepage_summaries] ([id], [location], [summaryId]) VALUES (-8, N'31', -26) 
			INSERT INTO [dbo].[voc_homepage_summaries] ([id], [location], [summaryId]) VALUES (-8, N'32', -26) 
			
			--
			-- Inserting data into table VOC_ROLE_TO_FEATURE
			--
			Print N'Inserting New feature information into voc_role_to_feature....'
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1412) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1411) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1410) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1405) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1404) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1400) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1314) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1313) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1311) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1310) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1308) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1307) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1305) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1301) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1300) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1215) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1091) 
			INSERT INTO [dbo].[voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1070) 
			--
			-- Inserting data into table VOC_SUMMARIES
			--
			Print N'Inserting New feature information into voc_summaries....'
			SET IDENTITY_INSERT [dbo].[voc_summaries] ON; 
			INSERT INTO [dbo].[voc_summaries] ([id], [summary_type], [title], [sourceFile], [refreshRate], [actionUrl], [actionMethod], [viewId], [tableId]) 
			VALUES (-26, N'tabularSummary', N'loadingLaborSummary.view.title', N'', 5, N'/loading/labor', N'getSummaryData', -1223, N'loadingLaborSummary') 
			INSERT INTO [dbo].[voc_summaries] ([id], [summary_type], [title], [sourceFile], [refreshRate], [actionUrl], [actionMethod], [viewId], [tableId]) 
			VALUES (-25, N'tabularSummary', N'loadingCurrentWorkSummary.view.title', N'', 5, N'/loading/workgroup', N'getSummaryData', -1222, N'loadingCurrentWorkSummary') 
			INSERT INTO [dbo].[voc_summaries] ([id], [summary_type], [title], [sourceFile], [refreshRate], [actionUrl], [actionMethod], [viewId], [tableId])
			VALUES (-24, N'tabularSummary', N'loadingRouteSummary.view.title', N'', 5, N'/loading/route', N'getSummaryData', -1221, N'loadingRouteSummary') 
			INSERT INTO [dbo].[voc_summaries] ([id], [summary_type], [title], [sourceFile], [refreshRate], [actionUrl], [actionMethod], [viewId], [tableId])
			VALUES (-23, N'tabularSummary', N'ccCurrentWorkSummary.view.title', N'', 5, N'/puttostore/workgroup', N'getSummaryData', -1113, N'cyclecountingExtraSummary') 
			INSERT INTO [dbo].[voc_summaries] ([id], [summary_type], [title], [sourceFile], [refreshRate], [actionUrl], [actionMethod], [viewId], [tableId])
			VALUES (-22, N'tabularSummary', N'ccCurrentWorkSummary.view.title', N'', 5, N'/cyclecounting/workgroup', N'getSummaryData', -1213, N'cyclecountingCurrentWorkSummary') 
			INSERT INTO [dbo].[voc_summaries] ([id], [summary_type], [title], [sourceFile], [refreshRate], [actionUrl], [actionMethod], [viewId], [tableId])
			VALUES (-21, N'tabularSummary', N'ccLaborSummary.view.title', N'', 5, N'/cyclecounting/labor', N'getSummaryData', -1214, N'cyclecountingLaborSummary') 
			INSERT INTO [dbo].[voc_summaries] ([id], [summary_type], [title], [sourceFile], [refreshRate], [actionUrl], [actionMethod], [viewId], [tableId])
			VALUES (-20, N'tabularSummary', N'ccAssignmentSummary.view.title', N'', 5, N'/cyclecounting/assignment', N'getSummaryData', -1212, N'cyclecountingAssignmentSummary') 
			SET IDENTITY_INSERT [dbo].[voc_summaries] OFF; 
		
			Print N'Inserting New feature information into voc_system_properties....'
			SET IDENTITY_INSERT [dbo].[voc_system_properties] ON; 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1177, N'LoadingRoute_Suspended_Purge', N'90') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1176, N'LoadingRoute_UnAvailable_Purge', N'90') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1175, N'LoadingRoute_Complete_Archive', N'90') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1174, N'LoadingRoute_Complete_Purge', N'3') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1173, N'LoadingRoute_InProgress_Purge', N'90') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1172, N'LoadingRoute_Reserved_Purge', N'90') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1171, N'LoadingRoute_Available_Purge', N'90') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1168, N'CycleCountingAssignment_Skipped_Purge', N'90') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1167, N'CycleCountingAssignment_Canceled_Archive', N'90') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1166, N'CycleCountingAssignment_Canceled_Purge', N'3') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1165, N'CycleCountingAssignment_Unavailable_Purge', N'90') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1164, N'CycleCountingAssignment_Complete_Archive', N'90') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1163, N'CycleCountingAssignment_Complete_Purge', N'3') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1162, N'CycleCountingAssignment_InProgress_Purge', N'90') 
			INSERT INTO [dbo].[voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1161, N'CycleCountingAssignment_Available_Purge', N'90') 
			SET IDENTITY_INSERT [dbo].[voc_system_properties] OFF; 
			
			--
			-- Inserting data into table VOC_VIEW
			--
			Print N'Inserting New feature information into voc_view....'
			SET IDENTITY_INSERT [dbo].[voc_view] ON; 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1223, N'loadingLaborSummary', N'com.vocollect.voicelink.core.model.OperatorLaborByRegion', N'operatorLaborManager', N'listLaborByRegionAndFilterType') 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1222, N'loadingCurrentWorkSummary', N'com.vocollect.voicelink.loading.model.LoadingSummary', N'loadingRegionManager', N'listCurrentWorkSummary') 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1221, N'loadingRouteSummary', N'com.vocollect.voicelink.loading.model.LoadingSummary', N'loadingRegionManager', N'listLoadingRouteSummary') 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1220, N'loadingRouteLabor', N'com.vocollect.voicelink.loading.model.LoadingRouteLabor', N'loadingLaborManager', NULL) 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1219, N'loadingContainers', N'com.vocollect.voicelink.loading.model.LoadingContainer', N'loadingContainerManager', NULL) 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1218, N'loadingStops', N'com.vocollect.voicelink.loading.model.LoadingStop', N'loadingStopManager', NULL) 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1217, N'loadingRoutes', N'com.vocollect.voicelink.loading.model.LoadingRoute', N'loadingRouteManager', NULL) 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1216, N'loadingRegions', N'com.vocollect.voicelink.loading.model.LoadingRegion', NULL, NULL) 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1214, N'cycleCountingLaborSummary', N'com.vocollect.voicelink.core.model.OperatorLaborByRegion', N'operatorLaborManager', N'listLaborByRegionAndFilterType') 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1213, N'cycleCountingCurrentWorkSummary', N'com.vocollect.voicelink.cyclecounting.model.CycleCountingSummary', N'cycleCountingRegionManager', N'listCurrentWorkSummary') 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1212, N'cycleCountingAssignmentSummary', N'com.vocollect.voicelink.cyclecounting.model.CycleCountingSummary', N'cycleCountingRegionManager', N'listAssignmentSummary') 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1211, N'cycleCountingAssignmentLabor', N'com.vocollect.voicelink.cyclecounting.model.CycleCountingLabor', N'cycleCountingLaborManager', NULL) 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1210, N'cycleCountingDetails', N'com.vocollect.voicelink.cyclecounting.model.CycleCountingDetail', NULL, NULL) 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1209, N'cycleCountingAssignments', N'com.vocollect.voicelink.cyclecounting.model.CycleCountingAssignment', NULL, NULL) 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1208, N'cycleCountingRegion', N'com.vocollect.voicelink.cyclecounting.model.CycleCountingRegion', NULL, NULL) 
			INSERT INTO [dbo].[voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1050, N'unitofmeasure', N'com.vocollect.voicelink.core.model.UnitOfMeasure', NULL, NULL) 
			SET IDENTITY_INSERT [dbo].[voc_view] OFF; 
			
			Print N'Adding Constraint FK17DCDF26241ED7C2....'
			ALTER TABLE [dbo].[voc_role_to_feature] ADD CONSTRAINT [FK17DCDF26241ED7C2] FOREIGN KEY ([feature_id]) REFERENCES [dbo].[voc_feature] ([id]) 
			Print N'Adding Constraint FK17DCDF264DCDD932....'
			ALTER TABLE [dbo].[voc_role_to_feature] ADD CONSTRAINT [FK17DCDF264DCDD932] FOREIGN KEY ([role_id]) REFERENCES [dbo].[voc_role] ([id]) 
			Print N'Adding Constraint FK35EB80819FA773C....'
			ALTER TABLE [dbo].[voc_feature] ADD CONSTRAINT [FK35EB80819FA773C] FOREIGN KEY ([group_id]) REFERENCES [dbo].[voc_feature_group] ([id]) 
			Print N'Adding Constraint FKBA3C05486A944441....'
			ALTER TABLE [dbo].[voc_homepage_summaries] ADD CONSTRAINT [FKBA3C05486A944441] FOREIGN KEY ([id]) REFERENCES [dbo].[voc_homepage] ([id]) 
			PRINT N'Creating FK789FEEA5E5EF59C2...';
			ALTER TABLE [dbo].[voc_homepage_access_summaries] ADD CONSTRAINT [FK789FEEA5E5EF59C2] FOREIGN KEY ([summary_id]) REFERENCES [dbo].[voc_summaries] ([id]) 
			PRINT N'Creating FK6049706B16D0F6D2...';
			ALTER TABLE [dbo].[voc_column] ADD CONSTRAINT [FK6049706B16D0F6D2] FOREIGN KEY ([view_id]) REFERENCES [dbo].[voc_view] ([id]) 

		END
	ELSE
		BEGIN
			Print N'New features already added'
		END
	GO
	
	
	--
	-- Inserting data into table VOC_TIMEWINDOW
	--
	IF (SELECT count(*) FROM voc_timewindow WHERE id= -23) = 0
		BEGIN
			Print N'Dropping Constraint FK2DB62B9216D0F6D2....'
			ALTER TABLE [dbo].[voc_timewindow] DROP CONSTRAINT [FK2DB62B9216D0F6D2] 
			Print N'Dropping Constraint FK2DB62B92F2F89D12....'
			ALTER TABLE [dbo].[voc_timewindow] DROP CONSTRAINT [FK2DB62B92F2F89D12] 
			PRINT N'Updating voc_timewindow'	
		
			SET IDENTITY_INSERT [dbo].[voc_timewindow] ON; 
			INSERT INTO [dbo].[voc_timewindow] ([id], [timewindow_type], [view_id], [value], [user_id]) VALUES (-23, N'com.vocollect.epp.model.TimeWindow', -1223, 1, NULL) 
			INSERT INTO [dbo].[voc_timewindow] ([id], [timewindow_type], [view_id], [value], [user_id]) VALUES (-22, N'com.vocollect.epp.model.TimeWindow', -1222, 1, NULL) 
			INSERT INTO [dbo].[voc_timewindow] ([id], [timewindow_type], [view_id], [value], [user_id]) VALUES (-21, N'com.vocollect.epp.model.TimeWindow', -1221, 1, NULL) 
			INSERT INTO [dbo].[voc_timewindow] ([id], [timewindow_type], [view_id], [value], [user_id]) VALUES (-20, N'com.vocollect.epp.model.TimeWindow', -1214, 1, NULL) 
			INSERT INTO [dbo].[voc_timewindow] ([id], [timewindow_type], [view_id], [value], [user_id]) VALUES (-19, N'com.vocollect.epp.model.TimeWindow', -1213, 1, NULL) 
			INSERT INTO [dbo].[voc_timewindow] ([id], [timewindow_type], [view_id], [value], [user_id]) VALUES (-18, N'com.vocollect.epp.model.TimeWindow', -1212, 1, NULL) 
			INSERT INTO [dbo].[voc_timewindow] ([id], [timewindow_type], [view_id], [value], [user_id]) VALUES (-17, N'com.vocollect.epp.model.TimeWindow', -1211, 1, NULL) 
			SET IDENTITY_INSERT [dbo].[voc_timewindow] OFF; 
			PRINT N'Creating FK2DB62B9216D0F6D2...';
			ALTER TABLE [dbo].[voc_timewindow] ADD CONSTRAINT [FK2DB62B9216D0F6D2] FOREIGN KEY ([view_id]) REFERENCES [dbo].[voc_view] ([id]) 
			PRINT N'Creating FK2DB62B92F2F89D12...';
			ALTER TABLE [dbo].[voc_timewindow] ADD CONSTRAINT [FK2DB62B92F2F89D12] FOREIGN KEY ([user_id]) REFERENCES [dbo].[voc_user] ([id]) 
		END	
	ELSE
		BEGIN
			PRINT N'new voc_timewindow(s) already added'	
		END
	GO
	
    -- *****************************************************************************************
	-- Updating data of table VOC_REPORTTYPE_PARAMETER
    -- *****************************************************************************************
	Print N'Updating VOC_REPORTTYPE_PARAMETER'
	UPDATE VOC_REPORTTYPE_PARAMETER SET DESCRIPTION = N'selection.report.assignment.localizedRoute' WHERE ID = -205;
	GO
    -- *****************************************************************************************
	-- Updating data in table VOC_SYSTEM_PROPERTIES
	-- This will need to be added to all future upgrade scripts
	-- ****************************************************************************************
	Print N'Updating voc_system_proerties so import_setup_xml will be forced to rebuild...'
	UPDATE [dbo].[voc_system_properties] set [value] = 1 WHERE [systemPropertyId] = -1005;
	GO
    -- *****************************************************************************************
	-- Updating data of table VOC_VIEW
    -- *****************************************************************************************
	UPDATE [dbo].[voc_view] SET [class_name]=N'com.vocollect.voicelink.core.model.DeliveryLocationMapping' WHERE [id]=-1027 
	UPDATE [dbo].[voc_view] SET [class_name]=N'com.vocollect.voicelink.core.model.DeliveryLocationMapping' WHERE [id]=-1025 
	GO
    -- *****************************************************************************************
	-- Update Report scheduler job executing class 
    -- *****************************************************************************************
	UPDATE [dbo].[voc_jobs] SET [job_class]=N'com.vocollect.voicelink.scheduling.VoiceLinkReportJobManagerImpl' WHERE [name]=N'reportSchedulerJobDetail';
	GO

	--
	-- Update module versions
	--	
	IF (SELECT count(*) FROM voc_plugin_modules WHERE moduleVersion = '4.1') = 0
		BEGIN
			-- Update module versions
			-- To update the version, change the SET @vlVersion... line below
			-- This needs to be done every time there is a new application added to VoiceLink
			SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
			SET XACT_ABORT ON;
			BEGIN TRANSACTION;
				DECLARE @vlVersion varchar(32);
				DECLARE @vipVersion varchar(32);
				SET @vlVersion = '4.1';
				SET @vipVersion = '2.0';
				DECLARE @newId bigint;
				DECLARE @oldId bigint;
				SET @oldId = (select moduleId from voc_plugin_modules where name = 'plugin.module.voicelink' AND isEnabled = 1);
				Print N'Updating plugin modules...'
				UPDATE voc_plugin_modules set isEnabled = 0 where name = 'plugin.module.voicelink';
				Print N'Inserting new plugin module'
				INSERT into voc_plugin_modules (name, moduleVersion, frameworkVersion, sequenceNumber, uri, navMenuName, isEnabled, createdDate) 
				VALUES ('plugin.module.voicelink', @vlVersion, @vipVersion, 101, '/selection/home.action', 'voicelink', 1, convert(varchar(10),getdate(),101));
				SET @newId = (select moduleId from voc_plugin_modules where name = 'plugin.module.voicelink' AND isEnabled = 1);
				UPDATE voc_plugin_components set moduleId = @newId where moduleId = @oldId;
				UPDATE voc_plugin_modules_auth set moduleId = @newId where moduleId = @oldId;
				--Insert new components here
				Print N'Inserting new modules'
				INSERT INTO [dbo].[voc_plugin_modules_auth] ([moduleId], [authName]) VALUES (@newId, N'featureGroup.voicelink.cyclecounting.name')
				INSERT INTO [dbo].[voc_plugin_modules_auth] ([moduleId], [authName]) VALUES (@newId, N'featureGroup.voicelink.loading.name')
				SET IDENTITY_INSERT voc_plugin_components ON;
				INSERT INTO [dbo].[voc_plugin_components] ([componentId], [name], [sequenceNumber], [navMenuName], [uri], [isEnabled], [moduleId], [parentComponentId]) 
				VALUES (106, N'plugin.component.voicelink.loading', 160, N'loading', N'/loading/home.action', 1, @newId, NULL) 
				INSERT INTO [dbo].[voc_plugin_components] ([componentId], [name], [sequenceNumber], [navMenuName], [uri], [isEnabled], [moduleId], [parentComponentId]) 
				VALUES (107, N'plugin.component.voicelink.cyclecounting', 170, N'cyclecounting', N'/cyclecounting/home.action', 1, @newId, NULL) 
				SET IDENTITY_INSERT voc_plugin_components OFF;
				Print N'Inserting new plugin components module'
				INSERT INTO [dbo].[voc_plugin_components_auth] ([componentId], [authName]) VALUES (106, N'featureGroup.voicelink.loading.name') 
				INSERT INTO [dbo].[voc_plugin_components_auth] ([componentId], [authName]) VALUES (107, N'featureGroup.voicelink.cyclecounting.name') 
			
				UPDATE voc_plugin_modules 
					SET     moduleVersion = @vipVersion 
					WHERE   moduleId      = 1;
				UPDATE voc_plugin_modules 
					SET     frameworkVersion = @vipVersion 
					WHERE   moduleId         = 1;
				UPDATE voc_plugin_modules 
					SET     moduleVersion = @vipVersion 
					WHERE   moduleId      = 2;
				UPDATE voc_plugin_modules 
					SET     frameworkVersion = @vipVersion 
					WHERE   moduleId         = 2;
			COMMIT TRANSACTION
			
			PRINT N'Creating FK_arch_CCAssignment_CCDetail...';
			ALTER TABLE [arch_cc_assignment_details] WITH NOCHECK ADD CONSTRAINT [FK_arch_CCAssignment_CCDetail] FOREIGN KEY ([cycleCountId]) REFERENCES [arch_cc_assignments] ([cycleCountId]) ON DELETE CASCADE ON UPDATE NO ACTION;
			PRINT N'Creating FK_arch_CCAssign_AssignLabors...';
			ALTER TABLE [arch_cc_labor] WITH NOCHECK ADD CONSTRAINT [FK_arch_CCAssign_AssignLabors] FOREIGN KEY ([assignmentId]) REFERENCES [arch_cc_assignments] ([cycleCountId]) ON DELETE CASCADE ON UPDATE NO ACTION;
			PRINT N'Creating FK_Arch_Load_Stop_Cont...';
			ALTER TABLE [arch_loading_containers] WITH NOCHECK ADD CONSTRAINT [FK_Arch_Load_Stop_Cont] FOREIGN KEY ([stopId]) REFERENCES [arch_loading_stops] ([stopId]) ON DELETE CASCADE ON UPDATE NO ACTION;
			PRINT N'Creating FK_Arch_Load_Route_Labor...';
			ALTER TABLE [arch_loading_labor] WITH NOCHECK ADD CONSTRAINT [FK_Arch_Load_Route_Labor] FOREIGN KEY ([routeId]) REFERENCES [arch_loading_routes] ([routeId]) ON DELETE CASCADE ON UPDATE NO ACTION;
			PRINT N'Creating FK_Arch_Load_Route_Load_Stop...';
			ALTER TABLE [arch_loading_stops] WITH NOCHECK ADD CONSTRAINT [FK_Arch_Load_Route_Load_Stop] FOREIGN KEY ([routeId]) REFERENCES [arch_loading_routes] ([routeId]) ON DELETE CASCADE ON UPDATE NO ACTION;
			PRINT N'Creating FKE0F45CE73EF30942...';
			ALTER TABLE [cc_assignment_tag] WITH NOCHECK ADD CONSTRAINT [FKE0F45CE73EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FKE0F45CE74518072E...';
			ALTER TABLE [cc_assignment_tag] WITH NOCHECK ADD CONSTRAINT [FKE0F45CE74518072E] FOREIGN KEY ([tagged_id]) REFERENCES [cc_assignments] ([cycleCountId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_Region_ccAssignment...';
			ALTER TABLE [cc_assignments] WITH NOCHECK ADD CONSTRAINT [FK_Region_ccAssignment] FOREIGN KEY ([regionId]) REFERENCES [cc_regions] ([regionId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_Operator_ccAssignment...';
			ALTER TABLE [cc_assignments] WITH NOCHECK ADD CONSTRAINT [FK_Operator_ccAssignment] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_Location_ccAssignment...';
			ALTER TABLE [cc_assignments] WITH NOCHECK ADD CONSTRAINT [FK_Location_ccAssignment] FOREIGN KEY ([locationId]) REFERENCES [core_locations] ([locationId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_Assignment_Detail_Import...';
			ALTER TABLE [cc_detail_import_data] WITH NOCHECK ADD CONSTRAINT [FK_Assignment_Detail_Import] FOREIGN KEY ([cycleCountId]) REFERENCES [cc_assignment_import_data] ([importID]) ON DELETE CASCADE ON UPDATE NO ACTION;
			PRINT N'Creating FK_Item_CCDetail...';
			ALTER TABLE [cc_details] WITH NOCHECK ADD CONSTRAINT [FK_Item_CCDetail] FOREIGN KEY ([itemId]) REFERENCES [core_items] ([itemId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_CCAssignment_CCDetail...';
			ALTER TABLE [cc_details] WITH NOCHECK ADD CONSTRAINT [FK_CCAssignment_CCDetail] FOREIGN KEY ([cycleCountId]) REFERENCES [cc_assignments] ([cycleCountId]) ON DELETE CASCADE ON UPDATE NO ACTION;
			PRINT N'Creating FK_ReasonCode_CycleCountDetail...';
			ALTER TABLE [cc_details] WITH NOCHECK ADD CONSTRAINT [FK_ReasonCode_CycleCountDetail] FOREIGN KEY ([reasonId]) REFERENCES [core_reason_codes] ([reasonCodeId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_CC_Assignment_AssignLabors...';
			ALTER TABLE [cc_labor] WITH NOCHECK ADD CONSTRAINT [FK_CC_Assignment_AssignLabors] FOREIGN KEY ([assignmentId]) REFERENCES [cc_assignments] ([cycleCountId]) ON DELETE CASCADE ON UPDATE NO ACTION;
			PRINT N'Creating FK_CC_OperLabor_AssignLabors...';
			ALTER TABLE [cc_labor] WITH NOCHECK ADD CONSTRAINT [FK_CC_OperLabor_AssignLabors] FOREIGN KEY ([operatorLaborId]) REFERENCES [core_operator_labor] ([operatorLaborId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_CC_Operator_AssignLabors...';
			ALTER TABLE [cc_labor] WITH NOCHECK ADD CONSTRAINT [FK_CC_Operator_AssignLabors] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK9C2A0B20630F1E6B...';
			ALTER TABLE [cc_regions] WITH NOCHECK ADD CONSTRAINT [FK9C2A0B20630F1E6B] FOREIGN KEY ([regionId]) REFERENCES [core_regions] ([regionId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FKB783CCF5FF09FF01...';
			ALTER TABLE [core_del_loc_map_setting_tag] WITH NOCHECK ADD CONSTRAINT [FKB783CCF5FF09FF01] FOREIGN KEY ([tagged_id]) REFERENCES [core_del_loc_mapping_settings] ([delLocMappingSettingId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FKB783CCF53EF30942...';
			ALTER TABLE [core_del_loc_map_setting_tag] WITH NOCHECK ADD CONSTRAINT [FKB783CCF53EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK1FF78EB3F419462B...';
			ALTER TABLE [core_del_loc_mappings_tag] WITH NOCHECK ADD CONSTRAINT [FK1FF78EB3F419462B] FOREIGN KEY ([tagged_id]) REFERENCES [core_del_loc_mappings] ([delLocMappingId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK1FF78EB33EF30942...';
			ALTER TABLE [core_del_loc_mappings_tag] WITH NOCHECK ADD CONSTRAINT [FK1FF78EB33EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK4DF039CE99A6FDC9...';
			ALTER TABLE [core_uom_tag] WITH NOCHECK ADD CONSTRAINT [FK4DF039CE99A6FDC9] FOREIGN KEY ([tagged_id]) REFERENCES [core_uom] ([uomId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK4DF039CE3EF30942...';
			ALTER TABLE [core_uom_tag] WITH NOCHECK ADD CONSTRAINT [FK4DF039CE3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FKE66FED19EF624936...';
			ALTER TABLE [loading_container_tag] WITH NOCHECK ADD CONSTRAINT [FKE66FED19EF624936] FOREIGN KEY ([tagged_id]) REFERENCES [loading_containers] ([containerId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FKE66FED193EF30942...';
			ALTER TABLE [loading_container_tag] WITH NOCHECK ADD CONSTRAINT [FKE66FED193EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_loading_container_oper...';
			ALTER TABLE [loading_containers] WITH NOCHECK ADD CONSTRAINT [FK_loading_container_oper] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_Loading_Stop_Containers...';
			ALTER TABLE [loading_containers] WITH NOCHECK ADD CONSTRAINT [FK_Loading_Stop_Containers] FOREIGN KEY ([stopId]) REFERENCES [loading_stops] ([stopId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FKB6F643FC630F1E6B...';
			ALTER TABLE [loading_regions] WITH NOCHECK ADD CONSTRAINT [FKB6F643FC630F1E6B] FOREIGN KEY ([regionId]) REFERENCES [core_regions] ([regionId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK16F750D6492F92D4...';
			ALTER TABLE [loading_route_operators] WITH NOCHECK ADD CONSTRAINT [FK16F750D6492F92D4] FOREIGN KEY ([routeID]) REFERENCES [loading_routes] ([routeId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_Loading_Operators_Route...';
			ALTER TABLE [loading_route_operators] WITH NOCHECK ADD CONSTRAINT [FK_Loading_Operators_Route] FOREIGN KEY ([operatorID]) REFERENCES [core_operators] ([operatorId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FKD809D561800FBA7E...';
			ALTER TABLE [loading_route_tag] WITH NOCHECK ADD CONSTRAINT [FKD809D561800FBA7E] FOREIGN KEY ([tagged_id]) REFERENCES [loading_routes] ([routeId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FKD809D5613EF30942...';
			ALTER TABLE [loading_route_tag] WITH NOCHECK ADD CONSTRAINT [FKD809D5613EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_Loading_DockDoor...';
			ALTER TABLE [loading_routes] WITH NOCHECK ADD CONSTRAINT [FK_Loading_DockDoor] FOREIGN KEY ([dockDoorLocationID]) REFERENCES [core_locations] ([locationId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_LoadingRegion_LoadingRoutes...';
			ALTER TABLE [loading_routes] WITH NOCHECK ADD CONSTRAINT [FK_LoadingRegion_LoadingRoutes] FOREIGN KEY ([regionId]) REFERENCES [loading_regions] ([regionId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_LoadRoute_LoadRouteLabors...';
			ALTER TABLE [loading_routes_labor] WITH NOCHECK ADD CONSTRAINT [FK_LoadRoute_LoadRouteLabors] FOREIGN KEY ([routeId]) REFERENCES [loading_routes] ([routeId]) ON DELETE CASCADE ON UPDATE NO ACTION;
			PRINT N'Creating FK_OperLabor_LoadRouteLabors...';
			ALTER TABLE [loading_routes_labor] WITH NOCHECK ADD CONSTRAINT [FK_OperLabor_LoadRouteLabors] FOREIGN KEY ([operatorLaborId]) REFERENCES [core_operator_labor] ([operatorLaborId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_Operator_LoadingRouteLabors...';
			ALTER TABLE [loading_routes_labor] WITH NOCHECK ADD CONSTRAINT [FK_Operator_LoadingRouteLabors] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_Route_Stop_Import...';
			ALTER TABLE [loading_stop_import_data] WITH NOCHECK ADD CONSTRAINT [FK_Route_Stop_Import] FOREIGN KEY ([routeId]) REFERENCES [loading_route_import_data] ([importID]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FKB7331980D37CE889...'; 
			ALTER TABLE [loading_stop_tag] WITH NOCHECK ADD CONSTRAINT [FKB7331980D37CE889] FOREIGN KEY ([tagged_id]) REFERENCES [loading_stops] ([stopId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FKB73319803EF30942...';
			ALTER TABLE [loading_stop_tag] WITH NOCHECK ADD CONSTRAINT [FKB73319803EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_Loading_Route_Stops...';
			ALTER TABLE [loading_stops] WITH NOCHECK ADD CONSTRAINT [FK_Loading_Route_Stops] FOREIGN KEY ([routeId]) REFERENCES [loading_routes] ([routeId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_Operator_Loading_Stops...';
			ALTER TABLE [loading_stops] WITH NOCHECK ADD CONSTRAINT [FK_Operator_Loading_Stops] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]) ON DELETE NO ACTION ON UPDATE NO ACTION;
			PRINT N'Creating FK_LoadingRegion_Picks...';
			ALTER TABLE [dbo].[sel_assignments] ADD CONSTRAINT [FK_LoadingRegion_Picks] FOREIGN KEY ([loadingRegionId]) REFERENCES [dbo].[loading_regions] ([regionId]) 
			PRINT N'Creating FK_Assignment_Picks_Import...';
		END
	ELSE
		BEGIN
			PRINT N'New modules and constraints already added'	
		END
	GO

		
	--
	--  Make sure we drop and recreate the views so the clustered upgrade is successful.F
	--
	print N'Dropping views'
	IF OBJECT_ID('dbo.vw_export_operatoractions') IS NOT NULL
		DROP VIEW dbo.vw_export_operatoractions
		GO

	IF OBJECT_ID('dbo.vw_import_assignments') IS NOT NULL
		DROP VIEW dbo.vw_import_assignments
		GO
		
	IF OBJECT_ID('dbo.vw_import_assignments_complete') IS NOT NULL
		DROP VIEW dbo.vw_import_assignments_complete
		GO
		
	IF OBJECT_ID('dbo.vw_import_picks') IS NOT NULL
		DROP VIEW dbo.vw_import_picks
		GO

	IF OBJECT_ID('dbo.vw_export_cc_complete') IS NOT NULL
		DROP VIEW dbo.vw_export_cc_complete
		GO

	IF OBJECT_ID('dbo.vw_export_cc_count') IS NOT NULL
		DROP VIEW dbo.vw_export_cc_count
		GO

	IF OBJECT_ID('dbo.vw_export_cc_labor') IS NOT NULL
		DROP VIEW dbo.vw_export_cc_labor
		GO

	IF OBJECT_ID('dbo.vw_export_load_complete') IS NOT NULL
		DROP VIEW dbo.vw_export_load_complete
		GO

	IF OBJECT_ID('dbo.vw_export_load_container') IS NOT NULL
		DROP VIEW dbo.vw_export_load_container
		GO

	IF OBJECT_ID('dbo.vw_export_load_route_labor') IS NOT NULL
		DROP VIEW dbo.vw_export_load_route_labor
		GO
			
	IF OBJECT_ID('dbo.vw_export_load_stop') IS NOT NULL
		DROP VIEW dbo.vw_export_load_stop
		GO

	IF OBJECT_ID('dbo.vw_import_cc_assignments') IS NOT NULL
		DROP VIEW dbo.vw_import_cc_assignments
		GO

	IF OBJECT_ID('dbo.vw_import_cc_assigns_complete') IS NOT NULL
		DROP VIEW dbo.vw_import_cc_assigns_complete
		GO

	IF OBJECT_ID('dbo.vw_import_cc_details') IS NOT NULL
		DROP VIEW dbo.vw_import_cc_details
		GO

	IF OBJECT_ID('dbo.vw_import_cc_details_complete') IS NOT NULL
		DROP VIEW dbo.vw_import_cc_details_complete
		GO

	IF OBJECT_ID('dbo.vw_import_load_cont_complete') IS NOT NULL
		DROP VIEW dbo.vw_import_load_cont_complete
		GO

	IF OBJECT_ID('dbo.vw_import_load_containers') IS NOT NULL
		DROP VIEW dbo.vw_import_load_containers
		GO

	IF OBJECT_ID('dbo.vw_import_load_route') IS NOT NULL
		DROP VIEW dbo.vw_import_load_route
		GO
			
	IF OBJECT_ID('dbo.vw_import_load_route_complete') IS NOT NULL
		DROP VIEW dbo.vw_import_load_route_complete
		GO


	IF OBJECT_ID('dbo.vw_import_load_stops') IS NOT NULL
		DROP VIEW dbo.vw_import_load_stops
		GO
