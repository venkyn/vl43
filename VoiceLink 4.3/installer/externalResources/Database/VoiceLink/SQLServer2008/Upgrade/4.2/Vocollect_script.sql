
--  ********************************************************************
--		CREATE VSC TABLES
--  ********************************************************************
	
	IF OBJECT_ID('dbo.core_vehicle_types') IS NULL
		BEGIN
			PRINT N'Creating [core_vehicle_types]...';
			CREATE TABLE [dbo].[core_vehicle_types](
			    [typeId] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
			    [version] [int] NOT NULL,
			    [createdDate] [datetime] NOT NULL,
			    [vehicleTypeNumber] [int] NOT NULL,
			    [description] [nvarchar](100) NOT NULL,
			    [isCaptureVehicleID] [tinyint] NOT NULL,
			PRIMARY KEY CLUSTERED 
			(
			    [typeId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
			
			CREATE NONCLUSTERED INDEX [IX_vehicle_type_no] ON [dbo].[core_vehicle_types]
			(
			    [vehicleTypeNumber] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
		END
	ELSE
		BEGIN
			print N'table - core_vehicle_types already exists...';
		END
	GO
	
	IF OBJECT_ID('dbo.core_vehicle_type_tag') IS NULL
		BEGIN	
			PRINT N'Creating [core_vehicle_type_tag]...';
			CREATE TABLE [dbo].[core_vehicle_type_tag](
			    [tagged_id] [numeric](19, 0) NOT NULL,
			    [tag_id] [numeric](19, 0) NOT NULL,
			PRIMARY KEY CLUSTERED 
			(
			    [tagged_id] ASC,
			    [tag_id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
			
			
			ALTER TABLE [dbo].[core_vehicle_type_tag]  WITH CHECK ADD  CONSTRAINT [FK6BB149683EF30942] FOREIGN KEY([tag_id])
			REFERENCES [dbo].[voc_tag] ([id])
			
			ALTER TABLE [dbo].[core_vehicle_type_tag] CHECK CONSTRAINT [FK6BB149683EF30942]
			
			ALTER TABLE [dbo].[core_vehicle_type_tag]  WITH CHECK ADD  CONSTRAINT [FK6BB149686C08E5EC] FOREIGN KEY([tagged_id])
			REFERENCES [dbo].[core_vehicle_types] ([typeId])
			
			ALTER TABLE [dbo].[core_vehicle_type_tag] CHECK CONSTRAINT [FK6BB149686C08E5EC]
		END
	ELSE
		BEGIN
			print N'table - core_vehicle_type_tag already exists...';
		END
	GO
	
	
	IF OBJECT_ID('dbo.core_vehicles') IS NULL
        BEGIN   
            PRINT N'Creating [core_vehicle_type_tag]...';
            CREATE TABLE [dbo].[core_vehicles](
			    [vehicleId] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
			    [version] [int] NOT NULL,
			    [createdDate] [datetime] NOT NULL,
			    [vehicleNumber] [nvarchar](255) NULL,
			    [spokenVehicleNumber] [nvarchar](5) NULL,
			    [vehicleCategory] [int] NOT NULL,
			    [typeId] [numeric](19, 0) NULL,
			PRIMARY KEY CLUSTERED 
			(
			    [vehicleId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
			
			
			ALTER TABLE [dbo].[core_vehicles]  WITH CHECK ADD  CONSTRAINT [FK_Vehicle_VehicleType] FOREIGN KEY([typeId])
			REFERENCES [dbo].[core_vehicle_types] ([typeId]) ON DELETE CASCADE
			
			ALTER TABLE [dbo].[core_vehicles] CHECK CONSTRAINT [FK_Vehicle_VehicleType]
        END
    ELSE
        BEGIN
            print N'table - core_vehicle_type_tag already exists...';
        END
    GO
    
    
    IF OBJECT_ID('dbo.core_vehicle_tag') IS NULL
        BEGIN   
            PRINT N'Creating [core_vehicle_tag]...';
            CREATE TABLE [dbo].[core_vehicle_tag](
			    [tagged_id] [numeric](19, 0) NOT NULL,
			    [tag_id] [numeric](19, 0) NOT NULL,
			PRIMARY KEY CLUSTERED 
			(
			    [tagged_id] ASC,
			    [tag_id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
			
			
			ALTER TABLE [dbo].[core_vehicle_tag]  WITH CHECK ADD  CONSTRAINT [FKC78A9F673EF30942] FOREIGN KEY([tag_id])
			REFERENCES [dbo].[voc_tag] ([id])
			
			ALTER TABLE [dbo].[core_vehicle_tag] CHECK CONSTRAINT [FKC78A9F673EF30942]
			
			ALTER TABLE [dbo].[core_vehicle_tag]  WITH CHECK ADD  CONSTRAINT [FKC78A9F6757846492] FOREIGN KEY([tagged_id])
			REFERENCES [dbo].[core_vehicles] ([vehicleId])
			
			ALTER TABLE [dbo].[core_vehicle_tag] CHECK CONSTRAINT [FKC78A9F6757846492]
        END
    ELSE
        BEGIN
            print N'table - core_vehicle_tag already exists...';
        END
    GO
    
    
    IF OBJECT_ID('dbo.core_vehicle_safety_checks') IS NULL
        BEGIN   
            PRINT N'Creating [core_vehicle_safety_checks]...';
            CREATE TABLE [dbo].[core_vehicle_safety_checks](
			    [checkId] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
			    [version] [int] NOT NULL,
			    [createdDate] [datetime] NOT NULL,
			    [sequenceNumber] [int] NOT NULL,
			    [description] [nvarchar](255) NULL,
			    [spokenDescription] [nvarchar](50) NOT NULL,
			    [typeId] [numeric](19, 0) NULL,
			    [responseType] [int] NOT NULL,
			PRIMARY KEY CLUSTERED 
			(
			    [checkId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
			
			
			ALTER TABLE [dbo].[core_vehicle_safety_checks]  WITH CHECK ADD  CONSTRAINT [FK_SafetyCheck_VehicleType] FOREIGN KEY([typeId])
			REFERENCES [dbo].[core_vehicle_types] ([typeId]) ON DELETE CASCADE
			
			ALTER TABLE [dbo].[core_vehicle_safety_checks] CHECK CONSTRAINT [FK_SafetyCheck_VehicleType]
        END
    ELSE
        BEGIN
            print N'table - core_vehicle_safety_checks already exists...';
        END
    GO
    
    
    IF OBJECT_ID('dbo.core_vehicle_safety_check_tag') IS NULL
        BEGIN   
            PRINT N'Creating [core_vehicle_safety_check_tag]...';
            CREATE TABLE [dbo].[core_vehicle_safety_check_tag](
			    [tagged_id] [numeric](19, 0) NOT NULL,
			    [tag_id] [numeric](19, 0) NOT NULL,
			PRIMARY KEY CLUSTERED 
			(
			    [tagged_id] ASC,
			    [tag_id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
			
			
			ALTER TABLE [dbo].[core_vehicle_safety_check_tag]  WITH CHECK ADD  CONSTRAINT [FKDB0D40093EF30942] FOREIGN KEY([tag_id])
			REFERENCES [dbo].[voc_tag] ([id])
			
			ALTER TABLE [dbo].[core_vehicle_safety_check_tag] CHECK CONSTRAINT [FKDB0D40093EF30942]
			
			ALTER TABLE [dbo].[core_vehicle_safety_check_tag]  WITH CHECK ADD  CONSTRAINT [FKDB0D4009627F3BC0] FOREIGN KEY([tagged_id])
			REFERENCES [dbo].[core_vehicle_safety_checks] ([checkId])
			
			ALTER TABLE [dbo].[core_vehicle_safety_check_tag] CHECK CONSTRAINT [FKDB0D4009627F3BC0]
        END
    ELSE
        BEGIN
            print N'table - core_vehicle_safety_check_tag already exists...';
        END
    GO
    
    IF OBJECT_ID('dbo.core_vehicle_check_responses') IS NULL
        BEGIN   
            PRINT N'Creating [core_vehicle_check_responses]...';
            CREATE TABLE [dbo].[core_vehicle_check_responses](
			    [responseId] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
				[version] [int] NOT NULL,
				[createdDate] [datetime] NOT NULL,
				[checkId] [numeric](19, 0) NOT NULL,
				[vehicleId] [numeric](19, 0) NOT NULL,
				[checkResponse] [int] NOT NULL,
				[repairAction] [int] NULL,
				[operatorId] [numeric](19, 0) NULL,
				[groupId] [numeric](19, 0) NULL,
				[note] [nvarchar](255) NULL,
			PRIMARY KEY CLUSTERED 
			(
				[responseId] ASC
			)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
			UNIQUE NONCLUSTERED 
			(
				[checkId] ASC,
				[vehicleId] ASC,
				[operatorId] ASC,
				[groupId] ASC
			)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
			) ON [PRIMARY]
			
			
			ALTER TABLE [dbo].[core_vehicle_check_responses]  WITH CHECK ADD  CONSTRAINT [FK_CheckResponse_Operator] FOREIGN KEY([operatorId])
			REFERENCES [dbo].[core_operators] ([operatorId])
			
			
			ALTER TABLE [dbo].[core_vehicle_check_responses] CHECK CONSTRAINT [FK_CheckResponse_Operator]
			
			
			ALTER TABLE [dbo].[core_vehicle_check_responses]  WITH CHECK ADD  CONSTRAINT [FK_CheckResponse_SafetyCheck] FOREIGN KEY([checkId])
			REFERENCES [dbo].[core_vehicle_safety_checks] ([checkId])
			
			
			ALTER TABLE [dbo].[core_vehicle_check_responses] CHECK CONSTRAINT [FK_CheckResponse_SafetyCheck]
			
			
			ALTER TABLE [dbo].[core_vehicle_check_responses]  WITH CHECK ADD  CONSTRAINT [FK_CheckResponse_Vehicle] FOREIGN KEY([vehicleId])
			REFERENCES [dbo].[core_vehicles] ([vehicleId])
			
			
			ALTER TABLE [dbo].[core_vehicle_check_responses] CHECK CONSTRAINT [FK_CheckResponse_Vehicle]
			
        END
    ELSE
        BEGIN
            print N'table - core_vehicle_check_responses already exists...';
        END
    GO   
    
    IF OBJECT_ID('dbo.core_vehicle_check_resp_tag') IS NULL
        BEGIN   
            PRINT N'Creating [core_vehicle_check_resp_tag]...';
            CREATE TABLE [dbo].[core_vehicle_check_resp_tag](
			    [tagged_id] [numeric](19, 0) NOT NULL,
				[tag_id] [numeric](19, 0) NOT NULL,
			PRIMARY KEY CLUSTERED 
			(
				[tagged_id] ASC,
				[tag_id] ASC
			)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
			) ON [PRIMARY]
			
			
			ALTER TABLE [dbo].[core_vehicle_check_resp_tag]  WITH CHECK ADD CONSTRAINT [FK831586153EF30942] FOREIGN KEY([tag_id])
			REFERENCES [dbo].[voc_tag] ([id])
			
			
			ALTER TABLE [dbo].[core_vehicle_check_resp_tag] CHECK CONSTRAINT [FK831586153EF30942]
			
			
			ALTER TABLE [dbo].[core_vehicle_check_resp_tag]  WITH CHECK ADD CONSTRAINT [FK831586155FDFA221] FOREIGN KEY([tagged_id])
			REFERENCES [dbo].[core_vehicle_check_responses] ([responseId])
			
			
			ALTER TABLE [dbo].[core_vehicle_check_resp_tag] CHECK CONSTRAINT [FK831586155FDFA221]
			
        END
    ELSE
        BEGIN
            print N'table - core_vehicle_check_resp_tag already exists...';
        END
    GO    
    
    IF OBJECT_ID('dbo.core_vehicle_operators') IS NULL
        BEGIN   
            PRINT N'Creating [core_vehicle_operators]...';
            CREATE TABLE [dbo].[core_vehicle_operators](
			    [vehicleId] [numeric](19, 0) NOT NULL,
			    [operatorID] [numeric](19, 0) NOT NULL,
				PRIMARY KEY CLUSTERED ([vehicleId] ASC, [operatorID] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
			);
			
			ALTER TABLE [dbo].[core_vehicle_operators]  WITH CHECK ADD CONSTRAINT [FK_Core_Operators_Vehicle] FOREIGN KEY([operatorID])
			REFERENCES [dbo].[core_operators] ([operatorId])
			
			
			ALTER TABLE [dbo].[core_vehicle_operators] CHECK CONSTRAINT [FK_Core_Operators_Vehicle]
			
			ALTER TABLE [dbo].[core_vehicle_operators]  WITH CHECK ADD CONSTRAINT [FK4E9DFE5C57C96D0B] FOREIGN KEY([vehicleId])
			REFERENCES [dbo].[core_vehicles] ([vehicleId])
			
			
			ALTER TABLE [dbo].[core_vehicle_operators] CHECK CONSTRAINT [FK4E9DFE5C57C96D0B]
		END
	ELSE
		BEGIN
			print N'table - core_vehicle_operators already exists...';
		END			
	GO	
	
	--  ********************************************************************
	--		CREATE External Job TABLES
	--  ********************************************************************
	IF OBJECT_ID('dbo.voc_external_job') IS NULL
        BEGIN   
            PRINT N'Creating [voc_external_job]...';
            CREATE TABLE [dbo].[voc_external_job](
			    [externalJobId] [numeric](19, 0) IDENTITY(1,1) NOT NULL,
			    [version] [int] NOT NULL,
				[createdDate] [datetime] NOT NULL,
				[name] [nvarchar](50) NOT NULL,
			    [jobType] [int] NULL,
			    [command] [nvarchar](255) NOT NULL,
			    [workingDirectory] [nvarchar](255) NOT NULL,
			PRIMARY KEY CLUSTERED 
			(
				[externalJobId] ASC
			)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
			) ON [PRIMARY]
			
		END
	ELSE
		BEGIN
			print N'table - voc_external_job already exists...';
		END			
	GO	
	
	
	--
	-- Inserting data into table VOC_COLUMN
	--
	IF (SELECT COUNT(*) from voc_column WHERE id = -24107) = 0
		BEGIN
			PRINT N'Dropping constraint FK6049706B16D0F6D2'
			ALTER TABLE [dbo].[voc_column] DROP CONSTRAINT [FK6049706B16D0F6D2]
			PRINT N'Dropping constraint FK6049706B971DA1E7'
			ALTER TABLE [dbo].[voc_column] DROP CONSTRAINT [FK6049706B971DA1E7]
			
			PRINT N'Inserting new voc views'
			SET IDENTITY_INSERT [dbo].[voc_view] ON
			INSERT INTO [dbo].[voc_view]([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-9, N'externalJob', N'com.vocollect.epp.model.ExternalJob', NULL, NULL)
			INSERT INTO [dbo].[voc_view]([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1224, N'vscResponse', N'com.vocollect.voicelink.core.model.VehicleSafetyCheckResponse', NULL, NULL)
			INSERT INTO [dbo].[voc_view]([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1225, N'vscVehicleType', N'com.vocollect.voicelink.core.model.VehicleType', NULL, NULL)
			INSERT INTO [dbo].[voc_view]([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1226, N'vscVehicle', N'com.vocollect.voicelink.core.model.Vehicle', NULL, NULL)
			INSERT INTO [dbo].[voc_view]([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1227, N'vscSafetyCheck', N'com.vocollect.voicelink.core.model.VehicleSafetyCheck', NULL, NULL)
			SET IDENTITY_INSERT [dbo].[voc_view] OFF
			
			PRINT N'Inserting new voc columns'
			SET IDENTITY_INSERT [dbo].[VOC_COLUMN] ON;
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24107, -1227, N'createdDate', N'vsc.safetyCheck.view.column.creationDate', 0, 5, 0, 2, 1, 150, 1, 6, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24106, -1227, N'responseType', N'vsc.safetyCheck.view.column.responseType', 1, 5, 0, 2, 1, 150, 1, 5, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24105, -1227, N'vehicleType.description', N'vsc.safetyCheck.view.column.vehicleType', 1, 5, 0, 2, 1, 150, 1, 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24104, -1227, N'spokenDescription', N'vsc.safetyCheck.view.column.spokenDescription', 0, 5, 0, 2, 1, 150, 1, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24103, -1227, N'description', N'vsc.safetyCheck.view.column.description', 0, 5, 0, 1, 1, 150, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24102, -1227, N'sequence', N'vsc.safetyCheck.view.column.sequence', 0, 5, 0, 1, 1, 150, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24101, -1227, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24005, -1226, N'createdDate', N'vsc.vehicle.view.column.createdDate', 0, 5, 0, 2, 1, 150, 1, 4, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24004, -1226, N'vehicleCategory', N'vsc.vehicle.view.column.vehicleCategory', 0, 5, 0, 1, 1, 150, 1, 3, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24003, -1226, N'spokenVehicleNumber', N'vsc.vehicle.view.column.spokenVehicleNumber', 1, 5, 0, 1, 1, 150, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24002, -1226, N'vehicleNumber', N'vsc.vehicle.view.column.vehicleNumber', 1, 5, 0, 1, 1, 150, 1, 1, NULL, 0, NULL, NULL, N'displayVehicleNumber', NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24001, -1226, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23906, -1225, N'safetyCheckCount', N'vsc.vehicleType.view.column.safetyChecks', 0, 5, 0, 1, 1, 150, 1, 5, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23905, -1225, N'vehicleCount', N'vsc.vehicleType.view.column.vehicles', 0, 5, 0, 1, 1, 150, 1, 4, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23904, -1225, N'isCaptureVehicleID', N'vsc.vehicleType.view.column.captureVehicleID', 0, 5, 0, 2, 1, 150, 1, 3, NULL, 4, NULL, NULL, N'displayCaptureVehicleId', NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23903, -1225, N'description', N'vsc.vehicleType.view.column.description', 0, 5, 0, 1, 1, 150, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23902, -1225, N'number', N'vsc.vehicleType.view.column.number', 1, 5, 0, 1, 1, 150, 1, 1, NULL, 1, NULL, NULL, N'displayVehicleTypeNumber', NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23901, -1225, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23810, -1224, N'internalGroupId', N'vsc.response.view.column.groupId', 1, 1, 0, 1, 0, 100, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23809, -1224, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23807, -1224, N'note', N'vsc.response.view.column.note', 0, 5, 0, 1, 1, 90, 1, 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, 1)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23806, -1224, N'operator.common.operatorIdentifier', N'common.view.column.operator', 0, 7, 0, 1, 1, 100, 1, 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23805, -1224, N'repairAction', N'vsc.response.view.column.repairAction', 0, 5, 0, 1, 1, 90, 1, 6, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23804, -1224, N'checkResponse', N'vsc.response.view.column.checkResponse', 0, 5, 0, 1, 1, 110, 1, 5, NULL, 0, N'displayCheckResponseFilter', NULL, N'displayCheckResponse', NULL, NULL, NULL, 2, NULL, NULL, 1)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23803, -1224, N'safetyCheck.description', N'vsc.response.view.column.safetyCheck', 0, 5, 0, 1, 1, 90, 1, 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23802, -1224, N'vehicle.vehicleType.description', N'vsc.response.view.column.vehicleType', 0, 5, 0, 1, 1, 90, 1, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-23801, -1224, N'vehicle.vehicleNumber', N'vsc.vehicle.view.column.vehicleNumber', 0, 1, 0, 1, 1, 150, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-22808, -1224, N'createdDate', N'vsc.response.view.column.checkDate', 1, 1, 0, 1, 0, 100, 1, 9, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-806, -9, N'createdDate', N'externaljob.view.column.creationDate', 0, 5, 0, 1, 1, 150, 1, 5, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-805, -9, N'workingDirectory', N'externaljob.view.column.workingDirectory', 0, 5, 0, 1, 1, 150, 1, 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-804, -9, N'command', N'externaljob.view.column.command', 0, 5, 0, 1, 1, 150, 1, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-803, -9, N'jobType', N'externaljob.view.column.type', 0, 5, 0, 1, 1, 150, 1, 2, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-802, -9, N'name', N'externaljob.view.column.name', 1, 5, 0, 1, 1, 150, 1, 1, NULL, 0, NULL, NULL, N'displayExternalJob', NULL, NULL, NULL, 1, NULL, NULL, NULL)
			INSERT INTO [dbo].[voc_column]([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-801, -9, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
			SET IDENTITY_INSERT [dbo].[voc_column] OFF
			
			Print N'Adding Constraint FK6049706B16D0F6D2....'
			ALTER TABLE [dbo].[voc_column] ADD CONSTRAINT [FK6049706B16D0F6D2] FOREIGN KEY ([view_id]) REFERENCES [dbo].[voc_view] ([id]) 
			Print N'Adding Constraint FK6049706B971DA1E7....'
			ALTER TABLE [dbo].[voc_column] ADD CONSTRAINT [FK6049706B971DA1E7] FOREIGN KEY ([data_type_id]) REFERENCES [dbo].[voc_data_type] ([id]) 
		END
	ELSE
		BEGIN
			PRINT N'New voc_columns were already added'
		END
	GO
	
	--
	-- Inserting data into table VOC_FEATURE
	--
	IF (SELECT count(*) FROM voc_feature WHERE id = -1514) = 0
		BEGIN
			PRINT N'Dropping constraing Updating FK17DCDF26241ED7C2...'
			ALTER TABLE [dbo].[voc_role_to_feature] DROP CONSTRAINT [FK17DCDF26241ED7C2] 
			PRINT N'Dropping constraing Updating FK17DCDF264DCDD932...'
			ALTER TABLE [dbo].[voc_role_to_feature] DROP CONSTRAINT [FK17DCDF264DCDD932] 
			PRINT N'Dropping constraing Updating FK35EB80819FA773C...'
			ALTER TABLE [dbo].[voc_feature] DROP CONSTRAINT [FK35EB80819FA773C] 	
			
			PRINT N'Inserting new rows into voc_feature'
			SET IDENTITY_INSERT [dbo].[voc_feature] ON; 
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1514, N'feature.voicelink.vsc.safetyCheck.edit', NULL, 0, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1513, N'feature.voicelink.vsc.safetyCheck.create', NULL, 0, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1512, N'feature.voicelink.vsc.safetyCheck.delete', NULL, 0, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1511, N'feature.voicelink.vsc.checkResponse.editNotes', NULL, 0, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1510, N'feature.voicelink.vsc.safetyCheck.view', NULL, 0, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1509, N'feature.voicelink.vsc.vehicle.edit', NULL, 0, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1508, N'feature.voicelink.vsc.vehicleType.edit', NULL, 0, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1507, N'feature.voicelink.vsc.vehicle.delete', NULL, 0, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1506, N'feature.voicelink.vsc.vehicle.view', NULL, 0, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1505, N'feature.voicelink.vsc.vehicle.create', NULL, 0, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1504, N'feature.voicelink.vsc.vehicleType.view', NULL, 1, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1503, N'feature.voicelink.vsc.vehicleType.delete', NULL, 0, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1502, N'feature.voicelink.vsc.vehicleType.create', NULL, 0, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1501, N'feature.voicelink.vsc.vehicleAndType.view', NULL, 1, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-1500, N'feature.voicelink.vsc.view', NULL, 1, -1005)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-29, N'feature.appAdmin.externalJob.delete', NULL, 0, -2)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-28, N'feature.appAdmin.externalJob.create', NULL, 0, -2)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-27, N'feature.appAdmin.externalJob.edit', NULL, 0, -2)
			INSERT INTO [dbo].[voc_feature]([id], [name], [description], [read_only], [group_id]) VALUES (-26, N'feature.appAdmin.externalJob.view', NULL, 1, -2)
			SET IDENTITY_INSERT [dbo].[voc_feature] OFF; 
			
			--
			-- Inserting data into table dbo.voc_role_to_feature
			--
			PRINT N'Inserting new rows in voc_role_to_feature'
			INSERT INTO [dbo].[voc_role_to_feature]([role_id], [feature_id]) VALUES (-3, -26)			
			INSERT INTO [dbo].[voc_role_to_feature]([role_id], [feature_id]) VALUES (-3, -1500)
			INSERT INTO [dbo].[voc_role_to_feature]([role_id], [feature_id]) VALUES (-3, -1501)
			INSERT INTO [dbo].[voc_role_to_feature]([role_id], [feature_id]) VALUES (-3, -1504)
			INSERT INTO [dbo].[voc_role_to_feature]([role_id], [feature_id]) VALUES (-3, -1506)
			INSERT INTO [dbo].[voc_role_to_feature]([role_id], [feature_id]) VALUES (-3, -1510)
			
			Print N'Adding Constraint FK17DCDF26241ED7C2....'
			ALTER TABLE [dbo].[voc_role_to_feature] ADD CONSTRAINT [FK17DCDF26241ED7C2] FOREIGN KEY ([feature_id]) REFERENCES [dbo].[voc_feature] ([id]) 
			Print N'Adding Constraint FK17DCDF264DCDD932....'
			ALTER TABLE [dbo].[voc_role_to_feature] ADD CONSTRAINT [FK17DCDF264DCDD932] FOREIGN KEY ([role_id]) REFERENCES [dbo].[voc_role] ([id]) 
			Print N'Adding Constraint FK35EB80819FA773C....'
			ALTER TABLE [dbo].[voc_feature] ADD CONSTRAINT [FK35EB80819FA773C] FOREIGN KEY ([group_id]) REFERENCES [dbo].[voc_feature_group] ([id]) 
		END
	ELSE
		BEGIN
			PRINT N'New voc_feature were already added'
		END
	GO		
	
	--
	-- Inserting data into table dbo.voc_reportType
	--
	IF (SELECT count(*) FROM voc_reportType WHERE id = -1600) = 0
		BEGIN
			PRINT N'Dropping constraint Updating FK853F878D58A22052...'
			ALTER TABLE [dbo].[voc_reportType_parameter] DROP CONSTRAINT FK853F878D58A22052
			SET IDENTITY_INSERT [dbo].[voc_reportType] ON
			INSERT INTO [dbo].[voc_reportType]([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-1600, N'VehicleSafetyChecks', N'report.name.vsc', N'Vehicle Safety Checks Report', N'voicelink.core', N'vscReport')
			SET IDENTITY_INSERT dbo.voc_reportType OFF
			
			SET IDENTITY_INSERT [dbo].[voc_reportType_parameter] ON
			INSERT INTO [dbo].[voc_reportType_parameter](id, reportType_id, parameter_name, default_value, validation_function, description, field_type, service_name, service_method, display_member, data_member, prompt_order, is_required, maxlength) VALUES (-1601, -1600, N'OPERATOR_ID', N'operators', N'', N'vsc.report.operator', 3, N'operatorManager', N'all', N'reportOperatorIdentifier', N'operatorIdentifier', 3, 0, NULL)
			INSERT INTO [dbo].[voc_reportType_parameter](id, reportType_id, parameter_name, default_value, validation_function, description, field_type, service_name, service_method, display_member, data_member, prompt_order, is_required, maxlength) VALUES (-1602, -1600, N'OPERATOR_TEAM_ID', N'operatorTeams', NULL, N'vsc.report.operatorteam', 3, N'operatorTeamManager', N'all', N'name', N'id', 4, 0, NULL)
			INSERT INTO [dbo].[voc_reportType_parameter](id, reportType_id, parameter_name, default_value, validation_function, description, field_type, service_name, service_method, display_member, data_member, prompt_order, is_required, maxlength) VALUES (-1603, -1600, N'VEHICLE_TYPE', N'vehicleTypes', NULL, N'vsc.report.vehicletype', 1, N'vehicleTypeManager', N'all', N'description', N'number', 1, 0, NULL)
			INSERT INTO [dbo].[voc_reportType_parameter](id, reportType_id, parameter_name, default_value, validation_function, description, field_type, service_name, service_method, display_member, data_member, prompt_order, is_required, maxlength) VALUES (-1604, -1600, N'VEHICLE', NULL, N'defCheck', N'vsc.report.vehicle', 0, NULL, NULL, N'vehicleNumber', NULL, 2, 0, NULL)
			SET IDENTITY_INSERT [dbo].[voc_reportType_parameter] OFF
			Print N'New report added'
			
			Print N'Adding Constraint FK853F878D58A22052....'
			ALTER TABLE [dbo].[voc_reportType_parameter] ADD CONSTRAINT [FK853F878D58A22052] FOREIGN KEY ([reportType_id]) REFERENCES [dbo].[voc_reportType] ([id]) 
		END
	GO
	
	-- *****************************************************************************************
	-- Updating data in table VOC_SYSTEM_PROPERTIES
	-- This will need to be added to all future upgrade scripts
	-- ****************************************************************************************
	Print N'Updating voc_system_proerties so import_setup_xml will be forced to rebuild...'
	UPDATE [dbo].[voc_system_properties] set [value] = 1 WHERE [systemPropertyId] = -1005;
	GO
	
	--
	-- Update module versions
	--	
	IF (SELECT count(*) FROM voc_plugin_modules WHERE moduleVersion = '4.2') = 0
		BEGIN
			-- Update module versions
			-- To update the version, change the SET @vlVersion... line below
			-- This needs to be done every time there is a new application added to VoiceLink
			SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
			SET XACT_ABORT ON;
			BEGIN TRANSACTION;
				DECLARE @vlVersion varchar(32);
				DECLARE @vipVersion varchar(32);
				SET @vlVersion = '4.2';
				SET @vipVersion = '2.1';
				DECLARE @newId bigint;
				DECLARE @oldId bigint;
				SET @oldId = (select moduleId from voc_plugin_modules where name = 'plugin.module.voicelink' AND isEnabled = 1);
				Print N'Updating plugin modules...'
				UPDATE voc_plugin_modules set isEnabled = 0 where name = 'plugin.module.voicelink';
				Print N'Inserting new plugin module'
				INSERT into voc_plugin_modules (name, moduleVersion, frameworkVersion, sequenceNumber, uri, navMenuName, isEnabled, createdDate) 
				VALUES ('plugin.module.voicelink', @vlVersion, @vipVersion, 101, '/selection/home.action', 'voicelink', 1, convert(varchar(10),getdate(),101));
				SET @newId = (select moduleId from voc_plugin_modules where name = 'plugin.module.voicelink' AND isEnabled = 1);
				UPDATE voc_plugin_components set moduleId = @newId where moduleId = @oldId;
				UPDATE voc_plugin_modules_auth set moduleId = @newId where moduleId = @oldId;
				
				UPDATE voc_plugin_modules SET moduleVersion = @vipVersion WHERE moduleId = 1;
				UPDATE voc_plugin_modules SET frameworkVersion = @vipVersion WHERE moduleId = 1;
				UPDATE voc_plugin_modules SET moduleVersion = @vipVersion WHERE moduleId = 2;
				UPDATE voc_plugin_modules SET frameworkVersion = @vipVersion WHERE moduleId = 2;
			COMMIT TRANSACTION
		END
	GO