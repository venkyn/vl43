/*
Deployment script for VL311
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO

PRINT N'Dropping FK7C71933E81FCA72...';


GO
ALTER TABLE [voc_report_parameter] DROP CONSTRAINT [FK7C71933E81FCA72];


GO
PRINT N'Dropping FK7FFEBD9CEA320F22...';


GO
ALTER TABLE [voc_site] DROP CONSTRAINT [FK7FFEBD9CEA320F22];


GO
PRINT N'Dropping FK7FFEBD9CB979502E...';


GO
ALTER TABLE [voc_site] DROP CONSTRAINT [FK7FFEBD9CB979502E];


GO
PRINT N'Dropping FK7FFEBD9CD458CE41...';


GO
ALTER TABLE [voc_site] DROP CONSTRAINT [FK7FFEBD9CD458CE41];


GO
PRINT N'Dropping FK7FFEBD9C7E140FE8...';


GO
ALTER TABLE [voc_site] DROP CONSTRAINT [FK7FFEBD9C7E140FE8];


GO
PRINT N'Dropping FKE0E0AF6C78E81B92...';


GO
ALTER TABLE [voc_user_to_site] DROP CONSTRAINT [FKE0E0AF6C78E81B92];


GO
PRINT N'Dropping [VOC_CONN_TEST]...';


GO
DROP TABLE [VOC_CONN_TEST];


GO
PRINT N'Dropping [voc_report]...';


GO
DROP TABLE [voc_report];


GO
PRINT N'Starting rebuilding table [voc_jobs]...';


GO
/*
The column [voc_jobs].[interval] is being dropped, data loss could occur.
*/
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

--BEGIN TRANSACTION;

CREATE TABLE [tmp_ms_xx_voc_jobs] (
    [jobId]           NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
    [version]         INT            NOT NULL,
    [name]            NVARCHAR (128) NOT NULL,
    [type]            INT            NULL,
    [job_class]       NVARCHAR (128) NOT NULL,
    [enabled]         TINYINT        NOT NULL,
    [display_name]    NVARCHAR (128) NOT NULL,
    [job_interval]    NUMERIC (19)   NULL,
    [cron_expression] NVARCHAR (255) NULL,
    [cron_hour]       NVARCHAR (255) NULL,
    [last_run]        DATETIME       NULL,
    [running]         TINYINT        NULL,
    [last_ping]       NUMERIC (19)   NULL,
    [ping_interval]   NUMERIC (19)   NULL,
    [repeat_count]    INT            NULL,
    PRIMARY KEY CLUSTERED ([jobId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF),
    UNIQUE NONCLUSTERED ([name] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF) ON [PRIMARY]
);

IF EXISTS (SELECT TOP 1 1
           FROM   [voc_jobs])
    BEGIN
        SET IDENTITY_INSERT [tmp_ms_xx_voc_jobs] ON;
        INSERT INTO [tmp_ms_xx_voc_jobs] ([jobId], [version], [name], [type], [job_class], [enabled], [display_name], [cron_expression], [cron_hour], [last_run], [running], [last_ping], [ping_interval], [repeat_count])
        SELECT   [jobId],
                 [version],
                 [name],
                 [type],
                 [job_class],
                 [enabled],
                 [display_name],
                 [cron_expression],
                 [cron_hour],
                 [last_run],
                 [running],
                 [last_ping],
                 [ping_interval],
                 [repeat_count]
        FROM     [voc_jobs]
        ORDER BY [jobId] ASC;
        SET IDENTITY_INSERT [tmp_ms_xx_voc_jobs] OFF;
    END;

DROP TABLE [voc_jobs];

EXECUTE sp_rename N'[tmp_ms_xx_voc_jobs]', N'voc_jobs';

--COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Starting rebuilding table [voc_report_parameter]...';


GO
/*
The column [voc_report_parameter].[data_member] is being dropped, data loss could occur.

The column [voc_report_parameter].[default_value] is being dropped, data loss could occur.

The column [voc_report_parameter].[description] is being dropped, data loss could occur.

The column [voc_report_parameter].[display_member] is being dropped, data loss could occur.

The column [voc_report_parameter].[field_type] is being dropped, data loss could occur.

The column [voc_report_parameter].[is_required] is being dropped, data loss could occur.

The column [voc_report_parameter].[maxlength] is being dropped, data loss could occur.

The column [voc_report_parameter].[parameter_name] is being dropped, data loss could occur.

The column [voc_report_parameter].[prompt_order] is being dropped, data loss could occur.

The column [voc_report_parameter].[service_method] is being dropped, data loss could occur.

The column [voc_report_parameter].[service_name] is being dropped, data loss could occur.

The column [voc_report_parameter].[validation_function] is being dropped, data loss could occur.

The column [voc_report_parameter].[reportTypeParameter] on table [voc_report_parameter] must be added, but the column has no default value and does not allow NULL values. If the table contains data, the ALTER script will not work. To avoid this issue, you must add a default value to the column or mark it as allowing NULL values.
*/
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

--BEGIN TRANSACTION;

PRINT N'Starting rebuilding table [voc_report_parameter]...';


DROP TABLE [voc_report_parameter];

CREATE TABLE [voc_report_parameter] (
    [id]                  NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [reportTypeParameter] NUMERIC (19)  NOT NULL,
    [value]               NVARCHAR (50) NULL,
    [report_id]           NUMERIC (19)  NULL,
    PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);

--COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Starting rebuilding table [voc_site]...';


GO
/*
The type for column name in table [voc_site] is currently  NVARCHAR (256) NOT NULL but is being changed to  NVARCHAR (255) NOT NULL. Data loss could occur.
*/
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

--BEGIN TRANSACTION;

CREATE TABLE [tmp_ms_xx_voc_site] (
    [siteId]                NUMERIC (19)    IDENTITY (1, 1) NOT NULL,
    [version]               INT             NOT NULL,
    [name]                  NVARCHAR (255)  NOT NULL,
    [description]           NVARCHAR (2000) NULL,
    [notes]                 NVARCHAR (2000) NULL,
    [timeZone]              NVARCHAR (255)  NULL,
    [eapType]               INT             NULL,
    [credentialAssociation] INT             NULL,
    [credentialType]        INT             NULL,
    [usePins]               TINYINT         NULL,
    [chargerDisconnect]     TINYINT         NULL,
    [restrictedSSID]        NVARCHAR (128)  NULL,
    [restrictedEapType]     INT             NULL,
    [sitePIN]               NVARCHAR (32)   NULL,
    [shiftStartTime]        NVARCHAR (5)    DEFAULT ('00:00') NOT NULL,
    [servercredentials_id]  NUMERIC (19)    NULL,
    [sitewideuser_id]       NUMERIC (19)    NULL,
    [restricteduser_id]     NUMERIC (19)    NULL,
    [ldapconfig_id]         NUMERIC (19)    NULL,
    PRIMARY KEY CLUSTERED ([siteId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF),
    UNIQUE NONCLUSTERED ([name] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF) ON [PRIMARY]
);

IF EXISTS (SELECT TOP 1 1
           FROM   [voc_site])
    BEGIN
        SET IDENTITY_INSERT [tmp_ms_xx_voc_site] ON;
        INSERT INTO [tmp_ms_xx_voc_site] ([siteId], [version], [name], [description], [notes], [timeZone], [eapType], [credentialAssociation], [credentialType], [usePins], [chargerDisconnect], [restrictedSSID], [restrictedEapType], [sitePIN], [servercredentials_id], [sitewideuser_id], [restricteduser_id], [ldapconfig_id])
        SELECT   [siteId],
                 [version],
                 [name],
                 [description],
                 [notes],
                 [timeZone],
                 [eapType],
                 [credentialAssociation],
                 [credentialType],
                 [usePins],
                 [chargerDisconnect],
                 [restrictedSSID],
                 [restrictedEapType],
                 [sitePIN],
                 [servercredentials_id],
                 [sitewideuser_id],
                 [restricteduser_id],
                 [ldapconfig_id]
        FROM     [voc_site]
        ORDER BY [siteId] ASC;
        SET IDENTITY_INSERT [tmp_ms_xx_voc_site] OFF;
    END;

DROP TABLE [voc_site];

EXECUTE sp_rename N'[tmp_ms_xx_voc_site]', N'voc_site';

--COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Creating [core_operator_teams]...';


GO
CREATE TABLE [core_operator_teams] (
    [operatorTeamId] NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
    [version]        INT           NOT NULL,
    [name]           NVARCHAR (50) NOT NULL,
    PRIMARY KEY CLUSTERED ([operatorTeamId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO
PRINT N'Creating [core_operators_operatorTeams]...';


GO
CREATE TABLE [core_operators_operatorTeams] (
    [operatorId]     NUMERIC (19) NOT NULL,
    [operatorTeamId] NUMERIC (19) NOT NULL,
    PRIMARY KEY CLUSTERED ([operatorTeamId] ASC, [operatorId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO
PRINT N'Creating [core_operatorTeam_tag]...';


GO
CREATE TABLE [core_operatorTeam_tag] (
    [tagged_id] NUMERIC (19) NOT NULL,
    [tag_id]    NUMERIC (19) NOT NULL,
    PRIMARY KEY CLUSTERED ([tagged_id] ASC, [tag_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO
PRINT N'Creating [voc_chart_settings]...';


GO
CREATE TABLE [voc_chart_settings] (
    [chartSettingsId]     NUMERIC (19) IDENTITY (1, 1) NOT NULL,
    [version]             INT          NOT NULL,
    [chartId]             NUMERIC (19) NOT NULL,
    [userId]              NUMERIC (19) NULL,
    [includeLegend]       TINYINT      NOT NULL,
    [includeLabels]       TINYINT      NOT NULL,
    [includeTooltips]     TINYINT      NOT NULL,
    [renderIn3D]          TINYINT      NOT NULL,
    [lowerBound]          INT          NULL,
    [upperBound]          INT          NULL,
    [barChartOrientation] INT          NOT NULL,
    PRIMARY KEY CLUSTERED ([chartSettingsId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO
PRINT N'Creating [voc_chart_settings].[IX_chartSettings_chart]...';


GO
CREATE NONCLUSTERED INDEX [IX_chartSettings_chart]
    ON [voc_chart_settings]([chartId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


GO
PRINT N'Creating [voc_charts]...';


GO
CREATE TABLE [voc_charts] (
    [chartId]             NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
    [version]             INT            NOT NULL,
    [chartName]           NVARCHAR (128) NOT NULL,
    [chartType]           INT            NOT NULL,
    [title]               NVARCHAR (128) NOT NULL,
    [description]         NVARCHAR (256) NULL,
    [categoryAxisLabel]   NVARCHAR (128) NULL,
    [rangeAxisLabel]      NVARCHAR (128) NULL,
    [datasetProviderBean] NVARCHAR (128) NOT NULL,
    [chartRendererBean]   NVARCHAR (128) NOT NULL,
    PRIMARY KEY CLUSTERED ([chartId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO
PRINT N'Creating [voc_charts].[IX_chart_type]...';


GO
CREATE NONCLUSTERED INDEX [IX_chart_type]
    ON [voc_charts]([chartType] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


GO
PRINT N'Creating [voc_report_tag]...';


GO
CREATE TABLE [voc_report_tag] (
    [tagged_id] NUMERIC (19) NOT NULL,
    [tag_id]    NUMERIC (19) NOT NULL,
    PRIMARY KEY CLUSTERED ([tagged_id] ASC, [tag_id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO
PRINT N'Creating [voc_reports]...';


GO
CREATE TABLE [voc_reports] (
    [reportId]   NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
    [version]    INT            NOT NULL,
    [name]       NVARCHAR (50)  NOT NULL,
    [interval]   INT            NOT NULL,
    [reportType] NUMERIC (19)   NOT NULL,
    [format]     INT            NOT NULL,
    [frequency]  INT            NOT NULL,
    [subject]    NVARCHAR (50)  NULL,
    [emails]     NVARCHAR (255) NULL,
    [time]       DATETIME       NULL,
    [lastRun]    DATETIME       NULL,
    PRIMARY KEY CLUSTERED ([reportId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO
PRINT N'Creating [voc_reportType]...';


GO
CREATE TABLE [voc_reportType] (
    [id]                     NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
    [reportType_name]        NVARCHAR (50)  NOT NULL,
    [reportType_name_key]    NVARCHAR (255) NOT NULL,
    [reportType_description] NVARCHAR (100) NULL,
    [app_name]               NVARCHAR (255) NULL,
    [session_bean_id]        NVARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF),
    UNIQUE NONCLUSTERED ([reportType_name] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF) ON [PRIMARY]
);


GO
PRINT N'Creating [voc_reportType_parameter]...';


GO
CREATE TABLE [voc_reportType_parameter] (
    [id]                  NUMERIC (19)    IDENTITY (1, 1) NOT NULL,
    [reportType_id]       NUMERIC (19)    NOT NULL,
    [parameter_name]      NVARCHAR (50)   NOT NULL,
    [default_value]       NVARCHAR (1000) NULL,
    [validation_function] NVARCHAR (100)  NULL,
    [description]         NVARCHAR (50)   NULL,
    [field_type]          INT             NOT NULL,
    [service_name]        NVARCHAR (100)  NULL,
    [service_method]      NVARCHAR (50)   NULL,
    [display_member]      NVARCHAR (50)   NULL,
    [data_member]         NVARCHAR (50)   NULL,
    [prompt_order]        SMALLINT        NULL,
    [is_required]         TINYINT         NOT NULL,
    [maxlength]           NVARCHAR (255)  NULL,
    PRIMARY KEY CLUSTERED ([id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
);


GO

PRINT N'Creating [core_operator_labor].[IX_core_oper_labor_createdDate]...';


GO
CREATE NONCLUSTERED INDEX [IX_core_oper_labor_createdDate]
    ON [core_operator_labor]([createdDate] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


GO
PRINT N'Creating [core_operator_labor].[IX_core_oper_labor_prevOpLabId]...';


GO
CREATE NONCLUSTERED INDEX [IX_core_oper_labor_prevOpLabId]
    ON [core_operator_labor]([previousOperatorLaborId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


GO
PRINT N'Creating [sel_pick_details].[IX_sel_pick_details_pickId]...';


GO
CREATE NONCLUSTERED INDEX [IX_sel_pick_details_pickId]
    ON [sel_pick_details]([pickId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


GO
PRINT N'Creating [sel_picks].[IX_pick_originalPickId]...';


GO
CREATE NONCLUSTERED INDEX [IX_pick_originalPickId]
    ON [sel_picks]([originalPickId] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF, ONLINE = OFF, MAXDOP = 0)
    ON [PRIMARY];


GO
PRINT N'Creating FK7C71933E81FCA72...';


GO
ALTER TABLE [voc_report_parameter] WITH NOCHECK
    ADD CONSTRAINT [FK7C71933E81FCA72] FOREIGN KEY ([report_id]) REFERENCES [voc_reports] ([reportId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK7C7193372D37F7E...';


GO
ALTER TABLE [voc_report_parameter] WITH NOCHECK
    ADD CONSTRAINT [FK7C7193372D37F7E] FOREIGN KEY ([reportTypeParameter]) REFERENCES [voc_reportType_parameter] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK7FFEBD9CEA320F22...';


GO
ALTER TABLE [voc_site] WITH NOCHECK
    ADD CONSTRAINT [FK7FFEBD9CEA320F22] FOREIGN KEY ([sitewideuser_id]) REFERENCES [voc_networkcredentials] ([actionItemId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK7FFEBD9CB979502E...';


GO
ALTER TABLE [voc_site] WITH NOCHECK
    ADD CONSTRAINT [FK7FFEBD9CB979502E] FOREIGN KEY ([servercredentials_id]) REFERENCES [voc_networkcredentials] ([actionItemId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK7FFEBD9CD458CE41...';


GO
ALTER TABLE [voc_site] WITH NOCHECK
    ADD CONSTRAINT [FK7FFEBD9CD458CE41] FOREIGN KEY ([restricteduser_id]) REFERENCES [voc_networkcredentials] ([actionItemId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK7FFEBD9C7E140FE8...';


GO
ALTER TABLE [voc_site] WITH NOCHECK
    ADD CONSTRAINT [FK7FFEBD9C7E140FE8] FOREIGN KEY ([ldapconfig_id]) REFERENCES [voc_ldapconfiguration] ([ldapConfigurationId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FKE0E0AF6C78E81B92...';


GO
ALTER TABLE [voc_user_to_site] WITH NOCHECK
    ADD CONSTRAINT [FKE0E0AF6C78E81B92] FOREIGN KEY ([site_id]) REFERENCES [voc_site] ([siteId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_Operators_OperatorTeams...';


GO
ALTER TABLE [core_operators_operatorTeams] WITH NOCHECK
    ADD CONSTRAINT [FK_Operators_OperatorTeams] FOREIGN KEY ([operatorTeamId]) REFERENCES [core_operator_teams] ([operatorTeamId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_OperatorTeams_Operators...';


GO
ALTER TABLE [core_operators_operatorTeams] WITH NOCHECK
    ADD CONSTRAINT [FK_OperatorTeams_Operators] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FKC68E681C6CAD2857...';


GO
ALTER TABLE [core_operatorTeam_tag] WITH NOCHECK
    ADD CONSTRAINT [FKC68E681C6CAD2857] FOREIGN KEY ([tagged_id]) REFERENCES [core_operator_teams] ([operatorTeamId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FKC68E681C3EF30942...';


GO
ALTER TABLE [core_operatorTeam_tag] WITH NOCHECK
    ADD CONSTRAINT [FKC68E681C3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK_Chart_ChartSettings...';


GO
ALTER TABLE [voc_chart_settings] WITH NOCHECK
    ADD CONSTRAINT [FK_Chart_ChartSettings] FOREIGN KEY ([chartId]) REFERENCES [voc_charts] ([chartId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FKAB10C779C9E8E129...';


GO
ALTER TABLE [voc_chart_settings] WITH NOCHECK
    ADD CONSTRAINT [FKAB10C779C9E8E129] FOREIGN KEY ([userId]) REFERENCES [voc_user] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK34BF94A486A6A33A...';


GO
ALTER TABLE [voc_report_tag] WITH NOCHECK
    ADD CONSTRAINT [FK34BF94A486A6A33A] FOREIGN KEY ([tagged_id]) REFERENCES [voc_reports] ([reportId]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK34BF94A43EF30942...';


GO
ALTER TABLE [voc_report_tag] WITH NOCHECK
    ADD CONSTRAINT [FK34BF94A43EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FKB187574A96D14234...';


GO
ALTER TABLE [voc_reports] WITH NOCHECK
    ADD CONSTRAINT [FKB187574A96D14234] FOREIGN KEY ([reportType]) REFERENCES [voc_reportType] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Creating FK853F878D58A22052...';


GO
ALTER TABLE [voc_reportType_parameter] WITH NOCHECK
    ADD CONSTRAINT [FK853F878D58A22052] FOREIGN KEY ([reportType_id]) REFERENCES [voc_reportType] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


GO
PRINT N'Refreshing [vw_export_operatoractions]...';


GO
EXECUTE sp_refreshview N'vw_export_operatoractions';


GO
PRINT N'Refreshing [vw_export_lineloading]...';


GO
EXECUTE sp_refreshview N'vw_export_lineloading';


GO
PRINT N'Refreshing [vw_export_putaway]...';


GO
EXECUTE sp_refreshview N'vw_export_putaway';


GO
PRINT N'Refreshing [vw_export_pts_container]...';


GO
EXECUTE sp_refreshview N'vw_export_pts_container';


GO
PRINT N'Refreshing [vw_export_pts_put]...';


GO
EXECUTE sp_refreshview N'vw_export_pts_put';


GO
PRINT N'Refreshing [vw_export_pts_complete]...';


GO
EXECUTE sp_refreshview N'vw_export_pts_complete';


GO
PRINT N'Refreshing [vw_export_pts_labor]...';


GO
EXECUTE sp_refreshview N'vw_export_pts_labor';


GO
PRINT N'Refreshing [vw_export_replenishment]...';


GO
EXECUTE sp_refreshview N'vw_export_replenishment';


GO
PRINT N'Refreshing [vw_export_container]...';


GO
EXECUTE sp_refreshview N'vw_export_container';


GO
PRINT N'Refreshing [vw_export_picked]...';


GO
EXECUTE sp_refreshview N'vw_export_picked';


GO
PRINT N'Refreshing [vw_export_complete]...';


GO
EXECUTE sp_refreshview N'vw_export_complete';


GO
PRINT N'Refreshing [vw_export_labor]...';


GO
EXECUTE sp_refreshview N'vw_export_labor';


GO
PRINT N'Checking existing data against newly created constraints';


GO

ALTER TABLE [voc_report_parameter] WITH CHECK CHECK CONSTRAINT [FK7C71933E81FCA72];

ALTER TABLE [voc_report_parameter] WITH CHECK CHECK CONSTRAINT [FK7C7193372D37F7E];

ALTER TABLE [voc_site] WITH CHECK CHECK CONSTRAINT [FK7FFEBD9CEA320F22];

ALTER TABLE [voc_site] WITH CHECK CHECK CONSTRAINT [FK7FFEBD9CB979502E];

ALTER TABLE [voc_site] WITH CHECK CHECK CONSTRAINT [FK7FFEBD9CD458CE41];

ALTER TABLE [voc_site] WITH CHECK CHECK CONSTRAINT [FK7FFEBD9C7E140FE8];

ALTER TABLE [voc_user_to_site] WITH CHECK CHECK CONSTRAINT [FKE0E0AF6C78E81B92];

ALTER TABLE [core_operators_operatorTeams] WITH CHECK CHECK CONSTRAINT [FK_Operators_OperatorTeams];

ALTER TABLE [core_operators_operatorTeams] WITH CHECK CHECK CONSTRAINT [FK_OperatorTeams_Operators];

ALTER TABLE [core_operatorTeam_tag] WITH CHECK CHECK CONSTRAINT [FKC68E681C6CAD2857];

ALTER TABLE [core_operatorTeam_tag] WITH CHECK CHECK CONSTRAINT [FKC68E681C3EF30942];

ALTER TABLE [voc_chart_settings] WITH CHECK CHECK CONSTRAINT [FK_Chart_ChartSettings];

ALTER TABLE [voc_chart_settings] WITH CHECK CHECK CONSTRAINT [FKAB10C779C9E8E129];

ALTER TABLE [voc_report_tag] WITH CHECK CHECK CONSTRAINT [FK34BF94A486A6A33A];

ALTER TABLE [voc_report_tag] WITH CHECK CHECK CONSTRAINT [FK34BF94A43EF30942];

ALTER TABLE [voc_reports] WITH CHECK CHECK CONSTRAINT [FKB187574A96D14234];

ALTER TABLE [voc_reportType_parameter] WITH CHECK CHECK CONSTRAINT [FK853F878D58A22052];


GO


/*
This script was created by Visual Studio on 11/4/2010 at 9:40 AM.
Run this script on [VL311] to make it the same as [VoiceLink].
This script performs its actions in the following order:
1. Disable foreign-key constraints.
2. Perform DELETE commands. 
3. Perform UPDATE commands.
4. Perform INSERT commands.
5. Re-enable foreign-key constraints.
Please back up your target database before running this script.
*/
SET XACT_ABORT, ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO
/*Pointer used for text / image updates. This might not be needed, but is declared here just in case*/
DECLARE @pv binary(16)
--BEGIN TRANSACTION;
ALTER TABLE [arch_pts_container_details] DROP CONSTRAINT [FK_ArchCont_ArchContDetail]
ALTER TABLE [core_lots] DROP CONSTRAINT [FK_Item_Lot]
ALTER TABLE [core_lots] DROP CONSTRAINT [FK_Location_Lot]
ALTER TABLE [voc_user_to_site] DROP CONSTRAINT [FKE0E0AF6C78E81B92]
ALTER TABLE [voc_user_to_site] DROP CONSTRAINT [FKE0E0AF6CF2F89D12]
ALTER TABLE [replen_regions] DROP CONSTRAINT [FKC056CF18630F1E6B]
ALTER TABLE [voc_notificationdetail] DROP CONSTRAINT [FKCFDAA91BC355A75]
ALTER TABLE [voc_operator_tag] DROP CONSTRAINT [FKC4CDDED43EF30942]
ALTER TABLE [voc_operator_tag] DROP CONSTRAINT [FKC4CDDED48BBBC9B5]
ALTER TABLE [pts_container_tag] DROP CONSTRAINT [FK248A498C183A1D0]
ALTER TABLE [pts_container_tag] DROP CONSTRAINT [FK248A498C3EF30942]
ALTER TABLE [voc_role_to_feature] DROP CONSTRAINT [FK17DCDF26241ED7C2]
ALTER TABLE [voc_role_to_feature] DROP CONSTRAINT [FK17DCDF264DCDD932]
ALTER TABLE [sel_assignment_containers] DROP CONSTRAINT [FK_Assignments_Containers]
ALTER TABLE [sel_assignment_containers] DROP CONSTRAINT [FK_Containers_Assignments]
ALTER TABLE [core_breaktype_tag] DROP CONSTRAINT [FK8F877A743EF30942]
ALTER TABLE [core_breaktype_tag] DROP CONSTRAINT [FK8F877A74D1BD43F]
ALTER TABLE [voc_device_tag] DROP CONSTRAINT [FKF030FCC63EF30942]
ALTER TABLE [voc_device_tag] DROP CONSTRAINT [FKF030FCC6DB9C1FE7]
ALTER TABLE [voc_user_column] DROP CONSTRAINT [FK4D5D625564367F32]
ALTER TABLE [voc_user_column] DROP CONSTRAINT [FK4D5D6255F2F89D12]
ALTER TABLE [lineload_regions] DROP CONSTRAINT [FK417D3F9A630F1E6B]
ALTER TABLE [replen_replenishment_det_tag] DROP CONSTRAINT [FK9983AF0C3B598268]
ALTER TABLE [replen_replenishment_det_tag] DROP CONSTRAINT [FK9983AF0C3EF30942]
ALTER TABLE [core_lot_tag] DROP CONSTRAINT [FK723AC90C38B884B7]
ALTER TABLE [core_lot_tag] DROP CONSTRAINT [FK723AC90C3EF30942]
ALTER TABLE [replen_replenishment_tag] DROP CONSTRAINT [FK2FD8BC383EF30942]
ALTER TABLE [replen_replenishment_tag] DROP CONSTRAINT [FK2FD8BC3878DBD0B7]
ALTER TABLE [putaway_license_detail_tag] DROP CONSTRAINT [FK930D826C3EF30942]
ALTER TABLE [putaway_license_detail_tag] DROP CONSTRAINT [FK930D826CA8E8E49E]
ALTER TABLE [core_location_item_tag] DROP CONSTRAINT [FKF0D0A5183EF30942]
ALTER TABLE [core_location_item_tag] DROP CONSTRAINT [FKF0D0A5186F29E79E]
ALTER TABLE [core_printer_tag] DROP CONSTRAINT [FKC5A2B155305E9D80]
ALTER TABLE [core_printer_tag] DROP CONSTRAINT [FKC5A2B1553EF30942]
ALTER TABLE [replen_replenishment_details] DROP CONSTRAINT [FK_Location_ReplenishDetail]
ALTER TABLE [replen_replenishment_details] DROP CONSTRAINT [FK_Operator_ReplenishDetail]
ALTER TABLE [replen_replenishment_details] DROP CONSTRAINT [FK_ReasonCode_ReplenishDetail]
ALTER TABLE [replen_replenishment_details] DROP CONSTRAINT [FK_Replenish_ReplenishDetail]
ALTER TABLE [core_item_tag] DROP CONSTRAINT [FK8D1D4D0E3EF30942]
ALTER TABLE [core_item_tag] DROP CONSTRAINT [FK8D1D4D0EC2569E49]
ALTER TABLE [arch_sel_containers] DROP CONSTRAINT [FK_Arch_Assignment_Arch_cont]
ALTER TABLE [voc_homepage_access_summaries] DROP CONSTRAINT [FK789FEEA56A944441]
ALTER TABLE [voc_homepage_access_summaries] DROP CONSTRAINT [FK789FEEA5E5EF59C2]
ALTER TABLE [core_workgroup_tag] DROP CONSTRAINT [FKFAFE69493EF30942]
ALTER TABLE [core_workgroup_tag] DROP CONSTRAINT [FKFAFE6949BFAC0174]
ALTER TABLE [voc_user_properties] DROP CONSTRAINT [FK6FE84B2C9E8E129]
ALTER TABLE [core_operators_operatorTeams] DROP CONSTRAINT [FK_Operators_OperatorTeams]
ALTER TABLE [core_operators_operatorTeams] DROP CONSTRAINT [FK_OperatorTeams_Operators]
ALTER TABLE [voc_reportType_parameter] DROP CONSTRAINT [FK853F878D58A22052]
ALTER TABLE [arch_pts_put_details] DROP CONSTRAINT [FK_Archive_Put_PutDetails]
ALTER TABLE [voc_user_tag] DROP CONSTRAINT [FK88FAA8FB3EF30942]
ALTER TABLE [voc_user_tag] DROP CONSTRAINT [FK88FAA8FB853527D1]
ALTER TABLE [pts_customer_location_tag] DROP CONSTRAINT [FK3C355813EF30942]
ALTER TABLE [pts_customer_location_tag] DROP CONSTRAINT [FK3C3558173697EE0]
ALTER TABLE [sel_sum_prompt_loc] DROP CONSTRAINT [FK_SumPromptDef_SumPromptLoc]
ALTER TABLE [sel_sum_prompt_tag] DROP CONSTRAINT [FKDF7B73383EF30942]
ALTER TABLE [sel_sum_prompt_tag] DROP CONSTRAINT [FKDF7B7338E9E659E5]
ALTER TABLE [sel_container_tag] DROP CONSTRAINT [FK69FBFBB73EF30942]
ALTER TABLE [sel_container_tag] DROP CONSTRAINT [FK69FBFBB7EE18CC3C]
ALTER TABLE [voc_homepage_summaries] DROP CONSTRAINT [FKBA3C05486A944441]
ALTER TABLE [core_reason_codes_tag] DROP CONSTRAINT [FKB6A508663EF30942]
ALTER TABLE [core_reason_codes_tag] DROP CONSTRAINT [FKB6A508665850A787]
ALTER TABLE [pts_put_tag] DROP CONSTRAINT [FKEF937D1A207FBD1E]
ALTER TABLE [pts_put_tag] DROP CONSTRAINT [FKEF937D1A3EF30942]
ALTER TABLE [sel_containers] DROP CONSTRAINT [FK_ContainerType_Containers]
ALTER TABLE [pts_container_details] DROP CONSTRAINT [FK_Container_ContainerDetails]
ALTER TABLE [pts_container_details] DROP CONSTRAINT [FK_Item_ContDetail]
ALTER TABLE [pts_container_details] DROP CONSTRAINT [FK_Operator_ContainerDetails]
ALTER TABLE [pts_container_details] DROP CONSTRAINT [FK_Region_ContDetail]
ALTER TABLE [pts_containers] DROP CONSTRAINT [FK_CustomerLoc_PtsContainers]
ALTER TABLE [voc_filter] DROP CONSTRAINT [FK6513688D16D0F6D2]
ALTER TABLE [voc_filter] DROP CONSTRAINT [FK6513688DF2F89D12]
ALTER TABLE [voc_summaries_extra_params] DROP CONSTRAINT [FK3097E545BC91011C]
ALTER TABLE [voc_chart_settings] DROP CONSTRAINT [FK_Chart_ChartSettings]
ALTER TABLE [voc_chart_settings] DROP CONSTRAINT [FKAB10C779C9E8E129]
ALTER TABLE [voc_report_parameter] DROP CONSTRAINT [FK7C7193372D37F7E]
ALTER TABLE [voc_report_parameter] DROP CONSTRAINT [FK7C71933E81FCA72]
ALTER TABLE [core_region_tag] DROP CONSTRAINT [FKB8B2786F15AC502A]
ALTER TABLE [core_region_tag] DROP CONSTRAINT [FKB8B2786F3EF30942]
ALTER TABLE [core_devices] DROP CONSTRAINT [FK_DeviceCommon_Device]
ALTER TABLE [core_devices] DROP CONSTRAINT [FK_Operator_Devices]
ALTER TABLE [pts_regions] DROP CONSTRAINT [FK3D5FB2AF630F1E6B]
ALTER TABLE [voc_homepage] DROP CONSTRAINT [FK7BE97BC3F2F89D12]
ALTER TABLE [arch_pts_license_labor] DROP CONSTRAINT [FK_ArchPtsLic_ArchPtsLicLabors]
ALTER TABLE [sel_del_loc_map_setting_tag] DROP CONSTRAINT [FK13B18B03EF30942]
ALTER TABLE [sel_del_loc_map_setting_tag] DROP CONSTRAINT [FK13B18B0F99B85CC]
ALTER TABLE [core_work_group_func_regions] DROP CONSTRAINT [FK_Regions_WorkgroupFuncs]
ALTER TABLE [core_work_group_func_regions] DROP CONSTRAINT [FK_WorkgroupFuncs_Regions]
ALTER TABLE [core_work_group_functions] DROP CONSTRAINT [FK_TFunctions_WGFunctions]
ALTER TABLE [core_work_group_functions] DROP CONSTRAINT [FK_WG_WGFunctions]
ALTER TABLE [putaway_license_details] DROP CONSTRAINT [FK_License_LicenseDetail]
ALTER TABLE [putaway_license_details] DROP CONSTRAINT [FK_Location_PutLocation]
ALTER TABLE [putaway_license_details] DROP CONSTRAINT [FK_Location_StartLocation]
ALTER TABLE [putaway_license_details] DROP CONSTRAINT [FK_Operator_LicenseDetail]
ALTER TABLE [putaway_license_details] DROP CONSTRAINT [FK_ReasonCode_LicenseDetail]
ALTER TABLE [sel_pick_import_data] DROP CONSTRAINT [FK_Assignment_Picks_Import]
ALTER TABLE [pts_put_details] DROP CONSTRAINT [FK_Operator_PutDetails]
ALTER TABLE [pts_put_details] DROP CONSTRAINT [FK_Put_PutDetails]
ALTER TABLE [replen_replenishments] DROP CONSTRAINT [FK_Item_Replenishment]
ALTER TABLE [replen_replenishments] DROP CONSTRAINT [FK_Location_FromLocation]
ALTER TABLE [replen_replenishments] DROP CONSTRAINT [FK_Location_ToLocation]
ALTER TABLE [replen_replenishments] DROP CONSTRAINT [FK_Operator_Replenishment]
ALTER TABLE [replen_replenishments] DROP CONSTRAINT [FK_Region_Replenishment]
ALTER TABLE [sel_assignment_labor] DROP CONSTRAINT [FK_Assignment_AssignLabors]
ALTER TABLE [sel_assignment_labor] DROP CONSTRAINT [FK_Operator_AssignLabors]
ALTER TABLE [sel_assignment_labor] DROP CONSTRAINT [FK_OperLabor_AssignLabors]
ALTER TABLE [core_operator_labor_tag] DROP CONSTRAINT [FKBEE7D9D03EF30942]
ALTER TABLE [core_operator_labor_tag] DROP CONSTRAINT [FKBEE7D9D0C853152]
ALTER TABLE [sel_pick_details] DROP CONSTRAINT [FK_Container_PickDetails]
ALTER TABLE [sel_pick_details] DROP CONSTRAINT [FK_Operator_PickDetails]
ALTER TABLE [sel_pick_details] DROP CONSTRAINT [FK_Pick_PickDetails]
ALTER TABLE [voc_feature] DROP CONSTRAINT [FK35EB80819FA773C]
ALTER TABLE [lineload_carton_tag] DROP CONSTRAINT [FK297726DF3EF30942]
ALTER TABLE [lineload_carton_tag] DROP CONSTRAINT [FK297726DF7DC0AB44]
ALTER TABLE [sel_regions] DROP CONSTRAINT [FK_ChaseProfile_Regions]
ALTER TABLE [sel_regions] DROP CONSTRAINT [FK_NormalProfile_Regions]
ALTER TABLE [sel_regions] DROP CONSTRAINT [FK_SumPromptChase_Regions]
ALTER TABLE [sel_regions] DROP CONSTRAINT [FK_SumPromptNormal_Regions]
ALTER TABLE [sel_regions] DROP CONSTRAINT [FK9DE1211A630F1E6B]
ALTER TABLE [voc_plugin_components] DROP CONSTRAINT [FK6657CF6D6B008C44]
ALTER TABLE [voc_plugin_components] DROP CONSTRAINT [FK6657CF6D8D0424EE]
ALTER TABLE [voc_user_to_role] DROP CONSTRAINT [FKE0E0509B4DCDD932]
ALTER TABLE [voc_user_to_role] DROP CONSTRAINT [FKE0E0509BF2F89D12]
ALTER TABLE [voc_site] DROP CONSTRAINT [FK7FFEBD9C7E140FE8]
ALTER TABLE [voc_site] DROP CONSTRAINT [FK7FFEBD9CB979502E]
ALTER TABLE [voc_site] DROP CONSTRAINT [FK7FFEBD9CD458CE41]
ALTER TABLE [voc_site] DROP CONSTRAINT [FK7FFEBD9CEA320F22]
ALTER TABLE [putaway_regions] DROP CONSTRAINT [FK2FD2083D630F1E6B]
ALTER TABLE [voc_notification_tag] DROP CONSTRAINT [FK4500111B3EF30942]
ALTER TABLE [voc_notification_tag] DROP CONSTRAINT [FK4500111B51FE0CF1]
ALTER TABLE [pts_customer_locations] DROP CONSTRAINT [FK_Location_CustomerLocation]
ALTER TABLE [core_location_items] DROP CONSTRAINT [FK_LocationItem_Item]
ALTER TABLE [core_location_items] DROP CONSTRAINT [FK_LocationItem_Location]
ALTER TABLE [lineload_pallet_tag] DROP CONSTRAINT [FK69FD15003EF30942]
ALTER TABLE [lineload_pallet_tag] DROP CONSTRAINT [FK69FD150093ECD1E5]
ALTER TABLE [core_operator_regions] DROP CONSTRAINT [FK_Regions_Operators]
ALTER TABLE [core_operator_regions] DROP CONSTRAINT [FKF36FA8045DEEFBCB]
ALTER TABLE [putaway_licenses] DROP CONSTRAINT [FK_Item_License]
ALTER TABLE [putaway_licenses] DROP CONSTRAINT [FK_Location_License]
ALTER TABLE [putaway_licenses] DROP CONSTRAINT [FK_Operator_License]
ALTER TABLE [putaway_licenses] DROP CONSTRAINT [FK_Region_License]
ALTER TABLE [sel_sum_prompt_def] DROP CONSTRAINT [FK_PromptItem_SummaryPromptDef]
ALTER TABLE [sel_sum_prompt_def] DROP CONSTRAINT [FK_SumPrompt_SumPromptDef]
ALTER TABLE [lineload_carton_detail_tag] DROP CONSTRAINT [FK93EA47C736817F35]
ALTER TABLE [lineload_carton_detail_tag] DROP CONSTRAINT [FK93EA47C73EF30942]
ALTER TABLE [lineload_carton_details] DROP CONSTRAINT [FK_carton_cartonDetail]
ALTER TABLE [lineload_carton_details] DROP CONSTRAINT [FK_Operator_cartonDetail]
ALTER TABLE [lineload_carton_details] DROP CONSTRAINT [FK_pallet_cartonDetail]
ALTER TABLE [core_location_tag] DROP CONSTRAINT [FK9F5100B03EF30942]
ALTER TABLE [core_location_tag] DROP CONSTRAINT [FK9F5100B076494EEB]
ALTER TABLE [lineload_route_stop_tag] DROP CONSTRAINT [FK133EEB183EF30942]
ALTER TABLE [lineload_route_stop_tag] DROP CONSTRAINT [FK133EEB1857ECE1E2]
ALTER TABLE [pts_licenses] DROP CONSTRAINT [FK_Operator_Put_Licenses]
ALTER TABLE [pts_licenses] DROP CONSTRAINT [FK_Region_Put_Licenses]
ALTER TABLE [pts_license_labor] DROP CONSTRAINT [FK_Operator_PtsLicenseLabors]
ALTER TABLE [pts_license_labor] DROP CONSTRAINT [FK_OperLabor_PtsLicenseLabors]
ALTER TABLE [pts_license_labor] DROP CONSTRAINT [FK_PtsLicense_PtsLicenseLabors]
ALTER TABLE [voc_filter_criterion] DROP CONSTRAINT [FK120AE56F1C17A2E2]
ALTER TABLE [voc_filter_criterion] DROP CONSTRAINT [FK120AE56F1D4606E2]
ALTER TABLE [voc_filter_criterion] DROP CONSTRAINT [FK120AE56F64367F32]
ALTER TABLE [voc_column] DROP CONSTRAINT [FK6049706B16D0F6D2]
ALTER TABLE [voc_column] DROP CONSTRAINT [FK6049706B971DA1E7]
ALTER TABLE [putaway_license_tag] DROP CONSTRAINT [FKA1266BDA3EF30942]
ALTER TABLE [putaway_license_tag] DROP CONSTRAINT [FKA1266BDA9556936D]
ALTER TABLE [lineload_cartons] DROP CONSTRAINT [FK_Operator_cartons]
ALTER TABLE [lineload_cartons] DROP CONSTRAINT [FK_Pallet_Carton]
ALTER TABLE [lineload_cartons] DROP CONSTRAINT [FK_RouteStop_Cartons]
ALTER TABLE [sel_assignment_tag] DROP CONSTRAINT [FKBDF179AD3EF30942]
ALTER TABLE [sel_assignment_tag] DROP CONSTRAINT [FKBDF179ADED83D64E]
ALTER TABLE [pts_puts] DROP CONSTRAINT [FK_Customer_Location_Puts]
ALTER TABLE [pts_puts] DROP CONSTRAINT [FK_Item_Puts]
ALTER TABLE [pts_puts] DROP CONSTRAINT [FK_License_Puts]
ALTER TABLE [pts_puts] DROP CONSTRAINT [FK_Operator_Puts]
ALTER TABLE [sel_region_profiles] DROP CONSTRAINT [FK_ContainerType_RegProfiles]
ALTER TABLE [sel_assignments] DROP CONSTRAINT [FK_Operator_Assignments]
ALTER TABLE [sel_assignments] DROP CONSTRAINT [FK_Region_Assignments]
ALTER TABLE [core_operators] DROP CONSTRAINT [FK_AssignedRegion_Operators]
ALTER TABLE [core_operators] DROP CONSTRAINT [FK_CurrentRegion_Operators]
ALTER TABLE [core_operators] DROP CONSTRAINT [FK_LastLocation_Operators]
ALTER TABLE [core_operators] DROP CONSTRAINT [FK_OperatorCommon_VLOperator]
ALTER TABLE [core_operators] DROP CONSTRAINT [FK_TaskFunction_Operators]
ALTER TABLE [core_operators] DROP CONSTRAINT [FK_Workgroup_Operators]
ALTER TABLE [arch_pts_puts] DROP CONSTRAINT [FK_Arch_PtsLic_Arch_Puts]
ALTER TABLE [voc_timewindow] DROP CONSTRAINT [FK2DB62B9216D0F6D2]
ALTER TABLE [voc_timewindow] DROP CONSTRAINT [FK2DB62B92F2F89D12]
ALTER TABLE [arch_sel_assignment_labor] DROP CONSTRAINT [FK_ArchAssign_AssignLabors]
ALTER TABLE [arch_sel_pick_details] DROP CONSTRAINT [FK_ArchContnr_ArchPickDtls]
ALTER TABLE [arch_sel_pick_details] DROP CONSTRAINT [FK_ArchPick_ArchPickDetails]
ALTER TABLE [arch_sel_picks] DROP CONSTRAINT [FK_Arch_Assignment_Arch_Picks]
ALTER TABLE [lineload_pallets] DROP CONSTRAINT [FK_RouteStop_Pallet]
ALTER TABLE [lineload_route_stops] DROP CONSTRAINT [FK_Region_RouteStops]
ALTER TABLE [core_operatorTeam_tag] DROP CONSTRAINT [FKC68E681C3EF30942]
ALTER TABLE [core_operatorTeam_tag] DROP CONSTRAINT [FKC68E681C6CAD2857]
ALTER TABLE [sel_del_loc_mappings_tag] DROP CONSTRAINT [FKDA7354D8158AFDC0]
ALTER TABLE [sel_del_loc_mappings_tag] DROP CONSTRAINT [FKDA7354D83EF30942]
ALTER TABLE [voc_report_tag] DROP CONSTRAINT [FK34BF94A43EF30942]
ALTER TABLE [voc_report_tag] DROP CONSTRAINT [FK34BF94A486A6A33A]
ALTER TABLE [voc_reports] DROP CONSTRAINT [FKB187574A96D14234]
ALTER TABLE [core_operator_labor] DROP CONSTRAINT [FK_BreakType_OperLabors]
ALTER TABLE [core_operator_labor] DROP CONSTRAINT [FK_Operator_OperLabors]
ALTER TABLE [core_operator_labor] DROP CONSTRAINT [FK_OperLabor_OperLabors]
ALTER TABLE [core_operator_labor] DROP CONSTRAINT [FK_Region_OperatorLabors]
ALTER TABLE [pts_puts_import_data] DROP CONSTRAINT [FK_License_Puts_Import]
ALTER TABLE [pts_license_tag] DROP CONSTRAINT [FK82158D4C365205D0]
ALTER TABLE [pts_license_tag] DROP CONSTRAINT [FK82158D4C3EF30942]
ALTER TABLE [sel_pick_tag] DROP CONSTRAINT [FK1B5B30C13EF30942]
ALTER TABLE [sel_pick_tag] DROP CONSTRAINT [FK1B5B30C1C4D491E2]
ALTER TABLE [sel_picks] DROP CONSTRAINT [FK_Assignment_Picks]
ALTER TABLE [sel_picks] DROP CONSTRAINT [FK_Item_Picks]
ALTER TABLE [sel_picks] DROP CONSTRAINT [FK_Location_Picks]
ALTER TABLE [sel_picks] DROP CONSTRAINT [FK_Operator_Picks]
ALTER TABLE [sel_picks] DROP CONSTRAINT [FK_OriginalPick_Picks]
DELETE FROM [voc_system_properties] WHERE [systemPropertyId]=1
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=1
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=2
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=3
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=4
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=5
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=6
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=7
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=8
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=9
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=10
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=11
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=12
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=13
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=14
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=15
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=16
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=17
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=18
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=19
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=20
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=21
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=22
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=23
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=24
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=25
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=26
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=27
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=28
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=29
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=30
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=31
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=32
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=33
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=34
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=35
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=36
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=37
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=38
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=39
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=40
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=41
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=42
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=43
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=44
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=45
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=46
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=47
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=48
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=49
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=50
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=51
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=52
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=53
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=54
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=55
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=56
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=57
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=58
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=59
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=60
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=61
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=62
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=63
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=64
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=65
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=66
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=67
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=68
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=69
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=70
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=71
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=72
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=73
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=74
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=75
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=76
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=77
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=78
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=79
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=80
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=81
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=82
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=83
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=84
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=85
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=86
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=87
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=88
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=89
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=90
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=91
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=92
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=93
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=94
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=95
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=96
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=97
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=98
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=99
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=100
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=101
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=102
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=103
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=104
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=105
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=106
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=107
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=108
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=109
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=110
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=111
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=112
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=113
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=114
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=115
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=116
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=117
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=118
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=119
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=120
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=121
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=122
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=123
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=124
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=125
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=126
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=127
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=128
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=129
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=130
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=131
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=132
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=133
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=134
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=135
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=136
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=137
DELETE FROM [voc_job_history] WHERE [jobHistoryId]=138
DELETE FROM [voc_jobs] WHERE [jobId]=1
DELETE FROM [voc_jobs] WHERE [jobId]=2
DELETE FROM [voc_jobs] WHERE [jobId]=3
DELETE FROM [voc_role_to_feature] WHERE [role_id]=-3 AND [feature_id]=-1207
DELETE FROM [voc_role_to_feature] WHERE [role_id]=-3 AND [feature_id]=-1117
DELETE FROM [voc_role_to_feature] WHERE [role_id]=-3 AND [feature_id]=-1116
DELETE FROM [voc_feature] WHERE [id]=-1208
DELETE FROM [voc_feature] WHERE [id]=-1207
DELETE FROM [voc_feature] WHERE [id]=-1117
DELETE FROM [voc_feature] WHERE [id]=-1116
UPDATE [voc_system_properties] SET [value]=N'1' WHERE [systemPropertyId]=-1005
UPDATE [voc_system_properties] SET [value]=N'../../epp/main/test/data' WHERE [systemPropertyId]=-100
UPDATE [voc_system_properties] SET [name]=N'reportType.DefaultRenderFormat' WHERE [systemPropertyId]=-17
UPDATE [voc_system_translations] SET [translation]=N'活动的' WHERE [code]=0 AND [keyValue]=N'com.vocollect.epp.eap.CredentialStatus' AND [locale]=N'zh_CN'
UPDATE [voc_system_translations] SET [translation]=N'En fonction du site' WHERE [code]=0 AND [keyValue]=N'com.vocollect.epp.eap.EAPAssociation' AND [locale]=N'fr'
UPDATE [voc_system_translations] SET [translation]=N'Com base na instalação' WHERE [code]=0 AND [keyValue]=N'com.vocollect.epp.eap.EAPAssociation' AND [locale]=N'pt_BR'
UPDATE [voc_system_translations] SET [translation]=N'站台架構' WHERE [code]=0 AND [keyValue]=N'com.vocollect.epp.eap.EAPAssociation' AND [locale]=N'zh_TW'
UPDATE [voc_system_translations] SET [translation]=N'活动的' WHERE [code]=1 AND [keyValue]=N'com.vocollect.epp.eap.CredentialStatus' AND [locale]=N'zh_CN'
UPDATE [voc_system_translations] SET [translation]=N'En fonction du terminal' WHERE [code]=1 AND [keyValue]=N'com.vocollect.epp.eap.EAPAssociation' AND [locale]=N'fr'
UPDATE [voc_system_translations] SET [translation]=N'En fonction de l’appareil' WHERE [code]=1 AND [keyValue]=N'com.vocollect.epp.eap.EAPAssociation' AND [locale]=N'fr_CA'
UPDATE [voc_system_translations] SET [translation]=N'Com base no dispositivo' WHERE [code]=1 AND [keyValue]=N'com.vocollect.epp.eap.EAPAssociation' AND [locale]=N'pt_BR'
UPDATE [voc_system_translations] SET [translation]=N'裝置架構' WHERE [code]=1 AND [keyValue]=N'com.vocollect.epp.eap.EAPAssociation' AND [locale]=N'zh_TW'
UPDATE [voc_system_translations] SET [translation]=N'Saisi' WHERE [code]=2 AND [keyValue]=N'com.vocollect.epp.eap.CredentialStatus' AND [locale]=N'fr_CA'
UPDATE [voc_system_translations] SET [translation]=N'入力' WHERE [code]=2 AND [keyValue]=N'com.vocollect.epp.eap.CredentialStatus' AND [locale]=N'ja'
UPDATE [voc_system_translations] SET [translation]=N'En fonction de l’opérateur' WHERE [code]=2 AND [keyValue]=N'com.vocollect.epp.eap.EAPAssociation' AND [locale]=N'fr'
UPDATE [voc_system_translations] SET [translation]=N'Com base no operador' WHERE [code]=2 AND [keyValue]=N'com.vocollect.epp.eap.EAPAssociation' AND [locale]=N'pt_BR'
UPDATE [voc_system_translations] SET [translation]=N'操作員架構' WHERE [code]=2 AND [keyValue]=N'com.vocollect.epp.eap.EAPAssociation' AND [locale]=N'zh_TW'
UPDATE [voc_system_translations] SET [translation]=N'Echec' WHERE [code]=2 AND [keyValue]=N'com.vocollect.epp.model.ScheduleLastResult' AND [locale]=N'fr'
UPDATE [voc_system_translations] SET [translation]=N'Incorrect' WHERE [code]=3 AND [keyValue]=N'com.vocollect.epp.eap.CredentialStatus' AND [locale]=N'fr_CA'
UPDATE [voc_system_translations] SET [translation]=N'무효' WHERE [code]=3 AND [keyValue]=N'com.vocollect.epp.eap.CredentialStatus' AND [locale]=N'ko'
UPDATE [voc_system_translations] SET [translation]=N'Non saisi' WHERE [code]=4 AND [keyValue]=N'com.vocollect.epp.eap.CredentialStatus' AND [locale]=N'fr_CA'
UPDATE [voc_system_translations] SET [translation]=N'停止' WHERE [code]=5 AND [keyValue]=N'com.vocollect.epp.model.ScheduleLastResult' AND [locale]=N'zh_CN'
UPDATE [voc_plugin_modules] SET [moduleVersion]=N'1.9', [frameworkVersion]=N'1.9' WHERE [moduleId]=1
UPDATE [voc_plugin_modules] SET [moduleVersion]=N'1.9', [frameworkVersion]=N'1.9' WHERE [moduleId]=2
UPDATE [voc_plugin_modules] SET [moduleVersion]=N'4.0', [frameworkVersion]=N'1.9' WHERE [moduleId]=101
UPDATE [voc_view] SET [name]=N'report' WHERE [id]=-1039
UPDATE [voc_operand] SET [label]=N'emptyDate', [js_input_function]=N'drawEmptyControl', [hql]=N'is null' WHERE [id]=-21
UPDATE [voc_column] SET [sortType]=8, [filterType]=2 WHERE [id]=-21125
UPDATE [voc_column] SET [sortType]=8, [filterType]=2 WHERE [id]=-21123
UPDATE [voc_column] SET [sortType]=8, [filterType]=2 WHERE [id]=-21120
UPDATE [voc_column] SET [sortType]=8, [filterType]=2 WHERE [id]=-21119
UPDATE [voc_column] SET [sortType]=0 WHERE [id]=-21103
UPDATE [voc_column] SET [field]=N'type.reportTypeNameKey', [display]=N'selection.report.view.column.type', [sortType]=1, [required]=0, [displayable]=1, [sort_asc]=1, [width]=110, [visible]=1, [column_order]=2, [operand_type]=0, [display_function]=N'displayReportTypeName' WHERE [id]=-13802
UPDATE [voc_column] SET [field]=N'name', [sortType]=1, [display_function]=NULL, [filterType]=2 WHERE [id]=-13801
UPDATE [voc_column] SET [display_function]=N'formatTime' WHERE [id]=-703
UPDATE [voc_column] SET [display_function]=N'formatTime' WHERE [id]=-702
UPDATE [voc_column] SET [display_function]=N'formatTime' WHERE [id]=-305
UPDATE [voc_column] SET [display_function]=N'formatTime' WHERE [id]=-304
SET IDENTITY_INSERT [voc_tag] ON;
INSERT INTO [voc_tag] ([id], [tag_type], [taggable_id]) VALUES (-927, 1, -927)
SET IDENTITY_INSERT [voc_tag] OFF;
SET IDENTITY_INSERT [voc_column] ON;
INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-22403, -1207, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-22402, -1207, N'operators', N'operatorTeam.view.column.operators', 0, 0, 0, 1, 1, 300, 1, 2, NULL, 0, N'displayOperators', NULL, N'displayOperators', NULL, NULL, NULL, 0, NULL, NULL, NULL)
INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-22401, -1207, N'name', N'operatorTeam.view.column.name', 1, 1, 0, 1, 1, 200, 1, 1, NULL, 0, NULL, NULL, N'displayOperatorTeam', NULL, NULL, NULL, 1, NULL, NULL, NULL)
INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-22303, -1206, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-22302, -1206, N'description', N'chart.view.column.description', 0, 1, 0, 1, 1, 300, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-22301, -1206, N'name', N'chart.view.column.name', 1, 1, 0, 1, 1, 200, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-13805, -1039, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL)
INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-13804, -1039, N'frequency', N'selection.report.view.column.frequency', 0, 5, 0, 1, 1, 110, 1, 4, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-13803, -1039, N'interval', N'selection.report.view.column.interval', 0, 3, 0, 1, 1, 110, 1, 3, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-10413, -1005, N'operatorTeams', N'operator.view.column.operatorTeams', 0, 0, 0, 1, 0, 0, 0, 12, NULL, 0, N'displayOperatorTeams', NULL, N'displayOperatorTeams', NULL, NULL, N'name', 1, NULL, NULL, NULL)
INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-406, -5, N'credentialAssociation', NULL, 0, 0, 1, 0, 0, 0, 0, 101, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL)
SET IDENTITY_INSERT [voc_column] OFF;
SET IDENTITY_INSERT [voc_operand] ON;
INSERT INTO [voc_operand] ([id], [label], [js_input_function], [type], [hql]) VALUES (-23, N'within', N'drawDropDownWithDates', 2, N'>= {0}')
SET IDENTITY_INSERT [voc_operand] OFF;
SET IDENTITY_INSERT [voc_feature] ON;
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1132, N'feature.voicelink.report.launch', NULL, 0, -1005)
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1131, N'feature.voicelink.report.schedule', NULL, 0, -1005)
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1130, N'feature.voicelink.report.create', NULL, 0, -1005)
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1129, N'feature.voicelink.report.edit', NULL, 0, -1005)
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1128, N'feature.voicelink.report.delete', NULL, 0, -1005)
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1127, N'feature.voicelink.report.view', NULL, 1, -1005)
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1126, N'feature.voicelink.operatorTeam.create', NULL, 0, -1005)
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1125, N'feature.voicelink.operatorTeam.edit', NULL, 0, -1005)
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1124, N'feature.voicelink.operatorTeam.delete', NULL, 0, -1005)
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1123, N'feature.voicelink.operatorTeam.view', NULL, 1, -1005)
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1122, N'feature.voicelink.chart.launchChart', NULL, 1, -1001)
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1121, N'feature.voicelink.chart.edit', NULL, 0, -1001)
INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1120, N'feature.voicelink.chart.view', NULL, 1, -1001)
SET IDENTITY_INSERT [voc_feature] OFF;
SET IDENTITY_INSERT [voc_charts] ON;
INSERT INTO [voc_charts] ([chartId], [version], [chartName], [chartType], [title], [description], [categoryAxisLabel], [rangeAxisLabel], [datasetProviderBean], [chartRendererBean]) VALUES (1, 1, N'Estimated Hours Remaining', 1, N'Estimated Man Hours Remaining', N'Displays an estimate of the man-hours remaining for each region', NULL, NULL, N'hoursRemainingChartDataProvider', N'standardBarChartRenderer')
INSERT INTO [voc_charts] ([chartId], [version], [chartName], [chartType], [title], [description], [categoryAxisLabel], [rangeAxisLabel], [datasetProviderBean], [chartRendererBean]) VALUES (2, 1, N'Items Picked/Remaining', 1, N'Items Picked/Remaining', N'Displays the items picked and items remaining for each region', NULL, NULL, N'casesChartDataProvider', N'standardBarChartRenderer')
INSERT INTO [voc_charts] ([chartId], [version], [chartName], [chartType], [title], [description], [categoryAxisLabel], [rangeAxisLabel], [datasetProviderBean], [chartRendererBean]) VALUES (3, 1, N'Route Completion', 1, N'Route Completion Percentage', N'Displays the completion percentages of routes that are in-progress', NULL, NULL, N'routeCompletionChartDataProvider', N'standardBarChartRenderer')
SET IDENTITY_INSERT [voc_charts] OFF;
SET IDENTITY_INSERT [voc_chart_settings] ON;
INSERT INTO [voc_chart_settings] ([chartSettingsId], [version], [chartId], [userId], [includeLegend], [includeLabels], [includeTooltips], [renderIn3D], [lowerBound], [upperBound], [barChartOrientation]) VALUES (1, 1, 1, NULL, 0, 1, 1, 1, 0, 100, 1)
INSERT INTO [voc_chart_settings] ([chartSettingsId], [version], [chartId], [userId], [includeLegend], [includeLabels], [includeTooltips], [renderIn3D], [lowerBound], [upperBound], [barChartOrientation]) VALUES (2, 1, 2, NULL, 0, 1, 1, 1, 0, 100, 1)
INSERT INTO [voc_chart_settings] ([chartSettingsId], [version], [chartId], [userId], [includeLegend], [includeLabels], [includeTooltips], [renderIn3D], [lowerBound], [upperBound], [barChartOrientation]) VALUES (3, 1, 3, NULL, 0, 1, 1, 1, 0, 100, 1)
SET IDENTITY_INSERT [voc_chart_settings] OFF;
SET IDENTITY_INSERT [voc_view] ON;
INSERT INTO [voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1207, N'operatorTeams', N'com.vocollect.voicelink.selection.model.OperatorTeam', NULL, NULL)
INSERT INTO [voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1206, N'charts', N'com.vocollect.voicelink.selection.model.Chart', NULL, NULL)
SET IDENTITY_INSERT [voc_view] OFF;
SET IDENTITY_INSERT [voc_reportType_parameter] ON;
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1503, -1500, N'CONTAINER_NUMBER', N'', N'defCheck', N'puttostore.report.container.number', 0, NULL, NULL, N'name', NULL, 1, 1, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1502, -1500, N'OPERATOR_TEAM_ID', N'operatorTeams', NULL, N'puttostore.report.container.operatorteam', 3, N'operatorTeamManager', N'all', N'name', N'id', 3, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1501, -1500, N'OPERATOR_ID', N'operators', N'', N'puttostore.report.container.operator', 3, N'operatorManager', N'all', N'reportOperatorIdentifier', N'operatorIdentifier', 2, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1402, -1400, N'OPERATOR_TEAM_ID', N'operatorTeams', NULL, N'puttostore.report.licenselabor.operatorteam', 3, N'operatorTeamManager', N'all', N'name', N'id', 2, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1401, -1400, N'OPERATOR_ID', N'operators', N'', N'puttostore.report.licenselabor.operator', 3, N'operatorManager', N'all', N'reportOperatorIdentifier', N'operatorIdentifier', 1, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1303, -1300, N'OPERATOR_TEAM_ID', N'operatorTeams', NULL, N'puttostore.report.license.operatorteam', 3, N'operatorTeamManager', N'all', N'name', N'id', 3, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1302, -1300, N'LICENSE_NUMBER', N'', NULL, N'puttostore.report.license.number', 0, NULL, NULL, N'name', NULL, 1, 0, N'50')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1301, -1300, N'OPERATOR_ID', N'operators', NULL, N'puttostore.report.license.operator', 3, N'operatorManager', N'all', N'reportOperatorIdentifier', N'operatorIdentifier', 2, 0, N'')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1206, -1200, N'REGION', NULL, N'', N'selection.report.regionprofiles.region', 1, N'operatorPerformanceReport', N'regions', N'reportName', N'id', 2, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1205, -1200, N'ACTION_TYPE', NULL, N'', N'selection.report.operatorPerformance.actionType', 1, N'operatorPerformanceReport', N'functions', N'label', N'value', 1, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1202, -1200, N'OPERATOR_COUNT', N'10', N'isNumeric', N'selection.report.operatorPerformance.count', 0, NULL, NULL, N'name', NULL, 3, 1, N'4')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1201, -1200, N'SORT_ORDER', N'1', N'', N'selection.report.operatorPerformance.sortOrder', 2, N'operatorPerformanceReport', N'sortOrders', N'label', N'value', 4, 0, N'1')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1101, -1100, N'REGION', NULL, N'', N'selection.report.regionprofiles.region', 1, N'selectionRegionManager', N'all', N'name', N'number', 1, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-1001, -1000, N'LOCATION_COUNT', N'10', N'isNumeric', N'selection.report.slotVisit.count', 0, NULL, NULL, N'name', NULL, 4, 1, N'4')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-901, -900, N'LOCATION_COUNT', N'10', N'isNumeric', N'selection.report.shortedLocations.count', 0, NULL, NULL, N'name', NULL, 4, 1, N'4')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-804, -800, N'OPERATOR_TEAM_ID', N'operatorTeams', NULL, N'selection.report.operatorbreak.operatorteam', 3, N'operatorTeamManager', N'all', N'name', N'id', 4, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-803, -800, N'MIN_DURATION', N'', N'isNumeric', N'selection.report.operatorbreak.minduration', 0, NULL, NULL, N'name', NULL, 2, 1, N'3')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-802, -800, N'BREAK_TYPE', NULL, N'', N'selection.report.operatorbreak.breaktype', 1, N'breakTypeManager', N'all', N'name', N'name', 1, 1, N'')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-801, -800, N'OPERATOR_ID', N'operators', N'', N'selection.report.operatorbreak.operator', 3, N'operatorManager', N'all', N'reportOperatorIdentifier', N'operatorIdentifier', 3, 1, N'')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-715, -701, N'SHIFT_LENGTH', N'8', NULL, N'selection.report.labordetail.shiftlength', 2, N'operatorLaborManager', N'shiftLengthHoursForReport', N'name', N'value', 3, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-714, -701, N'SHIFT_START', N'00:00', NULL, N'selection.report.labordetail.shiftstarttime', 2, N'operatorLaborManager', N'shiftStartHoursForReport', N'name', N'value', 2, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-711, -701, N'OPERATOR_ID', N'operators', NULL, N'selection.report.labordetail.operator', 2, N'operatorManager', N'all', N'reportOperatorIdentifier', N'operatorIdentifier', 1, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-707, -700, N'REGION', NULL, NULL, N'selection.report.laborsummary.region', 1, N'regionManager', N'selectionRegionsOrderByName', N'name', N'number', 4, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-706, -700, N'REPORT_TYPE', N'SELECTION_ONLY', NULL, N'selection.report.laborsummary.reporttype', 2, N'operatorLaborManager', N'actionTypesForReport', N'name', N'value', 3, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-705, -700, N'SHIFT_LENGTH', N'8', NULL, N'selection.report.laborsummary.shiftlength', 2, N'operatorLaborManager', N'shiftLengthHoursForReport', N'name', N'value', 2, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-704, -700, N'SHIFT_START', N'00:00', NULL, N'selection.report.laborsummary.shiftstarttime', 2, N'operatorLaborManager', N'shiftStartHoursForReport', N'name', N'value', 1, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-702, -700, N'OPERATOR_TEAM_ID', N'operatorTeams', NULL, N'selection.report.laborsummary.operatorteam', 3, N'operatorTeamManager', N'all', N'name', N'id', 6, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-701, -700, N'OPERATOR_ID', N'operators', NULL, N'selection.report.laborsummary.operator', 3, N'operatorManager', N'all', N'reportOperatorIdentifier', N'operatorIdentifier', 5, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-613, -601, N'OPERATOR_TEAM_ID', N'operatorTeams', NULL, N'puttostore.report.oepratorlabor.operatorteam', 3, N'operatorTeamManager', N'all', N'name', N'id', 3, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-611, -601, N'REGION_NUMBER', N'', NULL, N'puttostore.report.operatorlabor.region', 0, NULL, NULL, N'name', NULL, 1, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-610, -601, N'OPERATOR_ID', N'operators', NULL, N'puttostore.report.operatorlabor.operator', 3, N'operatorManager', N'all', N'reportOperatorIdentifier', N'operatorIdentifier', 2, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-501, -500, N'PALLET_NUMBER', N'', N'', N'selection.report.pallet_manifest.palletNumber', 0, NULL, NULL, N'name', NULL, 2, 0, N'50')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-402, -400, N'OPERATOR_TEAM_ID', N'operatorTeams', NULL, N'selection.report.assignmentlabor.operatorteam', 3, N'operatorTeamManager', N'all', N'name', N'id', 2, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-401, -400, N'OPERATOR_ID', N'operators', N'', N'selection.report.assignmentlabor.operator', 3, N'operatorManager', N'all', N'reportOperatorIdentifier', N'operatorIdentifier', 1, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-303, -300, N'OPERATOR_TEAM_ID', N'operatorTeams', NULL, N'selection.report.container.operatorteam', 3, N'operatorTeamManager', N'all', N'name', N'id', 3, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-302, -300, N'CONTAINER_NUMBER', N'', NULL, N'selection.report.container.number', 0, NULL, NULL, N'name', NULL, 1, 1, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-301, -300, N'OPERATOR_ID', N'operators', NULL, N'selection.report.container.operator', 3, N'operatorManager', N'all', N'reportOperatorIdentifier', N'operatorIdentifier', 2, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-206, -200, N'OPERATOR_TEAM_ID', N'operatorTeams', NULL, N'selection.report.assignment.operatorteam', 3, N'operatorTeamManager', N'all', N'name', N'id', 6, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-205, -200, N'ROUTE', N'', N'defCheck', N'selection.report.assignment.route', 0, NULL, NULL, N'name', NULL, 3, 0, N'50')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-204, -200, N'CUSTOMER_NUMBER', N'', N'defCheck', N'selection.report.assignment.customer', 0, NULL, NULL, N'name', NULL, 2, 0, N'50')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-203, -200, N'ASSIGNMENT_NUMBER', N'', N'isNumeric', N'selection.report.assignment.number', 0, NULL, NULL, N'name', NULL, 1, 0, N'18')
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-202, -200, N'OPERATOR_ID', N'operators', NULL, N'selection.report.assignment.operator', 3, N'operatorManager', N'all', N'reportOperatorIdentifier', N'operatorIdentifier', 5, 0, NULL)
INSERT INTO [voc_reportType_parameter] ([id], [reportType_id], [parameter_name], [default_value], [validation_function], [description], [field_type], [service_name], [service_method], [display_member], [data_member], [prompt_order], [is_required], [maxlength]) VALUES (-201, -200, N'WORK_ID', N'', N'defCheck', N'selection.report.assignment.workidentifier', 0, N'', N'', N'name', N'', 4, 0, N'50')
SET IDENTITY_INSERT [voc_reportType_parameter] OFF;
SET IDENTITY_INSERT [voc_reportType] ON;
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-1500, N'PutToStoreContainer', N'report.name.ptsContainers', N'Put To Store Containers report, grouped by containers and container details', N'voicelink.puttostore', N'ptsContainerReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-1400, N'PutToStoreLicenseLabor', N'report.name.ptsLicenseLabor', N'Put To Store license labor report of operators.', N'voicelink.puttostore', N'ptsLicenseLaborReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-1300, N'PtsLicense', N'report.name.ptsLicenses', N'License report grouped by licenses, containers, puts, and put details.', N'voicelink.puttostore', N'ptsLicenseReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-1200, N'OperatorPerformance', N'report.name.operatorPerformance', N'Operator Performance', N'voicelink.selection', N'operatorPerformanceReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-1100, N'RegionProfiles', N'report.name.regionProfiles', N'Region Configurations.', N'voicelink.selection', N'regionProfilesReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-1000, N'SlotVisit', N'report.name.slotVisit', N'Frequency of Slot Visit.', N'voicelink.selection', N'slotVisitReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-900, N'ShortedLocations', N'report.name.shortedLocations', N'Shorted Locations.', N'voicelink.selection', N'shortedLocationReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-800, N'OperatorBreaks', N'report.name.operatorBreaks', N'Break report of operators.', N'voicelink.selection', N'operatorBreakReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-701, N'LaborDetail', N'report.name.laborDetail', N'Labor detail report for selected operators.', N'voicelink.selection', N'laborDetailReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-700, N'LaborShiftSummaryReport', N'report.name.laborSummary', N'Labor summary report for selected operators.', N'voicelink.selection', N'laborShiftSummaryReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-601, N'PtsOperatorLabor', N'report.name.ptsOperatorLabor', N'Labor report of PTS operators.', N'voicelink.puttostore', N'ptsOperatorLaborReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-500, N'PalletManifest', N'report.name.palletManifest', N'Transactional report for line loading', N'task', N'sessionFactory')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-400, N'AssignmentLabor', N'report.name.selectionAssignmentLabor', N'Labor report of operators.', N'voicelink.selection', N'selectionAssignmentLaborReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-300, N'Container', N'report.name.selectionContainers', N'Containers report, grouped by containers, picks, and pick details', N'voicelink.selection', N'containerReport')
INSERT INTO [voc_reportType] ([id], [reportType_name], [reportType_name_key], [reportType_description], [app_name], [session_bean_id]) VALUES (-200, N'Assignment', N'report.name.selectionAssignments', N'Assignment report grouped by assignments, containers, picks, and pick details.', N'voicelink.selection', N'assignmentReport')
SET IDENTITY_INSERT [voc_reportType] OFF;
INSERT INTO [voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1132)
INSERT INTO [voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1127)
INSERT INTO [voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1123)
INSERT INTO [voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1122)
INSERT INTO [voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -1120)
ALTER TABLE [arch_pts_container_details] ADD CONSTRAINT [FK_ArchCont_ArchContDetail] FOREIGN KEY ([putContainerId]) REFERENCES [arch_pts_containers] ([containerId]) ON DELETE CASCADE;
ALTER TABLE [core_lots] ADD CONSTRAINT [FK_Item_Lot] FOREIGN KEY ([itemId]) REFERENCES [core_items] ([itemId]) ON DELETE CASCADE;
ALTER TABLE [core_lots] ADD CONSTRAINT [FK_Location_Lot] FOREIGN KEY ([locationId]) REFERENCES [core_locations] ([locationId]) ON DELETE CASCADE;
ALTER TABLE [voc_user_to_site] ADD CONSTRAINT [FKE0E0AF6C78E81B92] FOREIGN KEY ([site_id]) REFERENCES [voc_site] ([siteId]);
ALTER TABLE [voc_user_to_site] ADD CONSTRAINT [FKE0E0AF6CF2F89D12] FOREIGN KEY ([user_id]) REFERENCES [voc_user] ([id]);
ALTER TABLE [replen_regions] ADD CONSTRAINT [FKC056CF18630F1E6B] FOREIGN KEY ([regionId]) REFERENCES [core_regions] ([regionId]);
ALTER TABLE [voc_notificationdetail] ADD CONSTRAINT [FKCFDAA91BC355A75] FOREIGN KEY ([notificationDetailsId]) REFERENCES [voc_notification] ([id]);
ALTER TABLE [voc_operator_tag] ADD CONSTRAINT [FKC4CDDED43EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [voc_operator_tag] ADD CONSTRAINT [FKC4CDDED48BBBC9B5] FOREIGN KEY ([tagged_id]) REFERENCES [voc_operators] ([operatorId]);
ALTER TABLE [pts_container_tag] ADD CONSTRAINT [FK248A498C183A1D0] FOREIGN KEY ([tagged_id]) REFERENCES [pts_containers] ([containerId]);
ALTER TABLE [pts_container_tag] ADD CONSTRAINT [FK248A498C3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [voc_role_to_feature] ADD CONSTRAINT [FK17DCDF26241ED7C2] FOREIGN KEY ([feature_id]) REFERENCES [voc_feature] ([id]);
ALTER TABLE [voc_role_to_feature] ADD CONSTRAINT [FK17DCDF264DCDD932] FOREIGN KEY ([role_id]) REFERENCES [voc_role] ([id]);
ALTER TABLE [sel_assignment_containers] ADD CONSTRAINT [FK_Assignments_Containers] FOREIGN KEY ([assignmentId]) REFERENCES [sel_assignments] ([assignmentId]);
ALTER TABLE [sel_assignment_containers] ADD CONSTRAINT [FK_Containers_Assignments] FOREIGN KEY ([pickContainerId]) REFERENCES [sel_containers] ([containerId]);
ALTER TABLE [core_breaktype_tag] ADD CONSTRAINT [FK8F877A743EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [core_breaktype_tag] ADD CONSTRAINT [FK8F877A74D1BD43F] FOREIGN KEY ([tagged_id]) REFERENCES [core_break_types] ([breakTypeId]);
ALTER TABLE [voc_device_tag] ADD CONSTRAINT [FKF030FCC63EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [voc_device_tag] ADD CONSTRAINT [FKF030FCC6DB9C1FE7] FOREIGN KEY ([tagged_id]) REFERENCES [voc_device] ([id]);
ALTER TABLE [voc_user_column] ADD CONSTRAINT [FK4D5D625564367F32] FOREIGN KEY ([column_id]) REFERENCES [voc_column] ([id]);
ALTER TABLE [voc_user_column] ADD CONSTRAINT [FK4D5D6255F2F89D12] FOREIGN KEY ([user_id]) REFERENCES [voc_user] ([id]);
ALTER TABLE [lineload_regions] ADD CONSTRAINT [FK417D3F9A630F1E6B] FOREIGN KEY ([regionId]) REFERENCES [core_regions] ([regionId]);
ALTER TABLE [replen_replenishment_det_tag] ADD CONSTRAINT [FK9983AF0C3B598268] FOREIGN KEY ([tagged_id]) REFERENCES [replen_replenishment_details] ([detailId]);
ALTER TABLE [replen_replenishment_det_tag] ADD CONSTRAINT [FK9983AF0C3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [core_lot_tag] ADD CONSTRAINT [FK723AC90C38B884B7] FOREIGN KEY ([tagged_id]) REFERENCES [core_lots] ([lotId]);
ALTER TABLE [core_lot_tag] ADD CONSTRAINT [FK723AC90C3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [replen_replenishment_tag] ADD CONSTRAINT [FK2FD8BC383EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [replen_replenishment_tag] ADD CONSTRAINT [FK2FD8BC3878DBD0B7] FOREIGN KEY ([tagged_id]) REFERENCES [replen_replenishments] ([replenishId]);
ALTER TABLE [putaway_license_detail_tag] ADD CONSTRAINT [FK930D826C3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [putaway_license_detail_tag] ADD CONSTRAINT [FK930D826CA8E8E49E] FOREIGN KEY ([tagged_id]) REFERENCES [putaway_license_details] ([licenseDetailId]);
ALTER TABLE [core_location_item_tag] ADD CONSTRAINT [FKF0D0A5183EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [core_location_item_tag] ADD CONSTRAINT [FKF0D0A5186F29E79E] FOREIGN KEY ([tagged_id]) REFERENCES [core_location_items] ([mapId]);
ALTER TABLE [core_printer_tag] ADD CONSTRAINT [FKC5A2B155305E9D80] FOREIGN KEY ([tagged_id]) REFERENCES [core_printers] ([printerId]);
ALTER TABLE [core_printer_tag] ADD CONSTRAINT [FKC5A2B1553EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [replen_replenishment_details] ADD CONSTRAINT [FK_Location_ReplenishDetail] FOREIGN KEY ([replenishLocationId]) REFERENCES [core_locations] ([locationId]);
ALTER TABLE [replen_replenishment_details] ADD CONSTRAINT [FK_Operator_ReplenishDetail] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [replen_replenishment_details] ADD CONSTRAINT [FK_ReasonCode_ReplenishDetail] FOREIGN KEY ([reasonId]) REFERENCES [core_reason_codes] ([reasonCodeId]);
ALTER TABLE [replen_replenishment_details] ADD CONSTRAINT [FK_Replenish_ReplenishDetail] FOREIGN KEY ([replenishId]) REFERENCES [replen_replenishments] ([replenishId]) ON DELETE CASCADE;
ALTER TABLE [core_item_tag] ADD CONSTRAINT [FK8D1D4D0E3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [core_item_tag] ADD CONSTRAINT [FK8D1D4D0EC2569E49] FOREIGN KEY ([tagged_id]) REFERENCES [core_items] ([itemId]);
ALTER TABLE [arch_sel_containers] ADD CONSTRAINT [FK_Arch_Assignment_Arch_cont] FOREIGN KEY ([assignmentId]) REFERENCES [arch_sel_assignments] ([assignmentId]) ON DELETE CASCADE;
ALTER TABLE [voc_homepage_access_summaries] ADD CONSTRAINT [FK789FEEA56A944441] FOREIGN KEY ([id]) REFERENCES [voc_homepage] ([id]);
ALTER TABLE [voc_homepage_access_summaries] ADD CONSTRAINT [FK789FEEA5E5EF59C2] FOREIGN KEY ([summary_id]) REFERENCES [voc_summaries] ([id]);
ALTER TABLE [core_workgroup_tag] ADD CONSTRAINT [FKFAFE69493EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [core_workgroup_tag] ADD CONSTRAINT [FKFAFE6949BFAC0174] FOREIGN KEY ([tagged_id]) REFERENCES [core_work_groups] ([workgroupId]);
ALTER TABLE [voc_user_properties] ADD CONSTRAINT [FK6FE84B2C9E8E129] FOREIGN KEY ([userId]) REFERENCES [voc_user] ([id]);
ALTER TABLE [core_operators_operatorTeams] ADD CONSTRAINT [FK_Operators_OperatorTeams] FOREIGN KEY ([operatorTeamId]) REFERENCES [core_operator_teams] ([operatorTeamId]);
ALTER TABLE [core_operators_operatorTeams] ADD CONSTRAINT [FK_OperatorTeams_Operators] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [voc_reportType_parameter] ADD CONSTRAINT [FK853F878D58A22052] FOREIGN KEY ([reportType_id]) REFERENCES [voc_reportType] ([id]);
ALTER TABLE [arch_pts_put_details] ADD CONSTRAINT [FK_Archive_Put_PutDetails] FOREIGN KEY ([putId]) REFERENCES [arch_pts_puts] ([putId]) ON DELETE CASCADE;
ALTER TABLE [voc_user_tag] ADD CONSTRAINT [FK88FAA8FB3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [voc_user_tag] ADD CONSTRAINT [FK88FAA8FB853527D1] FOREIGN KEY ([tagged_id]) REFERENCES [voc_user] ([id]);
ALTER TABLE [pts_customer_location_tag] ADD CONSTRAINT [FK3C355813EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [pts_customer_location_tag] ADD CONSTRAINT [FK3C3558173697EE0] FOREIGN KEY ([tagged_id]) REFERENCES [pts_customer_locations] ([customerLocationId]);
ALTER TABLE [sel_sum_prompt_loc] ADD CONSTRAINT [FK_SumPromptDef_SumPromptLoc] FOREIGN KEY ([summaryDefintionId]) REFERENCES [sel_sum_prompt_def] ([summaryDefinitionId]) ON DELETE CASCADE;
ALTER TABLE [sel_sum_prompt_tag] ADD CONSTRAINT [FKDF7B73383EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [sel_sum_prompt_tag] ADD CONSTRAINT [FKDF7B7338E9E659E5] FOREIGN KEY ([tagged_id]) REFERENCES [sel_sum_prompt] ([summaryId]);
ALTER TABLE [sel_container_tag] ADD CONSTRAINT [FK69FBFBB73EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
;ALTER TABLE [sel_container_tag] ADD CONSTRAINT [FK69FBFBB7EE18CC3C] FOREIGN KEY ([tagged_id]) REFERENCES [sel_containers] ([containerId]);
ALTER TABLE [voc_homepage_summaries] ADD CONSTRAINT [FKBA3C05486A944441] FOREIGN KEY ([id]) REFERENCES [voc_homepage] ([id]);
ALTER TABLE [core_reason_codes_tag] ADD CONSTRAINT [FKB6A508663EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [core_reason_codes_tag] ADD CONSTRAINT [FKB6A508665850A787] FOREIGN KEY ([tagged_id]) REFERENCES [core_reason_codes] ([reasonCodeId]);
ALTER TABLE [pts_put_tag] ADD CONSTRAINT [FKEF937D1A207FBD1E] FOREIGN KEY ([tagged_id]) REFERENCES [pts_puts] ([putId]);
ALTER TABLE [pts_put_tag] ADD CONSTRAINT [FKEF937D1A3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [sel_containers] ADD CONSTRAINT [FK_ContainerType_Containers] FOREIGN KEY ([containerTypeId]) REFERENCES [sel_container_types] ([containerTypeId]);
ALTER TABLE [pts_container_details] ADD CONSTRAINT [FK_Container_ContainerDetails] FOREIGN KEY ([putContainerId]) REFERENCES [pts_containers] ([containerId]) ON DELETE CASCADE;
ALTER TABLE [pts_container_details] ADD CONSTRAINT [FK_Item_ContDetail] FOREIGN KEY ([itemId]) REFERENCES [core_items] ([itemId]);
ALTER TABLE [pts_container_details] ADD CONSTRAINT [FK_Operator_ContainerDetails] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [pts_container_details] ADD CONSTRAINT [FK_Region_ContDetail] FOREIGN KEY ([regionId]) REFERENCES [pts_regions] ([regionId]);
ALTER TABLE [pts_containers] ADD CONSTRAINT [FK_CustomerLoc_PtsContainers] FOREIGN KEY ([customerLocationId]) REFERENCES [pts_customer_locations] ([customerLocationId]);
ALTER TABLE [voc_filter] ADD CONSTRAINT [FK6513688D16D0F6D2] FOREIGN KEY ([view_id]) REFERENCES [voc_view] ([id]);
ALTER TABLE [voc_filter] ADD CONSTRAINT [FK6513688DF2F89D12] FOREIGN KEY ([user_id]) REFERENCES [voc_user] ([id]);
ALTER TABLE [voc_summaries_extra_params] ADD CONSTRAINT [FK3097E545BC91011C] FOREIGN KEY ([id]) REFERENCES [voc_summaries] ([id]);
ALTER TABLE [voc_chart_settings] ADD CONSTRAINT [FK_Chart_ChartSettings] FOREIGN KEY ([chartId]) REFERENCES [voc_charts] ([chartId]);
ALTER TABLE [voc_chart_settings] ADD CONSTRAINT [FKAB10C779C9E8E129] FOREIGN KEY ([userId]) REFERENCES [voc_user] ([id]);
ALTER TABLE [voc_report_parameter] ADD CONSTRAINT [FK7C7193372D37F7E] FOREIGN KEY ([reportTypeParameter]) REFERENCES [voc_reportType_parameter] ([id]);
ALTER TABLE [voc_report_parameter] ADD CONSTRAINT [FK7C71933E81FCA72] FOREIGN KEY ([report_id]) REFERENCES [voc_reports] ([reportId]);
ALTER TABLE [core_region_tag] ADD CONSTRAINT [FKB8B2786F15AC502A] FOREIGN KEY ([tagged_id]) REFERENCES [core_regions] ([regionId]);
ALTER TABLE [core_region_tag] ADD CONSTRAINT [FKB8B2786F3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [core_devices] ADD CONSTRAINT [FK_DeviceCommon_Device] FOREIGN KEY ([DeviceId]) REFERENCES [voc_device] ([id]);
ALTER TABLE [core_devices] ADD CONSTRAINT [FK_Operator_Devices] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [pts_regions] ADD CONSTRAINT [FK3D5FB2AF630F1E6B] FOREIGN KEY ([regionId]) REFERENCES [core_regions] ([regionId]);
ALTER TABLE [voc_homepage] ADD CONSTRAINT [FK7BE97BC3F2F89D12] FOREIGN KEY ([user_id]) REFERENCES [voc_user] ([id]);
ALTER TABLE [arch_pts_license_labor] ADD CONSTRAINT [FK_ArchPtsLic_ArchPtsLicLabors] FOREIGN KEY ([licenseId]) REFERENCES [arch_pts_licenses] ([licenseId]) ON DELETE CASCADE;
ALTER TABLE [sel_del_loc_map_setting_tag] ADD CONSTRAINT [FK13B18B03EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [sel_del_loc_map_setting_tag] ADD CONSTRAINT [FK13B18B0F99B85CC] FOREIGN KEY ([tagged_id]) REFERENCES [sel_del_loc_mapping_settings] ([delLocMappingSettingId]);
ALTER TABLE [core_work_group_func_regions] ADD CONSTRAINT [FK_Regions_WorkgroupFuncs] FOREIGN KEY ([regionId]) REFERENCES [core_regions] ([regionId]);
ALTER TABLE [core_work_group_func_regions] ADD CONSTRAINT [FK_WorkgroupFuncs_Regions] FOREIGN KEY ([workgroupFunctionId]) REFERENCES [core_work_group_functions] ([workgroupFunctionId]);
ALTER TABLE [core_work_group_functions] ADD CONSTRAINT [FK_TFunctions_WGFunctions] FOREIGN KEY ([taskFunctionId]) REFERENCES [core_task_functions] ([taskFunctionId]);
ALTER TABLE [core_work_group_functions] ADD CONSTRAINT [FK_WG_WGFunctions] FOREIGN KEY ([workgroupId]) REFERENCES [core_work_groups] ([workgroupId]);
ALTER TABLE [putaway_license_details] ADD CONSTRAINT [FK_License_LicenseDetail] FOREIGN KEY ([licenseId]) REFERENCES [putaway_licenses] ([licenseId]) ON DELETE CASCADE;
;ALTER TABLE [putaway_license_details] ADD CONSTRAINT [FK_Location_PutLocation] FOREIGN KEY ([putLocationId]) REFERENCES [core_locations] ([locationId]);
ALTER TABLE [putaway_license_details] ADD CONSTRAINT [FK_Location_StartLocation] FOREIGN KEY ([startLocationId]) REFERENCES [core_locations] ([locationId]);
ALTER TABLE [putaway_license_details] ADD CONSTRAINT [FK_Operator_LicenseDetail] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [putaway_license_details] ADD CONSTRAINT [FK_ReasonCode_LicenseDetail] FOREIGN KEY ([reasonId]) REFERENCES [core_reason_codes] ([reasonCodeId]);
ALTER TABLE [sel_pick_import_data] ADD CONSTRAINT [FK_Assignment_Picks_Import] FOREIGN KEY ([assignmentId]) REFERENCES [sel_assignment_import_data] ([importID]) ON DELETE CASCADE;
ALTER TABLE [pts_put_details] ADD CONSTRAINT [FK_Operator_PutDetails] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [pts_put_details] ADD CONSTRAINT [FK_Put_PutDetails] FOREIGN KEY ([putId]) REFERENCES [pts_puts] ([putId]) ON DELETE CASCADE;
ALTER TABLE [replen_replenishments] ADD CONSTRAINT [FK_Item_Replenishment] FOREIGN KEY ([itemId]) REFERENCES [core_items] ([itemId]);
ALTER TABLE [replen_replenishments] ADD CONSTRAINT [FK_Location_FromLocation] FOREIGN KEY ([fromLocationId]) REFERENCES [core_locations] ([locationId]);
ALTER TABLE [replen_replenishments] ADD CONSTRAINT [FK_Location_ToLocation] FOREIGN KEY ([toLocationId]) REFERENCES [core_locations] ([locationId]);
ALTER TABLE [replen_replenishments] ADD CONSTRAINT [FK_Operator_Replenishment] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [replen_replenishments] ADD CONSTRAINT [FK_Region_Replenishment] FOREIGN KEY ([regionId]) REFERENCES [replen_regions] ([regionId]);
ALTER TABLE [sel_assignment_labor] ADD CONSTRAINT [FK_Assignment_AssignLabors] FOREIGN KEY ([assignmentId]) REFERENCES [sel_assignments] ([assignmentId]) ON DELETE CASCADE;
ALTER TABLE [sel_assignment_labor] ADD CONSTRAINT [FK_Operator_AssignLabors] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [sel_assignment_labor] ADD CONSTRAINT [FK_OperLabor_AssignLabors] FOREIGN KEY ([operatorLaborId]) REFERENCES [core_operator_labor] ([operatorLaborId]);
ALTER TABLE [core_operator_labor_tag] ADD CONSTRAINT [FKBEE7D9D03EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [core_operator_labor_tag] ADD CONSTRAINT [FKBEE7D9D0C853152] FOREIGN KEY ([tagged_id]) REFERENCES [core_operator_labor] ([operatorLaborId]);
ALTER TABLE [sel_pick_details] ADD CONSTRAINT [FK_Container_PickDetails] FOREIGN KEY ([pickContainerId]) REFERENCES [sel_containers] ([containerId]);
ALTER TABLE [sel_pick_details] ADD CONSTRAINT [FK_Operator_PickDetails] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [sel_pick_details] ADD CONSTRAINT [FK_Pick_PickDetails] FOREIGN KEY ([pickId]) REFERENCES [sel_picks] ([pickId]) ON DELETE CASCADE;
ALTER TABLE [voc_feature] ADD CONSTRAINT [FK35EB80819FA773C] FOREIGN KEY ([group_id]) REFERENCES [voc_feature_group] ([id]);
ALTER TABLE [lineload_carton_tag] ADD CONSTRAINT [FK297726DF3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [lineload_carton_tag] ADD CONSTRAINT [FK297726DF7DC0AB44] FOREIGN KEY ([tagged_id]) REFERENCES [lineload_cartons] ([cartonId]);
ALTER TABLE [sel_regions] ADD CONSTRAINT [FK_ChaseProfile_Regions] FOREIGN KEY ([profileChaseAssignmentID]) REFERENCES [sel_region_profiles] ([regionProfileId]);
ALTER TABLE [sel_regions] ADD CONSTRAINT [FK_NormalProfile_Regions] FOREIGN KEY ([profileNormalAssignmentId]) REFERENCES [sel_region_profiles] ([regionProfileId]);
ALTER TABLE [sel_regions] ADD CONSTRAINT [FK_SumPromptChase_Regions] FOREIGN KEY ([summaryId_chase]) REFERENCES [sel_sum_prompt] ([summaryId]);
ALTER TABLE [sel_regions] ADD CONSTRAINT [FK_SumPromptNormal_Regions] FOREIGN KEY ([summaryId]) REFERENCES [sel_sum_prompt] ([summaryId]);
ALTER TABLE [sel_regions] ADD CONSTRAINT [FK9DE1211A630F1E6B] FOREIGN KEY ([regionId]) REFERENCES [core_regions] ([regionId]);
ALTER TABLE [voc_plugin_components] ADD CONSTRAINT [FK6657CF6D6B008C44] FOREIGN KEY ([parentComponentId]) REFERENCES [voc_plugin_components] ([componentId]);
ALTER TABLE [voc_plugin_components] ADD CONSTRAINT [FK6657CF6D8D0424EE] FOREIGN KEY ([moduleId]) REFERENCES [voc_plugin_modules] ([moduleId]);
ALTER TABLE [voc_user_to_role] ADD CONSTRAINT [FKE0E0509B4DCDD932] FOREIGN KEY ([role_id]) REFERENCES [voc_role] ([id]);
ALTER TABLE [voc_user_to_role] ADD CONSTRAINT [FKE0E0509BF2F89D12] FOREIGN KEY ([user_id]) REFERENCES [voc_user] ([id]);
ALTER TABLE [voc_site] ADD CONSTRAINT [FK7FFEBD9C7E140FE8] FOREIGN KEY ([ldapconfig_id]) REFERENCES [voc_ldapconfiguration] ([ldapConfigurationId]);
ALTER TABLE [voc_site] ADD CONSTRAINT [FK7FFEBD9CB979502E] FOREIGN KEY ([servercredentials_id]) REFERENCES [voc_networkcredentials] ([actionItemId]);
ALTER TABLE [voc_site] ADD CONSTRAINT [FK7FFEBD9CD458CE41] FOREIGN KEY ([restricteduser_id]) REFERENCES [voc_networkcredentials] ([actionItemId]);
ALTER TABLE [voc_site] ADD CONSTRAINT [FK7FFEBD9CEA320F22] FOREIGN KEY ([sitewideuser_id]) REFERENCES [voc_networkcredentials] ([actionItemId]);
ALTER TABLE [putaway_regions] ADD CONSTRAINT [FK2FD2083D630F1E6B] FOREIGN KEY ([regionId]) REFERENCES [core_regions] ([regionId]);
ALTER TABLE [voc_notification_tag] ADD CONSTRAINT [FK4500111B3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [voc_notification_tag] ADD CONSTRAINT [FK4500111B51FE0CF1] FOREIGN KEY ([tagged_id]) REFERENCES [voc_notification] ([id]);
ALTER TABLE [pts_customer_locations] ADD CONSTRAINT [FK_Location_CustomerLocation] FOREIGN KEY ([locationId]) REFERENCES [core_locations] ([locationId]);
ALTER TABLE [core_location_items] ADD CONSTRAINT [FK_LocationItem_Item] FOREIGN KEY ([itemId]) REFERENCES [core_items] ([itemId]) ON DELETE CASCADE;
ALTER TABLE [core_location_items] ADD CONSTRAINT [FK_LocationItem_Location] FOREIGN KEY ([locationId]) REFERENCES [core_locations] ([locationId]) ON DELETE CASCADE;
ALTER TABLE [lineload_pallet_tag] ADD CONSTRAINT [FK69FD15003EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [lineload_pallet_tag] ADD CONSTRAINT [FK69FD150093ECD1E5] FOREIGN KEY ([tagged_id]) REFERENCES [lineload_pallets] ([palletId]);
ALTER TABLE [core_operator_regions] ADD CONSTRAINT [FK_Regions_Operators] FOREIGN KEY ([regionId]) REFERENCES [core_regions] ([regionId]);
ALTER TABLE [core_operator_regions] ADD CONSTRAINT [FKF36FA8045DEEFBCB] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [putaway_licenses] ADD CONSTRAINT [FK_Item_License] FOREIGN KEY ([itemId]) REFERENCES [core_items] ([itemId]);
ALTER TABLE [putaway_licenses] ADD CONSTRAINT [FK_Location_License] FOREIGN KEY ([locationId]) REFERENCES [core_locations] ([locationId]);
ALTER TABLE [putaway_licenses] ADD CONSTRAINT [FK_Operator_License] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [putaway_licenses] ADD CONSTRAINT [FK_Region_License] FOREIGN KEY ([regionId]) REFERENCES [putaway_regions] ([regionId]);
ALTER TABLE [sel_sum_prompt_def] ADD CONSTRAINT [FK_PromptItem_SummaryPromptDef] FOREIGN KEY ([promptItemId]) REFERENCES [sel_sum_prompt_item] ([promptItemId]);
ALTER TABLE [sel_sum_prompt_def] ADD CONSTRAINT [FK_SumPrompt_SumPromptDef] FOREIGN KEY ([summaryId]) REFERENCES [sel_sum_prompt] ([summaryId]) ON DELETE CASCADE;
ALTER TABLE [lineload_carton_detail_tag] ADD CONSTRAINT [FK93EA47C736817F35] FOREIGN KEY ([tagged_id]) REFERENCES [lineload_carton_details] ([cartonDetailId]);
ALTER TABLE [lineload_carton_detail_tag] ADD CONSTRAINT [FK93EA47C73EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [lineload_carton_details] ADD CONSTRAINT [FK_carton_cartonDetail] FOREIGN KEY ([cartonId]) REFERENCES [lineload_cartons] ([cartonId]);
ALTER TABLE [lineload_carton_details] ADD CONSTRAINT [FK_Operator_cartonDetail] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [lineload_carton_details] ADD CONSTRAINT [FK_pallet_cartonDetail] FOREIGN KEY ([palletId]) REFERENCES [lineload_pallets] ([palletId]);
ALTER TABLE [core_location_tag] ADD CONSTRAINT [FK9F5100B03EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [core_location_tag] ADD CONSTRAINT [FK9F5100B076494EEB] FOREIGN KEY ([tagged_id]) REFERENCES [core_locations] ([locationId]);
ALTER TABLE [lineload_route_stop_tag] ADD CONSTRAINT [FK133EEB183EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [lineload_route_stop_tag] ADD CONSTRAINT [FK133EEB1857ECE1E2] FOREIGN KEY ([tagged_id]) REFERENCES [lineload_route_stops] ([routeStopId]);
ALTER TABLE [pts_licenses] ADD CONSTRAINT [FK_Operator_Put_Licenses] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [pts_licenses] ADD CONSTRAINT [FK_Region_Put_Licenses] FOREIGN KEY ([regionId]) REFERENCES [pts_regions] ([regionId]);
ALTER TABLE [pts_license_labor] ADD CONSTRAINT [FK_Operator_PtsLicenseLabors] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [pts_license_labor] ADD CONSTRAINT [FK_OperLabor_PtsLicenseLabors] FOREIGN KEY ([operatorLaborId]) REFERENCES [core_operator_labor] ([operatorLaborId]);
ALTER TABLE [pts_license_labor] ADD CONSTRAINT [FK_PtsLicense_PtsLicenseLabors] FOREIGN KEY ([licenseId]) REFERENCES [pts_licenses] ([licenseId]) ON DELETE CASCADE;
ALTER TABLE [voc_filter_criterion] ADD CONSTRAINT [FK120AE56F1C17A2E2] FOREIGN KEY ([operand_id]) REFERENCES [voc_operand] ([id]);
ALTER TABLE [voc_filter_criterion] ADD CONSTRAINT [FK120AE56F1D4606E2] FOREIGN KEY ([filterCriteriaId]) REFERENCES [voc_filter] ([id]);
ALTER TABLE [voc_filter_criterion] ADD CONSTRAINT [FK120AE56F64367F32] FOREIGN KEY ([column_id]) REFERENCES [voc_column] ([id]);
ALTER TABLE [voc_column] ADD CONSTRAINT [FK6049706B16D0F6D2] FOREIGN KEY ([view_id]) REFERENCES [voc_view] ([id]);
ALTER TABLE [voc_column] ADD CONSTRAINT [FK6049706B971DA1E7] FOREIGN KEY ([data_type_id]) REFERENCES [voc_data_type] ([id]);
ALTER TABLE [putaway_license_tag] ADD CONSTRAINT [FKA1266BDA3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [putaway_license_tag] ADD CONSTRAINT [FKA1266BDA9556936D] FOREIGN KEY ([tagged_id]) REFERENCES [putaway_licenses] ([licenseId]);
ALTER TABLE [lineload_cartons] ADD CONSTRAINT [FK_Operator_cartons] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [lineload_cartons] ADD CONSTRAINT [FK_Pallet_Carton] FOREIGN KEY ([palletId]) REFERENCES [lineload_pallets] ([palletId]);
ALTER TABLE [lineload_cartons] ADD CONSTRAINT [FK_RouteStop_Cartons] FOREIGN KEY ([routeStopId]) REFERENCES [lineload_route_stops] ([routeStopId]);
ALTER TABLE [sel_assignment_tag] ADD CONSTRAINT [FKBDF179AD3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [sel_assignment_tag] ADD CONSTRAINT [FKBDF179ADED83D64E] FOREIGN KEY ([tagged_id]) REFERENCES [sel_assignments] ([assignmentId]);
ALTER TABLE [pts_puts] ADD CONSTRAINT [FK_Customer_Location_Puts] FOREIGN KEY ([customerLocationId]) REFERENCES [pts_customer_locations] ([customerLocationId]);
ALTER TABLE [pts_puts] ADD CONSTRAINT [FK_Item_Puts] FOREIGN KEY ([itemId]) REFERENCES [core_items] ([itemId]);
ALTER TABLE [pts_puts] ADD CONSTRAINT [FK_License_Puts] FOREIGN KEY ([licenseId]) REFERENCES [pts_licenses] ([licenseId]) ON DELETE CASCADE;
ALTER TABLE [pts_puts] ADD CONSTRAINT [FK_Operator_Puts] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [sel_region_profiles] ADD CONSTRAINT [FK_ContainerType_RegProfiles] FOREIGN KEY ([containerTypeId]) REFERENCES [sel_container_types] ([containerTypeId]);
ALTER TABLE [sel_assignments] ADD CONSTRAINT [FK_Operator_Assignments] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [sel_assignments] ADD CONSTRAINT [FK_Region_Assignments] FOREIGN KEY ([regionId]) REFERENCES [sel_regions] ([regionId]);
ALTER TABLE [core_operators] ADD CONSTRAINT [FK_AssignedRegion_Operators] FOREIGN KEY ([assignedRegionId]) REFERENCES [core_regions] ([regionId]);
ALTER TABLE [core_operators] ADD CONSTRAINT [FK_CurrentRegion_Operators] FOREIGN KEY ([currentRegionId]) REFERENCES [core_regions] ([regionId]);
ALTER TABLE [core_operators] ADD CONSTRAINT [FK_LastLocation_Operators] FOREIGN KEY ([lastLocationId]) REFERENCES [core_locations] ([locationId]);
ALTER TABLE [core_operators] ADD CONSTRAINT [FK_OperatorCommon_VLOperator] FOREIGN KEY ([operatorId]) REFERENCES [voc_operators] ([operatorId]);
ALTER TABLE [core_operators] ADD CONSTRAINT [FK_TaskFunction_Operators] FOREIGN KEY ([taskFunctionId]) REFERENCES [core_task_functions] ([taskFunctionId]);
ALTER TABLE [core_operators] ADD CONSTRAINT [FK_Workgroup_Operators] FOREIGN KEY ([workgroupId]) REFERENCES [core_work_groups] ([workgroupId]);
ALTER TABLE [arch_pts_puts] ADD CONSTRAINT [FK_Arch_PtsLic_Arch_Puts] FOREIGN KEY ([licenseId]) REFERENCES [arch_pts_licenses] ([licenseId]) ON DELETE CASCADE;
ALTER TABLE [voc_timewindow] ADD CONSTRAINT [FK2DB62B9216D0F6D2] FOREIGN KEY ([view_id]) REFERENCES [voc_view] ([id]);
ALTER TABLE [voc_timewindow] ADD CONSTRAINT [FK2DB62B92F2F89D12] FOREIGN KEY ([user_id]) REFERENCES [voc_user] ([id]);
ALTER TABLE [arch_sel_assignment_labor] ADD CONSTRAINT [FK_ArchAssign_AssignLabors] FOREIGN KEY ([assignmentId]) REFERENCES [arch_sel_assignments] ([assignmentId]) ON DELETE CASCADE;
ALTER TABLE [arch_sel_pick_details] ADD CONSTRAINT [FK_ArchContnr_ArchPickDtls] FOREIGN KEY ([pickContainerId]) REFERENCES [arch_sel_containers] ([containerId]);
ALTER TABLE [arch_sel_pick_details] ADD CONSTRAINT [FK_ArchPick_ArchPickDetails] FOREIGN KEY ([pickId]) REFERENCES [arch_sel_picks] ([pickId]) ON DELETE CASCADE;
ALTER TABLE [arch_sel_picks] ADD CONSTRAINT [FK_Arch_Assignment_Arch_Picks] FOREIGN KEY ([assignmentId]) REFERENCES [arch_sel_assignments] ([assignmentId]) ON DELETE CASCADE;
ALTER TABLE [lineload_pallets] ADD CONSTRAINT [FK_RouteStop_Pallet] FOREIGN KEY ([routeStopId]) REFERENCES [lineload_route_stops] ([routeStopId]);
ALTER TABLE [lineload_route_stops] ADD CONSTRAINT [FK_Region_RouteStops] FOREIGN KEY ([regionId]) REFERENCES [lineload_regions] ([regionId]);
ALTER TABLE [core_operatorTeam_tag] ADD CONSTRAINT [FKC68E681C3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [core_operatorTeam_tag] ADD CONSTRAINT [FKC68E681C6CAD2857] FOREIGN KEY ([tagged_id]) REFERENCES [core_operator_teams] ([operatorTeamId]);
ALTER TABLE [sel_del_loc_mappings_tag] ADD CONSTRAINT [FKDA7354D8158AFDC0] FOREIGN KEY ([tagged_id]) REFERENCES [sel_del_loc_mappings] ([delLocMappingId]);
ALTER TABLE [sel_del_loc_mappings_tag] ADD CONSTRAINT [FKDA7354D83EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [voc_report_tag] ADD CONSTRAINT [FK34BF94A43EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [voc_report_tag] ADD CONSTRAINT [FK34BF94A486A6A33A] FOREIGN KEY ([tagged_id]) REFERENCES [voc_reports] ([reportId]);
ALTER TABLE [voc_reports] ADD CONSTRAINT [FKB187574A96D14234] FOREIGN KEY ([reportType]) REFERENCES [voc_reportType] ([id]);
ALTER TABLE [core_operator_labor] ADD CONSTRAINT [FK_BreakType_OperLabors] FOREIGN KEY ([breakId]) REFERENCES [core_break_types] ([breakTypeId]);
ALTER TABLE [core_operator_labor] ADD CONSTRAINT [FK_Operator_OperLabors] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [core_operator_labor] ADD CONSTRAINT [FK_OperLabor_OperLabors] FOREIGN KEY ([previousOperatorLaborId]) REFERENCES [core_operator_labor] ([operatorLaborId]);
ALTER TABLE [core_operator_labor] ADD CONSTRAINT [FK_Region_OperatorLabors] FOREIGN KEY ([regionId]) REFERENCES [core_regions] ([regionId]);
ALTER TABLE [pts_puts_import_data] ADD CONSTRAINT [FK_License_Puts_Import] FOREIGN KEY ([licenseId]) REFERENCES [pts_licenses_import_data] ([importID]) ON DELETE CASCADE;
ALTER TABLE [pts_license_tag] ADD CONSTRAINT [FK82158D4C365205D0] FOREIGN KEY ([tagged_id]) REFERENCES [pts_licenses] ([licenseId]);
ALTER TABLE [pts_license_tag] ADD CONSTRAINT [FK82158D4C3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [sel_pick_tag] ADD CONSTRAINT [FK1B5B30C13EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]);
ALTER TABLE [sel_pick_tag] ADD CONSTRAINT [FK1B5B30C1C4D491E2] FOREIGN KEY ([tagged_id]) REFERENCES [sel_picks] ([pickId]);
ALTER TABLE [sel_picks] ADD CONSTRAINT [FK_Assignment_Picks] FOREIGN KEY ([assignmentId]) REFERENCES [sel_assignments] ([assignmentId]) ON DELETE CASCADE;
ALTER TABLE [sel_picks] ADD CONSTRAINT [FK_Item_Picks] FOREIGN KEY ([itemId]) REFERENCES [core_items] ([itemId]);
ALTER TABLE [sel_picks] ADD CONSTRAINT [FK_Location_Picks] FOREIGN KEY ([locationId]) REFERENCES [core_locations] ([locationId]);
ALTER TABLE [sel_picks] ADD CONSTRAINT [FK_Operator_Picks] FOREIGN KEY ([operatorId]) REFERENCES [core_operators] ([operatorId]);
ALTER TABLE [sel_picks] ADD CONSTRAINT [FK_OriginalPick_Picks] FOREIGN KEY ([originalPickId]) REFERENCES [sel_picks] ([pickId]);
--COMMIT TRANSACTION;
