--Created for the VL 4.0 release
--Updates SQL Server 2005 Database

--Adding index IX_ARCHIVE_PICKDETAIL_PICKID to PICKID field of ARCH_SEL_PICK_DETAILS table for improved query performance
CREATE INDEX IX_ARCHIVE_PICKDETAIL_PICKID ON arch_sel_pick_details (pickId);

