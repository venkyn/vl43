--Created for the VL3.1.1 release
--Updates SQL Server 2000 Database
ALTER TABLE arch_pts_puts ALTER COLUMN itemDescription NVARCHAR(255) NULL;
ALTER TABLE arch_sel_picks ALTER COLUMN itemDescription NVARCHAR(255) NULL;
