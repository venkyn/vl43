--Created for the VL 4.0 release
--Updates SQL Server 2005 Database
--Updates VL version
--Adds indexes to improve performance

--This will need to be added to all future upgrade scripts
UPDATE voc_system_properties set value = 1 WHERE systemPropertyId = -1005;

--Update module versions
--To update the version, change the SET @vlVersion... line below
DECLARE @vlVersion varchar(32);
DECLARE @vipVersion varchar(32);
SET @vlVersion = '4.0';
SET @vipVersion = '1.9';
DECLARE @newId bigint;
DECLARE @oldId bigint;
SET @oldId = (select moduleId from voc_plugin_modules where name = 'plugin.module.voicelink' AND isEnabled = 1);
UPDATE voc_plugin_modules set isEnabled = 0 where name = 'plugin.module.voicelink';
INSERT into voc_plugin_modules (name, moduleVersion, frameworkVersion, sequenceNumber, uri, navMenuName, isEnabled, createdDate) VALUES ('plugin.module.voicelink', @vlVersion, @vipVersion, 101, '/selection/home.action', 'voicelink', 1, convert(varchar(10),getdate(),101));
SET @newId = (select moduleId from voc_plugin_modules where name = 'plugin.module.voicelink' AND isEnabled = 1);
UPDATE voc_plugin_components set moduleId = @newId where moduleId = @oldId;
UPDATE voc_plugin_modules_auth set moduleId = @newId where moduleId = @oldId;

UPDATE voc_plugin_modules
SET     moduleVersion = @vipVersion
WHERE   moduleId      = 1;
UPDATE voc_plugin_modules
SET     frameworkVersion = @vipVersion
WHERE   moduleId         = 1;
UPDATE voc_plugin_modules
SET     moduleVersion = @vipVersion
WHERE   moduleId      = 2;
UPDATE voc_plugin_modules
SET     frameworkVersion = @vipVersion
WHERE   moduleId         = 2;

--Adding index IX_PICK_ORIGINALPICKID to ORIGINALPICKID field of SEL_PICKS table for improved query performance
CREATE INDEX IX_PICK_ORIGINALPICKID ON sel_picks (originalPickId);

--Adding index IX_CORE_OPER_LABOR_CREATEDDATE to CREATEDDATE field of CORE_OPERATOR_LABOR table for improved query performance
CREATE INDEX IX_CORE_OPER_LABOR_CREATEDDATE ON core_operator_labor (createdDate);

--Adding index IX_CORE_OPER_LABOR_PREVOPLABID to PREVIOUSOPERATORLABORID field of CORE_OPERATOR_LABOR table for improved query performance
CREATE INDEX IX_CORE_OPER_LABOR_PREVOPLABID ON core_operator_labor (previousOperatorLaborId);

--Adding index SEL_PICK_DETAILS_PICKID to PICKID field of SEL_PICK_DETAILS table for improved query performance
CREATE INDEX IX_SEL_PICK_DETAILS_PICKID ON sel_pick_details (pickId);

-- Renaming voc_jobs table column name
Alter table voc_jobs rename column interval to job_interval;

-- Update report names starting with 'Archive'
UPDATE voc_report
SET report_name = 'AssignmentLabor'
WHERE ID = -400;
UPDATE voc_report
SET report_name = 'Container'
WHERE ID = -300;
UPDATE voc_report
SET report_name = 'Assignment'
WHERE ID = -200;
-- Changing the data_member for Operator ID.
UPDATE voc_reportType_parameter 
SET data_member='operatorIdentifier' 
WHERE reportType_id='-200' and id='-202'

-- Reordering the Reports to make them uniform
--ArchivePTSLicense
SET IDENTITY_INSERT voc_report ON;
UPDATE voc_report
SET id          = -1300
SET report_name = 'PTSLicense'
WHERE id        = -700;
--ArchivePTSContainers
UPDATE voc_report
SET id          = -1500
SET report_name = 'PTSContainers'
WHERE id        = -1010;
--ArchivePTSLicenseLabor
UPDATE voc_report
SET id          = -1400
SET report_name = 'PTSLicenseLabor'
WHERE id        = -1000;
--PTSOperatorLabor
UPDATE voc_report
SET id          = -601
WHERE id        = -800;
SET IDENTITY_INSERT voc_report OFF;
--Assignments Report
UPDATE voc_report
SET SESSION_BEAN_ID = 'assignmentReport'
WHERE ID            = -200;

--Containers Report
UPDATE voc_report
SET SESSION_BEAN_ID = 'containerReport'
WHERE ID            = -300;

-- Reordering the Reports Parameters to correspond to the new report id
SET IDENTITY_INSERT voc_report_parameter ON;
UPDATE voc_report_parameter
SET id          = -1300
WHERE id        = -700;
--ArchivePTSContainers
UPDATE voc_report_parameter
SET id          = -1500
WHERE id        = -1010;
--ArchivePTSLicenseLabor
UPDATE voc_report_parameter
SET id          = -1400
WHERE id        = -1000;
--PTSOperatorLabor
UPDATE voc_report_parameter
SET id          = -601
WHERE id        = -800;
SET IDENTITY_INSERT voc_report_parameter OFF;

--Insert the new reports
--Labor Shift Summary Report
SET IDENTITY_INSERT voc_report ON;
INSERT INTO voc_report (id,report_name,report_name_key,report_description,app_name,session_bean_id) VALUES (-700,'LaborShiftSummaryReport','selection.report.name.laborsummary','Labor summary report for selected operators.','selection','laborShiftSummaryReport');
INSERT INTO voc_report (id,report_name,report_name_key,report_description,app_name,session_bean_id) VALUES (-800,OperatorBreaks,'selection.report.name.operatorbreak','Break report of operators.','selection','operatorBreakReport');
INSERT INTO voc_report (id,report_name,report_name_key,report_description,app_name,session_bean_id) VALUES (-900,ShortedLocations,'selection.report.name.shortedLocations','Shorted Locations.',selection,shortedLocationReport);
INSERT INTO voc_report (id,report_name,report_name_key,report_description,app_name,session_bean_id) VALUES (-1000,SlotVisit,'selection.report.name.slotVisit','Frequency of Slot Visit.',selection,slotVisitReport);
INSERT INTO voc_report (id,report_name,report_name_key,report_description,app_name,session_bean_id) VALUES (-1100,RegionProfiles,'selection.report.name.regionProfiles','Region Configurations.',selection,regionProfilesReport);
INSERT INTO voc_report (report_name,report_name_key,report_description,app_name,session_bean_id) VALUES (-1200,OperatorPerformance,'selection.report.name.operatorPerformance','Operator Performance.',selection,operatorPerformanceReport);
SET IDENTITY_INSERT voc_report OFF;

--Labor Summary Report Parameters
SET IDENTITY_INSERT voc_report_parameter ON;
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-701,-700,'OPERATOR_ID',NULL,NULL,'selection.report.laborsummary.operator',1,'operatorManager','all','operatorIdentifier','operatorIdentifier',1,1,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-702,-700,'START_DATE',NULL,'reportDate','selection.report.laborsummary.startdate',0,NULL,NULL,'name',NULL,2,0,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-703,-700,'END_DATE',NULL,'reportDate','selection.report.laborsummary.enddate',0,NULL,NULL,'name',NULL,3,0,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-704,-700,'SHIFT_START',NULL,NULL,'selection.report.laborsummary.shiftstarttime',0,NULL,NULL,NULL,NULL,4,0,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-705,-700,'SHIFT_LENGTH',NULL,NULL,'selection.report.laborsummary.shiftlength',0,NULL,NULL,NULL,NULL,5,0,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-706,-700,'REPORT_TYPE',NULL,NULL,'selection.report.laborsummary.reporttype',0,NULL,NULL,NULL,NULL,6,0,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-707,-700,'REGION',NULL,NULL,'selection.report.laborsummary.region',1,'regionManager','all','name',number,7,0,NULL);
--Operator Break Types Report Parameters
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-801,-800,'OPERATOR_ID',NULL,NULL,'selection.report.operatorbreak.operator',1,'operatorManager','all','operatorIdentifier','operatorIdentifier',1,1,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-802,-800,'BREAK_TYPE',NULL,NULL,'selection.report.operatorbreak.breaktype',1,'breakTypeManager','all','name','name',2,1,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-803,-800,'MIN_DURATION',NULL,'isNumeric','selection.report.operatorbreak.minduration',0,NULL,NULL,'name',NULL,3,1,3);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-804,-800,'START_DATE',NULL,'reportDate','selection.report.operatorbreak.startdate',0,NULL,NULL,'name',NULL,4,0,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-805,-800,'END_DATE',NULL,'reportDate','selection.report.operatorbreak.enddate',0,NULL,NULL,'name',NULL,5,0,NULL);
--Shorted Locations Report Parameters
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-901,-900,'LOCATION_COUNT',10,'isNumeric','selection.report.shortedLocations.count',0,NULL,NULL,'name',NULL,4,1,4);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-902,-900,'START_DATE',NULL,'reportDate','selection.report.shortedLocations.startdate',0,NULL,NULL,'name',NULL,5,0,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-903,-900,'END_DATE',NULL,'reportDate','selection.report.shortedLocations.enddate',0,NULL,NULL,'name',NULL,6,0,NULL);
--Frequency of Slot Report Parameters
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-1001,-1000,'LOCATION_COUNT',10,'isNumeric','selection.report.slotVisit.count',0,NULL,NULL,'name',NULL,4,1,4);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-1002,-1000,'START_DATE',NULL,'reportDate,selection.report.slotVisit.startdate',0,NULL,NULL,'name',NULL,5,0,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-1003,-1000,'END_DATE',NULL,'reportDate,selection.report.slotVisit.enddate',0,NULL,NULL,'name',NULL,6,0,NULL);
--Region Profiles Report Parameters
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-1101,-1100,'REGION',NULL,NULL,'selection.report.regionprofiles.region,1,selectionRegionProfileManager','all',number,number,1,1,NULL);
--Operator Performance Report Parameters
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-1201,-1200,'SORT_ORDER',1,NULL,'selection.report.operatorPerformance.sortOrder',2,'operatorPerformanceReport','sortOrders','label','value',1,0,1);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-1202,-1200,'OPERATOR_COUNT',10,'isNumeric,selection.report.operatorPerformance.count',0,NULL,NULL,'name',NULL,2,1,4);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-1203,-1200,'START_DATE',NULL,'reportDate,selection.report.operatorPerformance.startdate',0,NULL,NULL,'name',NULL,3,0,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-1204,-1200,'END_DATE',NULL,'reportDate,selection.report.operatorPerformance.enddate',0,NULL,NULL,'name',NULL,4,0,NULL);
INSERT INTO voc_report_parameter (id,report_id,parameter_name,default_value,validation_function,description,field_type,service_name,service_method,display_member,data_member,prompt_order,is_required,maxlength)VALUES(-1205,-1200,'ACTION_TYPE',NULL,NULL,'selection.report.operatorPerformance.actionType,1,operatorPerformanceReport','functions','label','value',5,0,NULL);
-- PTS License Report Parameters
Insert into VOC_REPORTTYPE_PARAMETER (ID,REPORTTYPE_ID,PARAMETER_NAME,DEFAULT_VALUE,VALIDATION_FUNCTION,DESCRIPTION,FIELD_TYPE,SERVICE_NAME,SERVICE_METHOD,DISPLAY_MEMBER,DATA_MEMBER,PROMPT_ORDER,IS_REQUIRED,MAXLENGTH) values (-1301,-1300,'OPERATOR_ID','operators',null,'puttostore.report.license.operator',3,'operatorManager','all','operatorIdentifier','operatorIdentifier',1,0,null);
Insert into VOC_REPORTTYPE_PARAMETER (ID,REPORTTYPE_ID,PARAMETER_NAME,DEFAULT_VALUE,VALIDATION_FUNCTION,DESCRIPTION,FIELD_TYPE,SERVICE_NAME,SERVICE_METHOD,DISPLAY_MEMBER,DATA_MEMBER,PROMPT_ORDER,IS_REQUIRED,MAXLENGTH) values (-1302,-1300,'LICENSE_NUMBER',null,null,'puttostore.report.license.number',0,null,null,'name',null,2,0,'50');
Insert into VOC_REPORTTYPE_PARAMETER (ID,REPORTTYPE_ID,PARAMETER_NAME,DEFAULT_VALUE,VALIDATION_FUNCTION,DESCRIPTION,FIELD_TYPE,SERVICE_NAME,SERVICE_METHOD,DISPLAY_MEMBER,DATA_MEMBER,PROMPT_ORDER,IS_REQUIRED,MAXLENGTH) values (-1303,-1300,'OPERATOR_TEAM_ID','operatorTeams',null,'puttostore.report.license.operatorteam',3,'operatorTeamManager','all','name','id',3,0,null);
-- PTS License Labor Report Parameters
Insert into VOC_REPORTTYPE_PARAMETER (ID,REPORTTYPE_ID,PARAMETER_NAME,DEFAULT_VALUE,VALIDATION_FUNCTION,DESCRIPTION,FIELD_TYPE,SERVICE_NAME,SERVICE_METHOD,DISPLAY_MEMBER,DATA_MEMBER,PROMPT_ORDER,IS_REQUIRED,MAXLENGTH) values (-1401,-1400,'OPERATOR_ID',null,null,'puttostore.report.licenselabor.operator',1,'operatorManager','all','operatorIdentifier','operatorIdentifier',1,0,null);
Insert into VOC_REPORTTYPE_PARAMETER (ID,REPORTTYPE_ID,PARAMETER_NAME,DEFAULT_VALUE,VALIDATION_FUNCTION,DESCRIPTION,FIELD_TYPE,SERVICE_NAME,SERVICE_METHOD,DISPLAY_MEMBER,DATA_MEMBER,PROMPT_ORDER,IS_REQUIRED,MAXLENGTH) values (-1402,-1400,'OPERATOR_TEAM_ID','operatorTeams',null,'puttostore.report.licenselabor.operatorteam',3,'operatorTeamManager','all','name','id',2,0,null);
SET IDENTITY_INSERT voc_report_parameter OFF;
