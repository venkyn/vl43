--Created for the VL4.0.1 release
--Updates SQL Server 2000 Database
--Removes unsupported languages from the summary prompt view
--Adds newly supported or resupported languages to the summary prompt view
--Updates EPP and VL versions

--This will need to be added to all future upgrade scripts
UPDATE voc_system_properties set value = 1 WHERE systemPropertyId = -1005;

--Removes unsupported locales FROM list of summary prompts that can be built (DE,ES,FR_CA,IT,RU,ZH_TW)
DELETE FROM voc_column WHERE [id] IN (-13004,13006,-13009,13011,-13020,-13029);

--Removes unsupported locales FROM the voc_system_translation table
DELETE FROM voc_system_translations WHERE locale NOT IN ('da','en_AU','en_GB','en_US','es_MX','fi','fr','ja','ko','nl','pt_BR','sv','zh_CN');

--Need to insert previously removed summary prompts (DA,FI,NL,SV)
SET IDENTITY_INSERT voc_column ON;
INSERT INTO voc_column([id],[view_id],[field],[display],[sorted],[sort_asc],[width],[visible],[display_function],[required],[displayable],[column_order],[sortType],[filterType],[operand_type],[painter_function],[option_key_prefix])VALUES(-13003,-1033,'definitions','summaryPrompt.view.column.da',0,1,110,1,'displayPromptLanguage',0,1,4,0,0,4,'display_da','summaryPrompt.language');
INSERT INTO voc_column([id],[view_id],[field],[display],[sorted],[sort_asc],[width],[visible],[display_function],[required],[displayable],[column_order],[sortType],[filterType],[operand_type],[painter_function],[option_key_prefix])VALUES(-13008,-1033,'definitions','summaryPrompt.view.column.fi',0,1,110,1,'displayPromptLanguage',0,1,9,0,0,4,'display_fi','summaryPrompt.language');
INSERT INTO voc_column([id],[view_id],[field],[display],[sorted],[sort_asc],[width],[visible],[display_function],[required],[displayable],[column_order],[sortType],[filterType],[operand_type],[painter_function],[option_key_prefix])VALUES(-13012,-1033,'definitions','summaryPrompt.view.column.nl',0,1,110,1,'displayPromptLanguage',0,1,13,0,0,4,'display_nl','summaryPrompt.language');
INSERT INTO voc_column([id],[view_id],[field],[display],[sorted],[sort_asc],[width],[visible],[display_function],[required],[displayable],[column_order],[sortType],[filterType],[operand_type],[painter_function],[option_key_prefix])VALUES(-13015,-1033,'definitions','summaryPrompt.view.column.sv',0,1,110,1,'displayPromptLanguage',0,1,16,0,0,4,'display_sv','summaryPrompt.language');
SET IDENTITY_INSERT voc_column OFF;

--Updates VL version, EPP version and modules (VL 4.0.1,EPP 1.9)
DECLARE @vlVersion varchar(32);
DECLARE @vipVersion varchar(32);
SET @vlVersion = '4.0.1';
SET @vipVersion = '1.9';
DECLARE @newId bigint;
DECLARE @oldId bigint;
SET @oldId = (select moduleId from voc_plugin_modules where name = 'plugin.module.voicelink' AND isEnabled = 1);
UPDATE voc_plugin_modules set isEnabled = 0 where name = 'plugin.module.voicelink';
INSERT into voc_plugin_modules (name, moduleVersion, frameworkVersion, sequenceNumber, uri, navMenuName, isEnabled, createdDate) VALUES ('plugin.module.voicelink', @vlVersion, @vipVersion, 101, '/selection/home.action', 'voicelink', 1, convert(varchar(10),getdate(),101));
SET @newId = (select moduleId from voc_plugin_modules where name = 'plugin.module.voicelink' AND isEnabled = 1);
UPDATE voc_plugin_components set moduleId = @newId where moduleId = @oldId;
UPDATE voc_plugin_modules_auth set moduleId = @newId where moduleId = @oldId;

UPDATE voc_plugin_modules SET moduleVersion = @vipVersion WHERE moduleId = 1;
UPDATE voc_plugin_modules SET frameworkVersion = @vipVersion WHERE moduleId = 1;
UPDATE voc_plugin_modules SET moduleVersion = @vipVersion WHERE moduleId = 2;
UPDATE voc_plugin_modules SET frameworkVersion = @vipVersion WHERE moduleId = 2;


UPDATE voc_column SET display_function='displayChartName' WHERE id='-22301';
UPDATE voc_column SET display_function='displayChartDescription' WHERE id='-22302';

UPDATE voc_charts SET chartName = 'charts.name.estimatedHoursRemaining', title = 'charts.title.estimatedHoursRemaining', description = 'charts.description.estimatedHoursRemaining' WHERE (chartId = 1);
UPDATE voc_charts SET chartName = 'charts.name.itemsPicksRemaining', title = 'charts.description.itemsPicksRemaining', description = 'charts.description.itemsPicksRemaining' WHERE (chartId = 2);
UPDATE voc_charts SET chartName = 'charts.name.routeCompletion', title = 'charts.title.routeCompletion', description  = 'charts.description.routeCompletion' WHERE (chartId = 3);

--Report Schedular is now locale aware so add the column
ALTER TABLE voc_reports ADD [language] NVARCHAR (255) NULL;

