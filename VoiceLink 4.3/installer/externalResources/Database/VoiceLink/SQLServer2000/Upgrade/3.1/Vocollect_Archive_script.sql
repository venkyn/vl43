--create-archive-tables diff against VL 3.0.1 release on 5/22/08.
ALTER TABLE arch_sel_assignment_labor ADD operatorName NVARCHAR(128);
GO
UPDATE arch_sel_assignment_labor SET operatorName = employeeName;
GO
ALTER TABLE arch_sel_assignment_labor DROP COLUMN employeeName;
GO

ALTER TABLE arch_sel_assignments ADD operatorName NVARCHAR(128);
GO
UPDATE arch_sel_assignments SET operatorName = employeeName;
GO
ALTER TABLE arch_sel_assignments DROP COLUMN employeeName;
GO

ALTER TABLE arch_sel_pick_details ADD operatorName NVARCHAR(128);
GO
UPDATE arch_sel_pick_details SET operatorName = employeeName;
GO
ALTER TABLE arch_sel_pick_details DROP COLUMN employeeName;
GO

ALTER TABLE arch_sel_picks ADD shortedDate DATETIME, scannedVerification nvarchar(50);
CREATE INDEX IX_arch_pick_short_date ON arch_sel_picks (shortedDate);

ALTER TABLE arch_sel_pick_details ADD lotNumber NVARCHAR(50);

CREATE TABLE arch_pts_container_details (containerDetailId numeric(19,0) identity not null, version int not null, putContainerId numeric(19,0) null, operatorName nvarchar(128) null, operatorIdentifier nvarchar(64) null, putTime datetime not null, quantityPut int not null, licenseNumber nvarchar(50) not null, detailType int not null, itemNumber nvarchar(50) not null, itemDescription nvarchar(255) not null, regionName nvarchar(255) not null, regionNumber int null, exportStatus int null, createdDate datetime not null, primary key (containerDetailId));
CREATE TABLE arch_pts_containers (containerId numeric(19,0) identity not null, version int not null, containerNumber nvarchar(50) not null, preAisle nvarchar(255) null, aisle nvarchar(255) null, postAisle nvarchar(255) null, slot nvarchar(255) null, deliveryLocation nvarchar(50) null, percentComplete double precision null, customerNumber nvarchar(50) null, customerName nvarchar(50) null, customerRoute nvarchar(50) null, customerAddress nvarchar(50) null, exportStatus int not null, status int not null, siteId numeric(19,0) not null, siteName nvarchar(256) not null, createdDate datetime not null, primary key (containerId));
CREATE TABLE arch_pts_license_labor (licenseLaborId numeric(19,0) identity not null, version int not null, licenseId numeric(19,0) not null, operatorName nvarchar(128) null, operatorIdentifier nvarchar(64) null, operatorLaborId numeric(19,0) null, breakLaborId numeric(19,0) null, startTime datetime not null, endTime datetime null, duration numeric(19,0) not null check (duration >=0), exportStatus int not null, quantityPut int not null check (quantityPut >= 0), licenseProrateCount int not null check (licenseProrateCount >= 0), groupProrateCount int not null check (groupProrateCount >= 0), groupNumber numeric(19,0) not null, groupCount int not null check (groupCount > 0), actualRate double precision not null check (actualRate >= 0), percentOfGoal double precision not null check (percentOfGoal >= 0), createdDate datetime not null, primary key (licenseLaborId));
CREATE TABLE arch_pts_licenses (licenseId numeric(19,0) identity not null, version int not null, licenseNumber nvarchar(50) not null, groupNumber numeric(19,0) null, requestedOrderNo int not null, groupPosition int null, groupCount int null, operatorName nvarchar(128) null, operatorIdentifier nvarchar(64) null, regionName nvarchar(255) not null, regionNumber int not null, partialNumber nvarchar(50) not null, status int not null, exportStatus int not null, startTime datetime null, endTime datetime null, expectedResiduals int null, totalItemQuantity int null, putDetailCount int default 0 not null, siteId numeric(19,0) not null, siteName nvarchar(256) not null, createdDate datetime not null, primary key (licenseId));
CREATE TABLE arch_pts_put_details (putDetailId numeric(19,0) identity not null, version int not null, putId numeric(19,0) not null, detailType int not null, operatorIdentifier nvarchar(64) null, operatorName nvarchar(128) null, putTime datetime not null, quantityPut int not null, exportStatus int not null, containerNumber nvarchar(50) null, createdDate datetime not null, primary key (putDetailId));
CREATE TABLE arch_pts_puts (putId numeric(19,0) identity not null, version int not null, licenseId numeric(19,0) not null, operatorName nvarchar(128) null, operatorIdentifier nvarchar(64) null, itemNumber nvarchar(50) not null, itemDescription nvarchar(255) not null, preAisle nvarchar(255) null, aisle nvarchar(255) null, postAisle nvarchar(255) null, slot nvarchar(255) null, deliveryLocation nvarchar(50) null, percentComplete double precision null, customerNumber nvarchar(50) null, customerName nvarchar(50) null, customerRoute nvarchar(50) null, customerAddress nvarchar(50) null, sequenceNumber int null, quantityToPut int not null check (quantityToPut >= 0), quantityPut int not null check (quantityPut >= 0), residualQuantity int not null check (residualQuantity >= 0), allowOverPack tinyint default 0 not null, putTime datetime null, unitOfMeasure nvarchar(50) null, status int not null, putDetailCount int default 0 not null, siteId numeric(19,0) not null, siteName nvarchar(256) not null, createdDate datetime not null, primary key (putId));

ALTER TABLE arch_pts_container_details add constraint FK_ArchCont_ArchContDetail foreign key (putContainerId) references arch_pts_containers on delete cascade;
ALTER TABLE arch_pts_license_labor add constraint FK_ArchPtsLic_ArchPtsLicLabors foreign key (licenseId) references arch_pts_licenses on delete cascade;
ALTER TABLE arch_pts_put_details add constraint FK_Archive_Put_PutDetails foreign key (putId) references arch_pts_puts on delete cascade;
ALTER TABLE arch_pts_puts add constraint FK_Arch_PtsLic_Arch_Puts foreign key (licenseId) references arch_pts_licenses on delete cascade;
