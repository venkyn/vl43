--Created for the VL3.1.1 release
--Updates SQL Server 2000 Database
--Removes unsupported languages from the summary prompt view
--Updates VL version
--Updates Selection Pick export for VLINK-2574

--Changes sort types to support data translation tables
UPDATE voc_column SET SortType = 4 WHERE id IN (-22006,-22203,-21903,-21311,-21310,-21211,-21210,-21202,-21123,-21120,-21119,-21118,-21111,-21103,-21001);
UPDATE voc_column SET SortType = 4 WHERE id IN (-21916,-21912,-21910,-21905);
UPDATE voc_column SET SortType = 1 WHERE id IN (-21917);

--Updates Summary prompts remove cs,da,es,fi,de,el,hu,it,nl,no,pl,pt, and sv
DELETE FROM voc_column WHERE [id] IN (-13003,-13004,-13006,-13008,-13011,-13012,-13014,-13015,-13016,-13021,-13022,-13026,-13027,-13028);

--Update Summary prompts add ko and ru
SET IDENTITY_INSERT voc_column ON;
INSERT INTO voc_column([id],[view_id],[field],[display],[sorted],[sort_asc],[width],[visible],[display_function],[required],[displayable],[column_order],[sortType],[filterType],[operand_type],[painter_function],[option_key_prefix])VALUES(-13020,-1033,'definitions','summaryPrompt.view.column.ru',0,1,110,1,'displayPromptLanguage',0,1,21,0,0,4,'display_ru','summaryPrompt.language');
INSERT INTO voc_column([id],[view_id],[field],[display],[sorted],[sort_asc],[width],[visible],[display_function],[required],[displayable],[column_order],[sortType],[filterType],[operand_type],[painter_function],[option_key_prefix])VALUES(-13023,-1033,'definitions','summaryPrompt.view.column.ko',0,1,110,1,'displayPromptLanguage',0,1,24,0,0,4,'display_ko','summaryPrompt.language');
SET IDENTITY_INSERT voc_column OFF;

--This will need to be added to all future upgrade scripts
UPDATE voc_system_properties set value = 1 WHERE systemPropertyId = -1005;

--Update module versions
--To update the version, change the SET @vlVersion... line below
DECLARE @vlVersion varchar(32);
DECLARE @vipVersion varchar(32);
SET @vlVersion = '3.1.1';
SET @vipVersion = '1.6.1';
DECLARE @newId bigint;
DECLARE @oldId bigint;
SET @oldId = (select moduleId from voc_plugin_modules where name = 'plugin.module.voicelink' AND isEnabled = 1);
UPDATE voc_plugin_modules set isEnabled = 0 where name = 'plugin.module.voicelink';
INSERT into voc_plugin_modules (name, moduleVersion, frameworkVersion, sequenceNumber, uri, navMenuName, isEnabled, createdDate) VALUES ('plugin.module.voicelink', @vlVersion, @vipVersion, 101, '/selection/home.action', 'voicelink', 1, convert(varchar(10),getdate(),101));
SET @newId = (select moduleId from voc_plugin_modules where name = 'plugin.module.voicelink' AND isEnabled = 1);
UPDATE voc_plugin_components set moduleId = @newId where moduleId = @oldId;
UPDATE voc_plugin_modules_auth set moduleId = @newId where moduleId = @oldId;

UPDATE voc_plugin_modules 
SET     moduleVersion = @vipVersion 
WHERE   moduleId      = 1;
UPDATE voc_plugin_modules 
SET     frameworkVersion = @vipVersion 
WHERE   moduleId         = 1;
UPDATE voc_plugin_modules 
SET     moduleVersion = @vipVersion 
WHERE   moduleId      = 2;
UPDATE voc_plugin_modules 
SET     frameworkVersion = @vipVersion 
WHERE   moduleId         = 2;

DROP VIEW vw_export_picked;

CREATE VIEW vw_export_picked (pickDetailId,RecordType,PickTime,OriginalAssignmentNumber,LocationID,Item,Operator,PickStatus,QuantityPicked,RecordedVariableWeight,RecordedSerialNumber,RecordedLotNumber,ExportStatus,Site,PickSequenceNumber) AS SELECT pd.pickDetailId as pickDetailId,pd.detailType as RecordType,pd.pickTime,a.assignmentNumber as OriginalAssignmentNumber,l.scannedVerification as LocationID,i.itemNumber as Item,vo.operatorIdentifier as Operator,obj.status as PickStatus,pd.quantityPicked as QuantityPicked,pd.variableWeight as RecordedVariableWeight,pd.serialNumber as RecordedSerialNumber,pd.lotNumber as RecordedLotNumber,pd.exportStatus,site.name as Site,obj.sequenceNumber as PickSequenceNumber from sel_picks obj join sel_pick_details pd ON obj.pickID = pd.pickID left outer join core_operators o ON pd.OperatorID = o.OperatorID left outer join voc_operators vo ON o.OperatorID = vo.OperatorID left outer join core_items i ON obj.ItemID = i.ItemID join sel_assignments a ON a.AssignmentID = obj.AssignmentID left outer join core_locations l ON obj.locationID = l.locationID join sel_pick_tag objtag ON objtag.tagged_id = obj.pickID join voc_tag vt ON objtag.tag_ID=vt.ID and VT.Tag_Type=1 join voc_site site ON vt.taggable_ID=site.siteID where (pd.exportStatus = 0 or pd.exportStatus = 1);

INSERT into voc_homepage_access_summaries (id,summary_id) values (-1,-16);
INSERT into voc_homepage_access_summaries (id,summary_id) values (-1,-17);
INSERT into voc_homepage_access_summaries (id,summary_id) values (-1,-18);
INSERT into voc_homepage_access_summaries (id,summary_id) values (-1,-19);
