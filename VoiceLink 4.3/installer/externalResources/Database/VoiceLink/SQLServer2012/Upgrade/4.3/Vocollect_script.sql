	-- ***************************************************************************
	--		This table has been removed from EPP
	-- ***************************************************************************
	IF OBJECT_ID('voc_chart_settings') IS NOT NULL
		BEGIN
			PRINT N'Dropping FK_Chart_ChartSettings...'; 
			ALTER TABLE [voc_chart_settings] DROP CONSTRAINT [FK_Chart_ChartSettings]; 
			PRINT N'Dropping FKAB10C779C9E8E129...'; 
			ALTER TABLE [voc_chart_settings] DROP CONSTRAINT [FKAB10C779C9E8E129]; 
			PRINT N'Dropping [voc_chart_settings]...'; 
			DROP TABLE [voc_chart_settings];
		END; 
	ELSE  
		BEGIN  
			print N'table - voc_chart_settings has already been removed...'; 
			--
			-- Dummy update statement to make COMMIT happy 
			--
			UPDATE [voc_column] SET [field]=[field] WHERE [id]=0;
		END;
COMMIT 
;

	IF OBJECT_ID('voc_charts') IS NOT NULL  
		BEGIN 
			IF (SELECT count(*) FROM sys.columns 
        		WHERE [name] = N'chartName' AND [object_id] = OBJECT_ID(N'dbo.voc_charts')) > 0
        		BEGIN 					
	        		PRINT N'Dropping IX_chart_type...'; 
					DROP INDEX [IX_chart_type] ON [voc_charts];  
					PRINT N'Dropping [voc_charts]...';  
					DROP TABLE [voc_charts];
				END;
			ELSE  
				BEGIN  
					print N'table - voc_charts has already been removed...'; 
					--
					-- Dummy update statement to make COMMIT happy 
					--
					UPDATE [voc_column] SET [field]=[field] WHERE [id]=0;
				END;
		END;
COMMIT 
;  
		
	--  ********************************************************************
	--		CREATE VOC_DA_INFO TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_da_info') IS NULL  
		BEGIN  
			PRINT N'Creating [voc_da_info]...'; 
			CREATE TABLE [voc_da_info] (
	 			[daId]            NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
				[version]         INT            NOT NULL,
				[name]            NVARCHAR (255) NOT NULL,
				[displayName]     NVARCHAR (255) NOT NULL,
				[markedForDelete] TINYINT        NOT NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [daId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
		END 
	ELSE 
		BEGIN 
			print N'table - voc_da_info already exists...'; 
		END
	GO 
	
	--  ********************************************************************
	--		CREATE VOC_DA_COLUMN TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_da_column') IS NULL
		BEGIN
			PRINT N'Creating [voc_da_column]...'; 
			CREATE TABLE [voc_da_column] (
			    [columnId]       NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
			    [version]        INT            NOT NULL,
			    [daId]           NUMERIC (19)   NOT NULL,
			    [fieldId]        NVARCHAR (255) NOT NULL,
			    [fieldType]      INT            NOT NULL,
			    [displayName]    NVARCHAR (255) NOT NULL,
			    [uom]            NVARCHAR (255) NULL,
			    [identityColumn] TINYINT        NOT NULL,
			    [columnSequence] INT            NOT NULL,
			    [columnState]    INT            NOT NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [columnId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
		END
	ELSE
		BEGIN
			print N'table - voc_da_column already exists...'; 
		END
	GO
	
	--  ********************************************************************
	--		CREATE VOC_CHARTS TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_charts') IS NULL
		BEGIN
			PRINT N'Creating [voc_charts]...'; 
			CREATE TABLE [voc_charts] (
			    [chartId]       NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
			    [version]       INT            NOT NULL,
			    [name]          NVARCHAR (50)  NOT NULL,
			    [description]   NVARCHAR (255) NULL,
			    [daInformation] NUMERIC (19)   NULL,
			    [category]      NUMERIC (19)   NULL,
			    [type]          INT            NOT NULL,
			    [pivotOn]       NUMERIC (19)   NULL,
			    [pivotFor]      NUMERIC (19)   NULL,
			    [sortBy]        NUMERIC (19)   NULL,
			    [sortOrder]     INT            NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [chartId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
		END 
	ELSE
		BEGIN 
			print N'table - voc_charts already exists...'; 
		END 
	GO
	
	--  ********************************************************************
	--		CREATE VOC_CHART_TAG TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_chart_tag') IS NULL
		BEGIN
			PRINT N'Creating [voc_chart_tag]...'; 
			CREATE TABLE [voc_chart_tag] (
			    [tagged_id] NUMERIC (19) NOT NULL,
			    [tag_id]    NUMERIC (19) NOT NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [tagged_id] ASC, 
			    [tag_id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
		END
	ELSE
		BEGIN
			print N'table - voc_chart_tag already exists...'; 
		END
	GO
	
	--  ********************************************************************
	--		CREATE VOC_CHART_DATA_FIELDS TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_chart_data_fields') IS NULL
		BEGIN
			PRINT N'Creating [voc_chart_data_fields]...'; 
			CREATE TABLE [voc_chart_data_fields] (
			    [chartDataFieldId] NUMERIC (19) IDENTITY (1, 1) NOT NULL,
			    [version]          INT          NOT NULL,
			    [chartId]          NUMERIC (19) NULL,
			    [daColumnId]       NUMERIC (19) NOT NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [chartDataFieldId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
		END
	ELSE
		BEGIN
			print N'table - voc_chart_data_fields already exists...'; 
		END
	GO
	
	--  ********************************************************************
	--		CREATE VOC_DASHBOARDS TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_dashboards') IS NULL
		BEGIN
			PRINT N'Creating [voc_dashboards]...'; 
			CREATE TABLE [voc_dashboards] (
			    [dashboardId] NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
			    [version]     INT            NOT NULL,
			    [name]        NVARCHAR (50)  NOT NULL,
			    [timeFilter]  INT            NOT NULL,
			    [description] NVARCHAR (255) NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [dashboardId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
			PRINT N'Creating on [voc_dashboards].[timeFilter]...'; 
			ALTER TABLE [voc_dashboards] ADD DEFAULT ((0)) FOR [timeFilter]; 
		END
	ELSE
		BEGIN
			print N'table - voc_dashboards already exists...'; 
		END
	GO
	
	--  ********************************************************************
	--		CREATE VOC_DASHBOARD_TAG TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_dashboard_tag') IS NULL
		BEGIN
			PRINT N'Creating [voc_dashboard_tag]...'; 
			CREATE TABLE [voc_dashboard_tag] (
			    [tagged_id] NUMERIC (19) NOT NULL,
			    [tag_id]    NUMERIC (19) NOT NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [tagged_id] ASC, 
			    [tag_id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
		END
	ELSE
		BEGIN
			print N'table - voc_dashboard_tag already exists...'; 
		END
	GO
	
	--  ********************************************************************
	--		CREATE VOC_DASHBOARD_DETAILS TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_dashboard_details') IS NULL
		BEGIN
			PRINT N'Creating [voc_dashboard_details]...'; 
			CREATE TABLE [voc_dashboard_details] (
			    [dashboardDetailId]    NUMERIC (19) IDENTITY (1, 1) NOT NULL,
			    [version]              INT          NOT NULL,
			    [dashboardId]          NUMERIC (19) NOT NULL,
			    [chartId]              NUMERIC (19) NOT NULL,
			    [drillDownChartId]     NUMERIC (19) NULL,
			    [linkedDrillDownField] NUMERIC (19) NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [dashboardDetailId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
		END
	ELSE
		BEGIN
			print N'table - voc_dashboard_details already exists...'; 
		END
	GO
	
	--  ********************************************************************
	--		CREATE VOC_ALERTS TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_alerts') IS NULL
		BEGIN
			PRINT N'Creating [voc_alerts]...'; 
			CREATE TABLE [voc_alerts] (
		        [alertId]                   NUMERIC (19)   IDENTITY (1, 1) NOT NULL,
			    [version]                   INT            NOT NULL,
			    [alertName]                 NVARCHAR (50)  NOT NULL,
			    [daInformation]             NUMERIC (19)   NULL,
			    [alertNotificationPriority] INT            NULL,
			    [emailAddress]              NVARCHAR (255) NULL,
			    [hours]                     INT            NULL,
			    [minutes]                   INT            NULL,
			    [lastAlertEventTime]        DATETIME       NULL,
			    [lastEvaluationTime]        DATETIME       NULL,
			    [alertStatus]               INT            NOT NULL,
			    [dashboard]                 NUMERIC (19)   NULL,
			    [serverURL]                 NVARCHAR (255) NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [alertId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
			PRINT N'Creating on [voc_alerts].[hours]...'; 
			ALTER TABLE [voc_alerts] ADD DEFAULT ((0)) FOR [hours]; 
			PRINT N'Creating on [voc_alerts].[minutes]...'; 
			ALTER TABLE [voc_alerts] ADD DEFAULT ((0)) FOR [minutes]; 
		END
	ELSE
		BEGIN
			print N'table - voc_alerts already exists...'; 
		END
	GO
	
	--  ********************************************************************
	--		CREATE VOC_ALERT_TAG TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_alert_tag') IS NULL
		BEGIN
			PRINT N'Creating [voc_alert_tag]...'; 
			CREATE TABLE [voc_alert_tag] (
			    [tagged_id] NUMERIC (19) NOT NULL,
			    [tag_id]    NUMERIC (19) NOT NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [tagged_id] ASC, 
			    [tag_id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
		END
	ELSE
		BEGIN
			print N'table - voc_alert_tag already exists...'; 
		END
	GO
	
	--  ********************************************************************
	--		CREATE VOC_ALERT_CRITERIA TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_alert_criteria') IS NULL
		BEGIN
			PRINT N'Creating [voc_alert_criteria]...'; 
			CREATE TABLE [voc_alert_criteria] (
			    [criteriaId]       NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
			    [version]          INT           NOT NULL,
			    [alertId]          NUMERIC (19)  NOT NULL,
			    [criteriaSequence] INT           NOT NULL,
			    [daColumn]         NUMERIC (19)  NULL,
			    [operandType]      INT           NOT NULL,
			    [threshold]        NVARCHAR (50) NULL,
			    [criteriaRelation] INT           NOT NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [criteriaId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
		END
	ELSE
		BEGIN
			print N'table - voc_alert_criteria already exists...'; 
		END
	GO
	
	--  ********************************************************************
	--		CREATE VOC_USER_CHART_PREFERENCE TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_user_chart_preference') IS NULL
		BEGIN
			PRINT N'Creating [voc_user_chart_preference]...'; 
			CREATE TABLE [voc_user_chart_preference] (
			    [preferenceId] NUMERIC (19) IDENTITY (1, 1) NOT NULL,
			    [dashboardId]  NUMERIC (19) NULL,
			    [chartId]      NUMERIC (19) NULL,
			    [chartWidth]   FLOAT (53)   NULL,
			    [chartHeight]  FLOAT (53)   NULL,
			    [chartX]       FLOAT (53)   NULL,
			    [chartY]       FLOAT (53)   NULL,
			    [userId]       NUMERIC (19) NOT NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [preferenceId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
			PRINT N'Creating [FK_ChartPref_Dashboard]...'; 
			ALTER TABLE [voc_user_chart_preference] WITH NOCHECK ADD CONSTRAINT [FK_ChartPref_Dashboard] FOREIGN KEY ([dashboardId]) REFERENCES [voc_dashboards] ([dashboardId]); 
			PRINT N'Creating [FK_ChartPref_Chart]...'; 
			ALTER TABLE [voc_user_chart_preference] WITH NOCHECK ADD CONSTRAINT [FK_ChartPref_Chart] FOREIGN KEY ([chartId]) REFERENCES [voc_charts] ([chartId]); 
			PRINT N'Creating [FK29DB601BC9E8E129]...'; 
			ALTER TABLE [voc_user_chart_preference] WITH NOCHECK ADD CONSTRAINT [FK29DB601BC9E8E129] FOREIGN KEY ([userId]) REFERENCES [voc_user] ([id]); 
		END
	ELSE
		BEGIN
			print N'table - voc_user_chart_preference already exists...'; 
		END
	GO
	
	--  ********************************************************************
	--		CREATE VOC_USER_CHART_PREFERENCE_TAG TABLE
	--  ********************************************************************
	IF OBJECT_ID('voc_user_chart_preference_tag') IS NULL
		BEGIN
			PRINT N'Creating [voc_user_chart_preference_tag]...'; 
			CREATE TABLE [voc_user_chart_preference_tag] (
			    [tagged_id] NUMERIC (19) NOT NULL,
			    [tag_id]    NUMERIC (19) NOT NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [tagged_id] ASC, 
			    [tag_id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
			PRINT N'Creating [FKED4C976B450E6D4]...'; 
			ALTER TABLE [voc_user_chart_preference_tag] WITH NOCHECK ADD CONSTRAINT [FKED4C976B450E6D4] FOREIGN KEY ([tagged_id]) REFERENCES [voc_user_chart_preference] ([preferenceId]); 
			PRINT N'Creating [FKED4C9763EF30942]...'; 
			ALTER TABLE [voc_user_chart_preference_tag] WITH NOCHECK ADD CONSTRAINT [FKED4C9763EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]); 
	
		END
	ELSE
		BEGIN
			print N'table - voc_user_chart_preference_tag already exists...'; 
		END
	GO
	
	--  ********************************************************************
	--		CREATE CORE_SHIFTS TABLE
	--  ********************************************************************
	IF OBJECT_ID('core_shifts') IS NULL
		BEGIN
			PRINT N'Creating [core_shifts]...'; 
			CREATE TABLE [core_shifts] (
			    [shiftId]      NUMERIC (19)  IDENTITY (1, 1) NOT NULL,
			    [version]      INT           NOT NULL,
			    [name]         NVARCHAR (50) NOT NULL,
			    [startHours]   INT           NULL,
			    [startMinutes] INT           NULL,
			    [endHours]     INT           NULL,
			    [endMinutes]   INT           NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [shiftId] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
			PRINT N'Creating on [core_shifts].[startHours]...'; 
			ALTER TABLE [core_shifts] ADD DEFAULT ((0)) FOR [startHours]; 
			PRINT N'Creating on [core_shifts].[startMinutes]...'; 
			ALTER TABLE [core_shifts] ADD DEFAULT ((0)) FOR [startMinutes]; 
			PRINT N'Creating on [core_shifts].[endHours]...'; 
			ALTER TABLE [core_shifts] ADD DEFAULT ((0)) FOR [endHours]; 
			PRINT N'Creating on [core_shifts].[endMinutes]...'; 
			ALTER TABLE [core_shifts] ADD DEFAULT ((0)) FOR [endMinutes]; 
		END
	ELSE
		BEGIN
			print N'table - core_shifts already exists...'; 
		END
	GO
	
	--  ********************************************************************
	--		CREATE CORE_SHIFT_TAG TABLE
	--  ********************************************************************
	IF OBJECT_ID('core_shift_tag') IS NULL
		BEGIN
			PRINT N'Creating [core_shift_tag]...'; 
			CREATE TABLE [core_shift_tag] (
			    [tagged_id] NUMERIC (19) NOT NULL,
			    [tag_id]    NUMERIC (19) NOT NULL,
		    PRIMARY KEY CLUSTERED 
			(
			    [tagged_id] ASC, 
			    [tag_id] ASC
			)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
			) ON [PRIMARY]
			PRINT N'Creating [FKCB7039BD6D0487E8]...'; 
			ALTER TABLE [core_shift_tag] WITH NOCHECK ADD CONSTRAINT [FKCB7039BD6D0487E8] FOREIGN KEY ([tagged_id]) REFERENCES [core_shifts] ([shiftId]); 
			PRINT N'Creating [FKCB7039BD3EF30942]...'; 
			ALTER TABLE [core_shift_tag] WITH NOCHECK ADD CONSTRAINT [FKCB7039BD3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]); 
		END
	ELSE
		BEGIN
			print N'table - core_shift_tag already exists...'; 
		END
	GO
	
	PRINT N'Schema Update complete.'; 
	GO
	
	--
	-- Inserting data into table VOC_COLUMN
	--
	IF (SELECT COUNT(*) from voc_column WHERE id = -24204) = 0
		BEGIN
			PRINT N'Dropping constraint FK6049706B16D0F6D2'; 
			ALTER TABLE [voc_column] DROP CONSTRAINT [FK6049706B16D0F6D2]
			PRINT N'Dropping constraint FK6049706B971DA1E7'; 
			ALTER TABLE [voc_column] DROP CONSTRAINT [FK6049706B971DA1E7]
			
			PRINT N'Deleting unused voc views'; 
			DELETE FROM [voc_view] WHERE [id]=-1206; 
			
			PRINT N'Inserting new voc views'
			SET IDENTITY_INSERT [voc_view] ON
			INSERT INTO [voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-2004, N'dashboardDetails', N'com.vocollect.epp.dashboard.model.DashboardDetail', NULL, NULL); 
			INSERT INTO [voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-2003, N'dashboards', N'com.vocollect.epp.dashboard.model.Dashboard', NULL, NULL); 
			INSERT INTO [voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-2002, N'charts', N'com.vocollect.epp.chart.model.Chart', NULL, NULL); 
			INSERT INTO [voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-2001, N'alertCriteia', N'com.vocollect.epp.alert.model.AlertCriteria', NULL, NULL); 
			INSERT INTO [voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-2000, N'alerts', N'com.vocollect.epp.alert.model.Alert', NULL, NULL); 
			INSERT INTO [voc_view] ([id], [name], [class_name], [dataBeanName], [dataMethodName]) VALUES (-1250, N'shift', N'com.vocollect.voicelink.core.model.Shift', NULL, NULL); 
			SET IDENTITY_INSERT [voc_view] OFF
			
			--
			-- Deleting data from table voc_column
			--
			PRINT N'Deleting voc_column'
			DELETE FROM [voc_column] WHERE [id]=-22303
			DELETE FROM [voc_column] WHERE [id]=-22302
			DELETE FROM [voc_column] WHERE [id]=-22301
			
			PRINT N'Inserting new voc columns'
			SET IDENTITY_INSERT [voc_column] ON
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24204, -1250, N'endTime.descriptiveText', N'shift.view.column.endTime', 0, 5, 0, 1, 1, 150, 1, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24203, -1250, N'startTime.descriptiveText', N'shift.view.column.startTime', 0, 5, 0, 1, 1, 150, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24202, -1250, N'name', N'shift.view.column.name', 1, 5, 1, 1, 1, 150, 1, 1, NULL, 0, NULL, NULL, N'displayShift', NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-24201, -1250, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1105, -2004, N'id', NULL, 0, 0, 1, 0, 1, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1104, -2004, N'linkedDrillDownField.daColumnDisplayName', N'dashboard.detail.view.column.chart.drillDown.linkedField', 0, 5, 1, 1, 1, 200, 1, 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1103, -2004, N'drillDownChart.name', N'dashboard.detail.view.column.chart.drillDown', 1, 5, 1, 1, 1, 200, 1, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1102, -2004, N'parentChart.name', N'dashboard.detail.view.column.chart.parent', 1, 5, 1, 1, 1, 200, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1101, -2004, N'dashboard.name', N'dashboard.detail.view.column.dashboard', 1, 5, 1, 1, 1, 150, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1055, -2003, N'timeFilter', N'dashboardalert.dashboard.view.column.timeFilter', 1, 1, 1, 1, 1, 150, 1, 4, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1054, -2003, N'isDefault', N'dashboardalert.dashboard.view.column.isDefault', 0, 0, 1, 1, 1, 80, 1, 3, NULL, 4, N'formatisDefaultDashboard', NULL, N'displayDashboardIsDefault', NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1053, -2003, N'description', N'dashboardalert.dashboard.view.column.description', 1, 5, 1, 1, 1, 200, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1052, -2003, N'name', N'dashboardalert.dashboard.view.column.name', 1, 5, 1, 1, 1, 150, 1, 1, NULL, 0, NULL, NULL, N'displayDashboard', NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1051, -2003, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1010, -2002, N'sortOrder', N'dashboardalert.chart.view.column.sortOrder', 0, 3, 1, 1, 1, 150, 1, 9, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1009, -2002, N'sortByDisplayName', N'dashboardalert.chart.view.column.sortBy', 0, 5, 1, 1, 1, 150, 1, 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1008, -2002, N'pivotForDisplayName', N'dashboardalert.chart.view.column.pivotFor', 0, 5, 1, 1, 1, 150, 1, 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1007, -2002, N'pivotOnDisplayName', N'dashboardalert.chart.view.column.pivotOn', 0, 5, 1, 1, 1, 150, 1, 6, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1006, -2002, N'type', N'dashboardalert.chart.view.column.type', 0, 3, 1, 1, 1, 150, 1, 5, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1005, -2002, N'category.daColumnDisplayName', N'dashboardalert.chart.view.column.category', 0, 5, 1, 1, 1, 150, 1, 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1004, -2002, N'daInformation.daInformationDisplayName', N'dashboardalert.chart.view.column.dainformation.displayname', 0, 5, 1, 1, 1, 150, 1, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1003, -2002, N'description', N'dashboardalert.chart.view.column.description', 1, 5, 1, 1, 1, 200, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1002, -2002, N'name', N'dashboardalert.chart.view.column.name', 1, 5, 1, 1, 1, 150, 1, 1, NULL, 0, NULL, NULL, N'displayChart', NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-1001, -2002, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-957, -2001, N'daColumn.uomDisplayName', N'dashboardalert.alertcriteria.view.column.uom', 1, 5, 1, 1, 1, 150, 1, 6, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-956, -2001, N'threshold', N'dashboardalert.alertcriteria.view.column.thresholdValue', 1, 5, 1, 1, 1, 150, 1, 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-955, -2001, N'operandType', N'dashboardalert.alertcriteria.view.column.operation', 1, 3, 0, 1, 1, 150, 1, 4, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-954, -2001, N'daColumn.daColumnDisplayName', N'dashboardalert.alertcriteria.view.column.dataParameter', 1, 5, 1, 1, 1, 150, 1, 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-953, -2001, N'relation', N'dashboardalert.alertcriteria.view.column.relation', 1, 3, 0, 1, 1, 150, 1, 2, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-952, -2001, N'alert.alertName', N'dashboardalert.alert.view.column.alertName', 1, 5, 1, 1, 1, 150, 1, 1, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-951, -2001, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-910, -2000, N'dashboard.name', N'dashboardalert.alert.view.column.linkedDashboard', 0, 5, 1, 1, 1, 150, 1, 9, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-909, -2000, N'alertStatus', N'dashboardalert.alert.view.column.alertStatus', 1, 3, 0, 1, 1, 150, 1, 8, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-908, -2000, N'lastAlertEventTime', N'dashboardalert.alert.view.column.lastAlertEventTime', 0, 5, 0, 1, 1, 150, 1, 7, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-907, -2000, N'lastEvaluationTime', N'dashboardalert.alert.view.column.lastEvaluationTime', 0, 5, 0, 1, 1, 150, 0, 6, NULL, 2, N'formatTimeWithTimeZone', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-906, -2000, N'reNotificationFrequency.descriptiveText', N'dashboardalert.alert.view.column.frequency', 0, 5, 0, 1, 1, 150, 1, 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-905, -2000, N'emailAddress', N'dashboardalert.alert.view.column.emailAddress', 0, 5, 1, 1, 1, 150, 1, 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-904, -2000, N'alertNotificationPriority', N'dashboardalert.alert.view.column.alertNotificationPriority', 1, 3, 0, 1, 1, 150, 1, 3, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-903, -2000, N'daInformation.daInformationDisplayName', N'dashboardalert.alert.view.column.dainformation.displayname', 0, 5, 1, 1, 1, 150, 1, 2, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-902, -2000, N'alertName', N'dashboardalert.alert.view.column.alertName', 1, 5, 1, 1, 1, 150, 1, 1, NULL, 0, NULL, NULL, N'displayAlert', NULL, NULL, NULL, 1, NULL, NULL, NULL); 
			INSERT INTO [voc_column] ([id], [view_id], [field], [display], [sorted], [sortType], [required], [displayable], [sort_asc], [width], [visible], [column_order], [data_type_id], [operand_type], [display_function], [display_param], [painter_function], [tool_tip_function], [extra_fields], [extra_filter_info], [filterType], [filter_enum_type], [option_key_prefix], [filter_auto_complete_disable]) VALUES (-901, -2000, N'id', NULL, 0, 0, 1, 0, 0, 0, 0, 100, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL); 
			SET IDENTITY_INSERT [voc_column] OFF
			
			--
			-- Updating table voc_column
			--
			PRINT N'Updating voc_column'
			UPDATE [voc_column] SET [display_function]=N'formatTime' WHERE [id]=-13217; 
			UPDATE [voc_column] SET [display_function]=N'formatTime' WHERE [id]=-11413; 
			UPDATE [voc_column] SET [display_function]=N'formatTime' WHERE [id]=-11304; 
			UPDATE [voc_column] SET [display_function]=N'formatTime' WHERE [id]=-10945; 
	
			Print N'Adding Constraint FK6049706B16D0F6D2....'
			ALTER TABLE [voc_column] ADD CONSTRAINT [FK6049706B16D0F6D2] FOREIGN KEY ([view_id]) REFERENCES [voc_view] ([id]) 
			Print N'Adding Constraint FK6049706B971DA1E7....'
			ALTER TABLE [voc_column] ADD CONSTRAINT [FK6049706B971DA1E7] FOREIGN KEY ([data_type_id]) REFERENCES [voc_data_type] ([id]) 
		END
	ELSE
		BEGIN
			PRINT N'New voc_columns were already added'
		END
	GO
	
	--
	-- Inserting data into table VOC_FEATURE
	--
	IF (SELECT count(*) FROM voc_feature WHERE id = -1086) = 0
		BEGIN
			PRINT N'Dropping constraing Updating FK17DCDF26241ED7C2...'
			ALTER TABLE [voc_role_to_feature] DROP CONSTRAINT [FK17DCDF26241ED7C2] 
			PRINT N'Dropping constraing Updating FK17DCDF264DCDD932...'
			ALTER TABLE [voc_role_to_feature] DROP CONSTRAINT [FK17DCDF264DCDD932] 
			PRINT N'Dropping constraing Updating FK35EB80819FA773C...'
			ALTER TABLE [voc_feature] DROP CONSTRAINT [FK35EB80819FA773C] 	
			
			--
			-- Deleting data from table voc_role_to_feature
			--
			PRINT N'Deleting voc_feature'
			DELETE FROM [voc_feature] WHERE [id]=-1122; 
			DELETE FROM [voc_feature] WHERE [id]=-1121; 
			DELETE FROM [voc_feature] WHERE [id]=-1120; 
			
			PRINT N'Inserting new rows into voc_feature'
			SET IDENTITY_INSERT [voc_feature] ON; 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-1086, N'feature.selection.route.change.departureDate', NULL, 0, -1001); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-47, N'feature.dnaAdmin.chart.preview', NULL, 1, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-46, N'feature.dnaAdmin.dashboard.detail.delete', NULL, 0, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-45, N'feature.dnaAdmin.dashboard.delete', NULL, 0, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-44, N'feature.dnaAdmin.dashboard.edit', NULL, 0, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-43, N'feature.dnaAdmin.dashboard.addDrillDownChart', NULL, 0, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-42, N'feature.dnaAdmin.dashboard.myDashboard', NULL, 1, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-41, N'feature.dnaAdmin.dashboard.setAsDefault', NULL, 0, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-40, N'feature.dnaAdmin.dashboard.addChart', NULL, 0, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-39, N'feature.dnaAdmin.dashboard.create', NULL, 0, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-38, N'feature.dnaAdmin.dashboard.view', NULL, 1, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-37, N'feature.dnaAdmin.chart.delete', NULL, 0, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-36, N'feature.dnaAdmin.chart.edit', NULL, 0, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-35, N'feature.dnaAdmin.chart.create', NULL, 0, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-34, N'feature.dnaAdmin.chart.view', NULL, 1, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-33, N'feature.dnaAdmin.alert.delete', NULL, 0, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-32, N'feature.dnaAdmin.alert.edit', NULL, 0, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-31, N'feature.dnaAdmin.alert.view', NULL, 1, -4); 
			INSERT INTO [voc_feature] ([id], [name], [description], [read_only], [group_id]) VALUES (-30, N'feature.dnaAdmin.alert.create', NULL, 0, -4); 
			SET IDENTITY_INSERT [voc_feature] OFF; 
			
			--
			-- Deleting data from table voc_role_to_feature
			--
			PRINT N'Deleting voc_role_to_feature'
			DELETE FROM [voc_role_to_feature] WHERE [feature_id]=-1122; 
			DELETE FROM [voc_role_to_feature] WHERE [feature_id]=-1121; 
			DELETE FROM [voc_role_to_feature] WHERE [feature_id]=-1120; 
			
			--
			-- Inserting data into table voc_role_to_feature
			--
			PRINT N'Inserting new rows in voc_role_to_feature'
			INSERT INTO [voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -47); 
			INSERT INTO [voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -42); 
			INSERT INTO [voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -38); 
			INSERT INTO [voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -34); 
			INSERT INTO [voc_role_to_feature] ([role_id], [feature_id]) VALUES (-3, -31); 
			
			--
			-- Inserting data into table VOC_FEATURE_GROUP
			--
			SET IDENTITY_INSERT [voc_feature_group] ON; 
			INSERT INTO [voc_feature_group] ([id], [name], [description]) VALUES (-4, N'featureGroup.dnaAdmin.name', N'featureGroup.dashboardNAlerts.description'); 
			SET IDENTITY_INSERT [voc_feature_group] OFF; 
			
			SET IDENTITY_INSERT [voc_plugin_modules] ON
			INSERT INTO [voc_plugin_modules] ([moduleId], [name], [moduleVersion], [frameworkVersion], [sequenceNumber], [uri], [navMenuName], [masterHost], [masterPort], [masterContext], [isEnabled], [createdDate]) VALUES (3, N'plugin.module.dashboardAlert', N'2.2', N'2.2', 110, N'/dashboardalert/default.action', N'dashboardalert', NULL, NULL, NULL, 1, NULL)
			SET IDENTITY_INSERT [voc_plugin_modules] OFF
			
			INSERT INTO [voc_plugin_modules_auth] ([moduleId], [authName]) VALUES (3, N'featureGroup.dnaAdmin.name');

			SET IDENTITY_INSERT [voc_system_properties] ON
			INSERT INTO [voc_system_properties] ([systemPropertyId], [name], [value]) VALUES (-1200, N'RegionInterval_Configuration', N'2,4,6,8,10'); 
			SET IDENTITY_INSERT [voc_system_properties] OFF
			
			Print N'Adding Constraint FK17DCDF26241ED7C2....'
			ALTER TABLE [voc_role_to_feature] ADD CONSTRAINT [FK17DCDF26241ED7C2] FOREIGN KEY ([feature_id]) REFERENCES [voc_feature] ([id]) 
			Print N'Adding Constraint FK17DCDF264DCDD932....'
			ALTER TABLE [voc_role_to_feature] ADD CONSTRAINT [FK17DCDF264DCDD932] FOREIGN KEY ([role_id]) REFERENCES [voc_role] ([id]) 
			Print N'Adding Constraint FK35EB80819FA773C....'
			ALTER TABLE [voc_feature] ADD CONSTRAINT [FK35EB80819FA773C] FOREIGN KEY ([group_id]) REFERENCES [voc_feature_group] ([id]) 
		END
	ELSE
		BEGIN
			PRINT N'New voc_feature were already added'; 
		END
	GO		
	
	--
	-- Inserting data into table VOC_DA_INFO
	--
	IF (SELECT count(*) FROM voc_da_info WHERE daId = -8) = 0
		BEGIN
			SET IDENTITY_INSERT [voc_da_info] ON; 
			INSERT INTO [voc_da_info] ([daId], [version], [name], [displayName], [markedForDelete]) VALUES (CAST(-8 AS Decimal(19, 0)), 2, N'RouteOtherStatus', N'com.vocollect.aggregators.RouteOtherStatus', 0); 
			INSERT INTO [voc_da_info] ([daId], [version], [name], [displayName], [markedForDelete]) VALUES (CAST(-7 AS Decimal(19, 0)), 2, N'ProxyDataAggregator', N'com.vocollect.aggregators.ProxyDataAggregator', 0); 
			INSERT INTO [voc_da_info] ([daId], [version], [name], [displayName], [markedForDelete]) VALUES (CAST(-6 AS Decimal(19, 0)), 2, N'InfiniteRegionDataAggregator', N'com.vocollect.aggregators.InfiniteRegionDataAggregator', 0); 
			INSERT INTO [voc_da_info] ([daId], [version], [name], [displayName], [markedForDelete]) VALUES (CAST(-5 AS Decimal(19, 0)), 2, N'RegionAssignmentStatus', N'com.vocollect.aggregators.RegionAssignmentStatus', 0); 
			INSERT INTO [voc_da_info] ([daId], [version], [name], [displayName], [markedForDelete]) VALUES (CAST(-4 AS Decimal(19, 0)), 2, N'Region', N'com.vocollect.aggregators.Region', 0); 
			INSERT INTO [voc_da_info] ([daId], [version], [name], [displayName], [markedForDelete]) VALUES (CAST(-3 AS Decimal(19, 0)), 2, N'AssignmentStatus', N'com.vocollect.aggregators.AssignmentStatus', 0); 
			INSERT INTO [voc_da_info] ([daId], [version], [name], [displayName], [markedForDelete]) VALUES (CAST(-2 AS Decimal(19, 0)), 2, N'Route', N'com.vocollect.aggregators.Route', 0); 
			INSERT INTO [voc_da_info] ([daId], [version], [name], [displayName], [markedForDelete]) VALUES (CAST(-1 AS Decimal(19, 0)), 2, N'OperatorInfo', N'com.vocollect.aggregators.OperatorInfo', 0); 
			SET IDENTITY_INSERT [voc_da_info] OFF
			
			SET IDENTITY_INSERT [voc_da_column] ON;; 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-142 AS Decimal(19, 0)), 2, CAST(-8 AS Decimal(19, 0)), N'percentageroutecomplete', 3, N'aggregator.route.status.field.percentageroutecomplete', N' ', 0, 3, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-141 AS Decimal(19, 0)), 2, CAST(-8 AS Decimal(19, 0)), N'status', 1, N'aggregator.route.status.field.status', N' ', 0, 2, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-140 AS Decimal(19, 0)), 2, CAST(-8 AS Decimal(19, 0)), N'route', 1, N'aggregator.route.status.field.route', N' ', 1, 1, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-127 AS Decimal(19, 0)), 2, CAST(-7 AS Decimal(19, 0)), N'connectionbouncedcount', 2, N'aggregator.proxy.field.connectionbouncedcount', N' ', 0, 8, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-126 AS Decimal(19, 0)), 2, CAST(-7 AS Decimal(19, 0)), N'lutodrexecutionerrorcount', 2, N'aggregator.proxy.field.lutodrexecutionerrorcount', N' ', 0, 7, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-125 AS Decimal(19, 0)), 2, CAST(-7 AS Decimal(19, 0)), N'dataformaterrorcount', 2, N'aggregator.proxy.field.dataformaterrorcount', N' ', 0, 6, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-124 AS Decimal(19, 0)), 2, CAST(-7 AS Decimal(19, 0)), N'averagetriptime', 3, N'aggregator.proxy.field.averagetriptime', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.4', 0, 5, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-123 AS Decimal(19, 0)), 2, CAST(-7 AS Decimal(19, 0)), N'ioerrorcount', 2, N'aggregator.proxy.field.ioerrorcount', N' ', 0, 4, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-122 AS Decimal(19, 0)), 2, CAST(-7 AS Decimal(19, 0)), N'odrrejectioncount', 2, N'aggregator.proxy.field.odrrejectioncount', N' ', 0, 3, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-121 AS Decimal(19, 0)), 2, CAST(-7 AS Decimal(19, 0)), N'proxyserverstatus', 1, N'aggregator.proxy.field.proxyserverstatus', N' ', 0, 2, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-120 AS Decimal(19, 0)), 2, CAST(-7 AS Decimal(19, 0)), N'proxyhostname', 1, N'aggregator.proxy.field.proxyhostname', N' ', 1, 1, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-104 AS Decimal(19, 0)), 2, CAST(-6 AS Decimal(19, 0)), N'itemspicked', 2, N'aggregator.infinite.region.field.itemspicked', N' ', 0, 5, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-103 AS Decimal(19, 0)), 2, CAST(-6 AS Decimal(19, 0)), N'itemsremaining', 2, N'aggregator.infinite.region.field.itemsremaining', N' ', 0, 4, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-102 AS Decimal(19, 0)), 2, CAST(-6 AS Decimal(19, 0)), N'hoursremaining', 3, N'aggregator.infinite.region.field.hoursremaining', N'hrs.', 0, 3, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-101 AS Decimal(19, 0)), 2, CAST(-6 AS Decimal(19, 0)), N'regionnumber', 1, N'aggregator.infinite.region.field.regionnumber', N' ', 0, 2, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-100 AS Decimal(19, 0)), 2, CAST(-6 AS Decimal(19, 0)), N'region', 1, N'aggregator.infinite.region.field.region', N' ', 1, 1, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-85 AS Decimal(19, 0)), 2, CAST(-5 AS Decimal(19, 0)), N'numberofassignments', 2, N'aggregator.region.assignment.status.field.numberofassignments', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.1', 0, 5, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-84 AS Decimal(19, 0)), 2, CAST(-5 AS Decimal(19, 0)), N'status', 1, N'aggregator.region.assignment.status.field.status', N' ', 0, 4, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-83 AS Decimal(19, 0)), 2, CAST(-5 AS Decimal(19, 0)), N'departuredateinterval', 1, N'aggregator.region.assignment.status.field.departuredateinterval', N' ', 0, 3, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-82 AS Decimal(19, 0)), 2, CAST(-5 AS Decimal(19, 0)), N'region', 1, N'aggregator.region.assignment.status.field.region', N' ', 0, 2, 0);; 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-81 AS Decimal(19, 0)), 2, CAST(-5 AS Decimal(19, 0)), N'regionininterval', 1, N'aggregator.region.assignment.status.field.regionininterval', N' ', 1, 1, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-68 AS Decimal(19, 0)), 2, CAST(-4 AS Decimal(19, 0)), N'interval', 2, N'aggregator.region.field.interval', N' ', 0, 8, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-67 AS Decimal(19, 0)), 2, CAST(-4 AS Decimal(19, 0)), N'totalquantity', 2, N'aggregator.region.field.totalquantity', N' ', 0, 7, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-66 AS Decimal(19, 0)), 2, CAST(-4 AS Decimal(19, 0)), N'quantitytopick', 2, N'aggregator.region.field.quantitytopick', N' ', 0, 6, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-65 AS Decimal(19, 0)), 2, CAST(-4 AS Decimal(19, 0)), N'percentassignmentscomplete', 3, N'aggregator.region.field.percentassignmentscomplete', N' ', 0, 5, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-64 AS Decimal(19, 0)), 2, CAST(-4 AS Decimal(19, 0)), N'operatorrequired', 2, N'aggregator.region.field.operatorrequired', N' ', 0, 4, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-63 AS Decimal(19, 0)), 2, CAST(-4 AS Decimal(19, 0)), N'operatorsworking', 2, N'aggregator.region.field.operatorsworking', N' ', 0, 3, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-62 AS Decimal(19, 0)), 2, CAST(-4 AS Decimal(19, 0)), N'departuredateinterval', 1, N'aggregator.region.field.departuredateinterval', N' ', 0, 2, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-61 AS Decimal(19, 0)), 2, CAST(-4 AS Decimal(19, 0)), N'region', 1, N'aggregator.region.field.region', N' ', 1, 1, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-43 AS Decimal(19, 0)), 2, CAST(-3 AS Decimal(19, 0)), N'numberofassignments', 2, N'aggregator.assignment.status.field.numberofassignments', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.1', 0, 3, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-42 AS Decimal(19, 0)), 2, CAST(-3 AS Decimal(19, 0)), N'status', 1, N'aggregator.assignment.status.field.status', N' ', 0, 2, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-41 AS Decimal(19, 0)), 2, CAST(-3 AS Decimal(19, 0)), N'routeatdeparturedate', 1, N'aggregator.assignment.status.field.routeatdeparturedate', N' ', 1, 1, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-30 AS Decimal(19, 0)), 2, CAST(-2 AS Decimal(19, 0)), N'route', 1, N'aggregator.route.field.route', N' ', 0, 10, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-29 AS Decimal(19, 0)), 2, CAST(-2 AS Decimal(19, 0)), N'completedassignments', 2, N'aggregator.route.field.completedassignments', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.1', 0, 9, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-28 AS Decimal(19, 0)), 2, CAST(-2 AS Decimal(19, 0)), N'shortedassignments', 2, N'aggregator.route.field.shortedassignments', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.1', 0, 8, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-27 AS Decimal(19, 0)), 2, CAST(-2 AS Decimal(19, 0)), N'availableassignments', 2, N'aggregator.route.field.availableassignments', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.1', 0, 7, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-26 AS Decimal(19, 0)), 2, CAST(-2 AS Decimal(19, 0)), N'percentageroutecomplete', 3, N'aggregator.route.field.percentageroutecomplete', N' ', 0, 6, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-25 AS Decimal(19, 0)), 2, CAST(-2 AS Decimal(19, 0)), N'operatorrequired', 2, N'aggregator.route.field.operatorrequired', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.1', 0, 5, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-24 AS Decimal(19, 0)), 2, CAST(-2 AS Decimal(19, 0)), N'projecteddeparturedelay', 2, N'aggregator.route.field.projecteddeparturedelay', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.0', 0, 4, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-23 AS Decimal(19, 0)), 2, CAST(-2 AS Decimal(19, 0)), N'projecteddeparturedate', 5, N'aggregator.route.field.projecteddeparturedate', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.3', 0, 3, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-22 AS Decimal(19, 0)), 2, CAST(-2 AS Decimal(19, 0)), N'departuredate', 5, N'aggregator.route.field.departuredate', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.3', 0, 2, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-21 AS Decimal(19, 0)), 2, CAST(-2 AS Decimal(19, 0)), N'routeatdeparturedate', 1, N'aggregator.route.field.routeatdeparturedate', N' ', 1, 1, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-12 AS Decimal(19, 0)), 2, CAST(-1 AS Decimal(19, 0)), N'route', 1, N'aggregator.operator.field.route', N' ', 0, 12, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-11 AS Decimal(19, 0)), 2, CAST(-1 AS Decimal(19, 0)), N'operatorteams', 1, N'aggregator.operator.field.operatorteams', N' ', 0, 11, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-10 AS Decimal(19, 0)), 2, CAST(-1 AS Decimal(19, 0)), N'region', 1, N'aggregator.operator.field.region', N' ', 0, 10, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-9 AS Decimal(19, 0)), 2, CAST(-1 AS Decimal(19, 0)), N'idleafterbreak', 2, N'aggregator.operator.field.idleafterbreak', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.0', 0, 9, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-8 AS Decimal(19, 0)), 2, CAST(-1 AS Decimal(19, 0)), N'idlebeforebreak', 2, N'aggregator.operator.field.idlebeforebreak', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.0', 0, 8, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-7 AS Decimal(19, 0)), 2, CAST(-1 AS Decimal(19, 0)), N'idlebeforesignoff', 2, N'aggregator.operator.field.idlebeforesignoff', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.0', 0, 7, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-6 AS Decimal(19, 0)), 2, CAST(-1 AS Decimal(19, 0)), N'idleaftersignin', 2, N'aggregator.operator.field.idleaftersignin', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.0', 0, 6, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-5 AS Decimal(19, 0)), 2, CAST(-1 AS Decimal(19, 0)), N'breakduration', 2, N'aggregator.operator.field.breakduration', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.0', 0, 5, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-4 AS Decimal(19, 0)), 2, CAST(-1 AS Decimal(19, 0)), N'signon', 4, N'aggregator.operator.field.signon', N'com.vocollect.voicelink.core.model.DataAggregatorFieldType.2', 0, 4, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-3 AS Decimal(19, 0)), 2, CAST(-1 AS Decimal(19, 0)), N'actualrate', 3, N'aggregator.operator.field.actualrate', N' ', 0, 3, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-2 AS Decimal(19, 0)), 2, CAST(-1 AS Decimal(19, 0)), N'routeatdeparturedate', 1, N'aggregator.operator.field.routeatdeparturedate', N' ', 0, 2, 0); 
			INSERT INTO [voc_da_column] ([columnId], [version], [daId], [fieldId], [fieldType], [displayName], [uom], [identityColumn], [columnSequence], [columnState]) VALUES (CAST(-1 AS Decimal(19, 0)), 2, CAST(-1 AS Decimal(19, 0)), N'operator', 1, N'aggregator.operator.field.operator', N' ', 1, 1, 0); 
			SET IDENTITY_INSERT [voc_da_column] OFF
	
			Print N'Default Data Aggregators added'
			
			PRINT N'Creating [FK_DaColumn_DaInfo]...'; 
			ALTER TABLE [voc_da_column] WITH NOCHECK ADD CONSTRAINT [FK_DaColumn_DaInfo] FOREIGN KEY ([daId]) REFERENCES [voc_da_info] ([daId]) ON DELETE CASCADE; 
	
		END
	ELSE
		BEGIN
			PRINT N'New dashboards were already added'
		END
	GO	
	
	--
	-- Inserting data into table VOC_CHARTS
	--
	IF (SELECT count(*) FROM voc_charts WHERE chartId = -11) = 0
		BEGIN
			SET IDENTITY_INSERT [voc_charts] ON
			INSERT INTO [voc_charts] ([chartId], [version], [name], [description], [daInformation], [category], [type], [pivotOn], [pivotFor], [sortBy], [sortOrder]) VALUES (CAST(-11 AS Decimal(19, 0)), 0, N'Routes Completed or Not Started', N'Routes are Not Started until one assignment is complete', CAST(-8 AS Decimal(19, 0)), CAST(-140 AS Decimal(19, 0)), 2, NULL, NULL, NULL, NULL); 
			INSERT INTO [voc_charts] ([chartId], [version], [name], [description], [daInformation], [category], [type], [pivotOn], [pivotFor], [sortBy], [sortOrder]) VALUES (CAST(-9 AS Decimal(19, 0)), 0, N'Items Picked/Remaining', N'Items picked and remaining by region', CAST(-6 AS Decimal(19, 0)), CAST(-101 AS Decimal(19, 0)), 1, NULL, NULL, NULL, NULL); 
			INSERT INTO [voc_charts] ([chartId], [version], [name], [description], [daInformation], [category], [type], [pivotOn], [pivotFor], [sortBy], [sortOrder]) VALUES (CAST(-8 AS Decimal(19, 0)), 0, N'Estimated Hours Remaining', N'Approximate time to complete work by region', CAST(-6 AS Decimal(19, 0)), CAST(-101 AS Decimal(19, 0)), 1, NULL, NULL, NULL, NULL); 
			INSERT INTO [voc_charts] ([chartId], [version], [name], [description], [daInformation], [category], [type], [pivotOn], [pivotFor], [sortBy], [sortOrder]) VALUES (CAST(-7 AS Decimal(19, 0)), 0, N'Region assignment composition', N'Assignments by status in selected region by departure date interval', CAST(-5 AS Decimal(19, 0)), CAST(-84 AS Decimal(19, 0)), 0, NULL, NULL, NULL, NULL); 
			INSERT INTO [voc_charts] ([chartId], [version], [name], [description], [daInformation], [category], [type], [pivotOn], [pivotFor], [sortBy], [sortOrder]) VALUES (CAST(-6 AS Decimal(19, 0)), 0, N'Assignment Progress by Departure Date Interval', N'Percent assignments complete by departure date interval for selected region', CAST(-4 AS Decimal(19, 0)), CAST(-62 AS Decimal(19, 0)), 1, NULL, NULL, NULL, NULL); 
			INSERT INTO [voc_charts] ([chartId], [version], [name], [description], [daInformation], [category], [type], [pivotOn], [pivotFor], [sortBy], [sortOrder]) VALUES (CAST(-5 AS Decimal(19, 0)), 0, N'Operator Requirement', N'Operators required for workload per region by departure date interval', CAST(-4 AS Decimal(19, 0)), CAST(-61 AS Decimal(19, 0)), 2, CAST(-62 AS Decimal(19, 0)), CAST(-64 AS Decimal(19, 0)), NULL, NULL); 
			INSERT INTO [voc_charts] ([chartId], [version], [name], [description], [daInformation], [category], [type], [pivotOn], [pivotFor], [sortBy], [sortOrder]) VALUES (CAST(-4 AS Decimal(19, 0)), 0, N'Assignment status composition', N'Assginments by status in selected route', CAST(-3 AS Decimal(19, 0)), CAST(-42 AS Decimal(19, 0)), 0, NULL, NULL, NULL, NULL); 
			INSERT INTO [voc_charts] ([chartId], [version], [name], [description], [daInformation], [category], [type], [pivotOn], [pivotFor], [sortBy], [sortOrder]) VALUES (CAST(-3 AS Decimal(19, 0)), 0, N'Operator Actual Rate', N'Work rate by operator', CAST(-1 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0)), 1, NULL, NULL, NULL, NULL); 
			INSERT INTO [voc_charts] ([chartId], [version], [name], [description], [daInformation], [category], [type], [pivotOn], [pivotFor], [sortBy], [sortOrder]) VALUES (CAST(-2 AS Decimal(19, 0)), 0, N'Route Projected Departure Date', N'Projected departure dates and delays by route', CAST(-2 AS Decimal(19, 0)), CAST(-21 AS Decimal(19, 0)), 2, NULL, NULL, NULL, NULL); 
			INSERT INTO [voc_charts] ([chartId], [version], [name], [description], [daInformation], [category], [type], [pivotOn], [pivotFor], [sortBy], [sortOrder]) VALUES (CAST(-1 AS Decimal(19, 0)), 0, N'Route Progress', N'Percent work completed by route', CAST(-2 AS Decimal(19, 0)), CAST(-21 AS Decimal(19, 0)), 1, NULL, NULL, CAST(-22 AS Decimal(19, 0)), 0); 
			SET IDENTITY_INSERT [voc_charts] OFF
			
			INSERT INTO [voc_chart_tag] ([tagged_id], [tag_id]) VALUES (CAST(-11 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_tag] ([tagged_id], [tag_id]) VALUES (CAST(-9 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_tag] ([tagged_id], [tag_id]) VALUES (CAST(-8 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_tag] ([tagged_id], [tag_id]) VALUES (CAST(-7 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_tag] ([tagged_id], [tag_id]) VALUES (CAST(-6 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_tag] ([tagged_id], [tag_id]) VALUES (CAST(-5 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_tag] ([tagged_id], [tag_id]) VALUES (CAST(-4 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_tag] ([tagged_id], [tag_id]) VALUES (CAST(-3 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_tag] ([tagged_id], [tag_id]) VALUES (CAST(-2 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_tag] ([tagged_id], [tag_id]) VALUES (CAST(-1 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			
			SET IDENTITY_INSERT [voc_chart_data_fields] ON; 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-101 AS Decimal(19, 0)), 0, CAST(-11 AS Decimal(19, 0)), CAST(-142 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-82 AS Decimal(19, 0)), 0, CAST(-9 AS Decimal(19, 0)), CAST(-104 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-81 AS Decimal(19, 0)), 0, CAST(-9 AS Decimal(19, 0)), CAST(-103 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-71 AS Decimal(19, 0)), 0, CAST(-8 AS Decimal(19, 0)), CAST(-102 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-61 AS Decimal(19, 0)), 0, CAST(-7 AS Decimal(19, 0)), CAST(-85 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-51 AS Decimal(19, 0)), 0, CAST(-6 AS Decimal(19, 0)), CAST(-65 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-41 AS Decimal(19, 0)), 0, CAST(-5 AS Decimal(19, 0)), CAST(-63 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-31 AS Decimal(19, 0)), 0, CAST(-4 AS Decimal(19, 0)), CAST(-43 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-21 AS Decimal(19, 0)), 0, CAST(-3 AS Decimal(19, 0)), CAST(-3 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-13 AS Decimal(19, 0)), 0, CAST(-2 AS Decimal(19, 0)), CAST(-24 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-12 AS Decimal(19, 0)), 0, CAST(-2 AS Decimal(19, 0)), CAST(-23 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-11 AS Decimal(19, 0)), 0, CAST(-2 AS Decimal(19, 0)), CAST(-22 AS Decimal(19, 0))); 
			INSERT INTO [voc_chart_data_fields] ([chartDataFieldId], [version], [chartId], [daColumnId]) VALUES (CAST(-1 AS Decimal(19, 0)), 0, CAST(-1 AS Decimal(19, 0)), CAST(-26 AS Decimal(19, 0))); 
			SET IDENTITY_INSERT [voc_chart_data_fields] OFF
	
			Print N'Default Charts added'
			
			PRINT N'Creating [fk_chart_pivotFor_da_column]...'; 
			ALTER TABLE [voc_charts] WITH NOCHECK ADD CONSTRAINT [fk_chart_pivotFor_da_column] FOREIGN KEY ([pivotFor]) REFERENCES [voc_da_column] ([columnId]); 
			PRINT N'Creating [fk_chart_pivotOn_da_column]...'; 
			ALTER TABLE [voc_charts] WITH NOCHECK ADD CONSTRAINT [fk_chart_pivotOn_da_column] FOREIGN KEY ([pivotOn]) REFERENCES [voc_da_column] ([columnId]); 
			PRINT N'Creating [fk_chart_da_info]...'; 
			ALTER TABLE [voc_charts] WITH NOCHECK ADD CONSTRAINT [fk_chart_da_info] FOREIGN KEY ([daInformation]) REFERENCES [voc_da_info] ([daId]); 
			PRINT N'Creating [fk_chart_sortBy_da_column]...'; 
			ALTER TABLE [voc_charts] WITH NOCHECK ADD CONSTRAINT [fk_chart_sortBy_da_column] FOREIGN KEY ([sortBy]) REFERENCES [voc_da_column] ([columnId]); 
			PRINT N'Creating [fk_chart_category_da_column]...'; 
			ALTER TABLE [voc_charts] WITH NOCHECK ADD CONSTRAINT [fk_chart_category_da_column] FOREIGN KEY ([category]) REFERENCES [voc_da_column] ([columnId]); 
			
			PRINT N'Creating [FKE0637684EEAC00A4]...'; 
			ALTER TABLE [voc_chart_tag] WITH NOCHECK ADD CONSTRAINT [FKE0637684EEAC00A4] FOREIGN KEY ([tagged_id]) REFERENCES [voc_charts] ([chartId]); 
			PRINT N'Creating [FKE06376843EF30942]...'; 
			ALTER TABLE [voc_chart_tag] WITH NOCHECK ADD CONSTRAINT [FKE06376843EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]); 
			
			PRINT N'Creating [FK_DataField_Chart]...'; 
			ALTER TABLE [voc_chart_data_fields] WITH NOCHECK ADD CONSTRAINT [FK_DataField_Chart] FOREIGN KEY ([chartId]) REFERENCES [voc_charts] ([chartId]); 
			PRINT N'Creating [FK_DataField_daColumn]...'; 
			ALTER TABLE [voc_chart_data_fields] WITH NOCHECK ADD CONSTRAINT [FK_DataField_daColumn] FOREIGN KEY ([daColumnId]) REFERENCES [voc_da_column] ([columnId]); 
	
		END
	ELSE
		BEGIN
			PRINT N'New voc_feature were already added'
		END
	GO		
	
	--
	-- Inserting data into table VOC_DASHBOARDS
	--
	IF (SELECT count(*) FROM voc_dashboards WHERE dashboardId = -3) = 0
		BEGIN
			SET IDENTITY_INSERT [voc_dashboards] ON
			INSERT INTO [voc_dashboards] ([dashboardId], [version], [name], [timeFilter], [description]) VALUES (CAST(-3 AS Decimal(19, 0)), 0, N'Remaining Work', 0, N'Remaining progress detail for current shift'); 
			INSERT INTO [voc_dashboards] ([dashboardId], [version], [name], [timeFilter], [description]) VALUES (CAST(-2 AS Decimal(19, 0)), 0, N'Workforce Planning', 0, N'Region tracking and resources required'); 
			INSERT INTO [voc_dashboards] ([dashboardId], [version], [name], [timeFilter], [description]) VALUES (CAST(-1 AS Decimal(19, 0)), 0, N'Delivery Management', 0, N'Route Status Tracking'); 
			SET IDENTITY_INSERT [voc_dashboards] OFF
	
			INSERT INTO [voc_dashboard_tag] ([tagged_id], [tag_id]) VALUES (CAST(-3 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_dashboard_tag] ([tagged_id], [tag_id]) VALUES (CAST(-2 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_dashboard_tag] ([tagged_id], [tag_id]) VALUES (CAST(-1 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
	
			SET IDENTITY_INSERT [voc_dashboard_details] ON
			INSERT INTO [voc_dashboard_details] ([dashboardDetailId], [version], [dashboardId], [chartId], [drillDownChartId], [linkedDrillDownField]) VALUES (CAST(-301 AS Decimal(19, 0)), 0, CAST(-3 AS Decimal(19, 0)), CAST(-9 AS Decimal(19, 0)), NULL, NULL); 
			INSERT INTO [voc_dashboard_details] ([dashboardDetailId], [version], [dashboardId], [chartId], [drillDownChartId], [linkedDrillDownField]) VALUES (CAST(-300 AS Decimal(19, 0)), 0, CAST(-3 AS Decimal(19, 0)), CAST(-8 AS Decimal(19, 0)), NULL, NULL); 
			INSERT INTO [voc_dashboard_details] ([dashboardDetailId], [version], [dashboardId], [chartId], [drillDownChartId], [linkedDrillDownField]) VALUES (CAST(-23 AS Decimal(19, 0)), 0, CAST(-2 AS Decimal(19, 0)), CAST(-5 AS Decimal(19, 0)), NULL, NULL); 
			INSERT INTO [voc_dashboard_details] ([dashboardDetailId], [version], [dashboardId], [chartId], [drillDownChartId], [linkedDrillDownField]) VALUES (CAST(-22 AS Decimal(19, 0)), 0, CAST(-2 AS Decimal(19, 0)), CAST(-5 AS Decimal(19, 0)), CAST(-3 AS Decimal(19, 0)), CAST(-10 AS Decimal(19, 0))); 
			INSERT INTO [voc_dashboard_details] ([dashboardDetailId], [version], [dashboardId], [chartId], [drillDownChartId], [linkedDrillDownField]) VALUES (CAST(-21 AS Decimal(19, 0)), 0, CAST(-2 AS Decimal(19, 0)), CAST(-5 AS Decimal(19, 0)), CAST(-6 AS Decimal(19, 0)), CAST(-61 AS Decimal(19, 0))); 
			INSERT INTO [voc_dashboard_details] ([dashboardDetailId], [version], [dashboardId], [chartId], [drillDownChartId], [linkedDrillDownField]) VALUES (CAST(-20 AS Decimal(19, 0)), 0, CAST(-2 AS Decimal(19, 0)), CAST(-6 AS Decimal(19, 0)), CAST(-7 AS Decimal(19, 0)), CAST(-83 AS Decimal(19, 0))); 
			INSERT INTO [voc_dashboard_details] ([dashboardDetailId], [version], [dashboardId], [chartId], [drillDownChartId], [linkedDrillDownField]) VALUES (CAST(-5 AS Decimal(19, 0)), 0, CAST(-1 AS Decimal(19, 0)), CAST(-11 AS Decimal(19, 0)), NULL, NULL); 
			INSERT INTO [voc_dashboard_details] ([dashboardDetailId], [version], [dashboardId], [chartId], [drillDownChartId], [linkedDrillDownField]) VALUES (CAST(-4 AS Decimal(19, 0)), 0, CAST(-1 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0)), NULL, NULL); 
			INSERT INTO [voc_dashboard_details] ([dashboardDetailId], [version], [dashboardId], [chartId], [drillDownChartId], [linkedDrillDownField]) VALUES (CAST(-3 AS Decimal(19, 0)), 0, CAST(-1 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0)), CAST(-3 AS Decimal(19, 0)), CAST(-2 AS Decimal(19, 0))); 
			INSERT INTO [voc_dashboard_details] ([dashboardDetailId], [version], [dashboardId], [chartId], [drillDownChartId], [linkedDrillDownField]) VALUES (CAST(-2 AS Decimal(19, 0)), 0, CAST(-1 AS Decimal(19, 0)), CAST(-2 AS Decimal(19, 0)), NULL, NULL); 
			INSERT INTO [voc_dashboard_details] ([dashboardDetailId], [version], [dashboardId], [chartId], [drillDownChartId], [linkedDrillDownField]) VALUES (CAST(-1 AS Decimal(19, 0)), 0, CAST(-1 AS Decimal(19, 0)), CAST(-2 AS Decimal(19, 0)), CAST(-4 AS Decimal(19, 0)), CAST(-41 AS Decimal(19, 0))); 
			SET IDENTITY_INSERT [voc_dashboard_details] OFF
	
			Print N'Default Dashboards added'
			
			PRINT N'Creating [FK640BE0BA20D21F90]...'; 
			ALTER TABLE [voc_dashboard_tag] WITH NOCHECK ADD CONSTRAINT [FK640BE0BA20D21F90] FOREIGN KEY ([tagged_id]) REFERENCES [voc_dashboards] ([dashboardId]); 
			PRINT N'Creating [FK640BE0BA3EF30942]...'; 
			ALTER TABLE [voc_dashboard_tag] WITH NOCHECK ADD CONSTRAINT [FK640BE0BA3EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]); 
			
			PRINT N'Creating [FK_Dashboard_Detail]...'; 
			ALTER TABLE [voc_dashboard_details] WITH NOCHECK ADD CONSTRAINT [FK_Dashboard_Detail] FOREIGN KEY ([dashboardId]) REFERENCES [voc_dashboards] ([dashboardId]) ON DELETE CASCADE; 
			PRINT N'Creating [FK_Detail_Parent]...'; 
			ALTER TABLE [voc_dashboard_details] WITH NOCHECK ADD CONSTRAINT [FK_Detail_Parent] FOREIGN KEY ([chartId]) REFERENCES [voc_charts] ([chartId]) ON DELETE CASCADE; 
			PRINT N'Creating [fk_linked_drillDown_field]...'; 
			ALTER TABLE [voc_dashboard_details] WITH NOCHECK ADD CONSTRAINT [fk_linked_drillDown_field] FOREIGN KEY ([linkedDrillDownField]) REFERENCES [voc_da_column] ([columnId]); 
			PRINT N'Creating [FK_Detail_Child]...'; 
			ALTER TABLE [voc_dashboard_details] WITH NOCHECK ADD CONSTRAINT [FK_Detail_Child] FOREIGN KEY ([drillDownChartId]) REFERENCES [voc_charts] ([chartId]); 
	
		END
	ELSE
		BEGIN
			PRINT N'New dashboards were already added'
		END
	GO	
	
	--
	-- Inserting data into table voc_alerts
	--
	IF (SELECT count(*) FROM voc_alerts WHERE alertId = -16) = 0
		BEGIN
			SET IDENTITY_INSERT [voc_alerts] ON
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-16 AS Decimal(19, 0)), 0, N'Connection rejected', CAST(-7 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-15 AS Decimal(19, 0)), 0, N'Error in execution', CAST(-7 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-14 AS Decimal(19, 0)), 0, N'Data format error', CAST(-7 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-13 AS Decimal(19, 0)), 0, N'Average trip time', CAST(-7 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-12 AS Decimal(19, 0)), 0, N'IO error', CAST(-7 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-11 AS Decimal(19, 0)), 0, N'ODR rejection', CAST(-7 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-10 AS Decimal(19, 0)), 0, N'Proxy Server staus', CAST(-7 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-6 AS Decimal(19, 0)), 0, N'Routes missing departure time', CAST(-2 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-5 AS Decimal(19, 0)), 0, N'Operators required', CAST(-4 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-4 AS Decimal(19, 0)), 0, N'Operators idle before Sign off', CAST(-1 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-3 AS Decimal(19, 0)), 0, N'Operators idle after break', CAST(-1 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-2 AS Decimal(19, 0)), 0, N'Operators idle before break', CAST(-1 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			INSERT INTO [voc_alerts] ([alertId], [version], [alertName], [daInformation], [alertNotificationPriority], [emailAddress], [hours], [minutes], [lastAlertEventTime], [lastEvaluationTime], [alertStatus], [dashboard], [serverURL]) VALUES (CAST(-1 AS Decimal(19, 0)), 0, N'Operators taking longer break', CAST(-1 AS Decimal(19, 0)), NULL, N'', 0, 0, NULL, NULL, 0, NULL, NULL); 
			SET IDENTITY_INSERT [voc_alerts] OFF
			
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-16 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-15 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-14 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-13 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-12 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-11 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-10 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-6 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-5 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-4 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-3 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-2 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			INSERT INTO [voc_alert_tag] ([tagged_id], [tag_id]) VALUES (CAST(-1 AS Decimal(19, 0)), CAST(-1 AS Decimal(19, 0))); 
			
			SET IDENTITY_INSERT [voc_alert_criteria] ON; 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-16 AS Decimal(19, 0)), 0, CAST(-16 AS Decimal(19, 0)), 0, CAST(-127 AS Decimal(19, 0)), 2, N'10', 0); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-15 AS Decimal(19, 0)), 0, CAST(-15 AS Decimal(19, 0)), 0, CAST(-126 AS Decimal(19, 0)), 2, N'10', 0); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-14 AS Decimal(19, 0)), 0, CAST(-14 AS Decimal(19, 0)), 0, CAST(-125 AS Decimal(19, 0)), 2, N'10', 0); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-13 AS Decimal(19, 0)), 0, CAST(-13 AS Decimal(19, 0)), 0, CAST(-124 AS Decimal(19, 0)), 2, N'', 0); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-12 AS Decimal(19, 0)), 0, CAST(-12 AS Decimal(19, 0)), 0, CAST(-123 AS Decimal(19, 0)), 2, N'', 0); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-11 AS Decimal(19, 0)), 0, CAST(-11 AS Decimal(19, 0)), 0, CAST(-122 AS Decimal(19, 0)), 2, N'', 0); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-10 AS Decimal(19, 0)), 0, CAST(-10 AS Decimal(19, 0)), 0, CAST(-121 AS Decimal(19, 0)), 8, N'OK', 0); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-7 AS Decimal(19, 0)), 0, CAST(-6 AS Decimal(19, 0)), 0, CAST(-24 AS Decimal(19, 0)), 2, N'', 0); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-6 AS Decimal(19, 0)), 0, CAST(-5 AS Decimal(19, 0)), 1, CAST(-68 AS Decimal(19, 0)), 1, N'', 1); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-5 AS Decimal(19, 0)), 0, CAST(-5 AS Decimal(19, 0)), 0, CAST(-64 AS Decimal(19, 0)), 2, N'', 0); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-4 AS Decimal(19, 0)), 0, CAST(-4 AS Decimal(19, 0)), 0, CAST(-7 AS Decimal(19, 0)), 2, N'', 0); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-3 AS Decimal(19, 0)), 0, CAST(-3 AS Decimal(19, 0)), 0, CAST(-9 AS Decimal(19, 0)), 2, N'', 0); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-2 AS Decimal(19, 0)), 0, CAST(-2 AS Decimal(19, 0)), 0, CAST(-8 AS Decimal(19, 0)), 2, N'', 0); 
			INSERT INTO [voc_alert_criteria] ([criteriaId], [version], [alertId], [criteriaSequence], [daColumn], [operandType], [threshold], [criteriaRelation]) VALUES (CAST(-1 AS Decimal(19, 0)), 0, CAST(-1 AS Decimal(19, 0)), 0, CAST(-5 AS Decimal(19, 0)), 2, N'', 0); 
			SET IDENTITY_INSERT [voc_alert_criteria] OFF
			
			Print N'Default Alerts added'
			
			PRINT N'Creating [fk_alert_da_info]...'; 
			ALTER TABLE [voc_alerts] WITH NOCHECK ADD CONSTRAINT [fk_alert_da_info] FOREIGN KEY ([daInformation]) REFERENCES [voc_da_info] ([daId]); 
			PRINT N'Creating [fk_alert_dashboard]...'; 
			ALTER TABLE [voc_alerts] WITH NOCHECK ADD CONSTRAINT [fk_alert_dashboard] FOREIGN KEY ([dashboard]) REFERENCES [voc_dashboards] ([dashboardId]); 
			
			PRINT N'Creating [FK2AF839023AF316A0]...'; 
			ALTER TABLE [voc_alert_tag] WITH NOCHECK ADD CONSTRAINT [FK2AF839023AF316A0] FOREIGN KEY ([tagged_id]) REFERENCES [voc_alerts] ([alertId]); 
			PRINT N'Creating [FK2AF839023EF30942]...'; 
			ALTER TABLE [voc_alert_tag] WITH NOCHECK ADD CONSTRAINT [FK2AF839023EF30942] FOREIGN KEY ([tag_id]) REFERENCES [voc_tag] ([id]); 
			
			PRINT N'Creating [FK_Criteria_Alert]...'; 
			ALTER TABLE [voc_alert_criteria] WITH NOCHECK ADD CONSTRAINT [FK_Criteria_Alert] FOREIGN KEY ([alertId]) REFERENCES [voc_alerts] ([alertId]); 
			PRINT N'Creating [fk_alert_criteria_dacolumn]...'; 
			ALTER TABLE [voc_alert_criteria] WITH NOCHECK ADD CONSTRAINT [fk_alert_criteria_dacolumn] FOREIGN KEY ([daColumn]) REFERENCES [voc_da_column] ([columnId]); 
		END
	GO
	
	-- *****************************************************************************************
	-- Updating data in table VOC_SYSTEM_PROPERTIES
	-- This will need to be added to all future upgrade scripts
	-- ****************************************************************************************
	Print N'Updating voc_system_proerties so import_setup_xml will be forced to rebuild...'; 
	UPDATE [voc_system_properties] set [value] = 1 WHERE [systemPropertyId] = -1005; 
	GO
	
	--
	-- Update module versions
	--	
	IF (SELECT count(*) FROM voc_plugin_modules WHERE moduleVersion = '4.3') = 0
		BEGIN
			-- Update module versions
			-- To update the version, change the SET @vlVersion... line below
			-- This needs to be done every time there is a new application added to VoiceLink
			SET TRANSACTION ISOLATION LEVEL SERIALIZABLE; 
			SET XACT_ABORT ON; 
			BEGIN TRANSACTION; 
				DECLARE @vlVersion varchar(32); 
				DECLARE @vipVersion varchar(32); 
				SET @vlVersion = '4.3'; 
				SET @vipVersion = '2.2'; 
				DECLARE @newId bigint; 
				DECLARE @oldId bigint; 
				SET @oldId = (select moduleId from voc_plugin_modules where name = 'plugin.module.voicelink' AND isEnabled = 1); 
				Print N'Updating plugin modules...'
				UPDATE voc_plugin_modules set isEnabled = 0 where name = 'plugin.module.voicelink'; 
				Print N'Inserting new plugin module'
				INSERT into voc_plugin_modules (name, moduleVersion, frameworkVersion, sequenceNumber, uri, navMenuName, isEnabled, createdDate) 
				VALUES ('plugin.module.voicelink', @vlVersion, @vipVersion, 101, '/selection/home.action', 'voicelink', 1, convert(varchar(10),getdate(),101)); 
				SET @newId = (select moduleId from voc_plugin_modules where name = 'plugin.module.voicelink' AND isEnabled = 1); 
				UPDATE voc_plugin_components set moduleId = @newId where moduleId = @oldId; 
				UPDATE voc_plugin_modules_auth set moduleId = @newId where moduleId = @oldId; 
				
				UPDATE voc_plugin_modules SET moduleVersion = @vipVersion WHERE moduleId = 1; 
				UPDATE voc_plugin_modules SET frameworkVersion = @vipVersion WHERE moduleId = 1; 
				UPDATE voc_plugin_modules SET moduleVersion = @vipVersion WHERE moduleId = 2; 
				UPDATE voc_plugin_modules SET frameworkVersion = @vipVersion WHERE moduleId = 2; 
			COMMIT TRANSACTION
		END
	GO